<?php
class Monitoring_model extends CI_Model{
    //put your code here
    
    public function getData() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        error_reporting(-1);
        $data = array(
            'id',
            'ip_address',
            'timestamp',
            'data'
        );
        $this->db->select($data);
        $today = time();
        $last = time() - 30*60;
        $where = [
            'timestamp >=' => $last,
            'timestamp <= ' => time()
        ];
        $this->db->order_by('timestamp', 'desc');
        $this->db->where($where);
        $query = $this->db->get("ci_sessions");
        $no = 1;
        $users = [];
        foreach ($query->result() as $row)
        {   
            $session_data = $row->data;
            $return_data = array();
            $offset = 0;
            while ($offset < strlen($session_data)) {
                if (!strstr(substr($session_data, $offset), "|")) {
                    throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
                }
                $pos = strpos($session_data, "|", $offset);
                $num = $pos - $offset;
                $varname = substr($session_data, $offset, $num);
                $offset += $num + 1;
                $data = unserialize(substr($session_data, $offset));
                $return_data[$varname] = $data;
                $offset += strlen(serialize($data));
            }
            $args = [];
            if(!empty($return_data['web_user'])){
                $accessKey = $return_data['web_user']->access_key;
                if ($accessKey == 'member') {                
                    $username = $return_data['web_user']->username;
                    $ip = $row->ip_address;
                    $fullname = $return_data['web_user']->full_name;
                    $timeGenerate = $return_data['__ci_last_regenerate'];
                    $users[$username]['ip'] = $ip;
                    $users[$username]['full_name'] = $fullname;
                    $users[$username]['time'] = $row->timestamp;
                    $no++;
                }
            }
        }
        return $users;
    }
}