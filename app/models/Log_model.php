<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends CI_Model {
	protected $_table = 'user_logs';

	public function __construct()
	{
		parent::__construct();
	}
	public function generateLog( $args = array() )
	{
		try {
			$this->db->insert( $this->_table , $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getLog($where = [])
	{
		try {
			$log = $this->db->where($where)->limit(1)->get( $this->_table)->result();
			if(!empty($log)){
				return $log[0];
			}
			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function updateLog($where = [], $args = [])
	{
		try {
			$this->db->where($where);
			return $this->db->update($this->_table, $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
}