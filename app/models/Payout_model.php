<?php defined('BASEPATH') or exit('No direct script access allowed');

class Payout_Model extends CI_Model 
{

	public function __construct() 
	{
		parent::__construct();		
	}

	public function addPayout($args = [])
	{
		try{

			if ($this->db->insert('payouts', $args)){			
				return $this->db->insert_id();
			}

		} catch(Exception $ex) {
			echo $ex->getMessage();
		}

		return false;
	}

	public function getPayout($where = [])
    {
        try {
			$query = $this->db->where($where)->get('payouts');
			if ($query) {
				if (is_object($query->row())) {
					return $query->row();
				}
			}

			return null;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	exit;
		}

		return null;
    }
}