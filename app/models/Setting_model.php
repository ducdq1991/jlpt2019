<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	public function getOption($where = [])
	{
		try{
			$this->db->where_in( 'option_name', $where );
			$query = $this->db->get('settings');
			$result = $query->result();
			$option = array();
			if($result)
				foreach ($result as $value) {
					$option[$value->option_name] = $value->option_value;
				}
				return $option;
			return false;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	return false;
        }
	}
}
