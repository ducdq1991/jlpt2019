<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getQuestions($where = [], $limit = 0, $random = 0)
	{
		try{
			if ($limit > 0) {
				if (isset($where['NOT_IN']) && !empty($where['NOT_IN'])) {
					$notIN = $where['NOT_IN'];
					unset($where['NOT_IN']);
				}

				$this->db->where($where);
				if (isset($notIN)) {
					$this->db->where_not_in($notIN);
				}

				if ($random == 0) {
					$this->db->order_by('questions.id', 'asc');
				} else {
					$this->db->order_by('questions.id', 'RANDOM');
				}
				
				$this->db->limit($limit);
				$query = $this->db->get('questions');
				if ($query) {
					if ($result = $query->result()) {
						return $result;
					}
				}
			}
			return null;			
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	return false;
        }
	}

	public function addQuestionUser($args)
	{
		try {
			return $this->db->insert('question_users', $args);
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	return false;
		}
	}

	public function getQuestionUser($where = [])
	{
		try {
			$query = $this->db->where($where)->get('question_users');
			if ($query) {
				if (is_object($query->row())) {
					return $query->row();
				}
			}

			return null;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	return false;
		}		
	}

	public function getListQuestions($where = [])
	{
		try {
			$query = $this->db->where($where)->get('question_users');
			if ($query) {
				if (!empty($query->result())) {
					return $query->result();
				}
			}

			return null;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	return false;
		}			
	}
}
