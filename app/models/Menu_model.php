<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}	


    public function menu($id = '') {
		$where = array();
		$firstHtml = '<ul class="nav navbar-nav">';
		$lastHtml = '</ul>'; 
		if(is_string($id)){
			$menu = $this->getMenuId($id);
			$id = $menu->id;
		}		
		$html = $this->getMenuHtml($id, $parentId = 0, $currentId = 0, $str = '');
		return $firstHtml . $html . $lastHtml;
    }	

    public function getMenuHtml($id = '', $parentId = 0, $currentId = 0, $str = '')
    {
		$html = '';
		$where['menu_items.menu_id'] = $id;      
		$where['menu_items.parent_id'] = $parentId;
		$where['menu_items.status'] = 0;
		$this->db->select('*');
		$this->db->from('menu_items');      
		$this->db->where($where);
		$result = $this->db->get()->result();   
		$active = '';
		if($parentId > 0)
		{
			$html. '<ul>';
		}
		foreach ($result as $item) {          
		    if($currentId === (int) $item->id) {  
		    	$active = 'active';             
		        $html .= '<li ' . $active . ' data-id="' . $item->id . '"><a href="'.$item->link.'" title="'.$item->name.'">' . $item->name . '</a></li>';
		    } else {
		        $html .= '<li data-id="' . $item->id . '"><a href="'.$item->link.'" title="'.$item->name.'">' . $item->name . '</a></li>';
		    }
		    
		    $html .= $this->getMenuHtml($id, $item->id, $currentId, $str.'--');
		}
		$html. '</ul>';
		return $html;      	
    }

	public function getMenuId($id)
	{
		$query = $this->db->where(array('hash_id'=>$id))->get('menus');
		if($query)
		{
			$result = $query->result();
			return $result[0];
		}
		return null;
	}
	
	public function getMenus($id)
	{
		if(is_string($id)){
			$menu = $this->getMenuId($id);
			$id = $menu->id;
		}	
		$menuId = $id;		
		$where = array('menu_id' => $menuId, 'status' => 0);
		return $this->db->select('id,name,parent_id as parent, link as slug, ordering as number, icon, status')->where($where)->get("menu_items")
					->result_array();		
	}
	
}