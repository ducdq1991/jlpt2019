<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__DIR__) . '/libraries/Facebook/autoload.php';

if(!function_exists('login_facebook_link'))    
{
    function login_facebook_link()
    {    	
        $fb = new Facebook\Facebook([
          'app_id' => APP_ID,
          'app_secret' => SECRET_KEY,
          'default_graph_version' => 'v2.4',
          'default_access_token' => isset($_SESSION['facebook_access_token']) ? $_SESSION['facebook_access_token'] : APP_ID .'|' . SECRET_KEY
        ]);
          
        try {
          $response = $fb->get('/me?fields=id,name');
          $user = $response->getGraphUser();
          //print_r($user);
          //exit; //redirect
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          //echo 'Graph returned an error: ' . $e->getMessage();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          //echo 'Facebook SDK returned an error: ' . $e->getMessage();
        }
         
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email'];
        $loginUrl = $helper->getLoginUrl(base_url() . 'user/loginfacebook', $permissions);

        return $loginUrl; 
    }
}