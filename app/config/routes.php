<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'notfound';
$route['translate_uri_dashes'] = FALSE;

$route['tin-tuc'] = 'article/index';
$route['tin-tuc/page-([\d]+)'] = "article/index/page-$1";
$route['tin-tuc/(:any).html'] = 'article/detail/$1';
$route['tin-tuc/(:any)'] = 'article/category/$1';
$route['tin-tuc/tags/(:any)'] = 'article/tags/$1';
$route['tin-tuc/(:any)/page-([\d]+)'] = "article/category/$1/page-$2";
$route['tin-tuc/tags/(:any)/page-([\d]+)'] = "article/tags/$1/page-$2";
$route['gioi-thieu.html'] = 'article/detail/gioi-thieu';
$route['kho-de'] = 'examstorage/index';
$route['kho-de/(:any).html'] = 'examstorage/detail/$1';
$route['kho-de/page-([\d]+)'] = "examstorage/index/page-$1";
$route['kho-de/(:any)'] = 'examstorage/category/$1';
$route['kho-de/(:any)/page-([\d]+)'] = "examstorage/category/$1/page-$2";
$route['luyen-tap'] = 'practice/index';
$route['luyen-tap/page-([\d]+)'] = 'practice/index/page-$1';
$route['luyen-tap/chuyen-de/(:any)'] = 'practice/category/$1';
$route['luyen-tap/chuyen-de/(:any)/page-([\d]+)'] = 'practice/category/$1/page-$2';
$route['luyen-tap/(:any).html'] = 'practice/detail/$1';
$route['luyen-tap/(:any)'] = 'practice/detail/$1';

$route['khoa-hoc'] = 'courses/index';
$route['khoa-hoc/page-([\d]+)'] = 'courses/index/page-$1';
$route['khoa-hoc/(:any).html'] = 'courses/detail/$1';
$route['khoa-hoc/(:any)'] = 'courses/course/$1';
//$route['khoa-hoc/(:any)/chuyen-de'] = 'courses/sub/$1';
$route['khoa-hoc/(:any)/chuyen-de'] = 'courses/chuyende/$1';
$route['thi-truc-tuyen'] = 'exam/index';
$route['thi-truc-tuyen/([\d]+)'] = 'exam/index/$1';
$route['thi-truc-tuyen/([\d]+)/page-([\d]+)'] = 'exam/index/$1/page-$1';
$route['thi-truc-tuyen/(:any).html'] = 'exam/detail/$1';
$route['thi-truc-tuyen/(:any)'] = 'exam/detail/$1';
$route['thanh-vien'] = 'user/info';
$route['dang-ky'] = 'user/register';
$route['quen-mat-khau'] = 'user/forgotpassword';
$route['thanh-vien/doi-mat-khau'] = 'user/editPassword';
$route['thanh-vien/cau-hoi-da-luu'] = 'user/lessonSave';
$route['thanh-vien/bai-giang-vua-xem'] = 'user/lessonView';
$route['thanh-vien/lich-su-hoat-dong'] = 'user/activityByUser';
$route['cap-nhat-thanh-vien-facebook'] = 'user/profile';
$route['thanh-toan'] = 'payment/index';
$route['thanh-vien/cau-hoi-da-luu/page-([\d]+)'] = 'user/lessonSave/page-$1';
$route['thanh-vien/thanh-tich'] = 'user/achievement';
$route['noi-dung/(:any).html'] = 'page/view/$1';
$route['loi-giai'] = 'practice/answerbyid';
$route['thanh-vien/ket-qua-cuoc-thi-online'] = 'user/examResultOnlineByUser';
$route['thanh-vien/ket-qua-cuoc-thi-online/page-([\d]+)'] = 'user/examResultOnlineByUser/page-$1';
$route['thanh-vien/lich-su-hoat-dong/page-([\d]+)'] = 'user/activityByUser/page-$1';
$route['thanh-vien/lich-su-thanh-toan'] = 'user/paymentHistory';
$route['thanh-vien/lich-su-thanh-toan/page-([\d]+)'] = 'user/paymentHistory/page-$1';
$route['thanh-vien/khoa-hoc-da-mua'] = 'user/courseBought';
$route['thanh-vien/khoa-hoc-da-mua/page-([\d]+)'] = 'user/courseBought/page-$1';
$route['tim-kiem'] = 'search/index';
$route['hoi-dap'] = 'faqs/index';
$route['hoi-dap/page-([\d]+)'] = 'faqs/index/page-$1';
$route['hoi-dap/(:any).html'] = 'faqs/detail/$1';
$route['hoi-dap/(:any)'] = 'faqs/category/$1';
$route['hoi-dap/(:any)/page-([\d]+)'] = "faqs/category/$1/page-$2";
$route['bang-xep-hang'] = 'ranking/index';
$route['bang-xep-hang/page-([\d]+)'] = 'ranking/index/page-$1';
$route['tai-lieu'] = 'document/index';
$route['tai-lieu/page-([\d]+)'] = 'document/index/page-$1';
$route['tai-lieu/(:any).html'] = 'document/detail/$1';
$route['tai-lieu/(:any)'] = 'document/category/$1';
$route['tai-lieu/(:any)/page-([\d]+)'] = "document/category/$1/page-$2";
$route['thanh-vien/(:any)'] = 'user/memberInfo/$1';
$route['noi-dung/(:any)'] = 'page/view/$1';

$route['mon-hoc/(:any)'] = 'subjects/view/$1';
$route['mon-hoc/(:any)/page-([\d]+)'] = 'subjects/view/$1/page-$2';

$route['thu-vien-sach'] = 'books/index';
$route['thu-vien-sach/page-([\d]+)'] = 'books/index/page-$';
$route['sach/(:any)'] = 'books/view/$1';

$route['gio-hang.html'] = 'cart/index';

$route['thong-bao.html'] = 'cart/message';