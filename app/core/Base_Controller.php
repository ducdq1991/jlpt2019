<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Controller extends CI_Controller
{
    protected $_footer_menu = [];
    protected $_head = [];
    protected $_settings = [];

	function __construct()
	{
        parent::__construct();
        $this->websiteOff(); 	   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->loginCookie();

        
        $this->load->library('multi_menu');   
        //Load Models
        $this->load->model("menu_model",'menu');
        $this->load->model('article/article_model');
        $this->load->model('home/index_model');
        $this->load->model('log_model');
        $this->load->model('setting_model');
        $this->load->model('base_model');

        $this->setSettings();

        $config = ['nav_tag_open' => '<ul class="nav navbar-nav">'];  
        $this->_head['mainMenu'] = $this->setMenu($this->getMenu(6), $config);
        
        //get study menu at footer
        $items = $this->menu->getMenus(5);
        $this->multi_menu->set_items($items);
        $this->_footer_menu['footer_study_menu'] = $this->multi_menu->render();
        
        //get study menu at footer
        $items = $this->menu->getMenus(7);
        $this->multi_menu->set_items($items);
        $this->_footer_menu['footer_about_us_menu'] = $this->multi_menu->render();

        //get main menu
        $items = $this->menu->getMenus(2);
        $this->multi_menu->set_items($items);
        //get ads popup left right
        $this->_footer_menu['adsPosition8'] = $this->getBanners(POSITION_8, TYPE_ADS, 1)[0];
        $this->_footer_menu['adsPosition9'] = $this->getBanners(POSITION_9, TYPE_ADS, 1)[0];
        $this->setAdsBottomFix(); 

	}

    protected function setSettings()
    {

        $options = $this->base_model->getOptions();  
        $settings = [];
        if ($options) {
            foreach ($options as $option) {
                $settings[$option->option_name] = $option->option_value;
            }
        }
        $this->_footer['settings'] = $settings;
        $this->_head['settings'] = $settings;
        $this->_settings = $settings;
    } 

    /**
    * [loginCookie]
    * checking ixist cookie, if any then auto login
    */
	public function loginCookie()
    {
        $cookie = json_decode(get_cookie('cookieLogin'));
        if($cookie){
        	$this->session->set_userdata('web_user', $cookie);
        }
    }
    /*
    * [generateLog]
    * generate a log and insert into user log table
    * [int] $type kind of log, 
    * [int] $userId the ID of member who occur action, 
    * [int] data the ID of object related, 
    * [string] $content the message
    */
    public function generateLog( $type, $userId, $data, $content = '', $assigner = 0 )
    {        
        if(in_array($type, [RECHARGE, PAYMENT, PURCHASE_LESSON, PURCHASE_COURSE])){
            $log = null;
        } else {
            $whereLog = [
                'user_id' => $userId,
                'data' => $data,
                'type' => $type,
                'assigner' => $assigner
            ];
            $log = $this->log_model->getLog($whereLog);
        }
        if(!is_null($log)) {
            $args = [
                'updated' => date('Y-m-d H:i:s'),
            ];
            $where = ['id' => $log->id];
            $this->log_model->updateLog( $where, $args );
        } else {
            $args = array(
                'user_id' => (int) $userId,
                'type' => $type,
                'data' => $data,
                'content' => $content,
                'assigner' => $assigner,
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
            );
            $this->log_model->generateLog( $args );
        }
    }
    /**
    * [setCurrentUrl]
    * set current method as session redirector
    */
    public function setCurrentUrl()
    {
        $redirector = current_url();
        $this->session->set_userdata('redirector', $redirector);
    }
    /**
    * [websiteOff]
    * checking website turn off
    */
    public function websiteOff()
    {
        $controller = $this->router->fetch_class();
        if($controller == 'error')
            return true;        
        $where = array('status');
        $this->load->model('setting_model');
        $option = $this->setting_model->getOption($where);
        if($option['status'] == 1) {
            return true;
        } else {
            if($this->session->has_userdata('user'))
                return true;
        }
        redirect(base_url('error'));
        return false;
    }
    public function getMetas()
    {
        $option = array();
        $where = array(
            'meta_keyword', 'meta_description','meta_title'
        );
        $option = $this->setting_model->getOption($where);
        return $option;
    }
    /**
    * [logged]
    * checking user logged
    */
    public function logged()
    {
        $redirector = $this->input->post('redirector');
        if($redirector) {
            $this->session->set_userdata('redirector', $redirector);
        }
        $response = array(
            'code' => 403,
            'message' => 'Bạn chưa đăng nhập',
        );
        if( $this->session->has_userdata('web_user') ) {
            $response = array(
                'code' => 200,
                'message' => 'Bạn đã đăng nhập',
            );
        }
        echo json_encode($response); die;
    }
    /**
    * [get image banner]
    **/
    public function getBanners( $group = '', $type = '' ,$limit = 0 )
    {
        $where = $orderBy = [];
        $where['banner_items.status'] = 1;
        $where['banners.status'] = 1;
        if( $type != '' ){
            $where['banners.type'] = $type;
        }
        if( $group != '' ) {
            $where['banners.group'] = $group;
        }
        $banner = $this->base_model->getImageBaners( $where, $limit);
        if( $banner ) {
           return $banner;
        }
        return null;
    }
    /**
    * [nonXssData]
    * resolve Xss data on form
    */
    public function nonXssData( $data = [] ) {
        if(!empty($data)) {
            $this->load->helper("security");
            $data = $this->security->xss_clean($data);
        }
        return $data;
    }
    /**
    * getEmail
    * get email config
    */
    public function getEmail()
    {
        $where = array('option_name' => 'email');
        $setting = $this->base_model->getOption( $where );
        $email = !empty($setting) ? $setting->option_value : 'chiendau.it@gmail.com';
        return $email;
    }
    /**
    * getName
    * get site name config
    */
    public function getName()
    {
        $where = array('option_name' => 'site_name');
        $setting = $this->base_model->getOption( $where );
        $name = !empty($setting) ? $setting->option_value : 'Qstudy';
        return $name;
    }
    /**
    * getVideo
    * get site name config
    */
    public function getVideo()
    {
        $where = array('option_name' => 'video_intro');
        $setting = $this->base_model->getOption( $where );
        $name = !empty($setting) ? $setting->option_value : 'Qstudy';
        if($setting) {
            return $setting->option_value;
        }
        return false;
    }
    /**
    * [getExamination]
    * get survey exam config
    */
    public function getExamination()
    {        
        $where = array('option_name' => 'examination');
        $setting = $this->base_model->getOption( $where );
        $exam = !empty($setting) ? $setting->option_value : null;
        return $exam;
    }
    public function blockFeatured()
    {
        $data = [];
        $lastestPosts = $this->article_model->articles(['status' => 'publish']);
        $data['lastestPosts'] = $lastestPosts;        
        $whereExamOnline = $orderExamOnline = array();
        $whereExamOnline['type'] = 'online-exam';
        $whereExamOnline['status'] = 1;
        $orderExamOnline['key'] = 'exam.id';
        $orderExamOnline['value'] = 'ASC';
        $examOnline = $this->index_model->getExamsOnline( $whereExamOnline, $limit = 6, $orderExamOnline );
        $data['examOnline'] = $examOnline;     
        return $data;
    }

    public function setAdsBottomFix() {
        //get banner position 10
        $adsScreenBottom = $this->getBanners( POSITION_10, TYPE_ADS, $limit = 1 );
        if (!empty($adsScreenBottom)) {
            $this->_footer_menu['adsScreenBottom'] = $adsScreenBottom[0];
        }
    }

    protected function getMenu($menuId, $deep = 1)
    {
        $this->load->model('base_model');
        $items = $this->base_model->getMenus($menuId, $deep);        
        return $items;
    }
    /*
    * Set footer menu
    */
    protected function setMenu($menus, $config = [])
    {        
        $config['menu_id'] = 'id';   
        $this->multi_menu->set_items($menus);
        return $this->multi_menu->render($config);
    }    

    protected function setPayout($args = [])
    {
        $this->load->model('payout_model');
        $where = [
            'pay_group' => $args['pay_group'],
            'data_id' => $args['data_id'],
            'receiver_id' => $args['receiver_id'],
            'type' => $args['type'],
            'note' => $args['note']
        ];
        $payout = $this->payout_model->getPayout($where);
        if (is_null($payout)) {
            $this->payout_model->addPayout($args);
        }
    }

    protected function checkPayout($args = [])
    {
        $this->load->model('payout_model');
        $where = [
            'pay_group' => $args['pay_group'],
            'data_id' => $args['data_id'],
            'receiver_id' => $args['receiver_id'],
            'type' => $args['type'],
            'note' => $args['note']
        ];
        $payout = $this->payout_model->getPayout($where);
        if (is_null($payout)) {
            return true;
        }

        return false;
    }
}

