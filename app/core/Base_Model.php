<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/**
	* [getUserExam]
	* list user follow rank
	*/
	public function getUserExam($where = [], $limit = 0, $offset = 0, $likes = [])
	{
		try {
			$this->db->select('user_exam.score, user_exam.user_id, users.username, users.rank, users.image, user_exam.created, users.full_name')
			->select_sum( 'time')
			->where($where)
			->limit($limit, $offset)
			->join('users', 'user_exam.user_id = users.id', 'left')
			->group_by('user_id')
			->order_by('score DESC, time ASC', 'created DESC');
            if ($likes) {
                foreach ($likes as $key => $value) {
                    $this->db->like($key, $value);
                }
            }			
			$query = $this->db->get('user_exam')->result();
			if($query) {
				foreach ($query as $key => $value) {
					$query[$key]->time = round($value->time, 1);
					$full_name = $this->getNameuser($value->user_id);
					$query[$key]->name = !empty($full_name) ? $full_name : $this->getUsername($value->user_id);
				}
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getNameUser($id)
	{
		try {
			$where = array(
				'user_id' => (int) $id,
				'meta_key' => 'full_name',
			);
			$query = $this->db->select('meta_value')
			->where($where)
			->limit(1)
			->get('user_metas')
			->result();
			if(!empty($query))
				return $query[0]->meta_value;
			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getUsername($id)
	{
		try {
			$where = array(
				'id' => (int) $id,
			);
			$query = $this->db->select('username')
			->where($where)
			->limit(1)
			->get('users')
			->result();
			if($query)
				return $query[0]->username;
			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/**
	* [get image banner - slider - ads]
	**/
	public function getImageBaners( $where = [], $limit = 0 )
	{
		try {
			$this->db->select('banner_items.title, banner_items.image, banner_items.link, banner_items.desc');
			$this->db->from('banner_items');
			$this->db->join('banners', 'banner_items.parent = banners.id', 'left');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			$this->db->order_by('banner_items.ordering', 'asc');
			$this->db->limit( $limit );
			$query = $this->db->get();
			if( $query ){
				$result = $query->result();
				return $result;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
 	/**
    * [delete question save]
    **/
    public function delQuestionSaved( $where = array() )
    {
    	try {
    		return $this->db->where($where)->delete('user_exam');
    	} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
    }
    /**
     * @name getOption
     * @desc get a row from settings table
     * @param [array] $where
     * */
    public function getOption( $where = array() )
    {
    	try {
			$query = $this->db->where($where)
			->limit(1)
			->get('settings')
			->result();
			if(!empty($query))
				return $query[0];
			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
    }

    public function getOptions($where = [], $whereIn = [])
    {
    	try{
	        $this->db->where($where);
			if (!empty($whereIn)) {
				$this->db->where_in($whereIn['key'], $whereIn['value']);
			}	        	        
	        $query = $this->db->get('settings');
	        if ($query) {
	            return $query->result();
	        }

	        return null;
	    } catch (Exception $ex) {        	
        	return false;
        }
    }


    public function menu($id = '') {
		$where = array();
		$firstHtml = '<ul>';
		$lastHtml = '</ul>'; 
		if(is_string($id)){
			$menu = $this->getMenuId($id);
			$id = $menu->id;
		}		
		$html = $this->getMenuHtml($id, $parentId = 0, $currentId = 0, $str = '');
		return $firstHtml . $html . $lastHtml;
    }	

    public function getMenuHtml($id = '', $parentId = 0, $currentId = 0, $str = '')
    {
		$html = '';
		$where['menu_items.menu_id'] = $id;      
		$where['menu_items.parent_id'] = $parentId;
		$this->db->select('*');
		$this->db->from('menu_items');      
		$this->db->where($where);
		$result = $this->db->get()->result();   
		$active = '';
		if($parentId > 0)
		{
			$html. '<ul>';
		}
		foreach ($result as $item) {          
		    if($currentId === (int) $item->id) {  
		    	$active = 'active';             
		        $html .= '<li ' . $active . ' data-id="' . $item->id . '"><a href="'.$item->link.'" title="'.$item->name.'">' . $item->name . '</a></li>';
		    } else {
		        $html .= '<li data-id="' . $item->id . '"><a href="'.$item->link.'" title="'.$item->name.'">' . $item->name . '</a></li>';
		    }
		    
		    $html .= $this->getMenuHtml($id, $item->id, $currentId, $str.'--');
		}
		$html. '</ul>';
		return $html;      	
    }

	public function getMenuId($id)
	{
		$query = $this->db->where(array('hash_id'=>$id))->get('menus');
		if($query)
		{
			$result = $query->result();
			return $result[0];
		}
		return null;
	}
	
	public function getMenus($id, $deep = 0)
	{
		if(is_string($id)){
			$menu = $this->getMenuId($id);
			$id = $menu->menu_id;
		}	
		$menuId = $id;		
		$where = array('menu_id' => $menuId);
		if($deep == 1) {
			$where['parent_id'] = 0;
		}
		return $this->db->select('id,name,parent_id as parent, link as slug, ordering as number, icon')->where($where)->get("menu_items")
					->result_array();		
	}

	public function getProvinces()
	{
		$query = $this->db->get('provinces');
		if ($query) {
			$result = $query->result();
			return $result;
		}
		
		return null;
	}

	public function getDistricts($where = [])
	{
		$query = $this->db->where($where)->order_by('name', 'ASC')->get('districts');
		if ($query) {
			$result = $query->result();
			return $result;
		}

		return null;
	}    
}