<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Qstudy | Maintenance</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}
#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
	text-align: center;
}
#container img {
	max-height: 300px;
}
p {
	margin: 12px 15px 12px 15px;
	font-size: 18px;
}
</style>
</head>
<body>
	<div id="container">
		<img src="<?php echo base_url().'assets/img/under-construction.jpg';?>">
		<p>
		<?php echo $message; ?>
		</p>
	</div>
</body>
</html>