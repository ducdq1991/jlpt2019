<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" content-type="application/pdf">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php if(!empty($metas)) echo $metas['meta_description']; ?>" >
	<meta name="keywords" content="<?php if(!empty($metas)) echo $metas['meta_keyword']; ?>" >
	<title>
		<?php if(!empty($metas)) {
			echo $metas['meta_title'];
		} else {
			echo isset($title) ? $title : 'VNA - Thay Đổi Tư Duy - Bứt Phá Thành Công';			
		}		
		?>
	</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" />
	<link rel="manifest" href="<?php echo base_url();?>assets/img/favicon/manifest.json">
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/style.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/custom.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/responsive.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/owl-carousel.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
    <link href="<?php echo base_url();?>assets/css/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.js"></script>    
	<script type="text/javascript">
		var siteUrl = '<?php echo base_url();?>';
	</script>	
</head>
<?php 
    $controller = $this->router->fetch_class();
    $method = $this->router->fetch_method();
?>
<body class="<?php echo $controller;?>-<?php echo $method;?>">
<?php
$salemanSS = $this->session->userdata('saleman_ss');
$user = $this->session->userdata('web_user');
if ($user) {
	$userName = !empty($user->full_name) ? $user->full_name : $user->username;
}

?>
	<header>	
		<div id="header-top">
			<div class="topbar">
				<div class="container">
					<ul>
						<?php if (!$user): ?>
						<li><a href="<?php echo base_url();?>dang-ky" title="" class="btn-head-register"><i class="fa fa-edit"></i> Đăng ký</a></li>
						<li><a href="#" title="" id="display-login" class="btn-head-login"><i class="fa fa-user"></i> Đăng nhập</a></li>
						<?php endif;?>

						<?php if ($user): ?>
							<li><a href="/thanh-vien" title=""><i class="fa fa-user"></i> Hello, <?php echo @$userName; ?></a></li>
						<?php endif;?>	

						<?php if ($user): ?>
							<li><a href="/user/logout" title="" class="btn-head-login"><i class="fa fa-sign-out"></i> Thoát</a></li>
						<?php endif;?>
					</ul>
				</div>
			</div>
			<div class="container">
				<div id="logo" class="col-md-3 col-sm-3 col-xs-12">
					<a href="<?php echo base_url();?>" title="" class="desktop">
						<img style="width: 175px;" src="<?php echo base_url();?>assets/img/logo.png" alt="logo-nap" class="">
					</a>
					<a href="<?php echo base_url();?>" title="" class="mobile">
						<img src="<?php echo base_url();?>assets/img/logo-mobile.png" alt="logo-nap" class="img-responsive">
					</a>
				</div>
				<div class="text-center pull-left countdown-thptqg" style="display: none;">
                    <b>Chỉ còn</b>
                    <div id="getting-started">42 ngày 16 giờ 55 phút 56 giây</div>
                    <b>là thi THPT QG 2018</b>
                </div>
                <style>
                    header a.header-logo{
                        line-height: 80px;
                    }
                    .header-logo__img {
                        height:40px;
                    }
                    .countdown-thptqg {
                            font-size: 14px;
    color: #ff0000;
    background-color: #22b9ee30;
    border: 3px solid #20BCF0;
    margin: 0px 10px;
    padding: 5px 10px;
    border-radius: 10px;
                    }
                </style>
                <?php
                	$col1 = 5;
                	$col2 = 3;
                ?>
				<div id="slogan" class="col-md-<?php echo $col1;?> col-sm-<?php echo $col1;?> col-xs-12 text-center">
					<h1>ĐÁP ÁN KỲ THI NĂNG LỰC TIẾNG NHẬT JLPT 2019</h1>					
				</div>
				<div class="col-md-<?php echo $col2;?> col-sm-<?php echo $col2;?> col-xs-12 col-sm-12 desktop text-right">
					<?php if ($user): ?>
						<div id="account" class="login-complate">
							<div class="name-user">
								<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-user"></i>
									Quản lý tài khoản 
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</a>
								<ul class="dropdown-menu sub-accout">
									<li><a href="<?php echo base_url();?>thanh-vien"><i class="fa fa-user"></i> Thông tin cá nhân</a></li>
									<li><a href="<?php echo base_url();?>thanh-vien/doi-mat-khau"><i class="fa fa-key"></i> Thay đổi mật khẩu</a></li>
									<li><a href="<?php echo base_url();?>thanh-vien/lich-su-hoat-dong"><i class="fa fa-user"></i> Hoạt động</a></li>
									
									<li><a href="<?php echo base_url();?>thanh-vien/bai-giang-vua-xem"><i class="fa fa-tags"></i> Bài giảng đã xem</a></li>
									<li><a href="<?php echo base_url();?>thanh-vien/ket-qua-cuoc-thi-online"><i class="fa fa-certificate"></i> Kết quả bài thi</a></li>
									<!--<li><a href="<?php //echo base_url();?>thanh-vien/lich-su-thanh-toan"><i class="fa fa-usd"></i> Lịch sử thanh toán</a></li>
									<li><a href="<?php //echo base_url();?>thanh-toan"><i class="fa fa-usd"></i> Nạp tiền</a></li>
								-->
									<li><a href="<?php echo base_url();?>user/logout"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
								</ul>
							</div>
						</div>
					<?php endif; ?>
				</div>

			</div><!--container header top-->
			<div class="topnav-user-mobile mobile <?php if ($user): ?> userLogged <?php endif;?>">
			<?php if ($user): ?>
				<div class="col-md-12">
					<a href="<?php echo base_url();?>thanh-vien">
						<i class="fa fa-user"></i>
						<?php $userName = !empty($user->full_name) ? $user->full_name : $user->username; ?>
						 <?php echo $userName; ?> 
						<i class="fa fa-caret-down" aria-hidden="true"></i>
					</a>
					<a style="display: inline-block;padding-top: 27px;" href="<?php echo base_url();?>user/logout"> <i class="fa fa-sign-out"></i> Thoát</a>
				</div>
			<?php else: ?>	
				<div class="top-login col-md-12">
					<a href="<?php echo base_url();?>dang-ky" title="" class="btn-head-register"><i class="fa fa-edit"></i> Đăng ký</a>
					<a href="#" title="" id="display-login" class="btn-head-login"><i class="fa fa-user"></i> Đăng nhập</a>
				</div>
			<?php endif; ?>		

			</div>			
			<div id="overlay"><div class="overlay"  style="display:none;"></div></div>
			<div id="login" class="page-login" style="display:none;">
				<div id="wrap-login" class="col-md-3 col-sm-3 col-xs-12">
					<div id="title-login">
						<h2><i class="fa fa-key"></i> Đăng nhập</h2>
						<div class="close-login">
							<i class="fa fa-times"></i>
						</div>
					</div>	
					<div class="content-login">
						<div id="login-normal">
							<form action="<?php echo base_url();?>user/login" id="formLogin" method="post" accept-charset="utf-8">
								<div class="form-username form-login col-md-12 col-sm-12">
									<input type="text" name="username" class="form-control input-username" value="" placeholder="Tên đăng nhập" autofocus>
								</div>
								<div class="fomr-pass form-login col-md-12 col-sm-12">
									<input type="password" class="form-control input-password" name="password" value="" placeholder="Mật khẩu">
								</div>
								<input type="button" name="login" class="btn btn-login" value="Đăng nhập">
								<div class="about-pass">
									<input type="checkbox" name="remember_password" value="1" placeholder="" class="remember_password">
									Ghi nhớ
								</div>								
								<div class="col-md-12 label label-danger errorLogin"></div>
							</form>
						</div>	
						<div id="link-register">
							<a href="<?php echo base_url()?>dang-ky" title="">Đăng ký ngay</a>
							<span> | </span>
							<a href="<?php echo base_url();?>quen-mat-khau" title="">Quên mật khẩu</a>
						</div>
					</div>
				</div>
			</div>
		</div><!--header top-->
		<div id="menu">
			<div class="container">
				<nav class="navbar navbar-default col-md-11 com-sm-11 col-xs-12">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-top" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href=""></a>
                    </div>
                    <div class="collapse navbar-collapse" id="menu-top">
			        	<?php echo $this->multi_menu->render(); ?>
                    </div>
                </nav>
                <div class="dis-hidden-search"><i class="fa fa-search"></i></div>
			</div><!--container nav-->
		</div><!--menu-->	
		<div id="search">
			<div class="container">
				
				<div class="col-md-12 com-sm-12 col-xs-12 search">
					<form action="<?php echo base_url();?>tim-kiem" method="GET">
						<button type="submit"><i class="fa fa-search"></i></button>
		        		<input type="search" class="form-control form-search" name="keyword" value="" placeholder="Tìm kiếm">
		        	</form>
				</div>
	        </div>	
        </div>
	</header><!-- /header -->
	<div class="breadcrumb-header">
		<?php if( !empty($breadcrumb) ){ echo $breadcrumb; } ?>
	</div>
	<section class="main-content">

		<style type="text/css" media="screen">
			.mini-cart{
				    padding-right: 30px;
    margin-top: -4px;
			}
		</style>