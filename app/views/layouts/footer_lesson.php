</section><!--end.main-content-->
<?php $controller = (string) $this->router->fetch_class(); ?>
	<!-- ads popup left-->
	<?php $this->load->view('block/ads_position_8');?>
	<!-- ads popup right-->
	<?php $this->load->view('block/ads_position_9');?>
	<!-- ads screen bottom-->
	<?php $this->load->view('block/ads_bottom_fix');?>
	<div class="overlay-black" style="display: none">
	    <h3>Loading...</h3>
	</div>
	<!--face plugin-->
	<div id="fb-root"></div>
	<script type="text/javascript">
	    window.fbAsyncInit = function() {
	        FB.init({
	          appId : 268509590632517, 
	          cookie:true, 
	          status:true,
	          xfbml : true,
	          version    : 'v2.5'
	        });
	      };
	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/sdk.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'vi'}
	</script>
	<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="1415260232108563"
  logged_in_greeting="Chào em! Thầy có thể giúp được gì cho em không?"
  logged_out_greeting="Chào em! Thầy có thể giúp được gì cho em không?">
</div>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.easy-ticker.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/custom.js?v=1.0"></script> 
	<!-- Toastr script -->
    <script src="<?php echo base_url();?>assets/js/toastr/toastr.min.js"></script>
    <!-- scroll script -->
    <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>


	<script>
	$("#getting-started")
		.countdown("2018/06/24 06:00:00", function(event) {
		$(this).text(
		event.strftime('%D ngày %-H giờ %M phút %S giây')
		);
	});
	</script>    
</body>
</html>