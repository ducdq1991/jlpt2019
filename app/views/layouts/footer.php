</section><!--end.main-content-->
<?php $controller = (string) $this->router->fetch_class(); ?>
<footer class="footer-module-<?php echo $controller;?>">
		<div class="container">
			<div id="footer-1">
				
			</div><!--footer 1-->
			<div id="footer-2" class="text-center">
				<div id="info-license">
					<p>Website đang hoạt động thử nghiệm và chờ giấy phép của Bộ TTTT.</p>
				</div>				
			</div><!--footer 2-->
		</div><!--container footer-->	



	</footer><!--footer-->
	<!-- ads popup left-->
	<?php $this->load->view('block/ads_position_8');?>
	<!-- ads popup right-->
	<?php $this->load->view('block/ads_position_9');?>
	<!-- ads screen bottom-->
	<?php $this->load->view('block/ads_bottom_fix');?>
	<div class="overlay-black" style="display: none">
	    <h3>Loading...</h3>
	</div>
	<!--face plugin-->
	<div id="fb-root"></div>
	<script type="text/javascript">
	    window.fbAsyncInit = function() {
	        FB.init({
	          appId : 268509590632517, 
	          cookie:true, 
	          status:true,
	          xfbml : true,
	          version    : 'v2.5'
	        });
	      };
	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/sdk.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'vi'}
	</script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.easy-ticker.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.countdown.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/custom.js?v=1.0"></script> 
	<!-- Toastr script -->
    <script src="<?php echo base_url();?>assets/js/toastr/toastr.min.js"></script>
    <!-- scroll script -->
    <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>


	<script>
	$("#getting-started")
		.countdown("2018/06/24 06:00:00", function(event) {
		$(this).text(
		event.strftime('%D ngày %-H giờ %M phút %S giây')
		);
	});
	</script>    
</body>
</html>