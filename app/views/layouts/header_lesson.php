<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" content-type="application/pdf">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php if(!empty($metas)) echo $metas['meta_description']; ?>" >
	<meta name="keywords" content="<?php if(!empty($metas)) echo $metas['meta_keyword']; ?>" >
	<title>
		<?php if(!empty($metas)) {
			echo $metas['meta_title'];
		} else {
			echo isset($title) ? $title : 'VNA - Thay Đổi Tư Duy - Bứt Phá Thành Công';			
		}		
		?>
	</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" />
	<link rel="manifest" href="<?php echo base_url();?>assets/img/favicon/manifest.json">
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/style.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/custom.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/responsive.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/owl-carousel.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
    <link href="<?php echo base_url();?>assets/css/toastr/toastr.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.js"></script>    
	<script type="text/javascript">
		var siteUrl = '<?php echo base_url();?>';
	</script>	
</head>
<?php 
    $controller = $this->router->fetch_class();
    $method = $this->router->fetch_method();
?>
<body class="<?php echo $controller;?>-<?php echo $method;?>">
<?php $user = $this->session->userdata('web_user');?>
	<section class="main-content">