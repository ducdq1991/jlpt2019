<?php if( !empty($bannerPosition3) ) : ?>
	<div class="banner position-3">
	    <a href="<?php echo $bannerPosition3->link;?>" title="<?php echo $bannerPosition3->desc;?>">
			<img src="<?php echo base_url().$bannerPosition3->image;?>" alt="<?php echo $bannerPosition3->desc;?>">
		</a>
	</div>
<?php endif; ?>