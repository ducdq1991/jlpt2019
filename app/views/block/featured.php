<div class="box-competition featured_widget_exam">
	<ul class="nav nav-tabs">
	  <li id="tab1" class="active"><a data-toggle="tab" href="#news"><i class="fa fa-newspaper-o"></i> Tin nổi bật</a></li>	  
	</ul>
	<div class="tab-content">
	  <div id="news" class="tab-pane fade in active content-competition">
	  	<ul>
	  		<?php 
	  			if (count($lastestPosts) > 0):
	  				foreach ($lastestPosts as $item):
	  		?>
	  		<li>
	  			<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html"><i class="fa fa-chevron-circle-right"></i> 
	  			<?php echo word_limiter($item->title, 15);?>
	  			</a>
	  		</li>
	  		<?php 
	  				endforeach;
	  			endif;?>
	  	</ul>
	  </div>
	</div>
</div><!--box competition-->