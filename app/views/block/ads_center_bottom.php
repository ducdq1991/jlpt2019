<?php if( !empty($bannerCenterBottom) ) : ?>
<div class="container">
	<div class="banner center center-bottom">
	    <a href="<?php echo $bannerCenterBottom->link;?>" title="<?php echo $bannerCenterBottom->desc;?>">
			<img src="<?php echo base_url().$bannerCenterBottom->image;?>" alt="<?php echo $bannerCenterBottom->desc;?>">
		</a>
	</div>
</div>
<?php endif; ?>