<?php if( !empty($adsScreenBottom) ) : ?>
<div class="container">
	<div class="banner fix fix-bottom">
	    <a href="<?php echo $adsScreenBottom->link;?>" title="<?php echo $adsScreenBottom->desc;?>">
			<img src="<?php echo base_url().$adsScreenBottom->image;?>" alt="<?php echo $adsScreenBottom->desc;?>">
		</a>
	</div>
</div>
<?php endif; ?>