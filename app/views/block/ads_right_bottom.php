<?php if( !empty($adsRightBottom) ) : ?>
<div class="banner right-bottom">
  <?php foreach( $adsRightBottom as $item ) : ?>
    <a href="<?php echo $item->link;?>" title="<?php echo $item->desc;?>">
      <img src="<?php echo base_url().$item->image;?>" alt="<?php echo $item->desc;?>" class="img-responsive">
    </a>
    <br/>
  <?php endforeach; ?>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Quang cao 300x250 -->
	<ins class="adsbygoogle"
	     style="display:inline-block;width:250px;height:250px"
	     data-ad-client="ca-pub-6788049800525386"
	     data-ad-slot="9663279758"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>  
</div>
<?php endif; ?>