<?php if( !empty($bannerCenterCenter) ) : ?>
<div class="container">
	<div class="banner center center-center">
	    <a href="<?php echo $bannerCenterCenter->link;?>" title="<?php echo $bannerCenterCenter->desc;?>">
			<img src="<?php echo base_url().$bannerCenterCenter->image;?>" alt="<?php echo $bannerCenterCenter->desc;?>">
		</a>
	</div>
</div>
<?php endif; ?>