<?php if( !empty($bannerPosition5) ) : ?>
	<div class="banner banner-outer position-5 hidden-sm hidden-xs">
	    <a href="<?php echo $bannerPosition5->link;?>" title="<?php echo $bannerPosition5->desc;?>">
			<img src="<?php echo base_url().$bannerPosition5->image;?>" alt="<?php echo $bannerPosition5->desc;?>">
		</a>
	</div>
<?php endif; ?>