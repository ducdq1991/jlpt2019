<?php if( !empty($adsPosition7) ) : ?>
<div class="banner center-faqs">
  <?php foreach( $adsPosition7 as $item ) : ?>
    <a href="<?php echo $item->link;?>" title="<?php echo $item->desc;?>">
      <img src="<?php echo base_url().$item->image;?>" alt="<?php echo $item->desc;?>" class="img-responsive">
    </a>
    <br/>
  <?php endforeach; ?>
</div>
<?php endif; ?>