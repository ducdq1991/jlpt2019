<?php if( !empty($bannerPosition4) ) : ?>
	<div class="banner banner-outer position-4 hidden-sm hidden-xs">
	    <a href="<?php echo $bannerPosition4->link;?>" title="<?php echo $bannerPosition4->desc;?>">
			<img src="<?php echo base_url().$bannerPosition4->image;?>" alt="<?php echo $bannerPosition4->desc;?>">
		</a>
	</div>
<?php endif; ?>