<?php if( !empty($adsRightTop) ) : ?>
<div class="banner right-top">
  <?php foreach( $adsRightTop as $item ) : ?>
    <a href="<?php echo $item->link;?>" title="<?php echo $item->desc;?>">
      <img src="<?php echo base_url().$item->image;?>" alt="<?php echo $item->desc;?>" class="img-responsive">
    </a>
    <br/>
  <?php endforeach; ?>
</div>
<?php endif; ?>