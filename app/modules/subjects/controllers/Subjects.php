<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subjects extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('subjects_model');
		$this->load->model('courses/courses_model');
		$this->load->library('breadcrumbs');
		$this->load->helper('text');
		$this->output->cache($this->config->item('time_cache'));
	}

	public function index()
	{
		redirect(base_url() . 'notfound');
	}

	public function view($slug = '', $page = 0)
	{
		$data = $where = $like = $orderBy = [];
		if ($slug === '') {
			redirect(base_url() . 'notfound');
		}

		$slug = trim(str_replace('.html', '', $slug));		
		$subject = $this->subjects_model->getSubject(['slug' => $slug]);
		if ($subject === false || empty($subject)) {
			redirect(base_url() . 'notfound');
		}
		$data['subject'] = $subject;

		// Query khoa hoc thuoc mon hoc
		$limit = 20;
		$where['courses.parent'] = 0;
		$where['courses.status'] = 1;
		$where['courses.subject_id'] = $subject->subject_id;

       	$this->load->helper('paging');
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
        	$page = 1;
        }
		$offset = ($page - 1) * $limit;
		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];

		$total = $this->courses_model->totalCourses($where);
		$courses = $this->courses_model->getCourses($where, $limit, $offset, $orderBy);
        $data['courses'] = $courses;
		$data['paging'] = paging( base_url() .'khoa-hoc', $page, $total, $limit );


		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add($subject->name, base_url() . 'noi-dung' . $subject->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();        
        
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($subject->meta_keywords) ? $subject->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($subject->meta_description) ? $subject->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = $subject->name; 

        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('course', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}	
	
}
