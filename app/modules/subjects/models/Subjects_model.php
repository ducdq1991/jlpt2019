<?php defined('BASEPATH') or exit('No direct script access allowed');

class Subjects_model extends Base_Model
{
    public function __construct()
    {        
        parent::__construct();       
    }
    
    public function getSubject($where = [])
    {        
        if (!empty($where)) {  
            $this->db->where($where);          
            $query = $this->db->get('subjects');
            if ($query) {
                return $query->row();    
            }
        }
        return false;
    }
}