<div class="sidebar-news col-md-5 col-sm-5 col-xs-12">
	<?php $this->load->view('block/ads_right_top');?>
	<div class="school-news list-news-sidebar">
		<h1 class="title-sidebar-news title-shool-news"><i class="fa fa-tasks" aria-hidden="true"></i> Tin mới nhất</h1>
		<div class="list-post-sidebar">
			<?php foreach ($widget as $item) : ?>
				<div class="post-sidebar">
					<?php if ($item->image) :?>
						<div class="img-sidebar-news">
							<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
								<img src="<?php echo base_url() . $item->image;?>" alt="" class="img-responsive">
							</a>
						</div>
					<?php endif;?>
					<div class="info">
						<h2 class="title-news-sidebar">
							<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
								<?php echo $item->title;?>
							</a>
						</h2>	
						<p><?php echo $item->description;?></p>
					</div>	
				</div><!--post sidebar-->
			<?php endforeach; ?>	
		</div><!--list news sidebar-->
	</div><!--school news-->
	<?php $this->load->view('block/ads_right_bottom');?>
</div><!--sidebar news-->