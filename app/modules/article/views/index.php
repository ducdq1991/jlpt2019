<div id="article-module" class="module">
	<div class="container">
		<div class="row">
			<div class="news-list col-md-7 col-sm-7 col-xs-12">
				<h1 class="page-title"><?php echo $pageTitle;?></h1>
				<div class="news-items">
					<?php 
					$i=0; 	
					foreach ($news as $item): $i++;
					$newsClass = ($i==1) ? 'outstanding' : ''
					?>
					<div class="news-post <?php echo $newsClass;?>">
						<?php if ($item->image) :?>
							<div class="img-news img">
								<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
									<img src="<?php echo base_url() . $item->image;?>" alt="<?php echo $item->title;?>" class="img-responsive">
								</a>
							</div>
						<?php endif;?>
						<div class="content-news-list">
							<h2 class="title-post-news">
								<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="<?php echo $item->title;?>">
									<?php echo $item->title;?>
								</a>
							</h2>
							<p><?php echo $item->description;?></p>
						</div>
					</div><!--news post-->
					<?php endforeach;?>
				</div><!--news main-->
				<div class="pagination">
					<?php echo $paging;?>
				</div><!--pagination-->
			</div><!--news list-->
			<!--loadSidebar-->
			<?php $this->load->view('article/sidebar'); ?>
			<!--endloadSidebar-->
		</div>
	</div><!--end.container-->
</div><!--end#article-module-->
