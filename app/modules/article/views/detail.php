<div id="article-module" class="module">
	<div class="container">
		<div class="row">
			<div class="news-detail col-md-7 col-sm-7 col-xs-12">
				<h1 class="page-title"><?php echo $pageTitle;?></h1>
				<?php foreach ($newsDetails as $rows) : ?>
					<div class="content-news-detail">					
						<div class="date-push-news">
							Ngày đăng: <?php echo $datePublish; ?>
						</div>
						<div class="content-detail">
	                        <?php
	                            if (isset($rows->metas->link_video_youtube) && ($rows->metas->link_video_youtube != '' || $rows->metas->link_video_youtube != NULL)):
	                                $videoId = @end(explode('watch?v=', $rows->metas->link_video_youtube));
	                        ?>
	                        <script type="text/javascript" src="<?php echo base_url();?>assets/java/pageMinimalDark.js"></script>
	                        <script type="text/javascript" src="<?php echo base_url();?>assets/java/FWDEVPlayer.js"></script>
	                        <script type="text/javascript">
	                            FWDEVPUtils.onReady(function(){
	                                var videoId = '<?php echo $videoId;?>';
	                                init(videoId);
	                            });
	                        </script>
	                        <div id="productWrapper" oncontextmenu="return false">
	                            <div id="myDivMainHolder2">
	                                <div id="myDiv2" style="width:100%"></div>
	                            </div>
	                            <div id="bigshare_overlay_logo">
	                                <img src="<?php echo base_url();?>/assets/content/minimal_skin_dark/logo.png" alt="">
	                            </div>
	                        </div>
	                        <?php endif;?>
							<p><?php echo $rows->content;?></p>
						</div>
					</div><!--content-->
				<?php endforeach; ?>
				<div class="share-online">
					<div class="fb-like" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
					<div class="g-plusone" data-size="medium" data-annotation="none"></div>
				</div>
				<div class="fb-comments" data-href="<?php echo current_url();?>" data-colorscheme="light" data-numposts="5" data-width="100%"></div>
				<?php if( !empty($tags) ) : ?>
					<div class="tags-list">
						<p> Tags:
							<?php foreach ($tags as $item) : ?>
								<a href="<?php echo base_url();?>tin-tuc/tags/<?php echo $item;?>" title="">
									<?php echo $item; ?>
								</a>
							<?php endforeach; ?>
						</p>
					</div>
				<?php endif; ?>
				<?php if( !empty($newsTag) ): ?>
					<div class="relate-article relate-news">
						<h1 class="title-sidebar-news title-realte"><i class="fa fa-newspaper-o"></i> Tin tức liên quan</h1>
						<div class="post-relate">
							<?php foreach ($newsTag as $item) : ?>
								<div class="post-sidebar post-ralate">
									<?php if ($item->image) :?>
										<div class="img-sidebar-news">
											<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
												<img src="<?php echo base_url() . $item->image;?>" alt="" class="img-responsive">
											</a>
										</div>
									<?php endif;?>
									<div class="excerpt-post">
										<h2 class="title-news-sidebar title-news-relate">
											<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
												<?php echo $item->title;?>
											</a>	
										</h2>
										<p><?php echo $item->description;?></p>
									</div>	
								</div><!--post sidebar-->
							<?php endforeach;?>	
						</div>
					</div><!--relate plate-->
				<?php endif; ?>	
				<?php if( !empty($newsCatRelate) ): ?>
					<div id="relate-news-cat" class="relate-article relate-news">
						<h1 class="title-sidebar-news title-realte"><i class="fa fa-list-alt" aria-hidden="true"></i> Tin cùng chuyên mục</h1>
						<div class="post-relate">
							<?php foreach ($newsCatRelate as $item) : ?>
								<div class="post-sidebar post-ralate">
									<h2 class="title-news-sidebar title-news-relate">
										<i class="fa fa-star"></i>
										<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
											<?php echo $item->title;?> 
										</a>	
									</h2>
									 <span> ( <?php echo date('d/m/Y', strtotime( $item->publish_up )); ?> )</span>
								</div><!--post sidebar-->
							<?php endforeach;?>	
						</div>
					</div><!--relate plate-->
				<?php endif; ?>	
			</div><!--news detail-->
			<?php $this->load->view('article/sidebar'); ?>
			<!--endloadSidebar-->
		</div>
	</div><!--end.container-->
</div><!--end#article-module-->	