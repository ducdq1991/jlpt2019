<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends Base_model {

	public function __construct()
	{
		parent::__construct();
	}


	public function articles($where = [], $limit = 10)
	{
		try {
			$this->db->select('post_details.title, post_details.slug, post_details.content, posts.id, posts.image, posts.category_id, post_details.description, posts.created, posts.publish_up, publish_down, post_details.tags, post_details.meta_keywords, post_details.meta_description');
			$this->db->from('posts');
			$this->db->join('post_details','posts.id = post_details.post_id','left');
			$this->db->join('post_metas','posts.id = post_metas.post_id','left');
	        $this->db->order_by('publish_up', 'DESC');
			$this->db->limit($limit);					
			$query = $this->db->where($where)->get();
			if ($query) {
				$result = $query->result();
				return $result;
			}

			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			exit;
		}
	}

    public function getPostMetas($postId)
    {
        $where = ['post_id' => $postId];
        $query = $this->db->where($where)->get('post_metas');
        if ($query) {
            return $query->result();
        }

        return null;
    }

	public function getArticle( $where = array(), $whereIn = array(), $like = array(), $limit = 0, $offset = 0, $orderBy = array() )
	{
		try {
			$this->db->select('post_details.title, post_details.slug, post_details.content, posts.id, posts.image, posts.category_id, post_details.description, posts.created, posts.publish_up, publish_down, post_details.tags, post_details.meta_keywords, post_details.meta_description');
			$this->db->from('posts');
			$this->db->join('post_details','posts.id = post_details.post_id','left');
			$this->db->join('post_metas','posts.id = post_metas.post_id','left');
			if ( !empty($where) ) {
				$this->db->where( $where );
			}
			if( !empty($whereIn) ){
				$this->db->or_where_in('category_id', $whereIn );
			}
			if( !empty($like) ) {
				$this->db->like( $like );
			}
			$this->db->limit($limit, $offset);
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get();
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get total record news]
	**/
	public function totalArticle( $where = array() )
	{
		try{			
			return $this->db->where( $where )->count_all_results('posts');
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	** []
	**/
	public function totalArticleByTag( $where = array(), $like = array() )
	{
		try{	
			$this->db->select('posts.id, post_details.tags');
			$this->db->from('posts');
			$this->db->join('post_details','posts.id = post_details.post_id','left');		
			$this->db->where( $where );
			$this->db->like($like);
			$query = $this->db->get();
			if( $query ) {
				return $query->num_rows();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get category by slug]
	**/
	public function getCategoryBySlug( $where = array() )
	{
		$this->db->select('category_details.category_id, category_details.name, category_details.slug, category_details.meta_keywords, category_details.meta_description')
			 ->from('categories')
			 ->join('category_details','category_details.category_id = categories.id','left')
			 ->where($where);
		$query = $this->db->get();
		$result = $query->result();
		if( $result ) {
			return $result[0];
		}
		return false;
	}
	/**
	* [get category]
	**/
	public function getCategories( $where = array() )
	{
		$this->db->select('categories.id, category_details.category_id, category_details.name, category_details.slug, category_details.meta_keywords, category_details.meta_description')
			 ->from('categories')
			 ->join('category_details','category_details.category_id = categories.id','left')
			 ->where($where);
		$query = $this->db->get();
		if( $query ) {
			return $query->result();
		}
		return false;
	}
}