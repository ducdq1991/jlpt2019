<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_model');
		$this->load->library('breadcrumbs');
		$this->load->helper('text');
        $this->output->cache($this->config->item('time_cache'));
	}
	public function index( $page = 0 )
	{
        $this->setCurrentUrl();
		$this->load->helper('paging');
		$data = $where = $order = $like = $whereIn = [];

      	//Condition to get article
        $where['posts.type'] = 'post';
        $where['posts.status'] = 'publish';
        $order = ['key' => 'posts.id', 'value' => 'DESC'];

        //Pagination     
       	$limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
		$offset = ($page - 1) * $limit;

        //Query
        $total = $this->article_model->totalArticle($where); 		
		$news = $this->article_model->getArticle($where, $whereIn, $like, $limit, $offset, $order);
        $data['news'] = $news;
        $data['paging'] = paging(base_url() .'tin-tuc', $page, $total, $limit); 

		//sidebar
		$widgetWhere = [];
		$widgetWhere['posts.type'] = 'post';
        $widgetWhere['posts.status'] = 'publish';
        $widgetNews = $this->article_model->getArticle($widgetWhere, $whereIn, $like, $limit, $offset, $order);
        $data['widget'] = $widgetNews;

        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Tin tức', base_url().'tin-tuc');        
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $head['metas'] = $this->getMetas();
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }

        $data['pageTitle'] = 'Tin tức';
        $head['metas']['meta_title'] = 'Tin tức';
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('article/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
	/**
	* [article with category]
	**/
	public function category( $cateSlug = '', $page = 0 )
	{
        $this->setCurrentUrl();
		$this->load->helper('paging');
		$data = $where = $orderBy = $like = $whereIn = [];
      	if($cateSlug == ''){
      		redirect(base_url() . 'notfound');
      	}
      	// Get Categories
      	$cateWhere = [];
      	$cateWhere['category_details.lang_id'] = 'vi';
      	$cateWhere['category_details.slug'] = $cateSlug;      	
      	$cateWhere['categories.status'] = 1;
      	$category = $this->article_model->getCategoryBySlug( $cateWhere );
        if(empty($category)){
            redirect(base_url() . 'notfound');
        }        
      	//Get category child
        $whereCateChild = array(
            'parent' => $category->category_id
            );
        $categoryChild = $this->article_model->getCategories( $whereCateChild );
        if( $categoryChild ){
            $idCateChild = [];
            foreach ($categoryChild as $key=>$cateChild) {
               $idCateChild[] = $cateChild->id;
            }
            $idCateParent = array('' => $category->category_id);  
            $idCate = array_merge( $idCateChild, $idCateParent);
        } else {
            $idCate = [];
        }      
      	// Condition to get article
        $where['posts.type'] = 'post';
        $where['posts.status'] = 'publish';
        $where['category_id'] = $category->category_id;
        $Where['post_details.slug !='] = 'gioi-thieu';
        $orderBy['key'] = 'posts.id';
        $orderBy['value'] = 'DESC';
        // Set Breadcrumbs
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Tin tức', base_url().'tin-tuc');
        $this->breadcrumbs->add( ucfirst($category->name), base_url().'tin-tuc'.$category->slug);
        //pagination     
       	$limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->article_model->totalArticle( $where); 
		//news
		$news = $this->article_model->getArticle( $where, $idCate, $like, $limit, $offset, $orderBy );
        $data['news'] = $news;
        $data['paging'] = paging( base_url() .'tin-tuc/'. $category->slug, $page, $total, $limit ); 
		//sidebar
		$widgetWhere = [];
		$widgetWhere['posts.type'] = 'post';
        $widgetWhere['posts.status'] = 'publish';
        $widgetNews = $this->article_model->getArticle( $widgetWhere, $whereIn, $like, $limit, $offset, $orderBy );
        $data['widget'] = $widgetNews;
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
        $metaGeneral = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($category->meta_keywords) ? $category->meta_keywords : $metaGeneral['meta_keyword'],
            'meta_description' => !empty($category->meta_description) ? $category->meta_description : $metaGeneral['meta_description'],
        );
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = $category->name;        
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        $data['pageTitle'] = ucfirst($category->name);
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('article/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
	/**
	* [article with tag]
	**/
	public function tags($tags = '', $page = 0)
	{
        $this->setCurrentUrl();
		$this->load->helper('paging');
		$data = $where = $orderBy = $like = $whereIn = [];
      	if($tags == ''){
      		redirect(base_url());
      	}
      	$tag = urldecode($tags);
      	// Condition to get article
        $where['posts.type'] = 'post';
        $where['posts.status'] = 'publish';
        $like['tags'] = $tag;
        $orderBy['key'] = 'posts.id';
        $orderBy['value'] = 'DESC';
        // Set Breadcrumbs
        $head = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Tags', base_url().'tin-tuc/tags');
        $this->breadcrumbs->add( ucfirst( $tag ), base_url().'tin-tuc/tags/'.$tag);
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Tags | ' . $tag; 
        //pagination     
       	$limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->article_model->totalArticleByTag( $where, $like );
		//news
		$news = $this->article_model->getArticle( $where, $whereIn, $like, $limit, $offset, $orderBy );
        $data['news'] = $news;
        $data['paging'] = paging( base_url() .'tin-tuc/tags/'. $tag, $page, $total, $limit );
		//sidebar
		$widgetWhere = [];
		$widgetWhere['posts.type'] = 'post';
        $widgetWhere['posts.status'] = 'publish';
        $widgetNews = $this->article_model->getArticle( $widgetWhere, $whereIn, $like = [] , $limit, $offset, $orderBy );
        $data['widget'] = $widgetNews;
        $data['widgetTwo'] = '';
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        $data['pageTitle'] = 'Tags';
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('article/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
	/**
	* [detail news]
	**/
	public function detail($slug = '')
	{
        $this->setCurrentUrl();
		$data = $orderBy = $whereIn =  [];
		$where = array('slug' => $slug);
		$orderBy['key'] = 'posts.id';
        $orderBy['value'] = 'DESC';
		$limit = 1;
		$offset = 0;
		$newsDetail = $this->article_model->getArticle($where, $whereIn, $like = [],$limit, $offset);
        //Metas
        $metas = $this->article_model->getPostMetas($newsDetail[0]->id);
        $postMeta = [];
        foreach ($metas as $meta) {
            $postMeta[$meta->meta_key] = $meta->meta_value;
        }
        $newsDetail[0]->metas = (object) $postMeta;

        if (empty($newsDetail)) {
            redirect(base_url() . 'notfound');
        }
        $data['datePublish'] = date('d/m/Y', strtotime( $newsDetail[0]->publish_up ));
		$data['newsDetails'] = $newsDetail;
		$cateId = $newsDetail[0]->category_id;
		$articleId = $newsDetail[0]->id;
		// article relate with tags
		$tags = $newsDetail[0]->tags;
        $newsTags = [];
		if( $tags != '') {
			$tag = explode(',', $tags);
			$data['tags'] = $tag;
			
			foreach ($tag as $tagItem) {
				$like = [
					'tags' => $tagItem
				];
				$limit = 5;
				$result = $this->article_model->getArticle( $where = array('posts.id !=' => $articleId ), $whereIn, $like, $limit, $offset );                
				foreach ($result as $item) {
                    if (!in_array($item, $newsTags)) {
                        $newsTags[] = $item;
                    }
                }
			}
			$data['newsTag'] = $newsTags;            
		} else {
			$data['newsTag'] ='';
		}
		// Set Breadcrumbs
        $head = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
		if($newsDetail[0]->slug != 'gioi-thieu'){
			$this->breadcrumbs->add('Tin tức', base_url().'tin-tuc');
		}
        $this->breadcrumbs->add( $newsDetail[0]->title, base_url().'tin-tuc'.$newsDetail[0]->slug); 
		$head['breadcrumb'] = $this->breadcrumbs->display();
        //SEO
        $metaGeneral = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($newsDetail[0]->meta_keywords) ? $newsDetail[0]->meta_keywords : $metaGeneral['meta_keyword'],
            'meta_description' => !empty($newsDetail[0]->meta_description) ? $newsDetail[0]->meta_description : $metaGeneral['meta_description'],
        );
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = $newsDetail[0]->title;
		//sidebar news new
		$widgetWhere = $widgetTwoWhere = [];
		$widgetWhere['posts.type'] = 'post';
        $widgetWhere['posts.status'] = 'publish';
        $widgetWhere['post_details.slug !='] = 'gioi-thieu';
        $widgetNews = $this->article_model->getArticle( $widgetWhere, $whereIn, $like = [], $limit = 10, $offset, $orderBy );
        $data['widget'] = $widgetNews; 
        //sidebar news with category
        $relateCateWhere['posts.id !='] = $articleId;
        $relateCateWhere['posts.category_id'] = $cateId;
        $relateCateWhere['posts.type'] = 'post';
        $relateCateWhere['posts.status'] = 'publish';
        $newsCatRelate = $this->article_model->getArticle( $relateCateWhere, $whereIn, $like = [], $limit = 5, $offset, $orderBy );
        $data['newsCatRelate'] = $newsCatRelate; 
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }

        $data['pageTitle'] = $newsDetail[0]->title;
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('article/detail', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
}