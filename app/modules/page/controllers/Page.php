<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('article/article_model');
		$this->load->library('breadcrumbs');
		$this->load->helper('text');
		$this->output->cache($this->config->item('time_cache'));
	}

	public function index()
	{
		redirect(base_url() . 'notfound');
	}

	public function view($slug = '')
	{
		$data = $where = $widgetWhere = $whereIn = $like = $orderBy = [];
		if ($slug === '') {
			redirect(base_url() . 'notfound');
		}

		$slug = trim(str_replace('.html', '', $slug));
		$page = $this->page_model->getPage(['slug' => $slug], $limit = 1)[0];
		if ($page === false || empty($page)) {
			redirect(base_url() . 'notfound');
		}
		$data['page'] = $page;
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add($page->title, base_url() . 'noi-dung' . $page->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //sidebar
		$widgetWhere['posts.type'] = 'post';
        $widgetWhere['posts.status'] = 'publish';
        $widgetNews = $this->article_model->getArticle( $widgetWhere, $whereIn, $like, $limit = 10, $offset = 0, $orderBy );
        $data['widget'] = $widgetNews;
        $data['widgetTwo'] = '';
        $head['breadcrumb'] = $this->breadcrumbs->display();
        
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($page->meta_keywords) ? $page->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($page->meta_description) ? $page->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = $page->title; 

        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('page/view', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}	
	
}
