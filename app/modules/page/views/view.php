<div id="page-module" class="module">
	<div class="container">
		<div class="row">		
			<?php 
				if ($page->slug == 'dat-mua-sach') :
			?>
			<h1 class="page-title text-center"><?php echo $page->title;?></h1>

			<div class="content"><?php echo $page->content;?></div>
			<?php else:?>		
			<div class="page-view col-md-7 col-sm-7 col-xs-12">
				<h1 class="page-title"><?php echo $page->title;?></h1>
				<div class="info-content">
					<div class="content"><?php echo $page->content;?></div>
				</div>
			</div>
			<?php $this->load->view('article/sidebar');?>
			<?php endif;?>
		</div>
	</div><!--end.container-->
</div><!--end#page-module-->	