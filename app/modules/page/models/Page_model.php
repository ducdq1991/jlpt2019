<?php defined('BASEPATH') or exit('No direct script access allowed');

class Page_model extends Base_Model
{
    public function __construct()
    {        
        parent::__construct();       
    }
    
    public function getPage( $where = [], $limit = 0 )
    {        
        if (!empty($where)) {
            $this->db->where($where)->limit($limit);
            $query = $this->db->get('pages');
            if ($query) {
                return $query->result();    
            }
        }
        return false;
    }
}