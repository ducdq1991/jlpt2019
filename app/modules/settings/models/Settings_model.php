<?php defined('BASEPATH') or exit('No direct script access allowed');

class Settings_model extends Base_Model
{
    public function __construct()
    {        
        parent::__construct();       
    }
    
    public function getProvinces($where = [])
    {        
	    $this->db->where($where);          
	    $query = $this->db->order_by('ordering', 'asc')->get('provinces');
	    if ($query) {
	        return $query->result();    
	    }

        return null;
    }

    public function getDistricts($where = [])
    {        
	    $this->db->where($where);          
	    $query = $this->db->order_by('name', 'asc')->get('districts');
	    if ($query) {
	        return $query->result();    
	    }
	    
        return null;
    }
}