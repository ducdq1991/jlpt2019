<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('settings_model');
		$this->load->library('breadcrumbs');
		$this->load->helper('text');
	}

	public function districtOptions()
	{
		$provinceId = $this->input->get('province_id');
		$html = '<option value="">--Quận/Huyện--</option>';
		if ($provinceId > 0) {
			$where = ['province_id' => $provinceId];
			$districts = $this->settings_model->getDistricts($where);	
			if ($districts > 0) {
				foreach ($districts as $item) {
					$html .= "<option value='{$item->id}'>{$item->name}</option>";
				}
			}
		}
		
		echo $html;	
	}


}