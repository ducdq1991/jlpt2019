<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('breadcrumbs');
	}

	public function index()
	{
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Trang 404', base_url().'notfound');
        $headData['breadcrumb'] = $this->breadcrumbs->display();        	
		$this->load->view('layouts/header', $headData);
		$this->load->view('layouts/notfound');
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}
}