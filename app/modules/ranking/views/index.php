<div id="ranking-module">
    <div class="container">
        <div class="row">    
            <div class="col-md-12">
                <div class="ranking-wrap ranking-month">
                    <?php if( $this->session->flashdata('success') ) : ?>
                        <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                    <?php endif; ?>
                    <?php if( $this->session->flashdata('error') ) : ?>
                        <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                    <?php if( $this->session->flashdata('warning') ) : ?>
                        <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                    <?php endif; ?>
                        <div class="rank-heading">                            
                            <h1>Bảng xếp hạng</h1>
                            <div class="search-rank">
                                <div class="col-md-6" style="padding-top: 16px;">Tổng số: <strong><?php echo $total;?></strong> Học sinh</div>
                                <div class="col-md-6">
                                    <form action="" method="GET" id="rank-month-search-form" class="form-inline">
                                        <input type="search" class="form-control form-search" name="fullname" value="<?php echo $fullname;?>" placeholder="Tìm theo tên">
                                        <input type="search" class="form-control form-search search-one" name="username" value="<?php echo $username;?>" placeholder="Tìm theo tài khoản">
                                        <button type="submit" class="btn btn-danger" style="font-weight: 700; background: #20BCF0; border-color: #20BCF0">Tìm kiếm</button>                                     
                                    </form>                                    
                                </div>
                            </div>
                        </div>
                        <div class="rank-content">
                            <table class="table-hover table-responsive">
                                <thead>
                                    <tr>
                                        <th width="10%">Hạng</th>
                                        <th width="10%"></th>
                                        <th width="45%">Họ và tên</th>
                                        <th width="15%">Tổng điểm</th>
                                        <th width="20%" class="text-left">Tỉ lệ đúng</th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                <?php if(isset($result) && count($result) > 0) : ?>
                                    <?php   
                                        foreach ($result as $key => $value) : 
                                    ?>
                                        <?php
                                            $item_rank = 'item-rank';
                                            if($value->rank == 1) {
                                                $item_rank = 'item-rank rank-gold';
                                            } elseif ($value->rank == 2) {
                                                $item_rank = 'item-rank rank-sliver';
                                            } elseif($value->rank == 3) {
                                                $item_rank = 'item-rank rank-copper';
                                            }
                                        ?>
                                        <tr class="rank-ordinal">
                                            <td class="<?php echo $item_rank;?>">
                                                <p><?php echo $value->rank; ?></p>
                                            </td>
                                            <td class="rank-image">
                                                <?php if($value->image) :?>
                                                    <img src="<?php echo base_url().$value->image;?>">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url();?>assets/img/user.jpg">
                                                <?php endif;?>
                                            </td>
                                            <td class="text-left">
                                                <a style="padding: 0px 15px;display: inline-flex;" href="<?php echo base_url()?>thanh-vien/<?php echo $value->username;?>" title="">
                                                    <p class="name-user-rank"><?php echo $value->full_name; ?></p>
                                                </a>
                                            </td>
                                            
                                            <td><?php echo number_format($value->total_point); ?></td>
                                            <td class="text-left" style="padding-left: 30px;">
                                                <span class="right-percent" style="font-weight: 700">
                                                <?php
                                                    if ($value->total_exam_question > 0) {
                                                        echo round(($value->total_exam_question_true / $value->total_exam_question) * 100, 2);    
                                                    } else {
                                                        echo 0;
                                                    } 
                                                    
                                                ?> %
                                                </span>
                                                <span>(<?php echo $value->total_exam_question_true;?>/<?php echo $value->total_exam_question;?>)</span>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr><td colspan="5">Không có kết quả phù hợp</td></tr>
                                <?php endif;?>
                                </tbody>
                            </table>
                            <div class="pagination">
                                <?php echo $paging;?>
                            </div><!--pagination-->
                        </div>
                </div>  
            </div>
        </div>
    </div>
</div>