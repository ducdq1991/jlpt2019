<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Ranking
* @author ChienDau
* @copyright Bigshare
* 
*/

class Ranking extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('ranking_model');
        $this->load->library('breadcrumbs');
	}

	/**
	* [index]
	* default page for Ranking package
	**/
	public function index($page = 0)
	{
		$this->setCurrentUrl();
		// Set Breadcrumbs
		$head = $data = $where = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Bảng xếp hạng', base_url().'bang-xep-hang');
        $head['breadcrumb'] = $this->breadcrumbs->display();
		//pagination    
		$this->load->helper('paging'); 
       	$limit = 100;   
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
        	$page = 1;
        }

		$offset = ($page - 1) * $limit;

		$where = $likes = $data = [];
		$username = $this->input->get('username');
		if ($username) {
			$where['users.username'] = $username;
			
		}
		$data['username'] = $username;

		$fullname = $this->input->get('fullname');
		if ($fullname) {
			$likes['users.full_name'] = $fullname;
			
		}
		$data['fullname'] = $fullname;
		$where['users.group_id'] = 25;
        $total = $this->ranking_model->totalCount($where);
		$result = $this->ranking_model->getRanks($where, $likes, $limit, $offset);

		$data['result'] = $result;
		$data['total'] = $total;
		$data['paging'] = paging(base_url() .'bang-xep-hang/', $page, $total, $limit);

        $metas = $this->getMetas();
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Bảng xếp hạng'; 
		$this->load->view('layouts/header', $head);
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
}