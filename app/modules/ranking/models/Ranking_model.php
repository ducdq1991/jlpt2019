<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ranking_model extends Base_Model {

    protected $_table = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function totalCount($where = [])
    {
        $where['users.group_id'] =  25;
        try{            
            return $this->db->where($where)            
            ->count_all_results('users');
        } catch( Exception $ex ){
            
        }

        return 0;
    }

    public function getRanks($where, $like, $limit = 100, $offset = 0)
    {
        try {
            $this->db->select('id,username,full_name,image,total_point, total_exam_question, total_exam_question_true, rank');
            $query = $this->db->where($where);
            if ($like) {
                foreach ($like as $key => $value) {
                    $query = $query->like($key, $value);
                }
            }
            $query = $query->order_by('total_point', 'desc')->limit($limit, $offset)->get('users');
            if ($query->num_rows() > 0) {
                return $query->result();
            }
        } catch (Exception $ex) {
            
        } 

        return null; 
    }
}