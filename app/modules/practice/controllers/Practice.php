<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Exam
* @author ChienDau
* @copyright Bigshare
* 
*/

class Practice extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('exam_model');
        $this->load->model('courses/courses_model');
        $this->load->model('user/user_model');
        $this->load->library('breadcrumbs');
	}

	public function index($page = 0)
	{		
		$this->setCurrentUrl();
		// Set Breadcrumbs
		$head = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Luyện Tập', base_url().'luyen-tap');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
        $this->load->helper('paging');

        $data = $where = $where_in = [];
        //get Course
        $where = [
        	'parent' => 0,
        	'status' => 1,
        ];
        $orderBy = [];

        $orderBy['key'] = 'ordering';
        $orderBy['value'] = 'asc';        
        $listCourses = $this->courses_model->getCourses($where, 0, 0, $orderBy);
        //sub
        $where = [
        	'parent > ' => 0,
        	'status' => 1,
        ];

		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];
        $subCourses = $this->courses_model->getCourses($where, 0,0, $orderBy);        
        $coursesData = [];
        foreach ($listCourses as $item) {
        	$coursesData[$item->id] = $item;       	
        }

        foreach ($subCourses as $item) {
        	if (isset($coursesData[$item->parent]->id) && ($coursesData[$item->parent]->id == $item->parent)) {

        		$coursesData[$item->parent]->sub[]= $item;
        	}        	
        }
        $data['coursesData'] = $coursesData;


        $courseOption = $this->exam_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array(
			'exam.type' => 'online-exam',
			'exam.attribute' => 1, //1: multi-choices (trắc nghiệm)
			'exam.status' => 1,
		);
		//pagination     
       	$limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->exam_model->totalExam( $where);
        //filter
        $time = $this->input->get('time');
        $course_id = $this->input->get('course_id');
        if(!empty($time) && $time != 150) {
        	$where['exam.time'] = $time;
        } elseif (!empty($time) && $time = 150) {
        	$where['exam.time >='] = $time;
        }
        if(!empty($course_id)) {
        	$courseIds = [];
        	$query = $this->exam_model->getCourses(array('parent' => (int) $course_id));
        	if($query) {
	        	foreach ($query as $item) {
	        		$courseIds[] = $item['id'];
	        	}
	        	$courseIds[] = (int) $course_id;
	        	$where_in = array(
	        		'key' => 'exam.course_id',
	        		'value' => $courseIds,
	        	);
	        } else {
	        	$where['exam.course_id'] = (int) $course_id;
	        }
        }
        $data['time'] = $time;
        $data['course_id'] = $course_id;
        $orderby = [];
		$exam = $this->exam_model->getExams( $where, 0, $offset, $orderby);
		$data['paging'] = paging( base_url() .'luyen-tap/', $page, $total, $limit );
		$data['exam'] = $exam;

        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($faq->meta_keywords) ? $faq->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($faq->meta_description) ? $faq->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Luyện Tập'; 

		$this->load->view('layouts/header', $head);
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function category($slug = '', $page = 0) 
	{ 				
		$this->setCurrentUrl();
		// Set Breadcrumbs
		$head = $where = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Luyện Tập', base_url().'luyen-tap');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 

        $this->load->helper('paging');
       	$limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
        	$page = 1;
        }
        $offset = ($page - 1) * $limit;
        $where = ['course_details.slug' => $slug];	
		$course = $this->courses_model->getCourseBySlug($where);		
		$data['course'] = $course;
		if ($course->parent > 0) {
			$parentCourse = $this->courses_model->getParents($course->parent);
			$data['parentCourse'] = $parentCourse;
		} else {
			$data['parentCourse'] = $course;
		}

		//Lay chuyen de con
		$where = ['courses.parent' => $course->id];
		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];				
		$subCourses = $this->courses_model->getCourses($where, 0, 0, $orderBy);
		$data['subCourses'] = $subCourses;

		if (count($subCourses) == 0) {
			$where = [
				'exam.course_id' => $course->id,
				'exam.type' => 'exercise',
				'exam.attribute' => 1,
				'exam.status' => 1,
			];	

			$orderBy = [
				'key' => 'ordering',
				'value' => 'ASC',
			];
			
	        $total = $this->exam_model->totalExam($where);
			$exam = $this->exam_model->getExercise($where, [], $limit, $offset, $orderBy);			
			$data['paging'] = paging( base_url() .'luyen-tap/chuyen-de/' . $slug . '/', $page, $total, $limit );
			$data['exam'] = $exam;	
			$templateContent = 'category';
		} else {
			$templateContent = 'chuyende';
		}

        //get featrured
        $featrured = $this->blockFeatured();
        $data['lastestPosts'] = $featrured['lastestPosts'];
        $data['examOnline'] = $featrured['examOnline'];
        		
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($faq->meta_keywords) ? $faq->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($faq->meta_description) ? $faq->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Luyện Tập'; 

		$this->load->view('layouts/header', $head);
		$this->load->view('' . $templateContent, $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}

    public function createExam($params = [], $courseId = 0, $random = 0)
    {
    	$user = $this->session->userdata('web_user');
    	$questions = $questionName = [];

    	if(!$user || $courseId = 0) {
    		redirect(base_url().'notfound');
    	}    	

    	$this->load->model('question_model');

    	$userId = $user->id;
    	if ($random == 0) {
			foreach ($params as $k => $value) {
	    		$key = (int) $k;
	    		$where = [
	    			'questions.question_category_id' => $key,
	    		];

	    		$result = $this->question_model->getQuestions($where, $value);
	    		if (count($result) > 0) {
		    		foreach ($result as $item) {
		    			$questions[] = (array) $item;
		    		}
		    	}
	    	}

    	} else {

	    	//Lay cac cau hoi hoc vien da su dung de loai bo trong khi query cac cau hoi cho de tiep theo
	    	$usedQuestions = $this->question_model->getListQuestions(['question_users.user_id' => $userId]);
	    	$notIN = [];
	    	if (!is_null($usedQuestions)) {
	    		foreach ($usedQuestions as $q) {
	    			$notIN[] = (int) $q->question_id;
	    		}
	    	}
	    	
	    	
	    	foreach ($params as $k => $value) {
	    		$key = (int) $k;
	    		$where = [
	    			'questions.question_category_id' => $key,
	    			'NOT_IN' => [
	    				'questions.id' => $notIN
	    			]
	    		];

	    		$result = $this->question_model->getQuestions($where, $value, $random);    		
	    		if (count($result) > 0) {
		    		foreach ($result as $item) {
		    			$questions[] = (array) $item;
		    			$questionName[] = $item->name;

		    			// Kiem tra cau hoi da duoc hoc vien su dung hay chua, neu chua thi insert du lieu moi nay vao bang question_users
		    			$where = [
		    				'question_users.question_id' => (int) $item->id,
		    				'question_users.user_id' => (int) $userId,
		    			];
		    			if (is_null($this->question_model->getQuestionUser($where))) {
			    			$args = [
			    				'question_id' => (int) $item->id,
			    				'user_id' => (int) $userId,
			    				'course_id' => 0,
			    				'created_at' => date('Y-m-d H:i:s'),
			    			];
		    				$this->question_model->addQuestionUser($args);
		    			}
		    		}
	    		}
	    	}
	    }

    	return $questions;
    }

	public function detail($slug = '')
	{
		$data = [];

		$isExam = false;

		$user = $this->session->userdata('web_user');
		if (!$user){
			$this->session->set_flashdata('error', 'Bạn cần đăng nhập trước khi làm bài');
			redirect(base_url('khoa-hoc'));
		}

		$where = [
			'exam.slug' => $slug,
			'exam.type' => 'exercise',
			'exam.status' => 1
		];

		$exam = $this->exam_model->getExam($where);
		if (empty($exam)) {
			redirect(base_url() . 'notfound');
		}
		
		//Get course
		$course = $this->courses_model->getParents($exam->course_id);

		$examQuestions = $this->exam_model->getExamQuestion(array('exam_id' => $exam->id, 'user_id' => $user->id));

		// Kiem tra xem hoc sinh da thi bai thi nay chua
		$userExam = $this->exam_model->getExamByUser(array('exam_id' => $exam->id, 'user_id' => $user->id));
		$totalAnswer = $numRightQuestion = 0;
		if (is_object($userExam)) {
			$isExam = true;		
			$data['userExam'] = $userExam;
			$data['userAnswers'] = unserialize($userExam->answer);	

			foreach ($data['userAnswers'] as $value) {				
				if (strlen($value) >= 1) {
					$totalAnswer++;
				}
			}			
		}
		$data['totalAnswer'] = $totalAnswer;
		$data['isExam'] = $isExam;	

		if ($isExam) {			
			$questionIds = @explode(',', $examQuestions->questions);
			$exam->questions = $this->exam_model->getGroupQuestions($questionIds);
			foreach ($exam->questions as $item) {
				if(isset($data['userAnswers'][$item['id']]) && $data['userAnswers'][$item['id']] == $item['true_answer']) {
					$numRightQuestion++;
				}
			}

		} else {
			if ($course) {
				if ($course->price != 0) {
					
					$where = [
						'user_id' => (int) $user->id,
						'course_id' => (int) $course->id,
					];

					if (!$this->courses_model->existUserCourse($where)) {
						$this->session->set_flashdata('warning', 'Để tham gia thi, bạn cần mua khóa học trước');
						redirect(base_url('khoa-hoc/' . $course->slug));
					}
				}

				$args = [
					'exam_id' => $exam->id,
					'user_id' => $user->id,
				];
				
				//Check view count by exam_id
				$where = [
					'exam_id' => $exam->id, 
					'course_id' => $course->id,
					'user_id' => $user->id
				];			
				$totalViewed = $this->courses_model->viewedCount($where);
				if ($totalViewed >= $course->exam_view_limit) {
					$this->session->set_flashdata('warning', 'Lượt thi đã vượt quá giới hạn cho phép!');
					redirect(base_url('luyen-tap/chuyen-de/' . $course->slug));
				} else {
					$this->session->set_flashdata('warning', 'Chú ý: Bạn còn <b>' . ($course->exam_view_limit - $totalViewed) . '<b> lượt thi');
					$this->courses_model->updateViewLimit($course->id, $args);	
				}
			}

			// Dieu kien lay so cau hoi o moi kho
			$extendQuestionConfig = @unserialize($exam->extend_question_config);
			$exam->questions = $this->createExam($extendQuestionConfig, $course->id, $exam->is_random);

	    	$questionIds = [];
	    	foreach ($exam->questions as $item) {
	    		$questionIds[] = $item['id'];
	    	}

	    	$args = array(
	    		'questions' => implode(',', $questionIds),
	    		'exam_id' => $exam->id,
				'user_id' => $user->id,
				'created_at' => date('Y-m-d H:i:s')
	    	);

	    	if (is_null($examQuestions)) {
	    		$this->exam_model->addExamQuestion($args);
	    	} else {
	    		$this->exam_model->updateExamQuestion(array('id' => $examQuestions->id), $args);
	    	}
	    	
		}
		//print_r($exam->questions);
		
		$data['numRightQuestion'] = $numRightQuestion;

		$exam->count_down = (int) $exam->time;
		$exam->publish_up = date('Y-m-d H:i:s');
		$exam->publish_down = date('Y-m-d H:i:s', (time() + (int) $exam->time * 60));

		$data['metas'] = $this->getMetas();    
        $data['metas']['meta_title'] = $exam->title;        		

    	
    	
		//Pass exam data
		$data['exam'] = $exam;

		$this->load->view('detail', $data);
	}

	public function result()
	{
		$sessionUser = $this->session->userdata('web_user');	
		if (empty($sessionUser)) {
			$response = array(
				'code' => 204,
				'message' => 'Bạn cần đăng nhập để làm bài thi.',
				);
			echo json_encode($response); exit;			
		}

		$user = $this->user_model->getUserById($sessionUser->id);
		if (empty($user)) {
			$response = array(
				'code' => 204,
				'message' => 'Tài khoản không tồn tại.',
				);
			echo json_encode($response); exit;			
		}

		$total_exam_question = $user->total_exam_question;
		$total_exam_question_true = $user->total_exam_question_true;
		$total_exam_question_false = $user->total_exam_question_false;
		$total_point = $user->total_point;

		if($this->input->is_ajax_request()) {
			$response = [
				'code' => 204,
				'message' => 'Bạn chưa trả lời câu hỏi nào',
			];

			$options = $this->input->post('option');
			$exam_id = $this->input->post('exam_id');
			if($exam_id) {
				$exam = $this->exam_model->getExamById($exam_id);
			}

			$ids = $result = $answer_right_user = $answer_right = [];
			$numRightQuestion = 0;
			$core = 0;
			$score = 0;

			if ($options && count($options) > 0) {
				foreach ($options as $item) {
					$ids[] = $item['id'];
					$result[$item['id']] = $item['true_answer'];
				}
			}

			$questions = $this->exam_model->getGroupQuestions($ids);
			foreach ($questions as $item) {
				if(isset($result[$item['id']]) && $result[ $item['id'] ] == $item['true_answer']) {
					$answer_right_user[] =  $item['id'];
					$numRightQuestion++;
					$core++;
					$score += $item['score'];
				}

				$answer_right[$item['id']] = $item['true_answer'];
			}

			$where = array(
				'user_id' => (int) $sessionUser->id,
				'exam_id' => (int) $exam_id,
			);
			//print_r($where);
			$time = $this->input->post('time');

			if($exam) {
				if (!$this->exam_model->existUserExam($where)) {		
					$args = array(
						'user_id' => $sessionUser->id,
						'exam_id' => (int) $exam_id,
						'score' => (int) $score,
						'time' => (float) $time,
						'answer' => serialize($result),
						'attribute' => 1, //1 trắc nghiệm 2 là tự luận
						'created' => date('Y-m-d H:i:s'),
					);
					$this->exam_model->insertUserExam( $args );
				}
			}

			//Cap nhat diem thi cho hoc sinh
			$args = [
				'total_exam_question' => $total_exam_question + count($options),
				'total_exam_question_true' => $total_exam_question_true + $numRightQuestion,
				'total_exam_question_false' => $total_exam_question_false + (count($options) - $numRightQuestion),
				'total_point' => $total_point + $numRightQuestion,				
			];
			$this->user_model->updateUser($user->id, $args);


			$exam = $this->exam_model->getExamById($exam_id);
			$name = !empty($user->full_name) ? $user->full_name : $user->username;
			$content = '<a class="name-user" href="'.base_url().'user/profile/'.$user->id.'">'.$name.'</a> đã làm bài thi: <a href="'.base_url().'luyen-tap/'.$exam->slug.'">'.$exam->title.'</a>';
			$this->generateLog(DO_EXERCISE, $user->id, (int) $exam_id, $content );

			$response = [];
			$response['result'] = $answer_right_user;
			$response['answer'] = $answer_right;
			$response['code'] = 200;
			$response['message'] = 'Bạn làm đúng '.$numRightQuestion.' câu.';
			echo json_encode($response); die;
		}
	}

	public function filter()
	{
		$response = array(
			'code' => 201,
			'massage' => 'Không thể lọc'
		);
		$level = $this->input->post('level');
		$examId = $this->input->post('exam_id');
		$where = array(
			'exam.id' => $examId,
			'exam.type' => 'online-exam',
			'exam.attribute' => 1,
		);
		$exam = $this->exam_model->getFilterExam( $where );
		if($exam) {
			$questions = explode(',', $exam->questions);
			if($level == null) {
				$where = [];
			} else {
				$where = [
					'level' => $level
				];
			}
			$ids = $this->exam_model->getFilterQuestions($questions, $where);
			$response['result'] = $ids;
			$response['code'] = 200;
		}
		echo json_encode($response); die;
	}

	public function answerById($questionId = '')
	{
		$flag = false;

		$sessionUser = $this->session->userdata('web_user');	
		if (empty($sessionUser)) {
			$this->session->set_flashdata('error', 'Bạn chưa đăng nhập!');					
		}

		$data = $headers = [];
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Lời giải', base_url().'loi-giai');
		$questionId = $this->input->post('question_id');
		$courseId = $this->input->post('course_id');
		$course = $this->courses_model->getCourseById(['courses.id' => (int) $courseId]);
		
		if (empty($course)) {
			$this->session->set_flashdata('error', 'Khóa học không tồn tại');			
		} else {
			$where = [
				'user_id' => (int) $sessionUser->id,
				'course_id' => (int) $course->id,
			];	
			if (!$this->courses_model->existUserCourse($where)) {
				$this->session->set_flashdata('error', 'Bạn chưa mua khóa học: ' . $course->name);
			} else {
				$flag = true;
			}
		}

		if (($questionId != '' || $questionId != 0)) {
			if ($flag == true) {
				$item = $this->exam_model->getAnswerById(['id' => $questionId]);
				if(empty($item)){
					$this->session->set_flashdata('error', 'Mã câu hỏi không tồn tại trên hệ thống');
				}
				$data['item'] = $item;
			}
		} else {
			$this->session->set_flashdata('error', 'Truy vấn không hợp lệ!');
		}
		$headers['breadcrumb'] = $this->breadcrumbs->display(); 
		$this->load->view('layouts/header', $headers);
		$this->load->view('answer_by_id', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}

	public function getLoiGiai($string = '')
	{
		$string = $this->input->get('id');
		$file = str_replace('-VNA', '', base64_decode($string));		
		$ext = @end(@explode('.', $file));
		if ($ext == 'pdf') {
			$code = '<iframe src="http://docs.google.com/gview?url='. $file .'&embedded=true" style="width:100%; height:300px;" frameborder="0"></iframe>';
		} else {
			$code = '<img src="'.$file.'">';
		}
		echo $code; exit;
	}
}