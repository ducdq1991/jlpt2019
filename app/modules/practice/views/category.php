<div id="practice-module" class="module">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-lg-8">
				<h1 class="page-title">Thi Online | <?php echo $course->name;?></h1>
				<?php if( $this->session->flashdata('success') ) : ?>
	                <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('error') ) : ?>
	                <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('warning') ) : ?>
	                <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
	            <?php endif; ?>
				<div class="box-filter col-md-12">
					<form role="form">
						<div class="col-md-4">
							<select name="time" class="form-control" id="time">
								<option value="">--Thời gian (phút)--</option>
								<option value="15" <?php if (isset($time) && $time == 15) echo "selected='selected'"; ?>>15</option>
								<option value="30" <?php if (isset($time) && $time == 30) echo "selected='selected'"; ?>>30</option>
								<option value="45" <?php if (isset($time) && $time == 45) echo "selected='selected'"; ?>>45</option>
								<option value="60" <?php if (isset($time) && $time == 60) echo "selected='selected'"; ?>>60</option>
								<option value="90" <?php if (isset($time) && $time == 90) echo "selected='selected'"; ?>>90</option>
								<option value="120" <?php if (isset($time) && $time == 120) echo "selected='selected'"; ?>>120</option>
								<option value="150" <?php if (isset($time) && $time == 150) echo "selected='selected'"; ?>>> = 150</option>
							</select>
						</div>
						<div class="col-md-3"><button type="submit" class="btn btn-success">LỌC BÀI LUYỆN</button></div>
					</form>
				</div>            
				<div class="box-content">
					<?php if(empty($exam)) : echo "Không có kết quả nào được tìm thấy"; ?>
					<?php else: ?>
						<?php foreach ($exam as $row) : ?>
						<div class="box-item">
							<i class="bullet unlock pull-left fa fa-unlock-alt" aria-hidden="true"></i>
							<div class="content">
								<h1><a class="logged" target="_blank" href="<?php echo base_url().'luyen-tap/'.$row->slug.'.html';?>"><?php echo $row->title;?></a></h1>
								<p><a href="<?php echo base_url().'khoa-hoc/'.$row->course_slug;?>"><?php echo $row->course.'</a>'; if($row->teacher) echo ' - '. $row->teacher; ?></p>
								<span>Mã đề: <?php echo $row->id; ?></span>
								<?php
									$questions = @unserialize($row->extend_question_config);
									
									$total = array_sum($questions);									
								?>
								<span>Số câu hỏi: <?php echo $total; ?></span>
								<span>Thời gian: <?php echo $row->time; ?> phút</span>
							</div>
							<a class="logged btn btn-info btn-start pull-right" target="_blank" href="<?php echo base_url().'luyen-tap/'.$row->slug.'.html';?>">LUYỆN TẬP</a>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="pagination">
					<?php echo $paging;?>
				</div><!--pagination-->
			</div>
		  	<div id="competition" class="col-md-4 col-sm-4 col-xs-12">
		  		<?php $this->load->view('block/ads_right_top'); ?>
				<div class="box-competition featured_widget_exam course-practice">
					<ul class="nav nav-tabs">
					  <li class="active" style="float: inherit;"><a data-toggle="tab"><i class="fa fa-newspaper-o"></i> THÔNG TIN KHÓA HỌC</a></li>
					</ul>
					<div class="tab-content">
						<div class="description-course" style="overflow: hidden;">
							<div class="head-content">									
								<div class="img col-lg-12 col-md-12">
									<?php if (isset($parentCourse)) :?>
										<img src="<?php echo base_url().$parentCourse->image;?>" alt="<?php echo $parentCourse->name; ?>">	
									<?php else :?>
										<img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
									<?php endif;?>
								</div>					
								<div class="info-course col-lg-12 col-md-12">					
									<div class="info-detail">
										<h3 style="font-size: 14px;font-weight: 700"><a href="/khoa-hoc/<?php echo $parentCourse->slug; ?>"><?php echo $parentCourse->name; ?></a></h3>
										<p class="teacher">
											<label>Giáo viên: </label>
											<span class="name-teacher">
												<?php echo $parentCourse->teacher; ?>
											</span>
										</p>
										<p class="date-start">
											<label>Khai giảng: </label>
											<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
										</p>
										<?php if($parentCourse->end_date !=0) : ?>
										<p class="date-end">
											<label>Kết thúc: </label>
											<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
										</p>
										<?php endif; ?>

			                            <!--Kiem tra khoa hoc co phi hay Mien phi-->
			                            <?php if ($parentCourse->price == 0) : ?>
			                                <h3 class="text-center" style="font-weight:700;color:#CF5138;font-size: 20px;">Miễn phí</h3>
			                            <?php else:?>
			                                <h3 class="text-center" style="font-weight:700;color:#CF5138;font-size: 20px;">
			                                Học phí: <?php echo number_format($parentCourse->price, 0). ' đ'; ?></h3>
			                            <?php endif;?>
			                            <!--END Kiem tra khoa hoc co phi hay Mien phi-->

			                            <?php if ($parentCourse->title != NULL) :?>
			                            <h3 class="sub-title text-center" style="font-style: italic;"><?php echo $parentCourse->title; ?></h3>
			                            <?php endif;?>
			                            <!--Neu chua nop hoc phi hoac chua dang nhap-->	
			                            <div class="btn-courses text-center">
			                            <!--Kiem tra da nop hoc phi-->
			                            <?php if (isset($userSession) && $isBuyed === true && $price > 0) :?>
			                                <a href="#" title="" class="btn btn-success">Đã nộp học phí</a>
			                            <?php endif;?>                               
			                            </div>
									</div>
								</div>
							</div>
						</div><!--content course-->
					</div>
				</div><!--box competition-->
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var _course_id_practice = "<?php echo $course_id; ?>";
</script>