<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-quiv="imagetoolbar" content="no">
	<meta name="description" content="<?php if(!empty($metas)) echo $metas['meta_description']; ?>" >
	<meta name="keywords" content="<?php if(!empty($metas)) echo $metas['meta_keyword']; ?>" >
	<title>
		<?php if(!empty($metas)) {
			echo $metas['meta_title'];
		} else {
			echo $title;
		}		
		?>		
	</title>
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/style.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/custom.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/responsive.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/owl-carousel.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>/assets/css/font-awesome.min.css" />
	<!-- Toastr style -->
    <link href="<?php echo base_url();?>/assets/css/toastr/toastr.min.css" rel="stylesheet">
	<script type="text/javascript">
		var siteUrl = '<?php echo base_url();?>';
	</script>
</head>
<body oncontextmenu="return false" ondragstart="return false" onselectstart="return false">

	<div id="answer-sheet-panel">
		<div class="panel-exam-result text-center">
			<div class="your-result">Đã làm: <span class="aswed">0</span>/<span class="total-asw"><?php echo count($exam->questions);?></span> Câu</div>
		</div>	
		<?php if (!$isExam) :?>
		<div class="box-bottom mcq-exam">												
			<button id="view-result" class="view-result btn btn-primary" style="width: 100%"><i class="fa fa-send"></i> GỬI BÀI</button>				
		</div>
		<?php endif;?>

		<h3><i class="fa fa-edit"></i> PHIẾU TRẢ LỜI</h3>
		<div class="sheet-container">		
	<?php 
	if (isset($exam->questions) && !empty($exam->questions)):
		$sheet = 1;
		foreach ( $exam->questions as $row ) : 
	?>
		<div class="row-asw">
		<div class="ques-label">C<?php echo $sheet;?>: </div>
		<?php 
		$choices = unserialize($row['choices_answer']);
		if ($choices && count($choices) > 0):		
			foreach ($choices as $key => $value) : 
		?>
			<div class="radio-asw" id="ques-<?php echo $row['id'];?>">
				<label>				
				<a onclick="return chooseASW('<?php echo $row['id']; ?>', '<?php echo $key; ?>', '<?php echo $sheet;?>');" class="asw-text <?php echo $row['id'];?>" id="<?php echo $row['id'] . '-' . $key; ?>"><strong><?php echo $key; ?></strong></a></label>
			</div>
		<?php 
			endforeach;
		endif;
		?>		
		</div>
	<?php 
			$sheet++;
			endforeach;
		endif;
	?>
			</div>
	</div>

	<div class="exam-detail-heading">
		<div class="container">
			<div id="logo" class="col-md-3 col-sm-3 col-xs-12">
				<a href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>/assets/img/logo-mobile.png" alt="logo-qstudy" class="img-responsive" style="height: 50px;">
				</a>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
				<nav class="navbar navbar-default col-md-11 com-sm-11 col-xs-12">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-exam" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href=""></a>
                    </div>
                    <div class="collapse navbar-collapse" id="menu-exam">
			        	<?php echo $this->multi_menu->render(); ?>
                    </div>
                </nav>
			</div>
		</div>
		<div class="count-down" data-time="<?php echo $exam->time; ?>" style="display:none">
			<img src="<?php echo base_url();?>/assets/img/square.png" alt="" class="">
			<span>00:00:00</span>
		</div>
	</div>
	<div class="exam-detail-content">
		<div class="container">
			<div id="practice-heading" class="col-md-12 col-sm-12">
				<h1><?php echo $exam->title;?></h1>
				<p class="course">Khóa học/Chuyên đề: <span><?php echo $exam->course;?></span></p>
				<?php if($exam->teacher) : ?>
					<p>Giáo viên: <span><?php echo $exam->teacher;?></span></p>
				<?php endif; ?>
				<?php if($exam->time) : ?>
					<p class="time">Thời gian làm bài: <span><?php echo $exam->time; ?> phút</span></p>
				<?php else: ?>
					<p class="time">Thời gian làm bài: <span>không tính thời gian</span></p>
				<?php endif; ?>
				<?php if($exam->description) :?>
					<p class="desc"><?php echo $exam->description; ?></p>
				<?php endif; ?>
			</div>
			<div class="box-content col-md-12 col-sm-12">				
				<div class="box-result" style="display: none;">
					<div class="col-md-6 col-sm-12"><span class="mes-result"></span></div>
					<div class="col-md-6 col-sm-12">
						<a class="btn btn-sm btn-default" id='reset-page' >Làm lại</a>
						<a class="btn btn-sm btn-default" href="<?php echo base_url('luyen-tap/chuyen-de/' . url_title($exam->course, 'dash', true)); ?>" >Quay lại</a>
					</div>
				</div>
				<form id='practice-form-content'>
				<?php $order = 1; ?>
				<?php foreach ( $exam->questions as $row ) : ?>
					<div class="box-item" id="<?php echo $row['id'];?>" data-id="<?php echo $row['id'];?>">
						<div class="question">
							<strong>Câu <?php echo $order; ?>: </strong><?php echo $row['question']; ?>
						</div>
							<?php 
							$choices = unserialize($row['choices_answer']);									
		
							if ($choices && count($choices) > 0):		
								foreach ($choices as $key => $value) : 
							?>
								<div class="radio" id="<?php echo $row['id'];?>-<?php echo $key; ?>">
									<label><input type="radio" id="<?php echo $row['id'];?>-<?php echo $key; ?>" class="answer-item <?php echo $row['id'];?>-<?php echo $key; ?>" value="<?php echo $key; ?>" data-key="<?php echo $row['id'];?>" name="answer-<?php echo $order; ?>">
									<strong><?php echo $key; ?></strong>
									<?php echo $value; ?></label>
								</div>
							<?php 
								endforeach; 
							endif;
							?>

						<div class="box-meta">
							<?php
	                            $youtubeExp = explode('v=', $row['link_video_answer']);
	                            $videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';
	                        ?>
	                        <?php if($videoId) : ?>
								<a class="previewVideo protected" data-toggle="collapse" data-id="<?php echo $row['id'];?>" data-url="<?php echo base_url().'player/index/'.$videoId;?>"><i class="fa fa-play" aria-hidden="true"></i> Video lời giải</a>
							<?php endif; ?>
							<?php if($row['link_media_answer']) : ?>
								<a class="previewPdf protected" href="" data-toggle="collapse" data-id="<?php echo $row['id'];?>" data-url="<?php echo base_url().$row['link_media_answer']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Lời giải</a>
							<?php endif; ?>
							<span class="pull-left">Mã ID: <?php echo $row['id'];?></span>
							<?php if($row['link_practice']) : ?>
								<a class="link" href="<?php echo $row['link_practice'];?>" target="_blank"><i class="fa fa-book" aria-hidden="true"></i> Lý thuyết</a>
							<?php endif; ?>
							<i class="pull-left fa fa-check save-success" style="display: none; color: #53b425;" aria-hidden="true"></i>
							<i class="pull-left fa fa-times save-failed" style="display: none; color: #be0316;" aria-hidden="true"></i>
							
							<span class="result left"></span>
							<span class="result wrong"></span>
							
						</div>
						<div class="save-message"></div>
						<div id="videoCollapse-<?php echo $row['id'];?>" class="collapse video-player-collapse">
						    video lời giải
						</div>
						<div id="fileCollapse-<?php echo $row['id'];?>" class="collapse">
						    lời giải
						</div>
					</div>
				<?php $order++; endforeach ?>
				</form>
			</div>
			<div class="box-bottom col-lg-offset-1 col-md-offset-1 col-lg-8 col-md-8 mobile-show-panel">				
				<button id="view-result" class="view-result btn btn-primary"><i class="fa fa-send"></i> GỬI BÀI</button>				
			</div>

		</div>		
	</div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Hệ thống</h4>
                </div>
                <div class="modal-body">
                    <p>Hệ thống chỉ chấp nhận kết quả lần gửi bài đầu tiên.<br/>
                    Bạn chắc chắn gửi bài nhấn "Đồng ý"!
               		</p>
               		<button type="button" class="btn btn-sm btn-white pull-right" data-dismiss="modal">Hủy bỏ</button>
               		<button type="button" id="submit-modal" class="btn btn-sm btn-info pull-right">Đồng ý</button>
                </div>
            </form>
        </div>
    </div>
</div>
	<script>
		var _url_result = "<?php echo base_url('practice/result');?>";
		var _exam_id = "<?php echo $exam->id; ?>";
	</script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.easy-ticker.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>	
	<!-- practice detail load -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/practice-detail.js"></script>

	<!-- Toastr script -->
    <script src="<?php echo base_url();?>assets/js/toastr/toastr.min.js"></script>
</body>
</html>