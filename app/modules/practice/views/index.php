<div id="practice-module" class="module">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-lg-9">
				<h1 class="page-title">Luyện tập</h1>
				<?php if( $this->session->flashdata('success') ) : ?>
	                <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('error') ) : ?>
	                <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('warning') ) : ?>
	                <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
	            <?php endif; ?>
				<div class="box-content">
					<?php if(empty($coursesData)) : echo "Không có kết quả nào được tìm thấy"; ?>
					<?php else: ?>
						<?php $i = 0; foreach ($coursesData as $row) : ?>

			  			<?php if(($i % 3) == 0) :?>
			  			<div class="wrap-item-menu" id="bs-c-item-<?php echo $i;?>">
			  			<?php endif; $i++; ?>  													   
				            <div class="menu-full-item col-md-4">       
				                <h3 class="menu-item-title">
				                    <span><a href="/luyen-tap/chuyen-de/<?php echo $row->slug;?>" title="<?php echo $row->name;?>"><?php echo $row->name;?></a></span>                    
				                </h3>
				            </div>
						<?php if(($i % 3) == 0 || $i == count($coursesData)) :?>
						</div>
						<?php endif;?>	
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<div class="pagination">
					<?php echo $paging;?>
				</div><!--pagination-->
			</div>
			<div class="col-md-3 col-lg-3">
				<!--ads right top -->				
				<div class="box-filter" style="display: none">
					<form role="form">
						<div class="form-group">
							<label for="time">Thời gian (phút):</label>
							<select name="time" class="form-control" id="time">
								<option value="">--Chọn--</option>
								<option value="15" <?php if (isset($time) && $time == 15) echo "selected='selected'"; ?>>15</option>
								<option value="30" <?php if (isset($time) && $time == 30) echo "selected='selected'"; ?>>30</option>
								<option value="45" <?php if (isset($time) && $time == 45) echo "selected='selected'"; ?>>45</option>
								<option value="60" <?php if (isset($time) && $time == 60) echo "selected='selected'"; ?>>60</option>
								<option value="90" <?php if (isset($time) && $time == 90) echo "selected='selected'"; ?>>90</option>
								<option value="120" <?php if (isset($time) && $time == 120) echo "selected='selected'"; ?>>120</option>
								<option value="150" <?php if (isset($time) && $time == 150) echo "selected='selected'"; ?>>> = 150</option>
							</select>
						</div>
						<div class="form-group">
							<label for="course_id">Khóa học/Chuyên đề:</label>
							<select name="course_id" class="form-control" id="course_id">
								<option value="">--Chọn khóa học--</option>
								<?php //echo $courseOption; ?>
							</select>
						</div>
						<button type="submit" class="btn btn-success">Lọc</button>
					</form>
				</div>
				<!--ads banner right bottom-->
				<?php //$this->load->view('block/ads_right_bottom'); ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var _course_id_practice = "<?php echo $course_id; ?>";
</script>


<script type="text/javascript">
	$(document).ready(function() {
	  $('.lnk-menu-more').click(function() {
	      if ($(this).hasClass('current')) {
	          $(this).parent().parent().find('.menu-full-item').height('180');
	          $(this).removeClass('current');
	      }
	      else {
	          $(this).parent().parent().find('.menu-full-item').height('auto');
	          $(this).addClass('current');
	          $(this).empty().append('<i class="fa fa-angle-double-up"></i>');
	      }
	  })
	});
</script>
