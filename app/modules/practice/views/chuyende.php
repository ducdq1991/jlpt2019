<div id="courses-module">
  <div class="container">
  	<div class="row">
	  	<div  id="detail-courses" class="list-item col-md-9 col-sm-9 col-xs-12">
			<h1 class="title"><i class="fa fa-file-text-o"></i> CHỌN CHUYÊN ĐỀ CẦN LUYỆN</h1>
			<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('warning') ) : ?>
	            <div class="alert alert-sm alert-warning alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('warning'); ?>
	            </div>
	        <?php endif; ?>
			<div class="topical-lesson">
				<div class="list-topical-lesson">
					<?php if( !empty($subCourses) ) :?>
						<?php 
							$stt = 1; 
							foreach( $subCourses as $item ):
						?>
							<div class="item">
								<h3>								
									<a href="<?php echo base_url();?>luyen-tap/chuyen-de/<?php echo $item->slug; ?>" title="">
										<span class="stt"><?php echo $stt; ?>. </span>
										<?php echo $item->name;?>
									</a>
								</h3>
							</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>		
						<div class="item">
							<h3>Chuyên đề / Bài giảng chưa được cập nhật</h3>
						</div>
					<?php endif;?>	
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->
	  	</div>
	  	<div id="competition" class="col-md-3 col-sm-3 col-xs-12">
	  		<?php //$this->load->view('block/ads_right_top'); ?>
	  		<?php $this->load->view('block/featured'); ?>
	  		<?php //$this->load->view('block/ads_right_bottom'); ?>
	  	</div>	
  	</div>
  </div><!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nộp học phí</h4>
			</div>
			<div class="modal-body">
				<p class="price-text"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info purchase" data-dismiss="modal">Đồng ý</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->  	