﻿<div id="document-detail" class="module anserById">
	<div class="container">
		<h1 class="page-title">Đáp án</h1>

		<div class="col-md-12 col-sm-12 col-xs-12">


			<?php if( !empty($item) ) : ?>
				<div class="detail-top">
					<div class="box-heading">
						<h1>Lời giải: <?php echo $item->name; ?></h1>
					</div>
					<div class="box-content">
					    <div class="item text-center">
					     <?php 
					     	$extention = @end(@explode('.', $item->link_media_answer));
					     	if ($extention == 'pdf'):
					     ?>
					    	<iframe src="http://docs.google.com/gview?url=<?php echo base_url().$item->link_media_answer;?>&embedded=true" style="width:100%; height:100%;" frameborder="0"></iframe>
					    	<?php else: ?>
					    		<img src="<?php echo base_url() . $item->link_media_answer;?>" alt="" width="100%">
					    	<?php endif;?>
					    </div>


						<div class="video-box-aws">
							<?php							
							$video = $videoId = '';						
							if (strpos($item->link_video_answer, 'youtube.com')) :
								$exp = explode('/', $item->link_video_answer);
								if(isset($exp[2]) && $exp[2] == 'www.youtube.com') :
									$youtubeExp = explode('v=', $item->link_video_answer);
									$videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';

							?>
								<div id="productWrapper" oncontextmenu="return false">
									<div id="myDivMainHolder2">
										<div id="myDiv2" style="width:100%"></div>
									</div>
									<div id="bigshare_overlay_logo">
										<img src="<?php echo base_url();?>/assets/content/minimal_skin_dark/logo.png" alt="">
									</div>
								</div>						
							<?php 
								endif;						
							?>							
							<?php 
							elseif (strpos($item->link_video_answer, 'drive.google.com')):

							?>
							<iframe width="100%" height="360" src="<?php echo $item->link_video_answer;?>?autoplay=1" frameborder="0" allowfullscreen="0"></iframe>
							<?php else : $video = base_url().$item->link_video_answer; ?>
								<embed type="application/x-shockwave-flash" src="<?php base_url();?>/mediaplayer.swf" width="100%" height="360" style="undefined" id="MediaPlayer" name="MediaPlayer" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" flashvars="file=<?php echo $video;?>&amp;image=<?php echo base_url();?>assets/img/thumbnail-video.jpg&amp;showicons=true&amp;shownavigation=true"/>
							<?php endif;?>
						</div>

										    
					</div>
				</div>
			<?php else: ?>
                <div id="answer-question">
                	<?php if( $this->session->flashdata('success') ) : ?>
	                    <div class="alert alert-sm alert-success alert-dismissable">
	                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                        <?php echo $this->session->flashdata('success'); ?>
	                    </div>
	                <?php endif; ?>
	                <?php if( $this->session->flashdata('error') ) : ?>
	                    <div class="alert alert-sm alert-danger alert-dismissable">
	                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                        <?php echo $this->session->flashdata('error'); ?>
	                    </div>
	                <?php endif; ?>
				</div><!--question & answer-->
			<?php endif; ?>	
		</div>
	</div>
</div><!-- end #document-detail -->

<script type="text/javascript" src="<?php echo base_url();?>assets/java/pageMinimalDark.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/java/FWDEVPlayer.js"></script>
<script type="text/javascript">
	FWDEVPUtils.onReady(function(){
		var videoId = '<?php echo $videoId;?>';
		init(videoId);
	});
</script>