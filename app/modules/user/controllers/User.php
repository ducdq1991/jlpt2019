<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once dirname(__DIR__) . '/libraries/Facebook/autoload.php';

class User extends Base_Controller {

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->model('user_model'); 
        $this->load->library('breadcrumbs');
        $this->load->model('monitoring_model');
        
    }

    public function index()
    {
        $this->load->view('layouts/header');
        $this->load->view('layouts/footer');
    }
    /**
    * [register] [Method Register member on system]
    **/
    public function register()
    {    
        if ($this->session->has_userdata('web_user')) {
            redirect(base_url());
        }

        $data = [];

        $this->load->library('form_validation');
        $this->load->helper('captcha');
        $data = [];
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Đăng ký';

        $config = array(
            array(
                'field'  => 'full_name',
                'label'  => 'Họ và tên',
                'rules'  => 'trim|required|min_length[5]|max_length[40]',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                    'min_length' => '%s quá ngắn, tối thiểu 5 ký tự.',
                    'max_length' => '%s quá dài, tối đa 40 ký tự.'
                )
            ),
            array(
                'field'  => 'username',
                'label'  => 'Tên đăng nhập',
                'rules'  => 'trim|required|min_length[6]|max_length[16]|is_unique[users.username]|callback_validateUsername',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                    'min_length' => '%s quá ngắn, tối thiểu 6 ký tự.',
                    'max_length' => '%s quá dài, tối đa 16 ký tự.',
                    'is_unique'  => '%s đã tồn tại.'
                )
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'trim|required|min_length[8]',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.',
                    'min_length' => '%s quá ngắn, tối thiểu 8 ký tự.'
                )
            ),
            array(
                'field' => 're_password',
                'label' => 'Xác nhận mật khẩu',
                'rules' => 'trim|required|matches[password]',
                'errors' => array(
                    'required' => 'Bạn phải %s.',
                    'matches' => '%s không đúng'
                )
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|is_unique[users.email]',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.',
                    'valid_email' => 'Định dạnh $s không đúng.',
                    'is_unique' => '%s này đã được một tài khoản dùng để đăng ký.'
                )
            ),
            array(
                'field' => 'phone',
                'label' => 'Số điện thoại',
                'rules' => 'required|numeric|min_length[10]|max_length[11]|is_unique[users.phone]',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.',
                    'numeric' => '%s không đúng',
                    'min_length' => '%s không đúng',
                    'max_length' => '%s không đúng',
                    'is_unique' => '%s này đã được một tài khoản dùng để đăng ký.'
                )
            ),
            /*array(
                'field' => 'captcha',
                'label' => 'Mã xác nhận',
                'rules' => 'trim|required|callback_validCaptcha',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.'
                )
            )*/
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            //captcha
            $configCaptcha = array(
                'word' => '',
                'img_path' => 'upload/captcha/',
                'img_url' => base_url().'upload/captcha/',
                'img_width' => 150,
                'img_height' => 40,
                'expiration' => 120,
                'word_length'  => 3,
                'font_size'     => 40,
                'colors'        => array(
                    'background' => 'green',
                    'border' => array(100, 100, 100),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
            );
            $captcha = create_captcha($configCaptcha); 
            $data['codeCaptcha'] = $captcha['image'];   
            $this->session->set_userdata('captchaWord', $captcha['word']);
            //render Layouts
            $this->load->view('layouts/header', $head);
            $this->load->view('user/register',$data);
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $info = $this->input->post();
            $info['password'] = md5($info['password']);
            if( $info['provision'] = 1 ){
                $info['status'] = (int) ($info['provision']);
            } 
            $info['group_id'] = (int) GID;
            $name = $info['full_name'];
            unset($info['captcha'], $info['re_password'], $info['provision']);
            $userId = $this->user_model->addUser( $info );
            if ($userId) {
                $user = $this->user_model->getUser(['users.id' => $userId]);
                $this->session->set_userdata('web_user', $user);
                $this->session->set_flashdata('success', 'Bạn đã đăng ký thành công!');
                $this->generateLog( REGISTER, $userId, $data = 0, '<a class="name-user" href="'.base_url().'thanh-vien/'.$info["username"].'">'.$name.'</a> Vừa đăng ký tài khoản');
                redirect(base_url().'thanh-vien');
            } else {
                $this->session->set_flashdata('error', 'Đăng ký thất bại, bạn vui lòng kiểm tra lại');
            }
        }
    }
    /**
    * [validCaptcha] [Validation captcha]
    **/
    public function validCaptcha( $str = '' )
    {       
        $captchaWord = $this->session->get_userdata('captchaWord' );
        if( !isset($captchaWord['captchaWord']) ){
            return false;
        }
        $currentCaptcha = $captchaWord['captchaWord'];
        if( $str === $currentCaptcha ) {
            $this->session->unset_userdata('captchaWord');  
            return true;
        }
        $this->session->unset_userdata('captchaWord');
        $this->form_validation->set_message('validCaptcha', '{field} nhập không đúng');
        return false;
    }
    /**
    * [validateUsername] [valid username]
    **/
    public function validateUsername( $str = '' )
    {
        if( $str == '' ){ 
            return false;
        }
        $partten = "/^[A-Za-z0-9_\.]+$/";
        if(!preg_match($partten , $str)) {
            $this->form_validation->set_message('validateUsername', '{field} chỉ cho phép chữ cái, chữ số, dấu "_", và dấu "."');
            return false;
        }
        return true;
    }

    /**
    * [login] [Check User Login]
    **/
    public function login()
    {
        
        $response = [];
        $response['status'] = 100;
        $response['message'] = '';
        $userIP = $_SERVER['REMOTE_ADDR'];
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $rememberPassword = $this->input->post('remember_password');
        $where = array(
            'users.username' => $username,
            'users.status' => (int) 1,
            'user_groups.access_key' => 'member'
            );

        $user = $this->user_model->getUser($where);        
        //Create cookie
        if ($rememberPassword == 1) {
            $configCookie = array(
                        'name' => 'cookieLogin',
                        'value' => json_encode($user),
                        'expire' => 24 * 3600 * 30,
                        'path'   => $this->config->item('cookie_path'),
                        'prefix' => $this->config->item('cookie_prefix'),
            );               
            set_cookie($configCookie);
        }
        // validation form null
        if($username === "" && $password === "" ){
            $response['message'] = 'Bạn vui lòng điền vào tên đăng nhập và mật khẩu!';
            echo json_encode($response);
            exit; 
        }
        if($username === "" ){
            $response['message'] = 'Bạn vui lòng điền vào tên đăng nhập!';
            echo json_encode($response);
            exit; 
        }
        if($password === "" ){
            $response['message'] = 'Bạn vui lòng điền vào mật khẩu!';
            echo json_encode($response);
            exit; 
        }
        // validation form input
        if ( $this->input->post() ) {
            $standardPassword = md5($password);
            if ($user) {
                if( $user->password === $standardPassword ){
                    $user->ip = $_SERVER['REMOTE_ADDR'];
                    $user->login_time = time();

                    if ($this->checkUniqueUserLogin($user) == false) {
                        $response['message'] = 'Tài khoản đang được đăng nhập bởi 1 người khác!';
                        echo json_encode($response);
                        exit;                        
                    }     

                    $this->session->set_userdata('web_user', $user);
                    $this->generateLog( LOG_IN, $user->id, $data = 0, 'Đăng nhập hệ thống');
                    $response['status'] = 200;
                    $response['url'] = $this->session->has_userdata('redirector') ? $this->session->userdata('redirector') : base_url();
                    echo json_encode($response);
                    exit;
                } else {
                    $response['message'] = 'Sai mật khẩu!';
                    echo json_encode($response);
                    exit;                                         
                }
            }             
        }
        $response['message'] = 'Tài khoản không tồn tại!';
        echo json_encode($response);
        exit;
    }
    /**
    * [logout] [User Logout]
    **/
    public function logout()
    {
        if ($this->session->has_userdata('web_user')) {
            $user = $this->session->userdata('web_user');
            $this->generateLog( LOG_OUT, $user->id, $data = 0, 'Thoát khỏi hệ thống');
            $this->session->unset_userdata('web_user');
            $configCookie = array(
                'name' => 'cookieLogin',
                'path'   => $this->config->item('cookie_path'),
                'prefix' => $this->config->item('cookie_prefix'),
            ); 
            delete_cookie($configCookie);
        }
        if($this->session->has_userdata('redirector'))
            redirect($this->session->userdata('redirector'));
        redirect(base_url());
    }

    protected function checkUniqueUserLogin($user = null)
    {
        $sessions = $this->monitoring_model->getData();
        $userTime = $user->login_time;     
        if (!is_null($user)) {
            if (isset($sessions[$user->username])) {
               $timeDiff = floor(abs($userTime - $sessions[$user->username]['time'])/60); 
               if ($user->ip != $sessions[$user->username]['ip']) {
                    if ($timeDiff < 180) {
                        return false;
                    }
               }
            }
        }   

        return true;      
    }

    public function usernameUnique($username = '')
    {
        if( $username == '' ){
            return false;
        }
        if($this->user_model->getUser(array('username' => $username))){
            $username = $username .rand(1,10) . date('s');
            $this->usernameUnique( $username );
        }
        return $username;
    }

    public function profile()
    {
        $this->load->library('form_validation');
        $session = $this->session->userdata('web_user');
        $id = $session->id;
        if (!$id) {
            redirect(base_url());
            $this->session->set_flashdata('error', 'Bạn vui lòng đăng nhập để xem thông tin');
        }
        $data = [];
        $user = $this->user_model->getUserById($id);
        $data['user'] = $user;
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $this->form_validation->set_rules('full_name','Họ và tên','trim|required|min_length[5]|max_length[40]');
        if ($email == ($user->email)) {
            $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        } else {
            $this->form_validation->set_rules('email','Email','trim|required|is_unique[users.email]|valid_email');
        }
        if ($phone == ($user->phone)) {
            $this->form_validation->set_rules('phone','Điện thoại','required|numeric|min_length[9]|max_length[11]');
        } else {
            $this->form_validation->set_rules('phone','Điện thoại','required|numeric|min_length[10]|max_length[11]|is_unique[users.phone]');
        }

        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Tin tức', base_url().'tin-tuc');        
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Cập nhật thông tin';
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header', $head);
            $this->load->view('user/update', $data);
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);
            $args = [];
            $args['group_id'] = (int)25;
            $args['status'] = (int)1;
            $args['email'] = $postData['email'];
            $args['phone'] = $postData['phone'];
            $args['full_name'] = $postData['full_name'];
            $query = $this->user_model->updateUser($id, $args);

            if( $query ) {
                $this->session->set_flashdata('success', 'Cập nhật thành công!');
            } else {
                $this->session->set_flashdata('error', 'Cập nhật thất bại!');
            }

            redirect(base_url() . 'thanh-vien');   
        }
    }

    public function achievement()
    {
        $this->load->library('form_validation');
        $session = $this->session->userdata('web_user');
        $id = $session->id;
        if (!$id) {
            redirect(base_url());
            $this->session->set_flashdata('error', 'Bạn vui lòng đăng nhập để xem thông tin');
        }
 
        $data = $head = [];
        $user = $this->user_model->getUserById($id);
        $data['user'] = $user;

        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Thành tích';

        $this->load->view('layouts/header', $head);
        $this->load->view('user/achievement', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }

    public function info()
    {
        $this->load->library('form_validation');
        $session = $this->session->userdata('web_user');
        $id = $session->id;
        if (!$id) {
            redirect(base_url());
            $this->session->set_flashdata('error', 'Bạn vui lòng đăng nhập để xem thông tin');
        }
 
        $data = $head = [];
        $user = $this->user_model->getUserById($id);
        $data['user'] = $user;

        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Thông tin thành viên';

        // validate info user
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $this->form_validation->set_rules('full_name','Họ và tên','trim|required|min_length[5]|max_length[40]');
        if ($email == ($user->email)) {
            $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        } else {
            $this->form_validation->set_rules('email','Email','trim|required|is_unique[users.email]|valid_email');
        }
        if ( $phone == ($user->phone) ) {
            $this->form_validation->set_rules('phone','Điện thoại','required|numeric|min_length[9]|max_length[11]');
        } else {
            $this->form_validation->set_rules('phone','Điện thoại','required|numeric|min_length[10]|max_length[11]|is_unique[users.phone]');
        }
        
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header', $head);
            $this->load->view('user/edit', $data);
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);
            $args = [];
            // upload image
            $photoPath = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
            $configImage = array(
                'upload_path'   => $photoPath.'/upload',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'max_size'      => 3000,
                'max_width'     => 1000,
                'max_height'    => 1000
            ); 

            $photo = null;
            if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $this->load->library('upload', $configImage);
                if ($this->upload->do_upload('image')) {
                    $imgArray = $this->upload->data();
                    $photo = isset($imgArray) ? 'upload/' . $imgArray['file_name'] : '';
                    $args['image'] = $photo;
                    @unlink($photoPath . '/' .$postData->image);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect( current_url(), 'refresh');
                }
            }

            $args['email'] = $postData['email'];
            $args['phone'] = $postData['phone'];
            $args['full_name'] = $postData['full_name'];
            $query = $this->user_model->updateUser($id, $args);

            if ($query) {
                $this->session->set_flashdata('success', 'Cập nhật thông tin thành công!');
            } else {
                $this->session->set_flashdata('error', 'Cập nhật thông tin thất bại!');
            }

            redirect(base_url() . 'thanh-vien');   
        }
    }

    public function editPassword()
    {
        $this->load->library('form_validation');
        $user = $this->session->userdata('web_user');
        $head = [];
        $id = $user->id;
        if (!$id ) {
            redirect(base_url());
            $this->session->set_flashdata('error', 'Bạn chưa đăng nhập');
        }

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Thay đổi mật khẩu';

        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Thay đổi mật khẩu', base_url().'thanh-vien/doi-mat-khau');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $config = array(
            array(
                'field' => 'password',
                'label' => 'Mật khẩu cũ',
                'rules' => 'trim|required|callback_validCurPass',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.'
                )
            ),
            array(
                'field' => 'new_password',
                'label' => 'Mật khẩu mới',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.'
                )
            ),
            array(
                'field' => 're_new_password',
                'label' => 'Nhập lại Mật khẩu mới',
                'rules' => 'trim|required|matches[new_password]',
                'errors' => array(
                    'required' => 'Bạn phải %s.',
                    'matches' => '%s không đúng'
                )    
            ),
        ); 
        $this->form_validation->set_rules($config); 
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header', $head);
            $this->load->view('user/password');
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $args = [];
            $infoPassword = $this->input->post('new_password');
            $infoPassword = md5($infoPassword);
            $args['password'] =  $infoPassword;
            $query = $this->user_model->updateUser($id, $args);

            if ($query) {
                $this->session->set_flashdata('success', 'Cập nhật mật khẩu thành công');
            } else {
                $this->session->set_flashdata('error', 'Cập nhật mật khẩu thất bại!');
            }
            redirect(base_url() . 'thanh-vien'); 
        }
    }

    public function validCurPass($curPass = '')
    {
        $user = $this->session->userdata('web_user');
        if (is_null($user) || empty($curPass)){
            return false;
        }
        $id = $user->id;
        $curPass = md5($curPass);
        if ($curPass == $user->password) {
            return true;
        }

        $this->form_validation->set_message( 'validCurPass', '{field} không đúng');

        return false;
    }

    public function forgotPassword()
    {
        $this->load->library('form_validation');
        $head = [];
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Hoạt động vừa diễn ra';        
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Quên mật khẩu', base_url().'quen-mat-khau');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_validforgotPass',
                'errors' => array(
                    'required' => 'Bạn chưa điền %s.',
                    'valid_email' => 'Định dạnh %s không đúng.'
                )
            ),
        );    
        $this->form_validation->set_rules($config);
        if ( $this->form_validation->run() == FALSE ) {
            $this->load->view('layouts/header', $head);
            $this->load->view('user/forgot_password');
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $email = $this->input->post('email');
            $where = array('email' => $email);
            $user = $this->user_model->getUser($where);
            if( $this->resetPassword($user) == null ) {
                $this->session->set_flashdata('success', 'Mật khẩu mới của bạn đã được tạo và gửi đến email: '.$user->email.' ');
            } else {
                $this->session->set_flashdata('error', 'Yêu cầu tạo mật khẩu mới thất bại');
            }
            redirect(base_url().'quen-mat-khau');
        }
    }

    private function resetPassword($user)
    {
        $args = [];
        date_default_timezone_set('GMT');
        $this->load->helper('string');
        $this->load->helper('file');
        $password = random_string('alnum', 16);
        $args['password'] = md5($password);
        $this->user_model->updateUser($user->id, $args);
        $where = [];
        $where_in = array(
            'key' => 'option_name',
            'value' => array(
                'protocol', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'charset',  'mailtype',
                ),
            );
        $this->load->model('base_model');
        $mailSetting = $this->base_model->getOptions( $where, $where_in );
        $mailer = [];
        foreach ($mailSetting as $value) {
            $mailer[$value->option_name] = $value->option_value;
        }
        date_default_timezone_set('GMT');
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => $mailer['protocol'],
            'smtp_host' => $mailer['smtp_host'],
            'smtp_user' => $mailer['smtp_user'],
            'smtp_pass' => $mailer['smtp_pass'],
            'smtp_port' => (int) $mailer['smtp_port'],
            'charset' => $mailer['charset'],
            'mailtype'  => $mailer['mailtype'],
        ));
        $this->email->from($this->getEmail(), $this->getName());
        $this->email->to($user->email);     
        $this->email->subject('Thông báo cài đặt lại mật khẩu cho tài khoản tại '.base_url());
        $this->email->set_newline("\r\n");
        $userName = $user->full_name;
        $bodyData = [
            '{FULLNAME}' => '<span style="font-weight:bold">'.$userName.'</span>',
            '{DOMAIN}' => '<span style="font-weight:bold">'.base_url().'</span>',
            '{USERNAME}' => '<span style="font-weight:bold">'.$user->username.'</span>',
            '{PASS}' => '<span style="font-weight:bold">'.$password.'</span>',
        ];
        $path = base_url().'/email/reset.txt';
        $emailTemplate = read_file($path);
        $mes = $this->paramReplace($emailTemplate, $bodyData);
        $this->email->message($mes);
        $this->email->send();
    }

    public function paramReplace($txt, $search = [])
    {
        foreach($search as $k=>$v)
        {
            $txt = str_replace($k,$v,$txt);
        }
        return $txt;
    } 

    public function validforgotPass()
    {
       $email = $this->input->post('email');
       if(!$email){
            return false;
       }
       $where = array('email' => $email);
       if( $this->user_model->getUser($where) ){
            return true;
       }
        $this->form_validation->set_message( 'validforgotPass', '{field} không đúng');
        return false;
    }

    public function lessonSave( $page = 0 )
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        $this->load->helper('paging');
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $data = $head = $where = $orderBy = [];
        $where['user_id'] = $id;
        $where['question_id !='] = Null;
        $limit = 10;
        // count record
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
            $offset = ($page - 1) * $limit;
        $total = $this->user_model->totalQuestionSave( $where );
        $data['paging'] = paging( base_url() .'thanh-vien/cau-hoi-da-luu', $page, $total, $limit );
        // list question saved
        $questionSaves = $this->user_model->getQuestionSave( $where, $limit, $offset );
        foreach( $questionSaves as $key=>$value ){
            $questionSaves[$key]->choices_answer = !empty($value->choices_answer) ? unserialize($value->choices_answer) : [];
        }
        $data['questionSaves'] = $questionSaves;
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Bài giảng đã lưu', base_url().'thanh-vien/bai-giang-da-luu');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Bài giảng đã lưu';        
        //load view
        $this->load->view('layouts/header', $head);
        $this->load->view('user/question_save', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }

    public function lessonView( $page = 0 )
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        $this->load->helper('paging');
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $head = $data = $where = $orderBy = $whereIn =  [];
        $where['user_id'] = $id;
        $where['type'] = VIEW_LESSON;
        $orderBy['key'] = 'user_logs.id';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $total = $this->user_model->totalActivityByUser( $where, $whereIn ); 
        //get all lesson view of user
        $lessonViews = $this->user_model->getActivityByUser( $where, $whereIn, $limit, $offset, $orderBy );
        $data['lessonViews'] = $lessonViews;
        $data['offset'] = $offset+1;
        $data['paging'] = paging( base_url().'thanh-vien/bai-giang-vua-xem', $page, $total, $limit );
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Bài giảng vừa xem', base_url().'thanh-vien/bai-giang-vua-xem');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Bài giảng vừa xem';

        //load view
        $this->load->view('layouts/header', $head);
        $this->load->view('user/lesson_view', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    * [history activity of user]
    **/
    public function activityByUser()
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $head = $data = $where = $orderBy = $whereExc = [];
        $where['user_id'] = $id;
        $whereExc = [LOG_IN, LOG_OUT, PURCHASE_LESSON, PAYMENT, RECHARGE, PURCHASE_COURSE];
        $orderBy['key'] = 'user_logs.updated';
        $orderBy['value'] = 'DESC';
        $offset = 0;
        $limit = 5; 
        if ($this->input->is_ajax_request()) {
           $offset = (int) $this->input->post('offset');
        }  
        $total = $this->user_model->totalActivityByUser( $where, $whereExc ); 
        //get all activity of user
        $activityUser = $this->user_model->getActivityByUser( $where, $whereExc, $limit, $offset, $orderBy );
        $data['activityUser'] = $activityUser;
        if ($this->input->is_ajax_request()) {
            if($activityUser){
                $html = '';
                foreach ($activityUser as $row) :
                    $html .= '<div class="vertical-timeline-block">';
                        if($row->type == UPDATE_INFO) :
                            $html .= '<div class="vertical-timeline-icon yellow-bg"><i class="fa fa-user" aria-hidden="true"></i></div>';
                        elseif($row->type == VIEW_LESSON) :
                            $html .= '<div class="vertical-timeline-icon navy-bg"><i class="fa fa-file-video-o" aria-hidden="true"></i></div>';
                        elseif($row->type == DO_EXAM || $row->type == DO_EXERCISE) :
                            $html .= '<div class="vertical-timeline-icon lazur-bg"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>';
                        elseif($row->type == SEND_ANSWER || $row->type == UPLOAD_ANSWER) :
                            $html .= '<div class="vertical-timeline-icon blue-bg"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>';
                        endif;
                        $html .= '<div class="vertical-timeline-content">';
                           $html .= $row->content;
                            
                            $date_in = new DateTime($row->updated);
                            $date = new DateTime(date('Y-m-d H:i:s'));
                            $interval = date_diff($date_in, $date);
                            $y = $interval->y;
                            $m = $interval->m;
                            $d = $interval->d;
                            $h = $interval->h;
                            $i = $interval->i;
                            $timeago = '';
                            if($y) $timeago = $y.' năm trước<br/> <small>'.date('d/m', strtotime($row->updated)).' '.date('H:i a', strtotime($row->updated)).'</small>';
                            elseif($m) $timeago = $m.' tháng trước<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
                            elseif($d) $timeago = $d.' ngày trước<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
                            elseif($h) $timeago = 'Hôm nay<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
                            elseif($m) $timeago= 'Hôm nay<br/> <small>'.$m.' phút trước</small>';
                            
                            $html .= '<span class="vertical-date">';
                                $html .= $timeago;
                            $html .= '</span>';
                        $html .= '</div>';
                    $html .= '</div>';
                endforeach;
            }
            $response = [
                'offset' => ((int) $offset + (int) $limit) >= $total ? 0 : (int) $offset + (int) $limit,
                'html' => $html,
            ];
            echo json_encode($response);
            die;
        }
        $data['offset'] = $limit;
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('lịch sử hoạt động', base_url().'thanh-vien/lich-su-hoat-dong');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Lịch sử hoạt động';

        //load view
        $this->load->view('layouts/header', $head);
        $this->load->view('user/activity', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    * [get exam result online by user]
    **/
    public function examResultOnlineByUser( $page = 0 )
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        $this->load->helper('paging');
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $head = $data = $where = $orderBy = [];
        $where['user_id'] = $id;
        //$where['user_exam.exam_id !='] = null;
        $orderBy['key'] = 'user_exam.id';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $total = $this->user_model->totalExamResultByUser( $where );
        // exam result online
        $examResult = $this->user_model->getExamResultByUser( $where, $limit, $offset, $orderBy );
        
        $data['examResult'] = $examResult;
        $data['paging'] = paging(base_url().'thanh-vien/ket-qua-cuoc-thi-online', $page, $total, $limit);
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Kết quả thi online', base_url().'thanh-vien/ket-qua-cuoc-thi-online');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //load view
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Kết quả thi Online';        
        $this->load->view('layouts/header', $head);
        $this->load->view('user/exam_results', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    * [history payment]
    **/
    public function paymentHistory( $page = 0 )
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        $this->load->helper('paging');
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $data = $head = $where = $orderBy = $wherePay = [];
        $where['user_id'] = $id;
        $wherePay = [PAYMENT,RECHARGE,PURCHASE_COURSE,PURCHASE_LESSON];
        $orderBy['key'] = 'user_logs.created';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $total = $this->user_model->totalPayment( $where, $wherePay );
        //get history payment
        $payment = $this->user_model->getPaymentByUser( $where, $wherePay, $limit, $offset, $orderBy );
        $data['payment'] = $payment;
        $data['offset'] = (int) $offset + 1;
        $data['paging'] = paging( base_url().'thanh-vien/lich-su-thanh-toan', $page, $total, $limit );
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Lịch sử thanh toán', base_url().'thanh-vien/lich-su-thanh-toan');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Lịch sử thanh toán';

        //load view
        $this->load->view('layouts/header', $head);
        $this->load->view('user/payment_history', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    * [lesson bought]
    **/
    public function courseBought( $page = 0 )
    {
        $user = $this->session->userdata('web_user');
        $id = $user->id;
        $this->load->helper('paging');
        if (empty($user) && !isset($id)) {            
            redirect(base_url().'notfound');            
        }
        $data = $head = $where = $whereLesson = $orderBy = [];
        $where = [
            'user_course.user_id' => (int) $user->id,
        ];
        
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $total = $this->user_model->totalCourseBought($where);  
        $courses = $this->user_model->getCourseBought($where, $limit, $offset);
        $data['courses'] = $courses;
        $data['offset'] = $offset+1;
        //get lessons bought
        $data['paging'] = paging(base_url().'thanh-vien/khoa-hoc-da-mua', $page, $total, $limit);
        //breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add('Khóa học đã mua', base_url().'thanh-vien/khoa-hoc-da-mua');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //load view
        //
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Khóa học đã mua';

        $this->load->view('layouts/header', $head);
        $this->load->view('user/course_bought', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    * [info user]
    **/
    public function memberInfo( $name = '' )
    {
        $data = $head = [];
        if( empty($name) ){
            redirect(base_url().'notfound');
        }
        //get info user by username
        $where  = ['users.username' => $name];
        $user = $this->user_model->getUser( $where );
        if( empty($user) ){
            $this->session->set_flashdata('error', 'Thành viên này hiện không có');
            redirect(base_url().'notfound');
        }
        $data['user'] = $user;
        //get core user
        $where = [
            'exam_id !=' => null,
            'user_id' => $user->id
        ];
        $userScore = $this->user_model->getUserExam($where, $limit = 1)[0];
        $data['userScore'] = $userScore;
        //set breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thành viên', base_url().'thanh-vien');
        $this->breadcrumbs->add($user->full_name, base_url().'thanh-vien/'.$user->username.'');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //load view

        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Thành viên';

        $this->load->view('layouts/header', $head);
        $this->load->view('user/info', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
}