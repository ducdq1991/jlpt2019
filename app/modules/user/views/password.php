<div id="user-module">
	<div class="container">
        <?php $this->load->view('user/sidebar'); ?>
		<div id="change-pass" class="col-md-9 col-sm-9 col-xs-12">
			<h1 class="title-page title-site title-register">Thay đổi mật khẩu</h1>
            <div class="form-info user-box-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <!--end notification-->
    			<form action="" method="post">
    				<div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="username">Mật khẩu cũ :</label>
                        <div class="col-sm-9 col-md-9 col-xs-12">
                            <input type="password" name="password" class="form-control" id="password" value="" placeholder="Mật khẩu cũ">
                            <?php echo form_error('password', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="username">Mật khẩu mới :</label>
                        <div class="col-sm-9 col-md-9 col-xs-12">
                            <input type="password" name="new_password" class="form-control" id="new_password" value="" placeholder="Mật khẩu mới">
                            <?php echo form_error('new_password', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="username">Nhập lại mật khẩu mới :</label>
                        <div class="col-sm-9 col-md-9 col-xs-12">
                            <input type="password" name="re_new_password" class="form-control" id="re_new_password" value="" placeholder="Mật khẩu mới">
                            <?php echo form_error('re_new_password', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8 col-md-8">
                            <button type="submit" class="btn btn-user">Cập nhật</button>
                        </div>
                    </div>
    			</form>
            </div><!--div info-->    
		</div>
	</div>		
</div>