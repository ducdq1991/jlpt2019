<section id="register">
	<div class="container">
        <h1 id="title-register" class="title-page">Đăng ký tài khoản</h1>
		<div id="register-free" class="col-md-12 col-sm-12 col-xs-12">
            <!--notification-->
            <?php if( $this->session->flashdata('error') ) : ?>
                <div class="alert alert-sm alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php endif; ?>

            <form action="" method="post">

                <div class="form-group container-reg">
                    <input type="text" name="full_name" class="form-control" id="full_name" value="<?php echo set_value('full_name'); ?>" placeholder="Họ và tên *">
                    <?php echo form_error('full_name', '<div class="validation-alert alert alert-danger">', '</div>'); ?>                    
                </div>
                <div class="form-group container-reg">
                    <input type="text" name="username" class="form-control" id="username" value="<?php echo set_value('username'); ?>" placeholder="Tên đăng nhập *">
                    <?php echo form_error('username', '<div class="validation-alert alert alert-danger">', '</div>'); ?>                     
                </div>
                <div class="form-group container-reg">
                    <input type="password" name="password" class="form-control" id="password" value="" placeholder="Mật khẩu *">
                    <?php echo form_error('password', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                </div>
                <div class="form-group container-reg">
                    <input type="password" name="re_password" class="form-control" id="re_password" value="" placeholder="Nhập lại mật khẩu*">
                    <?php echo form_error('re_password', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                </div>
                <div class="form-group container-reg">
                    <input type="email" name="email" class="form-control" id="email" value="<?php echo set_value('email'); ?>" placeholder="Email *">
                    <?php echo form_error('email', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                </div>
                <div class="form-group container-reg">
                    <input type="text" name="phone" class="form-control" id="phone" value="<?php echo set_value('phone'); ?>" placeholder="Số điện thoại *">
                    <?php echo form_error('phone', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                </div>
                <div class="form-group container-reg" style="display: none;">  
                    <div class="col-sm-4 col-md-4 col-xs-12 input-captcha">
                        <input type="text" name="captcha" class="form-control" id="captcha" value="" placeholder="Nhập mã XN *">
                        <?php echo form_error('captcha', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                    </div>
                    <div class="col-sm-4 col-md-6 col-xs-12 img-captcha">
                        <div class="captcha-image span5 image" id="captImg"><?php echo $codeCaptcha;?></div>
                    </div>
                    <div class="col-lg-2"><a style="cursor: pointer;" class="refreshCaptcha" ><img src="<?php echo base_url();?>assets/img/icon-refresh.png" alt=""></div>
                </div>
                <hr class="left-oriented" />
                <div class="form-group text-center container-reg"> 
                    <div class="text-center">
                    <button type="submit" class="btn btn-register">Đăng ký</button>
                    <a href="<?php echo base_url();?>" class="btn btn-close" title="">Hủy bỏ</a>                        
                    </div>
                </div>
            </form>
		</div><!--div register free-->
	</div>
</section>

    <script>
    $(document).ready(function(){
        $('.refreshCaptcha').on('click', function(){            
            $.get('<?php echo base_url().'payment/refresh'; ?>', function(data){
                $('#captImg').html(data);
            });
        });
    });
    </script>