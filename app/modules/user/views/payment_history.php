<div id="user-module" class="lesson-viewer">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="lesson-view lesson-viewed payment-user col-md-9 col-sm-9 col-xs-12">
			<div class="user-box-content">
				<h1 class="title-page title-site">Lịch sử Thanh toán</h1>
				<div class="list-item">
					<?php if( !empty($payment) ) : ?>
						<?php $number = $offset; foreach( $payment as $row ) : ?>
							<div class="item">
								<div class="content">
									<h3><?php echo '<span>'.$number.'. </span><span class="content-detail">'.$row->content.'</span>'; ?></h3>
									
								</div>
								<p class="time-view"><?php echo 'Ngày thanh toán '.date('H:i a d/m/Y', strtotime( $row->created )); ?></p>
							</div>
						<?php $number++; endforeach; ?>
					<?php else: ?>
						<div class="item no_result">
							Bạn chưa thanh toán lần nào!
						</div>
					<?php endif; ?>	
					<div class="pagination"><?php echo $paging; ?></div>
				</div><!--list item-->
			</div>
		</div>
	</div>
</div>