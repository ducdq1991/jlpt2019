<div class="sidebar col-md-3 col-sm-3 col-xs-12">
	<h2>Tài khoản của tôi</h2>
	<div class="menu-user">
		<div class="list-item">
			<h3>Thông tin tài khoản</h3>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien" title="">Thông tin cá nhân</a>
			</li>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/thanh-tich" title="">Thành tích</a>
			</li>			
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/doi-mat-khau" title="">Thay đổi mật khẩu</a>
			</li>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/lich-su-hoat-dong" title="">Hoạt động vừa diễn ra</a>
			</li>
		</div>
		<div class="list-item">
			<h3>Học tập</h3>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/bai-giang-vua-xem" title="">Bài giảng vừa xem</a>
			</li>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/ket-qua-cuoc-thi-online" title="">Bài đã thi</a>
			</li>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/khoa-hoc-da-mua" title="">Khóa học đã mua</a>
			</li>
		</div>
		<div class="list-item">
			<h3>Thanh toán</h3>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-vien/lich-su-thanh-toan" title="">Lịch sử thanh toán</a>
			</li>
			<!--<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>thanh-toan" title="">Nạp tiền vào tài khoản</a>
			</li>-->
		</div>
	</div><!--menu user-->
</div>