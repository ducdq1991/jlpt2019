<div id="user-module" class="user-lesson">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="col-md-9 col-sm-9 col-xs-12">
			<div class="user-box-content">
				<h1 class="title-page title-site">Bài giảng/Câu hỏi đã lưu</h1>
				<div class="info">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>STT</th>
								<th>Mã</th>
								<th>câu hỏi</th>
								<th>Ngày lưu</th>
								<th>Hành động</th>
							</tr>
						</thead>
						<tbody>
							<?php $num = 1; foreach( $questionSaves as $item ) :?>
								<tr>
									<td class="stt"><?php echo $num; ?></td>
									<td class="code-qestion">KSSBTHS</td>
									<td class="name-qestion"><a href="" title=""><?php echo $item->title; ?></a></td>
									<td class="date"><?php echo date('d/m/Y H:i', strtotime( $item->created ));?></td>
									<td>
										<a href="" title="Xem lại"><i class="fa fa-eye"></i></a>
										<a href="" title="Xóa"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							<?php $num++; endforeach; ?>
						</tbody>
					</table>
					<div class="pagination"><?php echo $paging; ?></div>
				</div><!--info-->	
			</div>
		</div>
	</div>
</div>