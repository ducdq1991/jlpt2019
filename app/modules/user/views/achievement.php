<div id="user-module">
    <div class="container">
        <?php $this->load->view('user/sidebar'); ?>
        <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
        <div class="user-box-content">
            <h1 class="title-page title-site title-register">Thành tích</h1>
            <div class="col-md-12">

                <div class="form-group">
                    <span class=" col-sm-3 col-md-3 col-xs-12" for="class">Số câu đã làm: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12">
                        <strong><?php echo number_format($user->total_exam_question);?></strong>
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-sm-3 col-md-3 col-xs-12" for="class">Trả lời đúng: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12">
                        <strong><?php echo number_format($user->total_exam_question_true);?></strong>
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-sm-3 col-md-3 col-xs-12" for="class">Trả lời sai: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12">
                        <strong><?php echo number_format($user->total_exam_question_false);?></strong>
                    </div>
                </div>                                                            
                <div class="form-group">
                    <span class="col-sm-3 col-md-3 col-xs-12" for="class">Điểm: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12">
                        <strong><?php echo number_format($user->total_point);?></strong>
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-sm-3 col-md-3 col-xs-12" for="class">Tỷ lệ đúng: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12">
                    	<strong>
                        <?php 
                            if ($user->total_point > 0) {
                                echo round($user->total_exam_question_true/$user->total_point, 2);    
                            } else {
                                echo 0;
                            }
                            
                        ?> %
                        </strong>
                    </div>
                </div>                    
                <div class="form-group">
                    <span class="col-sm-3 col-md-3 col-xs-12" for="class">Xếp hạng: </span>
                    <div class="info col-sm-9 col-md-9 col-xs-12"><strong><?php echo $user->rank;?></strong></div>
                </div>
            </div><!--div info-->    
        </div><!--div register free-->
        </div>
    </div>
</div>