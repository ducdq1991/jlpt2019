<div id="user-module" class="user-action">
	<div class="container">
		<div class="row">
			<?php $this->load->view('user/sidebar');?>
			<div id="list-action" class="action-acc col-md-9 col-sm-9 col-xs-12">
				<div class="user-box-content">
				<h1 class="title-page title-site">Lịch sử hoạt động</h1>
	            <div id="vertical-timeline" data-offset="<?php echo $offset;?>" class="vertical-container dark-timeline center-orientation">
	                <?php if($activityUser):?>
	                	<?php foreach ($activityUser as $row) :?>
		                    <div class="vertical-timeline-block">
		                    	<?php if($row->type == UPDATE_INFO) :?>
		                        <div class="vertical-timeline-icon yellow-bg">
		                            <i class="fa fa-user" aria-hidden="true"></i>
		                        </div>
		                        <?php elseif($row->type == VIEW_LESSON) :?>
		                        	<div class="vertical-timeline-icon navy-bg">
		                            <i class="fa fa-file-video-o" aria-hidden="true"></i>
		                        </div>
		                        <?php elseif($row->type == DO_EXAM || $row->type == DO_EXERCISE) :?>
		                        	<div class="vertical-timeline-icon lazur-bg">
		                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
		                        </div>
		                        <?php elseif($row->type == SEND_ANSWER || $row->type == UPLOAD_ANSWER) :?>
		                        	<div class="vertical-timeline-icon blue-bg">
		                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
		                        </div>
		                    	<?php endif;?>
		                        <div class="vertical-timeline-content">
		                            <?php echo $row->content;?>
		                            <?php
		                            $date_in = new DateTime($row->updated);
	                                $date = new DateTime(date('Y-m-d H:i:s'));
	                                $interval = date_diff($date_in, $date);
	                                $y = $interval->y;
	                                $m = $interval->m;
	                                $d = $interval->d;
	                                $h = $interval->h;
	                                $i = $interval->i;
	                                $s = $interval->s;
	                                $timeago = '';
	                                if($y) $timeago = $y.' năm trước<br/> <small>'.date('d/m', strtotime($row->updated)).' '.date('H:i a', strtotime($row->updated)).'</small>';
	                                elseif($m) $timeago = $m.' tháng trước<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
	                                elseif($d) $timeago = $d.' ngày trước<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
	                                elseif($h) $timeago = 'Hôm nay<br/> <small>'.date('H:i a', strtotime($row->updated)).'</small>';
	                                elseif(!empty($i)) $timeago = 'Hôm nay<br/> <small>'.$i.' phút trước</small>';
	                                elseif($s) $timeago = 'Hôm nay<br/> <small>'.$s.' giây trước</small>';
	                                ?>
		                            <span class="vertical-date">
		                                <?php echo $timeago; ?>
		                            </span>
		                        </div>
		                    </div>
		                <?php endforeach;?>
		            <?php endif;?>
	            </div>
	            <div class="text-center load-more">
	            	<a class="btn-load-more link">Xem thêm</a>
	            	<p style="display: none">Loading...</p>
	            </div>
	            </div>
			</div>
		</div>
	</div>
</div>