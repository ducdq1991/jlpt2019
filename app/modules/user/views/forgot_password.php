<div id="user-module">
	<div class="container">
		<div id="forgot-pass" class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2">
			<h1 class="title-page title-site title-register">Quên mật khẩu</h1>
            <div class="form-info" >
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <?php $display = ($this->session->flashdata('success')) ? 'none' : 'block'; ?>
                <div style="display:<?php echo $display; ?>">
            		<p>Để lấy lại mật khẩu bạn hãy nhập địa chỉ email mà bạn đã dùng để đăng ký tài khoản  vào bên dưới và gửi cho chúng tôi. Chúng tôi sẽ gửi cho bạn một mật khẩu mới, Bạn có thể dùng mật khẩu đó để đăng nhập.</p>
            		 <form method="post">
            			<div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="email" class="form-control" id="email" value="<?php echo set_value('email');?>" placeholder="Hãy nhập Email của bạn đã đăng ký tài khoản">
                            <?php echo form_error('email', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12"> 
                            <div class="">
                                <button type="submit" class="btn btn-user">Gửi</button>
                            </div>
                        </div>
            		</form>
                </div>    
            </div>    
		</div>
	</div>		
</div>