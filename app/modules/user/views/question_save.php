<div id="user-module" class="user-lesson">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="col-md-9 col-sm-9 col-xs-12">
			<h1 class="title-page title-site">Bài giảng/Câu hỏi đã lưu</h1>
			<div class="info user-box-content">
				<?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>STT</th>
							<th>Mã câu hỏi</th>
							<th>Câu hỏi</th>
							<th>Ngày lưu</th>
							<th>Hành động</th>
						</tr>
					</thead>
					<tbody>
						<?php $num = 1; foreach( $questionSaves as $item ) :?>
							<tr>
								<td class="stt"><?php echo $num; ?></td>
								<td class="code-qestion"><?php echo $item->question_id; ?></td>
								<td class="name-qestion"><a href="" title=""><?php echo $item->title; ?></a></td>
								<td class="date"><?php echo date('d/m/Y H:i', strtotime( $item->created ));?></td>
								<td>
									<a data-toggle="collapse" data-target="#<?php echo $item->id;?>" title="Xem lại"><i class="fa fa-eye"></i></a>
									<a data-id="<?php echo $item->id;?>" class="question-saved-del" title="Xóa"><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							<tr id="<?php echo $item->id;?>" class="collapse infoQuestion">
								<td colspan="5" rowspan="1" >
									<div class="question">
										<?php echo $item->question; ?>
									</div>
									<div class="content-question">
										<?php foreach( $item->choices_answer as $key => $value ) : ?>
											<label>
												<strong><?php echo $key; ?>) </strong>
												<?php echo $value; ?>
											</label>
										<?php endforeach; ?>
									</div>
									<div class="answer">
										- Đáp án đúng:
										<strong><?php echo $item->true_answer;?></strong>
									</div>
								</td>
							</tr>
						<?php $num++; endforeach; ?>
					</tbody>
				</table>
				<div class="pagination"><?php echo $paging; ?></div>
			</div><!--info-->	
		</div>
	</div>
</div>
<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>
