<div id="user-module" class="result-exam-online">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="col-md-9 col-sm-9 col-xs-12">
			<div class="user-box-content">
				<h1 class="title-page title-site">Kết quả cuộc thi online</h1>
				<div class="info">
					<table class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>STT</th>
								<th>Cuộc thi</th>
								<th>Thời gian</th>
								<th>Điểm</th>
								<!--<th>Nhận xét</th>-->
								<th>Đáp án</th>
								<th>Hình thức</th>
								<th>Ngày thi</th>
							</tr>
						</thead>
						<tbody>
							<?php if( $examResult ) :?>
								<?php 
									$number = 1; 
									foreach( $examResult as $row ) :
									$answer = json_encode($row->answer);
									$result = json_encode($row->result); 
								?>
									<tr>
										<td class="stt"><?php echo $number; ?></td>
										<td class="name-exam"><a href="/luyen-tap/<?php echo $row->slug;?>.html" target="_blank"><?php echo $row->title; ?> <i class="fa fa-external-link"></i> </a></td>
										<td class="time-exam"><?php echo $row->time; ?> phút</td>
										<td class="score"><?php echo $row->score; ?></td>
										<td><a class="btn btn-sm btn-success previewArr" style="color:#fff;cursor: pointer" data-answer='<?php echo $answer;?>' data-result='<?php echo $result;?>' data-title="<?php echo $row->title; ?>" data-toggle="modal" data-target="#previewArrModal">Đáp án</a></td>
										<td class="exam-form">
											<?php 
												if($row->attribute == 1) {
													echo 'Trắc nghiệm';
												} else {
													echo 'Tự luận';
												}
											?>
										</td>
										<td class="date">
											<?php echo date('d/m/Y H:i', strtotime( $row->created ));?>
										</td>
									</tr>
								<?php $number++; endforeach; ?>	
							<?php else: ?>
								<tr>
									<td colspan="6"> Bạn chưa tham gia cuộc thi online nào! </td>
								</tr>
							<?php endif;?>
						</tbody>
					</table>
					<div class="pagination"><?php echo $paging; ?></div>
				</div><!--info-->	
			</div>
		</div>
	</div>
</div>
<div id="previewArrModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content arrFramePreview">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title arrHeader">Kết quả bài thi</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
 jQuery(document).ready(function($) {
    jQuery('.previewArr').click(function(){
        data = jQuery.parseJSON($(this).attr('data-answer'));
        result = $.parseJSON($(this).attr('data-result'));
        title = $(this).attr('data-title');
        var html = '';
        var index = 1;
        $.each(result, function(k1,v1){
            $.each(data, function(k2,v2){
                if(k1 == k2) {
                    if(v1==v2) {
                        html += '<div class="answer-true">Câu '+index+': (Mã câu hỏi: '+k1+') Trả lời: <strong>'+v2+'</strong></div>';
                    } else {
                        html += '<div class="answer-false">Câu '+index+': (Mã câu hỏi: '+k1+') Trả lời: <strong>'+v2+'</strong> Đáp án đúng: <strong>'+v1+'</strong></div>';
                    }
                }
            });
            index++;
        });
        jQuery('.arrFramePreview .modal-body').html(html);
        jQuery('.arrHeader').text('Kết quả bài thi: ' + title);
    });	 	
 });
</script>
<style type="text/css" media="screen">
#previewArrModal .arrFramePreview .modal-body .answer-false::after {
    content: "\f00d";
    font-family: "FontAwesome";
    color: #c92229;
    font-size: 16px;
    margin-left: 10px;
}	
#previewArrModal .arrFramePreview .modal-body .answer-true::after {
    content: "\f00c";
    font-family: "FontAwesome";
    color: #73f934;
    font-size: 16px;
    margin-left: 10px;
}
</style>