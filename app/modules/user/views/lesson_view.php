<div id="user-module" class="lesson-viewer">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="lesson-view lesson-viewed col-md-9 col-sm-9 col-xs-12">
			<div class="user-box-content">
			<h1 class="title-page title-site">Bài giảng vừa xem</h1>
			<div class="list-item">
				<?php if( !empty($lessonViews) ) : ?>
					<?php $number = $offset; foreach( $lessonViews as $row ) : ?>
						<div class="item">
							<div class="content">
								<h3><?php echo '<span>'.$number.'. </span><span class="content-detail">'.$row->content.'</span>'; ?></h3>
								<p class="code-question">Mã bài giảng: <?php echo $row->data; ?></p>
							</div>
							<p class="time-view"><?php echo 'Xem gần nhất '.date('H:i a d/m/y', strtotime( $row->updated )); ?></p>
						</div>
					<?php $number++; endforeach; ?>	
				<?php else: ?>
					<div class="item no_result">Bạn chưa xem bài giảng nào!
					</div>	
				<?php endif;?>	
				<div class="pagination"><?php echo $paging; ?></div>
			</div><!--list item-->
			</div>
		</div>
	</div>
</div>