<div id="user-module">
	<div class="container">
		<div id="detail" class="col-md-8 col-sm-8 col-xs-12">
            <div class="user-box-content">
    			<h1 class="title-page title-site title-register">Thành viên <?php echo $user->full_name;?></h1>
                <div class="user-info">
                    <div class="form-group">
                        <?php if (!empty($user->image)):?>
                           <img class="avatar-public" src="<?php echo base_url().$user->image;?>">
                        <?php else: ?>
                            <img class="avatar-public" src="<?php echo base_url();?>assets/img/user.jpg" alt="">
                        <?php endif;?>                                
                    </div>
                    <?php if( !empty( $user->full_name) ) :?>
        				<div class="form-group">
                            <label class="control-label col-sm-3 col-md-3 col-xs-12" for="full_name">Họ và tên :</label>
                            <div class="info col-sm-9 col-md-9 col-xs-12"><?php echo $user->full_name;?></div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Số câu đã làm: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Trả lời đúng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question_true);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Trả lời sai: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question_false);
                            ?>
                        </div>
                    </div>                                                            
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Điểm: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_point);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Tỷ lệ đúng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                if ($user->total_point > 0) {
                                    echo round($user->total_exam_question_true/$user->total_point, 2);    
                                } else {
                                    echo 0;
                                }
                                
                            ?> %
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Xếp hạng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12"><?php echo $user->rank;?></div>
                    </div>
                </div><!--div info-->  
            </div>  
		</div><!--div user info-->
	</div>
</div>