<div id="user-module">
    <div class="container">
        <?php $this->load->view('user/sidebar'); ?>
        <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
        <div class="user-box-content">
            <h1 class="title-page title-site title-register">Thông tin cá nhân của bạn</h1>
            <div class="form-info">
                <!--notification-->
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
                <!--end notification-->
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="full_name">Họ và tên :</label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <input type="text" name="full_name" class="form-control" id="full_name" value="<?php if( set_value('full_name') ) { echo set_value('full_name'); } else { echo $user->full_name; } ?>" placeholder="">
                            <?php echo form_error('full_name', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="username">Tên đăng nhập :</label>
                        <div class="info info-username col-sm-9 col-md-9 col-xs-12">
                            <span style="background: #e3e3e3; padding: 5px 20px;font-weight: 700;border-radius: 3px;"><?php echo $user->username; ?></span>                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="email">Email :</label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <input type="email" name="email" class="form-control" id="email" value="<?php if( set_value('email') ) { echo set_value('email'); } else { echo $user->email; } ?>" placeholder="">
                            <?php echo form_error('email', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="phone">Số điện thoại :</label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <input type="text" name="phone" class="form-control" id="phone" value="<?php if( set_value('phone') ) { echo set_value('phone'); } else { echo $user->phone; } ?>" placeholder="">
                            <?php echo form_error('phone', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Số dư tài khoản: </label>
                        <div class="info info-money col-sm-9 col-md-9 col-xs-12">
                            <?php echo number_format($user->amount, 2).' đ'; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Ảnh đại diện: </label>
                        <div class="col-sm-5 no-padding">                            
                            <input type="file" name="image" value="<?php echo $user->image;?>" <?php if($user->image) echo $user->image;?>/>
                        </div>
                        <div class="col-sm-5">
                            <?php if (!empty($user->image)):?>
                               <img src="<?php echo base_url().$user->image;?>" height="50">
                            <?php endif;?>                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Số câu đã làm: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Trả lời đúng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question_true);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Trả lời sai: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_exam_question_false);
                            ?>
                        </div>
                    </div>                                                            
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Điểm: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                echo number_format($user->total_point);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Tỷ lệ đúng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12">
                            <?php 
                                if ($user->total_point > 0) {
                                    echo round($user->total_exam_question_true/$user->total_point, 2);    
                                } else {
                                    echo 0;
                                }
                                
                            ?> %
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="control-label col-sm-3 col-md-3 col-xs-12" for="class">Xếp hạng: </label>
                        <div class="info col-sm-9 col-md-9 col-xs-12"><?php echo $user->rank;?></div>
                    </div>
                                        
                    <div class="form-group"> 
                        <div class="col-sm-offset-4 col-sm-8 col-md-8">
                            <button type="submit" class="btn btn-user">Chỉnh sửa thông tin</button>
                        </div>
                    </div>
                </form>
            </div><!--div info-->    
        </div><!--div register free-->
        </div>
    </div>
</div>