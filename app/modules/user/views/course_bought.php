<div id="user-module" class="lesson-viewer">
	<div class="container">
		<?php $this->load->view('user/sidebar');?>
		<div id="list-lessons" class="lesson-view course-bought col-md-9 col-sm-9 col-xs-12">
			<div class="user-box-content">
			<h1 class="title-page title-site">Bài giảng đã mua</h1>
			<div class="list-item">
				<?php if( !empty($courses) ) : ?>
					<?php $number = $offset; foreach( $courses as $row ) : ?>
						<div class="item">
							<div class="number-bullet">
								<?php echo $number;?>
							</div>
							<div class="content">
								<h3><a href="<?php echo base_url();?>khoa-hoc/<?php echo $row->slug;?>" title=""><?php echo $row->name; ?></a></h3>
								<p class="author"><?php echo $row->teacher;?></p>
							</div>
							<p class="date-time">
								<?php echo 'Ngày mua '.date('d/m/Y', strtotime($row->created));?>
							</p>
						</div>
					<?php $number++; endforeach; ?>	
				<?php else: ?>
					<div class="item no_result">Bạn chưa mua bài giảng nào!
					</div>	
				<?php endif;?>	
				<div class="pagination"><?php echo $paging; ?></div>
			</div><!--list item-->
			</div>
		</div>
	</div>
</div>