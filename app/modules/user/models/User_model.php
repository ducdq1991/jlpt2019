<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends Base_Model {

	public function __construct()
	{
		parent::__construct();
	}	

	public function addUser($args)
	{
		try {
			if( $this->db->insert('users', $args ))
			{
			   return $this->db->insert_id();
			} else {
				return false;
			}
		} catch (Exception $ex) {
			return false;
		}

        return false;
	}

	public function getUser($where = [])
	{
		try {
			$this->db->select('users.*, user_groups.access_key')
			->from('users')
			->join('user_groups','users.group_id = user_groups.id','left')
			->where($where)->limit(1);
			$query = $this->db->get();
			return $query->row();			
		} catch (Exception $ex) {

		}

        return null;
	}

	public function getUserById($id)
	{
		try {
			$where = [];
			$this->db->select('*');
			$where['users.id'] = (int) $id;
			$this->db->where($where)->limit(1);
			$query = $this->db->get('users');
            if ($query) {
                return $query->row();
            }
		} catch (Exception $ex) {			
			return null;
		}
        return null;
	}


    public function updateUser($id = 0, $args = [])
    {
    	try {
    		$where = array('id' => (int) $id );
    		if($this->db->where($where)->update('users',$args)){
    			return true;
    		}
    	} catch (Exception $ex) {
    		return false; 
    	}

        return false;
    }

    public function updateAmount($where = [], $amount = 0)
    {
    	try {   		
			$this->db->select('*');
			$this->db->where($where)->limit(1);
			$query = $this->db->get('users');
			$result = $query->row();
			if (!empty($result)) {
				$args = [];
				$args['amount'] = $result->amount + $amount;
	    		if ($this->db->where($where)->update('users',$args)) {
	    			return true;
	    		}
			}
    	} catch (Exception $ex) {
    		return false; 
    	}

    	return false;
    }

    public function getActivityByUser( $where = [], $whereExc = [], $limit = 0, $offset = 0, $orderBy = [] )
    {
    	try {
    		$this->db->select('content, data, updated, type');
    		if (!empty($where)) {
    			$this->db->where($where);
    		}
            if (!empty($whereExc)) {
                $this->db->where_not_in('type', $whereExc);
            }
    		$this->db->limit( $limit, $offset );
    		if (!empty($orderBy)) {
    			$this->db->order_by( $orderBy['key'], $orderBy['value'] );
    		}

    		$query = $this->db->get('user_logs');
    		if ($query) {
    			return $query->result();    			
    		}
    	} catch (Exception $ex) {

    	}

        return null;
    }

    public function totalActivityByUser( $where = [], $whereExc = [] )
    {
    	try {
    		$this->db->where($where);
            if (!empty($whereExc)) {
                $this->db->where_not_in('type', $whereExc);
            }
            return $this->db->count_all_results('user_logs');
    	} catch (Exception $ex) {    		
    		
    	}
        return 0;
    }

    public function getExamResultByUser( $where = [], $limit = 0, $offset = 0, $orderBy = [] )
    {        
    	try {
    		$this->db->select('user_exam.score, user_exam.attribute,user_exam.time, user_exam.created, user_exam.answer, user_exam.comment, exam.title, exam.slug, exam.questions, exam.publish_up, exam.publish_down');
    		$this->db->from('user_exam');
    		$this->db->join('exam', 'user_exam.exam_id = exam.id', 'left');
    		if( !empty($where) ){
    			$this->db->where( $where );
    		}
    		$this->db->limit( $limit, $offset );
    		if( !empty($orderBy) ){
    			$this->db->order_by( $orderBy['key'], $orderBy['value'] );
    		}
    		$query = $this->db->get();
            //print_r($this->db);

    		if( $query ){
                $userExam = $query->result();                
                if($userExam) {
                    foreach ($userExam as $key => $value) {
                        $userExam[$key]->answer = @unserialize($value->answer);
                        if ($value->attribute) {
                            $queIds = @explode(',', $value->questions);
                            $result = $this->getRightQuestions($queIds);                            
                            $userExam[$key]->result = $result;                         
                        }
 
                    }                    
                    return $userExam;
                }
                return null;
    		}
    		return false;
    	} catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }

    public function getRightQuestions($ids = null)
    {
        if($ids == null) {
            return null;
        }

        try {
            $this->db->select('id, score, true_answer');
            $query = $this->db->where_in('id', $ids)->get('questions');
            if($query) {
                $question = [];
                foreach ($query->result() as $item) {
                    $question[$item->id] = $item->true_answer;
                }
                return $question;
            }
            return null;            
        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }
    }

    public function totalExamResultByUser( $where = [] )
    {
    	try {
    		return $this->db->where($where)->count_all_results('user_exam');
    	} catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }

    public function getPaymentByUser( $where = [], $whereIn = [], $limit = 0, $offset = 0, $orderBy = [] )
    {
        try {
            $this->db->select('content, data, created');
            if( !empty($where) ){
                $this->db->where( $where );
            }
            if( !empty($whereIn) ){
                $this->db->where_in( 'type', $whereIn );
            }
            $this->db->limit( $limit, $offset );
            if( !empty($orderBy) ){
                $this->db->order_by( $orderBy['key'], $orderBy['value'] );
            }
            $query = $this->db->get('user_logs');
            if( $query ){
                $result = $query->result();
                return $result;
            }
            return false;
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }

    public function totalPayment( $where = [], $whereIn = [] )
    {
        try {
            $this->db->where($where);
            if( !empty($whereIn) ){
                $this->db->where_in('type', $whereIn);
            }
            return $this->db->count_all_results('user_logs');
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }

    public function getLessons( $whereIn = [], $limit = 0, $offset = 0  )
    {
        try {
            $query = $this->db->select('id, slug, title')
            ->where_in( $whereIn['field'], $whereIn['value'] )
            ->limit($limit, $offset)
            ->order_by('id','DESC')
            ->get('lessons')->result();
            if($query){
                return $query;
            }
            return false;
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }
    public function totalLessons( $whereIn = [] )
    {
        try {
            $this->db->where_in( $whereIn['field'], $whereIn['value'] );
            return $this->db->count_all_results('lessons');
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }
    public function getCourseBought($where = [], $limit = 10, $offset = 0)
    {
        try {
            $this->db->select('user_course.created, course_details.teacher, course_details.course_id, course_details.slug, course_details.name')
            ->join('course_details', 'course_details.course_id=user_course.course_id', 'left')
            ->where($where)
            ->limit($limit, $offset)
            ->order_by('user_course.created DESC');
            $query = $this->db->get('user_course');
            if($query) {
                return $query->result();
            }
            return null;
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }
    public function totalCourseBought( $where = [] )
    {
        try {
            $this->db->where($where)
            ->join('course_details', 'course_details.course_id=user_course.course_id', 'left');
            return $this->db->count_all_results('user_course');
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }

    public function getUsers($where = [], $limit = 10, $offset = 0, $orderby = [])
    {
        try {
            $this->db->select('users.*')
            ->where($where)
            ->limit($limit, $offset);
            if (!empty($orderby)) {

                foreach ($orderby as $key => $value) {
                    $this->db->order_by($key, $value);        
                }
                
            } else {
                $this->db->order_by('users.id', 'desc');    
            }
            
            $query = $this->db->get('users');
            if($query) {
                return $query->result();
            }
            return null;
        } catch (Exception $ex) {
            echo $ex->getMessage(); 
            return false; 
        }
    }
}