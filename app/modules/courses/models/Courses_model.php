<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_model extends Base_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getCategoryCourse()
	{
		$query = $this->db->get('course_categories');
		if ($query) {
			$result = $query->result();
			return $result;
		}	
	}
		
	/**
	* [get courses]
	**/
	public function getCourses( $where = [], $limit = 0, $offset = 0, $orderBy = array() )
	{
		try {
			$this->db->select('courses.id, courses.subject_id, courses.image, courses.intro_video, courses.parent, courses.start_date, courses.end_date, courses.lesson_view_limit, courses.practice_view_limit, courses.exam_view_limit, course_details.name, course_details.slug, course_details.description, course_details.teacher, course_details.price, course_details.origin_price, courses.status, course_details.manager_id, course_details.title');
			$this->db->from('courses');
			$this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
			$this->db->where( $where );
			$this->db->limit($limit, $offset);
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get();
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;

		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}

	public function getTopicLessions($where = [], $limit = 0, $offset = 0, $orderBy = [])
	{
		$data = [];

		try {
			$columns = [
				'courses.id',
				'courses.subject_id',
				'courses.image',
				'courses.intro_video',
				'courses.parent',
				'courses.start_date',
				'courses.end_date',
				'courses.lesson_view_limit',
				'courses.practice_view_limit',
				'courses.exam_view_limit',
				'course_details.name',
				'course_details.slug',
				'course_details.description',
				'course_details.teacher',
				'course_details.price',
				'course_details.origin_price',
				'courses.status',
				'course_details.manager_id',
				'course_details.title'
			];
			$this->db->select(implode(',', $columns));
			$this->db->from('courses');
			$this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
			$this->db->where( $where );
			$this->db->limit($limit, $offset);
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			} else {
				$this->db->order_by('courses.id', 'desc');
			}

			$query = $this->db->get();
			if ($query) {
				$result = $query->result();
				if (count($result) > 0) {
					foreach ($result as $item) {
						$item->lessons = $this->getLessons(['course_id' => $item->id]);
						$data[] = $item;
					}
				}
			}

			return $data;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}

		return null;
	}

	/**
	* [total courses]
	**/
	public function totalCourses( $where = array() )
	{
		try{			
			return $this->db->where($where)->count_all_results('courses');
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [total count lesson]
	**/
	public function totalLessons( $where = array(), $where_in=array() )
	{
		try{
			if(!empty($where) )		
				$this->db->where($where);
			if(!empty($where_in) )
				$this->db->where_in($where_in['key'], $where_in['value']);
			return $this->db->from('lessons')->count_all_results();
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get lesson]
	**/
	public function getLessons( $where = array(), $limit = 0, $orderBy = array() )
	{
		try {
			$this->db->select('id, title, slug, video, video_relates, file, video_title, link, created, course_id, price, access, views, status');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			} else {
				$this->db->order_by( 'ordering', 'asc' );
			}
			$query = $this->db->get('lessons');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get course with slug]
	**/
	public function getCourseBySlug( $where = [] )
	{
		try {
			$this->db->select('courses.id, courses.course_code, courses.subject_id, courses.image, courses.intro_video, courses.parent, courses.start_date, courses.end_date, courses.lesson_view_limit, courses.practice_view_limit, courses.exam_view_limit, course_details.name, course_details.slug, course_details.description, course_details.teacher, course_details.meta_keywords, course_details.meta_description, course_details.price, course_details.origin_price, course_details.manager_id, course_details.title')
			->from('courses')
			->join('course_details', 'courses.id = course_details.course_id', 'left')
			->where( $where )
			->limit(1);
			$query = $this->db->get();
			if ($query) {
				$result = $query->result();
				if (isset($result[0])) {
					return $result[0];	
				}	
							
				return null;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}

	public function getCourseById($where = [])
	{
		try {
			$this->db->select('courses.id, courses.course_code, courses.subject_id, courses.image, courses.intro_video, courses.parent, courses.start_date, courses.end_date, courses.lesson_view_limit, courses.practice_view_limit, courses.exam_view_limit, course_details.name, course_details.slug, course_details.description, course_details.teacher, course_details.meta_keywords, course_details.meta_description, course_details.price, course_details.origin_price, course_details.manager_id, course_details.title')
			->from('courses')
			->join('course_details', 'courses.id = course_details.course_id', 'left')
			->where($where)
			->limit(1);
			$query = $this->db->get();			
			if ($query) {
				return $query->row();
			}
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}

	public function getChildCourseId($id)
	{
		try{
			$query = $this->db->select('id')
			->where(array('parent' => (int) $id, 'status' => 1))
			->from('courses')->get()->result();
			$ids = array();
			if($query){
				foreach ($query as $value) {
					$ids[] = $value->id;
				}
				return $ids;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get user]
	**/
	public function getUser( $where = [] )
	{
		try {
			$result = $this->db->where( $where )
			->limit(1)
			->get('users')->result();
			if($result) {
				return $result[0];
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}
	/**
    * Update info user
    **/
    public function updateUser( $id = 0, $args = array() )
    {
    	try {
    		$where = array('id' => (int) $id );
    		if($this->db->where($where)->update('users',$args)){
    			return true;
    		}
	    	return false;
    	} catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }
    /*
    * check exist user course
    */
    public function existUserCourse($where = [])
    {
    	try {
	    	$query = $this->db->where($where)
	    	->limit(1)
	    	->get('user_course')
	    	->result();
	    	if($query) {
	    		return true;
	    	}
	    	return false;
	    } catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }
    /*
    * insert new record user course
    */
    public function insertUserCourse($args = [])
    {
    	try {
	    	return $this->db->insert('user_course', $args);
	    } catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }
    /*
    * get course follow user
    */
    public function getUserCourse($where = [])
    {    	
    	try {
	    	$query = $this->db->where($where)
	    	->get('user_course')
	    	->row();	    	
	    	if(is_object($query)) {
	    		return true;
	    	}
	    	return false;
	    } catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }

    public function getParents($parentId = 0)
    {
    		$where = [
    			'courses.id' => $parentId
    		];

			$obj = $this->db->select('courses.id, courses.course_code, courses.subject_id, courses.image, courses.lesson_view_limit, courses.practice_view_limit, courses.exam_view_limit,courses.intro_video, courses.parent, courses.start_date, courses.end_date, course_details.name, course_details.slug, course_details.description, course_details.teacher, course_details.meta_keywords, course_details.meta_description, course_details.price, course_details.origin_price, course_details.manager_id, course_details.title')
			->join('course_details', 'courses.id = course_details.course_id', 'left')
			->where($where)
			->limit(1)->get('courses')->row();
    		if (is_object($obj)) {
    			if ($obj->parent > 0) {    				
    				return $this->getParents($obj->parent);
    			}
    			return $obj;
    		}
    }

    public function getParentId($parentId = 0)
    {
    	if ($parentId > 0) {
    		$id = $parentId;
			$where = [
				'courses.id' => $parentId
			];    	
			$obj = $this->db->select('courses.id, courses.parent')
				->where($where)
				->limit(1)->get('courses')->row();		
			if (is_object($obj)) {
				$parentId = $obj->parent;
				$id = $obj->id;							
				$this->getParentId($parentId);				
			}

			return $id;
    	}

    	return false;
    }

    public function getcourseIds($id)
    {
    	$args = [];    	
		$where = [
			'courses.parent' => (int) $id
		];

		$query = $this->db->select('courses.id, courses.parent')->where($where)->order_by('courses.ordering', 'ASC')->get('courses');   	
		if ($query) {
			if (count($query->result()) > 0) {
				foreach ($query->result() as $item) {
					$where = [
						'courses.parent' => (int) $item->id
					];					
					$args[] = (int) $item->id;
					$query = $this->db->select('courses.id, courses.parent')->where($where)->order_by('courses.ordering', 'ASC')->get('courses');
					if ($query) {
						if (count($query->result()) > 0) {
							foreach ($query->result() as $sitem) {
								$args[] = (int) $sitem->id;

								$where = [
									'courses.parent' => (int) $sitem->id
								];					
								$query = $this->db->select('courses.id, courses.parent')->where($where)->order_by('courses.ordering', 'ASC')->get('courses');
								if ($query) {
									if (count($query->result()) > 0) {
										foreach ($query->result() as $ssitem) {
											$args[] = (int) $ssitem->id;
										}
									}
								}

							}
						}
					}
				}
			}
		}
		$args[] = (int) $id;
		return $args;
    }

    public function addCoursePayment($args = []) 
    {
    	if (empty($args)) {
    		return false;
    	}
    	$where = [
    		'payments.type' => 'course', 
    		'data_id' => $args['data_id'], 
    		'payer' => $args['payer']
    	];

    	if ($this->getCoursePayment($where) == null) {
    		if ($this->db->insert('payments', $args)) {
    			return $this->db->insert_id();
    		}
    	}
    	return false;
    }

	public function getCoursePayment($where = [])
	{
		$query = $this->db->where($where)->get('payments');
		if ($query) {
			if (is_object($query->row())) {
				return $query->row();
			}
		}

		return null;
	}

	//Update view item for course by lesson, exam or practice
    public function updateViewLimit($courseId = 0, $args = [])
    {
    	$default = ['lesson_id', 'exam_id', 'practice_id', 'user_id'];

    	if ($courseId !== 0) {
    		$data = [];
    		$data['course_id'] = $courseId;   		
    		foreach ($args as $key => $value) {
    			if (in_array($key, $default)) {
    				$data[$key] = $value;    				    				
    			}
    		}

    		return $this->db->insert('course_views_limit', $data);
    	}    
    }

    //Check view limit
	public function viewedCount($where = [])
	{
		$query = $this->db->where($where);
		$total = $query->count_all_results('course_views_limit');
		return $total;
	}

	public function getCode($where = [])
	{
		$query = $this->db->where($where)->get('course_codes');
		if ($query) {
			if (is_object($query->row())) {
				return $query->row();
			}
		}
		return null;
	}

	public function hasCode($courseId = 0)
	{
		$query = $this->db->where(['course_codes.course_id' => $courseId]);
		$total = $query->count_all_results('course_codes');
		return $total;		
	}

	public function getUserCourseCode($where = [])
	{
		if (!empty($where)) {
			$query = $this->db->where($where)->get('course_code_users');
			if ($query) {
				if (is_object($query->row())) {
					return $query->row();
				}
			}			
		}

		return null;
	}
	public function addUserCourseCode($args = [])
	{		
		if ($this->db->insert('course_code_users', $args)) {
			$where = ['code' => $args['code']];
			$this->db->where($where)->update('course_codes', ['status' => 0]);
			return $this->db->insert_id();
		}		
	}

	public function getCoupon($where = [])
    {    	
    	try {
    		$query = $this->db->where($where)->get('salemans');    	
	    	if (is_object($query->row())) {
					return $query->row();
			}	    	
	    } catch (Exception $ex) {
    		return null;    		
    	}

    	return null;
    }

    public function updateSaleman($where = [], $args = [])
	{		
		$this->db->where($where)->update('salemans', $args);
		return true;	
	}

	public function updateLesson($where = [], $args = array() )
    {
    	try {
    		if($this->db->where($where)->update('lessons',$args)){
    			return true;
    		}
	    	return false;
    	} catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }
}


