<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends Base_Controller {

	private $courseDiscountPercent = 0;

	public function __construct()
	{
		parent::__construct();

		if (isset($this->_settings['course_discount_percent'])) {
			$this->courseDiscountPercent = (float) $this->_settings['course_discount_percent'];
		}

		$this->load->library('breadcrumbs');
		$this->load->model('exam/exam_model');
		$this->load->model('courses_model');		
	}

	/*
	* Danh sach khoa hoc
	* List danh sach toan bo cac khoa hoc
	*/
	public function index( $page = 0 )
	{
		$this->setCurrentUrl();		
		$data = $where = $orderBy = $head = [];
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'khoa-hoc');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Khóa học';
		
	    $courseCategories = $this->index_model->getCategoryCourse();
	    $courseItems = [];
	    foreach ($courseCategories as $cate) {
		    $where = ['parent' => 0, 'courses.status' => 1, 'courses.category_id' => $cate->id];	    
		    $order = ['key' => 'courses.ordering', 'value' => 'ASC'];
		    $courses = $this->index_model->getCourses($where, $limit = 8, $order);
		    $cate->courses = $courses;
		    $courseItems[] = $cate;	    	
	    }
	    $data['courseItems'] = $courseItems;

		$this->load->view('layouts/header', $head);
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	/*
	* Trang chi tiet khoa cua khoa
	* Hien thi cac thong tin co ban cua khoa hoc
	*/
	public function course($slug = '')
	{			
		if ($slug == '') {
			redirect(base_url('khoa-hoc'));
		}
		$this->setCurrentUrl();

		$webUser = $this->session->userdata('web_user');
		$userId = isset($webUser) ? $webUser->id : null;
		
		$price = 0;
		$isBuyed = false;		
		$data = $where = $orderBy = $head = [];

		$where['course_details.slug'] = $slug;
		$where['courses.status'] = 1;
		$course = $this->courses_model->getCourseBySlug($where);
		if (empty($course)) {
			redirect(base_url('notfound'));
		}
		
		if ($course->parent > 0) {
			$parentCourse = $this->courses_model->getParents($course->parent);
			redirect(base_url('khoa-hoc/' . $parentCourse->slug));
		}

		$totalCourseCode = (int) $this->courses_model->hasCode($course->id);
		$data['totalCourseCode'] = $totalCourseCode;

		$courseId = $course->id;	
		$data['course'] = $course;				
		$price = $course->price;		

		$isBuyed = $this->isBuyed($userId, $course->id);
		$data['price'] = $price;
		$data['isBuyed'] = $isBuyed;	

		//Tong so chuyen de
	    $where = ['parent' => (int) $course->id, 'status' => 1];
	    $totalTopical = $this->courses_model->totalCourses($where);
	    $course->child_course = $totalTopical;

	    //Tong so bai giang
	    $courseIds = $this->courses_model->getChildCourseId($courseId);
	    $courseIds[] = (int) $courseId;	    
	    $where = [
	    	'key' => 'course_id',
	    	'value' => $courseIds
	    ]; 	    
	    $totalLesson = $this->courses_model->totalLessons([], $where);
	    $course->total_lesson = $totalLesson;

		//Breadcrumbs
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url() . 'khoa-hoc');
        if (!empty($parentCourse)) {
        	$this->breadcrumbs->add(ucfirst($parentCourse->name), base_url() . 'khoa-hoc/' . $parentCourse->slug);
       	}

        $this->breadcrumbs->add(ucfirst($course->name), base_url() . 'khoa-hoc/'. $course->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //SEO
        $metaGeneral = $this->getMetas();
        $metas = [
        	'meta_keyword' => !empty($course->meta_keywords) ? $course->meta_keywords : $metaGeneral['meta_keyword'],
        	'meta_description' => !empty($course->meta_description) ? $course->meta_description : $metaGeneral['meta_description'],
        ];
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = 'Khóa học | ' . $course->name;

        //get featrured
        $featrured = $this->blockFeatured();
        $data['lastestPosts'] = $featrured['lastestPosts'];

        //Nhap ma khoa hoc, kiem tra xem ma khoa hoc nay da duoc su dung chua, neu chua thi add vao bang su dung ma KH
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
        	$confirmCode = strtoupper($this->input->post('confirm_code', ''));
        	$confirmCourseId = (int) $this->input->post('confirm_course_id', ''); 
        	if ($confirmCode != '' && $confirmCourseId > 0) {
				$where = [
					'course_codes.course_id' => $confirmCourseId,
					'course_codes.code' => $confirmCode,					
				];
				
				$code = $this->courses_model->getCode($where);
				if (!is_null($code)) {


					//Kiem tra neu ma nay chua co trong danh sach da su dung, thi add vao danh sach
					$where = [
						'course_id' => $confirmCourseId,
						'code' => $confirmCode,					
					];
					if (is_null($this->courses_model->getUserCourseCode($where))) {
						$args = [
							'code' => $confirmCode,
							'course_id' => $courseId,
							'user_id' => $userId,
							'created_at' => date('Y-m-d H:i:s')
						];						
						$this->courses_model->addUserCourseCode($args);

						if (!$this->courses_model->existUserCourse(['user_id' => $userId, 'course_id' => $courseId ])) {
						$usr = [
							'user_id' => (int) $userId,
							'course_id' => (int) $courseId,
							'cost' => 0,
							'created' => date('Y-m-d H:i:s'),
							'type' => 3//Mua sach + Su dung ma
						];						
						if ($this->courses_model->insertUserCourse($usr)) {
							$this->session->set_flashdata('success', 'Chúc mừng bạn có thể bắt đầu tham gia khóa học!');

							$name = (!empty($webUser->full_name)) ? $webUser->full_name : $webUser->username;
							$content = $name. ' đã xác nhận mã ' . $confirmCode . ' của khóa học: '.$course->name.' thành công.';		
							$this->generateLog(PURCHASE_COURSE, (int) $userId, (int) $courseId, $content);

							$payments = [];
							$payments['name'] = 'Xác nhận mã của khóa học ' . $course->name;
							$payments['receiver'] = $course->manager_id;
							$payments['payer'] = (int) $userId;
							$payments['created_time'] = date('Y-m-d H:i:s');
							$payments['content'] = $content;
							$payments['type'] = 'course';
							$payments['price'] = (int) 0;
							$payments['data_id'] = (int) $course->id;
							$this->courses_model->addCoursePayment($payments);
						} else {
							$this->session->set_flashdata('error', 'Xác nhận thất bại. Mã không hợp lệ. Vui lòng thử lại!');
						}
					}

					} else {
						$this->session->set_flashdata('error', 'Mã xác nhận đã được sử dụng!');
					}				
								
				} else {
					$this->session->set_flashdata('error', 'Mã khóa học không chính xác!');
				}
        	}

        	redirect(base_url('khoa-hoc/' . $course->slug));
        }

        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('course', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function chuyende($slug = '')
	{
		$auth = $this->session->userdata('web_user');
		$userId = isset($auth) ? $auth->id : null;

		if ($slug == '') {

			redirect(base_url('khoa-hoc'));
		}

		$this->setCurrentUrl();

		$price = 0;
		$isBuyed = false;	
		$view = 'chuyende';
		$data = $where = $orderBy = $head = [];

		// Lay thong tin khoa hoc
		$where['course_details.slug'] = $slug;
		$where['courses.status'] = 1;
		$course = $this->courses_model->getCourseBySlug($where);
		if (empty($course)) {
			redirect(base_url('notfound'));
		}	
		$courseId = $course->id;	
		$data['course'] = $course;
		$price = $course->price;

		//Lay thong tin khoa hoc cha. De kiem tra gia cua khoa hoc, hoc sinh trong khoa hoc
		// Kiem tra neu co khoa hoc cha, thi thiet lap lai gia cua khoa hoa
		$parentId = $course->parent;
		if ($parentId > 0) {
			$parentCourse = $this->courses_model->getParents($parentId);
			if (!empty($parentCourse)) {
				if ($parentCourse->price > 0) {
					$price = $parentCourse->price;
				}
				$parentId = $parentCourse->parent;		
				$rootId = $parentCourse->id;
				$data['parentCourse'] = $parentCourse;
			}
		} else {
			$rootId = $course->id;
			$data['parentCourse'] = $course;
		}

		$data['rootCourseId'] = $rootId;

		//Breadcrumbs
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url() . 'khoa-hoc');
        if (!empty($parentCourse)) {
        	$this->breadcrumbs->add(ucfirst($parentCourse->name), base_url() . 'khoa-hoc/' . $parentCourse->slug);
       	}

        $this->breadcrumbs->add(ucfirst($course->name), base_url() . 'khoa-hoc/'. $course->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //SEO
        $metaGeneral = $this->getMetas();
        $metas = [
        	'meta_keyword' => !empty($course->meta_keywords) ? $course->meta_keywords : $metaGeneral['meta_keyword'],
        	'meta_description' => !empty($course->meta_description) ? $course->meta_description : $metaGeneral['meta_description'],
        ];

        $head['metas'] = $metas;
        $head['metas']['meta_title'] = 'Khóa học | ' . $course->name;

		// Kiem tra xem khoa hoc nay co tao ma hay khong
		$totalCourseCode = $this->courses_model->hasCode($rootId);
		$data['totalCourseCode'] = $totalCourseCode;

		//Kiem tra xem hoc sinh dang dang nhap co trong khoa hoc do hay khong
		if (!is_null($userId)) {
			$isBuyed = $this->isBuyed($userId, $rootId);
		}

		$data['price'] = $price;
		$data['isBuyed'] = $isBuyed;	

		//Danh sach chuyen de va bai giang
	    $where = [
		    	'parent' => (int) $rootId, 
		    	'status' => 1
		    ];
	    $orderBy = [
	    	'key' => 'courses.ordering',
	    	'value' => 'asc'
	    ];
		$topicLessons = $this->courses_model->getTopicLessions($where, 0, 0, $orderBy);
		$data['topicLessons'] = $topicLessons;

		$this->load->view('layouts/header', $head);
		$this->load->view($view, $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	/*
	* Danh sach cac chuyen de trong khoa hoc
	*/
	public function sub($slug = '')
	{	
		$webUser = $this->session->userdata('web_user');		
		if ($slug == '') {
			redirect(base_url('khoa-hoc'));
		}
		$this->setCurrentUrl();
		
		$userId = isset($webUser) ? $webUser->id : null;
		
		$price = 0;
		$isBuyed = false;		
		$data = $where = $orderBy = $head = [];

		// Lay thong tin khoa hoc
		$where['course_details.slug'] = $slug;
		$where['courses.status'] = 1;
		$course = $this->courses_model->getCourseBySlug($where);
		if (empty($course)) {
			redirect(base_url('notfound'));
		}	
		$courseId = $course->id;	
		$data['course'] = $course;
		$price = $course->price;

		//Lay thong tin khoa hoc cha. De kiem tra gia cua khoa hoc, hoc sinh trong khoa hoc
		// Kiem tra neu co khoa hoc cha, thi thiet lap lai gia cua khoa hoa
		$parentId = $course->parent;
		if ($parentId > 0) {
			$parentCourse = $this->courses_model->getParents($parentId);
			if (!empty($parentCourse)) {
				if ($parentCourse->price > 0) {
					$price = $parentCourse->price;
				}
				$parentId = $parentCourse->parent;		
				$rootId = $parentCourse->id;
				$data['parentCourse'] = $parentCourse;
			}
		} else {
			$rootId = $course->id;
			$data['parentCourse'] = $course;
		}

		$data['rootCourseId'] = $rootId;

		// Kiem tra xem khoa hoc nay co tao ma hay khong
		$totalCourseCode = $this->courses_model->hasCode($rootId);
		$data['totalCourseCode'] = $totalCourseCode;

		//Kiem tra xem hoc sinh dang dang nhap co trong khoa hoc do hay khong
		if (!is_null($userId)) {
			$isBuyed = $this->isBuyed($userId, $rootId);
		}
		$data['price'] = $price;
		$data['isBuyed'] = $isBuyed;	

		//Tong so chuyen muc con
	    $where = [
	    	'parent' => (int) $course->id, 
	    	'status' => 1
	    ];
	    $totalTopical = $this->courses_model->totalCourses($where);	    
	    $course->child_course = $totalTopical;

        //Neu child_course > 0 thi se lay tiep danh sach chuyen muc con
        if ($course->child_course > 0) {
        	$orderBy['key'] = 'courses.ordering';
        	$orderBy['value'] = 'ASC';
        	$topical = $this->courses_model->getCourses($where, $limit = 0, $offset = 0, $orderBy);
			foreach ($topical as $key=>$value) {
			    $where = ['course_id' => (int) $value->id]; 
			    $totalLesson = $this->courses_model->totalLessons($where);			    
			    $topical[$key]->total_lesson = $totalLesson;
			}
			$data['topicAndLesson'] = $topical;
        }

        //Neu child_course = 0 thi se lay tiep danh sach bai giang
        if ($course->child_course == 0) {
        	$orderBy['key'] = 'lessons.ordering';
        	$orderBy['value'] = 'ASC';
        	$where = ['course_id' => (int) $courseId];
        	$lessons = $this->courses_model->getLessons($where, $limit = 0, $orderBy);
        	$data['topicAndLesson'] = $lessons;
        }

	    //Tong so bai giang
	    $courseIds = $this->courses_model->getChildCourseId($courseId);
	    $courseIds[] = (int) $courseId;
	    $where = [
	    	'key' => 'course_id',
	    	'value' => $courseIds
	    ]; 	    
	    $totalLesson = $this->courses_model->totalLessons([], $where);
	    $course->total_lesson = $totalLesson;

		//Breadcrumbs
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url() . 'khoa-hoc');
        if (!empty($parentCourse)) {
        	$this->breadcrumbs->add(ucfirst($parentCourse->name), base_url() . 'khoa-hoc/' . $parentCourse->slug);
       	}

        $this->breadcrumbs->add(ucfirst($course->name), base_url() . 'khoa-hoc/'. $course->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //SEO
        $metaGeneral = $this->getMetas();
        $metas = [
        	'meta_keyword' => !empty($course->meta_keywords) ? $course->meta_keywords : $metaGeneral['meta_keyword'],
        	'meta_description' => !empty($course->meta_description) ? $course->meta_description : $metaGeneral['meta_description'],
        ];
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = 'Khóa học | ' . $course->name;

                
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners($positionAds, $type, $limit = 5);
        if (!empty($adsRightBottom)) {
            $data['adsRightBottom'] = $adsRightBottom;
        }

        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners($positionAds, $type, $limit = 5);
        if (!empty($adsRightTop)) {
            $data['adsRightTop'] = $adsRightTop;
        }

        //get featrured
        $featrured = $this->blockFeatured();
        $data['lastestPosts'] = $featrured['lastestPosts'];        

        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('sub', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}


	public function isBuyed($userId = 0, $courseId = 0)
	{
		$where = [
			'course_id' => $courseId,
			'user_id' => $userId
		];
		$result = $this->courses_model->getUserCourse($where);
		return $result;
		
	}

	public function canView($courseId = 0, $price = 0, $userId = 0, $slug = '')
	{
		$where = [
			'user_id' => (int) $userId,
			'course_id' => (int) $courseId,
		];	
		if (!$this->courses_model->existUserCourse($where)) {
			$this->session->set_flashdata('error', 'Để xem bài giảng này hãy mua khóa học!');
			if ($slug != '') {
				redirect(base_url().'khoa-hoc/' . $slug);
			} else {
				redirect(base_url('khoa-hoc'));	
			}		
		}

		return true;
	}


	public function detail($slug = '')
	{
		$this->setCurrentUrl();

		if ($slug == '') {			
			redirect(base_url() . 'khoa-hoc');
		}

		$user = $this->session->userdata('web_user');

		$price = 0;
		$data = $where = $orderBy = [];

		//Lay bai giang
		$where['slug'] = $slug;
		$where['status'] = 1;
		$lesson = $this->courses_model->getLessons($where, $limit = 1, $orderBy)[0];
		if (empty($lesson)) {
			$this->session->set_flashdata('error', 'Bài giảng chưa được phát hành!');
			redirect(base_url('khoa-hoc'));
		}		

		$course = $this->courses_model->getParents($lesson->course_id);

		if (!$user) {
			$this->session->set_flashdata('error', 'Bạn cần đăng nhập để xem bài giảng!');
			redirect(base_url('khoa-hoc/' . $course->slug . '/chuyen-de?is_login=1'));
		}

		// Bai giang khong mien phi
		if ($lesson->access == 0) {			
			if (!is_null($course)) {
				$price  = $course->price;
				$data['price'] = $price;

				if ($price > 0) {
					if ($this->canView($course->id, $price, $user->id, $course->slug)) {
						$data['isBuyed'] = true;
					}					
				}

			} else {
				$this->session->set_flashdata('error', 'Truy vấn không hợp lệ!');
				redirect(base_url('khoa-hoc'));			
			}

		}

		$data['course'] = $course;

		//Moi bai giang chi duoc truy cap so luot nhat dinh
		$args = [
			'lesson_id' => $lesson->id,
			'user_id' => $user->id
		];
		
		//Check view count by lesson_id
		$where = [
			'lesson_id' => $lesson->id, 
			'course_id' => $course->id,
			'user_id' => $user->id
		];
		$totalViewed = $this->courses_model->viewedCount($where);
		
		if ($totalViewed >= $course->lesson_view_limit) {
			$this->session->set_flashdata('warning', 'Lượt xem đã vượt quá giới hạn cho phép!');
			//redirect(base_url('khoa-hoc/' . $course->slug));
		} else {
			$this->session->set_flashdata('warning', 'Chú ý: Bạn còn <b>' . ($course->lesson_view_limit - $totalViewed) . '</b> lượt xem');
			$this->courses_model->updateViewLimit($course->id, $args);
		}

		// Ghi lich su truy cap
		if (!empty($user)) {
			$name = !empty($user->full_name) ? $user->full_name : $user->username;
			$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> xem bài giảng: <a href="'.base_url().'khoa-hoc/'.$lesson->slug.'.html">'.$lesson->title.'</a>';
			$this->generateLog(VIEW_LESSON, $user->id, (int) $lesson->id, $content );
		}

		$data['lesson'] = $lesson;

		//Cap nhat tong so luot xem
		
		$this->courses_model->updateLesson(['id' => $lesson->id], ['views' => $lesson->views + 1]);

		//Lay bai giang trong khoa hoc hoac chuyen de		
		$orderBy['key'] = 'lessons.ordering';
        $orderBy['value'] = 'ASC';
        $where = [
        	'course_id' => $lesson->course_id
        ];
		$lessonsCourse = $this->courses_model->getLessons($where, $limit = 0, $orderBy);
		$data['lessonsCourse'] = $lessonsCourse;

		//create breadcrumb
		$head = [];
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'khoa-hoc');
        if (!empty($course)) {
        	$this->breadcrumbs->add(ucfirst($course->name), base_url().'khoa-hoc/' . $course->slug );
        }        
        $this->breadcrumbs->add( ucfirst($lesson->title), base_url().'khoa-hoc' . $lesson->slug );
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //Thiet lap Meta SEO
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = $lesson->title;

		$this->load->view('layouts/header_lesson', $head);
		$this->load->view('courses/player', $data);
		$this->load->view('layouts/footer_lesson', $this->_footer_menu);
	}

	public function purchaseCourse()
	{
		$user = $this->session->userdata('web_user');
		if (empty($user) && !isset($user->id)) {            
            $this->session->set_flashdata('error', 'Bạn chưa đăng nhập');
            die;     
        }

        $args = [];
        $discount = 0;

        //get user
		$userInfo = $this->courses_model->getUser(['id' => (int) $user->id]);
		$amount = (float) $userInfo->amount;

		//get course
		$courseId = $this->input->post('id');
		if ($courseId != NULL) {
			$where = ['courses.id' => (int) $courseId];
			$course = $this->courses_model->getCourseBySlug($where);
			$price = (float) $course->price;

			$coupon_code = $this->input->post('coupon_code');
			$where = [
				'coupon_code' => $coupon_code
			];
			$coupon = $this->courses_model->getCoupon($where);
			if(!is_null($coupon)) {
				$discount = ($price * $this->courseDiscountPercent)/100;			
				$status = 1;
			}

			$grandTotal = ($price - $discount);


			if( $amount < $grandTotal ){			
				$this->session->set_flashdata('error', 'Tài khoản của em không đủ mua khóa học. Em cần <a href="/thanh-toan"><strong>NẠP TIỀN</strong></a> để có thể mua Khóa học em nhé!');
				echo 10;
				die;
			}
			$where = [
				'user_id' => (int) $userInfo->id,
				'course_id' => (int) $course->id,
			];
			if($this->courses_model->existUserCourse($where)) {
				$this->session->set_flashdata('warning', 'Bạn đã nộp học phí khóa học này rồi');
				echo 1;
				die;
			}

			$args = ['amount' => (float) ($amount - $grandTotal)];
			$updateAmount = $this->courses_model->updateUser($user->id, $args);
			if ($updateAmount) {
				$args = [
					'user_id' => (int) $userInfo->id,
					'course_id' => (int) $course->id,
					'cost' => (float) $price,
					'discount' => (float) $discount,
					'subtotal' => $grandTotal, 
					'created' => date('Y-m-d H:i:s'),
					'type' => 1
				];
				if($this->courses_model->insertUserCourse($args)) {
					$contentMessage = 'Bạn nộp học phí thành công! Khóa học: '.$course->name.', với giá: '. number_format($grandTotal) . 'đ';
					$this->session->set_flashdata('success', $contentMessage);

					$name = (!empty($user->full_name)) ? $user->full_name : $user->username;
					$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> mua khóa học: <a href="'.base_url().'khoa-hoc/'.$course->slug.'" title="">'.$course->name.'</a>';
					$this->generateLog(PURCHASE_COURSE, (int) $user->id, (int) $course->id, $content );

					$payments = [];
					$payments['name'] = 'Mua khóa học ' . $course->name;
					$payments['receiver'] = $course->manager_id;
					$payments['payer'] = (int) $user->id;
					$payments['created_time'] = date('Y-m-d H:i:s');
					$payments['content'] = $content;
					$payments['type'] = 'course';
					$payments['price'] = (float) $price;
					$payments['discount'] = (float) $discount;
					$payments['coupon_code'] = $coupon_code;
					$payments['data_id'] = (float) $course->id;
					$this->courses_model->addCoursePayment($payments);

					$where = [
						'coupon_code' => $coupon_code
					];
					$saleman = $this->courses_model->getCoupon($where);
					$args = ['amount' => (float) ($saleman->amount + $discount)];


					$payout = [];
                    $payout['name'] = 'Cộng tiền CTV KH #' . $course->id . '-UID' . $user->id;
                    $payout['note'] = 'KH' . $course->id . '-UID' . $user->id;
                    $payout['amount'] = $discount;
                    $payout['receiver_id'] = $saleman->id;
                    $payout['receiver_name'] = $saleman->name . '-' . $saleman->coupon_code;
                    $payout['data_id'] = $course->id;
                    $payout['type'] = 'plus';
                    $payout['pay_group'] = 'course';
                    $payout['created_at'] = date('Y-m-d H:i:s');
                    if ($this->checkPayout($payout)) {
                        $this->courses_model->updateSaleman($where, $args);
                        $this->setPayout($payout);
                    }

				} else {
					$this->session->set_flashdata('error', 'Hệ thống: Chưa thể lưu, nếu tiền đã bị trừ xin liên hệ admin để giải quyết');
				}
			} else {
				$this->session->set_flashdata('error', 'Chưa nộp được học phí');
			}
			die;
		} else {
			$this->session->set_flashdata('error', 'Chưa nộp được học phí');
			die();
		}
	}

	public function coupon()
	{		
		$status = 0;	
		$discount = 0;	
		$code = $this->input->post('coupon_code');
		$where = [
			'coupon_code' => $code
		];
		$coupon = $this->courses_model->getCoupon($where);
		if(!is_null($coupon)) {			
			$courseId = $this->input->post('course_id');
			if ($courseId != NULL) {
				$where = ['courses.id' => (int) $courseId];
				$course = $this->courses_model->getCourseById($where);
				$price = (float) $course->price;				
				$discount = ((float) $course->price * $this->courseDiscountPercent)/100;
				$status = 1;
			}
		}

		$data = [
			'status' => $status,
			'discount' => $discount			
		];
		echo json_encode($data);exit;
	}
}