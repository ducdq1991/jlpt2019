<div id="courses-module" class="module module-course trungdoan">
  <div class="container">
  	<div class="row">
	  	<h1 class="page-title" style="display: none;">Khóa học</h1>
	  	<div  id="list-courses" class="list-item">
	  		<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>

			<?php 
				if (isset($courseItems) && count($courseItems) > 0):
					foreach ($courseItems as $category): 
			?>
			<div class="category-course khoa-hoc">
				<div class="category-header">
					<h3><a href=""><?php echo $category->name;?></a></h3>
				</div>		
				<div class="items">

	        <?php $order = 0;?>
	  		<?php foreach ( $category->courses as $item ) : ?>
	  			<?php if(($order % 4) == 0) :?>
	  			<div class="row">
	  			<?php endif; $order++; ?>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="item">
						<div class="img-thumbnail">
							<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title="<?php echo $item->name; ?>">
								<img src="<?php echo base_url().$item->image;?>" alt="">
							</a>
							<?php
								if ($item->price > 0) {
									$priceClass = 'fee';
									$price = number_format($item->price) . ' đ';
								} else {
									$priceClass = 'free';
									$price = 'Miễn phí';								
								}
							?>
							<span class="hocphi <?php echo $priceClass;?>">
								<div>Học phí: <?php echo $price;?></div>
								<?php if ($item->origin_price != NULL || $item->origin_price > 0) :?>
								<div class="hocphi_goc">Học phí gốc: <del><strong><?php echo number_format( $item->origin_price ); ?></strong><sup>đ</sup></del></div>
								<?php endif;?>								
							</span>					
						</div>			
						<div class="info">
							<h3><a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title=""><?php echo $item->name; ?></a></h3>
							<?php if (isset($item->title) && $item->title != NULL) :?>
							<p class="sub-title"><?php echo $item->title; ?></p>
							<?php endif;?>
							<p class="khai_giang">Khai giảng: <strong><?php echo date('d/m/Y', strtotime( $item->start_date )); ?></strong></p>							
						</div>
					</div>			
				</div><!--div item-->
				<?php if(($order % 4) == 0 || $order == count($category->courses)) :?>
					
				</div>
				<?php endif;?>
			<?php endforeach; ?>
				</div>
			</div>	
			<?php 
					endforeach;
				endif;			
			?>
		</div>
	</div>
  </div><!--container-->
</div>  	