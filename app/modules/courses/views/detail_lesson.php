<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/content/global.css">
<div id="courses-module" class="module module-lesson">
	<div class="container">
		<div class="row">
                <?php if( $this->session->flashdata('success') ) : ?>
                <div class="alert alert-sm alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                <div class="alert alert-sm alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                <div class="alert alert-sm alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('warning'); ?>
                </div>
                <?php endif; ?>		
			<div id="content-lesson" class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="page-title"><?php echo $lesson->title; ?></h1>
				<div class="description-course">				
					<div class="video-lesson">
						<?php
						$video = $videoId = '';						
						if (strpos($lesson->video, 'youtube.com')) :
							$exp = explode('/', $lesson->video);
							if(isset($exp[2]) && $exp[2] == 'www.youtube.com') :
								$youtubeExp = explode('v=', $lesson->video);
								$videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';

						?>
							<div id="productWrapper" oncontextmenu="return false">
								<div id="myDivMainHolder2">
									<div id="myDiv2" style="width:100%"></div>
								</div>
								<div id="bigshare_overlay_logo">
									<img src="<?php echo base_url();?>/assets/img/logo.png" alt="" style="display: none;">
								</div>
							</div>						
						<?php 
							endif;						
						?>							
						<?php 
						elseif (strpos($lesson->video, 'drive.google.com')):

						?>
						<iframe width="100%" height="640" src="<?php echo $lesson->video;?>/preview?autoplay=1" frameborder="0" allowfullscreen="0"></iframe>
						<?php else : $video = base_url().$lesson->video; ?>
							<embed type="application/x-shockwave-flash" src="<?php base_url();?>/mediaplayer.swf" width="100%" height="360" style="undefined" id="MediaPlayer" name="MediaPlayer" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" flashvars="file=<?php echo $video;?>&amp;image=<?php echo base_url();?>assets/img/thumbnail-video.jpg&amp;showicons=true&amp;shownavigation=true"/>
						<?php endif;?>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="online-share">
							<div class="fb-like" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
							<div class="fb-send"></div>
						</div>					
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="download-lesson">
							<?php if( !empty($lesson->file) ):?>
								<a href="<?php echo base_url().$lesson->file;?>" class="download-lesson" download> <i class="fa fa-file-pdf-o"></i> Tải tài liệu</a>
							<?php endif; ?>
							<?php if( !empty($lesson->link) ):?>
								<a target="_blank" href="<?php echo $lesson->link;?>" class="download-lesson"> <i class="fa fa-edit"></i> Luyện Tập và Xem Lời Giải </a>
							<?php endif; ?>
						</div>
					</div>
				</div><!--content course-->
			</div>
			<div class="topical-lesson col-md-12 col-sm-12 col-xs-12">
				<div class="heading-lession">
					<h3><i class="fa fa-file-video-o"></i> BÀI GIẢNG</h3>
				</div>		
				<div class="list-topical-lesson scrollbar">
					<?php 
						if(!empty($lessonsCourse)):
							$stt = 1; 
							foreach ($lessonsCourse as $item) :
						?>
						<div class="item <?php if ($lesson->id === $item->id)  echo 'active' ; ?>">
							<h3><span class="stt"><?php echo $stt; ?>. </span> 								
								<a href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="" data-id="<?php echo $item->id; ?>">									
									<?php echo $item->title;?>
								</a>
							</h3>
						</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>
						<div class="item">
							<h3>Các bài giảng tiếp theo đang được cập nhật</h3>
						</div>
					<?php endif; ?>		
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->		
			<div class="chat col-md-12 col-sm-12 col-xs-12">
				<div class="chat-content">
					<h2><i class="fa fa-comments-o"></i> Hỏi đáp, thảo luận</h2>
					<div class="fb-comments" data-href="" data-colorscheme="light" data-numposts="5" data-width="100%"></div>
				</div>
			</div><!--chat-->
		</div>
	</div>	
</div><!--container-->	

<script type="text/javascript" src="<?php echo base_url();?>assets/java/pageMinimalDark.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/java/FWDEVPlayer.js"></script>
<script type="text/javascript">
	FWDEVPUtils.onReady(function(){
		var videoId = '<?php echo $videoId;?>';
		init(videoId);
	});
</script>
<script language="javascript">
	document.onmousedown=disableclick;
	function disableclick(event)
	{
	  if(event.button==2)
	   {
	     return false;    
	   }
	}
</script>