<?php $userSession = $this->session->userdata('web_user');?>
<div id="courses-module">
    <div class="container">
        <div class="row">
            <div  id="detail-courses" class="list-item col-md-8 col-sm-8 col-xs-12">
                <h1 class="title"><i class="fa fa-file-text-o"></i> <?php echo $course->name; ?></h1>
                <?php if( $this->session->flashdata('success') ) : ?>
                <div class="alert alert-sm alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                <div class="alert alert-sm alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                <div class="alert alert-sm alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('warning'); ?>
                </div>
                <?php endif; ?>
                <div class="description-course">
                    <div class="head-content">
                        <?php if ($course->image) :?>
                        <div class="img col-lg-5 col-md-5">
                            <img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
                        </div>
                        <?php endif;?>
                        <div class="info-course col-lg-7 col-md-7">
                            <div class="info-detail">
                                <p class="teacher">
                                    <label>Giáo viên: </label>
                                    <span class="name-teacher">
                                    <?php echo $course->teacher; ?>
                                    </span>
                                </p>

                                <?php if( $course->child_course > 0 ):?>
                                <p class="topical-count">
                                    <label>Số chuyên đề: </label>
                                    <span class="number-count"><?php echo $course->child_course;?></span>
                                </p>
                                <?php endif; ?>

                                <?php if ($course->total_lesson > 0 ) : ?>
                                <p>
                                    <label>Số bài giảng: </label>
                                    <span class="number-count"><?php echo $course->total_lesson;?></span>
                                </p>
                                <?php endif;?>

                                <p class="date-start">
                                    <label>Khai giảng: </label>
                                    <span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
                                </p>

                                <?php if($course->end_date !=0) : ?>
                                <p class="date-end">
                                    <label>Kết thúc: </label>
                                    <span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
                                </p>
                                <?php endif; ?>
                                
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div id="competition" class="col-md-4 col-sm-4 col-xs-12">
                <?php $this->load->view('block/ads_right_top'); ?>
                <?php                 
                if (isset($course->intro_video) && !is_null($course->intro_video)) : 
                    $videoId = @end(explode('?v=', $course->intro_video));
                ?>
                <div class="intro-video text-center">
                    <h3>Giới thiệu khóa học</h3>
                    <iframe width="340" height="305" src="https://www.youtube.com/embed/<?php echo $videoId;?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <?php endif;?>
            </div>

            <div class="course-blocks row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="course-box cb1">
                            <h3><a href="<?php echo base_url();?>khoa-hoc/<?php echo $course->slug;?>/chuyen-de">Từ vựng 語彙</a></h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="course-box cb2">
                            <h3><a href="<?php echo base_url();?>luyen-tap/chuyen-de/<?php echo $course->slug;?>">Ngữ pháp 文法</a></h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="course-box cb3">
                            <h3><a href="<?php echo base_url();?>thi-truc-tuyen?cid=<?php echo $course->id;?>">Kanji 漢字</a></h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="course-box cb3">
                            <h3><a href="<?php echo base_url();?>thi-truc-tuyen?cid=<?php echo $course->id;?>">Đọc hiểu 読解</a></h3>
                        </div>
                    </div>
                </div>                                                 
            </div>
            <div class="c-content-detail">
                <h3>Nội dung chi tiết khóa học</h3>
                <div class="ck-editor">
                    <?php echo $course->description; ?>    
                </div>                
            </div>

        </div>
    </div>
    <!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
    <div class="modal-dialog edit-modal">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nộp học phí</h4>
            </div>
            <div class="modal-body">                
                <div class="row">
                    <div class="col-md-6" style="border-right: 1px solid #ddd;">
                        <div class="payment-box">
                            <p class="price-text"></p>
                            <input type="text" name="coupon_code" placeholder="Mã giảm giá" class="form-control">
                            <div class="discountFee" style="display: none;"><span>Khuyến mại: - <span id="totalDiscount"></span></span></div>
                        </div>
                    </div>
                    <div class="col-md-6 totalFee">
                        <div class="payment-box">Học phí phải trả là: <strong id="courseTotal"></strong></div>
                    </div>                    
                </div>                                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info purchase" data-dismiss="modal">Đồng ý</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
            </div>
        </div>
    </div>
</div>
<!--modal info lesson-->

<div class="modal fade" id="course-content" tabindex="-1" role="dialog">
    <div class="modal-dialog edit-modal modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nội dung khóa học</h4>
            </div>
            <div class="modal-body">
                <div class="content-course">
                    <p><?php echo nl2br(htmlentities($course->description)); ?></p>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
            </div>
        </div>
    </div>
</div>
<!--modal info lesson-->
<style type="text/css" media="screen">
#purchaseModal .modal-body{
    background: #fafafa;
    padding-top: 0px;
    padding-bottom:0px;
}
.payment-box{
    padding-bottom:15px;
    padding-top:15px;
}
    input[name=coupon_code]{
        border: 1px solid #167ac6;
        box-shadow: none;
    }
    .totalFee>div{
        text-align: center;
        padding-top: 14px;
    }
    .totalFee strong{
    color: #188c04;
    font-size: 40px;
    display: block;
    }
    .discountFee{

    }
    .discountFee>span{
    margin-top: 12px;
    padding: 0px;
    display: inline-block;
    font-weight: 500;
    }

    .discountFee>span>span{
        font-weight: 700;
        color: red;
    }
</style>