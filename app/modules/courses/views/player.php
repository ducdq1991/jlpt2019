<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/content/global.css">
<style type="text/css" media="screen">
    #left-side {
        float: left;
        width: 12%;
        min-height: 300px;
        background: #03A9F4;
        position: relative;
    }
    
    #left-side .menu {
        float: left;
        width: 180px;
        margin-bottom: 15px;
        overflow: hidden;
        position: relative;
        border-top: 1px solid #DDD;
    }
    
    #left-side .menu > li {
        border: 1px solid #54aff7;
        border-top: none;
    }
    
    #left-side .menu > li > a {
        display: block;
        text-decoration: none;
        width: 100%;
        font-size: 14px;
        height: 40px;
        line-height: 40px;
        text-indent: 10px;
        position: relative;
        color:#fff;
    }
    
    #content-wrapper {
        position: relative;
        float: left;
        margin-left: 0px;
        width: 87%;
        border-left: 8px solid #ebf0f3;
    }
    
    #content-wrapper .main-content {
        margin-right: 315px;
    }
    
    #content-wrapper .sidebar {
        position: absolute;
        right: 0;
        top: 0;
        width: 300px;
    }
    
    .sidebar_module {
        margin-bottom: 10px;
        width: 100%;
        background: #FFF;
        float: left;
        overflow: hidden;
        border-left: 5px solid #ebf0f3;
        
    }

    .sidebar_module .item {
    	padding:0px 15px;
    }
    
    .module1 .scrollbar{
    	height: 450px;
    	overflow-y:scroll;
    }
    .module2 .scrollbar{
    	max-height: 400px;
    	overflow-y:scroll;
    }
    .sidebar_module .header {
            color: #FFF;
    background: #112870;
    padding: 6px 10px;
    font-size: 20px;
    margin: 0px;
    }

    #versus {
    width: 100%;
    background: #f0f0f0;
    color: #222;
    overflow: hidden;
    font-size: 25px;
    padding: 8px 15px;
    border-bottom: 9px solid #ebf0f3;
    }
    
    #versus .team {
        float: left;
        vertical-align: top;
        width: 40%;
        padding: 16px 0;
    }
    
    #versus .middle {
        float: left;
        width: 20%;
        font-size: 12px;
        line-height: 14px;
        background: rgba(5, 5, 5, 0.3);
        text-align: center;
        padding: 15px 0;
    }
    
    #player_container,
    #player_meta,
    #player_banner {
        position: relative;
        width: 100%;
    }
    
    #player_meta {
        float: left;
        width: 100%;
        margin: 15px 0;
    }
    .site_logo{
    	text-align: center;
    	margin-bottom: 10px;
    }
    .download-lesson{
    	margin-top: 20px;
    }
    .back-khoahoc a{
    	font-size: 18px;
    }
    .sidebar_module .item h3{
    	font-size: 16px;
    	font-weight: 300;
    }
    span.choice {
        background: green;
        padding: 3px 6px 3px 6px;
        border-radius: 5px;
        color: #ffffff;
    }

</style>

<div id="content">

    <div id="left-side">
    	<div class="site_logo"><a href="/" title=""><img src="http://jlpt-local.com/assets/img/logo.png" alt=""></a></div>
        <ul class="menu">
        	<li><a href="/" class="active">Trang chủ</a></li>
            <li><a href="/khoa-hoc">Khóa học</a></li>
            <li><a href="/bang-xep-hang">Bảng xếp hạng</a></li>
            <li><a href="/thu-vien-sach">Thư viện sách</a></li>
            <li><a href="#" target="_blank">Nhóm học tập</a></li>
            <li><a href="/user/logout">Đăng xuất</a></li>
        </ul>
    </div>
<?php
$relateVideos = unserialize($lesson->video_relates);
$relateNames = [];
$relateVideoNumber = [];
$relateVideoNumber[base64_encode($lesson->video)] = 1;
$relateVideoItem[1] = base64_encode($lesson->video);
if ($relateVideos) {
    $rID = 1;
    foreach ($relateVideos as $v) {
        $rID++;
        $relateNames[base64_encode($v['link'])] = $v['name'];
        $relateVideoNumber[base64_encode($v['link'])] = $rID;
        $relateVideoItem[$rID] = base64_encode($v['link']);
    }
}

//print_r($relateVideoNumber);
$video = $videoId = '';     
$vid = $this->input->get('vid'); 
if ($vid != '' || $vid != NULL) {
    $lessonName = $relateNames[$vid];
} else {
    $lessonName = ($lesson->video_title != '') ? $lesson->video_title : $lesson->title;
}
?>
    <div id="content-wrapper" style="min-height: 760px;background: #fff">
        <div class="main-content">
            <div id="versus">
               Bài Giảng: <?php echo $lessonName; ?>
               <div class="pull-right">
               	<span class="back-khoahoc">
               		<a href="<?php echo base_url('khoa-hoc/' . $course->slug . '/chuyen-de');?>" title=""><i class="icon fa fa-arrow-circle-o-left"></i> Quay lại khóa học</a>
               	</span>
               </div>
            </div>

            <div id="player_container">
                <div class="download-lesson">
                            <?php if( !empty($lesson->file) ):?>
                                <a href="<?php echo base_url().$lesson->file;?>" class="download-lesson" download> <i class="fa fa-file-pdf-o"></i> Tải tài liệu</a>
                            <?php endif; ?>
                            <?php if( !empty($lesson->link) ):?>
                                <a target="_blank" href="<?php echo $lesson->link;?>" class="download-lesson"> <i class="fa fa-edit"></i> Luyện Tập và Xem Lời Giải </a>
                            <?php endif; ?>
                        </div>




  <ul class="nav nav-tabs">
    <li class="active"><a href="#home">Nội dung lý thuyết</a></li>
    <li><a href="#menu1">Hoàn thành bài tập</a></li>
  </ul>

  <div class="tab-content">


    <div id="home" class="tab-pane fade in active">
      <h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p><b>Ý Nghĩa (意味)</b></br>
『際さいに』Cấu trúc này chỉ thời gian, trường hợp ( trong, ở, khi ), trang trọng hơn『 時 』nên được sử dụng nhiều trong những văn bản giải thích hoặc hướng dẫn.
Khi/lúc…</p>

<h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p><b>Ý Nghĩa (意味)</b></br>
『際さいに』Cấu trúc này chỉ thời gian, trường hợp ( trong, ở, khi ), trang trọng hơn『 時 』nên được sử dụng nhiều trong những văn bản giải thích hoặc hướng dẫn.
Khi/lúc…</p>

<h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p><b>Ý Nghĩa (意味)</b></br>
『際さいに』Cấu trúc này chỉ thời gian, trường hợp ( trong, ở, khi ), trang trọng hơn『 時 』nên được sử dụng nhiều trong những văn bản giải thích hoặc hướng dẫn.
Khi/lúc…</p>


<h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      <p><b>Ý Nghĩa (意味)</b></br>
『際さいに』Cấu trúc này chỉ thời gian, trường hợp ( trong, ở, khi ), trang trọng hơn『 時 』nên được sử dụng nhiều trong những văn bản giải thích hoặc hướng dẫn.
Khi/lúc…</p>





    </div><!--end#tab1-->


    <div id="menu1" class="tab-pane fade">
      <h3>練習しましょう</h3>
      <h3>Ⅰ。正しいほうに○をつけなさい</h3>
      <p>①英語は世界中で（<span class="choice">a. 話されて</span>　　b.　話られて）います。</p>
      <p>②英語は世界中で（a. 話されて　　<span class="choice">b.　話られて</span>）います。</p>
      <p>③英語は世界中で（a. 話されて　　<span class="choice">b.　話られて</span>）います。</p>
      <p>④英語は世界中で（<span class="choice">a. 話されて</span>　　b.　話られて）います。</p>
      <p>⑤英語は世界中で（<span class="choice">a. 話されて</span>　　b.　話られて）います。</p>

      <h3>Ⅱ　下の語を並べて替えて正しい文を作りなさい　に数字を書きなさい。</h3>
      <p>⑥　オリンピック---   ----   ---- ----開かれます。</p>
      <p>　1. 一度　　　　　２．に　　　　３．は　　　　　４．4年</p>
      <p>⑦　オリンピック---   ----   ---- ----開かれます。</p>
      <p>　1. 一度　　　　　２．に　　　　３．は　　　　　４．4年</p>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
</script>




                <div id="player" style="clear: both;margin-top: 20px;">



                    <div class="lesson-detail">
    







    
</div>



                </div>
            </div>
        </div>


        <div class="sidebar" style="margin-top: 0px;">            
            <div class="sidebar_module module2">
                <h2 class="header">Bài giảng khác</h2>
                <div class="list-topical-lesson scrollbar">
					<?php 
						if(!empty($lessonsCourse)):
							$stt = 1; 
							foreach ($lessonsCourse as $item) :
						?>
						<div class="item <?php if ($lesson->id === $item->id)  echo 'active' ; ?>">
							<h3><span class="stt"><?php echo $stt; ?>. </span> 								
								<a href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="" data-id="<?php echo $item->id; ?>">									
									<?php echo $item->title;?>
								</a>
							</h3>
						</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>
						<div class="item">
							<h3>Các bài giảng tiếp theo đang được cập nhật</h3>
						</div>
					<?php endif; ?>		
				</div><!--list topical or lesson-->
            </div>
        </div>
    </div>

    <br class="clear_fix">
</div>
