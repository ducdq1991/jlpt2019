<div id="courses-module" class="trung">
  <div class="container">
  	<div class="row">
	  	<div  id="detail-courses" class="list-item col-md-9 col-sm-9 col-xs-12">
			<h1 class="title"><i class="fa fa-file-text-o"></i> <?php echo $course->name; ?> - <?php if ($course->title != NULL) :?>
							<?php echo $course->title; ?>
							<?php endif;?></h1>
			<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('warning') ) : ?>
	            <div class="alert alert-sm alert-warning alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('warning'); ?>
	            </div>
	        <?php endif; ?>
			<div class="description-course">
				<div class="head-content">
					<?php if ($course->image) :?>
					<div class="img col-lg-5 col-md-5">
					<img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
					</div>
					<?php endif;?>
					<div class="info-course col-lg-7 col-md-7">					
						<div class="info-detail">
							<p class="teacher">
								<label>Giáo viên: </label>
								<span class="name-teacher">
									<?php echo $course->teacher; ?>
								</span>
							</p>
							<?php if( $course->child_course > 0 ):?>
							<p class="topical-count">
								<label>Số chuyên đề: </label>
								<span class="number-count"><?php echo $course->child_course;?></span>
							</p>
							<?php endif; ?>
							<p>
								<?php if ($course->total_lesson > 0 ) : ?>	
									<label>Số bài giảng: </label>
									<span class="number-count"><?php echo $course->total_lesson;?></span>
								<?php endif;?>
							</p>
							<p class="date-start">
								<label>Khai giảng: </label>
								<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
							</p>
							<?php if($course->end_date !=0) : ?>
							<p class="date-end">
								<label>Kết thúc: </label>
								<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
							</p>
							<?php endif; ?>
							<?php
							$session = $this->session->userdata('web_user');							
							 if ($isBuyed === true && isset($session)):?>
								<p class="study-free">
									<a href="" title="" class="btn btn-learning">
										Đã nộp học phí
									</a>
								</p>
							<?php else:?>						
							<?php if($price > 0 && $isBuyed === false) :?>
							<p class="date-end">
								<label>Học phí: </label>
								<span class="date-detail">
								<?php
									echo number_format($price, 0). ' VNĐ'; ?>
								</span>
							</p>
						
							<p class="study-price">
								<a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $course->id;?>" data-price="<?php echo $course->price;?>" data-toggle="modal" class="btn btn-learning">
									Nộp học phí
								</a>
							</p>
							<?php elseif ($price > 0 && $isBuyed === true) :?>
							<p class="date-end">
								<label>Học phí toàn khóa: </label>
								<span class="date-detail">
								<?php
									echo number_format($price, 0). 'VNĐ'; ?>
								</span>
							</p>
						
							<p class="study-price">
								<a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $parent->id;?>" data-price="<?php echo $parent->price;?>" data-toggle="modal" class="btn btn-learning">
									Nộp học phí
								</a>
							</p>
							<?php elseif($price == 0) : ?>
							<p class="study-free">
								<a href="" title="" class="btn btn-learning">
									Miễn phí
								</a>
							</p>
							<?php else:?>
							<?php endif;?>
							<?php endif;?>
						</div>
					</div>
				</div>
				<div class="content-course">
					<p><?php echo nl2br($course->description); ?></p>
				</div>
			</div><!--content course-->
			<div class="topical-lesson">
				<h1>
					<?php 
						if( $course->child_course > 0 ) {
							echo '<i class="fa fa-clipboard"></i> Các chuyên đề trong khóa học';
						} else {
							echo '<i class="fa fa-clipboard"></i> Các bài giảng trong khóa học';
						}
					?>
				</h1>				
				<div class="list-topical-lesson">
					<?php if( !empty($topicAndLesson) ) :?>
						<?php 
							$stt = 1; 
							foreach( $topicAndLesson as $item ):
						?>
							<div class="item">
								<h3>								
									<?php if( $course->child_course > 0 ) : ?>
										<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->name;?>
										</a>
									<?php else: ?>		
										<?php if ($item->status == 1) : ?>							
										<!--<a href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->title;?>
										</a>-->
										<a data-toggle="modal" href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->title;?>
										</a>
										<?php else:?>
										<a  class="<?php if($item->access == 0) echo 'logged';?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->title;?>
										</a>										
										<?php endif;?>
									<?php endif; ?>	
								</h3>
								<div class="info">
									<span class="number-lesson">
										<?php										
											if ($course->child_course > 0 && $item->total_lesson > 0) {
												echo 'Số bài: '. $item->total_lesson;
											}
										?>
									</span>
									<?php if ($item->status == 0):?>
									<span>Chưa phát hành</span>
									<?php else: ?>
									<span class="date-start">Ngày phát hành: 
										<?php 
											if( $course->child_course > 0 ) {
												echo date('d/m/Y', strtotime( $item->start_date ));
											} else {
												echo  date('d/m/Y', strtotime( $item->created ));
											}
										?>
									</span>
									<?php endif;?>
									<?php if( $course->child_course == 0 ) : ?>
										<span class="document-lesson">
											<?php if( !empty($item->file) ):?>
												<?php if ((($isBuyed === true) || ($price == 0)) && $this->session->has_userdata('web_user') && $item->status == 1) : ?> 
												 <i class="fa fa-file-pdf-o"></i> Tải tài liệu 
												 <a href="<?php echo base_url().$item->file;?>" class="download-lesson" download> tại đây</a>
												<?php endif;?>
											<?php endif; ?>	
										</span>
									<?php endif; ?>	
								</div>
							</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>		
						<div class="item">
							<h3>Chuyên đề / Bài giảng chưa được cập nhật</h3>
						</div>
					<?php endif;?>	
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->
	  	</div>
	  	<div id="competition" class="col-md-3 col-sm-3 col-xs-12">
	  		<?php $this->load->view('block/ads_right_top'); ?>
	  		<?php $this->load->view('block/featured'); ?>
	  		<?php $this->load->view('block/ads_right_bottom'); ?>
	  	</div>	
  	</div>
  </div><!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nộp học phí</h4>
			</div>
			<div class="modal-body">
				<p class="price-text"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info purchase" data-dismiss="modal">Đồng ý</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->
<div class="modal fade" id="codeChecker" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nhập mã khóa học</h4>
			</div>
			<div class="modal-body">
				<input type="text" name="course_code_checker" id="course_code_checker" class="form-control">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info purchase" data-dismiss="modal">Xác nhận</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->    	