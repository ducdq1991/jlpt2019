<?php $auth = $this->session->userdata('web_user');?>
<style type="text/css" media="screen">
    .list-collapse {
        margin-bottom: 10px;
        border-top: 3px solid #E91E63;
        -moz-box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.2);
    }
    .icon-Files:before {
        content: "\e684";
    }
    .list-collapse__item {
        position: relative;
        border-bottom: 1px solid #f4f4f4;
        background-color: #FFF;
    }
    .list-collapse__inner {
        position: relative;
        padding: 14px 15px 12px 24px;
        background: #fafafa;
        border: solid 1px #ccc;
    }
    .list-collapse__inner .icon {
        padding-right: 16px;
        font-size: 16px;
        color: #FF9900;
    }
    .stroke {
        font-family: 'Stroke-Gap-Icons';
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .list-collapse__title {
        display: inline-block;
        margin-top: -11px;
        padding-left: 16px;
        padding-right: 16px;
        line-height: 1;
        color: #383737;
        font-weight: bold;
        cursor: pointer;
    }
    .list-collapse__btn {
        position: absolute;
        top: 0;
        right: 0;
        padding: 12px 20px;
        font-size: 15px;
        color: #aaa;
        border: none;
        background-color: transparent;
    }
    .list-collapse__content {
        margin-top: 0px;
        border-top: 1px solid #eee;
    }
    .list-collapse__content .buy-topic {
        background-color: #f7f7f7;
    }
    .learn-outline-list {
        font-size: 13px;
        list-style: outside none none;
        margin: 0;
        padding: 0;
    }
    .learn-outline-item {
        border-bottom: 1px solid #d5d4d4;
        margin: 0;
        padding: 0;
    }
    .learn-outline-item:last-child{
        border-bottom: 0px;
    }
    .learn-lesson-wr {
        color: #4c4c4c;
        display: block;
        padding: 9px 10px 9px 50px;
        position: relative;
    }
    .scorm-section-right-action:before, .learn-lesson-wr:before {
        background: #e6e5e5 none repeat scroll 0 0;
        content: "";
        display: block;
        height: 100%;
        left: 30px;
        position: absolute;
        top: 0;
        width: 1px;
    }
    .lesson-process-wr {
        background-color: #fff;
        border: 1px solid #02BAD4;
        border-radius: 10px;
        display: block;
        height: 20px;
        left: 30px;
        margin-left: -10px;
        padding: 1px;
        position: absolute;
        top: 7px;
        width: 20px;
        z-index: 1;
    }
    .lesson-process-percent.percent-50 {
        background-color: #e84c3d;
        border-radius: 8px 0 0 8px;
        width: 8px;
    }
    .learn-lesson-wr .lesson-process-percent .fa, .lesson-process-wr .lesson-process-percent .fa {
        color: #fff;
        display: block;
        font-size: 11px;
        line-height: 16px;
    }
    .scorm-right-name {
        font-size: 13px;
        font-weight: 500;
        margin: 0 0 0px;
        font-weight: bold;
        max-width: 400px;
    }
    .scorm-right-action {
        color: #999;
        margin-left: 0;
        padding-left: 0;
    }
    .scorm-right-action>li.pull-right {
        margin-top: -24px;
    }
    .scorm-right-action>li {
        float: left;
        margin-right: 5px;
        list-style: none;
    }
    .scorm-learn-times {
        display: block;
        min-width: 70px;
    }
</style>
<div id="courses-module">
  <div class="container">
  	<div class="row">
	  	<div  id="detail-courses" class="list-item col-md-8 col-sm-8 col-xs-12">
			<h1 class="title">
				Chuyên đề và bài giảng khóa học: <?php echo $course->name;?>
			</h1>			
			<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('warning') ) : ?>
	            <div class="alert alert-sm alert-warning alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('warning'); ?>
	            </div>
	        <?php endif; ?>

			<div class="topical-lesson">			
				<div class="list-topical-lesson scrollbar">
				<?php if (count($topicLessons) > 0) : ?>
					<ul class="list-collapse list-unstyled ">
					<?php foreach ($topicLessons as $topic) : ?>
						<li class="list-collapse__item">
							<div style="cursor: pointer;" class="list-collapse__inner" data-toggle="collapse" data-target="#content-<?php echo $topic->id;?>" aria-expanded="true"><i class="fa fa fa-edit"></i><span class="list-collapse__title"><?php echo $topic->name;?></span>
								<button class="list-collapse__btn" data-toggle="collapse" data-target="#content-<?php echo $topic->id;?>" aria-expanded="true"><i class="fa fa-angle-down"></i></button>

							</div>
						<?php if (count($topic->lessons) > 0) : ?>
							<div class="list-collapse__content collapse in" id="content-<?php echo $topic->id;?>" aria-expanded="true" style="">
								<ul class="learn-outline-list">
									<?php foreach ($topic->lessons as $lesson) : ?>
									<li class="learn-outline-item ">
										<?php
											$classLogin = '';
											$link = '#';
											if ($lesson->status == 1) {
												$link = base_url('khoa-hoc/' . $lesson->slug .'.html');
											}

											if ($lesson->access == 0) {
												$classLogin = 'logged';
											}
										?>

										<a id="scorm-right-link-wr-<?php echo $lesson->id;?>" class="learn-lesson-wr scorm-right-link-wr" href="<?php echo $link;?>" title="<?php echo $lesson->title;?>">
											<div class="lesson-process-wr"><span class="lesson-process-percent percent-50"><i class="fa "></i></span></div>
											<h4 class="scorm-right-name visible"><span class="scorm-right-link">
												<?php echo $lesson->title;?>
												<?php if ($lesson->access == 1) : ?>
													<b class="free-animate-flicker faa-passing animated fee-lesson">Free</b></span>
												<?php endif;?>
											</h4>
											<ul class="scorm-right-action clearfix">
												
												<li class="pull-right">
													<span title="Khai giảng" class="scorm-learn-times"></span>
													<span class="pull-right" style="margin-top: 13px;"><?php if ($lesson->status == 1) echo 'Đã phát hành'; else echo 'Chưa phát hành'; ?></span>

												</li>
												<li style="display: none;">
													<span title="Thời lượng bài giảng" class="scorm-learn-times"><i class="fa fa-play-circle"></i> 30 phút</span>
												</li>
												<li><span title="Lượt xem"><i class="fa fa-eye"></i> <?php echo number_format($lesson->views);?></span></li>

											</ul>
										</a>
									</li>
									<?php endforeach;?>
								</ul>
							</div>
						<?php else : ?>
							<span style="padding:20px;font-style: italic;display: block;font-size: 13px;color: red;">Chưa có bài giảng trong chuyên đề này!</span>
						<?php endif;?>
						</li>
					<?php endforeach;?>
					</ul>
				<?php else : ?>
					<div class="item">
						<h3>Chuyên đề / Bài giảng chưa được cập nhật</h3>
					</div>
				<?php endif;?>
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->
	  	</div>
	  	<div id="competition" class="col-md-4 col-sm-4 col-xs-12">
	  		<?php $this->load->view('block/ads_right_top'); ?>
			<div class="box-competition featured_widget_exam course-practice">
				<ul class="nav nav-tabs">
				  <li class="active" style="float: inherit;"><a data-toggle="tab"><i class="fa fa-newspaper-o"></i> THÔNG TIN KHÓA HỌC</a></li>
				</ul>
				<div class="tab-content">
					<div class="description-course" style="overflow: hidden;">
						<div class="head-content">									
							<div class="img col-lg-12 col-md-12">
								<?php if (isset($parentCourse)) :?>
									<img src="<?php echo base_url().$parentCourse->image;?>" alt="<?php echo $parentCourse->name; ?>">	
								<?php else :?>
									<img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
								<?php endif;?>
							</div>					
							<div class="info-course col-lg-12 col-md-12">					
								<div class="info-detail">
									<h3 style="font-size: 14px;font-weight: 700"><a href="/khoa-hoc/<?php echo $parentCourse->slug; ?>"><?php echo $parentCourse->name; ?></a></h3>
									<p class="teacher">
										<label>Giáo viên: </label>
										<span class="name-teacher">
											<?php echo $parentCourse->teacher; ?>
										</span>
									</p>
									<p class="date-start">
										<label>Khai giảng: </label>
										<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
									</p>
									<?php if($parentCourse->end_date !=0) : ?>
									<p class="date-end">
										<label>Kết thúc: </label>
										<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
									</p>
									<?php endif; ?>

		                            <!--Kiem tra khoa hoc co phi hay Mien phi-->
		                            <?php if ($price == 0) : ?>
		                                <h3 class="text-center" style="font-weight:700;color:#EF0D4E;font-size: 20px;">Miễn phí</h3>
		                            <?php else:?>
		                                <h3 class="text-center" style="font-weight:700;color:#EF0D4E;font-size: 20px;">
		                                Học phí: <?php echo number_format($price, 0). ' đ'; ?></h3>
		                            <?php endif;?>
		                            <!--END Kiem tra khoa hoc co phi hay Mien phi-->

		                            <?php if ($parentCourse->title != NULL) :?>
		                            <h3 class="sub-title text-center" style="font-style: italic;"><?php echo $parentCourse->title; ?></h3>
		                            <?php endif;?>
		                            <!--Neu chua nop hoc phi hoac chua dang nhap-->	
		                            <div class="btn-courses text-center">
		                            <!--Kiem tra da nop hoc phi-->
		                            <?php if (isset($auth) && $isBuyed === true && $price > 0) :?>
		                                <a href="#" title="" class="btn btn-success">Đã nộp học phí</a>
		                            <?php endif;?>

		                            <?php if ((isset($auth) && $isBuyed === false) || !isset($auth)) :?>                                
		                                <?php if ($price > 0) :?>
		                                <a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $rootCourseId;?>" data-price="<?php echo $price;?>" data-toggle="modal" class="btn btn-danger">
		                                <i class="fa fa-money"></i> Nộp học phí
		                                </a>
		                                <?php endif;?>                  
		                            <?php endif;?>                                
		                            </div>
								</div>
							</div>
						</div>
					</div><!--content course-->
				</div>
			</div><!--box competition-->
	  	</div>	
  	</div>
  </div><!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nộp học phí</h4>
			</div>
			<div class="modal-body">
                <p class="price-text"></p>
                <p>Vui lòng chuyển khoản với nội dung: <strong>[Tên tài khoản] - Mã khóa học - [SĐT]</strong></p>
                <p>Ví dụ: <strong>nguyenvantuan-VNA3-0901.193.xxx</strong></p>
                <p>Ngân hàng: TP Bank (Tiên Phong Bank)<br>
Chủ Tài khoản: VŨ NGỌC ANH<br>
Số tài khoản: 01572023001<br>
Chi nhánh: HÀ NỘI</p>
            </div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->