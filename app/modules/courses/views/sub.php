<?php $userSession = $this->session->userdata('web_user');?>
<div id="courses-module">
  <div class="container">
  	<div class="row">
	  	<div  id="detail-courses" class="list-item col-md-8 col-sm-8 col-xs-12">
			<h1 class="title">
				<?php 
					if( $course->child_course > 0 ) {
						echo '<i class="fa fa-clipboard"></i> CÁC CHUYÊN ĐỀ TRONG KHÓA HỌC';
					} else {
						echo '<i class="fa fa-clipboard"></i> CÁC BÀI GIẢNG TRONG CHUYÊN ĐỀ: ' . $course->name;
					}
				?>
			</h1>			
			<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('warning') ) : ?>
	            <div class="alert alert-sm alert-warning alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('warning'); ?>
	            </div>
	        <?php endif; ?>

			<div class="topical-lesson">			
				<div class="list-topical-lesson scrollbar">
					<?php if( !empty($topicAndLesson) ) :?>
						<?php 
							$stt = 1; 
							foreach( $topicAndLesson as $item ):
						?>
							<div class="item">
								<h3>								
									<?php if( $course->child_course > 0 ) : ?>
										<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>/chuyen-de" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->name;?>
										</a>
									<?php else: ?>		
										<?php if ($item->status == 1) : ?>
											<a href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="">
												<span class="stt"><?php echo $stt; ?>. </span> 
												<?php echo $item->title;?>
											</a>
										<?php else:?>
										<a  class="<?php if($item->access == 0) echo 'logged';?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->title;?>
										</a>										
										<?php endif;?>
										<?php if($item->access == 1): ?><span class="faa-passing animated fee-lesson" style="color:red;font-weight: 700">Free</span><?php endif;?>
									<?php endif; ?>	
								</h3>
								<div class="info">
									<span class="number-lesson">
										<?php										
											if ($course->child_course > 0 && $item->total_lesson > 0) {
												echo 'Số bài: '. $item->total_lesson;
											}
										?>
									</span>
									<?php if ($item->status == 0):?>
									<span>Chưa phát hành</span>
									<?php else: ?>
									<span class="date-start">Ngày phát hành: 
										<?php 
											if( $course->child_course > 0 ) {
												echo date('d/m/Y', strtotime( $item->start_date ));
											} else {
												echo  date('d/m/Y', strtotime( $item->created ));
											}
										?>
									</span>
									<?php endif;?>
									<?php if( $course->child_course == 0 ) : ?>
										<span class="document-lesson">
											<?php if( !empty($item->file) ):?>
												<?php if ((($isBuyed === true) || ($price == 0)) && $this->session->has_userdata('web_user') && $item->status == 1) : ?> 
												 <i class="fa fa-file-pdf-o"></i> Tải tài liệu 
												 <a href="<?php echo base_url().$item->file;?>" class="download-lesson" download> tại đây</a>
												<?php endif;?>
											<?php endif; ?>	
										</span>
									<?php endif; ?>	
								</div>
							</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>		
						<div class="item">
							<h3>Chuyên đề / Bài giảng chưa được cập nhật</h3>
						</div>
					<?php endif;?>	
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->
	  	</div>
	  	<div id="competition" class="col-md-4 col-sm-4 col-xs-12">
	  		<?php $this->load->view('block/ads_right_top'); ?>
			<div class="box-competition featured_widget_exam course-practice">
				<ul class="nav nav-tabs">
				  <li class="active" style="float: inherit;"><a data-toggle="tab"><i class="fa fa-newspaper-o"></i> THÔNG TIN KHÓA HỌC</a></li>
				</ul>
				<div class="tab-content">
					<div class="description-course" style="overflow: hidden;">
						<div class="head-content">									
							<div class="img col-lg-12 col-md-12">
								<?php if (isset($parentCourse)) :?>
									<img src="<?php echo base_url().$parentCourse->image;?>" alt="<?php echo $parentCourse->name; ?>">	
								<?php else :?>
									<img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
								<?php endif;?>
							</div>					
							<div class="info-course col-lg-12 col-md-12">					
								<div class="info-detail">
									<h3 style="font-size: 14px;font-weight: 700"><a href="/khoa-hoc/<?php echo $parentCourse->slug; ?>"><?php echo $parentCourse->name; ?></a></h3>
									<p class="teacher">
										<label>Giáo viên: </label>
										<span class="name-teacher">
											<?php echo $parentCourse->teacher; ?>
										</span>
									</p>
									<p class="date-start">
										<label>Khai giảng: </label>
										<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
									</p>
									<?php if($parentCourse->end_date !=0) : ?>
									<p class="date-end">
										<label>Kết thúc: </label>
										<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
									</p>
									<?php endif; ?>

		                            <!--Kiem tra khoa hoc co phi hay Mien phi-->
		                            <?php if ($price == 0) : ?>
		                                <h3 class="text-center" style="font-weight:700;color:#EF0D4E;font-size: 20px;">Miễn phí</h3>
		                            <?php else:?>
		                                <h3 class="text-center" style="font-weight:700;color:#EF0D4E;font-size: 20px;">
		                                Học phí: <?php echo number_format($price, 0). ' đ'; ?></h3>
		                            <?php endif;?>
		                            <!--END Kiem tra khoa hoc co phi hay Mien phi-->

		                            <?php if ($parentCourse->title != NULL) :?>
		                            <h3 class="sub-title text-center" style="font-style: italic;"><?php echo $parentCourse->title; ?></h3>
		                            <?php endif;?>
		                            <!--Neu chua nop hoc phi hoac chua dang nhap-->	
		                            <div class="btn-courses text-center">
		                            <!--Kiem tra da nop hoc phi-->
		                            <?php if (isset($userSession) && $isBuyed === true && $price > 0) :?>
		                                <a href="#" title="" class="btn btn-success">Đã nộp học phí</a>
		                            <?php endif;?>

		                            <?php if ((isset($userSession) && $isBuyed === false) || !isset($userSession)) :?>                                
		                                <?php if ($price > 0) :?>
		                                <a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $rootCourseId;?>" data-price="<?php echo $price;?>" data-toggle="modal" class="btn btn-danger">
		                                <i class="fa fa-money"></i> Nộp học phí
		                                </a>
		                                <?php endif;?>                  
		                            <?php endif;?>                                
		                            </div>
								</div>
							</div>
						</div>
					</div><!--content course-->
				</div>
			</div><!--box competition-->
	  	</div>	
  	</div>
  </div><!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nộp học phí</h4>
			</div>
			<div class="modal-body">
                <p class="price-text"></p>
                <p>Vui lòng chuyển khoản với nội dung: <strong>[Tên tài khoản] - Mã khóa học - [SĐT]</strong></p>
                <p>Ví dụ: <strong>nguyenvantuan-VNA3-0901.193.xxx</strong></p>
                <p>Ngân hàng: TP Bank (Tiên Phong Bank)<br>
Chủ Tài khoản: VŨ NGỌC ANH<br>
Số tài khoản: 01572023001<br>
Chi nhánh: HÀ NỘI</p>
            </div>
			<div class="modal-footer">				
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->