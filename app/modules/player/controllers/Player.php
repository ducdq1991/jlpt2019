<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Player extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index($videoId)
	{
		$data['videoId'] = $videoId;
		$this->load->view('player/index', $data);
	}
}
