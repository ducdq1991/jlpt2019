<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/content/global.css">
	<style type="text/css">
		#productWrapper{
		    position: relative;
		}
		#productWrapper #bigshare_overlay_logo{
		    position: absolute;
		    top:3px;
		    right: 5px;
		    display: none;
		}
	</style>
</head>
<body>

	<div id="productWrapper">
		<div id="myDivMainHolder2">
			<div id="myDiv2" style="width:100%"></div>
		</div>
		<div id="bigshare_overlay_logo">
			<img src="<?php echo base_url();?>assets/content/minimal_skin_dark/logo.png" alt="">
		</div>
	</div>

	<script type="text/javascript" src="<?php echo base_url();?>assets/java/pageMinimalDark.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/java/FWDEVPlayer.js"></script>
	<script type="text/javascript">
		FWDEVPUtils.onReady(function(){
			var videoId = '<?php echo $videoId;?>';
			init(videoId);
		});
	</script>
</body>
</html>