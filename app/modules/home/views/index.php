<section class="newTickers">
	<div class="container">
		<div class="row">
			<div class="col-md-2 box-heading">
				<h3>Tin nổi bật:</h3>
			</div>	
			<div class="col-md-10 box-content text-left">
				<div class="vticker">
				  	<ul>
				  		<?php 
				  			if (count($featuredPosts) > 0):
				  				foreach ($featuredPosts as $item):
				  		?>
				  		<li>
				  			<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html"><?php echo $item->title;?></a>
				  		</li>
				  		<?php 
				  				endforeach;
				  			endif;?>
				  	</ul>				
				</div>
			</div>			
		</div>
	</div>
</section>
<section>
	<div id="banner-competition">
		<div class="container">
			<div class="row">
				<div id="banner-answer" class="col-md-9 col-sm-9 col-xs-12">
					<div id="banner-top">
						<?php if( !empty($slider) ) : ?>
							<?php foreach( $slider as $item ) :?>
								<a href="<?php echo $item->link; ?>" title="">
									<img src="<?php echo base_url().$item->image;?>" alt="" class="img-responsive">
								</a>
							<?php endforeach; ?>
						<?php endif; ?>		
					</div><!--banner-->
				</div><!--banner & question - answer-->
				<div id="competition" class="col-md-3 col-sm-3 col-xs-12">
					<?php $this->load->view('block/featured'); ?>
				</div><!--Competition-->				
			</div>
		</div><!--container banner and competition-->
		</div>	
	</div><!--Banner & competition-->

	<!-- ads center center -->
	<?php $this->load->view('block/ads_center_center');?>
	<!-- end-->
	<!-- start section khoa hoc-->

	<div class="khoa-hoc">
		<div class="container">			
			<?php 
				if (isset($courseItems) && count($courseItems) > 0):
					foreach ($courseItems as $category): 
			?>
			<div class="category-course">
				<div class="category-header">
					<h3><a href=""><?php echo $category->name;?></a></h3>
				</div>		
				<div class="items">
				<?php 
					if (isset($category->courses) && count($category->courses) > 0):
						$order = 0;
						foreach ($category->courses as $item):
				?>		
	  			<?php if(($order % 4) == 0) :?>
	  			<div class="row">
	  			<?php endif; $order++; ?>		
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="item">
							<div class="img-thumbnail">
								<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title="<?php echo $item->name; ?>">
									<img src="<?php echo base_url().$item->image;?>" alt="">
								</a>
												
							</div>			
							<div class="info">
								<h3><a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title=""><?php echo $item->name; ?></a></h3>
								<p class="khai_giang">Khai giảng: <?php echo date('d/m/Y', strtotime( $item->start_date )); ?></p>
							</div>
						</div>			
					</div><!--div item-->
				<?php if(($order % 4) == 0 || $order == count($category->courses)) :?>
					
				</div>
				<?php endif;?>				
				<?php 
						endforeach;
					endif;
				?>
				</div>
			</div>
			<?php 
					endforeach;
				endif;
			?>	
		</div>
	</div>
	<!-- ads center center -->
	<?php $this->load->view('block/ads_center_bottom');?>
	<!-- end-->
	<div id="comment-news">
		<div class="container">
			<div class="row">
				<div id="comment-about-me" class="col-md-5 col-sm-5 sol-xs-12">
					<div class="title-site title-news-comment">
						<h1>Học viên nói về chúng tôi</h1>
						<hr />
					</div>
					<?php if( !empty($testimonies) ): ?>
						<div id="item-commnent">
							<?php foreach( $testimonies as $item ) : ?>
							<div class="content-comment">
								<div class="img-student">
									<?php if( $item->image ): ?>
										<img src="<?php echo base_url().$item->image;?>" alt="Ảnh học viên">
									<?php else: ?>
										<img src="<?php echo base_url();?>assets/img/user.jpg" alt="Ảnh học viên">
									<?php endif; ?>
								</div>
								<div class="name-student">
									<p class="name"><?php echo $item->full_name; ?></p>
									<div class="info-student">
										<?php echo $item->description; ?>
									</div>
								</div>
								<div class="detail-content">
									<i class="fa fa-quote-left"></i>
									<p>
										<?php echo $item->content; ?>
									</p>
								</div>
							</div>
							<?php endforeach; ?>	
						</div>
					<?php endif; ?>		
				</div><!--comment about me-->
				<div id="activity" class="col-md-4 col-sm-4 col-xs-12 items-activity">
					<div class="title-site title-news-comment">
						<h1>Hoạt động mới nhất</h1>
						<hr />
					</div>
					<?php if( !empty($activityNew) ) : ?>
						<div class="box-activity">
							<?php foreach( $activityNew as $item ) : ?>
								<div class="user-activity">
									<div class="img-user-activity">
										<a href="" title="">
											<?php if($item->image) : ?>
												<img src="<?php echo base_url().$item->image;?>" alt="Ảnh Thành viên">
											<?php else: ?>
												<img src="<?php echo base_url();?>assets/img/user.jpg" alt="">
											<?php endif; ?>
										</a>	
									</div>
									<div class="content-activity">
										<?php echo $item->content; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>	
				</div><!--item activity-->
				<div class="col-md-3 col-sm-3 col-xs-12 home-video-intro">
					<div class="title-site title-news-comment">
						<h1>VNA</h1>
						<hr />
					</div>
					<?php if($video_intro) :
						$youtubeExp = explode('v=', $video_intro);
						$videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';
						$youtube = 'http://www.youtube.com/embed/'.$videoId.'?vq=hd720&fs=0&rel=0&showinfo=0&disablekb=1&enablejsapi=1';
					?>
					<div class="video-introduce">
						<iframe width="100%" height="100%" src="<?php echo $youtube;?>" frameborder="0" allowfullscreen></iframe>
					</div>
					<?php endif;?>
				</div>
			</div>
		</div><!--container news comment study-->
	</div><!--div comment and news-->
</section><!--session-->
<div id="examinationModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="survey-upload-form" accept-charset="utf-8" action="" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gửi bài khảo sát & đánh giá năng lực</h4>
                </div>
                <div class="modal-body">
                	<h2>Thông tin cá nhân:</h2>
                	<p class="support-text">(*) bắt buộc</p>
                	<div class="form-group">
                		<input type="text" name="full_name" class="form-control" placeholder="Họ và tên (*)" required="">
                	</div>
                	<div class="hr-line-dashed"></div>
                	<div class="form-group">
                		<input type="email" name="email" class="form-control" placeholder="Email (*)" required="">
                	</div>
                	<div class="hr-line-dashed"></div>
                	<div class="form-group">
                		<input type="phone" name="phone" class="form-control" placeholder="Số điện thoại">
                	</div>
                	<div class="hr-line-dashed"></div>
                	<div class="form-group">
                		<input type="text" name="school" class="form-control" placeholder="Trường">
                	</div>
                	<div class="hr-line-dashed"></div>
                	<div class="form-group">
                		<input type="text" name="class" class="form-control" placeholder="Lớp">
                	</div>
                	<h2>Tải bài làm:</h2>
                	<p class="support-text">Bài trả lời dạng ảnh jpg, png, jpeg, gif </p>
                    <div class="form-group">
                        <input type="file" name="answer_file_upload[]">
						<br/>
						<button type="button" id="add-answer-upload" class="btn btn-default btn-sm">Thêm file</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-send-survey" class="btn btn-info">Gửi</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Đóng</button>
                </div>
            </form>
        </div>
    </div>
</div>