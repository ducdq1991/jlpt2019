<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_model extends Base_model {

	public function __construct()
	{
		parent::__construct();
	}
	/**
	* [get exam online new]
	**/
	public function getExamsOnline( $where = array(), $limit = 0 , $orderBy = array() )
	{
		try {
			$this->db->select( 'exam.id, exam.title, exam.slug, exam.time, exam.publish_up, exam.publish_down, course_details.name' );
			$this->db->from('exam');
			$this->db->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			$this->db->where( array('exam.publish_up <=' => date('Y-m-d H:i:s') ));
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get();
			if ( $query ) {
				$result = $query->result();
				return $result;
			}
			return false;

		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}

	public function getCategoryCourse()
	{
		$query = $this->db->get('course_categories');
		if ($query) {
			$result = $query->result();
			return $result;
		}	
	}

	/**
	* [get courses new]
	**/
	public function getCourses( $where = array(), $limit = 0 , $orderBy = array() )
	{
		try {
			$this->db->select('courses.id, courses.image, course_details.name, course_details.slug, courses.start_date, course_details.price, course_details.origin_price, course_details.teacher');
			$this->db->from('courses');
			$this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
			if ( !empty($where) ) {
				$this->db->where( $where )
				->group_start()
					->where( array('courses.end_date !=' => 0))
					->where( array('courses.end_date >=' => date('Y-m-d') ) )
					->or_group_start()
					->where( array((int)'courses.end_date' => 0 ) )
					->group_end()
				->group_end();
			}
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get();
			if ( $query ) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get exam storage new]
	**/
	public function getExamsStorage( $where = array(), $limit = 0 , $orderBy = array() )
	{
		try {
			$this->db->select('exams_storage.id, exams_storage.title, exams_storage.slug');
			if ( !empty($where) ) {
				$this->db->where( $where );
			}
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get('exams_storage');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get document new]
	**/
	public function getDocuments( $where = array(), $limit = 0 , $orderBy = array() )
	{
		try {
			$this->db->select('documents.id, documents.title, documents.slug');
			if ( !empty($where) ) {
				$this->db->where( $where );
			}
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get('documents');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get testimonies]
	**/
	public function getTestimonies( $where = [], $limit = 0 )
	{
		try {
			$this->db->select('full_name, description, content, image');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			$this->db->limit( $limit );
			$query = $this->db->get('testimonies');
			if( $query ){
				return $query->result();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}
	/**
	* [get activity new]
	**/
	public function getActivitys( $where = [], $whereAct = [], $limit = 0, $orderBy = [] )
	{
		try {
			$this->db->select('users.image, users.username, user_logs.content');
			$this->db->from('user_logs');
			$this->db->join('users', 'user_logs.user_id = users.id','left');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			if( !empty($whereAct) ){
				$this->db->where_in('user_logs.type', $whereAct);
			}
			$this->db->limit( $limit );
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get();
			if( $query ){
				return $query->result();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}

	public function insertExamination($args = [])
	{
		try {
			return $this->db->insert( 'examinations' , $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
}