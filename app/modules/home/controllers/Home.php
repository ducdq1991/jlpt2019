<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Base_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('index_model');	
		$this->load->model('article/article_model');
	}
	
	public function index()
	{
	    $data = $head = [];
	    $head['metas'] = $this->getMetas();

	    $courseCategories = $this->index_model->getCategoryCourse();
	    $courseItems = [];
	    foreach ($courseCategories as $cate) {
		    $where = ['parent' => 0, 'courses.status' => 1, 'courses.category_id' => $cate->id];	    
		    $order = ['key' => 'courses.ordering', 'value' => 'ASC'];
		    $courses = $this->index_model->getCourses($where, $limit = 8, $order);
		    $cate->courses = $courses;
		    $courseItems[] = $cate;	    	
	    }
	    $data['courseItems'] = $courseItems;

	    //Home Slider
	    $type = TYPE_SLIDER;
	    $position = TOP_DOWN;
	    $slider = $this->getBanners($position, $type, $limit = 10);
	    if (!empty($slider)) {
	    	$data['slider'] = $slider;
	    }

	    //get banner bottom
	    $typeBanner = TYPE_BANNER;
	    $positionBanner = BOTTOM;
	    $bannerBottom = $this->getBanners($positionBanner, $typeBanner, $limit = 1);
	    if (!empty($bannerBottom)) {
	    	$data['bannerBottom'] = $bannerBottom[0];
	    }

	    //get banner bottom
	    $typeBanner = TYPE_ADS;
	    $positionBanner = TOP_DOWN;
	    $bannerTopDown = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
	    if (!empty($bannerTopDown)) {
	    	$data['bannerTopDown'] = $bannerTopDown[0];
	    }
	    //get banner center center
	    $typeBanner = TYPE_ADS;
	    $positionBanner = CENTER_CENTER;
	    $bannerCenterCenter = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
	    if (!empty($bannerCenterCenter)) {
	    	$data['bannerCenterCenter'] = $bannerCenterCenter[0];
	    }
	    //get banner center bottom 
	    $typeBanner = TYPE_ADS;
	    $positionBanner = CENTER_BOTTOM;
	    $bannerCenterBottom = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
	    if (!empty($bannerCenterBottom)) {
	    	$data['bannerCenterBottom'] = $bannerCenterBottom[0];
	    }
	    //get banner position 3
	    $typeBanner = TYPE_ADS;
	    $positionBanner = POSITION_3;
	    $bannerPosition3 = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
	    if (!empty($bannerPosition3)) {
	    	$data['bannerPosition3'] = $bannerPosition3[0];
	    }

	    //get testimonies
	    $where = ['status' => 1];
	    $testimonies = $this->index_model->getTestimonies($where, $limit = 20);
	    $data['testimonies'] = $testimonies;

	    //get activity new
	    $where = ['users.status' => 1];
	    $whereIn = [VIEW_LESSON, DO_EXAM, DO_EXERCISE, REGISTER, SEND_ANSWER, UPLOAD_ANSWER];
	    $order = ['key' => 'user_logs.updated', 'value' => 'DESC'];
	    $activities = $this->index_model->getActivitys($where, $whereIn, $limit = 5, $order);
	    $data['activityNew'] = $activities;

		$featuredPosts = $this->article_model->articles(['featured' => 1, 'status' => 'publish']);
		$data['featuredPosts'] = $featuredPosts;
		$lastestPosts = $this->article_model->articles(['status' => 'publish'], 30);
		$data['lastestPosts'] = $lastestPosts;
		//get video intro
		$data['video_intro'] = $this->getVideo();
		$head['mainMenu'] = $this->_head['mainMenu'];

    	//load view
    	$this->load->view('layouts/header', $head);
    	$this->load->view('home/index', $data);
    	$this->load->view('layouts/footer', $this->_footer_menu);
	}
}
