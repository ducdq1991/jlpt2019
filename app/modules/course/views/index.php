<div id="courses-module" class="module module-course">
  <div class="container">
  	<div class="row">
	  	<h1 class="page-title">Khóa học</h1>
	  	<div  id="list-courses" class="list-item">
	  		<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php $order = 0;?>
	  		<?php foreach ( $courses as $item ) : ?>
	  			<?php if(($order % 4) == 0) :?>
	  			<div class="row">
	  			<?php endif; $order++; ?>
				<div class="item col-md-3 col-sm-6 col-xs-12">
					<div class="box">
						<div class="img-thumbnail">
							<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title="<?php echo $item->name; ?>">
								<img src="<?php echo base_url().$item->image;?>" alt="">
							</a>					
						</div>				
						<div class="info">
							<h2><a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>" title=""><?php echo $item->name; ?></a></h2>
							<?php if ($item->title != NULL) :?>
							<h3 class="sub-title"><?php echo $item->title; ?></h3>
							<?php endif;?>
							<p>Khai giảng: <?php echo date('d/m/Y', strtotime( $item->start_date )); ?></p>
							<p>Học phí: <span class="c_price"><?php echo number_format($item->price);?></span> <sup>đ</sup></p>
						</div>					
					</div>			
				</div><!--div item-->
				<?php if(($order % 4) == 0 || $order == count($courses)) :?>
					
				</div>
				<?php endif;?>
			<?php endforeach; ?>	
			<div class="pagination"><?php echo $paging; ?></div><!--pagination-->
		</div>
	</div>
  </div><!--container-->
</div>  	