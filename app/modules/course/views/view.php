<?php $userSession = $this->session->userdata('web_user');?>
<div id="courses-module">
    <div class="container">
        <div class="row">
            <div  id="detail-courses" class="list-item col-md-8 col-sm-8 col-xs-12">
                <h1 class="title"><i class="fa fa-file-text-o"></i> <?php echo $course->name; ?></h1>
                <?php if( $this->session->flashdata('success') ) : ?>
                <div class="alert alert-sm alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                <div class="alert alert-sm alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                <div class="alert alert-sm alert-warning alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('warning'); ?>
                </div>
                <?php endif; ?>
                <div class="description-course">
                    <div class="head-content">
                        <?php if ($course->image) :?>
                        <div class="img col-lg-5 col-md-5">
                            <img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
                        </div>
                        <?php endif;?>
                        <div class="info-course col-lg-7 col-md-7">
                            <div class="info-detail">
                                <p class="teacher">
                                    <label>Giáo viên: </label>
                                    <span class="name-teacher">
                                    <?php echo $course->teacher; ?>
                                    </span>
                                </p>

                                <?php if( $course->child_course > 0 ):?>
                                <p class="topical-count">
                                    <label>Số chuyên đề: </label>
                                    <span class="number-count"><?php echo $course->child_course;?></span>
                                </p>
                                <?php endif; ?>

                                <?php if ($course->total_lesson > 0 ) : ?>
                                <p>
                                    <label>Số bài giảng: </label>
                                    <span class="number-count"><?php echo $course->total_lesson;?></span>
                                </p>
                                <?php endif;?>

                                <p class="date-start">
                                    <label>Khai giảng: </label>
                                    <span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
                                </p>

                                <?php if($course->end_date !=0) : ?>
                                <p class="date-end">
                                    <label>Kết thúc: </label>
                                    <span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
                                </p>
                                <?php endif; ?>
                                
                                <!--Kiem tra khoa hoc co phi hay Mien phi-->
                                <?php if ($price == 0) : ?>
                                    <h3 class="" style="font-weight:700;color:#EF0D4E;font-size: 20px;">Miễn phí</h3>
                                <?php else:?>
                                    <h3 class="" style="font-weight:700;color:#EF0D4E;font-size: 20px;">
                                    Học phí: <?php echo number_format($price, 0). ' đ'; ?></h3>
                                <?php endif;?>
                                <!--END Kiem tra khoa hoc co phi hay Mien phi-->

                                <?php if ($course->title != NULL) :?>
                                <h3 class="sub-title" style="font-style: italic;"><?php echo $course->title; ?></h3>
                                <?php endif;?>
                                <!--Neu chua nop hoc phi hoac chua dang nhap-->	
                                <div class="btn-courses">
                                <!--Kiem tra da nop hoc phi-->
                                <?php if (isset($userSession) && $isBuyed === true && $price > 0) :?>
                                    <a href="#" title="" class="btn btn-success">Đã nộp học phí</a>
                                <?php endif;?>

                                <?php if ((isset($userSession) && $isBuyed === false) || !isset($userSession)) :?>                                
                                    <?php if ($price > 0) :?>
                                    <a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $rootCourseId;?>" data-price="<?php echo $price;?>" data-toggle="modal" class="btn btn-danger">
                                    <i class="fa fa-money"></i> Nộp học phí
                                    </a>
                                    <?php endif;?>                  
                                <?php endif;?>
                                    <a href="#course-content" data-toggle="modal" class="btn btn-success"><i class="fa fa-info"></i> Nội dung khóa học</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
            <div id="competition" class="col-md-4 col-sm-4 col-xs-12">
                <?php $this->load->view('block/ads_right_top'); ?>
                <?php                 
                if (isset($course->intro_video) && !is_null($course->intro_video)) : 
                    $videoId = @end(explode('?v=', $course->intro_video));
                ?>
                <div class="intro-video text-center">
                    <h3>Giới thiệu khóa học</h3>
                    <iframe width="340" height="305" src="https://www.youtube.com/embed/<?php echo $videoId;?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <?php endif;?>
            </div>

            <div class="course-blocks row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="course-box cb1">
                            <h3><a href="<?php echo base_url();?>khoa-hoc/<?php echo $course->slug;?>/chuyen-de">Bài giảng</a></h3>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="course-box cb2">
                            <h3><a href="<?php echo base_url();?>luyen-tap/chuyen-de/<?php echo $course->slug;?>">Thi Online</a></h3>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="answer-question">
                            <h3>Các em có thể tìm lời giải theo ID câu hỏi tại đây</h3>
                            <form action="<?php echo base_url();?>loi-giai" method="GET" accept-charset="utf-8">
                                <div class="input-group">                                  
                                    <input type="text" placeholder="Nhập ID câu hỏi" class="form-control form-question" name="question_id" aria-describedby="basic-addon1">                                  
                                  <span class="input-group-addon" id="basic-addon1"><input type="submit" value="Xem lời giải" class="btn btn-answer"></span>
                                </div>                                
                            </form>
                        </div><!--question & answer-->
                    </div>
                </div>                                                 
            </div>
            <div class="c-content-detail" style="display: none;">
                <?php echo nl2br(htmlentities($course->description)); ?>
            </div>

        </div>
    </div>
    <!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
    <div class="modal-dialog edit-modal">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nộp học phí</h4>
            </div>
            <div class="modal-body">
                <p class="price-text"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info purchase" data-dismiss="modal">Đồng ý</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
            </div>
        </div>
    </div>
</div>
<!--modal info lesson-->

<div class="modal fade" id="course-content" tabindex="-1" role="dialog">
    <div class="modal-dialog edit-modal modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nội dung khóa học</h4>
            </div>
            <div class="modal-body">
                <div class="content-course">
                    <p><?php echo nl2br(htmlentities($course->description)); ?></p>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
            </div>
        </div>
    </div>
</div>
<!--modal info lesson-->