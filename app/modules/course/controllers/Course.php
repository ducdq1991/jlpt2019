<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('breadcrumbs');
		$this->load->model('exam/exam_model');
		$this->load->model('course_model');		
	}

	/*
		Danh sach tat ca cac khoa hoc
	*/
	public function index( $page = 0 )
	{
		$this->setCurrentUrl();		
		$head = $data = $where = $orderBy = [];
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'khoa-hoc');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Khóa học';
		
		$limit = 20;
		$where['courses.parent'] = 0;
		$where['courses.status'] = 1;

       	$this->load->helper('paging');
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
        	$page = 1;
        }
		$offset = ($page - 1) * $limit;

		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];

		$total = $this->course_model->totalCourses($where);
		$courses = $this->course_model->getCourses($where, $limit, $offset, $orderBy);
        $data['courses'] = $courses;
		$data['paging'] = paging( base_url() .'khoa-hoc', $page, $total, $limit );

		$this->load->view('layouts/header', $head);
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function course($slug = '')
	{		
		$webUser = $this->session->userdata('web_user');
		$userId = isset($webUser) ? $webUser->id : null;		
		if ($slug == '') {
			redirect(base_url('khoa-hoc'));
		}

		$this->setCurrentUrl();
		
		$price = 0;
		$isBuyed = false;		
		$data = $where = $orderBy = $head = [];

		$where['course_details.slug'] = $slug;
		$where['courses.status'] = 1;
		$course = $this->course_model->getCourseBySlug($where);
		if (empty($course)) {
			redirect(base_url('notfound'));
		}

		//Danh sach id cac khoa hoc va chuyen de
		$ids = $this->course_model->getcourseIds($course->id);	
		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];		

		$courseId = $course->id;	
		$data['course'] = $course;

		//Breadcrumbs
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url() . 'khoa-hoc');
        if (!empty($rootParents)) {
        	$this->breadcrumbs->add(ucfirst($rootParents->name), base_url() . 'khoa-hoc/' . $rootParents->slug);
       	}
        $this->breadcrumbs->add(ucfirst($course->name), base_url() . 'khoa-hoc/'. $course->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //SEO
        $metaGeneral = $this->getMetas();
        $metas = [
        	'meta_keyword' => !empty($course->meta_keywords) ? $course->meta_keywords : $metaGeneral['meta_keyword'],
        	'meta_description' => !empty($course->meta_description) ? $course->meta_description : $metaGeneral['meta_description'],
        ];
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = 'Khóa học | ' . $course->name;


        //Sidebar Tin tuc moi nhat        
        $data['lastestPosts'] = $featrured['lastestPosts'];

        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('view', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function sub($slug = '')
	{				
		if ($slug == '') {
			redirect(base_url('khoa-hoc'));
		}
		$this->setCurrentUrl();

		$webUser = $this->session->userdata('web_user');
		$userId = isset($webUser) ? $webUser->id : null;
		
		$price = 0;
		$isBuyed = false;		
		$data = $where = $orderBy = $head = [];

		$where['course_details.slug'] = $slug;
		$where['courses.status'] = 1;
		$course = $this->course_model->getCourseBySlug($where);

		if (empty($course)) {
			redirect(base_url('notfound'));
		}

		//Danh sach id cac khoa hoc va chuyen de
		$ids = $this->course_model->getcourseIds($course->id);	
		$wherePractice = [
			'exam.type' => 'exercise',
			'exam.attribute' => 1,
			'exam.status' => 1,
		];	
		$orderBy = [
			'key' => 'ordering',
			'value' => 'ASC',
		];
		// Danh sach bai luyen tap trong khoa hoc
		$exams = $this->exam_model->getExercise($wherePractice, ['key' => 'exam.course_id', 'value' => $ids], 0, 0, 'FIELD(exam.course_id,' . implode(',', $ids) . '), exam.ordering ASC');
		
		$data['exams'] = $exams;		

		$courseId = $course->id;	
		$data['course'] = $course;
		$parentId = $course->parent;

		if ($parentId > 0) {
			$rootParents = $this->course_model->getParents($parentId);
			$rootId = $rootParents->id;
		} else {
			$rootId = $course->id;
		}

		$totalCourseCode = $this->course_model->hasCode($rootId);
		$data['totalCourseCode'] = $totalCourseCode;
		
		$price = $course->price;		
		$data['rootCourseId'] = $rootId;		

		if (!empty($rootParents)) {
			$data['parent'] = $rootParents;			
			if ($rootParents->price > 0) {
				$price = $rootParents->price;
			}
			if (isset($userId)) {
				$isBuyed = $this->isBuyed($userId, $courseId);	
			}
			
			$parentId = $rootParents->parent;
		}

		$isBuyed = $this->isBuyed($userId, $rootId);
		$data['price'] = $price;
		$data['isBuyed'] = $isBuyed;	

		//count course lesson
	    $whereCourse = ['parent' => (int) $course->id, 'status' => 1];
	    $totalTopical = $this->course_model->totalCourses($whereCourse);
	    $course->child_course = $totalTopical;
	    $courseIds = $this->course_model->getChildCourseId($courseId);
	    $courseIds[] = (int) $courseId;
	    $whereLessonIn = [
	    	'key' => 'course_id',
	    	'value' => $courseIds
	    ]; 
	    
	    $totalLesson = $this->course_model->totalLessons([], $whereLessonIn);
	    $course->total_lesson = $totalLesson;

		//Breadcrumbs
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url() . 'khoa-hoc');
        if (!empty($rootParents)) {
        	$this->breadcrumbs->add(ucfirst($rootParents->name), base_url() . 'khoa-hoc/' . $rootParents->slug);
       	}

        $this->breadcrumbs->add(ucfirst($course->name), base_url() . 'khoa-hoc/'. $course->slug);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //SEO
        $metaGeneral = $this->getMetas();
        $metas = [
        	'meta_keyword' => !empty($course->meta_keywords) ? $course->meta_keywords : $metaGeneral['meta_keyword'],
        	'meta_description' => !empty($course->meta_description) ? $course->meta_description : $metaGeneral['meta_description'],
        ];
        $head['metas'] = $metas;
        $head['metas']['meta_title'] = 'Khóa học | ' . $course->name;

        //list topical with course
        if ($course->child_course > 0) {
        	$orderBy['key'] = 'courses.ordering';
        	$orderBy['value'] = 'ASC';
        	$topical = $this->course_model->getCourses($whereCourse, $limit = 0, $offset = 0, $orderBy);
			foreach ($topical as $key=>$value) {
			    $where = ['course_id' => (int) $value->id]; 
			    $totalLesson = $this->course_model->totalLessons($where);			    
			    $topical[$key]->total_lesson = $totalLesson;
			}
			$data['topicAndLesson'] = $topical;
        }

        if ($course->child_course == 0) {
        	$orderBy['key'] = 'lessons.ordering';
        	$orderBy['value'] = 'ASC';
        	$where = ['course_id' => (int) $courseId];
        	$lessons = $this->course_model->getLessons($where, $limit = 0, $orderBy);
        	$data['topicAndLesson'] = $lessons;
        }
                
        $data['lastestPosts'] = $featrured['lastestPosts'];

        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('sub', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}


	public function isBuyed($userId = 0, $courseId = 0)
	{
		$where = [
			'course_id' => $courseId,
			'user_id' => $userId
		];
		$result = $this->course_model->getUserCourse($where);
		return $result;
		
	}

	public function canView($courseId = 0, $price = 0, $userId = 0, $slug = '')
	{
		$where = [
			'user_id' => (int) $userId,
			'course_id' => (int) $courseId,
		];	
		if (!$this->course_model->existUserCourse($where)) {
			$this->session->set_flashdata('error', 'Để xem bài giảng này hãy mua khóa học!');
			if ($slug != '') {
				redirect(base_url().'khoa-hoc/' . $slug);
			} else {
				redirect(base_url('khoa-hoc'));	
			}		
		}

		return true;
	}

	/**
	* [detail course]
	**/
	public function detail($slug = '')
	{
		$this->setCurrentUrl();

		if ($slug == '') {			
			redirect(base_url() . 'khoa-hoc');
		}

		$user = $this->session->userdata('web_user');

		if (!$user) {
			$this->session->set_flashdata('error', 'Bạn cần đăng nhập để tham gia khóa học');
			if($course) {
				redirect(base_url().'khoa-hoc/'.$course->slug);
			}
			redirect(base_url('khoa-hoc'));
		}

		$price = 0;
		$data = $where = $orderBy = [];

		//Lay bai giang
		$where['slug'] = $slug;
		$where['status'] = 1;
		$lesson = $this->course_model->getLessons($where, $limit = 1, $orderBy)[0];
		if (empty($lesson)) {
			$this->session->set_flashdata('error', 'Bài giảng không tồn tại');
			redirect(base_url('khoa-hoc'));
		}

		// Bai giang khong truoc phep truy cap		
		if ($lesson->access == 0) {
			$course = $this->course_model->getParents($lesson->course_id);
			if (!is_null($course) || !empty($course)) {
				$price  = $course->price;				
				if ($price > 0) {
					if ($this->canView($course->id, $price, $user->id, $course->slug)) {
						$data['isBuyed'] = true;

						$args = [
							'lesson_id' => $lesson->id,
							'user_id' => $user->id
						];
						
						//Check view count by lesson_id
						$where = [
							'lesson_id' => $lesson->id, 
							'course_id' => $course->id,
							'user_id' => $user->id
						];
						$totalViewed = $this->course_model->viewedCount($where);
						if ($totalViewed >= $course->lesson_view_limit) {
							$this->session->set_flashdata('warning', 'Lượt xem đã vượt quá giới hạn cho phép!');
							redirect(base_url('khoa-hoc/' . $course->slug));
						}

						$this->course_model->updateViewLimit($course->id, $args);
					}
				}
			}			
		}

		$data['price'] = $price;

		// Ghi lich su truy cap
		if (!empty($user)) {
			$name = !empty($user->full_name) ? $user->full_name : $user->username;
			$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> xem bài giảng: <a href="'.base_url().'khoa-hoc/'.$lesson->slug.'.html">'.$lesson->title.'</a>';
			$this->generateLog(VIEW_LESSON, $user->id, (int) $lesson->id, $content );
		}

		$data['lesson'] = $lesson;

		//Lay bai giang trong khoa hoc hoac chuyen de		
		$orderBy['key'] = 'lessons.ordering';
        $orderBy['value'] = 'ASC';
        $where = [
        	'course_id' => $lesson->course_id
        ];
		$lessonsCourse = $this->course_model->getLessons($where, $limit = 0, $orderBy);
		$data['lessonsCourse'] = $lessonsCourse;

		//create breadcrumb
		$head = [];
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'khoa-hoc');
        if (!empty($course)) {
        	$this->breadcrumbs->add(ucfirst($course->name), base_url().'khoa-hoc/' . $course->slug );
        }
        $this->breadcrumbs->add( ucfirst($lesson->title), base_url().'khoa-hoc' . $lesson->slug );
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //Thiet lap Meta SEO
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = $lesson->title;

		$this->load->view('layouts/header', $head);
		$this->load->view('detail_lesson', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function checkCode()
	{
		$user = $this->session->userdata('web_user');
		if (empty($user) && !isset($user->id)) {            
            echo 3; exit;    
        }

		$postData = $this->input->post();
		if (!empty($postData)) {
			if (isset($postData['courseId']) && isset($postData['code'])) {
				$courseId = (int) $postData['courseId'];
				$course = $this->course_model->getParents($courseId);
				if (!empty($course)) {
					$where = [
						'course_codes.course_id' => $courseId,
						'course_codes.code' => $postData['code'],
					];					
					$code = $this->course_model->getCode($where);
					if (!is_null($code)) {
						echo 1; exit;
					} else {
						echo 2; exit;
					}
				}
			}
		} else {
			echo 4; exit;
		}
	}

	public function purchaseCourse()
	{
		$user = $this->session->userdata('web_user');
		if (empty($user) && !isset($user->id)) {            
            $this->session->set_flashdata('error', 'Bạn chưa đăng nhập');
            die;     
        }

        $args = [];

        //get user
		$userInfo = $this->course_model->getUser(['id' => (int) $user->id]);
		$amount = (float) $userInfo->amount;

		//get course
		$courseId = $this->input->post('id');
		if ($courseId != NULL) {
			$where = ['courses.id' => (int) $courseId];
			$course = $this->course_model->getCourseBySlug($where);
			$price = (float) $course->price;
			if( $amount < $price ){			
				$this->session->set_flashdata('error', 'Tài khoản của em không đủ mua khóa học. Em cần <a href="/thanh-toan"><strong>NẠP TIỀN</strong></a> để có thể mua Khóa học em nhé!');
				echo 10;
				die;
			}
			$where = [
				'user_id' => (int) $userInfo->id,
				'course_id' => (int) $course->id,
			];
			if($this->course_model->existUserCourse($where)) {
				$this->session->set_flashdata('warning', 'Bạn đã nộp học phí khóa học này rồi');
				echo 1;
				die;
			}

			$args = ['amount' => (float) ($amount - $price)];
			$updateAmount = $this->course_model->updateUser($user->id, $args);
			if ($updateAmount) {
				$args = [
					'user_id' => (int) $userInfo->id,
					'course_id' => (int) $course->id,
					'cost' => (float) $course->price,
					'created' => date('Y-m-d H:i:s'),
				];
				if($this->course_model->insertUserCourse($args)) {
					$contentMessage = 'Bạn nộp học phí thành công! Khóa học: '.$course->name.', với giá: '.$course->price . 'vnđ';
					$this->session->set_flashdata('success', $contentMessage);

					$name = (!empty($user->full_name)) ? $user->full_name : $user->username;
					$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> mua khóa học: <a href="'.base_url().'khoa-hoc/'.$course->slug.'" title="">'.$course->name.'</a>';
					$this->generateLog(PURCHASE_COURSE, (int) $user->id, (int) $course->id, $content );

					$payments = [];
					$payments['name'] = 'Mua khóa học ' . $course->name;
					$payments['receiver'] = $course->manager_id;
					$payments['payer'] = (int) $user->id;
					$payments['created_time'] = date('Y-m-d H:i:s');
					$payments['content'] = $content;
					$payments['type'] = 'course';
					$payments['price'] = (int) $course->price;
					$payments['data_id'] = (int) $course->id;
					$this->course_model->addCoursePayment($payments);

				} else {
					$this->session->set_flashdata('error', 'Hệ thống: Chưa thể lưu, nếu tiền đã bị trừ xin liên hệ admin để giải quyết');
				}
			} else {
				$this->session->set_flashdata('error', 'Chưa nộp được học phí');
			}
			die;
		} else {
			$this->session->set_flashdata('error', 'Chưa nộp được học phí');
			die();
		}
	}
}