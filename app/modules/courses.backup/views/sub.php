<?php $userSession = $this->session->userdata('web_user');?>
<div id="courses-module">
  <div class="container">
  	<div class="row">
	  	<div  id="detail-courses" class="list-item col-md-8 col-sm-8 col-xs-12">
			<h1 class="title"><i class="fa fa-file-text-o"></i> <?php echo $course->name; ?></h1>
			<?php if( $this->session->flashdata('success') ) : ?>
	            <div class="alert alert-sm alert-success alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('success'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('error') ) : ?>
	            <div class="alert alert-sm alert-danger alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('error'); ?>
	            </div>
	        <?php endif; ?>
	        <?php if( $this->session->flashdata('warning') ) : ?>
	            <div class="alert alert-sm alert-warning alert-dismissable">
	                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                <?php echo $this->session->flashdata('warning'); ?>
	            </div>
	        <?php endif; ?>
			<div class="description-course">
				<div class="head-content">									
					<div class="img col-lg-5 col-md-5">
						<?php if (isset($parent)) :?>
							<img src="<?php echo base_url().$parent->image;?>" alt="<?php echo $parent->name; ?>">	
						<?php else :?>
							<img src="<?php echo base_url().$course->image;?>" alt="<?php echo $course->name; ?>">	
						<?php endif;?>
					</div>					
					<div class="info-course col-lg-7 col-md-7">					
						<div class="info-detail">
							<p class="teacher">
								<label>Giáo viên: </label>
								<span class="name-teacher">
									<?php echo $course->teacher; ?>
								</span>
							</p>
							<?php if( $course->child_course > 0 ):?>
							<p class="topical-count">
								<label>Số chuyên đề: </label>
								<span class="number-count"><?php echo $course->child_course;?></span>
							</p>
							<?php endif; ?>
							<?php if ($course->total_lesson > 0 ) : ?>	
							<p>
								<label>Số bài giảng: </label>
								<span class="number-count"><?php echo $course->total_lesson;?></span>
							</p>
							<?php endif;?>
							<p class="date-start">
								<label>Khai giảng: </label>
								<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->start_date )); ?></span>
							</p>
							<?php if($course->end_date !=0) : ?>
							<p class="date-end">
								<label>Kết thúc: </label>
								<span class="date-detail"><?php echo date('d/m/Y', strtotime( $course->end_date )); ?></span>
							</p>
							<?php endif; ?>

                            <!--Kiem tra khoa hoc co phi hay Mien phi-->
                            <?php if ($price == 0) : ?>
                                <h3 class="" style="font-weight:700;color:#CF5138;font-size: 20px;">Miễn phí</h3>
                            <?php else:?>
                                <h3 class="" style="font-weight:700;color:#CF5138;font-size: 20px;">
                                Học phí: <?php echo number_format($price, 0). ' đ'; ?></h3>
                            <?php endif;?>
                            <!--END Kiem tra khoa hoc co phi hay Mien phi-->

                            <?php if ($course->title != NULL) :?>
                            <h3 class="sub-title" style="font-style: italic;"><?php echo $course->title; ?></h3>
                            <?php endif;?>
                            <!--Neu chua nop hoc phi hoac chua dang nhap-->	
                            <div class="btn-courses">
                            <!--Kiem tra da nop hoc phi-->
                            <?php if (isset($userSession) && $isBuyed === true && $price > 0) :?>
                                <a href="#" title="" class="btn btn-success">Đã nộp học phí</a>
                            <?php endif;?>

                            <?php if ((isset($userSession) && $isBuyed === false) || !isset($userSession)) :?>                                
                                <?php if ($price > 0) :?>
                                <a id="btn-purchase" href="#purchaseModal" data-id="<?php echo $rootCourseId;?>" data-price="<?php echo $price;?>" data-toggle="modal" class="btn btn-danger">
                                <i class="fa fa-money"></i> Nộp học phí
                                </a>
                                <?php endif;?>                  
                            <?php endif;?>                                
                            </div>
						</div>
					</div>
				</div>
			</div><!--content course-->
			<div class="topical-lesson">
				<h1>
					<?php 
						if( $course->child_course > 0 ) {
							echo '<i class="fa fa-clipboard"></i> Các chuyên đề trong khóa học';
						} else {
							echo '<i class="fa fa-clipboard"></i> Các bài giảng trong khóa học';
						}
					?>
				</h1>				
				<div class="list-topical-lesson scrollbar">
					<?php if( !empty($topicAndLesson) ) :?>
						<?php 
							$stt = 1; 
							foreach( $topicAndLesson as $item ):
						?>
							<div class="item">
								<h3>								
									<?php if( $course->child_course > 0 ) : ?>
										<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug; ?>/chuyen-de" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->name;?>
										</a>
									<?php else: ?>		
										<?php if ($item->status == 1) : ?>
											<?php if ($totalCourseCode > 0) :?>
											<a class="item_code_checker" data-toggle="modal" href="#codeChecker" course-id="<?php echo $course->id;?>" lesson-slug="<?php echo $item->slug; ?>">
												<span class="stt"><?php echo $stt; ?>. </span>
												<?php echo $item->title;?>
											</a>
											<?php else :?>
												<a href="<?php echo base_url()?>khoa-hoc/<?php echo $item->slug;?>.html" class="<?php if($item->access == 0) echo 'logged';?>" title="">
													<span class="stt"><?php echo $stt; ?>. </span>
													<?php echo $item->title;?>
												</a>
											<?php endif;?>
										<?php else:?>
										<a  class="<?php if($item->access == 0) echo 'logged';?>" title="">
											<span class="stt"><?php echo $stt; ?>. </span>
											<?php echo $item->title;?>
										</a>										
										<?php endif;?>
									<?php endif; ?>	
								</h3>
								<div class="info">
									<span class="number-lesson">
										<?php										
											if ($course->child_course > 0 && $item->total_lesson > 0) {
												echo 'Số bài: '. $item->total_lesson;
											}
										?>
									</span>
									<?php if ($item->status == 0):?>
									<span>Chưa phát hành</span>
									<?php else: ?>
									<span class="date-start">Ngày phát hành: 
										<?php 
											if( $course->child_course > 0 ) {
												echo date('d/m/Y', strtotime( $item->start_date ));
											} else {
												echo  date('d/m/Y', strtotime( $item->created ));
											}
										?>
									</span>
									<?php endif;?>
									<?php if( $course->child_course == 0 ) : ?>
										<span class="document-lesson">
											<?php if( !empty($item->file) ):?>
												<?php if ((($isBuyed === true) || ($price == 0)) && $this->session->has_userdata('web_user') && $item->status == 1) : ?> 
												 <i class="fa fa-file-pdf-o"></i> Tải tài liệu 
												 <a href="<?php echo base_url().$item->file;?>" class="download-lesson" download> tại đây</a>
												<?php endif;?>
											<?php endif; ?>	
										</span>
									<?php endif; ?>	
								</div>
							</div>
						<?php $stt++; endforeach; ?>
					<?php else: ?>		
						<div class="item">
							<h3>Chuyên đề / Bài giảng chưa được cập nhật</h3>
						</div>
					<?php endif;?>	
				</div><!--list topical or lesson-->
			</div><!--topical and lesson with course-->
	  	</div>
	  	<div id="competition" class="col-md-4 col-sm-4 col-xs-12">
	  		<?php $this->load->view('block/ads_right_top'); ?>
			<div class="box-competition featured_widget_exam course-practice">
				<ul class="nav nav-tabs">
				  <li class="active" style="float: inherit;"><a data-toggle="tab"><i class="fa fa-newspaper-o"></i> Bài luyện trong khóa học</a></li>
				</ul>
				<div class="tab-content">
				<?php if ($exams) :?>
				  <div id="news" class="practice-items scrollbar tab-pane fade in active content-competition">
				  	<ul>
						<?php 
						if(count($exams) > 0) :

							foreach ($exams as $exam) :?>
				  		<li>
				  			<a class="logged" href="<?php echo base_url();?>luyen-tap/<?php echo $exam->slug;?>.html"><?php echo $exam->title;?></a>
				  		</li>
				  		<?php 
				  			endforeach;
				  		endif;
				  		?>
				  	</ul>
				  </div>
				<?php else :?>
					<p>Chưa có bài luyện</p>
				<?php endif;?>
				</div>
			</div><!--box competition-->
	  	</div>	
  	</div>
  </div><!--container-->
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nộp học phí</h4>
			</div>
			<div class="modal-body">
				<p class="price-text"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info purchase" data-dismiss="modal">Đồng ý</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Đóng lại</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->  	


<div class="modal fade" id="codeChecker" tabindex="-1" role="dialog">
	<div class="modal-dialog edit-modal">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><i class="fa fa-tags"></i> Nhập mã khóa học</h4>
			</div>
			<div class="modal-body">
				<input type="text" name="course_code_checker" id="course_code_checker" class="form-control" placeholder="Mã khóa học được phát kèm sách">
				<input type="hidden" name="rootCourseId" id="rootCourseId" value="<?php echo $rootCourseId;?>">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info btn-check-code" data-dismiss="modal">Xác nhận</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
			</div>
	    </div>
	</div>
</div><!--modal info lesson-->  