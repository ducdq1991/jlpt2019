<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('baokim');
		$this->load->model('user/user_model');
		$this->load->library('breadcrumbs');
	}

    public function refresh(){
    	$this->load->helper('captcha');
        // Captcha configuration
        $config = array(
		        'word' => '',
		        'img_path' => 'upload/captcha/',
		        'img_url' => base_url().'upload/captcha/',
		        'img_width' => 150,
		        'img_height' => 30,
		        'expiration' => 120,
				'word_length'  => 3,
		        'font_size'     => 40,
                'colors'        => array(
                    'background' => 'red',
                    'border' => array(100, 100, 100),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaWord');
        $this->session->set_userdata('captchaWord',$captcha['word']);       
        // Display captcha image
        echo $captcha['image'];
    }

	public function index()
	{
		redirect(base_url('noi-dung/huong-dan-thanh-toan.html'));            
		$data = [];
		$this->load->library('form_validation');
		$this->load->helper('captcha');
		$user = $this->session->userdata('web_user');
		if (empty($user) && !isset($user->id)) {            
            redirect(base_url('noi-dung/huong-dan-thanh-toan.html'));            
		}

		$userId = $user->id;		
	    $config = [
			[
			'field'  => 'network',
			'label'  => 'Nhà mạng',
			'rules'  => 'required',
			'errors' => [
				'required'   => 'Vui lòng chọn  %s.',
				]
			],
			[
			'field'  => 'pinNumber',
			'label'  => 'số Pin',
			'rules'  => 'required',
			'errors' => [
				'required'   => 'Vui lòng chọn  %s.',
				]
			],
			[
			'field'  => 'serieNumber',
			'label'  => 'Số Serie',
			'rules'  => 'required',
			'errors' => [
				'required'   => 'Vui lòng chọn  %s.',
				]
			],
			/*[
			'field'  => 'captcha',
			'label'  => 'Mã xác nhận',
			'rules'  => 'trim|required|callback_validCaptcha',
			'errors' => [
				'required'   => 'Bạn chưa điền %s.',
				]
			],*/										    
	    ];
		$this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

			//captcha
			$configCaptcha = array(
		        'word' => '',
		        'img_path' => 'upload/captcha/',
		        'img_url' => base_url().'upload/captcha/',
		        'img_width' => 180,
		        'img_height' => 30,
		        'expiration' => 120,
				'word_length'  => 3,
		        'font_size'     => 40,
                'colors'        => array(
                    'background' => 'red',
                    'border' => array(100, 100, 100),
                    'text' => array(0, 0, 0),
                    'grid' => array(255, 255, 255)
                )
			);
			$captcha = create_captcha($configCaptcha); 
			$data['codeCaptcha'] = $captcha['image'];	
			$this->session->set_userdata('captchaWord', $captcha['word']);

			$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
	        $this->breadcrumbs->add('Thanh toán', base_url() . 'thanh-toan');
	        $head['breadcrumb'] = $this->breadcrumbs->display();

	        $metas = $this->getMetas();
	        $head['metas'] = $metas;      
	        $head['metas']['meta_title'] = 'Thanh toán'; 

            $this->load->view('layouts/header', $head);
			$this->load->view('payment/index', $data);
			$this->load->view('layouts/footer', $this->_footer_menu);        	
        } else {
			$serieNumber = $this->input->post('serieNumber');
			$pinNumber = $this->input->post('pinNumber');
			$network = $this->input->post('network');
			$data = $this->baokim->transaction($network, $serieNumber, $pinNumber);
			if ($data['status'] === 200) {
				//update user amount
				if ($this->user_model->updateAmount(['users.id' => $userId], $data['amount'])) {
					$this->session->set_flashdata('success', 'Nạp tiền thành công');

					$name = !empty($user->full_name) ? $user->full_name : $user->username;
					$content = '<a href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> nạp '.$data['amount'].' vào tài khoản '.$user->username; 
					$this->generateLog(RECHARGE, $userId, $data = 0, $content);
										
					redirect(base_url() .'thanh-toan');
				} else {
					redirect(base_url() .'thanh-vien');
					$this->session->set_flashdata('error', 'Không thể cập nhật trên hệ thống!');	
				}
			} else {
				$this->session->set_flashdata('error', $data['message']);
				redirect(base_url() .'thanh-toan');				
			}
			
        }	    
	}

	public function validCaptcha( $str = '' )
	{		
		$captchaWord = $this->session->get_userdata('captchaWord' );
        if (!isset($captchaWord['captchaWord'])) {
            return false;
        }

       	$currentCaptcha = $captchaWord['captchaWord'];
		if ($str === $currentCaptcha) {
			$this->session->unset_userdata('captchaWord');	
			return true;
		}
		
		$this->session->unset_userdata('captchaWord');
		$this->form_validation->set_message('validCaptcha', '{field} nhập không đúng');
		return false;
	}

}