<div id="user-module">
  <div class="container">
    <div class="row">
        <?php $this->load->view('user/sidebar'); ?>
    <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
    <div class="user-box-content">
      <h1 class="title-page title-site title-register">Thanh toán qua thẻ điện thoại</h1>
      <div class="form-info">
        <form action="" method="post">
          <?php if( $this->session->flashdata('error') ) : ?>
              <div class="alert alert-sm alert-danger alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?php echo $this->session->flashdata('error'); ?>
              </div>
          <?php endif; ?>
          <?php if( $this->session->flashdata('success') ) : ?>
              <div class="alert alert-sm alert-success alert-dismissable">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <?php echo $this->session->flashdata('success'); ?>
              </div>
          <?php endif; ?>                    
          <div class="form-group">
            <label for="network" class="col-lg-2 control-label">Chọn mạng</label>
            <div class="col-lg-10">
              <input type="radio" name="network" value="VIETEL" <?php echo set_radio('network', 'VIETEL'); ?>> Viettel
              <input type="radio" name="network" value="MOBI" <?php echo set_radio('network', 'MOBI'); ?>> Mobifone
              <input type="radio" name="network" value="VINA" <?php echo set_radio('network', 'VINA'); ?>> Vinaphone
              <input type="radio" name="network" value="GATE" <?php echo set_radio('network', 'GATE'); ?>> Gate
              <input type="radio" name="network" value="VNM" <?php echo set_radio('network', 'VNM'); ?>> VietNamMobile
              <?php 
                echo form_error('network', '<div class="validation-alert alert alert-danger">', '</div>'); 
              ?>                
            </div>
          </div>            
          <div class="form-group">
            <label for="pinNumber" class="col-lg-2 control-label">Mã thẻ cào</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" id="pinNumber" name="pinNumber" placeholder="Mã thẻ" data-toggle="tooltip" data-title="Mã số sau lớp bạc mỏng" value="<?php echo set_value('pinNumber'); ?>" />
              <?php 
                echo form_error('pinNumber', '<div class="validation-alert alert alert-danger">', '</div>'); 
              ?>                
            </div>
          </div>
          <div class="form-group">
            <label for="serieNumber" class="col-lg-2 control-label">Số Serie</label>
            <div class="col-lg-10">
              <input type="text" class="form-control" id="serieNumber" name="serieNumber" placeholder="Số Serie" data-toggle="tooltip" data-title="Mã seri nằm sau thẻ" value="<?php echo set_value('serieNumber'); ?>">
              <?php 
                echo form_error('serieNumber', '<div class="validation-alert alert alert-danger">', '</div>'); 
              ?>                
            </div>
          </div>
          <div class="form-group" style="display: none;">
            <label for="captcha" class="col-lg-2 control-label">Mã bảo vệ</label>
            <div class="col-lg-6">
              <input type="text" class="form-control" id="captcha" name="captcha" placeholder="Mã bảo vệ" data-toggle="tooltip" data-title="Mã captcha">
              <?php 
                echo form_error('captcha', '<div class="validation-alert alert alert-danger">', '</div>'); 
              ?>                
            </div>
            <div class="col-lg-4 image captcha-image" id="captImg"><?php echo $codeCaptcha;?></div>
            <div class="col-lg-2"><a style="cursor: pointer;" class="refreshCaptcha" >Lấy mã mới</div>
          </div>

          <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
              <button type="submit" class="btn btn-primary" name="napthe">Nạp thẻ</button>
            </div>
          </div>
        </form>
      </div><!--div info-->    
    </div><!--div register free-->
    </div>
    </div>
  </div>
</div>
    <script>
    $(document).ready(function(){
        $('.refreshCaptcha').on('click', function(){            
            $.get('<?php echo base_url().'payment/refresh'; ?>', function(data){
                $('#captImg').html(data);
            });
        });
    });
    </script>