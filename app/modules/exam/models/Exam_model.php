<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_model extends Base_Model {

	protected $_table = 'exam';

	public function __construct()
	{
		parent::__construct();
	}

	/*
	* [getExam]
	* get one exam follow conditions
	*/
	public function getExam($where = [])
	{
		try {
			$this->db->select('exam.id, exam.exam_code, exam.slug, exam.extend_question_config, exam.attribute, exam.title, exam.publish_up, exam.publish_down, exam.course_id, exam.questions, exam.time, exam.content_file, exam.answer_file, course_details.name AS course, course_details.description, course_details.teacher, course_details.slug AS course_slug')
			->join('course_details', 'exam.course_id=course_details.course_id', 'left')
			->where( $where )
			->where( array('exam.publish_up <=' => date('Y-m-d H:i:s') ) )
			->limit(1);
			$result = $this->db->get( $this->_table )->result();
			if( !empty( $result ) ) {
				$exam = $result[0];
				$questionsIds = explode(',', $exam->questions);
				$questions = [];
				if ($questionsIds) {
					foreach ($questionsIds as $qId) {
						$questions[] = $this->getOneMcq($qId);
					}
				}
				$exam->questions = $questions;
				return $exam;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/*
	* [getQuestionByExam]
	* get any question, that have id into [array] $ids
	* @param [array] $ids
	*/
	public function getGroupQuestions( $ids = array() )
	{
		try {
			$this->db->select( 'id, name, score, question, choices_answer, true_answer, link_video_answer, link_media_answer, link_practice')
			->where_in( 'id', $ids );
			$result = $this->db->get('questions')->result_array();
			if( !empty($result) ) {
				$questions = array();
				foreach ($result as $item) {
					$item['choices_answer'] = unserialize( $item['choices_answer'] );
					$questions[] = $item;
				}
				return $questions;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/**
     * [getOneMcq]
     * @param  [int or string] $id
     * @return [array or null]
     */
    public function getOneMcq( $id )
    {
        $query = $this->db->where(['id'=>(int) $id])->get('questions')->result_array(); 
        if ($query) {
        	$query[0]['choices_answer'] = unserialize($query[0]['choices_answer']);
        	return $query[0];
        }
        return null;
    }
	/*
	* [getExam]
	* get one exam follow conditions
	*/
	public function getExams( $where = array(), $where_in = array(), $limit = 10, $offset = 0, $orderby = [] )
	{
		try {
			$this->db->select('exam.id, exam.slug, exam.extend_question_config, exam.exam_code, exam.title, exam.course_id, exam.questions, exam.time, course_details.name AS course, course_details.teacher, course_details.slug AS course_slug, exam.publish_up, exam.publish_down')
			->where( $where )
			->where( array('exam.publish_up <=' => date('Y-m-d H:i:s') ))
			->group_start()
				->where( array('exam.publish_down !=' => 0))
				->where( array('exam.publish_down >=' => date('Y-m-d H:i:s') ) )
				->or_group_start()
					->where( array('exam.publish_down' => 0 ) )
				->group_end()
			->group_end()
			->limit( $limit, $offset );
			if(!empty($where_in))
				$this->db->where_in($where_in['key'], $where_in['value']);
			$this->db->from('exam');
			$this->db->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			if (!empty($orderby)) {
				$this->db->order_by($orderby['key'], $orderby['value']);
			} else {
				$this->db->order_by('exam.id', 'desc');
			}
			$query = $this->db->get()->result();
			if($query){
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}


	public function getExercise( $where = array(), $where_in = array(), $limit = 10, $offset = 0, $orderby = '' )
	{
		try {
			$this->db->select('exam.id, exam.slug, exam.exam_code, exam.extend_question_config, exam.title, exam.course_id, exam.questions, exam.time, course_details.name AS course, course_details.teacher, course_details.slug AS course_slug, exam.publish_up, exam.publish_down')
			->where( $where )
			->limit( $limit, $offset );
			if(!empty($where_in))
				$this->db->where_in($where_in['key'], $where_in['value']);
			$this->db->from('exam');
			$this->db->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			if (!empty($orderby)) {
				if (is_array($orderby)) {
					$this->db->order_by($orderby['key'], $orderby['value']);	
				} else {
					$this->db->order_by($orderby);
				}
				
			} else {
				$this->db->order_by('exam.id', 'desc');
			}			
			$query = $this->db->get()->result();
			if($query){
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}

	/**
	* [get total record from exam follow condition]
	**/
	public function totalExam( $where = array() )
	{
		try{			
			return $this->db->where($where)->count_all_results($this->_table);
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [getCourseOption description]
	* @param  integer $parentId [description]
	* @param  string  $str      [description]
	* @return [type]            [description]
	*/
	public function getCourseOption($parentId = 0, $currentId = 0, $str = '') {
	  $courses = array();
	  $html = '';
	  $this->db->select('courses.id, courses.parent, course_details.name');
	  $this->db->from('courses');
	  $this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
	  $this->db->where('courses.parent', $parentId);
	  $result = $this->db->get()->result();	  
	  $selected = '';
	  foreach ($result as $item) {	   		
	  		$name = $str.$item->name;

	  		if($currentId > 0) {
	  			$selected = 'selected = "selected"';
	  		}
	  		$html .= '<option ' .$selected. ' data-parent="'.$item->parent.'" value="' . $item->id . '">' . $name . '</option>';
	  		$html .= $this->getCourseOption($item->id, $currentId, $str.'|-- ');
	  }	 
	  return $html;
	}
	public function insertUserExam( $args = array() )
	{
		try {
			if( $this->db->insert('user_exam', $args )) {
			   return true;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function existUserExam( $where = array())
	{
		try {
			$query = $this->db->where($where)->limit(1)->get('user_exam')->result();
			if(!empty($query))
				return true;
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function updateUserExam( $where = array(), $args = array() )
	{
		try {
			return $this->db->where($where)
			->update('user_exam', $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getCourses( $where = array() )
	{
		try {
			$this->db->select('id');
			$query = $this->db->where($where)->get('courses')->result_array();
			if($query)
				return $query;
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/*
	* [getExaming]
	* get one exam follow conditions
	*/
	public function getExaming( $where = array())
	{
		try {
			$this->db->select('exam.id, exam.slug, exam.exam_code, exam.extend_question_config, exam.attribute, exam.title, exam.course_id, exam.questions, exam.time, exam.publish_up, exam.publish_down, course_details.name AS course, course_details.teacher, course_details.slug AS course_slug, course_details.price')
			->where( $where )
			->where( array('exam.publish_up <=' => date('Y-m-d H:i:s') ))
			->where( array('TIMESTAMP(exam.publish_up) + exam.time >=' => date('Y-m-d H:i:s') ) )
			->from('exam')
			->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			$query = $this->db->get()->result();
			if($query){
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/*
	* [getComingExam]
	* get one exam follow conditions
	*/
	public function getComingExam( $where = array())
	{
		try {
			$this->db->select('exam.id, exam.slug, exam.exam_code, exam.attribute, exam.extend_question_config, exam.title, exam.course_id, exam.questions, exam.time, exam.publish_up, exam.publish_down, course_details.name AS course, course_details.teacher, course_details.slug AS course_slug, course_details.price')
			->where( $where )
			->where( array('exam.publish_up >' => date('Y-m-d H:i:s') ))
			->from('exam')
			->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			$query = $this->db->get()->result();
			if($query){
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	/*
	* [getExam]
	* get one exam follow conditions
	*/
	public function getFinishedExams( $where = array(), $where_in = array(), $limit = 10, $offset = 0 )
	{
		try {
			$this->db->select('exam.id, exam.slug, exam.exam_code, exam.attribute, exam.title, exam.extend_question_config, exam.course_id, exam.questions, exam.time, exam.publish_up, exam.publish_down, course_details.name AS course, course_details.teacher, course_details.slug AS course_slug, course_details.price')
			->where( $where )
			->where( array('exam.publish_up <=' => date('Y-m-d H:i:s') ) )
			->group_start()
				->where( array('exam.publish_down' => 0))
				->or_group_start()
            		->where( array('exam.publish_down <' => date('Y-m-d H:i:s') ) )
                ->group_end()
			->group_end()
			->limit( $limit, $offset )
			->order_by('exam.publish_up DESC');
			if(!empty($where_in))
				$this->db->where_in($where_in['key'], $where_in['value']);
			$this->db->from('exam');
			$this->db->join('course_details', 'exam.course_id = course_details.course_id', 'left');
			$query = $this->db->get()->result();
			if($query){
				return $query;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getExamById($id)
	{
		try {
			$query = $this->db->select('exam.slug, exam.title, exam.time, exam.publish_up, exam.publish_down')
			->where( array('id' => (int) $id) )
			->limit(1)
			->from('exam')
			->get()->result();
			if($query)
				return $query[0];
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getFilterExam( $where )
	{
		try{
			$this->db->where($where)->limit(1);
			$result = $this->db->get('exam')->result();
			if($result)
				return $result[0];
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getFilterQuestions( $ids = array(), $where = array() )
	{
		try {
			$this->db->select( 'id')
			->where( $where )
			->where_in( 'id', $ids );
			$result = $this->db->get('questions')->result();
			if( !empty($result) ) {
				$questions = array();
				foreach ($result as $item) {
					$questions[] = $item->id;
				}
				return $questions;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}

	public function getAnswerById($where = [])
	{
        if (!empty($where)) {
            $this->db->where($where)->limit(1);
            $query = $this->db->get('questions');

            if ($query) {
                return $query->row();    
            }
        }
       return false;
	}
	public function getAnswerByUser($where=[])
	{
		try{
            $this->db->where($where)->limit(1);
            $query = $this->db->get('user_exam')->result();
            if($query) {
                return $query[0];    
            }
	        return false;
       	} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}

	public function getUserCousrse($where=[])
	{
		try{
            $this->db->where($where);
            $query = $this->db->get('user_course')->result();
            if($query) {
                return $query;    
            }
	        return false;
       	} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}


	public function getCourseTree($parentId = 0, $currentId = 0, $str = '', $class = 'exam-course-item', $link = '#') {
	  $courses = array();
	  $html = '';
	  $this->db->select('courses.id, courses.parent, course_details.name');
	  $this->db->from('courses');
	  $this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
	  $this->db->where('courses.parent', $parentId);
	  $this->db->order_by('courses.ordering', 'asc');
	  $result = $this->db->get()->result();	  
	  foreach ($result as $item) {	   		
	  		$name = $str.$item->name;
	  		$link = '?cid=' . $item->id;

	  		$html .= '<li class="'.$class.'" value="' . $item->id . '"><a href="'.$link.'">' . $name . '</a></li>';
	  		$html .= $this->getCourseTree($item->id, $currentId, $str.'|-- ', $class . ' child', $link);
	  }
	  return $html;
	}


	public function addGiftExam($args = [])
	{
		try {
			if( $this->db->insert('exam_gifts', $args )) {
			   return true;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}

	public function updateGiftExam($where, $args = [])
	{
		try {
			if( $this->db->where($where)->update('exam_gifts', $args )) {
			   return true;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}

	public function getGiftExam($where = [])
	{
        if (!empty($where)) {
            $this->db->where($where)->limit(1);
            $query = $this->db->get('exam_gifts');

            if ($query) {
                return $query->row();    
            }
        }
       return false;
	}
}
