﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Exam
* @author ChienDau
* @copyright Bigshare
* 
*/

class Exam extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('exam_model');
        $this->load->library('breadcrumbs');
        $this->load->model('user/user_model');
        $this->load->model('base_model');        
        $this->load->model('courses/courses_model');
	}

	public function index($courseId = 0, $page = 0)
	{
		$this->setCurrentUrl();
		// Set Breadcrumbs
		$headData = array();
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Thi online', base_url().'thi-truc-tuyen');
        $headData['breadcrumb'] = $this->breadcrumbs->display();
        $headData['metas'] = $this->getMetas();
        $this->load->helper('paging');
        $data = $where = $where_in = array();
        $courseOption = $this->exam_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array(
			'exam.type' => 'online-exam',			
			'exam.status' => 1,
		);
		
		//pagination     
       	$limit = 15;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->exam_model->totalExam( $where);
        //filter
        $time = $this->input->get('time');
        $course_id = $this->input->get('cid');

        if(!empty($time) && $time != 150) {
        	$where['exam.time'] = $time;
        	$data['time'] = $time;
        } elseif (!empty($time) && $time = 150) {
        	$where['exam.time >='] = $time;
        }

        $courseType = $this->input->get('course_type');
        if(!empty($courseType)) {
        	$where['exam.attribute'] = $courseType;
        }
        $data['courseType'] = $courseType;

        if (!empty($course_id)) {
        	$courseIds = array();
        	$query = $this->exam_model->getCourses(array('parent' => (int) $course_id));
        	if ($query) {
	        	foreach ($query as $item) {
	        		$courseIds[] = $item['id'];
	        	}
	        	$courseIds[] = (int) $course_id;
	        	$where_in = array(
	        		'key' => 'exam.course_id',
	        		'value' => $courseIds,
	        	);
	        } else {
	        	$where['exam.course_id'] = (int) $course_id;
	        }
        }

        $data['time'] = $time;
        $data['course_id'] = $course_id;
		$exam = $this->exam_model->getFinishedExams( $where, $where_in, $limit, $offset );
		$data['paging'] = paging( base_url() .'thi-truc-tuyen/' . $courseId . '/', $page, $total, $limit );
		$where = array(
			'exam.type' => 'online-exam',	
			'exam.status' => 1,
		);
		$examing = $this->exam_model->getExaming($where);
		$comingExam = $this->exam_model->getComingExam($where);
		$data['examing'] = $examing;
		$data['exam'] = $exam;
		$data['comingExam'] = $comingExam;
		//get user exam

		//side bar user rank
		$whereRank = array(
			'exam_id !=' => null,
		);
		$userRank = $this->exam_model->getUserExam($whereRank, $limit = 10);
		$data['userRank'] = $userRank;
		//get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        $metas = $this->getMetas();
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Thi Online';
        $data['courses'] = $this->exam_model->getCourseTree(0, 0, '', 'exam-course-item', '?cid'); 
        
        $topUserGifts = $this->user_model->getUsers(['group_id' => 25], 100, 0, ['gifts' => 'desc']);       
        $data['topUserGifts'] = $topUserGifts;

		$this->load->view('layouts/header', $headData);
		$this->load->view('exam/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function detail($slug = '')
	{
		$data = [];

		$user = $this->session->userdata('web_user');

		if (!$this->session->has_userdata('web_user')){
			$this->session->set_flashdata('error', 'Bạn cần đăng nhập trước khi làm bài');
			redirect(base_url('thi-truc-tuyen'));
		}

		$where = [
			'exam.slug' => $slug,
			'exam.type' => 'online-exam',
			'exam.status' => 1
		];
		$exam = $this->exam_model->getExam($where);
		if (empty($exam)) {
			redirect(base_url() . 'notfound');
		}

		//Get course
		$course = $this->courses_model->getParents($exam->course_id);
		if ($course) {
			if ($course->price != 0) {
				
				$where = [
					'user_id' => (int) $user->id,
					'course_id' => (int) $course->id,
				];

				if (!$this->courses_model->existUserCourse($where)) {
					$this->session->set_flashdata('warning', 'Để tham gia thi, bạn cần mua khóa học trước');
					redirect(base_url('khoa-hoc/' . $course->slug));
				}
			}

			$args = [
				'exam_id' => $exam->id,
				'user_id' => $user->id,
			];
			
			//Check view count by exam_id
			$where = [
				'exam_id' => $exam->id, 
				'course_id' => $course->id,
				'user_id' => $user->id
			];			
			$totalViewed = $this->courses_model->viewedCount($where);
			if ($totalViewed >= $course->exam_view_limit) {
				$this->session->set_flashdata('warning', 'Lượt thi đã vượt quá giới hạn cho phép!');
				redirect(base_url('thi-truc-tuyen'));
			}

			$this->courses_model->updateViewLimit($course->id, $args);	
		}

		//get answer user
		$where = [
			'exam_id' => (int) $exam->id,
			'user_id' => (int) $user->id,
		];
		$userExam = $this->exam_model->getAnswerByUser($where);		
		if ($userExam) {
			$userAnswer = unserialize($userExam->answer);
			$data['userAnswer'] = $userAnswer;
		}		
		
		$date = date('Y-m-d H:i:s');
		$publishDown = strtotime($exam->publish_down);
		$exam->count_down = 0;
		if ($date > $exam->publish_up && $date <= date('Y-m-d H:i:s', $publishDown)) {
			$time = strtotime($date) - strtotime($exam->publish_up);
			$time = (int)($time/60);
			$exam->count_down = ($time <= (int)$exam->time) ? (int) $exam->time - $time : 0;
		}		

		$where = [
			'user_id' => $user->id,
			'exam_id' => $exam->id,
		];
		$existUserExam = $this->exam_model->existUserExam($where);
		$data['userExam'] = $existUserExam;
		$data['metas'] = $this->getMetas();    
        $data['metas']['meta_title'] = $exam->title;        		

		//Dieu kien de tao de thi online theo logic moi (tao tu dong)
		$arrSubject = [2,3,4];
		if (in_array($course->subject_id, $arrSubject)) {
			$template = 'view';

    	// Dieu kien lay so cau hoi o moi kho
		$extendQuestionConfig = @unserialize($exam->extend_question_config);

    	$exam->questions = $this->createExam($extendQuestionConfig, $course->id);

		} else {
			//Tao thu cong theo logic A.Quang
			$template = 'detail';
		}		

		//Pass exam data
		$data['exam'] = $exam;

		$this->load->view('exam/' . $template, $data);
	}

	public function checkresult()
	{
		if ($this->input->is_ajax_request()) {
			$exam_id = $this->input->post('exam_id');
			$question_id = $this->input->post('question_id');
			$option = $this->input->post('option');
			$auth = $this->session->userdata('web_user');
			$status = false;
			$message = '';
			$exam = $this->exam_model->getExamById($exam_id);
			$finishedTime = strtotime($exam->publish_up) + $exam->time * 60;
			if (isset($auth->id)) {
				$where = array(
					'user_id' => (int) $auth->id,
					'exam_id' => (int) $exam_id,
				);
				if (!$this->exam_model->existUserExam($where)) {
					$user = $this->user_model->getUser(['users.id' => $auth->id]);
					$result = $this->exam_model->getAnswerById(['questions.id' => $question_id]);								
					if ($result->true_answer === $option) {
						$status = true;
						if ($finishedTime > time()) {
							$gifts = rand(1,10);	
						} else {
							$gifts = rand(1,3);
						}
						
						$userGifts = $user->gifts + $gifts;
						$this->user_model->updateUser($user->id, ['gifts' => $userGifts]);
						$message = "Bạn đã được +{$gifts} QUÀ!";

						$giftExam = $this->exam_model->getGiftExam(['exam_gifts.user_id' => $auth->id, 'exam_gifts.exam_id' => $exam_id]);
						if ($giftExam) {
							$this->exam_model->updateGiftExam(['exam_gifts.id' => $giftExam->id], ['gifts' => $giftExam->gifts + $gifts]);
						} else {
							$argsExamGift = [
								'user_id' => $auth->id,
								'exam_id' => $exam_id,
								'gifts' => $gifts,
							];
							$this->exam_model->addGiftExam($argsExamGift);
						}
				
					}
				}


				
			} else {
				$message = 'Vui lòng đăng nhập để được nhận quà tặng!';
			}

			echo json_encode(['status' => $status, 'message' => $message]);
			exit;
		}

	}

	public function result()
	{
		if($this->input->is_ajax_request()) {
			$response = array(
				'code' => 204,
				'message' => 'Đã có lỗi sảy ra',
				);
			$options = $this->input->post('option');
			$exam_id = $this->input->post('exam_id');			
			if($exam_id) {
				$exam = $this->exam_model->getExamById($exam_id);
			}
			$time = $this->input->post('time');
			$ids = array();
			$result = array();
			$core = 0;
			$score = 0;
			$answer_right = array();
			$answer_right_user = array();
			foreach ($options as $item) {
				$ids[] = $item['id'];
				$result[$item['id']] = $item['true_answer'];
			}

			$questions = $this->exam_model->getGroupQuestions($ids);
			foreach ($questions as $item) {
				if($result[ $item['id'] ] == $item['true_answer']) {
					$answer_right_user[] =  $item['id'];
					$core++;
					$score += $item['score'];
				}
				$answer_right[$item['id']] = $item['true_answer'];
			}
			$user = $this->session->userdata('web_user');
			$where = array(
				'user_id' => (int) $user->id,
				'exam_id' => (int) $exam_id,
			);
			if($exam) {
				$date = date('Y-m-d H:i:s', time());
				$timeExecution = $this->input->post('time_execution');
				$publishDown = 	date('Y-m-d H:i:s', strtotime($exam->publish_down) +  60);		
				if (($timeExecution != '' || !is_null($timeExecution)) && $publishDown >= $date) {
					if(!$this->exam_model->existUserExam( $where )) {
					
						$args = array(
							'user_id' => $user->id,
							'exam_id' => (int) $exam_id,
							'score' => (int) $score,
							'time' => (float) $time,
							'answer' => serialize($result),
							'attribute' => 1, //1 trắc nghiệm 2 là tự luận
							'created' => date('Y-m-d H:i:s'),
						);
						if( $this->exam_model->insertUserExam( $args ) ) {
							$response['code'] = 200;
							$response['message'] = 'Bài làm của bạn đã được ghi nhận. Điểm sẽ được công bố sau khi cuộc thi kết thúc';

							$name = !empty($user->full_name) ? $user->full_name : $user->username;
							$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> tham gia bài thi trắc nghiệm: <a href="'.base_url().'thi-truc-tuyen/'.$exam->slug.'">'.$exam->title.'</a>';
							$this->generateLog(SEND_ANSWER, $user->id, (int) $exam_id, $content );
							$this->sendEmail($exam);
						}
					} else {
						$response['code'] = 200;
						$response['message'] = 'Bạn đã làm bài thi rồi, xin chờ kết thúc để xem kết quả';
					}
				} else {
					$name = !empty($user->full_name) ? $user->full_name : $user->username;
					$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> làm bài thi trực tuyến: <a href="'.base_url().'thi-truc-tuyen/'.$exam->slug.'">'.$exam->title.'</a>';
					$this->generateLog(DO_EXAM, $user->id, (int) $exam_id, $content );
					$response['result'] = $answer_right_user;
					$response['answer'] = $answer_right;
					$response['code'] = 200;
					$response['message'] = 'Bạn làm đúng '.count($answer_right_user).' câu, đạt '.$score.' điểm';
				}
			} 
			echo json_encode($response); die;
		}
	}

	public function saveExam()
	{
		$response = array(
			'code' => 204,
			'message' => 'Lưu thất bại',
		);
		$question_id = $this->input->post('id');
		$user = $this->session->userdata('web_user');
		$where = array(
			'question_id' => (int) $question_id,
			'user_id' => (int) $user->id,
		);
		if(!$this->exam_model->existUserExam( $where )) {
			$args = array(
				'question_id' => (int) $question_id,
				'user_id' => (int) $user->id,
				'created' => date('Y-m-d H:i:s'),
			);
			if( $this->exam_model->insertUserExam( $args ) ) {
				$response = array(
					'code' => 200,
					'message' => 'Lưu thành công',
				);
			}
		} else {
			$response = array(
				'code' => 205,
				'message' => 'Câu hỏi đã được lưu trước đây',
			);
		}
		echo json_encode($response); die;
	}

	public function uploadFile()
	{
		if($this->session->has_userdata('web_user')) {
			$user = $this->session->userdata('web_user');
		} else {
			$this->session->set_flashdata('error', 'Bạn chưa đăng nhập!');
			redirect(base_url('thi-truc-tuyen'));
		}
		$response = [
			'code' => 201,
			'message' => ''
		];
		// Config Upload
		$photoPath = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    	$config = array(
    		'upload_path'	=> $photoPath.'/upload/answer',
    		'allowed_types' => 'gif|jpg|png|jpeg',
			'max_size'     	=> 15000,
			'max_width' 	=> 0,
			'max_height' 	=> 0,
    		);	
    	$photo = [];
    	$exam_id = $this->input->post('exam_id');
    	$time = $this->input->post('time');
    	// Check Upload
    	$existlFile = count(array_filter($_FILES['answer_file_upload']['name']));
    	$totalFile = count($_FILES['answer_file_upload']['name']);
    	//print_r($_FILES);die;
    	$files = $_FILES;
    	if ($existlFile>0) {
    		$this->load->library('upload', $config);
    		$count = 0;
	    	for ($i=0;$i<$totalFile;$i++) {
	    		$_FILES['answer_file_upload']['name']= $files['answer_file_upload']['name'][$i];
		        $_FILES['answer_file_upload']['type']= $files['answer_file_upload']['type'][$i];
		        $_FILES['answer_file_upload']['tmp_name']= $files['answer_file_upload']['tmp_name'][$i];
		        $_FILES['answer_file_upload']['error']= $files['answer_file_upload']['error'][$i];
		        $_FILES['answer_file_upload']['size']= $files['answer_file_upload']['size'][$i];
		        if ($this->upload->do_upload('answer_file_upload')) {
		        	$imgArray = $this->upload->data();
        			$photo[] = isset($imgArray) ? 'upload/answer/' . $imgArray['file_name'] : '';
        			$count++;
		        } elseif ($files['answer_file_upload']['name'][$i]) {
	        		$response['code'] = 201;
	        		$response['message'] = 'Sai định dạng hoặc dung lượng quá 15Mb';
	        		echo json_encode($response); die;
	        	}
	    	}
	    	$where = array(
				'user_id' => (int) $user->id,
				'exam_id' => (int) $exam_id,
			);
			if(!$this->exam_model->existUserExam( $where )) {	
				$exam = $this->exam_model->getExamById($exam_id);
				if($exam) {
					$date = Date('Y-m-d H:i:s');
					if( $exam->publish_down >= $date && $exam->publish_down != 0 && $exam->publish_up <= $date ) {
						$args = [
		        			'attribute' => 2, //tra loi bai tu luan
		        			'answer' => serialize($photo),
		        			'exam_id' => (int) 	$exam_id,
		        			'user_id' => (int) $user->id,
		        			'time' => $time,
		        			'created' => date('Y-m-d H:i:s'),
		        		];
		        		if( $this->exam_model->insertUserExam( $args ) ) {
							$response['code'] = 200;
							$response['message'] = 'Bài làm của bạn đã được ghi nhận!';
							//logs
							$name = !empty($user->full_name) ? $user->full_name : $user->username;
							$content = '<a class="name-user" href="'.base_url().'thanh-vien/'.$user->username.'">'.$name.'</a> nộp bài thi tự luận: <a href="'.base_url().'thi-truc-tuyen/'.$exam->slug.'">'.$exam->title.'</a>';
							$this->generateLog(UPLOAD_ANSWER, $user->id, (int) $exam_id, $content );
							$this->sendEmail($exam);
						} else {
							$response['code'] = 202;
							$response['message'] = 'Bài của bạn chưa được lưu, vui lòng gửi bài lại';
						}
					} else {
						$response['code'] = 202;
						$response['message'] = 'Quá thời hạn gửi bài';
					}
				}
			} else {
				$response['code'] = 202;
				$response['message'] = 'Bạn đã gửi bài thì này rồi!';
			} 
	    } else {
	    	$response['message'] = 'Bạn chưa chọn bài gửi';
	    }
    	echo json_encode($response); die;
	}
	/**
     * @name sendEmail
     * send a mail to user
     * */
    public function sendEmail( $exam )
    {
    	if(!$this->session->has_userdata('web_user')) {
    		redirect(base_url().'notfound');
    	}
    	$user = $this->session->userdata('web_user');
    	$userName = !empty($user->full_name) ? $user->full_name : $user->username;
        $this->load->helper('file');
        $where_in = array(
            'key' => 'option_name',
            'value' => array(
                'protocol', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'charset',  'mailtype',
                ),
            );
        $mailSetting = $this->base_model->getOptions( $where=[], $where_in );
        $mailer = [];
        foreach ($mailSetting as $value) {
            $mailer[$value->option_name] = $value->option_value;
        }
        date_default_timezone_set('GMT');
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => $mailer['protocol'],
            'smtp_host' => $mailer['smtp_host'],
            'smtp_user' => $mailer['smtp_user'],
            'smtp_pass' => $mailer['smtp_pass'],
            'smtp_port' => (int) $mailer['smtp_port'],
            'charset' => $mailer['charset'],
            'mailtype'  => $mailer['mailtype'],
        ));
        $this->email->from($user->email, $userName);
        $this->email->to($this->getEmail());     
        $this->email->subject('Thông báo thành viên gửi bài thi trên '.base_url());
        $this->email->set_newline("\r\n");
        $bodyData = [
            '{FULLNAME}' => '<span style="font-weight:bold">'.$userName.'</span>',
            '{DOMAIN}' => '<span style="font-weight:bold">'.base_url().'</span>',
            '{EXAM}' => '<a href="'.$exam->slug.'" style="font-weight:bold">'.$exam->title.'</a>',
            '{RESULT}' => '<a href="'.base_url().'admin/mark" style="font-weight:bold">Click vào đây</a>',
        ];
        $path = base_url().'/email/send_result.txt';
        $emailTemplate = read_file($path);
        $message = $this->paramReplace($emailTemplate, $bodyData);
        $this->email->message($message);
        $this->email->send();
    }

    public function paramReplace($txt, $search = array())
    {
        foreach ($search as $k=>$v) {
            $txt = str_replace($k,$v,$txt);
        }

        return $txt;
    } 


    public function createExam($params = [], $courseId = 0)
    {
    	$user = $this->session->userdata('web_user');

    	if(!$user || $courseId = 0 || empty($params)) {
    		redirect(base_url().'notfound');
    	}    	

    	$this->load->model('question_model');

    	$userId = $user->id;
    	//Lay cac cau hoi hoc vien da su dung de loai bo trong khi query cac cau hoi cho de tiep theo
    	$usedQuestions = $this->question_model->getListQuestions(['question_users.user_id' => $userId]);
    	$notIN = [];
    	if (!is_null($usedQuestions)) {
    		foreach ($usedQuestions as $q) {
    			$notIN[] = (int) $q->question_id;
    		}
    	}
    	
    	$questions = $questionName = [];
    	foreach ($params as $k => $value) {
    		$key = (int) $k;
    		$where = [
    			'questions.question_category_id' => $key,
    			'NOT_IN' => [
    				'questions.id' => $notIN
    			]
    		];

    		$result = $this->question_model->getQuestions($where, $value);
    		if (count($result) > 0) {
	    		foreach ($result as $item) {
	    			$questions[] = (array) $item;
	    			$questionName[] = $item->name;

	    			// Kiem tra cau hoi da duoc hoc vien su dung hay chua, neu chua thi insert du lieu moi nay vao bang question_users
	    			$where = [
	    				'question_users.question_id' => (int) $item->id,
	    				'question_users.user_id' => (int) $userId,
	    			];
	    			if (is_null($this->question_model->getQuestionUser($where))) {
		    			$args = [
		    				'question_id' => (int) $item->id,
		    				'user_id' => (int) $userId,
		    				'course_id' => 0,
		    				'created_at' => date('Y-m-d H:i:s'),
		    			];
	    				$this->question_model->addQuestionUser($args);
	    			}
	    		}
    		}
    	}
    	return $questions;
    }
}