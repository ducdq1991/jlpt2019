<div id="practice-module" class="module">
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-lg-7">
				<h1 class="page-title">Đấu Trường</h1>
				<?php if( $this->session->flashdata('success') ) : ?>
	                <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('error') ) : ?>
	                <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
	            <?php endif; ?>
	            <?php if( $this->session->flashdata('warning') ) : ?>
	                <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
	            <?php endif; ?>
				<div class="box-filter col-md-12">
					<form class="form-group-inline" role="form">			
						<div class="col-md-6">
							<select name="time" class="form-control" id="time">
								<option value="0">--Thời gian (p)--</option>
								<option value="15" <?php if (isset($time) && $time == 15) echo "selected=''"; ?>>15</option>
								<option value="30" <?php if (isset($time) && $time == 30) echo "selected=''"; ?>>30</option>
								<option value="45" <?php if (isset($time) && $time == 45) echo "selected=''"; ?>>45</option>
								<option value="60" <?php if (isset($time) && $time == 60) echo "selected=''"; ?>>60</option>
								<option value="90" <?php if (isset($time) && $time == 90) echo "selected=''"; ?>>90</option>
								<option value="120" <?php if (isset($time) && $time == 120) echo "selected=''"; ?>>120</option>
								<option value="150" <?php if (isset($time) && $time == 150) echo "selected=''"; ?>>> = 150</option>
							</select>
						</div>
						<div class="col-md-3"><button type="submit" class="btn btn-success">Lọc cuộc thi</button></div>					
						
					</form>
				</div>
				<?php if(empty($examing)) : ?>
				<?php else: ?>
					<div class="current-exam">
					<h1>Cuộc thi đang diễn ra</h1>
						<?php foreach ($examing as $row) : ?>
						<div class="box-item">
							<?php if(!empty($row->price)) :?>
								<i class="bullet lock pull-left fa fa-lock" aria-hidden="true"></i>
							<?php else :?>
								<i class="bullet unlock pull-left fa fa-unlock-alt" aria-hidden="true"></i>
							<?php endif; ?>
							<div class="content">
								<h1><a class="logged" href="<?php echo base_url().'thi-truc-tuyen/'.$row->slug.'.html';?>" target="_blank"><?php echo $row->title;?></a></h1>
								<p><a href="<?php echo base_url().'khoa-hoc/'.$row->course_slug;?>"><?php echo $row->course.'</a>'; if($row->teacher) echo ' - '. $row->teacher; ?></p>
								<span>Mã đề: <strong><?php echo $row->id; ?></strong></span>
								<span>Thời gian: <strong><?php echo $row->time; ?></strong>p</span>
								<span>Ngày thi: <strong><?php echo date('d/m',strtotime($row->publish_up));?></strong></span>
								
							</div>

							<a class="logged btn btn-info btn-start pull-right" href="<?php echo base_url().'thi-truc-tuyen/'.$row->slug.'.html';?>" target="_blank">Vào Thi</a>
							<?php							
								$timeUp = date('H:i', strtotime($row->publish_up));
								$timeDown = date('H:i', strtotime($row->publish_down) + ($row->time * 60));
								
							?><br>
							<span class="time-line" style="font-style: italic;font-size: 12px;margin-left: 26px;">
							<?php echo 'Từ: ' . $timeUp.' Đến '.$timeDown; ?></span>
						</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<?php if(empty($comingExam)) : ?>
				<?php else: ?>
					<div class="coming-exam">
					<h1>Cuộc thi sắp diễn ra</h1>
						<?php foreach ($comingExam as $row) : ?>
						<div class="box-item">
							<?php if(!empty($row->price)) :?>
								<i class="bullet lock pull-left fa fa-lock" aria-hidden="true"></i>
							<?php else :?>
								<i class="bullet unlock pull-left fa fa-unlock-alt" aria-hidden="true"></i>
							<?php endif; ?>
							<div class="content">
								<h1><?php echo $row->title;?></h1>
								<p><a href="<?php echo base_url().'khoa-hoc/'.$row->course_slug;?>"><?php echo $row->course.'</a>'; if($row->teacher) echo ' - '. $row->teacher; ?></p>
								<span>Mã đề: <strong><?php echo $row->id; ?></strong></span>
								<span>Thời gian: <strong><?php echo $row->time; ?></strong>p</span>
								<span>Ngày thi: <strong><?php echo date('d/m',strtotime($row->publish_up));?></strong>  - Bắt đầu: <strong style="color:red"><?php echo date('H:i:s',strtotime($row->publish_up));?></strong>	</span>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<div class="finished-exam">
					<h1>Cuộc thi đã diễn ra</h1>
					<div class="box-content">
						<?php if(empty($exam)) : echo "Không có kết quả nào được tìm thấy"; ?>
						<?php else: ?>
							<?php foreach ($exam as $row) : ?>
							<div class="box-item">
								<?php if(!empty($row->price)) :?>
									<i class="bullet lock pull-left fa fa-lock" aria-hidden="true"></i>
								<?php else :?>
									<i class="bullet unlock pull-left fa fa-unlock-alt" aria-hidden="true"></i>
								<?php endif; ?>
								<div class="content">
									<h1><a class="logged" href="<?php echo base_url().'thi-truc-tuyen/'.$row->slug.'.html';?>" target="_blank"><?php echo $row->title;?></a></h1>
									<p><a href="<?php echo base_url().'khoa-hoc/'.$row->course_slug;?>"><?php echo $row->course.'</a>'; if($row->teacher) echo ' - '. $row->teacher; ?></p>									
									<span>Mã đề: <strong><?php echo $row->id; ?></strong></span>
								<span>Thời gian: <strong><?php echo $row->time; ?></strong>p</span>
								<span>Ngày thi: <strong><?php echo date('d/m',strtotime($row->publish_up));?></strong></span>
									
								</div>
								<a class="logged btn btn-info btn-start pull-right" href="<?php echo base_url().'thi-truc-tuyen/'.$row->slug.'.html';?>" target="_blank">Bắt đầu</a>
								<?php 
									$timeUp = date_create($row->publish_up);
									$dateUp = date_format($timeUp, 'd/m/Y');
									$timeUp = date_format($timeUp, 'H:i');
								?>
								<span class="pull-right time-line"><?php echo $timeUp; ?><i><?php echo $dateUp; ?></i></span>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="pagination">
					<?php echo $paging;?>
				</div><!--pagination-->
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 doc-category">
			  <div class="list-txt">
			    <h1><span class="list"><img src=""></span>Chuyên đề</h1>
					<ul>
						<?php echo $courses;?>
					</ul>						
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 doc-category">

				<div class="list-txt">
						<h1 style="background: #f7941d;text-align: center;">TOP QUÀ TẶNG</h1>
						<ul style="border-left: 0px;padding: 0px;">
					<div class="table-ranking">
						<table class="table-hover table-responsive" style="text-align:center">
							<thead style="background: #fafafa">
								<tr>
									<th style="color:#111;font-size: 12px;text-align: center;">TT</th>
									<th style="color:#111;font-size: 12px;text-align: center;">Họ tên</th>
									<th style="color:#111;font-size: 12px;text-align: center;color:red"><i class="fa fa-gift"></i></th>
								</tr>
							</thead>
							<tbody>
							<?php if ($topUserGifts) :?>
								<?php $i = 0; foreach ($topUserGifts as $user) : $i++; ?>
									<?php
	                                    $item_rank = 'item-rank';
	                                    if($i == 1) {
	                                        $item_rank = 'item-rank rank-gold';
	                                    } elseif ($i == 2) {
	                                        $item_rank = 'item-rank rank-sliver';
	                                    } elseif($i == 3) {
	                                        $item_rank = 'item-rank rank-copper';
	                                    }
	                                ?>
									<tr>
										<td class="">
											<span><?php echo $i; ?></span>
										</td>
										<td class="item-name text-left" style="color:#333">
											<a href="<?php echo base_url()?>/thanh-vien/<?php echo $user->username;?>" title="">
												<span class="" style="font-size: 13px;"><?php echo $user->username; ?></span>
											</a>
										</td>
										<td style="color:red;padding-right: 10px;"><?php echo $user->gifts; ?></td>
									</tr>
								<?php endforeach; ?>
							<?php else: ?>
								<tr><td colspan="3">Chưa có thông kế xếp hạng</td></tr>
							<?php endif;?>
							</tbody>
						</table>
						<p class="read-more">
							<i class="fa fa-chevron-circle-right"></i>
							<a href="<?php echo base_url();?>bang-xep-hang">Xem tất cả</a>
						</p>
					</div>
					<div class="rank-bottom"></div>
				</ul>
				</div><!--ranking user-->
			</div>
			
		</div>
	</div>
</div>
<script type="text/javascript">
	var _course_id_practice = "<?php echo $course_id; ?>";
</script>

<style type="text/css" media="screen">
	.table-ranking table tbody tr td {
    padding: 0;
    margin: 0px;
    line-height: 22px;
}
.table-ranking table tbody{
	padding-top: 10px;
}
.table-ranking table tr th{
	padding: 5px 0px;
}
</style>