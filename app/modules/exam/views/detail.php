<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" content-type="application/pdf">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php if(!empty($metas)) echo $metas['meta_description']; ?>" >
	<meta name="keywords" content="<?php if(!empty($metas)) echo $metas['meta_keyword']; ?>" >
	<title>
		<?php if(!empty($metas)) {
			echo $metas['meta_title'];
		} else {
			echo $title;
		}		
		?>		
	</title>
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/style.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/custom.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/responsive.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/owl-carousel.css" />
	<link rel="stylesheet" style="text/css" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Patrick+Hand' rel='stylesheet' type='text/css'>
	<!-- Toastr style -->
    <link href="<?php echo base_url();?>assets/css/toastr/toastr.min.css" rel="stylesheet">
	<script type="text/javascript">
		var siteUrl = '<?php echo base_url();?>';

	</script>
</head>
<body>
<?php 
	$isDoing = 1;
	$endingTime = $exam->publish_down;
	if ($exam->publish_down < date('Y-m-d H:i:s')) {
		$isDoing = 0;
		$endingTime = date('Y-m-d H:i:s', time() + $exam->time * 60);
	}
?>
<script>	
	<?php if(!empty($userAnswer) ||  ($exam->publish_down < date('Y-m-d H:i:s'))):?>
	var is_done = 1;
	<?php else:?>
	var is_done = 0;
	<?php endif;?>

	var $examEndTime = '<?php echo $endingTime;?>';
	var $isDoing = <?php echo $isDoing;?>;
	var endExamTime = new Date($examEndTime);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var $examTimer;
</script>
	<div id="countdown" style="display: none"></div>
	<div id="answer-sheet-panel">
		<div class="panel-exam-result text-center">
			<div class="your-result">Đã làm: <span class="aswed">0</span>/<span class="total-asw"><?php echo count($exam->questions);?></span> Câu</div>
		</div>	
		<div class="box-bottom mcq-exam">						
			<?php if( $exam->publish_down >= date('Y-m-d H:i:s') && !$userExam ) : ?>
				<a href="<?php echo base_url().$exam->content_file;?>" class="btn btn-warning">Download Đề thi</a>
				<button id="send-result" class="send-result btn btn-primary" data-toggle="modal" data-target="#confirmModal">Gửi bài</button>							
			<?php elseif($exam->publish_down < date('Y-m-d H:i:s')) : ?>
				
				<?php if(!empty($userAnswer)) :?>
					<p class="suport-text">Bạn đã tham gia bài thi này rồi</p>
				<?php endif;?>
				<a href="<?php echo base_url().$exam->content_file;?>" class="btn btn-warning">Download Đề thi</a>
				<button id="view-result" class="view-result btn btn-primary">Xem kết quả</button>
				<a href="<?php echo base_url();?>bang-xep-hang" class="btn btn-primary">Xếp hạng</a>
				<a href="<?php echo base_url().$exam->answer_file;?>" class="btn btn-primary">Đáp án</a>
			<?php endif; ?>
		</div>

		<h3><i class="fa fa-edit"></i> PHIẾU TRẢ LỜI</h3>
		<div class="sheet-container">		
	<?php 
	if (isset($exam->questions) && !empty($exam->questions)):
		$sheet = 1;
		foreach ( $exam->questions as $row ) : 
	?>
		<div class="row-asw">
		<div class="ques-label">Câu <?php echo $sheet;?>: </div>
		<?php 
		if (isset($row['choices_answer']) && count($row['choices_answer']) > 0) :
			foreach ($row['choices_answer'] as $key => $value) : 
		?>
			<div class="radio-asw" id="ques-<?php echo $row['id'];?>">
				<label>				
				<a onclick="return chooseASW('<?php echo $row['id']; ?>', '<?php echo $key; ?>', '<?php echo $sheet;?>');" class="asw-text <?php echo $row['id'];?>" id="<?php echo $row['id'] . '-' . $key; ?>"><strong><?php echo $key; ?></strong></a></label>
			</div>
		<?php 
			endforeach;
		endif;
		?>		
		</div>
	<?php 
			$sheet++;
			endforeach;
		endif;
	?>
			</div>
	</div>
	<div class="exam-detail-heading">
		<div class="container">
			<div class="row">
				<div id="logo" class="col-md-3 col-sm-3 col-xs-12">
					<a href="<?php echo base_url();?>">
						<img src="<?php echo base_url();?>/assets/img/logo-mobile.png" alt="logo-qstudy" class="img-responsive">
					</a>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<nav class="navbar navbar-default col-md-11 com-sm-11 col-xs-12">
	                    <div class="navbar-header">
	                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-exam" aria-expanded="false">
	                        <span class="sr-only">Toggle navigation</span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                      </button>
	                      <a class="navbar-brand" href=""></a>
	                    </div>
	                    <div class="collapse navbar-collapse" id="menu-exam">
				        	<?php echo $this->multi_menu->render(); ?>
	                    </div>
	                </nav>
				</div>
			</div>
		</div>
		<div class="count-down" data-time="<?php echo $exam->count_down; ?>">
			<img src="<?php echo base_url();?>/assets/img/square.png" alt="" class="">
			<span>00:00:00</span>
		</div>
	</div>
	<div class="exam-detail-content">
		<?php $this->load->view('block/ads_left_outer'); ?>
		<div class="container">
			
			<div id="exam-heading" class="col-lg-offset-1 col-md-offset-1 col-md-10 col-sm-10">
				<h1><?php echo $exam->title;?></h1>
				<p class="course">Khóa học/Chuyên đề: <span><?php echo $exam->course;?></span></p>
				<?php $attribute = [1 => 'Bài thi trắc nghiệm', 2 => 'Bài thi tự luận'];?>
				<p class="time"><strong><?php echo $attribute[$exam->attribute];?></strong> - Thời gian làm bài: <?php echo $exam->time; ?> phút</p>
				<hr>				
			</div>
			<?php if($exam->attribute == 1 || ($exam->attribute == 2 && !empty($exam->questions) && empty($exam->content_file))	) :?>
				<div class="box-content col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10">
					<div class="box-result" style="display: none;">
						<span class="mes-result"></span>
						<a class="btn btn-sm btn-default pull-right" id='reset-page' >Làm lại</a>
						<a class="btn btn-sm btn-default pull-right" href="<?php echo base_url('thi-truc-tuyen'); ?>" >Quay lại</a>
					</div>
					<?php if( $exam->publish_down >= date('Y-m-d H:i:s') ) : ?>
						<?php $order = 1; ?>
						<?php 
							if (isset($exam->questions) && !empty($exam->questions)):
								foreach ( $exam->questions as $row ) : 
							?>
							<div class="box-item" id="<?php echo $row['id'];?>" data-id="<?php echo $row['id'];?>">
								<div class="question">
									<strong>Câu <?php echo $order; ?> <span><?php echo '('.$row['score'].' điểm)'; ?></span>: </strong><?php echo $row['question']; ?>
								</div>
								<?php 
								if (isset($row['choices_answer']) && count($row['choices_answer'])):
									foreach ($row['choices_answer'] as $key => $value) : 
								?>
									<div class="radio" id="<?php echo $row['id'];?>-<?php echo $key; ?>">
										<label><input type="radio" id="<?php echo $row['id'];?>-<?php echo $key; ?>" class="answer-item <?php echo $row['id'];?>-<?php echo $key; ?>" value="<?php echo $key; ?>" data-key="<?php echo $row['id'];?>" name="answer-<?php echo $order; ?>">
										<strong><?php echo $key.') '; ?></strong>
										<?php echo $value; ?></label>
									</div>
								<?php 
									endforeach; 
								endif;
								?>
							
								<div class="box-meta">
									<?php if($row['link_practice']) : ?>
										<a class="link" href="<?php echo $row['link_practice'];?>" target="_blank"><i class="fa fa-book" aria-hidden="true"></i> Lý thuyết</a>
									<?php endif; ?>
									<?php if($exam->course_slug) : ?>
										<a class="link" href="<?php echo base_url().'khoa-hoc/'.$exam->course_slug;?>" target="_blank"><i class="fa fa-tags" aria-hidden="true"></i> Khóa học</a>
									<?php endif;?>
									<i class="pull-left fa fa-check save-success" style="display: none; color: #53b425;" aria-hidden="true"></i>
									<i class="pull-left fa fa-times save-failed" style="display: none; color: #be0316;" aria-hidden="true"></i>
									<a class="pull-left button-save" style="cursor: pointer;" data-id="<?php echo $row['id'];?>"><i class="fa fa-plus" aria-hidden="true"></i> Lưu</a>
									<?php 
										$timeDoing = date('Y-m-d H:i:s');
										if ( $exam->publish_down < $timeDoing):
									?>
									<span class="pull-left">Mã ID: <?php echo $row['id'];?></span>
									<?php endif;?>									
									<span class="result left"></span>
									<span class="result wrong"></span>
									
								</div>
								<div class="save-message"></div>
							</div>
						<?php $order++; endforeach; endif; ?>
					<?php else : ?>
						<?php $order = 1; ?>
						<?php foreach ( $exam->questions as $row ) : ?>
							<div class="box-item" id="<?php echo $row['id'];?>" data-id="<?php echo $row['id'];?>">
								<div class="question">
									<strong>Câu <?php echo $order; ?>: </strong><?php echo $row['question']; ?>
								</div>
								<?php foreach ($row['choices_answer'] as $key => $value) : ?>
									<div class="radio">
										<label>
										<input type="radio" id="<?php echo $row['id'];?>-<?php echo $key; ?>" class="answer-item <?php echo $row['id'];?>-<?php echo $key; ?>" value="<?php echo $key; ?>" data-key="<?php echo $row['id'];?>" name="answer-<?php echo $order; ?>" <?php if(isset($userAnswer[$row['id']]) && $userAnswer[$row['id']] == $key) echo 'checked'; ?>>										
										<strong><?php echo $key.') '; ?></strong>
										<?php echo $value; ?></label>
									</div>
								<?php endforeach; ?>
							
								<div class="box-meta">
									<?php
			                            $youtubeExp = explode('v=', $row['link_video_answer']);
			                            $videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';
			                        ?>
			                        <?php if($videoId) : ?>
										<a class="previewVideo protected" href="#videoCollapse-<?php echo $row['id'];?>" data-toggle="collapse" data-id="<?php echo $row['id'];?>" data-url="<?php echo base_url().'player/index/'.$videoId;?>"><i class="fa fa-play" aria-hidden="true"></i> Video lời giải</a>
									<?php endif; ?>
									<?php if($row['link_media_answer']) : ?>
										<a class="previewPdf protected" href="#fileCollapse-<?php echo $row['id'];?>" data-toggle="collapse" data-id="<?php echo $row['id'];?>" data-url="<?php echo base_url().$row['link_media_answer']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Lời giải</a>
									<?php endif; ?>
									<?php if($row['link_practice']) : ?>
										<a class="link" href="<?php echo $row['link_practice'];?>" target="_blank"><i class="fa fa-book" aria-hidden="true"></i> Lý thuyết</a>
									<?php endif; ?>
									<?php if($exam->course_slug) : ?>
										<a class="link" href="<?php echo base_url().'khoa-hoc/'.$exam->course_slug;?>" target="_blank"><i class="fa fa-tags" aria-hidden="true"></i> Khóa học</a>
									<?php endif;?>
									<?php 
										$timeDoing = date('Y-m-d H:i:s');
										if ( $exam->publish_down < $timeDoing):
									?>
									<span class="pull-left">Mã ID: <?php echo $row['id'];?></span>
									<?php endif;?>									
									<i class="pull-left fa fa-check save-success" style="display: none; color: #53b425;" aria-hidden="true"></i>
									<i class="pull-left fa fa-times save-failed" style="display: none; color: #be0316;" aria-hidden="true"></i>
									<a class="pull-left button-save" style="cursor: pointer;" data-id="<?php echo $row['id'];?>"><i class="fa fa-plus" aria-hidden="true"></i> Lưu</a>
									<span class="result right"></span>
									<span class="result wrong"></span>
									
								</div>
								<div class="save-message"></div>
								<div id="videoCollapse-<?php echo $row['id'];?>" class="collapse">
								    video lời giải
								</div>
								<div id="fileCollapse-<?php echo $row['id'];?>" class="collapse">
								    lời giải
								</div>
							</div>
						<?php $order++; endforeach ?>
					<?php endif; ?>
				</div>
				<div class="col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10">
					<?php if($exam->publish_down < date('Y-m-d H:i:s') && isset($userAnswer) && $exam->attribute == 2) : ?>
						<div class="box-answer-file">
							<h4>Bài làm của bạn: </h4>
							<?php foreach ($userAnswer as $item) :?>
						    	<img src="<?php echo base_url().'/'.$item;?>">
						    <?php endforeach;?>
						</div>
					<?php endif;?>
					<?php if ($exam->attribute == 1) :?>
					<div class="box-bottom mcq-exam mobile-show-panel">						
						<?php if( $exam->publish_down >= date('Y-m-d H:i:s') && !$userExam ) : ?>
							<a href="<?php echo base_url().$exam->content_file;?>" class="btn btn-warning">Download Đề thi</a>
							<button id="send-result" class="send-result btn btn-primary" data-toggle="modal" data-target="#confirmModal">Gửi bài</button>							
						<?php elseif($exam->publish_down < date('Y-m-d H:i:s')) : ?>
							
							<?php if(!empty($userAnswer)) :?>
								<p class="suport-text">Bạn đã tham gia bài thi này rồi</p>
							<?php endif;?>
							<a href="<?php echo base_url().$exam->content_file;?>" class="btn btn-warning">Download Đề thi</a>
							<button id="view-result" class="view-result btn btn-primary">Xem kết quả</button>
							<a href="<?php echo base_url();?>bang-xep-hang" class="btn btn-primary">Xếp hạng</a>
							<a href="<?php echo base_url().$exam->answer_file;?>" class="btn btn-primary">Đáp án</a>
						<?php endif; ?>
					</div>
					<?php else: ?>
					<div class="box-bottom">
						<?php if($exam->publish_down == 0 || $exam->publish_down < date('Y-m-d H:i:s')) :?>
						<?php elseif(!$userExam) : ?>
							<a href="#upload-file-modal" id="upload_answer" class="btn btn-info" data-toggle="modal">Gừi bài</a>
						<?php endif; ?>
						<?php if($exam->course_slug) : ?>
							<a class="btn btn-info" href="<?php echo base_url().'khoa-hoc/'.$exam->course_slug;?>" target="_blank" title="Xem khóa học">Khóa học</a>
						<?php endif;?>
						<a href="<?php echo base_url();?>bang-xep-hang" class="btn btn-primary">Xếp hạng</a>
					</div>
					<!-- Modal -->
					<div id="upload-file-modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Trả lời phần thi tự luận</h4>
								</div>
								<div class="modal-body">
									<form id="upload-answer-file-form" accept-charset="utf-8" action="" method="post" enctype="multipart/form-data">
										<input type="file" name="answer_file_upload[]">
										<br/>
										<button type="button" id="add-answer-upload" class="btn btn-default btn-sm">Thêm file</button>
										<input type="hidden" name="exam_id" value="<?php echo $exam->id;?>">
										<input type="hidden" name="time" value="">
									</form>
								</div>
								<div class="modal-footer">
									<input type="submit" id="send-file-upload" class="btn btn-primary" value="Gửi bài">
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
			<?php else: ?>
				<div class="col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10">
					<div class="box-content-file">
					    <iframe src="http://docs.google.com/gview?url=<?php echo base_url().$exam->content_file;?>&embedded=true" style="width:100%; min-height:1000px;" frameborder="0"></iframe>
					</div>
					<?php if($exam->publish_down < date('Y-m-d H:i:s') && isset($userAnswer)) : ?>
						<div class="box-answer-file">
							<h4>Bài làm của bạn: </h4>
							<?php foreach ($userAnswer as $item) :?>
						    	<img src="<?php echo base_url().'/'.$item;?>">
						    <?php endforeach;?>
						</div>
					<?php endif;?>
					<div class="box-bottom">
						<a href="<?php echo base_url().$exam->content_file;?>" id="exam_question" class="btn btn-primary" download>Download đề thi</a>
						<?php if($exam->publish_down == 0 || $exam->publish_down < date('Y-m-d H:i:s')) :?>
							<a href="#previewPDF" data-toggle="collapse" id="view_exam_answer" class="btn btn-info" data-url="<?php echo base_url().'/'.$exam->answer_file;?>">Xem đáp án</a>
							<a href="<?php echo base_url().$exam->answer_file;?>" id="exam_answer" class="btn btn-primary" download>Download bài giải</a>
						<?php elseif(!$userExam) : ?>
							<a href="#upload-file-modal" id="upload_answer" class="btn btn-info" data-toggle="modal">Gừi bài</a>
						<?php endif; ?>
						<?php if($exam->course_slug) : ?>
							<a class="btn btn-info" href="<?php echo base_url().'khoa-hoc/'.$exam->course_slug;?>" target="_blank" title="Xem khóa học">Khóa học</a>
						<?php endif;?>
						<a href="<?php echo base_url();?>bang-xep-hang" class="btn btn-primary">Xếp hạng</a>
					</div>
					<!-- Modal -->
					<div id="upload-file-modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Trả lời phần thi tự luận</h4>
								</div>
								<div class="modal-body">
									<form id="upload-answer-file-form" accept-charset="utf-8" action="" method="post" enctype="multipart/form-data">
										<input type="file" name="answer_file_upload[]">
										<br/>
										<button type="button" id="add-answer-upload" class="btn btn-default btn-sm">Thêm file</button>
										<input type="hidden" name="exam_id" value="<?php echo $exam->id;?>">
										<input type="hidden" name="time" value="">
									</form>
								</div>
								<div class="modal-footer">
									<input type="submit" id="send-file-upload" class="btn btn-primary" value="Gửi bài">
								</div>
							</div>
						</div>
					</div>

					<div id="previewPDF" class="collapse">
						<h2>Lời giải</h2>
						<span></span>
					</div>
				</div><!-- end #document-detail -->
			<?php endif;?>
		</div>
		<?php $this->load->view('block/ads_right_outer');?>
	</div>
	<div id="comment-box"  class="col-lg-4 col-md-4">
		<h2 href="#comments" data-toggle="collapse" class="heading">Hỏi đáp, thảo luận</h2>
		<div id="comments" class="collapse">
			<div class="fb-comments" data-width="100%" data-numposts="5"></div>
		</div>
	</div>
</div>
<div id="confirmModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Hệ thống</h4>
                </div>
                <div class="modal-body">
                    <p>Hệ thống chỉ chấp nhận kết quả lần gửi bài đầu tiên.<br/>
                    Bạn chắc chắn gửi bài nhấn "Đồng ý"!
               		</p>
               		<button type="button" class="btn btn-sm btn-white pull-right" data-dismiss="modal">Hủy bỏ</button>
               		<button type="button" id="submit-modal" class="btn btn-sm btn-info pull-right">Đồng ý</button>
                </div>
            </form>
        </div>
    </div>
</div>
	<div id="fb-root"></div>
	<div class="overlay-black" style="display: none">
	    <h3>Đang xử lý...</h3>
	</div>
	<script>
		window.fbAsyncInit = function() {
	        FB.init({
	          appId : '<?php echo APP_ID?>', 
	          cookie:true, 
	          status:true,
	          xfbml : true,
	          version    : 'v2.5'
	        });
	    };
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		var _url_result = "<?php echo base_url('exam/result');?>";
		var _url_save = "<?php echo base_url('exam/saveExam');?>";
		var _data_exam_id = "<?php echo $exam->id; ?>";
		var _essay = "<?php echo $exam->attribute;?>";
	</script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery-2.1.4.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/owl.carousel.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script> 
	<script src="<?php echo base_url();?>assets/js/jquery.easy-ticker.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>	 
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/custom.js"></script>
	<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>
	<!-- exam detail load -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/exam.detail.js"></script>
	
	<!-- Toastr script -->
    <script src="<?php echo base_url();?>assets/js/toastr/toastr.min.js"></script>
    <script type="text/javascript">
    	

		jQuery(document).keydown(function(event){
		    if(event.keyCode==123){
		    return false;
		   }
		else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
		      return false;
		   }
		});

		jQuery(document).on("contextmenu",function(e){        
		   e.preventDefault();
		});

		// slight update to account for browsers not supporting e.which
		function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
		// To disable f5
		    /* jQuery < 1.7 */
		jQuery(document).bind("keydown", disableF5);
		/* OR jQuery >= 1.7 */
		jQuery(document).on("keydown", disableF5);

		// To re-enable f5
		    /* jQuery < 1.7 */
		jQuery(document).unbind("keydown", disableF5);
		/* OR jQuery >= 1.7 */
		jQuery(document).off("keydown", disableF5);

		document.onkeydown = function (e) {
		  if (e.keyCode === 116) {
		    e.preventDefault();
		 }
		};
    </script>

<script type="text/javascript">
    window.onbeforeunload = function(e) {
        e.preventDefault();
    }
</script>    
</body>
</html>