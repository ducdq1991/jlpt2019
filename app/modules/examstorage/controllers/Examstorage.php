<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examstorage extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('breadcrumbs');
		$this->load->model('examstorage_model');
        $this->output->cache($this->config->item('time_cache'));
	}

	public function index( $page = 0 )
	{
        $this->setCurrentUrl();
        $data = $where = $orderBy = $orderByProv = $whereIn = array();
        $this->load->helper('paging');
        $data['yearPublish'] = '';
        $data['provinceId'] = '';
    	//create breadcrumb
        $head = array();
    	$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'kho-de');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
        $head['metas'] = $this->getMetas();
        //list provices
        $orderByProv['key'] = 'provinces.prov_name';
        $orderByProv['value'] = 'ASC';
        $provinces = $this->examstorage_model->getProvinces([], $orderByProv);        
        $data['provinces'] = $provinces;
        //get provinces & year of exam
        $yearPublish = $this->input->get('year');
        $provinceId = $this->input->get('provinces');
        if ( $yearPublish != '' ) {
               $where['year_publish'] = $yearPublish;
               $data['yearPublish'] = $yearPublish;
        } 
        if( $provinceId != '' ) {
                $where['prov_id'] = $provinceId;
                $data['provinceId'] = $provinceId;
        }
        //list documents
        $where['status'] = 1;
        $orderBy['key'] = 'exams_storage.id';
        $orderBy['value'] = 'DESC';
        $limit = 25;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->examstorage_model->totalExam( $where );
        $listItems = $this->examstorage_model->getExams( $where, $whereIn, $limit, $offset, $orderBy );
        //print_r($documents); die;
        $data['listItems'] = $listItems;
        //pagination
        $data['paging'] = paging( base_url() .'kho-de', $page, $total, $limit );
        //list categories
        $categories = $this->examstorage_model->getCategoriesExam( $where = array('status' => 1) );
        $data['categories'] = $categories;
        //Title page exam 
        $data['category'] = '';
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        //get bannerAds center
        $type = TYPE_ADS;
        $positionAds = POSITION_7;
        $adsPosition7 = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsPosition7) ){
            $data['adsPosition7'] = $adsPosition7;
        }

        //get featrured
        $featrured = $this->blockFeatured();
        $data['lastestPosts'] = $featrured['lastestPosts'];
        $data['examOnline'] = $featrured['examOnline'];
        $metas = $this->getMetas();
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Kho đề';

        // load view
        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/index', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
	}
    /**
    * [exam with category]
    **/
    public function category( $cateSlug = '', $page = 0 )
    {
        $this->setCurrentUrl();
        $this->load->helper('paging');
        $data = $where = $orderBy = $orderByProv = array();
        $data['yearPublish'] = '';
        $data['provinceId'] = '';
        if($cateSlug == ''){
            redirect(base_url());
        }
        // get category
        $cateWhere = array();
        $cateWhere['exam_categories.slug'] = $cateSlug;   
        $cateWhere['exam_categories.status'] = 1;
        $category = $this->examstorage_model->getCategoryBySlug( $cateWhere );
        if ( empty($category )) {
                redirect(base_url());
        }
        //Get category child
        $whereCateChild = array(
            'parent' => $category->id
            );
        $categoryChild = $this->examstorage_model->getCategoriesExam( $whereCateChild );
        if( $categoryChild ){
            $idCateChild = array();
            foreach ($categoryChild as $key=>$cateChild) {
               $idCateChild[] = $cateChild->id;
            }
            $idCateParent = array('' => $category->id);  
            $idCate = array_merge( $idCateChild, $idCateParent);
        } else {
            $idCate = array();
        }
        //Get data category
        $data['category'] = $category; 
        //Set breadcrumb
        $head = array();
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'kho-de');
        $this->breadcrumbs->add( ucfirst($category->name), base_url().'kho-de'.$category->slug );
        $head['breadcrumb'] = $this->breadcrumbs->display();
        $metaGeneral = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($category->meta_keywords) ? $category->meta_keywords : $metaGeneral['meta_keyword'],
            'meta_description' => !empty($category->meta_description) ? $category->meta_description : $metaGeneral['meta_description'],
        );
        $head['metas'] = $metas;     
        $head['metas']['meta_title'] = $category->name;        
        //list provices
        $orderByProv['key'] = 'provinces.prov_id';
        $orderByProv['value'] = 'ASC';
        $provinces = $this->examstorage_model->getProvinces( $where, $orderByProv );
        $data['provinces'] = $provinces;
        //get provinces & year of exam
        $yearPublish = $this->input->get('year');
        $provinceId = $this->input->get('provinces');
        if ( $yearPublish != '' ) {
               $where['year_publish'] = $yearPublish;
               $data['yearPublish'] = $yearPublish;
        } 
        if( $provinceId != '' ) {
                $where['prov_id'] = $provinceId;
                $data['provinceId'] = $provinceId;
        }
        //list documents
        $where['status'] = 1;
        $where['category_id'] = $category->id;
        $orderBy['key'] = 'exams_storage.id';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
                $offset = ($page - 1) * $limit;
        $total = $this->examstorage_model->totalExam( $where );
        $listItems = $this->examstorage_model->getExams( $where, $idCate, $limit, $offset, $orderBy );
        //print_r($listItems); die;
        $data['listItems'] = $listItems;
        //pagination
        $data['paging'] = paging( base_url() .'kho-de/'. $category->slug, $page, $total, $limit );
        //list categories
        $categories = $this->examstorage_model->getCategoriesExam( $where = array( 'status' => 1 ) );
        $data['categories'] = $categories;
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }

        //get featrured
        $featrured = $this->blockFeatured();
        $data['lastestPosts'] = $featrured['lastestPosts'];
        $data['examOnline'] = $featrured['examOnline'];  

        // load view
        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/index', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
    /**
    ** [details exam]
    **/
	public function detail( $slug = '' )
    {
        $this->setCurrentUrl();
        $data = $orderBy = $whereIn = array();
        $where = array('slug' => $slug);
        $limit = 1;
        $offset = 0;
        $examDetail = $this->examstorage_model->getExams( $where, $whereIn, $limit, $offset );
        if ( empty($examDetail) ) {
               redirect(base_url());
        }
        $data['examDetail'] = $examDetail;
        $configCookie = array(
                    'name' => 'cookieView',
                    'value' => json_encode($examDetail),
                    'expire' => 24 * 3600 * 30 * 12,
                    'path'   => $this->config->item('cookie_path'),
                    'prefix' => $this->config->item('cookie_prefix'),
        );               
        //set breadcrumb
        $head = array();
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'kho-de');
        $this->breadcrumbs->add( ucfirst($examDetail[0]->title), base_url().'kho-de'.$examDetail[0]->slug); 
        $head['breadcrumb'] = $this->breadcrumbs->display();
        
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($examDetail->meta_keywords) ? $examDetail->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($examDetail->meta_description) ? $examDetail->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = $examDetail[0]->title;   
        //get banner position 4
        $typeBanner = TYPE_ADS;
        $positionBanner = POSITION_4;
        $bannerPosition4 = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
        if (!empty($bannerPosition4)) {
            $data['bannerPosition4'] = $bannerPosition4[0];
        }   
        //get banner position 5
        $typeBanner = TYPE_ADS;
        $positionBanner = POSITION_5;
        $bannerPosition5 = $this->getBanners( $positionBanner, $typeBanner, $limit = 1 );
        if (!empty($bannerPosition5)) {
            $data['bannerPosition5'] = $bannerPosition5[0];
        }   
        //load view
        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/detail', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }
}