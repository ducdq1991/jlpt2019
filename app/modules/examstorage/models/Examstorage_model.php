<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examstorage_model extends Base_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	/**
	* [get Exams]
	**/
	public function getExams( $where = array(), $whereIn = array(), $limit = 0, $offset = 0, $orderBy = array() )
	{
		try {
			$this->db->select('exams_storage.id, exams_storage.title, exams_storage.slug, exams_storage.question, exams_storage.answer, exams_storage.category_id, exams_storage.prov_id,exams_storage.year_publish');
			if ( !empty($where) ) {
				$this->db->where( $where );
			}
			if ( !empty($whereIn) ) {
				$this->db->or_where_in('category_id', $whereIn );
			}
			$this->db->limit($limit, $offset);
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get('exams_storage');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	** [count all record Exam]
	**/
	public function totalExam( $where = array() )
	{
		try{			
			return $this->db->where($where)->count_all_results('exams_storage');
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get provinces]
	**/
	public function getProvinces( $where = array(), $orderBy = array() )
	{
		try {
			$this->db->select('prov_id, prov_name');
			if( !empty($where) ) {
				$this->db->where($where);
			}
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get('provinces');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get category Exam with slug]
	**/
	public function getCategoryBySlug( $where = array() )
	{
		try {
			$this->db->select('exam_categories.id, exam_categories.name, exam_categories.slug,exam_categories.parent, exam_categories.meta_keywords, exam_categories.meta_description');
			$this->db->where($where);
			$query = $this->db->get('exam_categories');
			$result = $query->result();
			if( $result ) {
				return $result[0];
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
	/**
	* [get categories Exam]
	**/
	public function getCategoriesExam( $where = array() )
	{
		try {
			$this->db->select('exam_categories.id, exam_categories.name, exam_categories.slug');
			if( !empty($where) ){
				$this->db->where( $where );
			}
			$query = $this->db->get('exam_categories');
			if( $query ) {
				return $query->result();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
		return null;
	}
}