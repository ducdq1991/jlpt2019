<div id="document-module" class="module module-exam-store">
  <div class="container">
    <div class="col-md-6 col-sm-6 col-xs-12 list-docs">
      <div class="box-heading">
        <?php if( $category ) : ?>
          <h1><?php echo $category->name; ?></h1>
        <?php else: ?> 
          <h1>Kho đề</h1>
        <?php endif; ?> 
        <div class="dropdown dropdownlistexamination ">
          <form action="" method="get">
            <select name="provinces" class="form-control provinces" onchange="this.form.submit()">
                <option value="">Level</option>
              <?php foreach ($provinces as $item) : ?>
                <option <?php if($item->prov_id == $provinceId):?> selected="selected" <?php endif;?> value="<?php echo $item->prov_id; ?>"><?php echo $item->prov_name; ?></option>
              <?php endforeach; ?>            
            </select>
            <select name="year" class="form-control year" onchange="this.form.submit()">
              <option value="">Năm</option>
              <?php for ($i = date('Y'); $i >= 2007; $i-- ) : ?>
              <option <?php if($i == $yearPublish):?> selected="selected" <?php endif;?> value="<?php echo $i;?>"><?php echo $i; ?></option>
              <?php endfor; ?>            
            </select>
          </form>
        </div>
      </div>
      <?php 
        if( $listItems ):
          $number = 1; foreach ($listItems as $item) : ?>
            <div class="box-item">
              <h2><i class="fa fa-file-pdf-o"></i> 
                <a href='<?php echo base_url();?>kho-de/<?php echo $item->slug;?>.html'>
                  <?php echo $item->title; ?>
                </a>
              </h2>
            </div>
        <?php $number++; endforeach; else: ?>  
            <div class="box-item no_result text-center">
                Đề thi trong mục này chưa được cập nhật!
            </div>
      <?php endif; ?>
      <?php if( $paging ) : ?>
        <div class="pagination">
          <?php echo $paging;?>
        </div>	  
      <?php endif; ?>           	 
    </div>
    <?php $this->load->view('examstorage/sidebar'); ?>
  </div>
</div><!--end container section -->
