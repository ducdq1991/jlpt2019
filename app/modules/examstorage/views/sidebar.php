<div class="col-md-3 col-sm-3 col-xs-12 doc-category">
  <div class="list-txt">
    <h1><span class="list"><img src=""></span>Danh mục</h1>
    <ul>
      <?php foreach ($categories as $item) : ?>
        <li>
          <a href="<?php echo base_url();?>kho-de/<?php echo $item->slug;?>">
            <span class="arrow"><i class="fa fa-file-pdf-o"></i></span>
            <?php echo $item->name; ?>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
  <!-- ads-->
  <?php $this->load->view('block/ads_examstorage');?>
</div>
<div id="competition" class="col-md-3 col-sm-3 col-xs-12">
  <?php $this->load->view('block/ads_right_top');?>
  <?php $this->load->view('block/featured'); ?>
  <?php $this->load->view('block/ads_right_bottom');?> 
</div><!--Competition-->