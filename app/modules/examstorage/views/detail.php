<div id="document-detail">
	<?php $this->load->view('block/ads_left_outer'); ?>
	<div class="container">
		<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 no-margin">
			<?php foreach ( $examDetail as $item ) : ?>
				<div class="detail-top">
					<div class="box-heading">
						<h1><?php echo $item->title; ?></h1>
					</div>
					<div class="box-content">
					    <div class="item">
					    	<iframe src="https://docs.google.com/gview?url=<?php echo base_url().$item->question;?>&embedded=true" style="width:100%; height:100%;" frameborder="0"></iframe>
					    </div>
					</div>
				</div>
				<div class="detail-bottom">
					<a href="<?php echo base_url().$item->question;?>" id="exam_question" class="btn btn-primary" download>Download đề thi</a>
					<a href="<?php echo base_url().$item->answer;?>" id="exam_answer" class="btn btn-primary" download>Download bài giải</a>
				</div>
			</div>
		<?php endforeach; ?>	
	</div>
	<?php $this->load->view('block/ads_right_outer');?>
</div><!-- end #document-detail -->