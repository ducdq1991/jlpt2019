<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('search_model');
        $this->load->library('breadcrumbs');
        $this->output->cache($this->config->item('time_cache'));        
	}
	public function index()
	{
		$data = $head = $where = $whereExam = [];
		$keyword = $this->input->get('keyword');
		if ($keyword == "") {
			$this->session->set_flashdata('error', 'Bạn vui lòng nhập vào từ khóa cần tìm kiếm');
			redirect(base_url(). 'notfound');
		}

		$keyword = trim($keyword);
		$data['keyword'] = $keyword;	

		//get news search
		$tableNews = 'post_details';
		$searchNews = $this->search_model->searchKeyWord($where, $keyword, $tableNews);
		$data['searchNews'] = $searchNews;

		//get courses search
		$searchCourses = $this->search_model->searchCourses($keyword);
		$data['searchCourses'] = $searchCourses;

		//get exam
		$searchExamStorage = $this->search_model->searchKeyWord($where, $keyword, 'exams_storage');
		$data['searchExamStorage'] = $searchExamStorage;

		$whereExam['type !='] = 'online-exam';
		$searchExam = $this->search_model->searchKeyWord($whereExam, $keyword, 'exam');
		$data['searchExam'] = $searchExam;

		//get document
		$searchDocument = $this->search_model->searchKeyWord($where, $keyword, 'documents');
		$data['searchDocument'] = $searchDocument;

		//set breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Tìm kiếm', base_url().'tim-kiem');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners($positionAds, $type, $limit = 5);
        if (!empty($adsRightBottom)) {
            $data['adsRightBottom'] = $adsRightBottom;
        }

        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners($positionAds, $type, $limit = 5);
        if (!empty($adsRightTop)) {
            $data['adsRightTop'] = $adsRightTop;
        }

        $metas = $this->getMetas(); 
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Tìm kiếm'; 

		//load view
		$this->load->view('layouts/header', $head);
		$this->load->view('search/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

}