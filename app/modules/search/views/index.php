<div id="search-module">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12">
				<h2 class="title-page"><i class="fa fa-search"></i> Kết quả tìm kiếm từ khóa: "<span><?php echo $keyword; ?></span>"</h2>
				<div class="list-search">
					<?php if( !empty($searchNews) ): ?>
						<div class="list-news-search">
							<h3><i class="fa fa-newspaper-o"></i> Tin tức</h3>	
							<div class="list-item">
								<?php foreach( $searchNews as $item ) : ?>
									<div class="item">
										<h4>
											<a href="<?php echo base_url();?>tin-tuc/<?php echo $item->slug;?>.html" title="">
												<i class="fa fa-dot-circle-o"></i> <?php echo $item->title; ?>
											</a>
										</h4>
									</div>
								<?php endforeach; ?>	
							</div>
						</div><!--search news-->
					<?php endif; ?>
					<?php if( !empty($searchCourses) ) : ?>	
						<div class="list-news-search">
							<h3><i class="fa fa-cubes"></i> Khóa học</h3>	
							<div class="list-item">
								<?php foreach( $searchCourses as $item ) : ?>
									<div class="item">
										<h4>
											<a href="<?php echo base_url();?>khoa-hoc/<?php echo $item->slug;?>" title="">
												<i class="fa fa-dot-circle-o"></i> <?php echo $item->name;?>
											</a>
										</h4>
									</div>
								<?php endforeach; ?>	
							</div>
						</div><!--search course-->
					<?php endif; ?>	
					<?php if( !empty($searchExamStorage) || !empty($searchExam) ):?>
						<div class="list-news-search">
							<h3><i class="fa fa-database"></i> Đề thi</h3>	
							<div class="list-item">
								<?php foreach( $searchExamStorage as $item ) :?>
									<div class="item">
										<h4>
											<a href="<?php echo base_url();?>kho-de/<?php echo $item->slug;?>.html" title="">
												<i class="fa fa-dot-circle-o"></i> <?php echo $item->title;?>
											</a>
										</h4>
									</div>
								<?php endforeach; ?>
								<?php foreach( $searchExam as $item ) :?>
									<div class="item">
										<h4>
											<a href="<?php echo base_url();?>luyen-tap/<?php echo $item->slug;?>.html" title="">
												<i class="fa fa-dot-circle-o"></i> <?php echo $item->title;?>
											</a>
										</h4>
									</div>
								<?php endforeach; ?>	
							</div>
						</div><!--search exam question-->
					<?php endif; ?>	
					<?php if( !empty($searchDocument) ) : ?>
						<div class="list-news-search">
							<h3><i class="fa fa-file-pdf-o"></i> Tài liệu</h3>	
							<div class="list-item">
								<?php foreach( $searchDocument as $item ) : ?>
									<div class="item">
										<h4>
											<a href="<?php echo base_url();?>tai-lieu/<?php echo $item->slug;?>.html" title="">
												<i class="fa fa-dot-circle-o"></i> <?php echo $item->title; ?>
											</a>
										</h4>
									</div>
							<?php endforeach; ?>
							</div>
						</div><!--search exam document-->
					<?php endif; ?>
					<?php if( empty($searchNews) && empty($searchCourses) && empty($searchExamStorage) && empty($searchExam) && empty($searchDocument) ) : ?>		
						<div class="item">
							<h4>Không có kết quả tìm kiếm nào</h4>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-3 col-xs-12">
				<?php 
					$this->load->view('block/ads_right_top');
					$this->load->view('block/ads_right_bottom');
				?>
			</div>
		</div>
	</div>
</div>