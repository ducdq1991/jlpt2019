<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs_model extends Base_model {

	public function __construct()
	{
		parent::__construct();
	}
	/**
	* [get faqs]
	**/
	public function getFaqs( $where = [], $limit = 0, $offset = 0, $orderBy = [] )
	{
		try {
			$this->db->select('id, title, question, answer, slug, faq_category_id, link_video');
			if ( !empty($where) ) {
				$this->db->where( $where );
			}
			$this->db->limit($limit, $offset);
			if( !empty($orderBy) ) {
				$this->db->order_by( $orderBy['key'], $orderBy['value'] );
			}
			$query = $this->db->get('faqs');
			if ($query) {
				$result = $query->result();
				return $result;
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}
	/**
	* [total faqs]
	**/
	public function totalFaqs( $where = [] )
	{
		try{			
			return $this->db->where($where)->count_all_results('faqs');
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}
	/**
	* [get category faqs]
	**/
	public function getCategory( $where = [], $limit = 0 )
	{
		try {
			$query = $this->db->select('id, name, slug, parent_id')
					->where($where)
					->limit($limit)
					->order_by( 'faq_categories.id', 'ASC' )
					->get('faq_categories');
			if( $query ) {
				return $query->result();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}

	public function addFaqs($args = [])
	{
		try {
			if($this->db->insert('faqs', $args)) {
				return $this->db->insert_id();
			}
			return false;
		} catch( Exception $ex ){
			$this->writeLog($ex->getMessage());
		}
	}	
	public function updateFaq( $where = array(), $args = array() )
    {
        try {
    		if($this->db->where($where)->update('faqs',$args)){
    			return true;
    		}
	    	return false;
    	} catch (Exception $ex) {
    		echo $ex->getMessage(); 
    		return false; 
    	}
    }
}