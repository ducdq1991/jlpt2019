<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends Base_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('faqs_model');
		$this->load->library('breadcrumbs');
        $this->output->cache($this->config->item('time_cache'));	
	}
	/**
	* [list faqs]
	**/
	public function index( $page = 0 )
	{
		$this->setCurrentUrl();
		$this->load->helper('paging');
		$head = $data = $where =  $orderBy = [];
		// Set Breadcrumbs
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Hỏi đáp', base_url().'hoi-dap');
        $head['breadcrumb'] = $this->breadcrumbs->display();
        // get pagination
        $where['status'] = 1;
        $orderBy['key'] = 'faqs.id';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $total = $this->faqs_model->totalFaqs( $where );
        //get list faqs
        $faqs = $this->faqs_model->getFaqs( $where, $limit, $offset, $orderBy );
        $data['faqs'] = $faqs;
        $data['paging'] = paging( base_url() .'hoi-dap', $page, $total, $limit );
        //get category
        $categories = $this->faqs_model->getCategory( $where, $limit = 0 ); 
        $data['categories'] = $categories;
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        //get bannerAds postion 6, center faqs
        $type = TYPE_ADS;
        $positionAds = POSITION_6;
        $adsPosition6 = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsPosition6) ){
            $data['adsPosition6'] = $adsPosition6;
        }

        $metas = $this->getMetas();
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Hỏi đáp'; 

		//load view
		$this->load->view('layouts/header', $head);
		$this->load->view('faqs/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
	/**
	* [faqs with category]
	**/
	public function category( $cateSlug = '', $page = 0 )
	{
		$this->setCurrentUrl();
		$this->load->helper('paging');
		$data = $head = $whereCat = $where = $orderBy = [];
		//get faqs with slug category
		$whereCat['faq_categories.status'] = 1;
		$whereCate['faq_categories.slug'] = $cateSlug;
		$category = $this->faqs_model->getCategory( $whereCate, $limit = 1 )[0];
		if ( empty($category )) {
                redirect(base_url().'hoi-dap');
        }
		//Get category child
        $whereCateChild = array(
            'parent_id' => $category->id
            );
        $categoryChild = $this->faqs_model->getCategory( $whereCateChild );
        if( $categoryChild ){
            $idCateChild = array();
            foreach ($categoryChild as $key=>$cateChild) {
               $idCateChild[] = $cateChild->id;
            }
            $idCateParent = array('' => $category->id);  
            $idCate = array_merge( $idCateChild, $idCateParent);
        } else {
            $idCate = array();
        }
        //Get data category
        $data['category'] = $category;
        //pagination
        $where['status'] = 1;
        $where['faq_category_id'] = $category->id;
        $orderBy['key'] = 'faqs.id';
        $orderBy['value'] = 'DESC';
        $limit = 10;   
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $total = $this->faqs_model->totalFaqs( $where );
        //list faqs
        $faqs = $this->faqs_model->getFaqs( $where, $limit, $offset, $orderBy );
        $data['faqs'] = $faqs;
        $data['paging'] = paging( base_url() .'hoi-dap/'.$category->slug, $page, $total, $limit );
        //list categories
        $categories = $this->faqs_model->getCategory( $where = ['status' => 1], $limit = 0 ); 
        $data['categories'] = $categories;
        //Set breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Hỏi đáp', base_url().'hoi-dap');
        $this->breadcrumbs->add( ucfirst($category->name), base_url().'hoi-dap'.$category->slug );
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($category->meta_keywords) ? $category->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($category->meta_description) ? $category->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = $category->name; 

        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }
		//load view
		$this->load->view('layouts/header', $head);
		$this->load->view('faqs/index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}
	/**
	* [faqs detail]
	**/
	public function detail( $slug = '' )
	{
		$this->setCurrentUrl();
		$data = $head = $where = $orderBy = [];
		$limit = 1;
		$offset = 0;
		$where['faqs.slug'] = $slug;
        $where['faqs.status'] = 1;
		$faq = $this->faqs_model->getFaqs( $where, $limit, $offset, $orderBy )[0];
		if ( empty($faq) ) {
			$this->session->set_flashdata('error', 'Hỏi đáp này không tồn tại');
            redirect(base_url().'hoi-dap');
        }
		$data['faq'] = $faq;
		//list categories
        $categories = $this->faqs_model->getCategory( $where = ['status' => 1], $limit = 0 ); 
        $data['categories'] = $categories;
        //related post
        $where = [
            'faq_category_id' => $faq->faq_category_id,
            'status' => 1,
            'id !=' => (int) $faq->id,
        ];
        $orderBy = ['key'=>'title', 'value'=> 'RANDOM'];
        $relFaqs = $this->faqs_model->getFaqs( $where, $limit = 5, $offset = 0, $orderBy );
        $data['relFaqs'] = $relFaqs;
		//set breadcrumb
        $this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Hỏi đáp', base_url().'hoi-dap');
        $this->breadcrumbs->add( ucfirst($faq->title), base_url().'hoi-dap/'.$faq->slug); 
        $head['breadcrumb'] = $this->breadcrumbs->display();
        //get bannerAds right
        $type = TYPE_ADS;
        $positionAds = RIGHT_BOTTOM;
        $adsRightBottom = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightBottom) ){
            $data['adsRightBottom'] = $adsRightBottom;
        }
        $type = TYPE_ADS;
        $positionAds = RIGHT_TOP;
        $adsRightTop = $this->getBanners( $positionAds, $type, $limit = 5 );
        if( !empty($adsRightTop) ){
            $data['adsRightTop'] = $adsRightTop;
        }

        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => !empty($faq->meta_keywords) ? $faq->meta_keywords : $metas['meta_keyword'],
            'meta_description' => !empty($faq->meta_description) ? $faq->meta_description : $metas['meta_description'],
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = $faq->title; 

		//load view
		$this->load->view('layouts/header', $head);
		$this->load->view('faqs/detail', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	} 

    public function sendQuestion()
    {
        if(!$this->session->has_userdata('web_user')) {
            $res = [
                'code' => 201,
                'msg' => 'Ban phải đăng nhập'
            ];
            echo json_encode($res);die;
        }
        $user = $this->session->userdata('web_user');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        if (empty($content)) {
            $res = [
                'code' => 201,
                'msg' => 'Bạn phải điền nội dung câu hỏi'
            ];
        } elseif (empty($title)) {
            $res = [
                'code' => 201,
                'msg' => 'Bạn phải điền tiêu đề'
            ];
        } else {
            $this->load->library('string');
            $slug = $this->string->sanitizeTitle($title);
            $args = [
                'question' => $content,
                'user_id' => (int) $user->id,
                'status' => 0,
                'title' => $title,
                'slug' => $slug,
            ];
            $id = $this->faqs_model->addFaqs($args);
            if ($id) {
                $hash_id = md5($id);
                $args['hash_id'] = $hash_id;
                $this->faqs_model->updateFaq(['id' => $id], $args);
                $res = [
                    'code' => 200,
                    'msg' => 'Câu hỏi đã gửi, đang chờ phê duyệt'
                ];
            } else {
                $res = [
                    'code' => 202,
                    'msg' => 'Không thể gửi câu hỏi'
                ];
            }
        }
        echo json_encode($res);die;
    }
}