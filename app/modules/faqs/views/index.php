<div id="faqs-module" class="module module-faqs">
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-sm-5 col-xs-12">
        <h1 class="page-title"> Hỏi đáp</h1>
        <div class="list-faqs">
          <div class="list-item">
            <?php if( !empty($faqs) ) : ?>
              <?php foreach( $faqs as $row ):?>
                <?php $class_answer = empty($row->answer) ? 'not_answered' : 'answered'; ?>
                <div class="item">
                  <h4>
                    <a class="<?php echo $class_answer;?>" href="<?php echo base_url();?>hoi-dap/<?php echo $row->slug;?>.html" title="">
                      <?php echo '- '.$row->title; ?>
                    </a>
                  </h4>
                </div>
              <?php endforeach; ?>  
            <?php else: ?> 
               <div class="item">
                <h4><a href="" title="">Chưa có hỏi đáp nào!</a></h4>
              </div>
            <?php endif; ?>  
          </div>
        </div>
        <?php if( $paging ) : ?>
          <div class="pagination">
            <?php echo $paging;?>
          </div>	  
        <?php endif; ?> 
        <div class="question-request">
          <h4>Đặt một câu hỏi cho thầy</h4>
          <form id="user-request-form" method="POST">
            <input type="text" name="title" class="form-control" placeholder="Tiêu đề (*)">
            <br/>
            <textarea id="content" name="content" rows="5" cols="80" class="form-control" placeholder="Nội dung câu hỏi (*)"></textarea>
            <br/>
            <button type="button" id="send-question" class="logged btn btn-warning">Gửi câu hỏi</button>
          </form>
        </div>          	 
      </div>
      <div class="col-md-4 col-sm-4 col-xs-12">
        <?php $this->load->view('block/ads_faqs');?>
      </div>
      <?php $this->load->view('faqs/sidebar'); ?>
    </div>
  </div>
</div>
