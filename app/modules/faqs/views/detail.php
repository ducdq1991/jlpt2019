<div id="faqs-module" class="module module-faqs">
	<div class="container">
		<div class="row">
			<div class="faqs-detail col-md-9 col-sm-9 col-xs-12">
				<h1 class="page-title"><?php echo $faq->title;?></h1>
				<div class="content">
					<div class="question"><?php echo $faq->question; ?></div>
					<?php if($faq->answer || $faq->link_video) :?>
						<div class="answer">
							<h4>Trả lời: </h4>
							<?php
	                            if (isset($faq->link_video) && ($faq->link_video != '' || $faq->link_video != NULL)):
	                                $videoId = @end(explode('watch?v=', $faq->link_video));
	                        ?>
	                        <script type="text/javascript" src="<?php echo base_url();?>assets/java/pageMinimalDark.js"></script>
	                        <script type="text/javascript" src="<?php echo base_url();?>assets/java/FWDEVPlayer.js"></script>
	                        <script type="text/javascript">
	                            FWDEVPUtils.onReady(function(){
	                                var videoId = '<?php echo $videoId;?>';
	                                init(videoId);
	                            });
	                        </script>
	                        <div id="productWrapper" class="faq-video" oncontextmenu="return false">
	                            <div id="myDivMainHolder2">
	                                <div id="myDiv2" style="width:100%"></div>
	                            </div>
	                            <div id="bigshare_overlay_logo">
	                                <img src="<?php echo base_url();?>/assets/content/minimal_skin_dark/logo.png" alt="">
	                            </div>
	                        </div>
	                        <?php endif;?>
							<?php echo $faq->answer; ?>
						</div>
					<?php else: ?>
						<p style="font-style:italic">Chưa có trả lời</p>
					<?php endif;?>
				</div>
				<div class="related-qes">
					<h1><i class="fa fa-list-alt"></i> Hỏi đáp cùng chủ đề</h1>
					<div class="list-item">
						<?php foreach ($relFaqs as $item) :?>
							<?php $class_answer = empty($item->answer) ? 'not_answered' : 'answered'; ?>
							<div class="item">
								<h4 class="rel-title">
									<a class="<?php echo $class_answer;?>" href="<?php echo base_url();?>hoi-dap/<?php echo $item->slug;?>.html" title="<?php echo $item->title;?>">
										<?php echo '- '.$item->title;?>
									</a>	
								</h4>
							</div>
						<?php endforeach;?>
					</div>
				</div>
				<div class="question-request">
			        <h4>Đặt một câu hỏi cho thầy</h4>
			        <form id="user-request-form" method="POST">
						<input type="text" name="title" class="form-control" placeholder="Tiêu đề (*)">
						<br/>
						<textarea id="content" name="content" rows="5" cols="80" class="form-control" placeholder="Nội dung câu hỏi (*)"></textarea>
						<br/>
						<button type="button" id="send-question" class="logged btn btn-warning">Gửi câu hỏi</button>
			        </form>
			     </div>   
			</div><!--faqs detail-->
			<?php $this->load->view('faqs/sidebar'); ?>
			<!--endloadSidebar-->
		</div>
	</div><!--end.container-->
</div><!--end#faqs-module-->	