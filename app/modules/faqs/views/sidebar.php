<div class="faq-side col-md-3 col-sm-3 col-xs-12">
  <?php $this->load->view('block/ads_right_top'); ?>
  <div class="list-txt">
    <h1><span class="list"></span>Danh mục câu hỏi</h1>
    <ul>
      <?php if( !empty($categories) ) : ?>
        <?php foreach ($categories as $item) : ?>
          <li>
            <a href="<?php echo base_url();?>hoi-dap/<?php echo $item->slug;?>">
              <span class="arrow"></span>
              <?php echo $item->name; ?>
            </a>
          </li>
        <?php endforeach; ?>
      <?php endif; ?>  
    </ul>
  </div>
    <!--ads banner right bottom-->
    <?php $this->load->view('block/ads_right_bottom'); ?>
</div>