<section class="page-content">
	<div class="container">
		<div class="row cart-page">
			<div class="col-md-12">
				<h1 class="page-title" style="font-size: 16px;"><i class="fa fa-tags"></i> Thông báo</h1>
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
            </div>
            <div class="col-md-12 text-center" style="margin-bottom: 30px;">
            	<a href="<?php echo base_url();?>" class="btn btn-primary">Tiếp tục mua hàng</a>
            </div>
        </div>
    </div>
</section>