<?php
if (isset($breadcrumbs)):
	?>
    <div class="brd">
        <div class="container">
            <div class="row">
                <div class="inner">
                    <?php echo $breadcrumbs;?>
                </div>
            </div>
        </div>
    </div>
    <?php 
endif;
?>
        <section class="page-content">
            <div class="container">
                <div class="row cart-page">
                    <div class="col-md-12">
                        <h1 class="page-title" style="font-size: 16px;"><i class="fa fa-shopping-cart"></i> Giỏ hàng</h1>
                        <?php if (count($this->cart->contents()) > 0):?>
                            <?php echo form_open(base_url() . 'cart/update'); ?>
                                <div class="table-responsive">
                                    <table cellpadding="6" cellspacing="1" style="width:100%; background: #fff" border="0" class="table-bordered table-condensed table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Hình ảnh</th>
                                                <th class="text-center">Sản phẩm</th>
                                                <th class="text-right">Đơn giá (<sup>đ</sup>)</th>
                                                <th class="text-center col-quantity">Số lượng</th>
                                                <th class="text-right">Thành tiền (<sup>đ</sup>)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                                <?php 
								foreach ($this->cart->contents() as $items): ?>
                                                    <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                                                        <tr>
                                                            <td class="text-center">
                                                                <?php 
											if (strlen($items['image']) > 0):
												?>
                                                                    <img class="cart-pro-image" style="max-width: 50px" src="<?php echo base_url();?><?php echo $items['image'];?>" alt="">
                                                                    <?php 
											endif;
											?>
                                                            </td>
                                                            <td>
                                                                <?php echo $items['name']; ?>
                                                                    <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                                                        <p>
                                                                            <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                <strong><?php echo $option_name; ?>:</strong>
                                                                                <?php echo $option_value; ?>
                                                                                    <br />
                                                                                    <?php endforeach; ?>
                                                                        </p>
                                                                        <?php endif; ?> - <a href="<?php echo base_url();?>cart/deleteone/<?php echo $items['rowid'];?>" title=""><i class="fa fa-trash-o"></i></a>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo number_format($items['price']); ?>
                                                            </td>
                                                            <td class="text-right">
                                                                <?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5', 'class' => 'form-control cart-quantity', 'type' => 'number')); ?>
                                                            </td>
                                                            <td class="text-right subtotal">
                                                                <?php echo number_format($items['subtotal'], 0);?>
                                                            </td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="3">Mã giảm giá:
                                                    <input type="text" name="discount_code" placeholder="" style="border:1px solid #d5d5d5">
                                                    <div class="discountPrice pull-right" style="color:red"></div>
                                                </td>
                                                <td class="text-right"><strong>Tổng tiền</strong></td>
                                                <td class="text-right totalPayment">
                                                    <span><?php echo number_format($this->cart->total()); ?></span> <sup>đ</sup></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            Giỏ hàng của bạn chưa có sản phẩm nào!. Bạn muốn mua hàng ngay bây giờ không? <a href="<?php echo base_url();?>">Tôi muốn mua hàng</a>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                        <div class="row">
                                            <div class="text-center">
                                                <div class="cartBtn">
                                                    <div class="col-md-6 pull-left text-left">
                                                        <a class="btn btn-warning" href="<?php echo base_url() . 'thu-vien-sach';?>">Tiếp tục đặt hàng</a>
                                                    </div>
                                                    <div class="col-md-6 pull-right text-right">
                                                        <a href="<?php echo base_url();?>cart/emptycart" class="btn btn-danger"><i class="fa fa-remove"></i> Hủy bỏ</a>
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i> Cập nhật</button>
                                                        <a class="btn btn-primary btn-dathang" data-toggle="modal" data-target="#orderFormCart"><i class="fa fa-money"></i> Đặt hàng</a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_close();?>
                    </div>

                </div>
            </div>
        </section>
        <div class="modal fade cartPayment" id="orderFormCart" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="frmOrder" role="form" action="<?php echo base_url();?>dat-hang" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-money"></i> Thanh toán và giao hàng</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Họ và tên <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" required name="fullname" placeholder="" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Số điện thoại <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" required name="phone" placeholder="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Địa chỉ E-Mail</label>
                                    <div class="col-sm-8">
                                        <input type="email" name="email" placeholder="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Tỉnh/TP <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="province_id" class="form-control left" required>
                                            <option value="">Chọn</option>
                                            <?php foreach ($provinces as $province) :?>
                                                <option value="<?php echo $province->id;?>">
                                                    <?php echo $province->name;?>
                                                </option>
                                                <?php endforeach;?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Quận/Huyện <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="district_id" class="form-control right" required>
                                            <option value="">Chọn</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Địa chỉ <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" required name="address" class="form-control" placeholder="Lầu(tầng)/Số nhà/hẻm (ngõ)/Phố*">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Ghi chú </label>
                                    <div class="col-sm-8">
                                        <textarea name="note" placeholder="Thông tin chi tiết" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group pay_method_group" style="display: none;">
                                    <label class="col-sm-3 text-right">Phương thức TT <span class="required">*</span></label>

                                    <div class="col-md-4 radio">
                                        <label>
                                            <input checked="checked" type="radio" name="method_payment" value="1"> Giao hàng thu tiền tại nhà</label>
                                    </div>
                                    <div class="col-md-4 radio">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Hủy đơn hàng</button>
                            <button id="btnOrder" type="submit" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tôi đặt hàng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            function formatNumber(nStr, decSeperate, groupSeperate) {
            nStr += '';
            x = nStr.split(decSeperate);
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
            }
            return x1 + x2;
        }
            $(document).ready(function() {

                $('input[name=discount_code]').change(function(e) {
            getTotal($(this).val());
        });

        function getTotal(discount_code)
        {
            if (discount_code != '') {
                $.ajax({
                    url : '/salemans/getSalemanByCode',
                    method: 'POST',
                    data: {code:discount_code},
                    dataType: 'json',
                    success: function(res){
                        console.log(res);

                        if (res.status == true) {
                            $('.discountPrice').html('Được giảm giá: ' + formatNumber(res.totalDiscount, '.', ','));
                            $('.totalPayment').html(formatNumber(res.totalPayment, '.', ','));
                            $('.discountPrice').show();
                        } else {
                            $('.discountPrice').html('Mã giảm giá không hợp lệ!');
                            $('.discountPrice').show();
                            $('input[name=discount_code]').val('');
                            return false;
                        }
                    }
                });
            }

            $('.discountPrice').html('Số tiền phải trả: ' + formatNumber(_total, '.', ','));
            $('.discountPrice').show();
        }


                $('select[name=province_id]').change(function() {
                    province_id = $(this).val();
                    getDistrict(province_id);
                });

                function getDistrict(province_id) {
                    $.get("<?php echo base_url();?>cart/ajaxdistrict/" + province_id, function(res, status) {
                        data = $.parseJSON(res);

                        if (data.status == 200) {
                            $html = '';
                            $.each(data.data, function(index, item) {
                                $html += '<option value="' + item.id + '">' + item.name + '</option>';
                            });
                        } else {
                            $html = '<option value="">Quận/Huyện</option>';
                        }

                        $('select[name=district_id]').empty().html($html);
                    });
                }

                $('#btnOrder').click(function(e) {
                	e.preventDefault();

                	if ($('#frmOrder').find('input[name=fullname]').val() == '') {
                		$('#frmOrder').find('input[name=fullname]').addClass('error-box');
                		$('#frmOrder').find('input[name=fullname]').focus();
                		return false;
                	} else {
                		$('input[name=fullname]').removeClass('error-box');
                	}

                	if ($('#frmOrder').find('input[name=phone]').val() == '') {
                		$('#frmOrder').find('input[name=phone]').addClass('error-box');
                		$('#frmOrder').find('input[name=phone]').focus();
                		return false;
                	} else {
                		$('input[name=phone]').removeClass('error-box');
                	}

                	if ($('#frmOrder').find('select[name=province_id]').val() == '') {
                		$('#frmOrder').find('select[name=province_id]').addClass('error-box');
                		$('#frmOrder').find('select[name=province_id]').focus();
                		return false;
                	} else {
                		$('select[name=province_id]').removeClass('error-box');
                	}

                	if ($('#frmOrder').find('select[name=district_id]').val() == '') {
                		$('#frmOrder').find('select[name=district_id]').addClass('error-box');
                		$('#frmOrder').find('select[name=district_id]').focus();
                		return false;
                	} else {
                		$('select[name=district_id]').removeClass('error-box');
                	}

                	if ($('#frmOrder').find('input[name=address]').val() == '') {
                		$('#frmOrder').find('input[name=address]').addClass('error-box');
                		$('#frmOrder').find('input[name=address]').focus();
                		return false;
                	} else {
                		$('input[name=address]').removeClass('error-box');
                	}

                    let fullname = $('input[name=fullname]').val();
                    let address = $('input[name=address]').val();
                    let phone = $('input[name=phone]').val();
                    let province_id = $('select[name=province_id]').val();
                    let district_id = $('select[name=district_id]').val();
                    let discount_code = $('input[name=discount_code]').val();
                    let note = $('textarea[name=note]').val();

                    let _data = {
                        fullname: fullname,
                        address: address,
                        phone: phone,
                        province_id: province_id,
                        district_id: district_id,
                        discount_code: discount_code,
                        note: note
                    };

                    $.ajax({
                        url : '/books/order',
                        method: 'POST',
                        data: _data,
                        dataType: 'json',
                        success: function(res){
                            $msg = '';
                            for (i = 0; i < res.message.length; i++) {
                                $msg += res.message[i] + '<br>';
                            }

                            if (res.status == 1) {
                                toastr.success($msg, '');
                                $('#orderFormCart').modal('hide');
                                window.location.href='/thong-bao.html';
                            } else {
                                toastr.error($msg, '');
                            }
                        }
                    });

                });
            });
        </script>

        <style type="text/css" media="screen">
            #frmOrder .form-group {
                display: flow-root;
            }
            
            .required {
                color: red;
                font-size: 12px;
            }
            
            .cartBtn {
                padding-bottom: 30px;
                overflow: hidden;
            }
            .error-box{
            	border-color:red;
            }
        </style>