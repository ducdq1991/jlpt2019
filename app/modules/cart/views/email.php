<!DOCTYPE html>
<html>
<head>
	<title>Đơn hàng số <?php echo $orderId;?></title>
</head>
<style type="text/css" media="screen">
	.container{
		width: 890px;
		margin:10px auto;
		background: #fafafa;
		overflow: hidden;
		border:2px dashed #008000;
		padding: 15px;
	}	
	.text-center{
		text-align: center;
	}
	.container table caption{
		padding-bottom: 20px;
	}
	.container table{
		width: 100%;
		border-collapse: collapse;
		border-color: #d5d5d5;
	}
	.container table th{
		background: #f0f0f0;
	}
	.container table td, .container table th{
		border-color: #d5d5d5;
	}
	.text-left{
		text-align: left;
	}
	.text-right{
		text-align: right;
	}	
	.subTotal{
		font-weight: 600;
		color:red;
	}
	.total, .subTotal{
		padding-right: 20px;
	}
	.total{
		padding-top: 15px;
		padding-bottom: 15px;
	}
	.total span{
		color: #008000;
	}
</style>
<body>
	<div class="container">
		<header id="header" class="">
			<div class="logo text-center">
				<img src="http://thuocdietcontrung24h.com/assets/images/logo.png" alt="">
			</div>
			<div class="order-heading text-center">
				<h1>Đơn hàng <?php echo $orderId;?></h1>
				<p>Ngày đặt hàng: <?php echo $date;?></p>
			</div>
		</header><!-- /header -->
		<section>
			Khách hàng: <strong><?php echo $orderId;?></strong><br>
			Email: <strong><?php echo $email;?></strong><br>
			Số điện thoại: <strong><?php echo $phone;?></strong><br>
			Địa chỉ: <strong><?php echo $address;?></strong><br>
		</section>
		<?php echo $body;?>
	</div>
</body>
</html>