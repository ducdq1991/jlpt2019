<!-- Google Code for chuy&#7875;n &#273;&#7893;i Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 870524078;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "S92fCPbO53EQrsmMnwM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/870524078/?label=S92fCPbO53EQrsmMnwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<section class="page-content">
	<div class="container">
		<div class="row cart-page">
			<div class="col-md-12">
				<h1 class="page-title"><i class="fa fa-tags"></i> Thanh toán thành công</h1>
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
            </div>
            <div class="col-md-12 text-center">
            	<a href="<?php echo base_url();?>" class="btn btn-primary">Tiếp tục mua hàng</a>
            </div>
        </div>
    </div>
</section>