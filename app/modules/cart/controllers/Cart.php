<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('books/books_model');
		$this->load->library('cart');
		$this->load->helper('form');		
	}
	
	public function index()
	{	
		$this->load->model('base_model');		
		$data = [];

		$provinces = $this->base_model->getProvinces();
		$data['provinces'] = $provinces;

		$this->load->view('layouts/header');
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);
	}

	public function addToCart()
	{	
		$status = 400;			
		$postData = $this->input->post();
		$postData['price'] = (float) $postData['price'];
		$this->cart->product_name_rules = '\d\D';
		if ($this->cart->insert($postData)) {
			$quantity = 0;
			foreach ($this->cart->contents() as $item) {
				$quantity += $item['qty'];
			}
			$status = 200;
			echo json_encode(['status' => $status, 'quantity' => $quantity]);
			exit;
		}
		echo json_encode(['status' => $status]);
		exit;
	}

	public function update()
	{
		$data = $this->input->post();
		$this->cart->update($data);
		redirect('gio-hang.html');
	}

	public function emptyCart()
	{
		$this->cart->destroy();
		redirect('gio-hang.html');
	}

	public function ajaxdistrict($id)
	{
		$where = ['province_id' => $id];
		$districts = $this->base_model->getDistricts($where);
		if (!empty($districts)) {
			echo json_encode(['status' => 200, 'data' => $districts]);
			exit;
		}

		echo json_encode(['status' => 400]);
		exit;
	}

	public function order()
	{
		$data = $this->input->post();
		if (!empty($data)) {
			$paymentMethod = $data['method_payment'];
			unset($data['method_payment']);
			$data['type'] = ($paymentMethod == 1) ? 'cod' : 'bank_transfer';

			// Thanh toan COD
			$data['total'] = $this->cart->total();
			$data['created_date'] = date('Y-m-d H:i:s');
			$data['status'] = 'pending';
			$this->load->model('order_model');

			$body = '<table border="1">
				<caption class="text-center"><strong>THÔNG TIN ĐƠN HÀNG</strong></caption>
				<thead>
					<tr>
						<th class="text-center">STT</th>
						<th class="text-center">Sản phẩm</th>
						<th class="text-center">Đơn giá</th>
						<th class="text-center">Số lượng</th>
						<th class="text-center">Thành tiền (VNĐ)</th>
					</tr>
				</thead>
				<tbody>';

			if ($orderId = $this->order_model->addOrder($data)) {
				$i = 0;
				foreach ($this->cart->contents() as $item) {
					$i++;
					$orderProduct = [];
					$orderProduct['order_id'] = $orderId;
					$orderProduct['product_id'] = $item['id'];
					$orderProduct['price'] = $item['price'];
					$orderProduct['quantity'] = $item['qty'];
					$this->order_model->addOrderDetail($orderProduct);
					$body .= '<tr>
						<td class="text-center">'. $i .'</td>
						<td class="text-left">' . $item['name'] . '</td>
						<td class="text-center">' . number_format($item['price'], 0) . '</td>
						<td class="text-center">' . $item['qty'] . '</td>
						<td class="text-center subTotal">' . number_format($item['subtotal'], 0) . '</td>
					</tr>';					
				}	
				$body .= '</tbody>
						<footer>
							<strong>Tổng tiền: <span>'.number_format($this->cart->total()).'</span> VNĐ</strong>
						</footer>
					</table>';
				$mailer = [];
				$mailer['body'] = $body;
				$mailer['date'] = date('d/m/Y H:i:s');
				$mailer['total_amount'] = $this->cart->total();
				$mailer['fullname'] = $data['fullname'];
				$mailer['email'] = $data['email'];
				$mailer['phone'] = $data['phone'];
				$mailer['address'] = $data['address'];
				$mailer['orderId'] = $orderId;
				$this->sendMail($mailer);								
			} else {
				$this->session->set_flashdata('error', 'Lỗi! Chưa đặt được hàng. Bạn vui lòng thử lại hoặc gọi số <b>0983 828.393</b> để được hỗ trợ nhé');
				redirect('/gio-hang.html');
			}	

			if ($paymentMethod == 0) {
				$userPaymentInfo = [];
				$userPaymentInfo['total_amount'] = $this->cart->total();
				$userPaymentInfo['buyer_fullname'] = $data['fullname'];
				$userPaymentInfo['buyer_email'] = $data['email'];
				$userPaymentInfo['buyer_mobile'] = $data['phone'];
				$userPaymentInfo['address'] = $data['address'];
				$userPaymentInfo['orderId'] = $orderId;
				$this->session->set_userdata('paymentInfo', $userPaymentInfo);
				redirect('/thanh-toan-truc-tuyen.html');
			} else {			
				$this->session->set_flashdata('success', 'Cảm ơn bạn đã đặt mua sản phẩm của chúng tôi. Chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất!.');
				$this->cart->destroy();				
				redirect('/thong-bao.html');
			}
		} else {
			$this->session->set_flashdata('error', 'Lỗi nhập liệu. Bạn vui lòng thử lại hoặc gọi số <b>0983 828.393</b> để được hỗ trợ nhé');

			redirect('/gio-hang.html');
		}		
	}

	public function message()
	{

		$this->load->view('layouts/header', $this->_head);
		$this->load->view('message');
		$this->load->view('layouts/footer', $this->_footer);
	}	

	public function deleteOne($id){
		$data = $this->cart->contents();
		$deleteItem = [
			'rowid' => $id,
			'qty' => 0
		];

		if($this->cart->update($deleteItem)){
			$this->session->set_flashdata('success', 'Sản phẩm đã được xóa khỏi giỏ hàng!.');
		} else {
			$this->session->set_flashdata('error', 'Không xóa được sản phẩm!.');
		}

		redirect(base_url() . 'gio-hang.html');
	}


	public function orderStatus()
	{
		$data = [];

        $breadcrumbs = [];
        $breadcrumbs[] = ['<i class="fa fa-home"></i> Trang chủ', base_url()];
        $breadcrumbs[] = ['Trạng thái đơn hàng', base_url() . 'thanh-toan-thanh-cong.html'];
        $this->breadcrumbs->build($breadcrumbs);
        $this->_head['breadcrumbs'] = $this->breadcrumbs->display();

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
			$this->load->library('Nganluong');
			$nganLuong = new Nganluong();
			$nganLuong->version = '3.1';
			$nganLuong->merchant_id = MERCHANT_ID;
			$nganLuong->url_api = URL_API;
			$nganLuong->merchant_password = MERCHANT_PASS;
			$nganLuong->receiver_email = RECEIVER;
			$token = $this->input->post('token');
			$result = $nganLuong->GetTransactionDetail($token);

			if ($result) {
				$errorcode           = (string) $result->error_code;
				$transactionStatus  = (string) $result->transaction_status;
				if($errorcode == '00') {
					if($transactionStatus == '00') {
						$this->session->set_flashdata('success', 'Chúc mừng bạn đã thanh toán thành công!');
						$data['nganLuongData'] = $result;
						$this->cart->destroy();
						$this->session->unset_userdata('paymentInfo');
						// Update order
						$this->load->model('orders_model');
						$orderId = (int) $result->order_code;
						if ($orderId > 0) {
							$args = [
								'status' => 'complete',
								'modify_date' => date('Y-m-d H:i:s')
							];
							$this->orders_model->updateOrder($orderId, $args);	
						}						
					}
				}else{
					$this->session->set_flashdata('error', $nganLuong->GetErrorMessage($errorcode));
				}
			}
        }

		$this->load->view('layouts/header', $this->_head);
		$this->load->view('orderstatus', $data);
		$this->load->view('layouts/footer', $this->_footer);
	}

	public function payment()
	{
		$data = [];
		$this->load->library('form_validation');
		$config = [
			[
                'field' => 'total_amount',
                'label' => 'Số tiền',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Số tiền là bắt buộc',
                ]
            ],
			[
                'field' => 'buyer_fullname',
                'label' => 'Số tiền',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Số tiền là bắt buộc',
                ]
            ],
			[
                'field' => 'buyer_email',
                'label' => 'Địa chỉ Email',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Địa chỉ Email là bắt buộc',
                ]
            ],
			[
                'field' => 'buyer_mobile',
                'label' => 'Số điện thoại',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Số điện thoại là bắt buộc',
                ]
            ],
			[
                'field' => 'option_payment',
                'label' => 'Phương thức thanh toán',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Bạn chưa chọn phương thức thanh toán.',
                ]
            ],
			[
                'field' => 'bankcode',
                'label' => 'Mã ngân hàng',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Mã ngân hàng là bắt buộc.',
                ]
            ],                                                              		
		];

		$this->form_validation->set_rules($config);		
		if ( $this->form_validation->run() == FALSE ) {
	        $breadcrumbs = [];
	        $breadcrumbs[] = ['<i class="fa fa-home"></i> Trang chủ', base_url()];
	        $breadcrumbs[] = ['Thanh toán', base_url() . 'thanh-toan.html'];
	        $this->breadcrumbs->build($breadcrumbs);
	        $this->_head['breadcrumbs'] = $this->breadcrumbs->display();

			$this->load->view('layouts/header', $this->_head);
			$this->load->view('payment', $data);
			$this->load->view('layouts/footer', $this->_footer);			
		} else {
			$postData = $this->input->post();
			$this->load->library('Nganluong');

			$nganLuong = new Nganluong();
			$nganLuong->version = '3.1';
			$nganLuong->merchant_id = MERCHANT_ID;
			$nganLuong->url_api = URL_API;
			$nganLuong->merchant_password = MERCHANT_PASS;
			$nganLuong->receiver_email = RECEIVER;

			$totalAmount = $postData['total_amount'];
			$items = [];
			foreach ($this->cart->contents() as $item) {
				$args = [];

				$args['item_name1'] = $item['name'];
				$args['product_id'] = $item['id'];
				$args['item_amount1'] = $item['price'];
				$args['item_quantity1'] = $item['qty'];
				$items[] = $args;
			}
		 	
			$paymentMethod = $postData['option_payment'];
			$bankCode = @$postData['bankcode'];
			$orderCode = $postData['orderId'];

			$paymentType = '';
			$discountAmount = 0;
			$orderDescription = '';
			$taxAmount=0;
			$feeShipping=0;
			$returnUrl = base_url() . 'thanh-toan-thanh-cong.html';
			$cancelUrl =urlencode(''.$orderCode) ;

			$fullname = $postData['buyer_fullname'];
			$email = $postData['buyer_email'];
			$mobile = $postData['buyer_mobile'];	 
			$address = $postData['address'];
			if ($paymentMethod !='' && $email !="" && $mobile !="" && $fullname !="" && filter_var($email, FILTER_VALIDATE_EMAIL)) {
				switch ($paymentMethod) {
					case 'VISA':
						$result = $nganLuong->VisaCheckout($orderCode, $totalAmount, $paymentType, 
						$orderDescription, $taxAmount, $feeShipping, $discountAmount, $returnUrl,
						$cancelUrl, $fullname, $email, $mobile, $address, $items, $bankCode);
						break;

					case 'ATM_ONLINE':
						$result = $nganLuong->BankCheckout($orderCode, $totalAmount, $bankCode,
						$paymentType, $orderDescription, $taxAmount, $feeShipping, $discountAmount,
						$returnUrl, $cancelUrl, $fullname, $email, $mobile, $address, $items);
						break;

					case 'NH_OFFLINE':
						$result = $nganLuong->officeBankCheckout($orderCode, $totalAmount, $bankCode, $paymentType, $orderDescription, $taxAmount, $feeShipping, 
							$discountAmount, $returnUrl, $cancelUrl, $fullname, $email, $mobile, 
							$address, $items);
						break;

					case 'ATM_OFFLINE':
						$result = $nganLuong->BankOfflineCheckout($orderCode, $totalAmount, $bankCode, $paymentType, $orderDescription, $taxAmount, $feeShipping, 
							$discountAmount, $returnUrl, $cancelUrl, $fullname, $email, $mobile, 
							$address, $items);
						break;								
					
					case 'IB_ONLINE':
						$result = $nganLuong->IBCheckout($orderCode, $totalAmount, $bankCode, 
							$paymentType, $orderDescription, $taxAmount, $feeShipping,
							$discountAmount, $returnUrl, $cancelUrl, $fullname, $email, $mobile, 
							$address, $items);
						break;
																				
					default:
						$result = $nganLuong->NLCheckout($orderCode, $totalAmount, $paymentType,
							$orderDescription, $taxAmount, $feeShipping, $discountAmount, 
							$returnUrl, $cancelUrl, $fullname, $email, $mobile, $address, $items);
						break;
				}

				if ($result->error_code =='00') {
					redirect($result->checkout_url);
					exit;
				} else {
					echo $result->error_message;
					exit;
				}
			}	
		}			
	}


	public function sendMail($body)
	{
        $this->load->helper('file');
        $userName = 'Dear,';
        $args = [];
        date_default_timezone_set('GMT');
        $this->load->helper('string');
        $this->load->library('email');
        $smtp = $this->config->item('smtp');
        $this->email->initialize($smtp);
        $this->email->from($body['email'], $body['fullname']);
        $this->email->to($this->_head['settings']['email']);  
         $this->email->cc('info@thuocdietcontrung24h.com');   
        $this->email->subject('[ThuocDietConTrung24h] Đơn hàng số ['.$body['orderId'].'] từ ' . $body['fullname']);
        $this->email->set_newline("\r\n");
        $bodyData = [
        	'{USERNAME}' => $userName,
        	'{ORDERINFO}' => $body['body'],
        	'{ORDERID}' => $body['orderId'],
        	'{FULLNAME}' => $body['fullname'],
        	'{EMAIL}' => $body['email'],
        	'{PHONE}' => $body['phone'],
        	'{ADDRESS}' => $body['address'],
        	'{TOTAL}' => $body['total_amount'],
        	'{DATE}' => $body['date'],
        ];
        $template = $this->load->view('email.php',$body,TRUE);
        $this->email->message($template);
        $mail = $this->email->send();
	}
}