<div class="container">
	<div class="book-view">
		<div class="row">
			<div class="col-md-4 book-image">
				<div class="box-image-box">
					<img src="<?php echo base_url() . $book->image;?>" alt="<?php echo $book->name;?>">
				</div>
				<div class="book-addon">
					<a href="<?php echo $book->extra_link;?>" target="_blank" class="btn btn-success" data-toggle="modal"><i class="fa fa-book"></i> Đọc thử</a>
					<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#tryVideo"><i class="fa fa-youtube"></i> Video giới thiệu sách</a>
				</div>
			</div>
			<div class="col-md-4 book-info">
				<h1 itemprop="name" title="<?php echo $book->name;?>"><?php echo $book->name;?></h1>
				<p class="code">Khổ giấy : <span itemprop="mpn"><?php echo $book->paper_size;?></span></p>
				<p class="code">Số trang : <span itemprop="mpn"><?php echo $book->num_page;?></span></p>
				<p class="code">Trọng lượng : <span itemprop="mpn"><?php echo $book->weight;?></span></p>				
				<p class="code">Tình trạng :
					<?php if ($book->qty > 0) : ?>
					<span class="in_stock">Còn hàng</span>
					<?php else:?>
						<span class="out_of_stock">Hết hàng</span>
					<?php endif;?>
				</p>
				<div class="desc" itemprop="description">
					<p>
						<?php echo $book->description;?>
					</p>
				</div>
				<div class="product-price">                        
					<div class="price-item">
						<span class="price-label">Giá bán: </span><span class="price-value"> <?php echo number_format($book->price);?>đ </span>
					</div>
					<div class="price-item">
						<span class="price-label">Giá gốc: </span><span class="del"><del><?php echo number_format($book->origin_price);?></del> <span>₫</span></span>
					</div>
					<div class="price-item">
						<?php 
							$discountPrice = $book->origin_price - $book->price;
							$percent = ($discountPrice * 100) / $book->origin_price;
						?>
						<span class="price-label">Tiết kiệm ~<?php echo round($percent);?>%</span><span class="price-value">(<?php echo number_format($discountPrice);?> <span>₫</span>)</span>
					</div>
				</div>
				<items id="item-<?php echo $book->book_id;?>"  
							    item-name="<?php echo $book->name;?>"  
							    item-price="<?php echo $book->price;?>"  
							    item-id="<?php echo $book->book_id;?>"  
							    item-slug="<?php echo $book->slug;?>"  
							    item-image="<?php echo $book->image;?>" >  
							</items>                                         		
			</div>
			<div class="col-md-4">
				<div class="booking">
				<h3>Đặt sách nhanh tay, nhận ngay ưu đãi!</h3>
				<form action="" method="POST" accept-charset="utf-8">
					<input type="text" name="fullname" class="form-control" placeholder="Họ và tên" required>
					<select name="province_id" class="form-control w48 left" required>
						<option value="">--Tỉnh/TP--</option>
						<?php foreach ($provinces as $province) :?>
							<option value="<?php echo $province->id;?>"><?php echo $province->name;?></option>
						<?php endforeach;?>
					</select>
					<select name="district_id" class="form-control w48 right" required>
						<option value="">--Quận/Huyện--</option>
					</select>				
					<input type="text" name="address" class="form-control" placeholder="Địa chỉ" required>
					<input type="text" name="phone" class="form-control w48 left" placeholder="SĐT" required>					
					<span class="label-sl">SL:</span> <input type="number" name="qty" placeholder="Số lượng đặt" class="w48 sl right form-control" value="<?php echo $this->cart->total_items();?>" required>
					<input type="text" name="facebook_link" placeholder="Facebook" class="form-control">

					<textarea name="note" class="form-control" placeholder="Ghi chú"></textarea>
					<input type="text" name="discount_code" placeholder="Mã giảm giá (Nếu có)" class="form-control">
					<div class="discountPrice"> Số tiền phải trả là: 190,000 VNĐ</div>
					<button type="submit" class="btn btn-success btn-booking"><i class="fa fa-shopping-cart"></i> Đặt ngay</button>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" defer>

 function formatNumber(nStr, decSeperate, groupSeperate) {
            nStr += '';
            x = nStr.split(decSeperate);
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
            }
            return x1 + x2;
        }
	$(document).ready(function(){
		
		function addToCart()
		{
			let qty = $('input[name=qty]').val();
			var _total = price * qty;
			let _itemID = '<?php echo $book->book_id;?>';
			var title = $('items#item-' + _itemID).attr('item-name');
    		var price = $('items#item-' + _itemID).attr('item-price');
    		var product_id = $('items#item-' + _itemID).attr('item-id');
    		var image = $('items#item-' + _itemID).attr('item-image');
    		var slug = $('items#item-' + _itemID).attr('item-slug');
    		var quantity = qty;

			params = {id:product_id, price:price,name:title, qty:quantity, image:image,slug:slug};
			$.ajax({
				url: '/cart/addtocart',
				method: 'POST',
				data : params,
				success: function(res) {
					console.log(res);
					let _totalQTY = parseInt($('#cart-total').text()) + 1;
					$('#cart-total').text(_totalQTY);
					toastr.success('Đã thêm sách vào giỏ hàng!', '');
				}
			});
		}

		$('input[name=qty]').change(function(e) {
			let discount_code = $('input[name=discount_code]').val();
			getTotal(discount_code);
			addToCart();
		});

		$('input[name=discount_code]').change(function(e) {
			getTotal($(this).val());
		});

		function getTotal(discount_code)
		{
			let price = <?php echo $book->price;?>;
			let qty = $('input[name=qty]').val();
			var _total = price * qty;

			if (discount_code != '') {
				$.ajax({
		      		url : '/salemans/getSalemanByCode',
		      		method: 'POST',
		      		data: {code:discount_code},
		      		dataType: 'json',
		      		success: function(res){
		      			if (res.status == true) {
		      				_total = _total - (_total * res.discount_percent/100);
		      				$('.discountPrice').html('Số tiền phải trả: ' + formatNumber(_total, '.', ','));
	      					$('.discountPrice').show();
		      			} else {
		      				$('.discountPrice').html('Mã giảm giá không hợp lệ!');
		      				$('.discountPrice').show();
		      				$('input[name=discount_code]').val('');
		      				return false;
		      			}
			      	}
		      	});
			}

			$('.discountPrice').html('Số tiền phải trả: ' + formatNumber(_total, '.', ','));
	      	$('.discountPrice').show();
		}

      $('.btn-booking').click(function(e){      	
      	e.preventDefault();
      	let fullname = $('input[name=fullname]').val();
      	let address = $('input[name=address]').val();
      	let email = $('input[name=email]').val();
      	let phone = $('input[name=phone]').val();
      	let province_id = $('select[name=province_id]').val();
      	let district_id = $('select[name=district_id]').val();
      	let qty = $('input[name=qty]').val();
      	let facebook_link = $('input[name=facebook_link]').val();
      	let product_id = <?php echo $book->book_id;?>;
      	let product_name = '<?php echo $book->name;?>';
      	let price = <?php echo $book->price;?>;
      	let discount_code = $('input[name=discount_code]').val();

      	let options = {facebook_link: facebook_link};

      	data = {fullname: fullname, address:address,province_id:province_id, district_id:district_id, email:email, phone:phone, qty:qty, product_id: product_id, product_name:product_name, price:price, discount_code: discount_code, options:options};
      	$.ajax({
      		url : '/books/order',
      		method: 'POST',
      		data: data,
      		dataType: 'json',
      		success: function(res){
      			$msg = '';
      			for (i = 0; i < res.message.length; i++) {
      				$msg += res.message[i] + '<br>';
      			}

      			if (res.status == 1) {
      				toastr.success($msg, '')
      			} else {
      				toastr.error($msg, '')
      			}
      		}
      	});
      });

      $('select[name=province_id]').change(function(){
      	$.ajax({
      		url : '/settings/districtoptions?province_id=' + $(this).val(),
      		method: 'GET',      		
      		success:function(res){
      			$('select[name=district_id]').empty().html(res);
      		}
      	});
      });
	});
</script>

<style type="text/css" media="screen">
	.form-control.w48 {
    width: 48%;
    float: left;
    margin-right: 2px;
}
.w48.sl{
	width: 30%;
	float: right;
}
.discountPrice{
	    color: red;
	    display: none;
    font-weight: 900;
    background: #fff;
    padding: 5px;
    border-radius: 3px;
}

.label-sl{
	    color: #fff;
    margin-top: 10px;
    display: inline-block;
    margin-left: 26px;
}
</style>