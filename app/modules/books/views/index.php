<div id="courses-module" class="module module-course">
  <div class="container">
  	<div class="row">
	  	<h1 class="page-title">Sách hay</h1>
	  	<div  id="list-courses" class="list-item book-items">
	        <?php $order = 0;?>
	  		<?php foreach ( $books as $item ) : ?>
	  			<?php if(($order % 4) == 0) :?>
	  			<div class="row">
	  			<?php endif; $order++; ?>
				<div class="item col-md-3 col-sm-6 col-xs-12">
					<div class="box">
						<div class="img-thumbnail">
							<a>
								<img src="<?php echo base_url().$item->image;?>" alt="">
							</a>					
						</div>				
						<div class="info">
							<h2><a href="<?php echo base_url();?>sach/<?php echo $item->slug; ?>" title=""><?php echo $item->name; ?></a></h2>							
							<div class="price-wrapper">
								<p class="price-sale">Giá: 
									<span class="amount">
										<?php echo number_format($item->price);?><span class="symbol">₫</span>
									</span>
									<?php 
										$discountPrice = $item->origin_price - $item->price;
										$percent = ($discountPrice * 100) / $item->origin_price;
									?>
									<span class="price-regular"><?php echo number_format($item->origin_price);?>&nbsp;₫</span>
									<span class="sale-tag sale-tag-square">-<?php echo round($percent);?>%</span>
								</p>
							</div>
							<div class="cart-form"><a data-id="<?php echo $item->book_id;?>" data-price="<?php echo $item->price;?>" href="" class="btn addTocart btn-success"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ</a></div>

							<items id="item-<?php echo $item->book_id;?>"  
							    item-name="<?php echo $item->name;?>"  
							    item-price="<?php echo $item->price;?>"  
							    item-id="<?php echo $item->book_id;?>"  
							    item-slug="<?php echo $item->slug;?>"  
							    item-image="<?php echo $item->image;?>" >  
							</items>
						</div>					
					</div>			
				</div><!--div item-->
				<?php if(($order % 4) == 0 || $order == count($books)) :?>
					
				</div>
				<?php endif;?>
			<?php endforeach; ?>	
			<div class="pagination"><?php echo $paging; ?></div><!--pagination-->
		</div>
	</div>
  </div><!--container-->
</div>

<script>
	$(document).ready(function(){
		$('.addTocart').click(function(e){
			e.preventDefault();
			let _itemID = $(this).data('id');
			var title = $('items#item-' + _itemID).attr('item-name');
    		var price = $('items#item-' + _itemID).attr('item-price');
    		var product_id = $('items#item-' + _itemID).attr('item-id');
    		var image = $('items#item-' + _itemID).attr('item-image');
    		var slug = $('items#item-' + _itemID).attr('item-slug');
    		var quantity = 1;
    		params = {id:product_id, price:price,name:title, qty:quantity, image:image,slug:slug};
			$.ajax({
				url: '/cart/addtocart',
				method: 'POST',
				data : params,
				success: function(res) {
					console.log(res);
					let _totalQTY = parseInt($('#cart-total').text()) + 1;
					$('#cart-total').text(_totalQTY);
					toastr.success('Đã thêm sách vào giỏ hàng!', '');
				}
			});
			console.log($(this).data('id'));
		});
	});
</script>