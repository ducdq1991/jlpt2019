<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends Base_Controller {

	private $bookDiscountPercent = 0;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('books_model');
		$this->load->model('settings/settings_model');
		$this->load->model('salemans/saleman_model'); 
		$this->load->library('breadcrumbs');
		$this->load->library('cart');
		$this->load->helper('text');

		if (isset($this->_settings['book_discount_percent'])) {
            $this->bookDiscountPercent = (float) $this->_settings['book_discount_percent'];
        }
	}

	public function index($page = 0)
	{
		$data = $where = $like = $orderBy = [];

		$limit = 20;
		$where['books.status'] = 1;
       	$this->load->helper('paging');
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
        	$page = 1;
        }
		$offset = ($page - 1) * $limit;

		$total = $this->books_model->totalBook($where);
		$books = $this->books_model->getBooks($where, $limit, $offset);
        $data['books'] = $books;
		$data['paging'] = paging(base_url() .'thu-vien-sach', $page, $total, $limit);


		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Sách hay', base_url() . 'thu-vien-sach');
        $head['breadcrumb'] = $this->breadcrumbs->display();        
        
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => 'sách hay, sách luyện thi thpt, sách luyện thi toán thpt, sách luyện thi hóa thpt',
            'meta_description' => 'sách hay, sách luyện thi thpt, sách luyện thi toán thpt, sách luyện thi hóa thpt'
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Thư viện sách'; 
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('index', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}

	public function view($slug = '')
	{
		$data = [];

		$where = [
			'slug' => $slug
		];

		$book = $this->books_model->getBook($where);
		$data['book'] = $book;

		$provinces = $this->settings_model->getProvinces();
		$data['provinces'] = $provinces;
		
		$this->breadcrumbs->add('<i class="fa fa-home"></i>Trang chủ', base_url());
        $this->breadcrumbs->add('Sách hay', base_url() . 'thu-vien-sach');
        $head['breadcrumb'] = $this->breadcrumbs->display();        
        
        $metas = $this->getMetas();
        $metas = array(
            'meta_keyword' => 'sách hay, sách luyện thi thpt, sách luyện thi toán thpt, sách luyện thi hóa thpt',
            'meta_description' => 'sách hay, sách luyện thi thpt, sách luyện thi toán thpt, sách luyện thi hóa thpt'
        );  
        $head['metas'] = $metas;      
        $head['metas']['meta_title'] = 'Thư viện sách'; 
        //load view
		$this->load->view('layouts/header', $head);
		$this->load->view('view', $data);
		$this->load->view('layouts/footer', $this->_footer_menu);		
	}
	

	public function order()
	{
		$status = 0;
		$message = [];
		
		if ($this->cart->total_items() <= 0) {
			$message[] = 'Quá trình đặt hàng có lỗi!. Vui lòng thử lại.';
			echo json_encode(
				[
					'status' => $status,
					'message' => $message
				]
			);
			exit;
		}

		$data = $this->input->post();
		$data['options'] = isset($data['options']) ? json_encode($data['options']) : null;


		if ($data['fullname'] == '') {
			$message[] = 'Vui lòng nhập họ tên.';
		}

		if ($data['province_id'] == '') {
			$message[] = 'Vui lòng chọn Tỉnh/TP.';
		}

		if ($data['district_id'] == '') {
			$message[] = 'Vui lòng Quận/Huyện.';
		}

		if ($data['address'] == '') {
			$message[] = 'Vui lòng nhập địa chỉ.';
		}

		if ($data['phone'] == '') {
			$message[] = 'Vui lòng nhập số điện thoại.';
		}

		$discount = 0;
		$discount_code = NULL;

		$total = $this->cart->total();

		if ($data['discount_code'] != '') {
			$saleman = $this->saleman_model->getUser(['salemans.coupon_code' => trim($data['discount_code'])]);
	        if (!is_null($saleman)) {
	        	$discount_code = $data['discount_code'];
	            $discount = ($total * $this->bookDiscountPercent) / 100;
	        }
		}

		$order = [];

		if (count($message) == 0) {
			$order['fullname'] = $data['fullname'];
			//$order['email'] = $data['email'];
			$order['phone'] = $data['phone'];
			$order['province_id'] = $data['province_id'];
			$order['district_id'] = $data['district_id'];
			$order['address'] = $data['address'];
			$order['total'] = $total;
			$order['status'] = 0;
			$order['cod'] = 0;
			$order['shipping'] = 0;
			$order['discount'] = $discount;
			$order['created_at'] = date('Y-m-d');
			$order['discount_code'] = $discount_code;

			$orderId = $this->books_model->addOrder($order);
			
			if ($orderId) {
				$orderCode = $this->generateOrderCode($orderId, 'DH');
				$where = ['id' => $orderId];
				$this->books_model->updateOrder($where, ['order_code' => $orderCode]);

				foreach ($this->cart->contents() as $item) {
					$orderItem = [
						'order_id' => $orderId,
						'product_id' => $item['id'],
						'product_name' => $item['name'],
						'price' => $item['price'],
						'qty' => $item['qty']
					];
					$this->books_model->addOrderItem($orderItem);				
				}
				$message[] = 'Đặt sách thành công!';
				$this->session->set_flashdata('success', 'Cảm ơn em đã đặt mua sách của Thầy Vũ Ngọc Anh. VNA sẽ liên hệ lại em trong thời gian sớm nhất!.');
				$this->cart->destroy();
				$status = 1;
			}
		}

		echo json_encode(
			[
				'status' => $status,
				'message' => $message
			]
		);
		exit;
	}


	protected function generateOrderCode($orderID = 0, $prefix = '')
	{				
		$code = null;
	
		if ($orderID > 0) {
			if ($orderID < 10) {
				$code = $prefix . '00000' . $orderID;
			}

			if ($orderID >= 10 && $orderID < 100) {
				$code = $prefix . '0000' . $orderID;
			}

			if ($orderID >= 100 && $orderID < 1000) {
				$code = $prefix . '000' . $orderID;
			}

			if ($orderID >= 1000 && $orderID < 10000) {
				$code = $prefix . '00' . $orderID;
			}

			if ($orderID >= 10000 && $orderID < 100000) {
				$code = $prefix . '0' . $orderID;
			}

			if ($orderID >= 100000 && $orderID < 1000000) {
				$code = $prefix . '' . $orderID;
			}						
		}

		return $code;
	}

	public function addToCart()
	{
		
	}	
}
