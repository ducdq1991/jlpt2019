<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books_model extends Base_model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getBook($where = [])
    {
        try {
            $query = $this->db->select('*')
                            ->where($where)
                            ->limit(1)
                            ->order_by('book_id','DESC')
                            ->get('books');
            if( $query ) {
                return $query->result()[0];
            }
            return false;
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());
        }
    }

    public function getBooks($where = [], $limit = 0, $offset = 0)
    {
        try {
            $query = $this->db->select('*')
                            ->where($where)
                            ->limit($limit, $offset)
                            ->order_by('book_id','DESC')
                            ->get('books');
            if( $query ) {
                return $query->result();
            }
            return false;
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());
        }
    }

    public function totalBook($where = [])
    {
        try{            
            return $this->db->where($where)->count_all_results('books');
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());
        }
    }
    
    public function getCategories($where = [], $limit = 0)
    {
        try {
            $query = $this->db->select('*')
                            ->where($where)
                            ->limit($limit)                            
                            ->get('books_categories');
            if( $query ) {
                return $query->result();
            }
            return false;
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());
        }
    }

    public function addOrder($data = [])
    {
        try {   

            if ($this->db->insert('orders', $data)) { 
                return $this->db->insert_id();
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }
    }

    public function addOrderItem($data = [])
    {
        try {   

            if ($this->db->insert('order_items', $data)) { 
                return $this->db->insert_id();
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }        
    }

    public function updateOrder($where = [], $data = [])
    {
        try {   

            if ($this->db->where($where)->update('orders', $data)) { 
                return true;
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }        
    }

    public function updateOrderItem($where = [], $data = [])
    {
        try {   

            if ($this->db->where($where)->update('order_items', $data)) { 
                return true;
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
        }        
    }
}