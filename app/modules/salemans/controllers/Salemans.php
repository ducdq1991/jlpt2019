<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salemans extends Base_Controller {

    private $bookDiscountPercent = 0;

    private $courseDiscountPercent = 0;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->model('saleman_model'); 
        $this->load->library('breadcrumbs');

        if (isset($this->_settings['book_discount_percent'])) {
            $this->bookDiscountPercent = (float) $this->_settings['book_discount_percent'];
        }

        if (isset($this->_settings['course_discount_percent'])) {
            $this->courseDiscountPercent = (float) $this->_settings['course_discount_percent'];
        }
    }

    public function index()
    {
        $salemanSS = $this->session->userdata('saleman_ss');
        if (!$salemanSS) {
            redirect(base_url() . 'salemans/login');
        }

        $data = [];

        $this->load->view('layouts/header');
        $this->load->view('salemans/index', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }

    public function payments()
    {
        $salemanSS = $this->session->userdata('saleman_ss');
        if (!$salemanSS) {
            redirect(base_url() . 'salemans/login');
        }

        $data = [];

        $payments = $this->saleman_model->getPayouts(['payouts.receiver_id' => $salemanSS->id], 0);
        $data['payments'] = $payments;

        $this->load->view('layouts/header');
        $this->load->view('salemans/payments', $data);
        $this->load->view('layouts/footer', $this->_footer_menu);
    }

    public function getSalemanByCode()
    {
        $code = $this->input->post('code');
        $data = [];
        $status = false;
        $amount = 0;
        $saleman = $this->saleman_model->getUser(['salemans.coupon_code' => $code]);
        if (!is_null($saleman)) {
            $status =  true;
            $amount = $saleman->amount;
        }

        $data['status'] = $status;
        $data['amount'] = $amount;
        $data['code'] = trim($code);
        $data['discount_percent'] = $this->bookDiscountPercent;

        echo json_encode($data);
        exit;
    }

    public function checkUsername($username = '')
    {
        $username = strtoupper($username);
        $saleman = $this->saleman_model->getUser(['salemans.coupon_code' => $username]);

        if(!$saleman){ 
            $this->form_validation->set_message('checkUsername', '{field} Tài khoản không chính xác!');
            return false;
        }

        return true;
    }

    public function profile()
    {
        $data = [];
        $salemanSS = $this->session->userdata('saleman_ss');
        if (!$salemanSS) {
            redirect(base_url() . 'salemans/login');
        }

        $saleman = $this->saleman_model->getUser(['salemans.id' => $salemanSS->id]);
        if (!$saleman) {
            redirect(base_url() . 'salemans/login');
        }

        $data['saleman'] = $saleman;

        $this->load->library('form_validation');
        
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Thông tin | Cộng Tác Viên';


        $config = array(
            array(
                'field'  => 'name',
                'label'  => 'Họ tên',
                'rules'  => 'trim|required',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                )
            ),
            array(
                'field'  => 'email',
                'label'  => 'Địa chỉ email',
                'rules'  => 'trim|required',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                )
            ),
            array(
                'field'  => 'phone',
                'label'  => 'Số điện thoại',
                'rules'  => 'trim|required',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                )
            )
        );
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header', $head);
            $this->load->view('salemans/profile', $data);
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $postData = $this->input->post();
            $args['name'] = $postData['name'];
            $args['email'] = $postData['email'];
            $args['phone'] = $postData['phone'];
            $this->saleman_model->updateUser($saleman->id, $args);
            $this->session->set_flashdata('success', 'Cập nhật thành công!');
            redirect(base_url() . 'salemans/profile');

        }
    }

    public function login()
    {
        $this->load->library('form_validation');
        $data = [];
        $head['metas'] = $this->getMetas();
        $head['metas']['meta_title'] = 'Đăng nhập | Cộng Tác Viên';


        $config = array(
            array(
                'field'  => 'username',
                'label'  => 'Tài khoản',
                'rules'  => 'trim|required|callback_checkUsername',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                )
            ),
            array(
                'field'  => 'password',
                'label'  => 'Mật khẩu',
                'rules'  => 'trim|required',
                'errors' => array(
                    'required'   => 'Bạn chưa điền %s.',
                )
            )
        );
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header', $head);
            $this->load->view('salemans/login', $data);
            $this->load->view('layouts/footer', $this->_footer_menu);
        } else {
            $postData = $this->input->post();
            $username = strtoupper($postData['username']);
            $saleman = $this->saleman_model->getUser(['salemans.coupon_code' => $username]);
            if ($saleman) {
                if (md5(trim($postData['password'])) === $saleman->password) {
                    $this->session->set_userdata('saleman_ss', $saleman);
                    redirect(base_url() . 'salemans/profile');
                } else {
                    $this->session->set_flashdata('error', 'Sai mật khẩu!');
                    redirect(base_url() . 'salemans/login');
                }
            }

            redirect(base_url());   
        }
    }


    public function logout()
    {
        if ($this->session->has_userdata('saleman_ss')) {
            $user = $this->session->userdata('saleman_ss');
            $this->session->unset_userdata('saleman_ss');
        }

        if ($this->session->has_userdata('redirector'))
            redirect($this->session->userdata('redirector'));
        redirect(base_url());
    }
}