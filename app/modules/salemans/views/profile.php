<div id="user-module" style="margin-top: 30px;">
    <div class="container">
        <?php $this->load->view('salemans/sidebar'); ?>
        <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
        <div class="user-box-content">
            <h1 class="title-page title-site title-register">Thông tin cộng tác viên</h1>
            <div class="form-info">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>

                <form class="form" action="" method="POST">
                    <div class="form-group">
                        <label for="name" class="text-white">Họ tên:</label><br>
                        <input type="text" name="name" id="name" class="form-control" value="<?php echo set_value('name', $saleman->name);?>">
                    </div>
                    <div class="form-group">
                        <label for="email" class="text-white">Email:</label><br>
                        <input type="text" name="email" id="email" class="form-control" value="<?php echo set_value('email', $saleman->email);?>">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="text-white">Điện thoại:</label><br>
                        <input type="text" name="phone" id="phone" class="form-control" value="<?php echo set_value('phone', $saleman->phone);?>">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="text-white">Số dư: <strong style="color:red"><?php echo number_format($saleman->amount);?></strong> <sup>đ</sup></label><br>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="text-white">Mã CTV: <strong style="color:red"><?php echo $saleman->coupon_code;?></strong></label><br>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Cập nhật</button>
                    </div>
                </form>

            </div>
        </div>
        </div>
    </div>
</div>