<div id="user-module" style="margin-top: 30px;">
    <div class="container">
        <?php $this->load->view('salemans/sidebar'); ?>
        <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
        <div class="user-box-content">
            <h1 class="title-page title-site title-register">Thông tin cộng tác viên</h1>
            <div class="form-info">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        </div>
    </div>
</div>