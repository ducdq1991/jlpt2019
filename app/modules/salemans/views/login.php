<div id="ctv-module">
    <div class="container">
        <div class="row">    
            <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-4 col-md-offset-4">
                <div class="box">
                    <h1 class="text-center">Đăng nhập tài khoản Cộng Tác Viên</h1>
                    <div class="float">
                        <?php echo validation_errors(); ?>
                        <?php if( $this->session->flashdata('error') ) : ?>
                        <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                    <?php endif; ?>
                        <form class="form" action="" method="POST">
                            <div class="form-group">
                                <label for="username" class="text-white">Tài khoản (Mã CTV):</label><br>
                                <input type="text" name="username" id="username" class="form-control" value="<?php echo set_value('username');?>">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-white">Mật khẩu:</label><br>
                                <input type="password" name="password" id="password" class="form-control" value="<?php echo set_value('password');?>">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info btn-md" value="Đăng nhập">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

<style type="text/css" media="screen">
    #ctv-module h1{

    font-size: 29px;
    font-weight: 700;
    margin: 38px 0px;
    line-height: 41px;

    }
</style>
