<div class="sidebar col-md-3 col-sm-3 col-xs-12">
	<h2>Tài khoản của tôi</h2>
	<div class="menu-user">
		<div class="list-item">
			<h3>Thông tin tài khoản</h3>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>salemans/profile" title="">Thông tin cá nhân</a>
			</li>		
			<li style="display: none;">
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>salemans/changepassword" title="">Thay đổi mật khẩu</a>
			</li>
			<li>
				<i class="fa fa-chevron-circle-right"></i>
				<a href="<?php echo base_url();?>salemans/payments" title="">Thu nhập</a>
			</li>
			<li>
				<i class="fa fa-sign-out"></i>
				<a href="<?php echo base_url();?>salemans/logout" title="">Thoát tài khoản CTV</a>
			</li>
		</div>
	</div><!--menu user-->
</div>