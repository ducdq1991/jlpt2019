<div id="user-module" style="margin-top: 30px;">
    <div class="container">
        <?php $this->load->view('salemans/sidebar'); ?>
        <div id="detail" class="col-md-9 col-sm-9 col-xs-12">
        <div class="user-box-content">
            <h1 class="title-page title-site title-register">Lịch sử thu nhập</h1>
            <div class="form-info">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php endif; ?>

                <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Tiêu đề <a href="javascript: doSort('name', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Số tiền <a href="javascript: doSort('phone', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Nhóm <a href="javascript: doSort('coupon_code', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Ngày <a href="javascript: doSort('email', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($payments as $item) : ?>
                                <tr id="row-<?php echo $key; ?>" class="">
                                    <td class="text-center"><?php echo $item->id; ?></td>
                                    <td><strong><?php echo $item->name; ?></strong></td>
                                    <td class="text-center">
                                        <?php if ($item->type == 'minus') : ?>
                                            <span style="color:red">-</span>
                                        <?php endif;?>
                                        <?php if ($item->type == 'plus') : ?>
                                            <span style="color:green">+</span>
                                        <?php endif;?>
                                        <?php echo number_format($item->amount); ?> <sup>đ</sup>
                                    </td>
                                    <td class="text-center">
                                        <?php
                                        if ($item->pay_group == 'book_order') {
                                            echo 'Sách';
                                        }

                                        if ($item->pay_group == 'course') {
                                            echo 'Khóa học';
                                        }

                                        ?>
                                    </td>
                                    <td class="text-center"><?php echo date('d/m/Y H:i:A', strtotime($item->created_at));?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

            </div>
        </div>
        </div>
    </div>
</div>