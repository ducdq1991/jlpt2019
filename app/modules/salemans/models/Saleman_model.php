<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saleman_model extends Base_Model {

	public function __construct()
	{
		parent::__construct();
	}	

	public function addUser($args)
	{
		try {
			if( $this->db->insert('salemans', $args ))
			{
			   return $this->db->insert_id();
			} else {
				return false;
			}
		} catch (Exception $ex) {
			return false;
		}

        return false;
	}

	public function getUser($where = [])
	{
		try {
			$this->db->select('salemans.*')
			->from('salemans')
			->where($where)->limit(1);
			$query = $this->db->get();
			return $query->row();			
		} catch (Exception $ex) {

		}

        return null;
	}

	public function getUserById($id)
	{
		try {
			$where = [];
			$this->db->select('*');
			$where['salemans.id'] = (int) $id;
			$this->db->where($where)->limit(1);
			$query = $this->db->get('salemans');
            if ($query) {
                return $query->row();
            }
		} catch (Exception $ex) {			
			return null;
		}
        return null;
	}


    public function updateUser($id = 0, $args = [])
    {
    	try {
    		$where = array('id' => (int) $id );
    		if($this->db->where($where)->update('salemans',$args)){
    			return true;
    		}
    	} catch (Exception $ex) {
    		return false; 
    	}

        return false;
    }

    public function updateAmount($where = [], $amount = 0)
    {
    	try {   		
			$this->db->select('*');
			$this->db->where($where)->limit(1);
			$query = $this->db->get('salemans');
			$result = $query->row();
			if (!empty($result)) {
				$args = [];
				$args['amount'] = $result->amount + $amount;
	    		if ($this->db->where($where)->update('salemans',$args)) {
	    			return true;
	    		}
			}
    	} catch (Exception $ex) {
    		return false; 
    	}

    	return false;
    }


    public function getPayouts($where = [], $limit = 10, $offset = 0, $like = [], $orderby = [])
    {
    	$this->db->where($where);
    	if ($limit > 0) {
    		$this->db->limit( $limit, $offset );
    	}

    	$this->db->order_by('id', 'desc');

    	$query = $this->db->get('payouts');
    	if ($query) {
    		return $query->result();
    	}

    	return null;
    }
}