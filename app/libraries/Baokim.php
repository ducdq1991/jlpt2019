<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @package Baokim
 * @author Trung Doan <trung.doan@bigshare.vn>
 */
class Baokim{

	protected $httpUsername;
	protected $httpPassword;
	protected $restUrl;
	protected $merchantId;
	protected $apiUsername;
	protected $apiPassword;
	protected $secureCode;
	protected $transactionId;

	public function __construct()
	{
		$this->httpUsername = 'nguyenanhphongvn';
		$this->httpPassword = 'taFjVizxKQCdBNHWMb42'; 
		$this->restUrl = 'https://www.baokim.vn/the-cao/restFul/send';
		$this->merchantId = '29095';
		$this->apiUsername = 'nguyenanhphongvndsadsa';
		$this->apiPassword = 'taFjVizxKQCdBNHWMb42dsadsa';
		$this->secureCode = '794970743db15228';
	}

	public function transaction($network, $seriNumber, $pinNumber)
	{
		$response = [];
		date_default_timezone_set('Asia/Ho_Chi_Minh');

		switch ($network) {
			case 'MOBI':
				$name = 'Mobifone';
				break;

			case 'VIETEL':
				$name = 'Viettel';
				break;

			case 'GATE':
				$name = 'Gate';
				break;

			case 'VNM':
				$name = 'VNM';
				break;													
			
			default:
				$name = 'Vinaphone';
				break;
		}

		$this->transactionId = time();

		$arrayPost = [
			'merchant_id' => $this->merchantId,
			'api_username' => $this->apiUsername,
			'api_password' => $this->apiPassword,
			'transaction_id' => $this->transactionId,
			'card_id' => $network,
			'pin_field' => $pinNumber,
			'seri_field' => $seriNumber,
			'algo_mode' => 'hmac',
		];
		ksort($arrayPost);

		$dataSign = hash_hmac('SHA1', implode('', $arrayPost),$this->secureCode);

		$arrayPost['data_sign'] = $dataSign;

		$curl = curl_init($this->restUrl);

		curl_setopt_array($curl, [
			CURLOPT_POST => true,
			CURLOPT_HEADER => false,
			CURLINFO_HEADER_OUT => true,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPAUTH => CURLAUTH_DIGEST|CURLAUTH_BASIC,
			CURLOPT_USERPWD =>$this->httpUsername.':'. $this->httpPassword,
			CURLOPT_POSTFIELDS => http_build_query($arrayPost)
		]);

		$data = curl_exec($curl);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response['status'] = $status;
		$result = json_decode($data, true);	

		$time = time();		
		if ($status==200) {
		    $amount = $result['amount'];
			switch ($amount) {
				case 10000: 
					$xu = 10000; 
					break;

				case 20000: 
					$xu = 20000; 
					break;

				case 30000: 
					$xu = 30000; 
					break;

				case 50000: 
					$xu = 50000; 
					break;

				case 100000: 
					$xu = 100000; 
					break;

				case 200000: 
					$xu = 200000; 
					break;

				case 300000: 
					$xu = 300000; 
					break;

				case 500000: 
					$xu = 500000; 
					break;

				case 1000000: 
					$xu = 1000000; 
					break;
				default:
					break;
			}
			
			$response['amount'] = $amount;
			$response['cardName'] = $name;
			$response['message'] = 'Thanh toán thành công!';
		} else {
			 $response['message'] = $result['errorMessage'];
		}

		return $response;
	}
}