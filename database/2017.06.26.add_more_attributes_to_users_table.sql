ALTER TABLE `users` ADD `total_exam_question` INT NOT NULL DEFAULT '0' AFTER `bonus_point`;
ALTER TABLE `users` ADD `total_exam_question_true` INT NOT NULL DEFAULT '0' AFTER `total_exam_question`, ADD `total_exam_question_false` INT NOT NULL DEFAULT '0' AFTER `total_exam_question_true`;
ALTER TABLE `users` ADD `total_point` INT NOT NULL DEFAULT '0' AFTER `total_exam_question_false`;