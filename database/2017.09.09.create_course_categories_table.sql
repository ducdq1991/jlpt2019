CREATE TABLE `course_categories`(  
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255),
  `slug` VARCHAR(255),
  `status` TINYINT(0),
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;
