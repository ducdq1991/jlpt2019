jQuery(document).ready(function(){

    $('.form-add-user #group_id').change(function(){
        var _url = $(this).attr('url');
        var check = $(this).find(":selected").val();
        if(check == 0) {
            $('input.per-child').prop('checked', false);
            return false;
        }
        $.ajax({
            url: _url + '/' + check,
            dataType: 'json'
        }).done(function(result) {
            $('input.per-child').prop('checked', false);
            $.each(result, function( index, value ) {
                for(v in value) {
                    $('#'+ index + '-' + value[v]).prop('checked', true);
                }
            });
        });
    });
    var userPers = $('#permission').attr('userPermission');

    if(typeof userPers != 'undefined') {
        userPers = jQuery.parseJSON(userPers);
        for( c in userPers ) {
            $.each(userPers[c], function(key, value){
                $('#'+ c + '-' + value).prop('checked', true);
            });
        } 
    }   
    
});
function loadMethodDefault()
{
    $('#method').val('index,add,edit,del,changeStatus');
}