$(document).ready(function () {


    $('button.givePoint').on('click', function(){
        $('.processing').show();
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');
        bonusPoint = $('#bonus_point').val();
        if (type == 1) {
            params = {type:type, bonusPoint:bonusPoint};
        } else if (type == 0) {
            if (checkIds.length == 0) {
                alert('Bạn cần chọn học sinh trước!');
                $('.processing').hide();
                return false;
            }
            params = {listUserId: checkIds, type:type, bonusPoint:bonusPoint};
        } else {
            return false;
        }

        if (bonusPoint == '') {
            alert('Bạn chưa nhập điểm cần cộng!');
            return false;
        }        
        confirm("Bạn chắc chắn muốn thực hiện hành động này?");
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            success: function(json){                
                res = $.parseJSON(json);     
                if (res.status == 0) {
                    alert(res.message);
                    return false;
                }            
                $('.processing').hide();
                location.reload(true);
                return;
            }
        });
    });


    // give Money
    $('button.giveMoney').on('click', function(){
        $('.processing').show();
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');
        var paymentType = $(this).attr('payment-type');
 
        bonusNormal = $('#bonus_normal').val();
        if (type == 1) {
            params = {type:type, payment_type:paymentType, bonusNormal:bonusNormal};
        } else if (type == 0) {
            if (checkIds.length == 0) {
                alert('Bạn cần chọn học sinh trước!');
                $('.processing').hide();
                return false;
            }
            params = {listUserId: checkIds, type:type, payment_type:paymentType, bonusNormal:bonusNormal};
        } else {
            return false;
        }

        if (bonusNormal == '') {
            alert('Bạn chưa nhập tiền thưởng!');
            return false;
        }        
        confirm("Bạn chắc chắn muốn thực hiện hành động này?");
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            success: function(json){ 
                res = $.parseJSON(json);   
                if (res.status == 0) {
                    alert(res.message);
                    return false;
                }            
                $('.processing').hide();
                location.reload(true);
                return;
            }
        });
    });


    $('select.rowPerPage').on('change', function(){
        var numRow = $(this).val();
        $("#numRecord").empty().val(numRow);
        document.getElementById('formList').submit();    
    });
    //nut check radio
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    //tooltip
    $('[data-toggle="tooltip"]').tooltip(); 
    //Create auto Slug
    $('.autoSlug.auto-category-slug').change(function(){
        $('.slug.slug-category').val(normalizeSlug($('.autoSlug.auto-category-slug').val()));    
    });
     //Create auto Slug
    $('.autoSlug.auto-document-slug').change(function(){
        $('.slug.slug-document').val(normalizeSlug($('.autoSlug.auto-document-slug').val()));    
    });
    //Create auto Slug
    $('.autoSlug.auto-course-slug').change(function(){
        $('.slug.slug-course').val(normalizeSlug($('.autoSlug.auto-course-slug').val()));    
    });
    
    //change ordering
    $('.ordering').change(function(){
       var ordering  = $(this).val();
       var id  = $(this).attr('data-id');
       var url = $(this).attr('url');

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {ordering: ordering, id: id},
                success: function(res){
                }
            });
    });
    
    var checkIds = [];
    // check all
    $('#checkAll').click(function() {  
        checkIds = [];
        if(this.checked) { 
            $('.checkItem').each(function() { 
                this.checked = true;   
                checkIds.push(parseInt($(this).attr('value')));  
                $('.btn-function').css('display','block');          
            });
        }else{
            $('.checkItem').each(function() { 
                this.checked = false;    
                checkIds = []; 
                $('.btn-function').css('display','none');                  
            });         
        }
    });
    //check item
    $(".checkItem").change(function (){
        var value = parseInt($(this).attr('value'));
        if( $(this).prop('checked') ){
            checkIds.push(value);
        } else {
            var index = checkIds.indexOf(value);
            checkIds.splice(index, 1);
        }
        if(checkIds.length === 0){
            $('.btn-function').css('display','none');
        } else {
            $('.btn-function').css('display','block');
        }
    });
    // active any
    $('a.activeStatus').on('click', function(){
        var url = $(this).attr('url');
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {ids: checkIds, type:'multi', status:1},
            success: function(response){                 
                location.reload(true);
                return;
            }
        });
    });
    //deactivate any
    $('a.deactivateStatus').on('click', function(){
        var url = $(this).attr('url');
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {ids: checkIds, type:'multi', status:0},
            success: function(response){               
                location.reload(true);
                return;
            }
        });
    });
    //Delete any checkbox checked
    $('.delMultiple').on('click', function(){
        if ( confirm( message_confirm_delete ) ) {
            var url = $(this).attr('url');
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {ids: checkIds},
                success: function(response){
                    location.reload(true);
                    return;
                }
            });
        }
    });

    //Create auto Slug for title article
    $('.autoSlug.auto-article-slug').change(function(){
        $('.slug.slug-article').val(normalizeSlug($('.autoSlug.auto-article-slug').val()));    
    });
    
    //envet click publish
    $('#article-submit').click(function(){
        $('#form-add-article').submit();
    });

    //Add a new tag article
    $('#add-tag').click(function(){
        var tag = $.trim( $('#tag-text').val() );
        if( tag != '' ) {
            $('#tags-box').append('<span><i class="fa fa-times-circle"></i>&nbsp;' + tag + '</span>');
            var tag_input = $('#tag-input').val();
            if( tag_input != '' ) {
                $('#tag-input').val( tag_input + ',' + tag );
            } else {
                $('#tag-input').val( tag );
            }
        }
        $('#tag-text').val('');
        return false;
    });
    $('#hot-tag-content a').click(function(e){
        e.preventDefault();
        var tag = $(this).text();
        var tag_input = $('#tag-input').val();
        $('#tags-box').append('<span><i class="fa fa-times-circle"></i>' + tag + '</span>');
        if( tag_input != '' ) {
            $('#tag-input').val( tag_input + ',' + tag );
        } else {
            $('#tag-input').val( tag );
        }
    });
    $(document).on('click', '#tags-box span i', function(){
        $(this).parent().remove();
        var text = '';
        $('#tags-box span' ).each(function(){
            if(text == ''){
                text += $(this).text();
            } else {
                text += ',' + $(this).text();
            }
        });
        $('#tag-input').val( text );
    });
    $('#tag-text').focusin(function(){
        $(this).change(function(){
            var tag = $.trim( $('#tag-text').val() );
            if( tag != '' ) {
                $('#tags-box').append('<span><i class="fa fa-times-circle"></i>&nbsp;' + tag + '</span>');
                var tag_input = $('#tag-input').val();
                if( tag_input != '' ) {
                    $('#tag-input').val( tag_input + ',' + tag );
                } else {
                    $('#tag-input').val( tag );
                }
            }
            $('#tag-text').val('');
            return false;
        })
    });
    //click remove featured button
    $('#remove-featured-image').on('click', function(){
        $('input#image').attr('value', '');
        $('#load-image').html('');
        $('#load-form-featured-image').show();
        $('#remove-featured-image').hide();
    })
    //autoload featured image
    var imageLoad = $('input#image').val();
    if( imageLoad && typeof _baseUrl != 'undefined') {
        $('#load-image').html('<img style="max-width:300px;" src="' + _baseUrl + '' + imageLoad + '" alt="featured image" />');
        $('#load-form-featured-image').hide();
        $('#remove-featured-image').show();
    }
    //checkbox meta extension
    $('#meta_extension').change(function(){
        if( $(this).prop('checked') == true ) {
            $('.meta-extension-box').show();
        } else {
            $('.meta-extension-box').hide();
        }
    });
    //category selected for index and, edit
    if(typeof _category_id_article != 'undefined') {
        $('#category_id option').each(function(){
            if( $(this).val() == _category_id_article ) {
                $(this).prop('selected', true);
            }
        });
    }

    //datetime picker course
    $('#start-date .input-group.date').datetimepicker({ 
        format: 'DD-MM-YYYY'
    });
    $('.datepicker').datetimepicker({ 
        format: 'DD-MM-YYYY'
    });
    $('#end-date .input-group.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    //course selected for course edit
    $('#parent_id option').each(function(){
        if( $(this).val() == _parent_id ) {
            $(this).prop('selected', true);
        }
    });
    //datetime picker article
    $('#publish-up .input-group.date').datetimepicker({ 
        showClose: true,
        format: 'DD-MM-YYYY'
    });
    $('#publish-down .input-group.date').datetimepicker({
        showClose: true,
        format: 'DD-MM-YYYY'
    });
    //Create auto Slug for title article
    $('.autoSlug.auto-lesson-slug').change(function(){
        $('.slug.slug-lesson').val(normalizeSlug($('.autoSlug.auto-lesson-slug').val()));    
    });
    
    //click remove file button - lesson
    $('#remove-featured-file').on('click', function(){
        $('input#file').attr('value', '');
        $('#load-file').html('');
        $('#load-form-featured-file').show();
        $('#remove-featured-file').hide();
    })
    //autoload file name - lesson
    var fileLoad = $('input#file').val();
    if( fileLoad ) {
        $('#load-file').html('</p>' + fileLoad + '</p>');
        $('#load-form-featured-file').hide();
        $('#remove-featured-file').show();
    }
    //load preview video -lesson
    jQuery('.previewVideo').click(function(){
        if(typeof _base_url != 'undefined') {
            var video = $(this).attr('data-video');
            var youtube = $(this).attr('data-youtube');
            if( video == '') {
                var iframe = '<iframe id="iframe-video" width="100%" height="450" src="'+youtube+'" frameborder="1"></iframe>';
                jQuery('.videoFramePreview').empty().append(iframe); 
            } else {
                var embed = '<embed type="application/x-shockwave-flash" src="'+_base_url+'/mediaplayer.swf" width="100%" height="520" style="undefined" id="MediaPlayer" name="MediaPlayer" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" flashvars="file='+video+'&amp;image='+_base_url+'/assets/img/hocvien.jpg&amp;showicons=true&amp;shownavigation=true"/>';
                jQuery('.videoFramePreview').empty().append(embed); 
            }
        }
    });
    //evnet close modal
    $('#previewVideoModal').on('hidden.bs.modal', function () {
        $('#iframe-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    
    $('#course_id option').each(function(){
        if(typeof __course_id != 'undefined') {
            if( $(this).val() == __course_id)
            {
                $(this).prop('selected', true);
            }
        }
    });
    //datetime picker exam
    $('#publish-up-exam .input-group.date').datetimepicker({
        showClose: true,
        format: 'DD-MM-YYYY HH:mm',
    });
    $('#publish-down-exam .input-group.date').datetimepicker({
        showClose: true,
        format: 'DD-MM-YYYY HH:mm'
    });
    //Create auto Slug exam
    $('.autoSlug.auto-exam-slug').change(function(){
        $('.slug.slug-exam').val(normalizeSlug($('.autoSlug.auto-exam-slug').val()));    
    });
    
    //envet click publish exam
    $('#submit_exam').click(function(){
        $('#form-add-exam').submit();
    });
    //multi select
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
    //auto load question when submit false
    var multi_question = [];
    if( $('input[name=questions]').val() ) {
        var __questions = $('input[name=questions]').val();
        $.ajax({
            url: adminUrl + 'exam/getgroupquestion',
            method: 'POST',
            data: {'ids':__questions},
            success: function(response){
                if(response != 0) {
                    $('#table-questions tbody').append(response);
                }
            }
        });

        multi_question = __questions.split(',');
    }
    //get multi select value
    $('#add-question').click(function(){
        $('#multi-select-question :selected').each(function(i, selected){ 
            var i = multi_question.indexOf($(selected).val());
            var val = i>-1 ? null : $(selected).val();
            if( val != null ) {
                multi_question.push( val );
                $.ajax({
                    url: adminUrl + '/exam/getquestion',
                    method: 'POST',
                    data: {'id':val},
                    success: function(response){
                        if(response != 0) {
                            $('#table-questions tbody').append(response);
                        }
                    }
                });
            }
        });
        $('input[name=questions]').val(multi_question);
        $('ul.chosen-choices li a.search-choice-close').click();
    });

    //remove question 
    $(document).on('click', 'button#remove-question', function(){
        var q_id = $(this).attr('data-id');
        var i = multi_question.indexOf(q_id);
        if( i>-1 ) {
            multi_question.splice(i, 1);
            $('input[name=questions]').val(multi_question);
        } 
        $('#table-questions tbody tr#row-'+q_id).remove();
    });
    //slected course for exam
    $('.exam_course option').each(function(){
        if(typeof _exam_course_id != 'undefined') {
            if( $(this).val() == _exam_course_id)
            {
                $(this).prop('selected', true);
            }
        }
    });
    
    //click remove file button - lesson
    $('#remove-exam-file').on('click', function(){
        $('input#equestion').attr('value', '');
        $('#load-file-exam').html('');
        $('#load-form-exam-file').show();
        $('#remove-exam-file').hide();
    });
    //auto load file
    if( _exam_uri = $('input#question').val() ){
        $('#load-file-exam').html('<p>' + _exam_uri + '</p>');
        $('#load-form-exam-file').hide();
        $('#remove-exam-file').show();
    }
    
    //click remove file button - lesson
    $('#remove-answer-file').on('click', function(){
        $('input#answer').attr('value', '');
        $('#load-file-answer').html('');
        $('#load-form-answer-file').show();
        $('#remove-answer-file').hide();
    });
    //auto load file
    if( _answer_uri = $('input#answer').val() ){
        $('#load-file-answer').html('<p>' + _answer_uri + '</p>');
        $('#load-form-answer-file').hide();
        $('#remove-answer-file').show();
    }
    
    //load preview PDF document
    jQuery('.previewPdf').click(function(){
        var embedUrl = $(this).attr('data-url');
        var title = $(this).attr('data-title');
        var iframe = '<iframe src="http://docs.google.com/gview?url='+embedUrl+'&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>';
        jQuery('.fileFramePreview .modal-body').empty().append(iframe);
        if(typeof title != 'undefined') {
            jQuery('.fileFramePreview .fileHeader').text(title);
        } else {
            jQuery('.fileFramePreview .fileHeader').text('QStudy Preview Modal');
        }
        //http://infolab.stanford.edu/pub/papers/google.pdf file test
    });
    //load preview IMG document
    jQuery('.previewImg').click(function(){
        var arrImg = $(this).attr('data-url');
        arrImg = $.parseJSON(arrImg);
        var title = $(this).attr('data-title');
        var img = '';
        $.each(arrImg, function(k,v){
            img += '<img src="'+baseUrl+'/'+v+'"> <br/>';
        });
        jQuery('.fileFramePreview .modal-body').empty().append(img);
        if(typeof title != 'undefined') {
            jQuery('.fileFramePreview .fileHeader').text(title);
        } else {
            jQuery('.fileFramePreview .fileHeader').text('QStudy Preview Modal');
        }
        //http://infolab.stanford.edu/pub/papers/google.pdf file test
    });
    //slected course for exam
    $('#category_id option').each(function(){
        if(typeof _document_category_id != 'undefined') {
            if( $(this).val() == _document_category_id)
            {
                $(this).prop('selected', true);
            }
        }
    });
    //load permission for group edit
    if(typeof __permission_group != 'undefined') {
        var permission = $.parseJSON(__permission_group);
        $.each(permission, function( index, value ) {
            for(v in value) {
                $('#'+ index + '-' + value[v]).prop('checked', true);
            }
        });
    }

    //click remove file button - essay
    $('#remove-content-file').on('click', function(){
        $('input#content_file').attr('value', '');
        $('#load-content-file').html('');
        $('#load-form-content-file').show();
        $('#remove-content-file').hide();
    });
    //auto load file essay
    if( _exam_uri = $('input#content_file').val() ){
        $('#load-content-file').html('<p>' + _exam_uri + '</p>');
        $('#load-form-content-file').hide();
        $('#remove-content-file').show();
    }
    
    //click remove file button - essay
    $('#remove-answer-up-file').on('click', function(){
        $('input#answer_file').attr('value', '');
        $('#load-up-file-answer').html('');
        $('#load-form-answer-up-file').show();
        $('#remove-answer-up-file').hide();
    });
    //auto load file essay
    if( _answer_uri = $('input#answer_file').val() ){
        $('#load-up-file-answer').html('<p>' + _answer_uri + '</p>');
        $('#load-form-answer-up-file').hide();
        $('#remove-answer-up-file').show();
    }
    
    //click remove file button - mcq
    $('#remove-mcq-answer-file').on('click', function(){
        $('input#link_media_answer').attr('value', '');
        $('#load-mcq-answer-file').html('');
        $('#load-form-mcq-answer-file').show();
        $('#remove-mcq-answer-file').hide();
    })
    //autoload file name - mcq
    var fileLoad = $('input#link_media_answer').val();
    if( fileLoad ) {
        $('#load-mcq-answer-file').html('</p>' + fileLoad + '</p>');
        $('#load-form-mcq-answer-file').hide();
        $('#remove-mcq-answer-file').show();
    }
     //chang view multi row
    $('a.changeView').on('click', function(){
        var url = $(this).attr('url');
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {ids: checkIds, type:'multi', view:0},
            success: function(response){               
                location.reload(true);
                return;
            }
        });
    });
    //load preview array
    jQuery('.previewArr').click(function(){
        var data = $(this).attr('data-answer');
        var result = $(this).attr('data-result');
        var author = $(this).attr('data-author');
        data = $.parseJSON(data);
        result = $.parseJSON(result);
        var html = '';
        var index = 1;
        console.log(data);
        $.each(result, function(k1,v1){
            $.each(data, function(k2,v2){
                if(k1 == k2) {
                    if(v1==v2) {
                        html += '<div class="answer-true">Câu '+index+': (Mã câu hỏi: '+k1+') Trả lời: <strong>'+v2+'</strong></div>';
                    } else {
                        html += '<div class="answer-false">Câu '+index+': (Mã câu hỏi: '+k1+') Trả lời: <strong>'+v2+'</strong> Đáp án đúng: <strong>'+v1+'</strong></div>';
                    }
                }
            });
            index++;
        });
        jQuery('.arrFramePreview .modal-body').html(html);
        jQuery('.arrHeader').text('Kết quả thi trắc nghiệm - '+author);
        //http://infolab.stanford.edu/pub/papers/google.pdf file test
    });
    //change mark
    $('input.score').change(function(){
       var score  = $(this).val();
       var id  = $(this).attr('data-id');
       var url = $(this).attr('data-url');

            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {score: score, id: id},
                success: function(res){
                    return;
                }
            });
    });
    //datetimepicker mark
    $('#datetimepicker-mark').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    $('#datetimepicker-payment').datetimepicker({
        format: 'DD-MM-YYYY'
    });
     //sendmessage any
    $('a.sendMultiMes').on('click', function(){
        var url = $(this).attr('url');
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {ids: checkIds},
            success: function(response){             
                location.reload(true);
                return;
            },
            beforeSend: function() {
                $('.overlay-black').show();
            },
            complete: function() {
                $('.overlay-black').fadeOut('slow');
            },
        });
    });
    //send one message
    $('.send-message').click(function(){
        var exam_id = $(this).attr('data-exam');
        var user_id = $(this).attr('data-user');
        var user_exam_id = $(this).attr('data-id');
        $('#btn-send-message').attr('data-exam', exam_id);
        $('#btn-send-message').attr('data-user', user_id);
        $('#btn-send-message').attr('data-id', user_exam_id);
    });
    //click send
    $('#btn-send-message').click(function(){
        var exam_id = $(this).attr('data-exam');
        var user_id = $(this).attr('data-user');
        var user_exam_id = $(this).attr('data-id');
        var url = adminUrl+'/mark/sendmessage';
        var comment = $('textarea[name=comment]').val();
        $('#commentModal').modal('toggle');
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {exam_id:exam_id, user_id:user_id, comment:comment, user_exam_id:user_exam_id},
            success: function(response){
                location.reload(true);
                return;
            },
            beforeSend: function() {
                $('.overlay-black').show();
            },
            complete: function() {
                $('.overlay-black').fadeOut('slow');
            },
        });
    });
    $('#commentModal').on('hidden.bs.modal', function () {
        $('#comment-author-form')[0].reset();
    });
    
    //click remove video button - lesson
    $('#btn-remove-video').on('click', function(){
        $('input[name=video]').val('');
        $('#btn-upload-video').show();
        $('p.support-text').show('slow');
        $('#btn-remove-video').hide();
    });
    //auto load video
    if( $('input[name=video]').val() ){
        $('#btn-upload-video').hide();
        $('p.support-text').hide();
        $('#btn-remove-video').show();
    };

    //auto load question when submit false
    var multi_users = [];
    
    if( $('.form-add-user-promotion input[name=user_id]').val() ) {
        var __users = $('.form-add-user-promotion input[name=user_id]').val().split(',');
        $.each(__users, function(i, value){
            /*$.ajax({
                url: adminUrl + '/promotion/getuser',
                method: 'POST',
                data: {'id':value},
                success: function(response){
                    if(response != 0) {
                        $('#table-users tbody').append(response);
                    }
                }
            });*/
        });
        multi_users = $.merge( multi_users, __users);
    }
    //get multi select value promotion
    $('#add-user-promotion').click(function(){
        $('#multi-select-user :selected').each(function(i, selected){ 
            var i = multi_users.indexOf($(selected).val());
            var val = i>-1 ? null : $(selected).val();
            if( val != null ) {
                multi_users.push( val );
                $.ajax({
                    url: adminUrl + '/promotion/getuser',
                    method: 'POST',
                    data: {'id':val},
                    success: function(response){
                        if(response != 0) {
                            $('#table-users tbody').append(response);
                        }
                    }
                });
            }
        });
        $('input[name=user_id]').val(multi_users);
        $('ul.chosen-choices li a.search-choice-close').click();
    });

    //remove user promotion 
    $(document).on('click', '.form-add-user-promotion button#remove-user', function(){
        var q_id = $(this).attr('data-id');
        var i = multi_users.indexOf(q_id);
        if( i>-1 ) {
            multi_users.splice(i, 1);
            $('input[name=user_id]').val(multi_users);
        } 
        $('#table-users tbody tr#row-'+q_id).remove();

        $.ajax({
            url : adminUrl + 'promotion/deletepromotion',
            method : 'post',
            data : {'user_id':q_id,'course_id': courseId},
            dataType : 'json',
            success : function(res){
                if (res == 1) {
                    alert('Đã hủy thành công!');
                } else {
                    alert('Không hủy được!');
                }
                return;
            },
        });

    });

    $('#addQuestionModal input#match-question').keyup(function(e) {
        var match = $(this).val();
        if(match.length > 0 ) {
            $.ajax({
                url : adminUrl + 'exam/getmatchnamequestion',
                method : 'post',
                data : {'match':match},
                dataType : 'json',
                success : function(res){
                    if (res.code == 0) {
                        var $html = 'Không có kết quả nào được tìm thấy';
                    } else {
                        var $html = '';
                        $.each(res.data, function(k,v){
                            $html += '<li><a href="" class="question-choice" data="'+v.id+'">'+v.title+'</a></li>';
                        });
                    }
                    $('.result-questions-box ul').empty().append($html);
                    $('.result-questions-box').show();
                    return true;
                },
            });
        } else {
            $('.result-questions-box').hide();
            $('.result-questions-box ul').empty();
        }
    });

    $(document).on('click', '.result-questions-box li .question-choice', function(e){
        e.preventDefault();
        var $this = $(this).parent();
        var id = $(this).attr('data');
        $.ajax({
            url: adminUrl + '/exam/getquestion',
            method: 'POST',
            data: {'id':id},
            success: function(response){
                if(response != 0) {
                    var index = multi_question.indexOf(id);
                    if (index == -1) {
                        multi_question.push(id);
                        $('#table-questions tbody').append(response);
                        $('input[name=questions]').val(multi_question);
                        $this.hide();
                    }
                }
            }
        });
    });

});
// Change Status
function changeState(url, id, status) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {ids:id, status:status, type:'one'},
        success: function(response){
            location.reload(true);
            return;
        }
    });
}
function delSingle(url, id) {
    if ( confirm('Bạn chăc chắn muốn xóa?') ) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {ids:id},
            success: function(response){
                location.reload(true);
                return;
            }
        });
    }
}
// Change Featured
function changeFeatured(url, id, featured) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {id:id, featured:featured},
        success: function(response){
            location.reload(true);
            return;
        }
    });
}
function normalizeSlug(str) {
    str = str.replace(/^\s+|\s+$/g, '');
    var _from = "ÁÀẠẢÃĂẮẰẶẲẴÂẤẦẬẨẪáàạảãăắằặẳẵâấầậẩẫóòọỏõÓÒỌỎÕôốồộổỗÔỐỒỘỔỖơớờợởỡƠỚỜỢỞỠéèẹẻẽÉÈẸẺẼêếềệểễÊẾỀỆỂỄúùụủũÚÙỤỦŨưứừựửữƯỨỪỰỬỮíìịỉĩÍÌỊỈĨýỳỵỷỹÝỲỴỶỸĐđÑñÇç·/_,:;";
    var _to   = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaooooooooooooooooooooooooooooooooooeeeeeeeeeeeeeeeeeeeeeeuuuuuuuuuuuuuuuuuuuuuuiiiiiiiiiiyyyyyyyyyyddnncc------";
    for (var i=0, l=_from.length ; i<l ; i++) {
        str = str.replace(new RegExp(_from[i], "g"), _to[i]);
    }
    str = str.replace(/[^a-zA-Z0-9 -]/g, '')
    .replace(/\s+/g, '-')
    .replace(/-+-/g,"-")
    .replace(/^\-+|\-+$/g,"")
    .toLowerCase();
    return str;
}
// Change view
function changeView(url, id) {
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {ids:id, type:'one'},
        success: function(response){
            location.reload(true);
            return;
        }
    });
}


function doSort(key, value)
{
    $("#sort").val(key);
    $("#sortBy").val(value);
    document.getElementById('formList').submit();
}
//set featured callback
function responsive_filemanager_callback(field_id){
    var url=jQuery('#'+field_id).val();
    path = url.replace(/^.*\/\/[^\/]+\//, '');
    if(field_id == 'image') {
        //for image
        $('input#image').attr('value', path);
        $('#load-image').html('<img src="' + url + '" alt="featured image" />');
        $('#load-form-featured-image').hide();
        $('#remove-featured-image').show();
        $('#featuredImageModal').modal('toggle');
    } else if(field_id == 'question') {
        //for file
        $('input#question').attr('value', path);
        $('#load-file-exam').html('<p>' + path + '</p>');
        $('#load-form-exam-file').hide();
        $('#remove-exam-file').show();
        $('#examFileModal').modal('toggle');
    } else if(field_id == 'answer') {
        $('input#answer').attr('value', path);
        $('#load-file-answer').html('<p>' + path + '</p>');
        $('#load-form-answer-file').hide();
        $('#remove-answer-file').show();
        $('#answerFileModal').modal('toggle');
    } else if(field_id == 'content_file') {
        $('input#content_file').attr('value', path);
        $('#load-content-file').html('<p>' + path + '</p>');
        $('#load-form-content-file').hide();
        $('#remove-content-file').show();
        $('#contentFileModal').modal('toggle');
    } else if(field_id == 'answer_file') {
        $('input#answer_file').attr('value', path);
        $('#load-up-file-answer').html('<p>' + path + '</p>');
        $('#load-form-answer-up-file').hide();
        $('#remove-answer-up-file').show();
        $('#answerUpFileModal').modal('toggle');
    } else if(field_id == 'file') {
        $('input#file').attr('value', path);
        $('#load-file').html('<p>' + path + '</p>');
        $('#load-form-featured-file').hide();
        $('#remove-featured-file').show();
        $('#featuredFileModal').modal('toggle');
    } else if(field_id == 'video') {
        $('input[name=video]').val(path);
        $('#btn-upload-video').hide();
        $('p.support-text').hide();
        $('#btn-remove-video').show();
        $('#featuredVideoModal').modal('toggle');
    } else if(field_id == 'link_media_answer') {
        $('input#link_media_answer').attr('value', path);
        $('#load-mcq-answer-file').html('<p>' + path + '</p>');
        $('#load-form-mcq-answer-file').hide();
        $('#remove-mcq-answer-file').show();
        $('#mcqAnswerFileModal').modal('toggle');
    }
}