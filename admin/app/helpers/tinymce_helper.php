<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('tinymce'))    
{
    function tinymce($height = 250)
    {
    	$script = '';
    	$script .= '<script type="text/javascript" src="'.base_url().'assets/js/tinymce/plugins/asciimath/js/ASCIIMathMLwFallback.js"></script>';
    	$script .= '<script type="text/javascript"> var AMTcgiloc = "http://www.imathas.com/cgi-bin/mimetex.cgi"; </script>';
    	$script .= '<script src="'.base_url().'assets/js/tinymce/tinymce.min.js"></script>';
    	$script .= '<script>
						tinymce.init({
						    selector: "textarea.editor",
						    height: '.$height.',
						    theme: "modern",
						    relative_urls: false,
						    remove_script_host: false,       
						    plugins: [
						        "advlist autolink save autosave autolink lists link image charmap print preview hr anchor pagebreak",
						        "searchreplace wordcount visualblocks visualchars code fullscreen",
						        "insertdatetime media nonbreaking save table contextmenu directionality",
						        "emoticons template paste textcolor colorpicker textpattern imagetools responsivefilemanager asciimath asciisvg"
						    ],
						    toolbar1: "insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent table | asciimath asciimathcharmap asciisvg | responsivefilemanager link image media | fullscreen | styleselect formatselect fontselect fontsizeselect",
						    image_advtab: true,
						    AScgiloc : "http://www.imathas.com/editordemo/php/svgimg.php", 
						    ASdloc : "http://www.imathas.com/editordemo/jscripts/tiny_mce/plugins/asciisvg/js/d.svg",     
						    external_filemanager_path: adminUrl+"assets/js/filemanager/",
						    filemanager_title:"'.lang('file_manager').'" ,
						    external_plugins: { "filemanager" : adminUrl+"assets/js/filemanager/plugin.min.js"},
							setup : function(ed)
							{
							    ed.on("init", function() 
							    {
							        this.getDoc().body.style.fontSize = "15px";        
							    });
							}      
						});
						</script>';
		return $script;
   	}


   	function math_tinymce($height = 250)
   	{
    	$script = '';
    	$script .= '<script type="text/javascript" src="'.base_url().'assets/js/tinymce/plugins/asciimath/js/ASCIIMathMLwFallback.js"></script>';
    	$script .= '<script type="text/javascript"> var AMTcgiloc = "http://www.imathas.com/cgi-bin/mimetex.cgi"; </script>';
    	$script .= '<script src="'.base_url().'assets/js/tinymce/tinymce.min.js"></script>';
    	$script .= '<script>
						tinymce.init({
						    selector: "textarea.editor",
						    height: '.$height.',
						    theme: "modern",
						    relative_urls: false,
						    remove_script_host: false,       
						    plugins: [
						        "save autosave charmap preview image",
						        "wordcount visualchars",
						        "save",
						        "paste textcolor colorpicker textpattern asciimath asciisvg"
						    ],
						    toolbar1: "asciimath asciimathcharmap asciisvg | image",
						    image_advtab: true,
						    AScgiloc : "http://www.imathas.com/editordemo/php/svgimg.php", 
						    ASdloc : "http://www.imathas.com/editordemo/jscripts/tiny_mce/plugins/asciisvg/js/d.svg",     
						    external_filemanager_path: adminUrl+"assets/js/filemanager/",
						    filemanager_title:"'.lang('file_manager').'" ,
						    external_plugins: { "filemanager" : adminUrl+"assets/js/filemanager/plugin.min.js"},
							setup : function(ed)
							{
							    ed.on("init", function() 
							    {
							        this.getDoc().body.style.fontSize = "15px";        
							    });
							}      
						});
						</script>';
		return $script;   		
   	}
}
?>

