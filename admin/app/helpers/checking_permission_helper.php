<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('checking_permission'))    
{
    function checking_permission()
    {
        $CI =& get_instance();
        $controller = $CI->router->fetch_class();
        if(in_array($controller, array( 'auth', 'error' ) ) )
            return true;
        $user = $CI->session->userdata('user');
        if( $user->access_key == 'supper_admin' ) {
            return true;
        }
        if( $user->access_key != 'admin'  ) {
            //die("sdsđs");
             redirect(base_url('error/memberError'));
            return false;
        }
        if(in_array($controller, array( 'auth', 'dashboard', 'error', 'coursecategory', 'orders', 'books','salemans' ) ) )
            return true;
        $method = $CI->router->fetch_method();
        $allowedMethods = ['getgroupquestion', 'getGroupQuestion', 'getquestion', 'getQuestion', 'getGroup', 'usernameChecking', 'getgroup', 'getmatchnamequestion', 'copy', 'monitoring','deletepromotion', 'ajaxsearch', 'addajax', 'ordering', 'copylesson', 'copyLesson', 'copySubCourse', 'copysubcourse', 'updateRank', 'updaterank'
        ];
        if (in_array($method, $allowedMethods)) {
            return true;
        }
        
        $permissions = $user->permission;
        $permissions = unserialize( $permissions );
        if ( !empty($controller) && array_key_exists( $controller, $permissions) ) {
            $methodArr = $permissions[$controller];
            if ( in_array($method, $methodArr) ) {
                $CI->session->set_userdata('redirect', current_url());
                return true;
            }
        }
        redirect(base_url('error'));
        return false;
    }
}