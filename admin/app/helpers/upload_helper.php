<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('upload_media'))    
{

	function mkdir_r($dirName, $rights = 0777){
		$uploadPath = @str_replace('system/', 'upload', BASEPATH);
	    $dirs = @explode('-', $dirName);
	    $dir='';  
	    foreach ($dirs as $part) {	    	
	        $dir.= '/' . $part;
	        if (!is_dir($uploadPath . $dir) && @strlen($uploadPath . $dir)>0){
	            if(@mkdir($uploadPath . $dir, $rights)){
	            	continue;
	            }
	        }
	    }	    
	    return $uploadPath . $dir;
	}


    function upload_media($filename = 'file', $path = '')
    {
    	$core =& get_instance();
    	$uploadPath = @str_replace('system/', 'upload', BASEPATH);
    	$fileUrl = 'upload';
    	$date = date('Y-m-d');
    	if($customDir = mkdir_r($date)){
    		if(is_dir($uploadPath)){
    			$fileUrl .= '/' . str_replace('-', '/', $date);
    		}
    		$uploadPath = $customDir;
    	}
    	// Config Upload    	
    	$config = array(
    		'upload_path'	=> $uploadPath,
    		'allowed_types' => 'gif|jpg|png|jpeg|docx|doc|pptx|xlsx|xls|pdf|mp4|flv|mp3',
			'max_size'     	=> 15000
    		);	
    	$file = null;
    	// Check Upload
		if (isset($_FILES["{$filename}"]) && is_uploaded_file($_FILES["{$filename}"]['tmp_name'])) {
        	$core->load->library('upload', $config);
        	if ($core->upload->do_upload("{$filename}")) {
        		$fileAttribute = $core->upload->data();
        		if(isset($fileAttribute) && !empty($fileAttribute)){
        			if(isset($fileAttribute['file_name'])){       				
        				$file = $fileUrl .'/' . $fileAttribute['file_name'];	
        				return $file;
        			}        			
        			return false;
        		} else {
        			$core->session->set_flashdata('error', $core->upload->display_errors());
        			return false;
        		}        			       
        	} else {					
				$core->session->set_flashdata('error', $core->upload->display_errors());
				return false;			
        	} 
    	}
    	return false;   	
    }
}