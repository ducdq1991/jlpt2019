<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('toExcelReport'))
{
	function toExcelReport($template = '', $data, $filename = '')
	{
		$core =& get_instance();
        $core->load->helper('download');
        $core->load->library('PHPReport');
        $templateDir = FCPATH . "Example" . DIRECTORY_SEPARATOR;
		$config = [
			'template' => $template,
			'templateDir' => $templateDir      
		];

		$R = new PHPReport($config);

		/*$data = [
			['id' => 'item','repeat' => true,'data' => $bills],
			['id' => 'bill', 'repeat' => false, 'data' => $data['bill']],
		];*/

		$R->load($data);

		$outputFileDir =  FCPATH . "Media" . DIRECTORY_SEPARATOR;		
		$file = $outputFileDir  . $filename . ".xlsx";
		$result = $R->render('excel', $file);
		//force_download($file, NULL);

		if (file_exists($file)) {
			return true;
		} else {
			return false;
		}				
	}
}