<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('paging'))    
{
    function paging($link, $page, $total, $limit, $queryString = '')
    {
    	$CI =& get_instance();
		$CI->load->library('pagination');        
		//Pagination
		$config = [];
        if ($queryString != '') {
            $config['suffix'] = '?' . $queryString;    
        }        
        $config['base_url'] = $link . '';  	
        $config['prefix'] = 'page-';
        $config["total_rows"] = $total;
		$config['per_page'] = $limit;
        $config['cur_page'] = $page;
        $config['use_page_numbers'] = true;
        $config["num_links"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';           
        $CI->pagination->initialize($config);             
        return $CI->pagination->create_links();  

    }
}