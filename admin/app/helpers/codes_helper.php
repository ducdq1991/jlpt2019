<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('createRandomCode')) {
    function createRandomCode($length = 8)
    {
	    $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
	    srand((double)microtime()*1000000); 
	    $i = 0; 
	    $code = '' ; 

	    while ($i < $length) { 
	        $num = rand() % 33; 
	        $tmp = substr($chars, $num, 1); 
	        $code = $code . $tmp; 
	        $i++; 
	    } 

	    return strtoupper($code);     	
    }
}