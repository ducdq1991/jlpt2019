<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['module_name'] = 'Câu hỏi trắc nghiệm';
$lang['true_answer'] = 'Đáp án đúng';
$lang['question'] = 'Câu hỏi';
$lang['question_mcq'] = 'Đề bài';
$lang['answers'] = 'Lời giải';
$lang['pdf_answer'] = 'Hình ảnh hoặc file PDF hình ảnh';
$lang['video_answer'] = 'Video lời giải';
$lang['link_practices'] = 'Link các bài lý thuyết, hoặc luyện tập';
$lang['easy'] = 'Dễ';
$lang['normal'] = 'Bình thường';
$lang['difficult'] = 'Khó';
$lang['very_difficult'] = 'Rất khó';
$lang['question_level'] = 'Cấp độ';
$lang['score'] = 'Điểm';
$lang['choose_file'] = 'Chọn file lời giải';
$lang['remove_file'] = 'Xóa file';
$lang['type'] = 'Loại câu hỏi';
$lang['essay'] = 'Tự luận';
$lang['multi_choice'] = 'Trắc nghiệm';

$lang['mes_add_success'] = '%s được thêm thành công';
$lang['mes_add_error'] = '$s chưa thêm thành công';
$lang['mes_update_success'] = '%s được sửa thành công';
$lang['mes_update_error'] = '$s chưa sửa thành công';