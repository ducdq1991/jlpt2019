<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_document'] = 'Thêm Đề Mới';
$lang['edit_document'] = 'Sửa thông tin đề thi';
$lang['documents_management'] = 'Quản lý đề thi';
$lang['the_document'] = 'Đề thi';
$lang['documents'] = 'Các Đề thi';
$lang['category'] = 'Loại đề';
$lang['exam_upload'] = 'Đề thi';
$lang['answer_upload'] = 'Lời giải';
$lang['choose_file'] = 'Chọn PDF file';
$lang['remove_file'] = 'Xóa file';
$lang['choose_category'] = 'Chọn loại';
$lang['exam'] = 'Đề thi';
$lang['answer'] = 'Lời giải';
$lang['preview_exam'] = 'Xem đề thi';
$lang['preview_answer'] = 'Xem lời giải';
$lang['not_exist'] = 'Không có';