<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['setting_management'] = 'Cài đặt';
$lang['general'] = 'Cài đặt chung';

$lang['site_name'] = 'Tên website';
$lang['domain'] = 'Tên miền';
$lang['meta_keyword'] = 'Keyword [SEO]';
$lang['meta_description'] = 'Description [SEO]';
$lang['email'] = 'email';
$lang['site_off_message'] = 'Thông báo tắt website';
$lang['site_status'] = 'Trạng thái website';
$lang['turn_off'] = 'Tắt';
$lang['turn_on'] = 'Bật';
// for examination
$lang['examination_module'] = 'Quản lý khảo sát học viên';
$lang['exam_upload'] = 'Đề khảo sát';
$lang['choose_file'] = 'Chọn đề';
$lang['remove_file'] = 'Xóa file';
//for contact
$lang['com_name'] = 'Tên tổ chức';
$lang['remove_file'] = 'Xóa file';
$lang['phone'] = 'Số điện thoại';
$lang['address'] = 'Địa chỉ';
//for mailer
$lang['mailer_configuration'] = 'Cấu hình mailer';
$lang['protocol'] = 'protocol';
$lang['smtp_host'] = 'smtp host';
$lang['smtp_user'] = 'smtp user';
$lang['smtp_pass'] = 'smtp pass';
$lang['smtp_port'] = 'smtp port';
$lang['charset'] = 'charset';
$lang['mailtype'] = 'mailtype';
//for video intro
$lang['video_link'] = 'Link video';
$lang['video_intro'] = 'Video giới thiệu';
$lang['videointro_module'] = 'Quản lý video giới thiệu';