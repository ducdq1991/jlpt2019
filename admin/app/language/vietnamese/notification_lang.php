<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['notification_management'] = 'Quán lý thông báo';
$lang['content'] = 'Nội dung';
$lang['type'] = 'Loại';
$lang['hidden'] = 'Ẩn';
$lang['readed'] = 'Đã xem';
$lang['notification'] = 'Thông báo';
$lang['date_time'] = 'Thời gian';
$lang['send'] = 'Đã gửi';