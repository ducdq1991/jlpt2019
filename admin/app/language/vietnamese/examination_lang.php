<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['examination_module'] = 'Quản lý khảo sát năng lực học viên';
$lang['examination'] = "Bài khảo sát";
$lang['full_name'] = "Người gửi";
$lang['email'] = "Email";
$lang['phone'] = "Số điện thoại";
$lang['answer'] = "Bài làm";
$lang['created'] = "Ngày tháng";
$lang['preview_answer'] = "Xem bài làm";
$lang['search_by_email'] = "Tìm theo email";
$lang['search_by_phone'] = "Tìm theo số điện thoại";