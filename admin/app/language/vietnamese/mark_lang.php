<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['mark_management'] = 'Quản lý bài thi';
$lang['the_exam'] = "Bài thi";
$lang['username'] = 'Học viên';
$lang['exam_title'] = 'Bài thi';
$lang['score'] = 'Điểm';
$lang['date'] = 'Ngày thi';
$lang['time'] = 'Thời gian gửi bài';
$lang['attribute'] = 'Dạng bài thi';
$lang['multi_choice'] = 'Trắc nghiệm';
$lang['essay'] = 'Tự luận';
$lang['answer'] = 'Đáp án học viên';
$lang['preview_answer'] = 'Xem đáp án';