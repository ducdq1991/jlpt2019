<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_user'] = 'Thêm thành viên mới';
$lang['user_management'] = 'Quản lý thành viên';
$lang['register_new_user'] = 'Đăng ký thành viên mới';
$lang['edit_user'] = 'Sửa thông tin thành viên';
$lang['the_user'] = 'Thành viên';
$lang['the_new_user'] = 'Thành viên mới';
$lang['user'] = 'Thành viên';
$lang['users'] = 'Thành viên';
//validation
$lang['mes_user_minlength'] = '%s quá ngắn, phải có tối thiểu 6 ký tự';
$lang['mes_user_maxlength'] = '%s quá dài, tối đa 16 ký tự';
$lang['mes_pass_minlength'] = '%s quá ngắn, phải có tối thiểu 8 ký tự';
$lang['mes_confim_pass'] = '%s không đúng';
$lang['mes_number'] = '%s không đúng';
$lang['mes_valid_phone'] = '%s không đúng';
$lang['usernameChecking'] = '{field} phải bắt đầu bằng một ký tự và chỉ cho phép ký tự, số';
//label
$lang['username'] = 'Tài khoản';
$lang['password'] = 'Mật khẩu';
$lang['confirm_password'] = 'Xác nhận mật khẩu';
$lang['full_name'] = 'Họ và tên';
$lang['email'] = 'Email';
$lang['phone_no'] = 'Số điện thoại';
$lang['school'] = 'Trường';
$lang['class'] = 'Lớp';
$lang['address'] = 'Địa chỉ';
$lang['group'] = 'Nhóm';
$lang['permission'] = 'Phân quyền';
$lang['choose_group'] = 'Chọn nhóm';
//table
$lang['last_login_date'] = 'Đăng nhập lần cuối';
//tooltip
$lang['edit_tooltip'] = 'Sửa bản ghi';
$lang['reset_tooltip'] = 'Cài lại mật khẩu';
$lang['del_tooltip'] = 'Xóa bản ghi';
//confirm
$lang['reset_confirm'] = 'Bạn chắc chắn muốn cài lại mật khẩu mặc định cho tài khoản này?';
//alert
$lang['message_reset_success'] = 'Mật khẩu đã được cài đặt lại thành công';
$lang['message_reset_error'] = 'Không thể cài đặt lại mật khẩu';
$lang['message_forbidden_error'] = 'Bạn không được phép thực hiện!';