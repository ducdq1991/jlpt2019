<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_lesson'] = 'Thêm mới bài giảng';
$lang['edit_lesson'] = 'Sửa bài giảng';
$lang['lesson'] = 'Bài giảng';
$lang['the_lesson'] = 'Bài giảng';
$lang['lessons'] = 'Bài giảng';
//form
$lang['new_lesson'] = 'Bài giảng mới';
$lang['video'] = 'Video';
$lang['related_link'] = 'Link luyện tập';
$lang['upload_file'] = 'Tải tệp tin';
$lang['remove_file'] = 'Hủy tệp tin';
$lang['upload_video'] = 'Chọn video';
$lang['remove_video'] = 'Hủy video';
$lang['choose_file'] = 'Chọn tệp PDF';
$lang['course_thematic'] = 'Khóa học/Chuyên đề';
$lang['choose_course'] = 'Chọn khóa học';
$lang['access'] = 'Xem miễn phí';
//index
$lang['name_lesson'] = 'Tên bài giảng';
$lang['document'] = 'Tài liệu';
$lang['preview_video'] = 'Xem video';
$lang['preview_document'] = 'Xem tài liệu';
$lang['not-file'] = 'Không có vi deo';
$lang['price'] = 'Giá bán';