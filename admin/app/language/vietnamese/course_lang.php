<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['name_course'] = 'Tên khóa học';
$lang['add_course'] = 'Thêm khóa học mới';
$lang['edit_course'] = 'Sửa khóa học';
$lang['the_course'] = 'Khóa học';
$lang['course'] = 'Khóa học';
$lang['courses'] = 'Các khóa học';

//form add
$lang['avatar'] = 'Avatar';
$lang['description'] = 'Mô tả';
$lang['course_parent'] = 'Root';
$lang['course_name'] = 'Tên khóa học';
$lang['course_code'] = 'Mã khóa học';
$lang['slug'] = 'Slug';
$lang['teacher'] = 'Giáo viên';
$lang['start_date'] = 'Ngày bắt đầu';
$lang['end_date'] = 'Ngày kết thúc';
$lang['avatar_button'] = 'Chọn ảnh đại diện';
$lang['remove_button'] = 'Xóa ảnh đại diện';
$lang['seo_keywords'] = 'Từ khóa [SEO]';
$lang['seo_description'] = 'Mô tả [SEO]';
$lang['order'] = 'Order';
$lang['price'] = 'Giá';
//for alert
$lang['new_course'] = 'Khóa học mới';
$lang['course'] = 'Khóa học';
$lang['new_course_details'] = 'Thông tin khóa học mới';
$lang['course_details'] = 'Thông tin khóa học';
//member of course table
$lang['last_login_date'] = 'Đăng nhập lần cuối';
$lang['username'] = 'Tài khoản';
$lang['full_name'] = 'Họ và tên';
$lang['email'] = 'Email';
$lang['phone_no'] = 'Số điện thoại';
$lang['school'] = 'Trường';
$lang['class'] = 'Lớp';
$lang['address'] = 'Địa chỉ';
$lang['group'] = 'Nhóm';
$lang['members_management'] = 'Thành viên tham gia khóa học';