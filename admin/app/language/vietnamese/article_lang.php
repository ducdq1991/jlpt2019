<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_article'] = 'Tạo bài viết mới';
$lang['edit_article'] = 'Sửa bài viết';
$lang['management_article'] = 'Quản lý bài viết';
$lang['article'] = 'Bài viết';
//form add article
$lang['content'] = 'Nội dung';
$lang['description'] = 'Mô tả';
$lang['meta_extension'] = 'Tối ưu SEO';
$lang['meta_title'] = 'Tiêu đề [SEO]';
$lang['meta_description'] = 'Mô tả [SEO]';
$lang['meta_keywords'] = 'Từ khóa [SEO]';

$lang['publish_heading'] = 'Xuất bản';
$lang['status'] = 'Trạng thái';
$lang['publish_up'] = 'Xuất bản';
$lang['publish_down'] = 'Ngừng xuất bản';
$lang['button_trash'] = 'Thùng rác';
$lang['button_publish'] = 'Xuất bản';
$lang['button_update'] = 'Cập nhật';
$lang['featured_image_heading'] = 'Ảnh đại điện';
$lang['featured_image_button'] = 'Cập nhật ảnh đại diện';
$lang['remove_button'] = 'Xóa';
$lang['category_heading'] = 'Danh mục';
$lang['category_option'] = '-- Danh mục --';
$lang['tags_heading'] = 'Tags';
$lang['tags_add'] = 'Thêm';
$lang['tags_more'] = 'Chọn tags hay sử dụng';
$lang['statistic_heading'] = 'Thông kế';
$lang['s_featured'] = 'Nổi bật';
$lang['s_view'] = 'Lượt xem';
$lang['s_author'] = 'Tác giả';
$lang['draft'] = 'Nháp';
$lang['rows'] = 'Rows';

//validation message
$lang['mes_minlength'] = '%s quá ngắn, tối thiểu 3 ký tự';

//alert message
$lang['new_article'] = 'Bài viết mới';
$lang['new_article_detail'] = 'Bài chi tiết';
$lang['new_hash_id'] = 'Hash ID';
$lang['article'] = 'Bài viết';
$lang['articles'] = 'Bài viết';
$lang['featured'] = 'Nổi bật';
//status option
$lang['publish'] = 'Xuất bản';
$lang['draft'] = 'Bản thảo';
$lang['pending_review'] = 'Chờ duyệt';
//table
$lang['author'] = 'Tác giả';
$lang['category'] = 'Danh mục';
$lang['publish_date'] = 'Ngày xuất bản';
$lang['created_date'] = 'Ngày tạo';