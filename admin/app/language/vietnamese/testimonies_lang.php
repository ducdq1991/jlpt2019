<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_testimonie'] = 'Thêm mới ý kiến';
$lang['edit_testimonie'] = 'Sửa ý kiến';
$lang['management_testimoniew'] = 'Quản lý ý kiến';
//form add article
$lang['content'] = 'Nội dung';
$lang['description'] = 'Thông tin học viên';

$lang['status'] = 'Trạng thái';
$lang['button_update'] = 'Cập nhật';
$lang['featured_image_heading'] = 'Ảnh đại điện';
$lang['featured_image_button'] = 'Cập nhật ảnh đại diện';
$lang['remove_button'] = 'Xóa';

//validation message
$lang['mes_minlength'] = '%s quá ngắn, tối thiểu 5 ký tự';
$lang['mes_maxlength'] = '%s quá dài, tối thiểu 35 ký tự';
//alert message
$lang['new_testimonie'] = 'Ý kiến mới';
$lang['testimonies'] = 'Học viên nói về chúng tôi';
$lang['testimonies'] = 'Học viên nói về chúng tôi';
//status option
$lang['active'] = 'Kích hoạt';
$lang['deactive'] = 'Vô hiệu';
//title
$lang['full_name'] = 'Họ và tên';
$lang['avartar'] = 'Ảnh đại diện';
$lang['add_new'] = 'Thêm mới';
$lang['choose_file'] = 'Chọn ảnh đại diện';
$lang['remove_file'] = 'Xóa ảnh';