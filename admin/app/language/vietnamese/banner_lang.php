<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_banner'] = 'Tạo quảng cáo mới';
$lang['the_banner'] = 'Khung hình/quảng cáo';
$lang['banner'] = 'Khung hình/quảng cáo';
$lang['banner_management'] = 'Quảng cáo';
$lang['edit_banner'] = 'Sửa quảng cáo';
$lang['num_rows'] = 'Dòng';
$lang['image'] = 'Hình ảnh';
$lang['choose_button'] = 'Chọn hình ảnh';
$lang['remove_button'] = 'Xóa hình ảnh';
$lang['link'] = 'Link tĩnh';
$lang['desc'] = 'Mô tả';
$lang['group'] = 'Phân nhóm';
$lang['type'] = 'Phân loại';
$lang['choose_type'] = 'Chọn loại';
$lang['choose_group'] = 'Chọn nhóm';
//type option
$lang['banner'] = 'Banner';
$lang['slider'] = 'Slider';
$lang['ads'] = 'Ads';