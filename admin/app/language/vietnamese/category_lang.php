<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*--------------------Category-----*/
$lang['name_category'] = 'Tên danh mục';
$lang['add_category'] = 'Tạo danh mục';
$lang['categories'] = 'Danh mục';
$lang['added_category'] = 'Thêm danh mục thành công!';
$lang['faild_added_category'] = 'Không thể thêm danh mục!';
$lang['this_field_is_required'] = '% là bắt buộc';
$lang['enter_link_file'] = 'Nhập link tài liệu';
$lang['this_field_is_required'] = 'Bạn phải nhập %';
$lang['file_manager'] = 'Media';
$lang['add_success'] = 'Thêm %s thành công!';
$lang['but'] = 'nhưng';
$lang['faild_media_upload'] = 'Không thể upload tệp %s!';
$lang['exited_item'] = '%s đã tồn tại. Vui lòng nhập một %s khác!';
$lang['level'] = 'Cấp độ';
