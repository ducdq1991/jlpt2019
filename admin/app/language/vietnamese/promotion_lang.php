<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['promotion_management'] = 'Quản lý khuyên mại khóa học';
$lang['add_new_promotion'] = 'Thêm khóa học được khuyến mại viên';
$lang['edit_promotion'] = 'Sửa khóa học được khuyến mại';
//form add
$lang['choose_the_user'] = 'Chọn học viên...';
$lang['user'] = 'Học viên';
$lang['add_user'] = 'Thêm học viên';
$lang['course'] = 'Khóa học';
$lang['choose_course'] = 'Chọn khóa học';
$lang['promotion_course'] = 'Khóa học khuyến mại';