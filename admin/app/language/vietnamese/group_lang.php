<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['group_management'] = 'Quản lý nhóm';
$lang['add_new_group'] = 'Tạo nhóm mới';
$lang['the_group'] = 'Nhóm';
$lang['edit_group'] = 'Sửa nhóm';
$lang['group-management'] = 'Quản lý nhóm';
$lang['permission'] = 'Phân quyền';
$lang['num_rows'] = 'Dòng';
$lang['access_group'] = 'Nhóm quản trị';
$lang['choose_access'] = 'Chọn nhóm';

$lang['mes_min_length'] = '%s quá ngắn, ít nhất 2 ký tự';
$lang['mes_not_update'] = 'Nhóm %s không được phép sửa';
$lang['mes_not_delete'] = 'Nhóm %s không được phép xóa';

$lang['guest'] = 'Khách';
$lang['member'] = 'Thành viên';
$lang['admin'] = 'Quản trị viên';
$lang['supper_admin'] = 'Quản trị viên cao cấp';