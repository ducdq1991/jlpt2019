<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['name_course'] = 'Course Name';
$lang['add_course'] = 'Add New Course';
$lang['edit_course'] = 'Edit Course';

//form add
$lang['avatar'] = 'Avatar';
$lang['description'] = 'Description';
$lang['course_parent'] = 'Course Parent';
$lang['course_name'] = 'Course Name';
$lang['course_code'] = 'Course Code';
$lang['slug'] = 'Slug';
$lang['teacher'] = 'Teacher';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['avatar_button'] = 'Choose avatar';
$lang['remove_button'] = 'Delete avatar';
$lang['seo_keywords'] = 'SEO Keywords';
$lang['seo_description'] = 'SEO Description';
$lang['order'] = 'Order';
//for alert
$lang['new_course'] = 'The new course';
$lang['course'] = 'The course';
$lang['new_course_details'] = 'New course details';
$lang['course_details'] = 'Course details';