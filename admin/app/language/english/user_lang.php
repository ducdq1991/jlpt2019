<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_user'] = 'Add New User';
$lang['user_management'] = 'Users Management';
$lang['register_new_user'] = 'Register new user';
$lang['edit_user'] = 'Edit User Infomation';
$lang['the_user'] = 'The user';
$lang['the_new_user'] = 'The new user';
$lang['user'] = 'User';
$lang['users'] = 'Users';
//validation
$lang['mes_user_minlength'] = '%s is too short, minimum 6 chars';
$lang['mes_user_maxlength'] = '%s is too long, maximum 16 chars';
$lang['mes_pass_minlength'] = '%s is too short, minimum 8 chars';
$lang['mes_confim_pass'] = '%s is not right';
$lang['mes_number'] = '%s is not right';
$lang['mes_valid_phone'] = '%s is not format right';
$lang['usernameChecking'] = '{field} allow characters, number, symbol "_", "."  ';
//label
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['confirm_password'] = 'Confirm password';
$lang['full_name'] = 'Full name';
$lang['email'] = 'Email';
$lang['phone_no'] = 'Phone number';
$lang['school'] = 'School';
$lang['class'] = 'Class';
$lang['address'] = 'Address';
$lang['group'] = 'Group';
$lang['permission'] = 'Permission';
$lang['choose_group'] = 'Choose group';
//table
$lang['last_login_date'] = 'Last login date';
//tooltip
$lang['edit_tooltip'] = 'Edit row';
$lang['reset_tooltip'] = 'Reset password';
$lang['del_tooltip'] = 'Delete row';
//confirm
$lang['reset_confirm'] = 'Do you want to reset password of this account?';
//alert
$lang['message_reset_success'] = 'Password was reseted successfully';
$lang['message_reset_error'] = 'Do not reset password';