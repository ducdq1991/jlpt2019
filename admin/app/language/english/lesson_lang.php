<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_lesson'] = 'Add New Lesson';
$lang['edit_lesson'] = 'Edit Lesson';
$lang['lesson'] = 'Lesson';
$lang['the_lesson'] = 'The Lesson';
$lang['lessons'] = 'Lessons';
//form
$lang['new_lesson'] = 'The new lesson';
$lang['video'] = 'Video';
$lang['related_link'] = 'Related Link';
$lang['upload_file'] = 'Upload File';
$lang['remove_file'] = 'Remove file';
$lang['choose_file'] = 'Choose file PDF';
$lang['course_thematic'] = 'Course or Thematic';
$lang['choose_course'] = 'Choose Course';
//index
$lang['name_lesson'] = 'Lesson Name';
$lang['document'] = 'Document';
$lang['preview_video'] = 'Preview Video';
$lang['preview_document'] = 'Preview Document';
$lang['not-file'] = 'Not file';
$lang['price'] = 'Price';