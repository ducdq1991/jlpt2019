<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_module'] = 'Add New Module';
$lang['the_module'] = 'The module';
$lang['module'] = 'Module';
$lang['module_management'] = 'Module Management';
$lang['edit_module'] = 'Edit Module';
$lang['controller'] = 'Controler';
$lang['method'] = 'Method';
$lang['num_rows'] = 'Rows';