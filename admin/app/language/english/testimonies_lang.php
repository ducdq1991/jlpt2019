<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_testimonie'] = 'Add New Testimonie';
$lang['edit_testimonie'] = 'Edit Testimonie';
$lang['management_testimoniew'] = 'testimonies Management';
//form add article
$lang['content'] = 'Content';
$lang['description'] = 'Description';

$lang['status'] = 'Status';
$lang['button_update'] = 'Update';
$lang['featured_image_heading'] = 'Featured Image';
$lang['featured_image_button'] = 'Set featured image';
$lang['remove_button'] = 'Remove';

//validation message
$lang['mes_minlength'] = '%s too short, minlength is three chars';
$lang['mes_minlength'] = '%s too short, maxlength is three chars';
//alert message
$lang['new_testimonie'] = 'The new testimonie';
$lang['new_testimonie_detail'] = 'The testimonie detail';
$lang['new_hash_id'] = 'Hash ID';
$lang['testimonies'] = 'Testimonies';
$lang['featured'] = 'Featured';
//status option
$lang['active'] = 'active';
$lang['deactive'] = 'deactive';
//title
$lang['full_name'] = 'full name';
$lang['avartar'] = 'Avartar';
$lang['add_new'] = 'Add new';
$lang['choose_file'] = 'Choose image';
$lang['remove_file'] = 'remove image';