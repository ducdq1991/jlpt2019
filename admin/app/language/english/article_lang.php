<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['add_article'] = 'Add New Article';
$lang['edit_article'] = 'Edit Article';
$lang['management_article'] = 'Articles Management';
//form add article
$lang['content'] = 'Content';
$lang['description'] = 'Description';
$lang['meta_extension'] = 'SEO Optimize';
$lang['meta_title'] = 'Meta title';
$lang['meta_description'] = 'Meta description';
$lang['meta_keywords'] = 'Meta keywords';

$lang['publish_heading'] = 'Publish';
$lang['status'] = 'Status';
$lang['publish_up'] = 'Publish Up';
$lang['publish_down'] = 'Publish Down';
$lang['button_trash'] = 'Trash';
$lang['button_publish'] = 'Publish';
$lang['button_update'] = 'Update';
$lang['featured_image_heading'] = 'Featured Image';
$lang['featured_image_button'] = 'Set featured image';
$lang['remove_button'] = 'Remove';
$lang['category_heading'] = 'Category';
$lang['category_option'] = '-- Choose Category --';
$lang['tags_heading'] = 'Tags';
$lang['tags_add'] = 'Add';
$lang['tags_more'] = 'Choose from the most used tags';
$lang['statistic_heading'] = 'Statistic';
$lang['s_featured'] = 'Featured';
$lang['s_view'] = 'View';
$lang['s_author'] = 'Author';
$lang['draft'] = 'Draft';
$lang['rows'] = 'Rows';

//validation message
$lang['mes_minlength'] = '%s too short, minlength is three chars';

//alert message
$lang['new_article'] = 'The new article';
$lang['new_article_detail'] = 'The article detail';
$lang['new_hash_id'] = 'Hash ID';
$lang['article'] = 'The article';
$lang['articles'] = 'Articles';
$lang['featured'] = 'Featured';
//status option
$lang['publish'] = 'Publish';
$lang['draft'] = 'Draft';
$lang['pending_review'] = 'Pending Review';
//table
$lang['author'] = 'Author';
$lang['category'] = 'Category';
$lang['publish_date'] = 'Publish date';
$lang['created_date'] = 'Created date';