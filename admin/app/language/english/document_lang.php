<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['add_new_document'] = 'Add New Document';
$lang['edit_document'] = 'Edit Document';
$lang['documents_management'] = 'Documents Management';
$lang['the_document'] = 'The Document';
$lang['documents'] = 'Documents';
$lang['category'] = 'Category';
$lang['exam_upload'] = 'Exam upload';
$lang['answer_upload'] = 'Answer upload';
$lang['choose_file'] = 'Choose PDF file';
$lang['remove_file'] = 'Remove file';
$lang['choose_category'] = 'Choose category';
$lang['exam'] = 'Exam';
$lang['answer'] = 'Answer';
$lang['preview_exam'] = 'Preview Exam';
$lang['preview_answer'] = 'Preview Answer';
$lang['not_exist'] = 'Not exist';