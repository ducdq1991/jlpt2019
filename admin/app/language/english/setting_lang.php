<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['setting_management'] = 'Setting Management';
$lang['site_name'] = 'Tên website';
$lang['domain'] = 'Tên miền';
$lang['meta_keyword'] = 'Keyword [SEO]';
$lang['meta_description'] = 'Description [SEO]';
$lang['email'] = 'email';
$lang['site_off_message'] = 'Thông báo tắt website';
$lang['turn_off'] = 'Tắt';
$lang['turn_on'] = 'Bật';
