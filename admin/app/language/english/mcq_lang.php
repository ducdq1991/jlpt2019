<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['module_name'] = 'Multiple Choice Questions';
$lang['true_answer'] = 'Correct answer';
$lang['question'] = 'Questions';
$lang['question_mcq'] = 'Questions';
$lang['answers'] = 'The answer';
$lang['pdf_answer'] = 'Image, PDF';
$lang['video_answer'] = 'Answer Video';
$lang['link_practices'] = 'Practice or doctrine link';
$lang['easy'] = 'Easy';
$lang['normal'] = 'Normal';
$lang['difficult'] = 'Difficult';
$lang['very_difficult'] = 'Very difficult';
$lang['question_level'] = 'Level';
