<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//ini_set('display_errors', 1);
class Bigshare_Controller extends CI_Controller
{
	protected $_loginUrl;
	protected $_langId;
	function __construct()
	{
	   parent::__construct();
	   $this->load->library('user_agent');	 	   
	   $this->_loginUrl = base_url(). 'auth';
       $this->isLogged();
       checking_permission();
       $this->setLanguage();
       date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
/**
 * [isLogged]
 * @return boolean
 */
	public function isLogged()
	{
		$sessionUser = $this->session->userdata('user');
		if( !$sessionUser) {
			if(current_url() != $this->_loginUrl){
				redirect(base_url() .'auth');
			}			
		} else {
			if(current_url() === $this->_loginUrl  || current_url() == base_url()){
				redirect(base_url() . 'dashboard','refresh');
			}
		}
	}

	public function setLanguage(){
		$language = $this->session->userdata('lang');
		if(isset($language) && !empty($language)){
			if($language == 'vietnamese'){
				$this->_langId = 'vi';
			} else {
				$this->_langId = 'en';
			}
		} else {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->setLanguage();
		}
	}
	public function getBaseUrl()
	{
		$this->load->model('setting/setting_model');
		$where = array('option_name' => 'domain');
		$setting = $this->setting_model->getOption( $where );
		$host = !empty($setting) ? $setting->option_value : ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && ! in_array(strtolower($_SERVER['HTTPS']), array( 'off', 'no' ))) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
		return $host;
	}
	public function nonXssData( $data = [] ) {
        if(!empty($data)) {
        	if(is_array($data)) {
	        	$this->load->helper("security");
	        	foreach ($data as $key => $value) {
		            $data[$key] = $this->security->xss_clean($value);
		        }
		    } else {
		    	echo "data do not type of array";die;
		    }
        }
        return $data;
    }
    /*
    * [generateLog]
    * generate a log and insert into user log table
    * [int] $type kind of log, 
    * [int] $userId the ID of member who occur action, 
    * [int] data the ID of object related, 
    * [string] $content the message
    */
    public function generateLog( $type, $userId, $data, $content = '', $assigner = 0 )
    {
        $this->load->model('bigshare_model');
        $whereLog = [
            'user_id' => $userId,
            'data' => $data,
            'type' => $type,
            'assigner' => $assigner
        ];
        $log = $this->bigshare_model->getLog($whereLog);
        if(!is_null($log)) {
            $args = [
                'updated' => date('Y-m-d H:i:s'),
            ];
            $where = ['id' => $log->id];
            return $this->bigshare_model->updateLog( $where, $args );
        } else {
            $args = array(
                'user_id' => (int) $userId,
                'type' => $type,
                'data' => $data,
                'content' => $content,
                'assigner' => $assigner,
                'created' => date('Y-m-d H:i:s'),
                'updated' => date('Y-m-d H:i:s'),
            );
            return $this->bigshare_model->generateLog( $args );
        }
        return false;
    }
    public function getEmail()
	{
		$this->load->model('setting/setting_model');
		$where = array('option_name' => 'email');
		$setting = $this->setting_model->getOption( $where );
		$email = !empty($setting) ? $setting->option_value : 'chiendau.it@gmail.com';
		return $email;
	}
	public function getName()
	{
		$this->load->model('setting/setting_model');
		$where = array('option_name' => 'site_name');
		$setting = $this->setting_model->getOption( $where );
		$name = !empty($setting) ? $setting->option_value : 'Qstudy';
		return $name;
	}

	protected function setPayout($args = [])
	{
		$this->load->model('payout_model');
		$where = [
			'pay_group' => $args['pay_group'],
			'data_id' => $args['data_id'],
			'receiver_id' => $args['receiver_id'],
			'type' => $args['type'],
			'note' => $args['note']
		];
		$payout = $this->payout_model->getPayout($where);
		if (is_null($payout)) {
			$this->payout_model->addPayout($args);
		}
	}

	protected function checkPayout($args = [])
	{
		$this->load->model('payout_model');
		$where = [
			'pay_group' => $args['pay_group'],
			'data_id' => $args['data_id'],
			'receiver_id' => $args['receiver_id'],
			'type' => $args['type'],
			'note' => $args['note']
		];
		$payout = $this->payout_model->getPayout($where);
		if (is_null($payout)) {
			return true;
		}

		return false;
	}
}