<?php defined('BASEPATH') or exit('No direct script access allowed');

class Bigshare_Model extends CI_Model 
{

	protected $_table = '';
	protected $_selectItem = '*';	
	protected $_wheres = array();
	protected $_joins = array();
	protected $_from = null;
	protected $_likes = array();
	protected $_order = array();
	protected $_limit = 0;
	protected $_offset = 0;
	protected $_multiple = false;
	protected $_data = array();
	protected $_batchConditionField = null;
	protected $_batchConditionValue = array();
	protected $_groupBy = array();
	protected $_where_not_in = array();
	protected $_distinct = '';

	public function __construct() 
	{
		parent::__construct();		
	}
	/**
	 * [add]
	 * @param [array] $args  [array('name' => 'Trung', 'email' => 'trung.doan@bigshare.vn')]
	 */
	public function addRecord()
	{
		try{

			if($this->db->insert( $this->_table, $this->_data )){	
				$this->resetQuery();			
				return $this->db->insert_id();
			}

		} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		return false;		
	}
	/**
	 * [update]
	 * @param  [array] $where [array('id' => 1)]
	 * @param  [array] $args  [array('name' => 'Trung', 'email' => 'trung.doan@bigshare.vn')]
	 * @return [boolean]
	 */
	public function updateRecord()
	{
		try{
			if($this->db->where($this->_wheres)->update( $this->_table, $this->_data )){
				$this->resetQuery();
				return true;
			}
			return false;

		} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		$this->resetQuery();
		return false;			
	}

	public function updateBatchRecord()
	{
		try{

			if($this->db
				->where_in($this->_batchConditionField, $this->_batchConditionValue)
				->update( $this->_table, $this->_data))
			{
				$this->resetQuery();
				return true;
			} 

			return false;

		} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		$this->resetQuery();
		return false;
	}
	/**
	 * [deleteRecord]
	 * @param  [int] $id
	 * @return [boolean]
	 */
	public function deleteRecord()
	{
		try{

			if($this->_multiple == true){
				if($this->db->where_in($this->_wheres['field'], $this->_wheres['value'])->delete( $this->_table )){
					$this->resetQuery();
					return true;		
				}
			}

			if($this->db->where($this->_wheres)->delete($this->_table)){
				$this->resetQuery();
				return true;
			}

			return false;

		} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		$this->resetQuery();
		return false;
	}
	/**
	 * [totalRecord description]
	 * @param  array  $where [description]
	 * @return [type]        [description]
	 */
	public function totalRecord()
	{
		try{
			if (!empty($this->_selectItem)) {
				$this->db->select($this->_selectItem);
			}
            if(!empty($this->_joins)){
			    foreach($this->_joins as $join){
			        $this->db->join($join['table'], $join['condition'], $join['type']);
			    }
			}
			if(!empty($this->_where_not_in)) {
				$this->db->where_not_in($this->_where_not_in['field'], $this->_where_not_in['value']);
			}	
			if(!empty($this->_likes)){          	
	        	$this->db->like( $this->_likes['field'], $this->_likes['value']); 
	        } 
	        if (!empty($this->_wheres)) {
	        	$this->db->where($this->_wheres);
	        }
	        if(!empty($this->_batchConditionValue)) {
	        	$this->db->where_in( $this->_batchConditionField, $this->_batchConditionValue);
	        }	     
	        if (!empty($this->_groupBy)) {
	        	$this->db->group_by($this->_groupBy);
	        }   
	        if (!empty($this->_distinct)) {
	        	$this->db->distinct($this->_distinct);
	        }
	        $total = $this->db->count_all_results($this->_table);
			return $total;			

		} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		$this->resetQuery();
		return null;		
	}	


	/**
	 * [getOneRecord description]
	 * @return [type] [description]
	 */
	public function getOneRecord()
    {
        try{   
            $this->db->select( $this->_selectItem );    
            $this->db->from($this->_table);
            if(!empty($this->_joins)){
                foreach($this->_joins as $join){
                    $this->db->join($join['table'], $join['condition'], $join['type']);
                }       
            }
	        if(!empty($this->_likes)){          	
	        	$this->db->like( $this->_likes['field'], $this->_likes['value']); 
	        }

            $this->db->where( $this->_wheres )->limit(1); 
	        if(!empty($this->_batchConditionValue)) {
	        	$this->db->where_in( $this->_batchConditionField, $this->_batchConditionValue);
	        }            
			$query = $this->db->get();                           
            $result = $query->result();  
            $this->resetQuery();          
            if($result && isset($result[0]))
                return $result[0];
            return null;   
            
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());

        }
        $this->resetQuery();
        return null;  
    }


	/**
	 * [getListRecords description]
	 * @return [type] [description]
	 */
	
    public function getListRecords()
    {
        try{    	
	    	$this->db->select( $this->_selectItem );    
	    	$this->db->from($this->_from);
	    	if(!empty($this->_joins)){
	    		foreach($this->_joins as $join){
	    			$this->db->join($join['table'], $join['condition'], $join['type']);
	    		}	    		
	    	}	
	        if(!empty($this->_likes)){          	
	        	$this->db->like( $this->_likes['field'], $this->_likes['value']); 
	        }    	
	        if(is_array($this->_order) && !empty($this->_order)){
	        	foreach($this->_order as $key => $value){
	        		$this->db->order_by($key, $value);
	        	}
	        } else {
	           $this->db->order_by('id', 'desc');
	        }	   			
	        $this->db->where( $this->_wheres )->limit( $this->_limit, $this->_offset );
	        if(!empty($this->_batchConditionValue)) {
	        	$this->db->where_in( $this->_batchConditionField, $this->_batchConditionValue);
	        }
	        if(!empty($this->_where_not_in)) {
	        	$this->db->where_not_in($this->_where_not_in['field'], $this->_where_not_in['value']);
	        }
	        if(!empty($this->_groupBy)){
	        	$this->db->group_by($this->_groupBy);
	        }	        
	        $query = $this->db->get(); 	    
	        //print_r($this->db); exit;
	        if($query){	        	
	        	return $query->result();
	        }
        } catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		return null;  
    }

    /**
     * [resetQuery description]
     * @return [type] [description]
     */
	public function resetQuery()
	{
		$this->_table = '';
        $this->_selectItem = '*';
        $this->_wheres = array();
        $this->_joins = array();     
        $this->_likes = array();
        $this->_data = array();
        $this->_batchConditionValue = array();
        $this->_batchConditionField = null;
        $this->_order = array();
        $this->_limit = 10;
        $this->_offset = 0;
        $this->_multiple = false;
        $this->_groupBy = array();
        $this->_where_not_in = array();
	}

	/**
	 * [writeLog description]
	 * @param  [type] $message [description]
	 * @return [type]          [description]
	 */
	public function writeLog( $message ){
		$data = array();
		$data['table'] = $this->_table;
		$data['message'] = $message;
		$data['date'] = date('Y-m-d H:i:s');
		$this->_table = 'logs';
		$this->_data = $data;
		$this->add();		
		return true;		
	}
	public function generateLog( $args = array() )
	{
		try {
			return $this->db->insert( 'user_logs' , $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function exitLog($where=[])
	{
		try {
			$log = $this->db->where($where)->limit(1)->get( 'user_logs')->result();
			if($log){
				return true;
			}
			return false;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function getLog($where = [])
	{
		try {
			$log = $this->db->where($where)->limit(1)->get('user_logs')->result();
			if(!empty($log)){
				return $log[0];
			}
			return null;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
	public function updateLog($where = [], $args = [])
	{
		try {
			$this->db->where($where);
			return $this->db->update('user_logs', $args);
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return false;
		}
	}
}