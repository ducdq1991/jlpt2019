<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package User
* @author ChienDau
* @copyright Bigshare
* 
*/

class User extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
        $this->lang->load('user', $this->session->userdata('lang'));
        $this->load->model('module/module_model');
        $this->load->model('setting/setting_model');
	}
	
	/**
	* Index, this is default page for user packet
	*/
	public function index( $page = 0 )
	{
        $data = $where = $likes = $head = $orderCondition = [];

        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        if ($numRecord == NULL) {
            $limit =  $limit;
        } else {
            $limit = (int) $numRecord;
        }
        
        $page = intval(str_replace('page-', '', $page));    
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;

        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'users.full_name';
            $likes['value'] = $keyword;
        }
        $data['keyword'] = $keyword;

        $username = $this->input->get('username');
        if (!empty($username)) {
            $likes['field'] = 'users.username';
            $likes['value'] = $username;
        }
        $data['username'] = $username;       

        $status = $this->input->get('status');
        
        if ($status != '') {
          $where['users.status'] = ($status ==='active') ? 1 : 0;
        }
        $data['status'] = $status;  


        //Sorting
        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }
        $data['order'] = $orderOption;        
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc'; 


        $total = $this->user_model->totalUsers($where, $likes);
        
        $listItems = $this->user_model->getUsers($where, $limit, $offset, $likes, $orderCondition);
        $data['listItems'] = $listItems;

        $showFrom = $offset + count($listItems);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];
        $data['paging'] = paging(base_url() .'user', $page, $total, $limit, $queryString);
        $data['itemPerPages'] = $this->config->item('rowPerPages');
        
        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('user_management'), base_url().'user');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 

        $this->load->view('layouts/header', $head);        
		$this->load->view('user/index', $data);
		$this->load->view('layouts/footer');
	}

    public function ajaxsearch()
    {   
        $where = [];
        $keyword = $this->input->get('term');
        if (!empty($keyword)) {
            $likes['field'] = 'users.full_name';
            $likes['value'] = $keyword;
        }                
        $listItems = $this->user_model->getUsers($where, 0, 0, $likes, []);
        echo json_encode($listItems);      
    }
	/**
	* Add, used to add a new member
	*/
	public function add()
    {
        $this->load->library('form_validation');
        
        $modules = $this->module_model->getModules( array('status' => (int) 1), $limit=0, $offset=0 );

        $admin = $this->session->userdata('user');
        if ($admin->access_key != 'supper_admin') {
            $permissions = unserialize($admin->permission);
            $controllerArgs = [];
            foreach ($permissions as $controller => $item){
                $controllerArgs[] = $controller;
            }


            foreach($modules as $key => $module) {
                if (in_array($module->controller, $controllerArgs)) {
                    $module->method = unserialize( $module->method );
                } else {
                    unset($modules[$key]);
                    foreach ($module as $item => $value) {
                        unset($module->$item);
                    }                
                }            
            }
        } else {
            foreach($modules as $key => $module) {
                $module->method = unserialize( $module->method );          
            }            
        }

        $data = array();
        $listGroups = $this->user_model->listGroup();
        foreach ($listGroups as $key => $item) {
            $accessKey = $item->access_key;
            if ($admin->access_key != 'supper_admin') {
                if ($item->access_key == 'supper_admin') {
                    unset($listGroups[$key]);
                }
            }
        }

        $data = array(
            'listGroups' => $listGroups,
            'modules' => $modules
        );
        $config = array(
            array(
                'field' => 'username',
                'label' => lang('username'),
                'rules' => 'trim|required|min_length[6]|max_length[16]|callback_usernameChecking|is_unique[users.username]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_user_minlength'),
                    'max_length' => lang('mes_user_maxlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'password',
                'label' => lang('password'),
                'rules' => 'trim|required|min_length[8]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_pass_minlength'),
                ),
            ),
            array(
                'field' => 'repwd',
                'label' => lang('confirm_password'),
                'rules' => 'trim|required|matches[password]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'matches' => lang('mes_confim_pass'),
                ),
            ),
            array(
                'field' => 'email',
                'label' => lang('email'),
                'rules' => 'trim|required|valid_email|is_unique[users.email]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'valid_email' => lang('mes_valid_email'),
                    'is_unique' => lang('mes_has_registered')
                ),
            ),
            array(
                'field' => 'phone',
                'label' => lang('phone_no'),
                'rules' => 'trim|required|numeric|min_length[10]|max_length[11]|is_unique[users.phone]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'numeric' => lang('mes_number'),
                    'is_unique' => lang('mes_has_registered'),
                    'min_length' => lang('mes_valid_phone'),
                    'max_length' => lang('mes_valid_phone'),
                ),
            )
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('user_management'), base_url().'user');
            $this->breadcrumbs->add(lang('add'), base_url().'user/add');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('user/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            if( !empty( $info['permission'] ) ) {
                $info['permission'] = serialize( $info['permission'] );
            }
            $info['password'] = md5($info['password']);
            if( !empty($info['status']) ) {
                $info['status'] = (int) $info['status'];
            }

            unset($info['repwd']);
            $userId = $this->user_model->insertUser( $info );

            if( $userId ) {
                $message = sprintf(lang('message_create_success'), lang('the_new_user'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), lang('the_new_user'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'user');
        }
    }
    
    /**
     * @name usernameChecking
     * Valid username checking
	 */
    public function usernameChecking( $str )
    {
        $partten = "/^[a-zA-Z][A-Za-z0-9]+$/";
        if(!preg_match($partten , $str)) {
            $this->form_validation->set_message('usernameChecking', lang('usernameChecking'));
            return false;
        }
        return true;
    }
    /**
     * @name Edit
     * Update info of member by member's id
     * */
    public function edit( $userId = 0 )
    {
        if( !$userId ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_user'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'user');
        }
        $this->load->library('form_validation');
        $user = $this->user_model->getUserById( (int) $userId );
        $user->permission = json_encode( unserialize( $user->permission ) );

        $admin = $this->session->userdata('user');
        $listGroup = $this->user_model->listGroup();
        foreach ($listGroup as $key => $item) {
            $accessKey = $item->access_key;
            if ($admin->access_key != 'supper_admin') {
                if ($item->access_key == 'supper_admin') {
                    unset($listGroup[$key]);
                }
            }
        }

        $modules = $this->module_model->getModules( array('status' => (int) 1) );

        $admin = $this->session->userdata('user');

        if ($admin->access_key != 'supper_admin') {
            $permissions = unserialize($admin->permission);
            $controllerArgs = [];
            foreach ($permissions as $controller => $item){
                $controllerArgs[] = $controller;
            }

            foreach($modules as $key => $module) {
                if (in_array($module->controller, $controllerArgs)) {
                    $module->method = unserialize( $module->method );
                } else {
                    unset($modules[$key]);
                    foreach ($module as $item => $value) {
                        unset($module->$item);
                    }                
                }            
            }
        } else {
            foreach($modules as $key => $module) {
                $module->method = unserialize( $module->method );          
            }            
        }

        $data = array(
            'listGroups' => $listGroup,
            'user' => $user,
            'modules' => $modules,
        );
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $config = array(
            array(
                'field' => 'email',
                'label' => lang('email'),
                'rules' => 'trim|required|valid_email|is_unique[users.email]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'valid_email' => lang('mes_valid_email'),
                    'is_unique' => lang('mes_has_registered')
                ),
            ),
            array(
                'field' => 'phone',
                'label' => lang('phone_no'),
                'rules' => 'trim|required|numeric|min_length[10]|max_length[11]|is_unique[users.phone]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'numeric' => lang('mes_number'),
                    'is_unique' => lang('mes_has_registered'),
                    'min_length' => lang('mes_valid_phone'),
                    'max_length' => lang('mes_valid_phone'),
                ),
            )
        );
        
        if( $email == $user->email ) {
            $config[0] = array(
                'field' => 'email',
                'label' => lang('email'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            );
        }
        if ( $phone == $user->phone ) {
            $config[1] = array(
                'field' => 'phone',
                'label' => lang('phone_no'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            );
        }
        $this->form_validation->set_rules( $config );
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('user_management'), base_url().'user');
            $this->breadcrumbs->add(lang('edit'), base_url().'user/edit/' . $userId);
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('user/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            unset($info['username']);
            $info = $this->nonXssData($info);
            if( isset($info['permission']) ) {
                $info['permission'] = serialize( $info['permission'] );
            }
            if( !isset( $info['status'] ) || empty( $info['status'] ) ) $info['status'] = (int) 0;
            
            unset($info['repwd']);
            $query = $this->user_model->updateUser( $userId, $info );
            if( $query ) {
                $message = sprintf(lang('message_update_success'), lang('the_user'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_user'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'user');
        }
    }
    
    /**
     * @name delete
     * Delete a account or any acount by member id, registered ago
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('user'));
            $this->session->set_flashdata('warning', $message);
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->user_model->deleteUser( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('users'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('users'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->user_model->deleteUser( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('user'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('user'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    
    /**
     * @name resetPass
     * Reset password of account, then default password to member's email
     * */
    public function reset( $id )
    {
        $user = $this->user_model->getUserById( (int) $id);
        if( empty($user) ) {
            $message = sprintf(lang('message_delete_warning'), lang('user'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'user');
        }
        $this->load->helper('file');
        $userName = isset($user->meta['full_name']) ? $user->meta['full_name'] : $user->username;
        $args = array();
        $this->load->helper('string');
        $password = random_string('alnum', 16);
        $args['password'] = md5($password);
        $result = $this->user_model->updateUser($user->id,$args);
        if( $result ) {
            $where = array(
                'lang_id' => $this->_langId,
                );
            $where_in = array(
                'key' => 'option_name',
                'value' => array(
                    'protocol', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'charset',  'mailtype',
                    ),
                );
            
            $mailSetting = $this->setting_model->getOptions( $where, $where_in );
            $mailer = [];
            foreach ($mailSetting as $value) {
                $mailer[$value->option_name] = $value->option_value;
            }
            date_default_timezone_set('GMT');
            $this->load->library('email');
            $this->email->initialize(array(
                'protocol' => $mailer['protocol'],
                'smtp_host' => $mailer['smtp_host'],
                'smtp_user' => $mailer['smtp_user'],
                'smtp_pass' => $mailer['smtp_pass'],
                'smtp_port' => (int) $mailer['smtp_port'],
                'charset' => $mailer['charset'],
                'mailtype'  => $mailer['mailtype'],
            ));
            $this->email->from($this->getEmail(), $this->getName());
            $this->email->to($user->email);     
            $this->email->subject('Thông báo cài đặt lại mật khẩu cho tài khoản tại'.$this->getBaseUrl());
            $this->email->set_newline("\r\n");
            $bodyData = [
                '{FULLNAME}' => '<span style="font-weight:bold">'.$userName.'</span>',
                '{DOMAIN}' => '<span style="font-weight:bold">'.$this->getBaseUrl().'</span>',
                '{USERNAME}' => '<span style="font-weight:bold">'.$user->username.'</span>',
                '{PASS}' => '<span style="font-weight:bold">'.$password.'</span>',
            ];
            $path = $this->getBaseUrl().'/email/reset.txt';
            $emailTemplate = read_file($path);
            $mes = $this->paramReplace($emailTemplate, $bodyData);
            $this->email->message($mes);
            $this->email->send();

            $message = sprintf(lang('message_reset_success'));
            $this->session->set_flashdata('success', $message);
        } else {
            $message = sprintf(lang('message_reset_error'));
            $this->session->set_flashdata('error', $message);
        }
        redirect(base_url() . 'user');
    }
    public function paramReplace($txt, $search = array())
    {
        foreach($search as $k=>$v)
        {
            $txt = str_replace($k,$v,$txt);
        }
        return $txt;
    } 
    
    /**
     * @name changeStatus
     * Change status of member
     * */
    public function changeStatus()
	{
        $user = $this->session->userdata('user');
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
                if($user->access_key == 'admin') {
                    foreach ($ids as $key => $value) {
                        $userModify = $this->user_model->getUserForbidden( $value );
                        if($userModify->access_key == 'admin' || $userModify->access_key == 'supper_admin') {
                            unset($ids[$key]);
                        }
                    }
                } else {
                    foreach ($ids as $key => $value) {
                        $userModify = $this->user_model->getUserForbidden( $value );
                        if($userModify->access_key == 'supper_admin') {
                            unset($ids[$key]);
                        }
                    }
                }
				$args['status'] = $status;	
				if($this->user_model->changeStatus( $ids, $args))
				{
                    $message = sprintf(lang('message_change_success'), lang('users'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('users'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
			}
		} else {
            if($user->access_key == 'admin') {
                $userModify = $this->user_model->getUserForbidden( $ids );
                if($userModify->access_key == 'admin' || $userModify->access_key == 'supper_admin') {
                    $message = sprintf(lang('message_forbidden_error'));
                    $this->session->set_flashdata('warning', $message);
                    echo 1;
                    die;
                }
            } else {
                $userModify = $this->user_model->getUserForbidden( $ids );
                if($userModify->access_key == 'supper_admin') {
                    $message = sprintf(lang('message_forbidden_error'));
                    $this->session->set_flashdata('warning', $message);
                    echo 1;
                    die;
                }
            }
			$args['status'] = ($status == 1) ? 0 : 1;		
			if( $this->user_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('user'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('user'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
            
		}			
	}
    
    public function getGroup( $id = 0)
    {
        $group = $this->user_model->getOneGroup( array('id' => (int) $id) );
        if( !is_null( $group ) ) {
            $group->permission = unserialize($group->permission);
            echo json_encode($group->permission);
            die;
        }
    }

    public function updateNameUser()
    {
        $users = $this->user_model->getUsers([],0);
        foreach ($users as $user) {
            $name = isset($user->metas['full_name']) ? $user->metas['full_name'] : NULL;
            $args = [];
            $args['full_name'] = $name;            
            $this->user_model->updateUser($user->id, $args);
        }
    }

    public function updateRank()
    {
        $where = [];
        $where['users.group_id'] = 25;
        $result = $this->user_model->getRanks($where);
        $i = 0;
        $rank = 1;
        while ($i < count($result)) {                        
            if (isset($result[$i])) {
                $this->user_model->updateUser($result[$i]->id, ['rank' => $rank]);    
            }
            $rank++;
            $i++;            
        }
        $this->session->set_flashdata('success', 'BXH đã được cập nhật!');
        redirect(base_url() . 'user');
    }
}