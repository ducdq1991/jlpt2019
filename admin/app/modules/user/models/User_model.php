<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends Bigshare_Model
{
     public function __construct()
    {
        parent::__construct();
        $this->_table = 'users';
    }

    public function getUsers($where = [], $limit = 10, $offset = 0, $likes = [], $orderBy = [], $courseId = 0)
    {
        $this->_selectItem = 'users.*, user_groups.name AS group';
        
        $this->_from = 'users';
        $joins = array();
        $joinGroup['table'] = 'user_groups';
        $joinGroup['condition'] = 'users.group_id = user_groups.id';
        $joinGroup['type'] = 'left';       
        $joins[] = $joinGroup;
        $this->_joins = $joins;
        $this->_limit = $limit;
        $this->_offset = $offset;
        $where['user_groups.access_key !='] = 'supper_admin';
        $this->_wheres = $where;
        $this->_groupBy = array('users.id');
        $this->_order = $orderBy;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }      
        if ($courseId) {
        	$userIds = $this->getUserOfCourse($courseId);
        	$this->_batchConditionField = 'users.id';
        	$this->_batchConditionValue = $userIds;
        }  
        $queryUsers = $this->getListRecords();
        $this->resetQuery();
        $users = array();
        foreach($queryUsers as $user) {
            $users[] = $user;            
        }
        return $users;
    }
    
    protected function getPoint($userId) 
    {
        $this->db->select('SUM(score) as points');
        $this->db->from('user_exam');
        $this->db->where('MONTH(CURDATE()) = MONTH(created)');
        $this->db->where(['user_exam.user_id' => (int) $userId]);

        $query = $this->db->get();           
        if ($query) {
            $result = $query->row();
            return (int) $result->points;
        }

        return 0;        
    }

    public function listGroup()
    {
        $this->_selectItem =  '*';
        $this->_from = 'user_groups';
        return $this->getListRecords();
    }
    
    public function insertUser( $args )
    {
        $this->_data = $args;
        $this->_table = 'users';
        return $id = $this->addRecord();
    }
    
    /**
     * @name updateUser
     * @desc edit a user or some user has selected and update to user table
     * */
    public function updateUser($id = 0, $args = [])
    {
        $where = array('id' => (int) $id );
        $this->_wheres = $where;
        $this->_data = $args;
        $this->_table = 'users';
        return $this->updateRecord();
    }
    /**
     * @name deleteUser
     * @desc delete a user or some user has selected
     * @param [bool] $multiple: true for multi record, fale for single record
     * */
    public function deleteUser( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        if($multiple == true){
            $userId = $where['value'];
        } else {
            $userId = $where['id'];
        }
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }
    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_table = 'users';
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    
    public function totalUsers( $where = [], $likes = [], $courseId = 0)
    {
        
        $joins = array();
        $joinGroup['table'] = 'user_groups';
        $joinGroup['condition'] = 'users.group_id = user_groups.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
        if($courseId != 0) {
            $joinCourse['table'] = 'user_course';
            $joinCourse['condition'] = 'user_course.user_id = users.id';
            $joinCourse['type'] = 'left';  
            $joins[] = $joinCourse;
            $where['user_course.course_id'] = $courseId;
        }

        $this->_joins = $joins;
        $this->_likes = $likes;
        $where['user_groups.access_key !='] = 'supper_admin';
        $this->_wheres = $where;
        return $this->totalRecord(true);
    }
    
    public function getOneGroup( $where = array() )
    {
        $this->_table = 'user_groups';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    
    public function getUserById( $id )
    {
        $this->_selectItem = 'A.id, A.username, A.password, A.email, A.phone, A.status, A.permission, A.last_login, A.group_id, B.name AS group, B.access_key';
        $this->_table = 'users as A';
        $joins = array();
        $joinGroup['table'] = 'user_groups as B';
        $joinGroup['condition'] = 'A.group_id = B.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
        $this->_joins = $joins;
        $where = array();
        $where['A.id'] = (int) $id;
        $this->_wheres = $where;
        $user = $this->getOneRecord();
        $this->resetQuery();
        return $user;
    }

    public function getUserForbidden( $id )
    {
        $this->_selectItem = 'users.id, users.group_id, user_groups.access_key';
        $this->_table = 'users';
        $joins = array();
        $joinGroup['table'] = 'user_groups';
        $joinGroup['condition'] = 'users.group_id = user_groups.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
        $this->_joins = $joins;
        $where = array();
        $where['users.id'] = (int) $id;
        $this->_wheres = $where;
        $user = $this->getOneRecord();
        $this->resetQuery();
        return $user;
    }

    public function getUserOfCourse($id)
    {
    	$query = $this->db->where(['course_id' => (int) $id])->get('user_course')->result();
    	if (!is_null($query)) {
    		$result = [];
    		foreach ($query as $item) {
    			$result[] = (int) $item->user_id;
    		}
    		return $result;
    	}
    	return null;
    }

    public function getTechers($where = [])
    {   
        $this->db->select('id, username, full_name, email');
        $whereIn = [];
        if (isset($where['IN'])) {
            $whereIn = $where['IN'];
            unset($where['IN']);                        
        }
        $this->db->where($where);
        if (!empty($whereIn)) {
            $this->db->where_in($whereIn['key'], $whereIn['value']);    
        }        
        $query = $this->db->get('users');
        if ($query) {
            if (count($query->result()) > 0) {
                return $query->result();
            }
        }        
        return null;
    }


    public function getRanks($where = [])
    {
        try {
            $this->db->select('id,total_point');
            $query = $this->db->where($where);
            $query = $query->order_by('total_point', 'desc')->get('users');
            if ($query->num_rows() > 0) {
                return $query->result();
            }
        } catch (Exception $ex) {
            
        } 

        return null; 
    }    
}