<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('users');?> <small> <?php echo lang('register_new_user'); ?></small></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="username"><?php echo lang('username');?> <span>*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="username" class="form-control" id="username" value="<?php echo set_value('username'); ?>" placeholder="">
                            <?php echo form_error('username', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="password"><?php echo lang('password');?> <span>*</span></label>
                        <div class="col-sm-8"> 
                            <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>" placeholder="">
                            <?php echo form_error('password', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="re-pwd"><?php echo lang('confirm_password');?> <span>*</span></label>
                        <div class="col-sm-8"> 
                            <input type="password" name="repwd" class="form-control" id="repwd" value="<?php echo set_value('repwd'); ?>" placeholder="">
                            <?php echo form_error('repwd', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="full_name"><?php echo lang('full_name');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="full_name" class="form-control" id="full_name" value="<?php echo set_value('full_name'); ?>" placeholder="">
                            <?php echo form_error('full_name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email"><?php echo lang('email');?> <span>*</span></label>
                        <div class="col-sm-8">
                            <input type="email" name="email" class="form-control" id="email" value="<?php echo set_value('email'); ?>" placeholder="">
                            <?php echo form_error('email', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone"><?php echo lang('phone_no');?> <span>*</span></label>
                        <div class="col-sm-8">
                            <input type="tel" name="phone" class="form-control" id="phone" value="<?php echo set_value('phone'); ?>" placeholder="">
                            <?php echo form_error('phone', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="group"><?php echo lang('group');?></label>
                        <div class="col-sm-8">
                        <select url="<?php echo base_url() . 'user/getGroup'; ?>" class="form-control" id="group_id" name="group_id">
                            <option value="0">--<?php echo lang('choose_group');?>--</option>
                            <?php foreach( $listGroups as $item ) : ?>
                                <option value="<?php echo $item->id; ?>" <?php echo set_select('group_id', $item->id); ?>><?php echo $item->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" id="permission">
                        <label class="control-label col-sm-2"><?php echo lang('permission');?>:</label>
                        <?php $methodPerName = $this->config->item('methodPer'); ?>
                        <?php foreach( $modules as $key => $value ) : ?>
                        <div class="col-sm-offset-2 col-sm-10">
                            <?php
                            if( !empty( $value->method ) ) : ?>
                                <label class="parent"><?php echo $value->name; ?></label>
                                <?php foreach ($value->method as $item) : ?>
                                    <label class="child"><input type="checkbox" value="<?php echo $item; ?>" name="permission[<?php echo $value->controller; ?>][]" <?php echo set_checkbox('permission['.$value->controller.'][]', $item); ?> id="<?php echo  $value->controller.'-'.$item; ?>" class="per-child per-child-<?php echo $key; ?>"><?php if(isset($methodPerName[$item])) echo lang($methodPerName[$item]); ?></label>
                                <?php endforeach; 
                            endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <label><input type="checkbox" class="" value="1" name="status" id="status" <?php echo set_checkbox('status', '1'); ?>><?php echo lang('active');?></label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>