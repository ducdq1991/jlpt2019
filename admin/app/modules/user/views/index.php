<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('user_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="processing" style="display: none"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span>Đang xử lý.... Vui lòng đợi trong giây lát!</span></div>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="form-group money-funtions">
                        <div class="col-md-2 bs-money-label">Nạp tiền</div>                       
                        <div class="col-md-2">
                        <input id="bonus_normal" placeholder="Số tiền" type="number" name="bonus_normal" class="form-control">
                        </div>
                        <div class="col-md-1 text-left">
                            <button data-type='0' payment-type="1" data-url="<?php echo base_url();?>transactions/ajaxgive" id="giveMoney" type="button" class="btn btn-primary btn-sm giveMoney">Nạp tiền</button>
                            <button data-type='0' payment-type="0" data-url="<?php echo base_url();?>transactions/ajaxgive" id="giveMoney" type="button" class="btn btn-danger btn-sm giveMoney">Trừ tiền</button>
                        </div>
                    </div>
                    <div class="form-group money-funtions" style="display: none">
                        <div class="col-md-2 bs-money-label">Cộng điểm tháng</div>                       
                        <div class="col-md-2">
                        <input id="bonus_point" placeholder="Số điểm" type="number" name="bonus_point" class="form-control">
                        </div>
                        <div class="col-md-1 text-left"><button data-type='0' data-url="<?php echo base_url();?>transactions/ajaxpoint" id="givePoint" type="button" class="btn btn-danger btn-sm givePoint">Cộng điểm</button></div>
                        <div class="col-md-1 text-left"><button data-url="<?php echo base_url();?>transactions/ajaxpoint" data-type='1' id="givePointAll" type="button" class="btn btn-success btn-sm givePoint">Cộng tất cả</button></div>                                                    
                    </div>                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left"><a href="<?php echo base_url();?>user/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> <?php echo lang('add_new_user');?></a>
                                <a href="<?php echo base_url();?>user/updaterank" class="btn btn-primary btn-sm">
                                <i class="fa fa-tag"></i> Cập nhật bảng xếp hạng</a>
                                </div>
                                <div class="pull-right">
                                    <div class="btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'user/del'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                    <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'user/changestatus'; ?>">
                                    <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                    <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'user/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                                    
                                    </div>                               
                                </div>                                                        
                            </div>                            
                        </div>                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="Username" name="username" value="<?php echo $username;?>">                                
                            </div>                        
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i>Tìm kiếm</button>
                            </div>
                            <div class="col-md-2">
                                <select name="status" class="form-control input-sm sys-filler" onchange="this.form.submit();">
                                    <?php foreach($this->config->item('statusConfigs') as $key=>$value):?>
                                    <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>                                
                                    <?php endforeach;?>
                                </select>                            
                            </div>                        
                            <div class="col-md-4 padding-middle text-right">
                                    <?php 
                                        if (count($totalCount) > 0) {                                        
                                            echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                        }
                                    ?>
                            </div>                            
                        </div>                        
                    </div>  
                        <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>
                                    <th class="text-center">ID</th>
                                    <th class="text-center"><?php echo lang('username');?></th>
                                    <th class="text-center"><?php echo lang('full_name');?></th>
                                    <th class="text-center"><?php echo lang('email');?></th>
                                    <th class="text-center"><?php echo lang('phone_no');?></th>
                                    <th class="text-center"><?php echo lang('group');?></th>
                                    <th class="text-center"><?php echo lang('status');?></th>
                                    <th class="text-center"><?php echo lang('last_login_date');?></th>                    
                                    <th class="text-center">Số dư <a href="javascript: doSort('amount', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Tổng điểm <a href="javascript: doSort('total_point', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Tỷ lệ đúng <a href="javascript: doSort('total_point', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('actions');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listItems as $k=>$user) : ?>
                                <tr id="row-<?php echo $k; ?>" class="">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $user->id;?>" name="checkItems[]" />
                                    </td> 
                                    <td class="text-center"><?php echo $user->id; ?></td>
                                    <td><strong><?php echo $user->username; ?></strong></td>
                                    <td class="text-left"><?php echo $user->full_name; ?> </td>
                                    <td class="text-left"><?php echo $user->email; ?></td>
                                    <td class="text-left"><?php echo $user->phone; ?></td>
                                    <td width="10%" class="text-left"><?php echo $user->group; ?></td>
                                    <td width="7%" class="text-center">
                                            <?php if($user->status == 1): ?>
                                                <i onclick="changeState('<?php echo base_url().'user/changestatus'; ?>', <?php echo $user->id;?>, <?php echo $user->status;?>)" title="<?php echo lang('acc_actived');?>" class="fa fa-check-circle active-status"></i>
                                            <?php else : ?>
                                                <i onclick="changeState('<?php echo base_url().'user/changestatus'; ?>', <?php echo $user->id;?>, <?php echo $user->status;?>)" title="<?php echo lang('acc_deactive');?>" class="fa fa-check-circle deault-status"></i>
                                            <?php endif;?>
                                        </td>
                                    <td width="12%" class="text-right"><?php $last_login = new DateTime( $user->last_login);echo $last_login->format('d-m-Y H:i:s'); ?></td>
                                    <td class="text-center">
                                        <strong><?php echo number_format($user->amount);?></strong> <sup>đ</sup>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php 
                                        echo number_format($user->total_point);
                                        ?></strong>
                                    </td>
                                    <td>
                                        <?php
                                            if ($user->total_exam_question > 0) {
                                                echo round(($user->total_exam_question_true / $user->total_exam_question) * 100, 2);    
                                            } else {
                                                echo 0;
                                            } 
                                            
                                        ?> %
                                        </span>
                                        <span>(<?php echo $user->total_exam_question_true;?>/<?php echo $user->total_exam_question;?>)</span>                                        
                                    </td>                                    
                                    <td  width="10%" class="text-center">
                                        <a href="<?php echo base_url();?>user/edit/<?php echo $user->id; ?>" data-toggle="tooltip" title="<?php echo lang('edit_tooltip');?>">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="confirmReset('<?php echo base_url().'user/reset/'.$user->id; ?>')" class="" data-toggle="tooltip" title="<?php echo lang('reset_tooltip');?>">
                                            <button class="btn btn-xs btn-info" type="button">
                                                <i class="fa fa-refresh"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('user/del'); ?>', <?php echo $user->id; ?>)" data-toggle="tooltip" title="<?php echo lang('del_tooltip');?>">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <select class="rowPerPage form-control input-sm sys-filler">                                
                                        <?php foreach($itemPerPages as $key=>$value):?>
                                        <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                        <?php endforeach;?>
                                    </select>                             
                                </div>
                                <div class="paging col-md-8 text-center">
                                    <?php
                                        if (count($totalCount) > 0) {
                                            echo $paging;    
                                        }                                 
                                    ?>                                
                                </div>
                                <div class="col-md-3 padding-middle text-right">
                                    <?php 
                                        if (count($listItems) > 0) {                                        
                                            echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                        }
                                    ?>
                                </div>                                 
                            </div>                           
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 form-module-action text-right">
                                    <div class="pull-right btn-function" style="display:none;">
                                     <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'user/del'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                    <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'user/changestatus'; ?>">
                                    <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                    <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'user/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                                
                                    </div>
                                    <div class="pull-left">
                                    <a href="<?php echo base_url();?>user/add" class="btn btn-primary btn-sm">
                                    <i class="fa fa-plus"></i> <?php echo lang('add_new_user');?></a>                                        
                                    </div>
                                </div>                                    
                            </div>               
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function confirmReset( url ) {
    if ( confirm("<?php echo lang('reset_confirm');?>") ) {
        window.location.href = url;
    } 
}
</script>