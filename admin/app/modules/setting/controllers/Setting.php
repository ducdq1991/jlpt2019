<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Settings
* @author ChienDau
* @copyright Bigshare
* 
*/

class Setting extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('setting_model');
        $this->lang->load('setting', $this->session->userdata('lang'));
	}
    
    public function general()
    {

    	$where = array(
    		'lang_id' => $this->_langId,
    		);
    	$where_in = array(
    		'key' => 'option_name',
    		'value' => array(
    			'site_name', 'domain', 'meta_keyword', 'meta_description', 'email', 'status', 'site_off_message', 'phone_1', 'phone_2', 'address', 'com_name','meta_title','exchange_rate_points', 'course_discount_percent', 'book_discount_percent'
    			),
    		);
    	$settingResult = $this->setting_model->getOptions( $where, $where_in );
        $setting = [];
    	foreach ($settingResult as $value) {
    		$setting[$value->option_name] = $value->option_value;
    	}
    	$data = array('setting'=>$setting);
    	$this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'domain',
                'label' => lang('domain'),
                'rules' => 'trim|required|valid_url',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'valid_url' => lang('mes_valid_url'),
                ),
            ),
            array(
                'field' => 'email',
                'label' => lang('email'),
                'rules' => 'trim|required|valid_email',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'valid_url' => lang('mes_valid_email'),
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('setting/general', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args = $where = array();
            $order = 0;
            foreach ($info as $key => $value) {
            	$args = array(
            		'lang_id' => $this->_langId,
            		'option_name' => $key,
            		'option_value' => $value,
            	);
            	$where = array(
            		'option_name' => $key,
            		'lang_id' => $this->_langId,
            	);
            	$option = $this->setting_model->getOption( $where );
            	if( $option ) {
            		if( !$this->setting_model->updateOption( $where, $args ) ) {
            			$order++;
            			$message = sprintf(lang('message_update_error'), lang($key));
                		$this->session->set_flashdata('error', $message);
                	}
            	} else {
            		if( !$this->setting_model->insertOption( $args ) ) {
            			$order++;
            			$message = sprintf(lang('message_update_error'), lang($key));
                		$this->session->set_flashdata('error', $message);
                	}
            	}
            }
            if( $order == 0 ) {
            	$message = sprintf(lang('message_update_success'), lang('general'));
                $this->session->set_flashdata('success', $message);
            }
            redirect(base_url() . 'setting/general');
        }
    }
    public function examination()
    {
        $where = [
            'lang_id' => $this->_langId,
            'option_name' => 'examination',
        ];
        $settingResult = $this->setting_model->getOption( $where );
        $data = [];
        if($settingResult) {
            $data['examination'] = $settingResult->option_value;
        }
        $this->load->library('form_validation');
        $config = [
            [
                'field' => 'content_file',
                'label' => lang('exam_upload'),
                'rules' => 'trim',
                'errors' => [],
            ],
        ];
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Cài đặt', base_url().'setting');
            $this->breadcrumbs->add('Đề khảo sát', base_url().'setting/examination');
            $head['breadcrumb'] = $this->breadcrumbs->display();   

            $this->load->view('layouts/header', $head);            
            $this->load->view('setting/examination', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $args = $where = [];
            $args = [
                'lang_id' => $this->_langId,
                'option_name' => 'examination',
                'option_value' => $info['content_file'],
            ];
            $where = [
                'option_name' => 'examination',
                'lang_id' => $this->_langId,
            ];
            $option = $this->setting_model->getOption( $where );
            if( $option ) {
                if( !$this->setting_model->updateOption( $where, $args ) ) {
                    $message = sprintf(lang('message_update_error'), lang('exam_upload'));
                    $this->session->set_flashdata('error', $message);
                } else {
                    $message = sprintf(lang('message_update_success'), lang('exam_upload'));
                    $this->session->set_flashdata('success', $message);
                }
            } else {
                if( !$this->setting_model->insertOption( $args ) ) {
                    $message = sprintf(lang('message_create_error'), lang('exam_upload'));
                    $this->session->set_flashdata('error', $message);
                } else {
                    $message = sprintf(lang('message_create_create'), lang('exam_upload'));
                    $this->session->set_flashdata('success', $message);
                }
            }
            redirect(base_url() . 'setting/examination');
        }
    }
    public function mailer()
    {
        $where = array(
            'lang_id' => $this->_langId,
            );
        $where_in = array(
            'key' => 'option_name',
            'value' => array(
                'protocol', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'charset',  'mailtype',
                ),
            );
        $settingResult = $this->setting_model->getOptions( $where, $where_in );
        $setting = [];
        foreach ($settingResult as $value) {
            $setting[$value->option_name] = $value->option_value;
        }
        $data = array('setting'=>$setting);
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'smtp_user',
                'label' => lang('smtp_user'),
                'rules' => 'trim|required|valid_email',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'valid_email' => lang('mes_valid_email'),
                ),
            ),
            array(
                'field' => 'smtp_pass',
                'label' => lang('smtp_pass'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('setting/mailer', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args = $where = array();
            $order = 0;
            foreach ($info as $key => $value) {
                $args = array(
                    'lang_id' => $this->_langId,
                    'option_name' => $key,
                    'option_value' => $value,
                );
                $where = array(
                    'option_name' => $key,
                    'lang_id' => $this->_langId,
                );
                $option = $this->setting_model->getOption( $where );
                if( $option ) {
                    if( !$this->setting_model->updateOption( $where, $args ) ) {
                        $order++;
                        $message = sprintf(lang('message_update_error'), lang($key));
                        $this->session->set_flashdata('error', $message);
                    }
                } else {
                    if( !$this->setting_model->insertOption( $args ) ) {
                        $order++;
                        $message = sprintf(lang('message_update_error'), lang($key));
                        $this->session->set_flashdata('error', $message);
                    }
                }
            }
            if( $order == 0 ) {
                $message = sprintf(lang('message_update_success'), lang('general'));
                $this->session->set_flashdata('success', $message);
            }
            redirect(base_url() . 'setting/mailer');
        }
    }
    public function videointro()
    {
        $where = [
            'lang_id' => $this->_langId,
            'option_name' => 'video_intro',
        ];
        $settingResult = $this->setting_model->getOption( $where );
        $data = [];
        if($settingResult) {
            $data['video_intro'] = $settingResult->option_value;
        }
        $this->load->library('form_validation');
        $config = [
            [
                'field' => 'video_intro',
                'label' => lang('video_link'),
                'rules' => 'trim',
                'errors' => [],
            ],
        ];
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Cài đặt', base_url().'setting');
            $this->breadcrumbs->add('Video giới thiệu', base_url().'setting/videointro');
            $head['breadcrumb'] = $this->breadcrumbs->display();   

            $this->load->view('layouts/header', $head);            
            $this->load->view('setting/video', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $args = [];
            $args = [
                'lang_id' => $this->_langId,
                'option_name' => 'video_intro',
                'option_value' => $info['video_intro'],
            ];
            if( $settingResult ) {
                if( !$this->setting_model->updateOption( $where, $args ) ) {
                    $message = sprintf(lang('message_update_error'), lang('video_intro'));
                    $this->session->set_flashdata('error', $message);
                } else {
                    $message = sprintf(lang('message_update_success'), lang('video_intro'));
                    $this->session->set_flashdata('success', $message);
                }
            } else {
                if( !$this->setting_model->insertOption( $args ) ) {
                    $message = sprintf(lang('message_create_error'), lang('video_intro'));
                    $this->session->set_flashdata('error', $message);
                } else {
                    $message = sprintf(lang('message_create_create'), lang('video_intro'));
                    $this->session->set_flashdata('success', $message);
                }
            }
            redirect(base_url() . 'setting/videointro');
        }
    }
}