<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('examination_module');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-exam" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-offset-1 control-label col-sm-2" for="content_file"><?php echo lang('exam_upload'); ?></label>
                        <div class="col-sm-8">
                            <div id="load-content-file">
                                <!-- section display image uploaded -->
                            </div>
                            <input type="hidden" name="content_file" value="<?php if(isset($examination)) echo $examination; ?>" id="content_file" />
                            <button type="button" id="load-form-content-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#contentFileModal"><?php echo lang('choose_file'); ?></button>
                            <button type="button" id="remove-content-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="contentFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">           
            <iframe id="content-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=content_file" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
