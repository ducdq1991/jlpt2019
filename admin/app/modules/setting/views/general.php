<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('setting_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <br/>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="site_name"><?php echo lang('site_name');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="site_name" class="form-control" id="site_name" value="<?php if(isset($setting['site_name'])) {echo set_value('site_name', $setting['site_name']);} else {echo set_value('site_name');} ?>" placeholder="">
                            <?php echo form_error('site_name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="domain"><?php echo lang('domain');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="domain" class="form-control" id="domain" value="<?php if(isset($setting['domain'])) : echo set_value('domain', $setting['domain']); else : echo set_value('domain'); endif;?>" placeholder="">
                            <?php echo form_error('domain', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="meta_keyword">Meta Title</label>
                        <div class="col-sm-8">
                            <input type="text" name="meta_title" class="form-control" id="meta_keyword" value="<?php if(isset($setting['meta_title'])) echo set_value('meta_title', $setting['meta_title']); else echo set_value('meta_title'); ?>" placeholder="">
                            <?php echo form_error('meta_title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>                    
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="meta_keyword"><?php echo lang('meta_keyword');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="meta_keyword" class="form-control" id="meta_keyword" value="<?php if(isset($setting['meta_keyword'])) echo set_value('meta_keyword', $setting['meta_keyword']); else echo set_value('meta_keyword'); ?>" placeholder="">
                            <?php echo form_error('meta_keyword', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="meta_description"><?php echo lang('meta_description');?></label>
                        <div class="col-sm-8">
                            <textarea name="meta_description" class="form-control" id="meta_description"><?php if(isset($setting['meta_description'])) echo set_value('meta_description', $setting['meta_description']); else echo set_value('meta_description'); ?></textarea>
                            <?php echo form_error('meta_description', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email"><?php echo lang('email');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="email" class="form-control" id="email" value="<?php if(isset($setting['email'])) echo set_value('email', $setting['email']); else echo set_value('email'); ?>" placeholder="">
                            <?php echo form_error('email', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="status"><?php echo lang('site_status');?></label>
                        <div class="col-sm-8">
                            <select class="form-control" id="status" name="status">
                                <option value="1" <?php if(isset($setting['status'])) echo set_select('status', '1', $setting['status'] == 1); else echo set_select('status', '1');?>><?php echo lang('turn_on');?></option>
                                <option value="0" <?php if(isset($setting['status'])) echo set_select('status', '0', $setting['status'] == 0); else echo set_select('status', '0');?>><?php echo lang('turn_off');?></option>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="site_off_message"><?php echo lang('site_off_message');?></label>
                        <div class="col-sm-8">
                            <textarea name="site_off_message" class="form-control" id="site_off_message"><?php if(isset($setting['site_off_message'])) echo set_value('site_off_message', $setting['site_off_message']); else echo set_value('site_off_message'); ?></textarea>
                            <?php echo form_error('site_off_message', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="com_name"><?php echo lang('com_name');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="com_name" class="form-control" id="com_name" value="<?php if(isset($setting['com_name'])) { echo set_value('com_name', $setting['com_name']);} else {echo set_value('com_name');} ?>" placeholder="">
                            <?php echo form_error('com_name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone_1"><?php echo lang('phone');?></label>
                        <div class="col-sm-8">
                            <input type="tel" name="phone_1" class="form-control" id="phone_1" value="<?php if(isset($setting['phone_1'])) echo set_value('phone_1', $setting['phone_1']); else echo set_value('phone_1'); ?>" placeholder="Số điện thoại 01">
                            <?php echo form_error('phone_1', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            <br/>
                            <input type="tel" name="phone_2" class="form-control" id="phone_2" value="<?php if(isset($setting['phone_2'])) echo set_value('phone_2', $setting['phone_2']); else echo set_value('phone_2'); ?>" placeholder="Số điện thoại 02">
                            <?php echo form_error('phone_2', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="address"><?php echo lang('address');?></label>
                        <div class="col-sm-8">
                            <textarea name="address" class="form-control" id="address"><?php if(isset($setting['address'])) echo set_value('address', $setting['address']); else echo set_value('address'); ?></textarea>
                            <?php echo form_error('address', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="exchange_rate_points">
                        Tỷ lệ đổi điểm ra tiền</label>
                        <div class="col-sm-8">1 điểm = 
                            <input type="number" name="exchange_rate_points" class="form-control" id="exchange_rate_points" value="<?php if(isset($setting['exchange_rate_points'])) echo set_value('exchange_rate_points', $setting['exchange_rate_points']); else echo set_value('exchange_rate_points'); ?>" placeholder="">
                            <?php echo form_error('exchange_rate_points', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="course_discount_percent">
                        Giảm giá khóa học (%)</label>
                        <div class="col-sm-8">
                            <input type="number" name="course_discount_percent" class="form-control" id="course_discount_percent" value="<?php if(isset($setting['course_discount_percent'])) echo set_value('course_discount_percent', $setting['course_discount_percent']); else echo set_value('course_discount_percent'); ?>" placeholder="">
                            <?php echo form_error('course_discount_percent', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="book_discount_percent">
                        Giảm giá sách (%)</label>
                        <div class="col-sm-8">
                            <input type="number" name="book_discount_percent" class="form-control" id="book_discount_percent" value="<?php if(isset($setting['book_discount_percent'])) echo set_value('book_discount_percent', $setting['book_discount_percent']); else echo set_value('book_discount_percent'); ?>" placeholder="">
                            <?php echo form_error('book_discount_percent', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="reset" class="btn btn-white"><?php echo lang('reset');?></button>
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>