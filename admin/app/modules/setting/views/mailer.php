<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('mailer_configuration');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <br/>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="protocol"><?php echo lang('protocol');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="protocol" class="form-control" id="protocol" value="<?php if(isset($setting['protocol'])) { echo set_value('protocol', $setting['protocol']);} else {echo set_value('protocol');} ?>" placeholder="">
                            <?php echo form_error('protocol', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="smtp_host"><?php echo lang('smtp_host');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="smtp_host" class="form-control" id="smtp_host" value="<?php if(isset($setting['smtp_host'])) { echo set_value('smtp_host', $setting['smtp_host']);} else {echo set_value('smtp_host');} ?>" placeholder="">
                            <?php echo form_error('smtp_host', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="smtp_user"><?php echo lang('smtp_user');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="smtp_user" class="form-control" id="smtp_user" value="<?php if(isset($setting['smtp_user'])) { echo set_value('smtp_user', $setting['smtp_user']);} else {echo set_value('smtp_user');} ?>" placeholder="">
                            <?php echo form_error('smtp_user', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="smtp_pass"><?php echo lang('smtp_pass');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="smtp_pass" class="form-control" id="smtp_pass" value="<?php if(isset($setting['smtp_pass'])) { echo set_value('smtp_pass', $setting['smtp_pass']);} else {echo set_value('smtp_pass');} ?>" placeholder="">
                            <?php echo form_error('smtp_pass', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="smtp_port"><?php echo lang('smtp_port');?></label>
                        <div class="col-sm-8">
                            <input type="number" name="smtp_port" class="form-control" id="smtp_port" value="<?php if(isset($setting['smtp_port'])) { echo set_value('smtp_port', $setting['smtp_port']);} else {echo set_value('smtp_port');} ?>" placeholder="">
                            <?php echo form_error('smtp_port', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="charset"><?php echo lang('charset');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="charset" class="form-control" id="charset" value="<?php if(isset($setting['charset'])) { echo set_value('charset', $setting['charset']);} else {echo set_value('charset');} ?>" placeholder="">
                            <?php echo form_error('charset', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="mailtype"><?php echo lang('mailtype');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="mailtype" class="form-control" id="mailtype" value="<?php if(isset($setting['mailtype'])) { echo set_value('mailtype', $setting['mailtype']);} else {echo set_value('mailtype');} ?>" placeholder="">
                            <?php echo form_error('mailtype', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="reset" class="btn btn-white"><?php echo lang('reset');?></button>
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>