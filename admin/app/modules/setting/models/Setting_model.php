<?php defined('BASEPATH') or exit('No direct script access allowed');

class Setting_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_table = 'settings';
    }
    
    /**
     * @name getOption
     * @desc get a row from settings table
     * @param [array] $where
     * */
    public function getOption( $where = array() )
    {
        $this->_table = 'settings';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    
    public function updateOption( $where = array(), $args = array() )
    {
        $this->_table = 'settings';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
    public function insertOption ( $args = array() ) 
    {
        $this->_table = 'settings';
        $this->_data = $args;
        return $this->addRecord();
    }
    public function getOptions( $where = array(), $where_in = array(), $limit = 0, $offset = 0)
    {
        $this->_from = 'settings';
        $this->_wheres = $where;
        $this->_batchConditionField = $where_in['key'];
        $this->_batchConditionValue = $where_in['value'];
        $this->_limit = $limit;
        $this->_offset = $offset;
        return $this->getListRecords();
    }
}