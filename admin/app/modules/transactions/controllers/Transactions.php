<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('transactions_model');
		$this->load->model('user/user_model');
		$this->load->model('setting/setting_model');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
	}
	public function index()
	{

	}

	public function add()
	{
		$this->load->library('form_validation');
		$head = $data = [];

		$config = [
            [
                'field' => 'bonus_normal',
                'label' => 'Tiền thưởng',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Bạn chưa nhập Tiền thưởng',
                ],
            ],
        ];
		$this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
	        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Tiền thưởng cho học sinh', base_url().'transactions');
	        $head['breadcrumb'] = $this->breadcrumbs->display();   

	        $this->load->view('layouts/header', $head);
	        $this->load->view('transactions/add', $data);
	        $this->load->view('layouts/footer');        	
        } else {
        	$postData = $this->input->post();
        	if (isset($postData['type']) && ($postData['type'] == 1)) {
        		$users = $this->user_model->getUsers([], 0);
        		foreach ($users as $user) {
        			$updates = [];
        			$updates['bonus_normal'] = $user->bonus_normal + $postData['bonus_normal'];
        			$updates['amount'] = $user->amount + $postData['bonus_normal'];
        			$this->user_model->updateUser($user->id, $updates);

					$name = !empty($user->full_name) ? $user->full_name : $user->username;
					$content = '';
					$content .= $name.'['.$user->username.']' . ' được thưởng '. $postData['bonus_normal'].' vào tài khoản từ hệ thống';

		            $paymentLogs = [
		                'user_id' => (int) $user->id,
		                'type' => BONUS_NORMAL,
		                'data' => 0,
		                'content' => $content,
		                'assigner' => 0,
		                'created' => date('Y-m-d H:i:s'),
		                'updated' => date('Y-m-d H:i:s'),
		            ];					 
					$this->transactions_model->addPaymentLog($paymentLogs);
        		}        		
                $this->session->set_flashdata('success', 'Thành công!');
        		redirect(base_url() . 'transactions/add');
        	}
        }		
	}

	public function batch()
	{
		$where = ['users.group_id' => 25];
		$data = $this->transactions_model->getPointByUsers($where);		
		$rates = $this->setting_model->getOption(['option_name' => 'exchange_rate_points']);
		$exchangeRate = (int) $rates->option_value;
		if (!is_null($data) && count($data) > 0) {
			foreach ($data as $user) {
				$currentBonus = $user->bonus_exam;
				$lastestPoints = $currentBonus/$exchangeRate;
				
				$justBonusPoints = $user->points - $lastestPoints;
				$bonus = $justBonusPoints * $exchangeRate;

    			$updates = [];
    			$updates['bonus_exam'] = $user->bonus_exam + $bonus;
    			$updates['amount'] = $user->amount + $bonus;
    			$this->user_model->updateUser($user->user_id, $updates);

				$name = !empty($user->full_name) ? $user->full_name : $user->username;
				$content = '';
				$content .= $name.'['.$user->username.']' . ' được thưởng '. number_format($bonus).' vào tài khoản từ hệ thống';

	            $paymentLogs = [
	                'user_id' => (int) $user->user_id,
	                'type' => BONUS_NORMAL,
	                'data' => 0,
	                'content' => $content,
	                'assigner' => 0,
	                'created' => date('Y-m-d H:i:s'),
	                'updated' => date('Y-m-d H:i:s'),
	            ];					 
				$this->transactions_model->addPaymentLog($paymentLogs);

			}
			$this->session->set_flashdata('success', 'Đã thực hiện quy đổi tiền cho thành viên!');
        		redirect(base_url() . 'user');
		}
	}

	public function ajaxGive()
	{
		$response = [];
		$status = 0;
		$message = 'Dữ liệu hoặc thao tác không hợp lệ!';
		$type = (int) $this->input->post('type');
		$paymentType = $this->input->post('payment_type');
		
		$listUserId = $this->input->post('listUserId');
		$bonusNormal = (int) $this->input->post('bonusNormal');

		$whereIn = $where = [];
		$where['users.group_id'] = 25;

		$run = false;

		if (($type == 0) && isset($listUserId) && count($listUserId) > 0) {
			$whereIn = [
				'key' => 'users.id',
				'value' => $listUserId
			]; 
			$run = true;
		}

		if (empty($listUserId) && $type == 0) {
			$message = 'Vui lòng chọn học sinh trước!';
		}

		if ($bonusNormal > 0) {
			$run = true;
		} else {
			$message = 'Bạn chưa nhập số tiền!';
		}

        if ($run == true){

			$users = $this->transactions_model->getUsers($where, $whereIn);

			$paymentLogType = NULL;

			if ($paymentType == 1) {
				$paymentLogType = BONUS_NORMAL;
				$text = ' đã được cộng ';
			} 
			if ($paymentType == 0) {
				$paymentLogType = MINUS_BONUS_NORMAL;
				$text = ' đã bị trừ ';
			}

			if (!is_null($users)) {
				$success = 0;
				foreach ($users as $user) {
					$updates = [];						
					if ($paymentType == 1) {
						$updates['amount'] = $user->amount + $bonusNormal;
					} else {
						$updates['amount'] = $user->amount - $bonusNormal;	
					}
					
					if ($this->user_model->updateUser($user->id, $updates)) {
						$name = !empty($user->full_name) ? $user->full_name : $user->username;
						$content = '';
						$content .= $name.'['.$user->username.']' . $text . $bonusNormal.' đ từ hệ thống';

			            $paymentLogs = [
			                'user_id' => (int) $user->id,
			                'type' => $paymentLogType,
			                'data' => $updates['amount'],
			                'content' => $content,
			                'assigner' => 0,
			                'created' => date('Y-m-d H:i:s'),
			                'updated' => date('Y-m-d H:i:s'),
					            ];					 
						$this->transactions_model->addPaymentLog($paymentLogs);
						$success++;
					}

		        }

		        if ($type == 1) {
		        	$message = 'Tất cả thành viên ' . $text . '<strong>' . number_format($bonusNormal) . ' đ </strong>.';
		        } else {
			        if ($success == count($users)) {
				        $message = 'Những thành viên được chọn ' . $text .' <strong>' . number_format($bonusNormal) . ' đ </strong>.';
			        } else {
				        $message = 'Bạn đã cộng <strong>' . number_format($bonusNormal) . ' đ </strong> cho một số thành viên thành công. Có thể lỗi một vài học sinh do sự cố bất thường.';	        	
			        }	
		        }

		        $status = 1;		
			} 
		}

		if ($status == 1) {
			$this->session->set_flashdata('success', $message);
		} else {
			$this->session->set_flashdata('error', $message);
		}
		
		$response['status'] = $status;
		$response['message'] = $message;
		echo json_encode($response);
		exit;
	}

	public function ajaxPoint()
	{
		$response = [];
		$status = 0;
		$message = 'Dữ liệu hoặc thao tác không hợp lệ!';
		$type = (int) $this->input->post('type');
		$listUserId = $this->input->post('listUserId');
		$bonusPoint = (int) $this->input->post('bonusPoint');

		$whereIn = $where = [];
		$where['users.group_id'] = 25;

		$run = false;

		if (($type == 0) && isset($listUserId) && count($listUserId) > 0) {
			$whereIn = [
				'key' => 'users.id',
				'value' => $listUserId
			]; 
			$run = true;
		}

		if (empty($listUserId) && $type == 0) {
			$message = 'Vui lòng chọn học sinh trước!';
		}

		if ($bonusPoint > 0) {
			$run = true;
		} else {
			$message = 'Bạn chưa nhập điểm cộng!';
		}

        if ($run == true){

			$users = $this->transactions_model->getUsers($where, $whereIn);

			if (!is_null($users)) {
				$success = 0;
				foreach ($users as $user) {
					$updates = [];
					$updates['bonus_point'] = $user->bonus_point + $bonusPoint;
					$updates['point_this_month'] = $user->point_this_month + $updates['bonus_point'];
					if ($this->user_model->updateUser($user->id, $updates)) {
						$name = !empty($user->full_name) ? $user->full_name : $user->username;
						$content = '';
						$content .= $name.'['.$user->username.']' . ' được cộng thêm '. $bonusPoint.' vào tổng điểm trong tháng từ hệ thống';

			            $paymentLogs = [
			                'user_id' => (int) $user->id,
			                'type' => BONUS_POINT,
			                'data' => 0,
			                'content' => $content,
			                'assigner' => 0,
			                'created' => date('Y-m-d H:i:s'),
			                'updated' => date('Y-m-d H:i:s'),
					            ];					 
						$this->transactions_model->addPaymentLog($paymentLogs);
						$success++;
					}

		        }

		        if ($type == 1) {
		        	$message = 'Bạn đã cộng <strong>' . number_format($bonusPoint) . ' điểm </strong> cho tất cả thành viên.';
		        } else {
			        if ($success == count($users)) {
				        $message = 'Bạn đã cộng <strong>' . number_format($bonusPoint) . ' điểm </strong> cho các thành viên được chọn.';
			        } else {
				        $message = 'Bạn đã cộng <strong>' . number_format($bonusPoint) . ' điểm </strong> cho một số thành viên thành công. Có thể lỗi một vài học sinh do sự cố bất thường.';	        	
			        }	
		        }

		        $status = 1;		
			} 
		}

		if ($status == 1) {
			$this->session->set_flashdata('success', $message);
		} else {
			$this->session->set_flashdata('error', $message);
		}
		
		$response['status'] = $status;
		$response['message'] = $message;
		echo json_encode($response);
		exit;
	}

	public function updatePoint()
	{
		$where = [
			'users.group_id' => 25,
			'MONTH(user_exam.created) = MONTH(CURDATE())' => null,
		];

		$data = $this->transactions_model->getPointByUsers($where);

		$rates = $this->setting_model->getOption(['option_name' => 'exchange_rate_points']);
		$exchangeRate = (int) $rates->option_value;

		if (!is_null($data) && count($data) > 0) {
			foreach ($data as $user) {
    			$updates = [];
    			$bonusPointNormal = $user->bonus_point; 
    			$updates['point_this_month'] = (int) $user->points + $bonusPointNormal;

				$currentBonus = $user->bonus_exam;
				$lastestPoints = $currentBonus/$exchangeRate;
				
				$justBonusPoints = $user->points - $lastestPoints + $bonusPointNormal;
				$bonus = $justBonusPoints * $exchangeRate;

    			$updates['bonus_exam'] = $user->bonus_exam + $bonus;
    			$updates['amount'] = $user->amount + $bonus;
    			$this->user_model->updateUser($user->user_id, $updates);

				$name = !empty($user->full_name) ? $user->full_name : $user->username;
				$content = '';
				$content .= $name.'['.$user->username.']' . ' được quy đổi điểm và được cộng'. number_format($bonus).' vào tài khoản từ hệ thống';

	            $paymentLogs = [
	                'user_id' => (int) $user->user_id,
	                'type' => BONUS_EXAM,
	                'data' => 0,
	                'content' => $content,
	                'assigner' => 0,
	                'created' => date('Y-m-d H:i:s'),
	                'updated' => date('Y-m-d H:i:s'),
	            ];					 
				$this->transactions_model->addPaymentLog($paymentLogs);

			}    
			$this->session->set_flashdata('success', 'Thực hiện quy đổi thành công!');    	
		} else {
			$this->session->set_flashdata('error', 'Chưa có dữ liệu mới!');
		}
		
        redirect(base_url() . 'user');		
	}

	public function resetBonusExam()
	{
		/*$where = [
			'users.group_id' => 25,
		];		
		$users = $this->transactions_model->getPointByUsers($where);
		foreach ($users as $user) {
			$updates = [];
			$updates['bonus_exam'] = 0;
			$updates['point_this_month'] = 0;
			$updates['bonus_point'] = 0;
			$updates['amount'] = $user->amount - $user->bonus_exam;
			$this->user_model->updateUser($user->user_id, $updates);			
		}*/
		$this->resetPoint();
		$this->session->set_flashdata('success', 'Thành công!');
		redirect(base_url() . 'user');
	}

	public function resetPoint()
	{
		$where = [
			'users.group_id' => 25,
		];		
		$users = $this->transactions_model->getUsers($where);
		foreach ($users as $user) {
			$updates = [];
			$updates['point_this_month'] = 0;
			$updates['bonus_point'] = 0;
			$updates['bonus_exam'] = 0;			
			$amount = $user->amount - $user->bonus_exam - $user->bonus_normal;
			if($amount < 0) {
				$amount = 0;
			}		
			//$amount = 0;	
			$updates['amount'] = $amount;
			
			$this->user_model->updateUser($user->id, $updates);			
		}
	}
}