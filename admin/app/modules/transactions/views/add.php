<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-money"></i> Thưởng tiền cho học sinh</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php elseif( $this->session->flashdata('success') ) : ?>
                        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                    <?php else: ?>
                    <p></p>
                <?php endif;?>    
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                <?php echo validation_errors('<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>                
                    <div class="form-group">
                        <div class="col-sm-8">
                        <label class="text-right">Số tiền</label>
                            <input type="number" name="bonus_normal" class="form-control">
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8">
                        <label class="text-right">Phương án thưởng</label><br>
                            <input type="radio" name="type" value="1"> Tất cả
                            <input type="radio" name="type" value="2"> Nhóm học sinh
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8"><button type="submit" class="btn btn-success">Xác nhận</button></div>
                    </div>                    
                </form>
            </div>
        </div>
    </div>
</div>