<?php defined('BASEPATH') or exit('No direct script access allowed');

class Transactions_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_table = '';
    }

    public function getPointByUsers($where = [])
    {
        $this->db->select('users.bonus_exam, users.bonus_point, users.point_this_month, users.amount, user_exam.user_id, SUM(user_exam.score) as points, users.full_name, users.username, user_exam.created');
        $this->db->from('user_exam');
        $this->db->join('users', 'users.id = user_exam.user_id', 'inner');
        $this->db->where($where);
        $this->db->group_by('user_id');
        $query = $this->db->get();
        if ($query) {
            return $query->result();
        }

        return null;
    }

    public function getUsers($where = [], $whereIn = [], $limit = 0)
    {
        $this->db->select('users.id, users.full_name, users.bonus_point, users.point_this_month, users.bonus_normal, users.bonus_exam, users.amount,users.id, users.username, users.password, users.email, users.phone, users.status, users.permission, users.last_login, users.group_id');
        $this->db->from('users');
        $this->db->where($where);
        if (!empty($whereIn)) {
            $this->db->where_in($whereIn['key'], $whereIn['value']);    
        }
        
        $this->db->limit($limit);
        $query = $this->db->get();
        if ($query) {
            if (count($query->result()) > 0) {
                return $query->result();
            }
        }

        return null;
    }

    public function addPaymentLog($data)
    {
        if ($this->db->insert('user_logs', $data)) {
            return true;
        }

        return false;
    }
}