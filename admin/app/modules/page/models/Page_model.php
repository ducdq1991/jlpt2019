<?php defined('BASEPATH') or exit('No direct script access allowed');

class Page_model extends Bigshare_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->_table = 'pages';
    }
    
    public function insertPage( $args = array() ) 
    {
        $this->_data  =$args;
        return $this->addRecord();
    }
    
    
    public function updatePage( $id, $args = array() )
    {
        $where = array();
        $where['pages.id'] = (int) $id;
        $this->_table = 'pages';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getPages( $where = array(), $limit = 10, $offset = 0, $orderBy, $likes = array())
    {
        $this->_selectItem = '*';
        $this->_from = 'pages';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;       
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $query = $this->getListRecords();
        $this->resetQuery();
        $items = array();
        foreach ($query as $value) {
            $author = $this->getAuthor( $value->created_by );
            $value->author = $author;
            $items[] = $value;
        }
        return $query;
    }

    public function getAuthor( $id ) 
    {
        $where = array('user_id' => (int) $id, 'meta_key' => 'full_name');
        $this->_wheres = $where;
        $this->_table = 'user_metas';
        $author = $this->getOneRecord();
        if(!empty($author->meta_value))
            return $author->meta_value;
        return '';
    }

    public function totalPages( $where = array() )
    {
        $this->_wheres = $where;
        return $this->totalRecord();
    }

    /**
     * @name deleteActicle
     * @desc delete a article or some article has selected
     * @param [bool] $multiple: true for multi record, fale for single record
     * */
    public function deletePage( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        if($multiple == true){
            $postId = $where['value'];
        } else {
            $postId = $where['id'];
        }
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }

    /**
     * @name changeStatus
     * @desc change status a row or any rows from posts table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    /**
     * [getPageById]
     * @param  [int] $id
     * @return [object]
     */
    public function getPage( $where = array())
    {
        $this->_selectItem = '*';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
}