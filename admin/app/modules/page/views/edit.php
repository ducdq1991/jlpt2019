<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-edit"></i> <?php echo lang('edit_page');?> | <?php echo $page->title;?></h5>
            </div>
            
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-page" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-1" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-11">
                                <input type="text" name="title" class="autoSlug auto-article-slug form-control" id="title" value="<?php echo set_value('title', $page->title); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-1" for="title"><?php echo lang('alias'); ?></label>
                            <div class="col-sm-11">
                                <input type="text" name="slug" class="slug slug-article form-control" id="slug" value="<?php echo set_value('slug', $page->slug); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12 sr-only" for="title"><?php echo lang('content'); ?></label>
                            <div class="col-sm-12">                                                                     
                                <textarea id="content" name="content" rows="15" cols="80" class="editor form-control"><?php if($page->content) echo set_value('content', $page->content); else echo set_value('content'); ?></textarea>
                            </div>
                        </div>
                        <?php $meta_extension = !empty($page->meta_description || $page->meta_keywords || $page->meta_title) ? true : false; ?>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label><input type="checkbox" class="" value="1" name="meta_extension" id="meta_extension" <?php echo set_checkbox('meta_extension', '1', $meta_extension); ?>> <?php echo lang('meta_extension'); ?></label>
                            </div>
                        </div>
                        <div class="meta-extension-box form-group" <?php if( !$meta_extension) echo 'style="display: none;"'; ?>>
                            <label class="col-sm-12" for="meta_title"><?php echo lang('meta_title'); ?></label>
                            <div class="col-sm-12">
                                <textarea name="meta_title" class="form-control" id="meta_title"><?php if($page->meta_title) echo set_value('meta_title', $page->meta_title); else echo set_value('meta_title'); ?></textarea>
                            </div>
                        </div>
                        <div class="meta-extension-box form-group" <?php if( !$meta_extension) echo 'style="display: none;"'; ?>>
                            <label class="col-sm-12" for="meta_description"><?php echo lang('meta_description'); ?></label>
                            <div class="col-sm-12">
                                <textarea name="meta_description" class="form-control" id="meta_description"><?php if($page->meta_description) echo set_value('meta_description', $page->meta_description); else echo set_value('meta_description'); ?></textarea>
                            </div>
                        </div>
                        <div class="meta-extension-box form-group" <?php if( !$meta_extension) echo 'style="display: none;"'; ?>>
                            <label class="col-sm-12" for="meta_keywords"><?php echo lang('meta_keywords'); ?></label>
                            <div class="col-sm-12">
                                <textarea name="meta_keywords" class="form-control" id="meta_keywords"><?php if($page->meta_keywords) echo set_value('meta_keywords', $page->meta_keywords); else echo set_value('meta_keywords'); ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-cogs"></i> <?php echo lang('publish_heading'); ?></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="group"><?php echo lang('status'); ?></label>
                                    <div class="col-sm-8">
                                        <select class="form-control" id="status" name="status">
                                            <?php $statusArticle = $this->config->item('statusArticle'); ?>
                                            <?php foreach ($statusArticle as $key => $value) : ?>
                                                <option value="<?php echo $key; ?>" <?php echo set_select('status', $key, $key == $page->status); ?>><?php echo lang($value); ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <a onclick="confirmDelete('<?php echo base_url('page/del').'/'.$page->id; ?>')" title="">
                                <button type="button" class="btn btn-sm btn-white"><?php echo lang('button_trash'); ?></button>
                                </a>
                                <button type="submit" id="page-submit" class="btn btn-sm btn-primary"><?php echo lang('button_update'); ?></button>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><i class="fa fa-picture-o"></i> <?php echo lang('featured_image_heading'); ?></div>
                            <div class="panel-body">
                                <div id="load-image">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="image" value="<?php if($page->image) echo set_value('image', $page->image); else echo set_value('image'); ?>" id="image" />
                                <button type="button" id="load-form-featured-image" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredImageModal"><?php echo lang('featured_image_button'); ?></button>
                                <button type="button" id="remove-featured-image" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_button'); ?></button>
                            </div>  
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Featured Image Modal -->
<div id="featuredImageModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-image-iframe" src="<?php echo base_url(); ?>/assets/js/filemanager/dialog.php?field_id=image" width="100%" height="450px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<?php echo $tinymce; ?>
<script>
var _baseUrl = "<?php echo base_url(); ?>";
function confirmDelete( $url ) {
    if ( confirm(message_confirm_delete) ) {
        window.location.href = $url;
    } 
}
</script>