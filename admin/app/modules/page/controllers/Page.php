<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Page
* @author TrungDoan
* @copyright Bigshare
* 
*/

class Page extends Bigshare_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('page_model');
        $this->lang->load('page', $this->session->userdata('lang'));
    }
    
    /**
    * Index, this is default page for page packet
    * Show all page
    */
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $where['pages.lang_id'] = $this->_langId;
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'pages.title';
            $likes['value'] = $keyword;
        }

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if( $status != '' ){
          $where['pages.status'] = $status;
        }
        $total = $this->page_model->totalPages( $where );
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'page', $page, $total, $limit, $queryString);
        $pages = $this->page_model->getPages($where, $limit, $offset, $orderCondition, $likes);
        $data['pages'] = $pages;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($pages);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('management_page'), base_url().'page');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('page/index', $data);
        $this->load->view('layouts/footer');
    }
    /**
    * Add
    * insert a new page into posts table
    **/
    public function add()
    {
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);
        $author = $this->session->userdata('user');
        $data['author'] = array(
            'id' =>$author->id,
            'username' => $author->username,
        );
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[pages.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[pages.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('management_page'), base_url().'page');
            $this->breadcrumbs->add(lang('add_page'), base_url().'page/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('page/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $page = array();
            $postData = $this->input->post();            
            unset($postData['meta_extension']);
            $postData['created_date'] = date('Y-m-d H:i:s');
            $pageId = $this->page_model->insertPage( $postData );
            if( $pageId ) {
                $args = array();
                $args['hash_id'] = md5( $pageId );
                if( !$this->page_model->updatePage( $pageId, $args ) )
                {
                    $message = sprintf(lang('message_create_success'), lang('new_page'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_create_error'), lang('new_page_detail'));
                    $this->session->set_flashdata('error', $message);                    
                }
                redirect(base_url() . 'page/edit/' . $pageId);
            } else {
                $message = sprintf(lang('message_creat_error'), lang('new_page'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'page');
        }
    }

    /**
     * @name delete
     * Delete a page or any page by id
     * */
    public function del( $id = 0 )
    {
        if( $id ) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'page');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->page_model->deletePage( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('pages'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('pages'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->page_model->deletePage( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('page'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('page'));
                $this->session->set_flashdata('error', $message);
            }
            if( $id ) {
                redirect(base_url() . 'page');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'pages.lang_id' => $this->_langId,
            'pages.id' => (int) $id,
            );
        $page = $this->page_model->getPage( $where );
        if ( empty($page) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('error', $message);
            redirect( base_url() . 'page');
        }
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(500);
        $author = $this->session->userdata('user');
        $data['author'] = array(
            'id' =>$author->id,
            'username' => $author->username,
        );
        $data['page'] = $page;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[pages.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[pages.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
        //checking changed title
        $title = $this->input->post('title');
        $slug = $this->input->post('slug');
        if( $page->title == $title ) {
            $config[0] = array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                ),
            );
        }
        if( $page->slug == $slug ) {
            $config[1] = array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required')
                ),
            );
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('management_page'), base_url().'page');
            $this->breadcrumbs->add($page->title, base_url().'page/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('page/edit', $data);
            $this->load->view('layouts/footer');
        } else {            
            $postData = $this->input->post();            
            unset($postData['meta_extension']);
            if( $this->page_model->updatePage( $id, $postData ) ) {
                    $message = sprintf(lang('message_update_success'), lang('page'));
                    $this->session->set_flashdata('success', $message);
            } else {
                    $message = sprintf(lang('message_update_error'), lang('new_page_detail'));
                    $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'page/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->page_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
}