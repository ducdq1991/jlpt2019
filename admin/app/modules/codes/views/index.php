<div class="row">
    <div class="col-lg-12">        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-book"></i> Danh sách</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                    <form method="GET" accept-charset="utf-8" id="formList">
                        <input type="hidden" id="sort" name="order" value="">
                        <input type="hidden" id="sortBy" name="by" value="">
                        <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">

                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'codes/del'; ?>">
                                    <i class="fa fa-remove"></i> Xóa</a>                                
                                </div>
                                <a href="<?php echo base_url();?>codes/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Thêm mới</a>
                            </div>                    
                        </div>                                                
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="code" value="<?php echo $code;?>">                                
                            </div>
                            <div class="col-md-3">
                                <select name="course_id" class="select2 form-control">
                                    <option value="">Khóa học</option>
                                    <?php
                                        $courseNames = [];
                                        if (isset($courses) && count($courses) > 0):
                                            foreach ($courses as $item) :
                                                $courseNames[$item->id] = $item->name;
                                    ?>
                                    <option <?php if ($courseId == $item->id): ?> selected="selected" <?php endif;?> value="<?php echo $item->id;?>"><?php echo $item->name;?></option>          
                                    <?php 
                                            endforeach;
                                        endif;
                                    ?>                       
                                </select>                                
                            </div>                            
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                                <a href="<?php echo base_url('codes') .  $queryStringExport;?>" title="" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Excel</a>
                            </div>  
                            <div class="col-md-5 padding-middle text-right pull-right">
                                <?php if (count($listItems) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                                                                     
                        </div>
                        <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center" width="50">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>
                                    <th class="text-center" width="100">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-left">Mã <a href="javascript: doSort('code', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-left">Khóa học <a href="javascript: doSort('course_id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>                               
                                    <th class="text-center" width="150">Ngày tạo
                                        <a href="javascript: doSort('created_at', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                    </th>
                                    <th class="text-center" width="150">Tình trạng
                                        <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                    </th>                                    
                                    <th class="text-center" width="100"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listItems as $item) : ?>
                                <tr id="" class="">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $item->id;?>" name="checkItems[]" />
                                    </td> 
                                    <td class="text-center"><?php echo $item->id; ?></td>
                                    <td>
                                        <strong><?php echo $item->code; ?></strong>
                                    </td>
                                    <td>
                                        <?php if (isset($courseNames[$item->course_id])) echo $courseNames[$item->course_id]; ?>
                                    </td>                                    
                                    <td><?php echo date('d/m/Y H:i:s', strtotime($item->created_at));?></td>
                                    <td class="text-center">
                                        <?php if($item->status == 1):?>
                                            <span class="label label-success">Còn hiệu lực</span>
                                        <?php else : ?>
                                            <span class="label label-danger">Hết hiệu lực</span>
                                        <?php endif;?>
                                    </td>                                                                        
                                    <td width="10%" class="text-center">
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($listItems) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                                <?php if (count($listItems) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'codes/del'; ?>">
                                    <i class="fa fa-remove"></i> Xóa</a>                               
                                </div>  
                                <a href="<?php echo base_url();?>codes/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Thêm mới</a>
                            </div>                    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var message_confirm_delete = '<?php echo lang('message_confirm_delete'); ?>';
</script>

<script type="text/javascript">
$(document).ready(function() {
  $(".select2").select2();
});
</script>