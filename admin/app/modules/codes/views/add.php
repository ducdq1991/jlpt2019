<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> Tạo mã cho khóa học</h5>
            </div>
            <div class="ibox-content">
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Số lượng mã<span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="nums" class="form-control" id="title" value="<?php echo set_value('nums'); ?>" placeholder="Số lượng mã cần tạo">
                                <?php echo form_error('nums', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Khóa học</label>
                            <div class="col-sm-10">
                                <select name="course_id" class="select2 form-control">
                                    <option value="">Khóa học</option>
                                    <?php
                                        if (isset($courses) && count($courses) > 0):
                                            foreach ($courses as $item) :
                                    ?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>          
                                    <?php 
                                            endforeach;
                                        endif;
                                    ?>                       
                                </select>                                
                                <?php echo form_error('course_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"></label>
                            <div class="col-sm-10">
                            <button type="button" id="article-submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Tạo mã</button>
                            </div>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $(".select2").select2();
});
</script>