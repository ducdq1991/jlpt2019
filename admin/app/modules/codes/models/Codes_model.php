<?php defined('BASEPATH') or exit('No direct script access allowed');

class Codes_model extends Bigshare_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->_table = 'course_codes';
    }
    
    public function addCode($args = []) 
    {
        $this->_table = 'course_codes';
        $this->_data = $args;
        return $this->addRecord();
    }
    
    
    public function updateCode($where = [], $args = [])
    {
        $this->_table = 'course_codes';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getCodes($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {
        $this->_selectItem = '*';
        $this->_from = 'course_codes';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;       
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }

    public function totalCode($where = [], $likes = [])
    {
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }          
        return $this->totalRecord();
    }


    public function deleteCode($where = [], $multiple = false)
    {
        
        $this->_multiple = $multiple;
        $this->_wheres = $where;
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }

    public function changeStatus($ids, $args = [])
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int) $ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getCode($where = [])
    {
        $this->_table = 'course_codes';
        $this->_selectItem = '*';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
}