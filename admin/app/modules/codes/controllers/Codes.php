<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codes extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('codes_model');
        $this->load->model('course/course_model');
	}
    
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];

        //Get list courses
        $courses = $this->course_model->getCourses(['courses.parent' => 0]);   
        $data['courses'] = $courses;

        foreach ($courses as $item) {
            $courseNames[$item->id] = $item->name;
        }

        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $code = $this->input->get('code');
        if (!empty($code)) {
            $likes['field'] = 'course_codes.code';
            $likes['value'] = $code;
        }
        $data['code'] = $code;
        $orderCondition['course_codes.id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if ( $status != '' ){
          $where['course_codes.status'] = $status;
        }

        $courseId = $this->input->get('course_id');
        $data['courseId'] = $courseId;
        if ( $courseId != '' ){
          $where['course_codes.course_id'] = $courseId;
        }        

        $isExport = $this->input->get('is_export');
        if ($isExport == 1) {
            $codes = $this->codes_model->getCodes($where, 0, $offset, $orderCondition, $likes);
            $this->exportCSV($codes, $courseNames);
        }

        $total = $this->codes_model->totalCode($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        if ($queryString == '') {
            $data['queryStringExport'] = '?is_export=1';
        } else {
            $data['queryStringExport'] = '?' . $queryString . '&is_export=1';
        }
        
        $data['paging'] = paging(base_url() .'codes', $page, $total, $limit, $queryString);
        $codes = $this->codes_model->getCodes($where, $limit, $offset, $orderCondition, $likes);
        $data['listItems'] = $codes;
        // Set actived Fillter        
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($codes);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Danh sách mã khóa học', base_url().'codes');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
    }

    protected function exportCSV($codes = [], $courseNames = [])
    {
        $this->load->helper('download');
        $this->load->library('PHPReport');
        $template = 'Codes.xlsx';
        $templateDir = FCPATH . "assets" . DIRECTORY_SEPARATOR;
        $config = [
            'template' => $template,
            'templateDir' => $templateDir      
        ];
        $data = []; 
        $i = 1;
        if (!empty($codes)) {
            foreach ($codes as  $item) {
                $item->date = date('d/m/Y' , strtotime($item->created_at));
                $item->stt = $i;    
                $item->course_name = $courseNames[$item->course_id];
                $item = (array) $item;
                $data[] = $item;
                $i++;
            }
        }

        //load template
        $R = new PHPReport($config);

        $R->load([
            [
              'id' => 'item',
              'repeat' => TRUE,
              'data' => $data  
            ]
        ]);

        // define output directoy
        $outputFileDir =  FCPATH . "assets" . DIRECTORY_SEPARATOR;
        $filename = 'Ma_khoa_hoc' . time();
        $file = $outputFileDir  . $filename . ".xlsx";
        $result = $R->render('excel', $file);
        force_download($file, NULL);        
    }

    public function add()
    {
        $data = array();        
        //Get list courses
        $courses = $this->course_model->getCourses(['courses.parent' => 0]);   
        $data['courses'] = $courses;        

        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'nums',
                'label' => 'Số lượng mã',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Vui lòng nhập số lượng mã',
                ),
            ),
            array(
                'field' => 'course_id',
                'label' => 'Mã khóa học',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Vui lòng chọn khóa học.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Mã khóa học', base_url().'codes');
            $this->breadcrumbs->add('Thêm mới', base_url().'codes/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('add', $data);
            $this->load->view('layouts/footer');
        } else {
            $this->load->helper('codes');
            $postData = $this->input->post();           
            $nums = (int) $postData['nums'];
            $courseId = (int) $postData['course_id'];

            if ($nums > 0 && $courseId > 0) {
                $i = 0;
                while ($i <= $nums) {
                    $code = createRandomCode(8);                
                    if (is_null($this->codes_model->getCode(['course_codes.code' => $code]))) {
                        $args = [];
                        $args['code'] = $code;
                        $args['course_id'] = $courseId;
                        $args['total_use'] = 0;
                        $args['created_at'] = date('Y-m-d H:i:s');
                        if ($this->codes_model->addCode($args) ) {
                            unset($args);                           
                            $i++;
                        }
                    }
                }
                $numCode = $i-1;
                $this->session->set_flashdata('success', 'Đã tạo ' . $numCode . ' thành công!');
                redirect( base_url() . 'codes');
            }

            redirect(base_url() . 'codes');
        }
    }


    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if(empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'course_codes');
            return false;
        }
        $where = array(
            'field' => 'subject_id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->codes_model->deleteSubject($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa môn học thành công.');
            } else {
                $this->session->set_flashdata('error', 'Không xóa được môn học');
            }
            echo 1;
            die;
        } else {
            $query = $this->codes_model->deleteSubject( array( 'subject_id' => (int) $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa môn học thành công.');
            } else {                
                $this->session->set_flashdata('error', 'Không xóa được môn học');
            }
            if( $id ) {
                redirect(base_url() . 'course_codes');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'course_codes.subject_id' => (int) $id,
            );
        $subject = $this->codes_model->getSubject($where);        
        if (empty($subject)) {            
            $this->session->set_flashdata('error', 'Môn học không tồn tại');
            redirect( base_url() . 'course_codes');
        }
        $postData = $this->input->post();

        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $data['subject'] = $subject;
        $this->load->library('form_validation');
        $validateName = $validateSlug = '';
        if (isset($postData['name'])) {
            if ($subject->name != $postData['name']) {
                $validateName = '|is_unique[course_codes.name]';
            }
        }

        if (isset($postData['slug'])) {
            if ($subject->slug != $postData['slug']) {
                $validateSlug = '|is_unique[course_codes.slug]';
            }
        }

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên môn học',
                'rules' => 'trim|required' . $validateName,
                'errors' => array(
                    'required' => 'Tên môn học là bắt buộc.',
                    'is_unique' => 'Môn học này đã tồn tại',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required' . $validateSlug,
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Môn học', base_url().'course_codes');
            $this->breadcrumbs->add($subject->name, base_url().'course_codes/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('edit', $data);
            $this->load->view('layouts/footer');
        } else {                        
            if( $this->codes_model->updateSubject($where, $postData ) ) {
                $this->session->set_flashdata('success', 'Đã cập nhật môn học.');
            } else {                    
                $this->session->set_flashdata('error', 'Không cập nhật được môn học.');
            }

            redirect(base_url() . 'course_codes/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->codes_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
}