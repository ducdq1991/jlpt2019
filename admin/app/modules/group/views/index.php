<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> <?php echo lang('group_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-right form-group-action">
                        <a href="<?php echo base_url();?>group/add" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('title');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('access_group');?></th>
                                <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($groups as $row) :
                                    if ($row->access_key != 'supper_admin'):
                            ?>
                                <tr class="gradeA odd" role="row">                                   
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->name;?></td>
                                    <td><?php echo lang($row->access_key);?></td>
                                    <td class="center">
                                        <a href="<?php echo base_url('group/edit').'/'.$row->id;?>" title="">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('group/del'); ?>', <?php echo $row->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php 
                                endif;
                            endforeach;?>
                        </tbody>
                    </table>
                    <div class="paging">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
