<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('add_new_group');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name"><?php echo lang('title');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="name" value="<?php echo set_value('name'); ?>" placeholder="">
                            <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="access_key"><?php echo lang('access_group');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="access_key" name="access_key">
                                <option value="">--<?php echo lang('choose_access');?>--</option>
                                <?php 
                                    $session = $this->session->userdata;
                                    $accessKey = $session['user']->access_key; 
                                    $accessGroup = $this->config->item('accessGroup'); 
                                ?>
                                <?php 
                                    foreach ($accessGroup as $value) :
                                        if ($accessKey == 'supper_admin' || ($accessKey != 'supper_admin' && $value != 'supper_admin')):
                                ?>
                                    <option value="<?php echo $value; ?>" <?php echo set_select('access_key', $value); ?>><?php echo lang($value); ?></option>
                                <?php 
                                    endif;
                                endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group" id="permission">
                        <label class="control-label col-sm-2"><?php echo lang('permission');?>:</label>
                        <?php $methodPerName = $this->config->item('methodPer'); ?>
                        <?php foreach( $modules as $key => $value ) : ?>
                        <div class="<?php if($key != 0) echo 'col-sm-offset-2'; ?> col-sm-8">
                            <?php
                            if( !empty( $value->method ) ) : ?>
                                <label class="parent"><?php echo $value->name; ?></label>
                                <?php foreach ($value->method as $item) : ?>
                                    <label class="child"><input type="checkbox" value="<?php echo $item; ?>" name="permission[<?php echo $value->controller; ?>][]" <?php echo set_checkbox('permission['.$value->controller.'][]', $item); ?> id="<?php echo  $value->controller.'-'.$item; ?>" class="per-child per-child-<?php echo $key; ?>"><?php if(isset($methodPerName[$item])) echo lang($methodPerName[$item]); ?></label>
                                <?php endforeach; 
                            endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>