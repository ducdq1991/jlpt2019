<?php defined('BASEPATH') or exit('No direct script access allowed');

class Group_model extends Bigshare_Model
{
    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
            $this->_table = 'user_groups';
    }
    
    public function insertGroup( $args = array() )
    {
        $this->_data = $args;
        if($id = $this->addRecord() ){
            return $id;
        }
        return false;
    }
    
    public function getGroups( $where = array(), $limit = null, $offset = null )
    {
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    public function deleteGroup( $where = array(), $where_not = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_where_not = $where_not;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
    
    
    public function getGroup( $where = array() )
    {
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    
    public function updateGroup( $where = array(), $args = array() )
    {
        $this->_table = 'user_groups';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
}