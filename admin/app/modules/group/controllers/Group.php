<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Group
* @desc Manage group for user (member)
* @author ChienDau
* @copyright Bigshare
* 
*/

class Group extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('group_model');
        $this->lang->load('group', $this->session->userdata('lang'));
        $this->load->model('module/module_model');
	}
    
    public function index()
    {
        $data = $where = array(); 
        $groups = $this->group_model->getGroups( $where );
        $data['groups'] = $groups;
        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Nhóm thành viên', base_url().'group');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 

        $this->load->view('layouts/header', $head);
        $this->load->view('group/index', $data);
        $this->load->view('layouts/footer');
    }
    
    public function add()
    {
        $data = array();
        $this->load->library('form_validation');
        
        $modules = $this->module_model->getModules( array('status' => (int) 1), $limit=0, $offset=0 );

        $admin = $this->session->userdata('user');
        if ($admin->access_key != 'supper_admin') {
            $permissions = unserialize($admin->permission);
            $controllerArgs = [];
            foreach ($permissions as $controller => $item){
                $controllerArgs[] = $controller;
            }


            foreach($modules as $key => $module) {
                if (in_array($module->controller, $controllerArgs)) {
                    $module->method = unserialize( $module->method );
                } else {
                    unset($modules[$key]);
                    foreach ($module as $item => $value) {
                        unset($module->$item);
                    }                
                }            
            }
        } else {
            foreach($modules as $key => $module) {
                $module->method = unserialize( $module->method );          
            }            
        }

        /*foreach( $modules as $module ) {
            $module->method = unserialize( $module->method );
        }*/

        $data['modules'] = $modules;
        $methodPer = $this->config->item('methodPer');
        $data['methodPer'] = $methodPer;
        $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[2]|is_unique[user_groups.name]',
                'errors' => array(
                    'required' =>  lang('mes_required'),
                    'min_length' => lang('mes_min_length'),
                    'is_unique' => lang('mes_has_registered'),
                ),
            ),
            array(
                'field' => 'access_key',
                'label' => lang('access_key'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  lang('mes_required')
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Nhóm thành viên', base_url().'group');
            $this->breadcrumbs->add(lang('add'), base_url().'group/add');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('group/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $info['permission'] = serialize($info['permission']);
            $query = $this->group_model->insertGroup( $info );
            if( $query ) {
                $message = sprintf(lang('message_create_success'), lang('the_group'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), lang('the_group'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url('group'));
        }
    }
    /**
     * @name Del
     * @desc delete a Group from user_groups table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');

        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_group'));
            $this->session->set_flashdata('warning', $message);
            echo 1;
            return false;
        }
        if( is_array($ids) ){
            $where = array(
                'field' => 'id',
                'value' => $ids
            );
            $query = $this->group_model->deleteGroup( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_group'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_group'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $where = array('id' => (int) $ids);
            $group = $this->group_model->getGroup( $where );
            if( $group->access_key == 'supper_admin' ) {
                $message = sprintf(lang('mes_not_delete'), lang('supper_admin'));
                $this->session->set_flashdata('warning', $message);
                echo 1;
                die;
            }
            $query = $this->group_model->deleteGroup( $where );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_group'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_group'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    /*
    * [Edit]
    * update a group into user_groups table
    */
    public function edit( $id )
    {
        $where = array('id' => (int) $id);
        $group = $this->group_model->getGroup( $where );
        if(empty($group)) {
            $message = sprintf(lang('message_delete_warning'), lang('the_group'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url('group'));
            return false;
        }
        $admin = $this->session->userdata('user');
        if( $group->access_key == 'supper_admin' && $admin->access_key != 'supper_admin') {
            $message = sprintf(lang('mes_not_update'), lang('supper_admin') );
            $this->session->set_flashdata('warning', $message);
            redirect(base_url('group'));
            return false;
        }
        $data = array();
        $this->load->library('form_validation');
        $modules = $this->module_model->getModules( array('status' => (int) 1) );

        $admin = $this->session->userdata('user');
        if ($admin->access_key != 'supper_admin') {
            $permissions = unserialize($admin->permission);
            $controllerArgs = [];
            foreach ($permissions as $controller => $item){
                $controllerArgs[] = $controller;
            }


            foreach($modules as $key => $module) {
                if (in_array($module->controller, $controllerArgs)) {
                    $module->method = unserialize( $module->method );
                } else {
                    unset($modules[$key]);
                    foreach ($module as $item => $value) {
                        unset($module->$item);
                    }                
                }            
            }
        } else {
            foreach($modules as $key => $module) {
                $module->method = unserialize( $module->method );          
            }            
        }

        $group->permission = unserialize($group->permission);
        $data['modules'] = $modules;
        $data['group'] = $group;
        $methodPer = $this->config->item('methodPer');
        $data['methodPer'] = $methodPer;
        $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[2]|is_unique[user_groups.name]',
                'errors' => array(
                    'required' =>  lang('mes_required'),
                    'min_length' => lang('mes_min_length'),
                    'is_unique' => lang('mes_has_registered'),
                ),
            )
        );
        $title = $this->input->post('name');
        if( $title == $group->name) {
           $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  lang('mes_required'),
                ),
            )
        );
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Nhóm thành viên', base_url().'group');
            $this->breadcrumbs->add(lang('edit'), base_url().'group/edit');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('group/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $info['permission'] = serialize($info['permission']);
            $where = array( 'id' => (int) $id );
            $query = $this->group_model->updateGroup( $where, $info );
            if( $query ) {
                $message = sprintf(lang('message_update_success'), lang('the_group'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_group'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url('group'));
        }
    }
}