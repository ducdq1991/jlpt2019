<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> <?php echo lang('mark_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="sendMultiMes btn btn-info btn-sm" url="<?php echo base_url().'mark/sendMultiMessage'; ?>">
                                <i class="fa fa-bell-o" aria-hidden="true"></i> Gửi thông báo</a>
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'mark/del'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                            </div>
                        </div>                    
                    </div>
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                         <div class="col-md-2 text-right">
                            <input type="text" class="form-control input-sm sys-filler" placeholder="Nhập mã đề" name="exam_id" value="<?php echo $exam_id;?>">
                        </div>
                        <div class="col-md-2">
                            <!--<select name="user" class="form-control input-sm sys-filler">
                                <option value="">Người gửi</option>
                                <?php //foreach($userList as $key => $item) :?>
                                <option value="<?php //echo $key; ?>" <?php //if($key == $user):?> selected = "selected" <?php //endif;?>><?php //echo $item; ?></option>
                                <?php //endforeach; ?>
                            </select>-->
                            <input type="text" class="form-control input-sm sys-filler" placeholder="Nhập tên học sinh" name="username" value="<?php echo $username;?>">                                                     
                        </div>                        
                        <div class="col-md-2">
                            <select name="attribute" class="form-control input-sm sys-filler">
                                <option value="">Loại</option>
                                <?php $attributeExam = $this->config->item('attributeExam'); ?>
                                <?php foreach($attributeExam as $key => $item) :?>
                                <option value="<?php echo $key; ?>" <?php if($key == $attribute):?> selected = "selected" <?php endif;?>><?php echo lang($item); ?></option>
                                <?php endforeach; ?>
                            </select>                           
                        </div>
                        <div class="col-md-2">
                            <div class='input-group date' id='datetimepicker-mark'>
                                <input type='text' name="date" value="<?php echo $date;?>" class="form-control input-sm sys-filler" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </span>
                            </div>                            
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                        </div>
                    </div>                                            
                </form>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="text-center">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th class="text-center">ID</th>
                                <th class="text-center"><?php echo lang('username');?></th>
                                <th class="text-center"><?php echo lang('exam_title');?></th>
                                <th class="text-center"><?php echo lang('attribute');?></th>
                                <th class="text-center"><?php echo lang('answer');?></th>
                                <th class="text-center"><?php echo lang('date');?></th>
                                <th class="text-center"><?php echo lang('time');?></th>
                                <th width="8%" class="text-center"><?php echo lang('score');?></th>
                                <th class="text-center"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!is_null($userExam)) :?>
                            <?php foreach ($userExam as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="text-center"><?php echo $row->id;?></td>
                                    <td><strong><?php echo $row->username; if(!empty($row->full_name)) echo '('.$row->full_name.')';?></strong></td>
                                    <td><strong><?php echo $row->title;?></strong></td>
                                    <td>
                                        <?php 
                                            $attributeExam = $this->config->item('attributeExam');
                                            if(isset($attributeExam[$row->attribute])) {
                                                echo lang($attributeExam[$row->attribute]);
                                            } else {
                                                echo 'N/A';
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $answer = json_encode($row->answer);
                                            if ($row->attribute == 1) {
                                                $result = json_encode($row->result);
                                            }
                                            $name = !empty($row->full_name) ? $row->username.'('.$row->full_name.')' : $row->username;
                                        ?>
                                        <?php if($row->attribute == 1) :?>
                                            <button type="button" class="btn btn-xs btn-white previewArr" data-author="<?php echo $name; ?>" data-answer='<?php echo $answer; ?>' data-result='<?php echo $result;?>' data-toggle="modal" data-target="#previewArrModal"><?php echo lang('preview_answer');?></button>
                                        <?php else : ?>
                                            <button type="button" class="btn btn-xs btn-white previewImg" data-title="<?php echo 'Bài thi tự luận - '.$name; ?>" data-url='<?php echo $answer; ?>' data-toggle="modal" data-target="#previewImgModal"><?php echo lang('preview_answer');?></button>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php echo date('d/m/Y', strtotime($row->created)); ?>
                                    </td>
                                    <td>
                                        <?php echo date('H:i', strtotime($row->created)); ?>
                                    </td>
                                    <td><input type="number" name="score" max="10" value="<?php echo $row->score;?>" data-id="<?php echo $row->id;?>" data-url="<?php echo base_url('mark/score');?>" class="score <?php if(!isset($row->score)) echo 'unmark'; ?>"></input></td>
                                    
                                    <td class="center">
                                        <a class="send-message" href="#commentModal" data-id="<?php echo $row->id;?>" data-exam="<?php echo $row->exam_id;?>" data-user="<?php echo $row->user_id;?>" data-toggle="modal" title="Gửi thông báo">
                                            <button class="btn btn-xs btn-info" type="button">
                                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('mark/del'); ?>', <?php echo $row->id; ?>)" title="Xóa bài thi">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="form-group">
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                    </div>                    
                        <div class="col-md-3 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div> 
                    </div>                    
                    <div class="paging">
                        <?php echo $paging;?>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="sendMultiMes btn btn-info btn-sm" url="<?php echo base_url().'mark/sendMultiMessage'; ?>">
                                <i class="fa fa-bell-o" aria-hidden="true"></i> Gửi thông báo</a>
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'mark/del'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                            </div>
                        </div>                    
                    </div>                                                      
                </div>
            </div>
        </div>
    </div>
</div>
<div id="previewImgModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content fileFramePreview">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title fileHeader">Bài làm tự luận</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div id="previewArrModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content arrFramePreview">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title arrHeader">Phần trả lời trắc nghiệm - </h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<div id="commentModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="comment-author-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gửi thông báo</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="label-control" for="comment">Nhận xét:</label>
                        <textarea name="comment" id="comment" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-send-message" class="btn btn-info">Gửi</button>
                    <button type="button" id="btn-close-message" class="btn btn-white" data-dismiss="modal">Đóng</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var baseUrl = "<?php echo $baseUrl;?>";
</script>
