<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Notification
* @author ChienDau
* @copyright Bigshare .,JSC
* 
*/

class Mark extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('mark_model');
        $this->lang->load('mark', $this->session->userdata('lang'));
        $this->load->model('user/user_model');
        $this->load->model('setting/setting_model');        
	}

	public function index($page = 0)
	{
		$data = $where = $likes = $order = array();
		$data['baseUrl'] = $this->getBaseUrl();
       	$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        $user = $this->input->get('user');
        $attribute = $this->input->get('attribute');
        $date = $this->input->get('date');
        $exam_id = $this->input->get('exam_id');
        $username = $this->input->get('username');
        $data['date'] = $date;
        $data['keyword'] = $keyword;
        $data['user'] = $user;
        $data['username'] = $username;
        $data['attribute'] = $attribute;
        $data['exam_id'] = $exam_id;
        $where['user_exam.exam_id !='] = null; 
        if(!empty($exam_id)) {
            $where['user_exam.exam_id'] = (int) $exam_id;
        }
        if(!empty($keyword)){
            $likes['field'] = 'exam.title';
            $likes['value'] = $keyword;
        } 
        if(!empty($username)){
            $likes['field'] = 'users.full_name';
            $likes['value'] = $username;
        }            
        if($date != '') {
            $date = date('Y-m-d H:i:s', strtotime($date));
            $where['user_exam.created >='] = $date;
            $where['user_exam.created <'] = date('Y-m-d H:i:s', (strtotime($date) + 24*60*60) );
        }
		if($user != ''){
			$where['user_exam.user_id'] = (int) $user;
		}
        if($attribute != ''){
            $where['user_exam.attribute'] = (int) $attribute;
        }
		$order = [
			'user_exam.created' => 'DESC',
			'user_exam.exam_id'	=> 'DESC'
		];
        $total = $this->mark_model->totalUserExam( $where, $likes ); 
		$userExam = $this->mark_model->getUserExam( $where, $limit, $offset, $likes, $order, $username);
        $data['userExam'] = $userExam;

        $showFrom = $offset + count($userExam);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];     
        $data['paging'] = paging(base_url() .'mark', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Bài thi', base_url().'mark');
        $head['breadcrumb'] = $this->breadcrumbs->display();   

        $this->load->view('layouts/header', $head);
        $this->load->view('mark/index', $data);
        $this->load->view('layouts/footer');

	}
	/**
     * @name Del
     * @desc delete a Module from modules table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
        	$message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->mark_model->deleteUserExam( $where, true );
            if( $query ) {
            	$message = sprintf(lang('message_delete_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->mark_model->deleteUserExam( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    public function score()
    {
    	$data = array();
		$score = (float) $this->input->post('score');
		$id = (int) $this->input->post('id');
		if(isset($score) && isset($id)){
			$data['score'] = $score;
			if($this->mark_model->updateUserExam( $id, $data )) {
				echo 1; exit;
			}
		}		
    }
    public function sendMessage()
    {
        $baseUrl = $this->getBaseUrl();
        $sender = $this->session->userdata('user');
        $arriver = (int)$this->input->post('user_id');
        $exam_id = (int)$this->input->post('exam_id');
        $comment = $this->input->post('comment');
        $user_exam_id = (int) $this->input->post('user_exam_id');
        if(!isset($sender) && !isset($arriver) && !isset($exam_id) )
        {
            $message = sprintf('Không được phép');
            $this->session->set_flashdata('warning', $message);
            echo 1; die;
        }
        if(!empty($comment)) {
            $args = [
                'comment' => $comment
            ];
            if(!$this->mark_model->updateUserExam( $user_exam_id, $args )) {
                 $this->session->set_flashdata('error', 'Không thể lưu nhận xét');
            }
        }
        $where = [
            'user_id' => $sender->id,
            'data' => $exam_id,
            'assigner' => $arriver,
            'type' => RESULT_EXAM,
        ];
        if($this->mark_model->exitLog($where)) {
            $message = sprintf('Đã gửi thông báo tới thành viên này rồi');
            $this->session->set_flashdata('warning', $message);
            echo 1; die;
        }
        $exam = $this->mark_model->getExam($exam_id);
        $content = 'Hệ thống: <br/> Bài thi <a href="'.$baseUrl.'/thanh-vien/ket-qua-cuoc-thi-online/">'.$exam->title.'</a> đã có điểm và đáp án';
        if($this->generateLog( RESULT_EXAM, $sender->id, $exam_id, $content, $arriver ))
        {
            $message = sprintf('Thông báo đã được gửi đi');
            $this->session->set_flashdata('success', $message);
            $this->sendEmail($arriver, $exam);
        }
        echo 1;
        die;
    }
    public function sendMultiMessage()
    {
        $baseUrl = $this->getBaseUrl();
        $sender = $this->session->userdata('user');
        $ids = $this->input->post('ids');
        if(empty($sender) && empty($ids) )
        {
            $message = sprintf('Không được phép');
            $this->session->set_flashdata('warning', $message);
            echo 1; die;
        }
        $userExams = $this->mark_model->getUserExamByIds($ids);
        $d = 0; $r = 0; $e=0;
        foreach ($userExams as $key => $value) {
            $where = [
                'user_id' => $sender->id,
                'data' => $value->exam_id,
                'assigner' => $value->user_id,
                'type' => RESULT_EXAM,
            ];
            if(!$this->mark_model->exitLog($where)) {
                $content = 'Hệ thống: Bài thi <a href="'.$baseUrl.'/thanh-vien/ket-qua-cuoc-thi-online/">'.$value->title.'</a> đã có điểm và đáp án';
                if($this->generateLog( RESULT_EXAM, $sender->id, $value->exam_id, $content, $value->user_id ))
                {
                    $this->sendEmail($value->user_id, $value);
                    $d++;
                } else {
                    $e++;
                }
            } else {
                $r++;
            }
        }
        $text = 'Lỗi: không thể gửi được thông báo';
        if($d > 0) {
            $message = sprintf('Thông báo đã được gửi cho '.$d.' thành viên');
            $this->session->set_flashdata('success', $message);
        }
        if($r > 0) {
            $message = sprintf( $r.' thành viên đã được gửi thông báo trước đây');
            $this->session->set_flashdata('warning', $message);
        }
        if($e > 0) {
            $message = sprintf( 'Lỗi: '.$r.' thành viên không thể gửi thông báo');
            $this->session->set_flashdata('error', $message);
        }
        echo 1; die;
    }
    /**
     * @name sendEmail
     * send a mail to user
     * */
    public function sendEmail( $userId = 0, $exam )
    {
        $this->load->helper('file');
        $user = $this->user_model->getUserById( (int) $userId);
        $userName = isset($user->meta['full_name']) ? $user->meta['full_name'] : $user->username;
        $where = array(
            'lang_id' => $this->_langId,
            );
        $where_in = array(
            'key' => 'option_name',
            'value' => array(
                'protocol', 'smtp_host', 'smtp_user', 'smtp_pass', 'smtp_port', 'charset',  'mailtype',
                ),
            );
        $mailSetting = $this->setting_model->getOptions( $where, $where_in );
        $mailer = [];
        foreach ($mailSetting as $value) {
            $mailer[$value->option_name] = $value->option_value;
        }
        date_default_timezone_set('GMT');
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => $mailer['protocol'],
            'smtp_host' => $mailer['smtp_host'],
            'smtp_user' => $mailer['smtp_user'],
            'smtp_pass' => $mailer['smtp_pass'],
            'smtp_port' => (int) $mailer['smtp_port'],
            'charset' => $mailer['charset'],
            'mailtype'  => $mailer['mailtype'],
        ));
        $this->email->from($this->getEmail(), $this->getName());
        $this->email->to($user->email);     
        $this->email->subject('Thông báo kết quả thi trực tuyến tại website: '.$this->getBaseUrl());
        $this->email->set_newline("\r\n");
        $bodyData = [
            '{FULLNAME}' => '<span style="font-weight:bold">'.$userName.'</span>',
            '{DOMAIN}' => '<span style="font-weight:bold">'.$this->getBaseUrl().'</span>',
            '{EXAM}' => '<a href="'.$exam->slug.'" style="font-weight:bold">'.$exam->title.'</a>',
            '{DATE}' => '<span style="font-weight:bold">'.date('d-m-Y', strtotime($exam->publish_up)).'</span>',
            '{TIME}' => '<span style="font-weight:bold">'.date('H:i', strtotime($exam->publish_up)).'</span>',
        ];
        $path = $this->getBaseUrl().'/email/exam.txt';
        $emailTemplate = read_file($path);
        $message = $this->paramReplace($emailTemplate, $bodyData);
        $this->email->message($message);
        $this->email->send();
    }

    public function paramReplace($txt, $search = array())
    {
        foreach($search as $k=>$v)
        {
            $txt = str_replace($k,$v,$txt);
        }
        return $txt;
    } 
}