<?php defined('BASEPATH') or exit('No direct script access allowed');

class Mark_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'user_exam';
    }
    public function getUserExam( $where = [], $limit = 0, $offset = 0, $likes = [], $order = [] )
    {
        $this->_selectItem = 'user_exam.id, user_exam.user_id, user_exam.exam_id, user_exam.score, user_exam.time, user_exam.attribute, user_exam.answer, user_exam.created, exam.content_file, exam.title, users.username, exam.questions';
        $this->_from = 'user_exam';
        $joins = array();
        $joinExamDetail['table'] = 'exam';
        $joinExamDetail['condition'] = 'user_exam.exam_id = exam.id';
        $joinExamDetail['type'] = 'left';
        $joinUser['table'] = 'users';
        $joinUser['condition'] = 'user_exam.user_id = users.id';
        $joinUser['type'] = 'left';         
        $joins[] = $joinUser;
        $joins[] = $joinExamDetail;
        $this->_joins = $joins;
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        $this->_order = $order;
        $user_exam = $this->getListRecords();
        $this->resetQuery();
        if($user_exam) {
            foreach ($user_exam as $key => $value) {
                $user_exam[$key]->answer = @unserialize($value->answer);
                if ($value->attribute) {
                    $where = [
                        'user_id' => $value->user_id
                    ];
                    $queIds = $this->getQuestIds($where);
                    $result = $this->getGroupQues($queIds);
                    $user_exam[$key]->result = $result;
                }
                $fullName = $this->getFullName($value->user_id);
                if($fullName) {
                    $user_exam[$key]->full_name = $fullName;
                }
            }
            return $user_exam;
        }
        return null;
    }


    public function getQuestIds($where = [])
    {
        try {
            $this->_selectItem = 'question_id';
            $this->_from = 'question_users';
            $this->_wheres = $where;
            $this->_table = 'question_users';
            $ques = $this->getListRecords();
            $this->resetQuery();
            $result = [];
            foreach ($ques as $item) {
                $result[] = $item->question_id;
            }

            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    public function getGroupQues($ids = [])
    {
        $this->_selectItem = 'id, score, true_answer';
        $this->_batchConditionField = 'id';
        $this->_batchConditionValue = $ids;
        $this->_from = 'questions';
        $this->_limit = 0;
        $ques = $this->getListRecords();
        $this->resetQuery();
        if($ques) {
            $question = [];
            foreach ($ques as $key => $value) {
                $question[$value->id] = $value->true_answer;
            }
            return $question;
        }
        return [];
    }

    public function getFullName($id)
    {
        $where = [
            'meta_key' => 'full_name',
            'user_id' => (int) $id,
        ];
        $this->_wheres = $where;
        $this->_table = 'user_metas';
        $meta = $this->getOneRecord();
        if($meta) {
            return $meta->meta_value;
        }
        return null;
    }
    /**
	 * [totalNotifications]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalUserExam($where = [], $likes = [])
	{
        $joins = [];
        $joinExamDetail['table'] = 'exam';
        $joinExamDetail['condition'] = 'user_exam.exam_id = exam.id';
        $joinExamDetail['type'] = 'left';
        $joinUser['table'] = 'users';
        $joinUser['condition'] = 'user_exam.user_id = users.id';
        $joinUser['type'] = 'left';         
        $joins[] = $joinUser;
        $joins[] = $joinExamDetail;
        $this->_joins = $joins;
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
		$this->_wheres = $where;
		return $this->totalRecord();
	}
    /**
     * [delCourse delete One Course]
     * @param  [int] $courseId
     * @return [boolean]
     */
    public function deleteUserExam($where = array(), $multiple = false)
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
    public function updateUserExam( $id = 0, $args = [])
    {
        $this->_wheres = ['id' => $id];
        $this->_data = $args;
        $this->_table = 'user_exam';
        return $this->updateRecord();
    }
    public function getAllUser()
    {
        $this->_from = 'users';
        $this->_selectItem = 'users.id, users.username';
        $this->_wheres = ['user_groups.access_key=' => 'member'];
        $joinExamDetail['table'] = 'user_groups';
        $joinExamDetail['condition'] = 'users.group_id = user_groups.id';
        $joinExamDetail['type'] = 'left';
        $joins[] = $joinExamDetail;
        $this->_joins = $joins;
        $this->_limit = 0;
        $users = $this->getListRecords();
        $this->resetQuery();
        $listUser = [];
        if(!empty($users)) {
            foreach ($users as $value) {
                $fullName = $this->getFullName($value->id);
                $listUser[$value->id] = isset($fullName) ? $value->username.'('.$fullName.')' : $value->username;
            }
        }
        return $listUser;
    }
    public function getExam($id)
    {
        $this->_wheres = ['id' => (int) $id];
        $this->_table = 'exam';
        return $this->getOneRecord();
    }
    public function getUserExamByIds($ids)
    {
        $this->_selectItem = 'user_exam.id, user_exam.user_id, user_exam.exam_id, exam.title, exam.slug, exam.publish_up,';
        $this->_batchConditionField = 'user_exam.id';
        $this->_batchConditionValue = $ids;
        $this->_from = 'user_exam';
        $joinExamDetail['table'] = 'exam';
        $joinExamDetail['condition'] = 'user_exam.exam_id = exam.id';
        $joinExamDetail['type'] = 'left';
        $joins[] = $joinExamDetail;
        $this->_joins = $joins;
        return $this->getListRecords();
    }
    
}