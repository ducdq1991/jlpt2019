<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model'); 
	}

	public function index()
	{	
		$data = [];
		$orderBy = ['key' => 'user_logs.updated', 'value' => 'DESC'];
		$whereNotIn = [LOG_IN, LOG_OUT, PURCHASE_LESSON, RECHARGE, PAYMENT, PURCHASE_COURSE];
		$activities = $this->dashboard_model->getActivities( $whereNotIn, 10, $orderBy );
		$data['activities'] = $activities;
		//history payment
		$whereIn = [PURCHASE_LESSON, RECHARGE, PAYMENT, PURCHASE_COURSE];
		$paymentHistory = $this->dashboard_model->getPaymentHistory( $whereIn, 10, $orderBy );
		$data['paymentHistory'] = $paymentHistory;
		//total record course
		$tableCourse = 'courses';
		$where = [
			'status' => 1,
			'parent' => 0
		];
		$numRecordCourse = $this->dashboard_model->totalRecord( $where, $tableCourse );
		$data['numRecordCourse'] = (int) $numRecordCourse;
		//total record lesson
		$where = ['status' => 1];
		$tableLesson = 'lessons';
		$numRecordLesson = $this->dashboard_model->totalRecord( $where, $tableLesson );
		$data['numRecordLesson'] = (int) $numRecordLesson;


		//total record faq
		$tableFaq = 'questions';
		$numQuestion = $this->dashboard_model->totalRecord( $where, $tableFaq );
		$data['numQuestion'] = (int) $numQuestion;
		//total record exam_test
		$tableExamTest = 'exam';
		$where = [
			'attribute' => 1,
			'status'	=> 1
		];

		//total record exercise
		$where = [
			'type' => 'exercise',
			'status'	=> 1
		];
		$numRecordExercise = $this->dashboard_model->totalRecord( $where, $tableExamTest );
		$data['numRecordExercise'] = (int) $numRecordExercise;
		//load view
		$this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('dashboard'), base_url().'dashboard');
        $head['breadcrumb'] = $this->breadcrumbs->display();
		
		$this->load->view('layouts/header', $head);		
		$this->load->view('layouts/home', $data);
		$this->load->view('layouts/footer');
	}	

}