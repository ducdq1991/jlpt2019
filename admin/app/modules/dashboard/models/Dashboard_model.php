<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends Bigshare_Model {

	public function __construct()
    {
        parent::__construct();
    }
    /**
    * [get activities]
    **/
    public function getActivities( $whereNotIn = [], $limit = 0, $orderBy = [] )
    {
    	try {
    		$query = $this->db->select('content, updated')
    						->where_not_in('type', $whereNotIn)
    						->limit($limit)
    						->order_by( $orderBy['key'], $orderBy['value'] )
    						->get('user_logs');
    		if( $query ) {
    			return $query->result();
    		}				
    		return false;
    	} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		return null;
    }
    /**
    * [get history payment]
    **/
    public function getPaymentHistory( $whereIn = [], $limit = 0, $orderBy = [] )
    {
    	try {
    		$query = $this->db->select('content, created')
    						->where_in('type', $whereIn)
    						->limit($limit)
    						->order_by( $orderBy['key'], $orderBy['value'] )
    						->get('user_logs');
    		if( $query ) {
    			return $query->result();
    		}				
    		return false;
    	} catch( Exception $ex ){

			$this->writeLog($ex->getMessage());
		}
		return null;
    }
    /**
    * [total record number in module]
    **/
	public function totalRecord( $where = array(), $table = '' )
	{
		try{			
			return $this->db->where($where)->count_all_results($table);
		} catch( Exception $ex ){
			echo $ex->getMessage();
            exit;
		}
		return null;
	}
}