<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Module</h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Tiêu đề <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="name" value="<?php if(set_value('name')) echo set_value('name'); else echo $module->name; ?>" placeholder="">
                            <?php echo form_error('name', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="controller">Controller <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="controller" class="form-control" id="controller" value="<?php if(set_value('controller')) echo set_value('controller'); else echo $module->controller; ?>" placeholder="">
                            <?php echo form_error('controller', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="method">Method <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="method" class="form-control" id="method" value="<?php if(set_value('method')) echo set_value('method'); else echo $module->method; ?>" placeholder="E.g: view,add,edit,del">
                            <?php echo form_error('method', '<div class="validation-alert alert alert-danger">', '</div>'); ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="button" id="method-default" onclick="loadMethodDefault()" class="btn btn-sm btn-default">Mặc định</button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <label><input type="checkbox" class="" value="1" name="status" id="status" <?php if(set_checkbox('status', '1')) echo set_checkbox('status', '1'); else if($module->status == 1) echo 'checked=""'; ?>>Kích hoạt</label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info">Xác nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>