<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('add_new_module');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name"><?php echo lang('title');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" id="name" value="<?php echo set_value('name'); ?>" placeholder="">
                            <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="controller"><?php echo lang('controller');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="controller" class="form-control" id="controller" value="<?php echo set_value('controller'); ?>" placeholder="">
                            <?php echo form_error('controller', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="method"><?php echo lang('method');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="method" class="form-control" id="method" value="<?php echo set_value('method'); ?>" placeholder="E.g: view,add,edit,del">
                            <?php echo form_error('method', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="button" id="method-default" onclick="loadMethodDefault()" class="btn btn-sm btn-default"><?php echo lang('default');?></button>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="method"><?php echo lang('status');?>:</label>
                        <div class="col-sm-8">
                            <label><input type="checkbox" class="" value="1" name="status" id="status" <?php echo set_checkbox('status', '1'); ?>><?php echo lang('active');?></label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>