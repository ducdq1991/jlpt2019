<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Module
* @author ChienDau
* @copyright Bigshare
* 
*/

class Module extends Bigshare_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('module_model');
        $this->lang->load('module', $this->session->userdata('lang'));
    }
    
    /**
     * @name Index
     * @desc show all of Module has registed
     * */
    public function index( $page = 0 )
    {
        $data = $where = $likes = array();
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));
        
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['modules.status'] = ($status ==='active') ? 1 : 0;
        }
        if(!empty($keyword)){
            $likes['field'] = 'modules.name';
            $likes['value'] = $keyword;
        }  
        $numRecords = $this->module_model->totalModules( $where );      
        $data['paging'] = paging(base_url() .'module', $page, $numRecords, $limit); 
        $moduleQuery = $this->module_model->getModules( $where, $limit, $offset, $likes );
        $modules = array();
        foreach( $moduleQuery as $item ) {
            $item->method = unserialize( $item->method );
            $modules[] = $item;
        }
        $data['modules'] = $modules;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');
        $this->load->view('layouts/header');
        $this->load->view('module/index', $data);
        $this->load->view('layouts/footer');
    }
    
    /**
     * @name Add
     * @desc register a new Module
     * */
    public function add()
    {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'controller',
                'label' => lang('controller'),
                'rules' => 'trim|required|is_unique[modules.controller]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_has_registered'),
                ),
            ),
            array(
                'field' => 'method',
                'label' => lang('method'),
                'rules' => 'trim|required|callback_checkingMethod',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('module/add');
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $methods = strtolower($info['method']);
            $methodsExp = explode(',', $methods);
            $methodsExp = array_filter( $methodsExp );
            $info['method'] = serialize( $methodsExp );
            if( $this->module_model->insertModule( $info ) ) {
                $message = sprintf(lang('message_create_success'), lang('the_module'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), lang('the_module'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'module');
        }
    }
    /**
     * @name checkingMethod
     * @desc preg match method field in form add new module
     * */
    public function checkingMethod( $str )
    {
        $partten = "/^[A-Za-z\,]+$/";
        if(!preg_match($partten , $str)) {
            $this->form_validation->set_message('checkingMethod', '{field} chỉ cho phép chữ cái và dấu ","');
            return true;
        }
        return true;
    }
    
    public function edit( $id )
    {
        if(!$id) {
            return false;
        }
        $data = array();
        $module = $this->module_model->getModule( array('id' => (int) $id) );
        $module->method = unserialize($module->method);
        $module->method = implode(',',$module->method);
        $data['module'] = $module;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'controller',
                'label' => lang('controller'),
                'rules' => 'trim|required|is_unique[modules.controller]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_has_registered'),
                ),
            ),
            array(
                'field' => 'method',
                'label' => lang('method'),
                'rules' => 'trim|required|callback_checkingMethod',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $controller = $this->input->post('controller');
        if( $controller == $module->controller ) {
            unset($config[1]);
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('module/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $methods = strtolower($info['method']);
            $methodsExp = explode(',', $methods);
            $methodsExp = array_filter( $methodsExp );
            $info['method'] = serialize( $methodsExp );
            if( $this->module_model->updateModule( array('id' => (int) $id), $info ) ) {
                $message = sprintf(lang('message_update_success'), lang('the_module'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_module'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'module');
        }
    }
    /**
     * @name Del
     * @desc delete a Module from modules table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('module'));
            $this->session->set_flashdata('warning', $message);
            echo 1;
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->module_model->deleteModule( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('module'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('module'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->module_model->deleteModule( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('module'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('module'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    
    /**
     * @name changeStatus
     * @desc change status a Module from modules table
     * */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->module_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('module'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('module'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->module_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('module'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('module'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }
}