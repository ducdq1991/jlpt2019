<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_table = 'modules';
    }
    
    /**
     * @name insertModule
     * @desc insert new module in modules table
     * @param [array] $args
     * */
    public function insertModule( $args = array() )
    {
        $this->_data = $args;
		if($id = $this->addRecord() ){
			return $id;
		}
		return false;
    }
    
    /**
     * @name getModules
     * @desc get all row from modules table
     * @param [array] $where
     * */
    public function getModules( $where = array(), $limit = 0, $offset = 0, $likes = array() )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
	 * [totalModules]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalModules($where = array())
	{
		$this->_wheres = $where;
		return $this->totalRecord();
	}
    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    /**
     * @name deleteModule
     * @desc delete a row from modules table
     * @param [array] $where
     * */
    public function deleteModule( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
    
    
    public function getModule( $where = array() )
    {
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    
    public function updateModule( $where = array(), $args = array() )
    {
        $this->_table = 'modules';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
}