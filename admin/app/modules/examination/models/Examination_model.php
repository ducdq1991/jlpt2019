<?php defined('BASEPATH') or exit('No direct script access allowed');

class Examination_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_table = 'examinations';
    }
    /**
     * @name getExaminations
     * @desc get all row from examination table
     * @param [array] $where
     * */
    public function getExaminations( $where = array(), $limit = 0, $offset = 0, $likes = array() )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
	 * [total]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function total($where = array())
	{
		$this->_wheres = $where;
		return $this->totalRecord();
	}
	/**
     * [delCourse delete a/any Exam]
     * @param  [int] $courseId
     * @return [boolean]
     */
    public function deleteExamination( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
}