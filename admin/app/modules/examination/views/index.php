<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> <?php echo lang('examination_module');?></h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-left btn-function" style="display:none;">
                        <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'examination/del'; ?>">
                        <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                    </div>    
                    <div class="pull-right form-module-action">
                        <?php echo lang('num_records');?>:
                        <select name="numRecord" class="form-control input-sm sys-filler">                                
                            <option value="">--<?php echo lang('rows');?>--</option>
                            <?php foreach($itemPerPages as $item):?>
                            <option value="<?php echo $item;?>" <?php if($numRecord == $item):?> selected="selected" <?php endif;?>><?php echo $item;?></option>
                            <?php endforeach;?>
                        </select>                      
                        <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('full_name');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('email');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('phone');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('answer');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('created');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php foreach ($examinations as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->full_name;?></td>
                                    <td><?php echo $row->email;?></td>
                                    <td><?php echo $row->phone;?></td>
                                    <td>
                                        <?php 
                                            $answer = unserialize($row->answer_file);
                                            $answer = json_encode($answer);
                                            
                                            $name = !empty($row->full_name) ? $row->full_name : '';
                                        ?>
                                        <?php if(is_null($answer)) :
                                            echo lang('not_exist');
                                        else : ?>
                                            <button type="button" class="btn btn-xs btn-white previewImg" data-title="Bài làm khảo sát năng lực - <?php echo $name;?>" data-url='<?php echo $answer; ?>' data-toggle="modal" data-target="#previewImgModal"><?php echo lang('preview_answer');?></button>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo date('d/m/Y H:i', strtotime($row->created)); ?></td>
                                    <td class="center text-center">
                                        <a onclick="delSingle('<?php echo base_url('examination/del'); ?>', <?php echo $row->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>                            
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    <div class="paging">
                        <?php echo $paging;?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="previewImgModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content fileFramePreview">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title fileHeader"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var baseUrl = "<?php echo $baseUrl;?>";
</script>