<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Examination
* @author ChienDau
* @copyright Bigshare
* 
*/

class Examination extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('examination_model');
        $this->lang->load('examination', $this->session->userdata('lang'));
	}
    /**
	* index
	* Default page for examination package, list all record of examination table
	*/
	public function index( $page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = array();
        $data['baseUrl'] = $this->getBaseUrl();
		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
		//Get Filters
		$keyword = $this->input->get('keyword');
		// Set Order Condition
		if( isset($orderOption) && ( !empty($orderOption) || $orderOption !='') ) {
			$orderCondition[$orderOption] = $orderBy;
		}
		// Set Likes Condition
		if($keyword !=''){
			$likes['field'] = 'full name';
			$likes['value'] = (string) trim($keyword);
		}
		// Set actived Fillter
		$data['keyword'] = $keyword;
		$data['numRecord'] = $limit;
		$data['itemPerPages'] = $this->config->item('itemPerPages');
		$total = $this->examination_model->total($where);
		$examinations = $this->examination_model->getExaminations( $where, $limit, $offset, $orderCondition, $likes );
		$data['examinations'] = $examinations;
		$data['paging'] = paging(base_url() .'exam', $page, $total, $limit);

        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Khảo sát năng lực học viên', base_url().'examination');
        $head['breadcrumb'] = $this->breadcrumbs->display();   

		$this->load->view('layouts/header', $head);
        $this->load->view('examination/index', $data);
        $this->load->view('layouts/footer');
	}
	/**
     * @name delete
     * Delete a exam or any exam table by id
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'exam');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->examination_model->deleteExamination( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('examination'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('examination'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->examination_model->deleteExamination( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('examination'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('examination'));
                $this->session->set_flashdata('error', $message);
            }
            echo 2;
            die;
        }
    }
}