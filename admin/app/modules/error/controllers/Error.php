<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Error 404
* @author ChienDau
* @copyright Bigshare
* 
*/

class Error extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$indexUrl = $this->getBaseUrl();
		$data['indexUrl'] = $indexUrl;
		$this->load->view('layouts/header');
        $this->load->view('error/index', $data);
        $this->load->view('layouts/footer');
	}
	public function memberError()
	{
		$this->session->unset_userdata('user');
        $this->session->sess_destroy();
		redirect($this->getBaseUrl());
	}
}