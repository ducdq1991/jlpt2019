<div class="row">
    <div class="col-lg-12">
    	<h3>403 Forbidden</h3>
    	<p><?php echo lang('message_access_error');?></p>
    	<?php if( $this->session->has_userdata('redirect') ) :?>
        <a href="<?php echo $this->session->userdata('redirect'); ?>" class="btn btn-white">Quay lại</a>
        <?php endif; ?>
    	<a href="<?php echo $indexUrl; ?>" class="btn btn-info">Trang chủ</a>
    	
    </div>
</div>