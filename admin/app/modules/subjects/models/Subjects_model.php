<?php defined('BASEPATH') or exit('No direct script access allowed');

class Subjects_model extends Bigshare_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->_table = 'subjects';
    }
    
    public function addSubject($args = []) 
    {
        $this->_data  =$args;
        return $this->addRecord();
    }
    
    
    public function updateSubject($where = [], $args = [])
    {
        $this->_table = 'subjects';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getSubjects($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {
        $this->_selectItem = '*';
        $this->_from = 'subjects';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;       
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }

    public function totalSubject($where = [])
    {
        $this->_wheres = $where;
        return $this->totalRecord();
    }


    public function deleteSubject($where = [], $multiple = false)
    {
        
        $this->_multiple = $multiple;
        $this->_wheres = $where;
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }

    public function changeStatus($ids, $args = [])
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'subject_id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('subject_id' => (int) $ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getSubject($where = [])
    {
        $this->_selectItem = '*';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
}