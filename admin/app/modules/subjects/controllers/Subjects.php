<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Subjects
* @author TrungDoan
* @copyright Bigshare
* 
*/

class Subjects extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('subjects_model');        
	}
    
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'subjects.name';
            $likes['value'] = $keyword;
        }
        $orderCondition['subject_id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if( $status != '' ){
          $where['subjects.status'] = $status;
        }
        $total = $this->subjects_model->totalSubject($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'subjects', $page, $total, $limit, $queryString);
        $subjects = $this->subjects_model->getSubjects($where, $limit, $offset, $orderCondition, $likes);
        $data['subjects'] = $subjects;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($subjects);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Môn học', base_url().'subjects');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
    }

    public function add()
    {
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên môn học',
                'rules' => 'trim|required|is_unique[subjects.name]',
                'errors' => array(
                    'required' => 'Tên môn học là bắt buộc.',
                    'is_unique' => 'Môn học này đã tồn tại.',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => 'Đường dẫn tĩnh',
                'rules' => 'trim|required|is_unique[subjects.slug]',
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Môn học', base_url().'subjects');
            $this->breadcrumbs->add('Thêm mới', base_url().'subjects/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('add', $data);
            $this->load->view('layouts/footer');
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);            
            $subjectId = $this->subjects_model->addSubject($postData);
            if($subjectId) {
                $message = 'Thêm môn học thành công!';
                $messageType = 'success';                
            } else {  
                $message = 'Lỗi! Không thêm được môn học';                
            }
            $this->session->set_flashdata($messageType, $message);

            redirect(base_url() . 'subjects');
        }
    }


    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if(empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'subjects');
            return false;
        }
        $where = array(
            'field' => 'subject_id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->subjects_model->deleteSubject($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa môn học thành công.');
            } else {
                $this->session->set_flashdata('error', 'Không xóa được môn học');
            }
            echo 1;
            die;
        } else {
            $query = $this->subjects_model->deleteSubject( array( 'subject_id' => (int) $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa môn học thành công.');
            } else {                
                $this->session->set_flashdata('error', 'Không xóa được môn học');
            }
            if( $id ) {
                redirect(base_url() . 'subjects');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'subjects.subject_id' => (int) $id,
            );
        $subject = $this->subjects_model->getSubject($where);        
        if (empty($subject)) {            
            $this->session->set_flashdata('error', 'Môn học không tồn tại');
            redirect( base_url() . 'subjects');
        }
        $postData = $this->input->post();

        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $data['subject'] = $subject;
        $this->load->library('form_validation');
        $validateName = $validateSlug = '';
        if (isset($postData['name'])) {
            if ($subject->name != $postData['name']) {
                $validateName = '|is_unique[subjects.name]';
            }
        }

        if (isset($postData['slug'])) {
            if ($subject->slug != $postData['slug']) {
                $validateSlug = '|is_unique[subjects.slug]';
            }
        }

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên môn học',
                'rules' => 'trim|required' . $validateName,
                'errors' => array(
                    'required' => 'Tên môn học là bắt buộc.',
                    'is_unique' => 'Môn học này đã tồn tại',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required' . $validateSlug,
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Môn học', base_url().'subjects');
            $this->breadcrumbs->add($subject->name, base_url().'subjects/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('edit', $data);
            $this->load->view('layouts/footer');
        } else {                        
            if( $this->subjects_model->updateSubject($where, $postData ) ) {
                $this->session->set_flashdata('success', 'Đã cập nhật môn học.');
            } else {                    
                $this->session->set_flashdata('error', 'Không cập nhật được môn học.');
            }

            redirect(base_url() . 'subjects/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->subjects_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
}