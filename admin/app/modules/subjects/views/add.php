<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> Thêm mới môn học</h5>
            </div>
            <div class="ibox-content">
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-article-slug form-control" id="title" value="<?php echo set_value('name'); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('alias'); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="slug" class="slug slug-article form-control" id="slug" value="<?php echo set_value('slug'); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"></label>
                            <div class="col-sm-10">
                            <button type="button" id="article-submit" class="btn btn-sm btn-info">Lưu</button>
                            </div>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>