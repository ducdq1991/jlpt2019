<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Monitoring extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        /*$logged_in = $this->session->userdata('user');
        if(!$logged_in){
            header("location: ".base_url());
        }*/
    }
    
    public function index() {

        $this->load->view('layouts/header');
        $this->load->view('monitoring/index');
        $this->load->view('layouts/footer');
    }
    
    public function get_data_monitoring() {
        $this->load->model('monitoring_model');
        $result = $this->monitoring_model->get_data_monitoring();
        return $result;
    }
    
    public function logout_user() {
        $this->load->model('monitoring/queries_model');
        $id = $this->input->post('id');
        $result = $this->queries_model->logout_user($id);
        if ($result) {
            echo 1;
            exit;
        }
        echo 2;
        exit;
    }
}