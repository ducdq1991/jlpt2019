<?php
class Monitoring_model extends CI_Model{
    //put your code here
    
    public function get_data_monitoring() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        error_reporting(-1);
        $data = array(
            'id',
            'ip_address',
            'timestamp',
            'data'
        );
        $this->db->select($data);
        $today = time();
        $yesterday = time() - 6*60*60;
        $where = [
            'timestamp >=' => $yesterday,
            'timestamp <= ' => time()
        ];
        //$this->db->group_by('ip_address'); 
        $this->db->order_by('timestamp', 'desc');
        //$this->db->order_by('ip_address', 'desc');

        $this->db->where($where);
        $query = $this->db->get("ci_sessions");
        $no = 1;
        foreach ($query->result() as $row)
        {   
            $session_data = $row->data;
            $return_data = array();
            $offset = 0;
            while ($offset < strlen($session_data)) {
                if (!strstr(substr($session_data, $offset), "|")) {
                    throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
                }
                $pos = strpos($session_data, "|", $offset);
                $num = $pos - $offset;
                $varname = substr($session_data, $offset, $num);
                $offset += $num + 1;
                $data = unserialize(substr($session_data, $offset));
                $return_data[$varname] = $data;
                $offset += strlen(serialize($data));
            }
            $args = [];
            if(!empty($return_data['web_user'])){
                $accessKey = $return_data['web_user']->access_key;
                if ($accessKey == 'member') {
                    $username = $return_data['web_user']->username;
                    $ip = $row->ip_address;
                    $fullname = $return_data['web_user']->full_name;
                    $timeGenerate = $return_data['__ci_last_regenerate'];

                    $html = '';
                    $html .= "<tr>";
                    $html .= "<td style=\"width: 10%;text-align: center\"><div id=\"dv_ip_$no\">" . date("d/m/Y H:i:s", $timeGenerate) . "</div></td>";
                    $html .= "<td style=\"width: 20%;text-align: left\">" . $username . "</td>";
                    $html .= "<td style=\"width: 20%;text-align: left\">" . $fullname . "</td>";
                    $html .= "<td style=\"width: 11%;text-align: center\"><div id=\"dv_$no\"><strong>" . $ip . "<strong></div></td>";
                    $html .= "<td style=\"width: 7%;text-align: center\">"
                        . " <button class=\"btn btn-sm btn-danger\" onclick=\"logout_user('" . $row->id . "','" . $username . "');\">Đăng xuất</button></td>";
                    $html .= "</tr>";
                    echo $html;
                    $no++;
                }
            }
        }
    }
}