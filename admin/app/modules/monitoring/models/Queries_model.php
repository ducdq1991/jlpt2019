<?php
class Queries_model extends CI_Model {
    //put your code here
    function __construct(){
        parent::__construct();
    }    
    
    function logout_user($id = '') {
    	if ($id != '') {
    		$this->db->delete('ci_sessions', array('id' => $id));
    		return true;	
    	}
        
        return false;        
    }
}