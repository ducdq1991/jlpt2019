<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-area-chart "></i> Người dùng Online (6 giờ gần nhất)<span class="numUser" style="color:red"></span></h5>
            </div>
            <div class="ibox-content">
                 <div class="box">
                    <div class="box-body">    
                      <div class="checkbox">
                        <label>
                            <input type="checkbox" name="chkAutoRefresh" id="chkAutoRefresh" onclick="get_data_monitor();" /> Tự động làm mới
                        </label>
                      </div>
                      <table id="example1" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th class="text-center">Thời gian đăng nhập cuối cùng</th>
                            <th class="text-center">Tài khoản</th>
                            <th class="text-center">Họ tên</th>
                            <th class="text-center">Địa chỉ IP</th>
                            <!--<th>Trình duyệt</th>
                            <th>Platform</th>-->
                            <th class="text-center">Hành động</th>
                          </tr>
                        </thead>
                        <tbody id="rmonitoringuser">
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">  
    var chk_auto = "";
    var auto_refresh = setInterval(function(){
        if(chk_auto){                
            get_data_monitor();
        }
    }, 3000);
    get_data_monitor();

    function logout_user(id,username){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'monitoring/logout_user'; ?>",
            data: {"id":id},
            success: function(resp){
                    alert("Thành viên " + username + " sẽ bị block");
                    get_data_monitor();
            },
            error:function(event, textStatus, errorThrown) {
                alert('Có lỗi: '+ textStatus + ' , HTTP Error: '+errorThrown);
            },
            timeout: 4000
        });
    }

    function get_data_monitor(){    
        chk_auto = $("#chkAutoRefresh").is(':checked');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'monitoring/get_data_monitoring'; ?>",
            success: function(resp){
                    $("#rmonitoringuser").html(resp);
                    var liList = document.getElementById("rmonitoringuser").getElementsByTagName("tr");

                    var num = liList.length;     
                    $('span.numUser').html('(' + num + ')');              
            },
            error:function(event, textStatus, errorThrown) {
                $("#rmonitoringuser").html('Error Message: '+ textStatus + ' , HTTP Error: '+errorThrown);
            },
            timeout: 4000
        });
    }
</script> 