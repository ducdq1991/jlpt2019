<?php defined('BASEPATH') or exit('No direct script access allowed');

class Article_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'posts';
    }
    
    public function insertArticle( $args = array() ) 
    {
        $this->_data  =$args;
        return $this->addRecord();
    }
    
    public function insertArticleDetail( $args = array() )
    {
        $this->_table = 'post_details';
        $this->_data  =$args;
        if( $this->addRecord() )
            return true;
        return false;
    }

    public function addMeta($args)
    {
        $this->_table = 'post_metas';
        $this->_data  =$args;
        if( $this->addRecord() )
            return true;
        return false;
    }
    public function updateMeta($id, $args = [])
    {
        $where = [];
        $where['post_metas.post_id'] = (int) $id;
        $where['meta_key'] = $args['meta_key'];
        $this->_table = 'post_metas';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function deleteMeta($where = [], $multiple = false )
    {
        $this->_wheres = $where;
        $this->_table = 'post_metas';
        if($this->deleteRecord()){
            $this->resetQuery();
        }
        return false;
    }

    public function getPostMetas($postId)
    {
        $this->_selectItem = 'post_metas.meta_key, post_metas.meta_value';
        $where = ['post_id' => $postId];
        $this->_from = 'post_metas';
        $this->_wheres = $where;
        $result = $this->getListRecords();
        $this->resetQuery();
        return $result;
    }

    public function updateArticle( $id, $args = array() )
    {
        $where = array();
        $where['posts.id'] = (int) $id;
        $this->_table = 'posts';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function updateArticleDetails( $id, $args = array() )
    {
        $where = array();
        $where['post_details.post_id'] = (int) $id;
        $this->_table = 'post_details';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getArticles( $where = array(), $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {
        $this->_selectItem = 'posts.id, posts.hash_id, posts.type, posts.image, posts.category_id, posts.created, posts.created_by, posts.publish_up, posts.publish_down, posts.featured, posts.hit, posts.created_by, posts.status, post_details.title, post_details.content, post_details.description, post_details.slug, post_details.tags, post_details.lang_id, post_details.meta_keywords, post_details.meta_description, post_details.meta_title, category_details.name AS category';
        
        $this->_from = 'posts';
        $joins = array();
        $joinDetail['table'] = 'post_details';
        $joinDetail['condition'] = 'posts.id = post_details.post_id';
        $joinDetail['type'] = 'left';
        $joinCate['table'] = 'category_details';
        $joinCate['condition'] = 'posts.category_id = category_details.category_id';
        $joinCate['type'] = 'left';
        $joins[] = $joinDetail;
        $joins[] = $joinCate;
        $this->_joins = $joins;
        $this->_order = $orderBy;
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $queryArticle = $this->getListRecords();
        $this->resetQuery();
        $articles = array();
        foreach ($queryArticle as $value) {
            $author = $this->getAuthor( $value->created_by );
            $value->author = $author;
            $articles[] = $value;
        }
        return $queryArticle;
    }

    public function getAuthor( $id ) 
    {
        $where = array('user_id' => (int) $id, 'meta_key' => 'full_name');
        $this->_wheres = $where;
        $this->_table = 'user_metas';
        $author = $this->getOneRecord();
        if(!empty($author->meta_value))
            return $author->meta_value;
        return '';
    }

    public function totalArticles( $where = array() )
    {
        $joins = array();
        $joinDetail['table'] = 'post_details';
        $joinDetail['condition'] = 'posts.id = post_details.post_id';
        $joinDetail['type'] = 'left';
        $joinCate['table'] = 'category_details';
        $joinCate['condition'] = 'posts.category_id = category_details.category_id';
        $joinCate['type'] = 'left';
        $joins[] = $joinDetail;
        $joins[] = $joinCate;
        $this->_joins = $joins;
        $this->_wheres = $where;
        return $this->totalRecord();
    }

    /**
     * @name deleteActicle
     * @desc delete a article or some article has selected
     * @param [bool] $multiple: true for multi record, fale for single record
     * */
    public function deleteArticle( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        if($multiple == true){
            $postId = $where['value'];
        } else {
            $postId = $where['id'];
        }
        if($this->deleteRecord()){
            $this->resetQuery();
            return $this->deleteActicleDetails( $postId );
        }
        return false;
    }

    public function deleteActicleDetails( $id, $where = array() )
    {   
        $multiple = false;
        if(is_array($id)){           
            $where['field'] = 'post_id';
            $where['value'] = $id;
            $multiple = true;
        } else {
            $where['post_id'] = (int) $id; 
        }      
        $this->_wheres = $where;
        $this->_table = 'post_details';
        $this->_multiple = $multiple;
        return $this->deleteRecord();        
    }
    /**
     * @name changeStatus
     * @desc change status a row or any rows from posts table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    /**
     * [getArticleById]
     * @param  [int] $id
     * @return [object]
     */
    public function getArticle( $where = array())
    {
        $this->_selectItem = 'posts.id, posts.hash_id, posts.type, posts.image, posts.category_id, posts.created, posts.created_by, posts.publish_up, posts.publish_down, posts.featured, posts.hit, posts.status, post_details.title, post_details.content, post_details.description, post_details.slug, post_details.tags, post_details.lang_id, post_details.meta_keywords, post_details.meta_description, post_details.meta_title';
        $joins = array();
        $joinDetail['table'] = 'post_details';
        $joinDetail['condition'] = 'posts.id = post_details.post_id';
        $joinDetail['type'] = 'left';
        $joins[] = $joinDetail;
        $this->_joins = $joins;
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
}