<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Article
* @author ChienDau
* @copyright Bigshare
* 
*/

class Article extends Bigshare_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('article_model');
        $this->load->model('category/category_model');
        $this->lang->load('article', $this->session->userdata('lang'));
    }
    
    /**
    * Index, this is default page for article packet
    * Show all article
    */
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $where['post_details.lang_id'] = $this->_langId;
        $where['category_details.lang_id'] = $this->_langId;


        $categoryOption = $this->category_model->getCategoryOption();
        $data['categoryOption'] = $categoryOption;

        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }

        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'post_details.title';
            $likes['value'] = $keyword;
        }

        $status = $this->input->get('status');
        $data['status'] = $status;
        if (!empty($status)) {
          $where['posts.status'] = $status;
        }

        $categoryId = $this->input->get('category_id');
        if ($categoryId ) {
            $where['posts.category_id'] = $categoryId;
        }
        $data['category_id'] = $categoryId;

        $numRecords = $this->article_model->totalArticles($where);   
        
        $articles = $this->article_model->getArticles($where, $limit, $offset, $orderCondition, $likes);
        $data['article'] = $articles;

        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        $data['status'] = $status;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';  
        $data['itemPerPages'] = $this->config->item('rowPerPages');
        $data['sortByItems'] = $this->config->item('sortBy');

        $showFrom = $offset + count($articles);        
        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $numRecords;
        
        $queryString = $_SERVER['QUERY_STRING'];
        $data['paging'] = paging(base_url() .'article', $page, $numRecords, $limit, $queryString);                
        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Bài viết', base_url().'article');
        $headers['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $headers);
        $this->load->view('article/index', $data);
        $this->load->view('layouts/footer');
    }
    /**
    * Add
    * insert a new article into posts table
    **/
    public function add()
    {
        $data = [];
        $data['_baseUrl'] = $this->getBaseUrl();

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);

        $categoryOption = $this->category_model->getCategoryOption();

        $author = $this->session->userdata('user');

        $data['author'] = [
            'id' =>$author->id,
            'username' => $author->username,
        ];
        $data['categoryOption'] = $categoryOption;

        $this->load->library('form_validation');
        $config = [
            [
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[post_details.title]',
                'errors' => [
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ],
            ],
            [
                'field' => 'category_id',
                'label' => lang('category'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ],
            ],
            [
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[post_details.slug]',
                'errors' => [
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ],
            ],
            [
                'field' => 'hit',
                'label' => lang('s-view'),
                'rules' => 'trim|is_natural',
                'errors' => [
                    'required' => lang('mes_required'),
                    'is_natural' => lang('mes_is_natural'),
                ],
            ],
        ];
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Bài viết', base_url().'article');
            $this->breadcrumbs->add('Thêm mới', base_url().'article/add');
            $headers['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $headers);
            $this->load->view('article/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $article = [];
            $articleDetail = $this->input->post();
            $metas = $articleDetail['metas'];
            unset($articleDetail['metas']);
            //$articleDetail = $this->nonXssData($articleDetail);
            if (!empty($articleDetail['publish_up'])) {
                $article['publish_up'] = date('Y-m-d H:i:s');
            } else {
                $date = date_create($articleDetail['publish_up']);
                $article['publish_up'] = date_format($date,"Y-m-d H:i:s");
            }

            if (!empty($articleDetail['publish_down'])) {
                $date = date_create($articleDetail['publish_down']);
                $article['publish_down'] = date_format($date,"Y-m-d H:i:s");
            }

            if ($articleDetail['hit']) {
                $article['hit'] = $articleDetail['hit'];
            }

            if (isset($articleDetail['featured'])) {
                $article['featured'] = $articleDetail['featured'];
            }

            if ($articleDetail['status']) {
                $article['status'] = $articleDetail['status'];
            }

            if ($articleDetail['image']) {
                $article['image'] = $articleDetail['image'];
            }

            unset( $articleDetail['publish_up'], $articleDetail['publish_down'], $articleDetail['hit'], $articleDetail['featured'], $articleDetail['status'], $articleDetail['image'] );

            $articleDetail['lang_id'] = $this->_langId;
            $article['created_by'] = $author->id;
            $article['created'] = date('Y-m-d H:i:s');

            if (isset( $articleDetail['meta_extension'])) {
                unset($articleDetail['meta_extension']);
            }

            if (!empty($articleDetail['category_id'])) {
                $article['category_id'] = (int) $articleDetail['category_id'];
            }
            unset($articleDetail['category_id']);

            $articleId = $this->article_model->insertArticle($article);
            if ($articleId) {
                $articleDetail['post_id'] = $articleId;
                if ($detail = $this->article_model->insertArticleDetail($articleDetail)) {
                    //Add Meta
                    if (count($metas) > 0) {
                        foreach ($metas as $key => $value) {
                            $metaArgs = [];
                            $metaArgs['meta_key'] = $key;
                            $metaArgs['meta_value'] = $value;
                            $metaArgs['post_id'] = $articleId;
                            $this->article_model->deleteMeta(['post_id' => $articleId, 'meta_key' => $key]);
                            $this->article_model->addMeta($metaArgs);
                        }
                    }

                    $message = sprintf(lang('message_create_success'), lang('new_article'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_create_error'), lang('new_article_detail'));
                    $this->session->set_flashdata('error', $message);
                }

                $args = [];
                $args['hash_id'] = md5( $articleId );
                if (!$this->article_model->updateArticle( $articleId, $args)) {
                    $message = sprintf(lang('message_creat_error'), lang('new_hash_id'));
                    $this->session->set_flashdata('error', $message);
                }

                redirect(base_url() . 'article/edit/' . $articleId);
            } else {
                $message = sprintf(lang('message_creat_error'), lang('new_article'));
                $this->session->set_flashdata('error', $message);
            }

            redirect(base_url() . 'article');
        }
    }

    /**
     * @name delete
     * Delete a article or any article by id
     * */
    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if (empty($ids)) {
            $message = sprintf(lang('message_delete_warning'), lang('article'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'article');
            return false;
        }

        $where = [
            'field' => 'id',
            'value' => $ids
        ];

        if (is_array($ids)) {
            $query = $this->article_model->deleteArticle($where, true);
            if ($query) {
                $message = sprintf(lang('message_delete_success'), lang('articles'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('articles'));
                $this->session->set_flashdata('error', $message);
            }

            echo 1;
            die;
        } else {
            $query = $this->article_model->deleteArticle(array('id' => (int) $ids));
            if ($query) {
                $message = sprintf(lang('message_delete_success'), lang('article'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('article'));
                $this->session->set_flashdata('error', $message);
            }

            if($id) {
                redirect(base_url() . 'article');
            }

            echo 2;
            die;
        }
    }
    /*
    * changeFeatured
    * activate, deactivate a featured article
    */
    public function changeFeatured()
    {
        $info = $this->input->post();
        $args = [];
        $featured = empty($info['featured']) ? 1 : 0;
        $args['featured'] = $featured;
        if ($this->article_model->updateArticle($info['id'], $args)) {
            $message = sprintf(lang('message_change_success'), lang('featured'));
            $this->session->set_flashdata('success', $message);
        } else {
            $message = sprintf(lang('message_change_error'), lang('featured'));
            $this->session->set_flashdata('error', $message);
        }

        echo 1; die;
    }
    /**
    * Edit
    * Update info of article into article table by id
    **/
    public function edit($id)
    {
        $where = [
            'post_details.lang_id' => $this->_langId,
            'posts.id' => (int) $id,
        ];

        $article = $this->article_model->getArticle($where);
        //Get Post Meta
        $metas = $this->article_model->getPostMetas($article->id);
        $postMeta = [];
        foreach ($metas as $meta) {
            $postMeta[$meta->meta_key] = $meta->meta_value;
        }
        $article->metas = (object) $postMeta;
        if (empty($article)) {
            $message = sprintf(lang('message_delete_warning'), lang('article'));
            $this->session->set_flashdata('error', $message);
            redirect( base_url() . 'article');
        }

        if ($article->publish_up) {
            $date = date_create($article->publish_up);
            $article->publish_up = date_format($date,"d-m-Y H:i:s");
        }

        if ($article->publish_down != 0) {
            $date = date_create($article->publish_down);
            $article->publish_down = date_format($date,"d-m-Y H:i:s");
        }

        $data = [];
        $data['_baseUrl'] = $this->getBaseUrl();
        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(500);        
        $categoryOption = $this->category_model->getCategoryOption();
        $author = $this->session->userdata('user');
        $data['author'] = [
            'id' => $author->id,
            'username' => $author->username,
        ];
        $data['categoryOption'] = $categoryOption;
        $data['article'] = $article;
        $this->load->library('form_validation');
        $config = [
            [
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[post_details.title]',
                'errors' => [
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ],
            ],
            [
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[post_details.slug]',
                'errors' => [
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ],
            ],
            [
                'field' => 'hit',
                'label' => lang('s-view'),
                'rules' => 'trim|is_natural',
                'errors' => [
                    'required' => lang('mes_required'),
                    'is_natural' => lang('mes_is_natural'),
                ],
            ],
        ];
        //checking changed title
        $title = $this->input->post('title');
        $slug = $this->input->post('slug');
        if ($article->title == $title) {
            $config[0] = [
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]',
                'errors' => [
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                ],
            ];
        }

        if ($article->slug == $slug) {
            $config[1] = [
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('mes_required')
                ],
            ];
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Bài viết', base_url().'article');
            $this->breadcrumbs->add('Sửa bài viết | ' . $article->title, base_url().'article/edit/' . $article->id);
            $headers['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $headers);
            $this->load->view('article/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $article = [];
            $articleDetail = $this->input->post();
            $metas = $articleDetail['metas'];
            unset($articleDetail['metas']);
            //$articleDetail = $this->nonXssData($articleDetail);

            if (!empty($articleDetail['publish_up'])) {
                $article['publish_up'] = date('Y-m-d H:i:s');
            } else {
                $date = date_create($articleDetail['publish_up']);
                $article['publish_up'] = date_format($date,"Y-m-d H:i:s");
            }

            if (!empty($articleDetail['publish_down'] )) {
                $date = date_create($articleDetail['publish_down']);
                $article['publish_down'] = date_format($date,"Y-m-d H:i:s");
            } else {
                $article['publish_down'] = '';
            }

            if ($articleDetail['hit']) {
                $article['hit'] = $articleDetail['hit'];
            }

            if (isset($articleDetail['featured'])) {
                $article['featured'] = $articleDetail['featured'];
            }

            if ($articleDetail['status']) {
                $article['status'] = $articleDetail['status'];
            }

            if ($articleDetail['image']) {
                $article['image'] = $articleDetail['image'];
            }

            unset($articleDetail['publish_up'], $articleDetail['publish_down'], $articleDetail['hit'], $articleDetail['featured'], $articleDetail['status'], $articleDetail['image']);

            $articleDetail['lang_id'] = $this->_langId;
            $article['modified_by'] = $author->id;
            $article['modified'] = date('Y-m-d H:i:s');

            if (isset( $articleDetail['meta_extension'])) {
                unset($articleDetail['meta_extension']);
            }

            if (!empty($articleDetail['category_id'])) {
                $article['category_id'] = (int) $articleDetail['category_id'];
            }
            unset( $articleDetail['category_id'] );

            $articleId = $this->article_model->updateArticle($id, $article);
            if ($articleId) {
                if ($this->article_model->updateArticleDetails($id, $articleDetail)) {
                    //Add Meta
                    if (count($metas) > 0) {
                        foreach ($metas as $key => $value) {
                            $metaArgs = [];
                            $metaArgs['meta_key'] = $key;
                            $metaArgs['meta_value'] = $value;
                            $metaArgs['post_id'] = $id;
                            $this->article_model->deleteMeta(['post_id' => $id, 'meta_key' => $key]);
                            $this->article_model->addMeta($metaArgs);
                        }
                    }

                    $message = sprintf(lang('message_update_success'), lang('article'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('new_article_detail'));
                    $this->session->set_flashdata('error', $message);
                }
            } else {
                $message = sprintf(lang('message_creat_error'), lang('article'));
                $this->session->set_flashdata('error', $message);
            }

            redirect(base_url() . 'article/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
        $args = [];
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if (!isset($ids) || empty($ids)){
            return false;
        }

        if (isset($type) && !empty($type) && $type =='multi') {             
            if (!empty($ids)) {
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if ($this->article_model->changeStatus( $ids, $args)) {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                
                echo 1;
                die;
            }
        } 
    }
}