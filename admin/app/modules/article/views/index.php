<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-newspaper-o"></i> <?php echo lang('management_article');?></h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>                
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                    <form method="GET" accept-charset="utf-8" id="formList">
                        <input type="hidden" id="sort" name="order" value="">
                        <input type="hidden" id="sortBy" name="by" value="">
                        <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">                        
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'article/del'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                    <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'article/changestatus'; ?>">
                                    <i class="fa fa-check"></i> <?php echo lang('button_publish');?></a>
                                    <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'article/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('draft');?></a>                               
                                </div>

                                <a href="<?php echo base_url();?>category" class="btn btn-success btn-sm">
                                <i class="fa fa-navicon"></i> Danh mục</a>   
                                <a href="<?php echo base_url();?>article/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> <?php echo lang('add_article');?></a>
                            </div>                    
                        </div>                                                
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="col-md-3 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                            </div>

                            <div class="col-md-2">
                                <select name="status" class="form-control input-sm sys-filler" onchange="this.form.submit();">
                                    <?php foreach($this->config->item('statusArticle') as $key=>$value):?>
                                    <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>                                
                                    <?php endforeach;?>
                                </select>                            
                            </div>   
                            <div class="col-md-2">
                                <select name="category_id" id="category_id" class="form-control input-sm sys-filler" onchange="this.form.submit();">
                                    <option value=""><?php echo lang('category_option');?></option>
                                    <?php echo $categoryOption; ?>
                                </select>                            
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                                <?php if (count($article) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                                                                     
                        </div>
                        <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>
                                    <th class="text-center">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('title');?> <a href="javascript: doSort('title', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('author');?> <a href="javascript: doSort('created_by', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('category');?> <a href="javascript: doSort('category_id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('status');?> <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('featured');?> <a href="javascript: doSort('featured', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('publish_up');?> <a href="javascript: doSort('publish_up', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('publish_down');?> <a href="javascript: doSort('publish_down', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('actions');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if (count($article) > 0):
                                    foreach ($article as $k=>$item): 
                            ?>
                                <tr id="row-<?php echo $k; ?>" class="">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $item->id;?>" name="checkItems[]" />
                                    </td> 
                                    <td class="text-center"><?php echo $item->id; ?></td>
                                    <td>
                                        <strong><?php echo $item->title; ?></strong>
                                    </td>
                                    <td><?php echo $item->author; ?></td>
                                    <td><?php echo $item->category; ?></td>
                                    <td class="text-center">
                                        <?php if( $item->status == 'draft' ) : ?>
                                            <span class="text-danger"><?php echo lang($item->status); ?></span>
                                        <?php elseif($item->status == 'publish') : ?>
                                            <span class="text-success"><?php echo lang($item->status); ?></span>
                                        <?php elseif($item->status == 'pending_review') : ?>
                                            <span class="text-warning"><?php echo lang($item->status); ?></span>
                                        <?php endif; ?>
                                    </td>
                                    <td width="7%" class="text-center">
                                        <?php if($item->featured == 1): ?>
                                            <i onclick="changeFeatured('<?php echo base_url().'article/changeFeatured'; ?>', <?php echo $item->id;?>, <?php echo $item->featured;?>)" title="bài viết nổi bật" class="fa fa-star active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeFeatured('<?php echo base_url().'article/changeFeatured'; ?>', <?php echo $item->id;?>, <?php echo $item->featured;?>)" title="bài viết bình thường" class="fa fa-star deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <?php $publish_up = date_create( $item->publish_up); 
                                        $publish_up = date_format($publish_up,'d-m-Y');
                                        echo $publish_up; ?>
                                    </td>
                                    <?php if( $item->publish_down != 0 ) : ?>
                                    <td>
                                        <?php $publish_down = date_create( $item->publish_down); 
                                        $publish_down = date_format($publish_down,'d-m-Y');
                                        echo $publish_down; ?>
                                    </td>
                                    <?php else : ?>
                                    <td></td>
                                    <?php endif; ?>
                                    <td width="10%" class="text-center">
                                        <a href="<?php echo base_url().'article/edit/'.$item->id; ?>" title="">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('article/del'); ?>', <?php echo $item->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php 
                                    endforeach; 
                                else:
                            ?>
                            <tr>
                                <td colspan="10" class="text-center">Không có bản ghi nào!</td>
                            </tr>
                            <?php 
                                endif;
                            ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($article) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                                <?php if (count($article) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'article/del'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                    <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'article/changestatus'; ?>">
                                    <i class="fa fa-check"></i> <?php echo lang('button_publish');?></a>
                                    <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'article/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('draft');?></a>                               
                                </div>

                                <a href="<?php echo base_url();?>category" class="btn btn-success btn-sm">
                                <i class="fa fa-navicon"></i> Danh mục</a>   
                                <a href="<?php echo base_url();?>article/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> <?php echo lang('add_article');?></a>
                            </div>                    
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var message_confirm_delete = '<?php echo lang('message_confirm_delete'); ?>';
    var _category_id_article = '<?php echo $category_id; ?>';
</script>