<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> Tạo mã khóa học</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Số lượng mã (<span class="required">*</span>)</label>
                            <div class="col-sm-10">
                                <input type="text" name="nums" value="<?php echo set_value('nums'); ?>" class="form-control">
                                <?php echo form_error('nums', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>                             
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">                            
                            <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Tạo mã</button>
                        </div>
                    </div>                      
                </form>
            </div>
        </div>
    </div>  
</div>