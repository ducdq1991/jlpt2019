<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('members_management') . ' - ' . $course->name;?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">                
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="Username" name="username" value="<?php echo $username;?>">                                
                            </div>                              
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                            </div>
                            <div class="col-md-2 text-right">
                               <select name="type" class="form-control">
                                   <option value="" <?php if($type == ''):?> selected="selected" <?php endif;?>>Hình thức</option>
                                   <option value="1" <?php if($type == 1):?> selected="selected" <?php endif;?>>Mua khóa học</option>
                                   <option value="2" <?php if($type == 2):?> selected="selected" <?php endif;?>>Khuyến mại</option>
                                </select>                                
                            </div>                                                  
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i>Tìm kiếm</button>
                            </div>
                            <div class="col-md-2">                           
                            </div>                        
                            <div class="col-md-4 padding-middle text-right">
                                    <?php 
                                        if (count($totalCount) > 0) {                                        
                                            echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                        }
                                    ?>
                            </div>                            
                        </div>                        
                    </div>  
                        <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                <!--
                                    <th class="text-center">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>-->
                                    <th class="text-center">ID
                                    	<a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                    </th>
                                    <th class="text-center"><?php echo lang('username');?>
                                    	<a href="javascript: doSort('username', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('full_name');?>
                                    	<a href="javascript: doSort('full_name', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                        <th class="text-center"><?php echo lang('email');?>
                                        <a href="javascript: doSort('email', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"><?php echo lang('phone_no');?>
                                    	<a href="javascript: doSort('phone', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>                                    
                                    <th class="text-center">Số dư <a href="javascript: doSort('bonus_exam', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Tổng điểm 
                                        <a href="javascript: doSort('total_point', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Tỷ lệ đúng 
                                    </th>
                                    <th>Số khóa học đã mua</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>                            
                            <?php foreach($listItems as $k=>$user) : ?>
                                <tr id="row-<?php echo $k; ?>" class="">
                                   
                                    <td class="text-center"><?php echo $user->user_id; ?></td>
                                    <td><strong><?php echo $user->username; ?></strong></td>
                                    <td class="text-left"><?php echo $user->full_name; ?></td>
                                    <td class="text-left"><?php echo $user->email; ?></td>
                                    <td class="text-left"><?php echo $user->phone; ?></td>
                                    <td class="text-center">
                                        <strong><?php echo number_format($user->amount);?></strong> <sup>đ</sup>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php 
                                        echo number_format($user->total_point);
                                        ?></strong>
                                    </td>                                    
                                    <td>
                                        <?php
                                            if ($user->total_exam_question > 0) {
                                                echo round(($user->total_exam_question_true / $user->total_exam_question) * 100, 2);    
                                            } else {
                                                echo 0;
                                            } 
                                            
                                        ?> %
                                        </span>
                                        <span>(<?php echo $user->total_exam_question_true;?>/<?php echo $user->total_exam_question;?>)</span>                                        
                                    </td>
                                    <td><?php echo $user->courseCount;?></td>
                                    <td><a href="<?php echo base_url();?>course/removeuser?course_id=<?php echo $course->id;?>&user_id=<?php echo $user->user_id;?>" title=""><fa class="fa fa-trash"></fa></a></td>
                                    
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-1">
                                    <select class="rowPerPage form-control input-sm sys-filler">                                
                                        <?php foreach($itemPerPages as $key=>$value):?>
                                        <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                        <?php endforeach;?>
                                    </select>                             
                                </div>
                                <div class="paging col-md-8 text-center">
                                    <?php
                                        if (count($totalCount) > 0) {
                                            echo $paging;    
                                        }                                 
                                    ?>                                
                                </div>
                                <div class="col-md-3 padding-middle text-right">
                                    <?php 
                                        if (count($listItems) > 0) {                                        
                                            echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                        }
                                    ?>
                                </div>                                 
                            </div>                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>