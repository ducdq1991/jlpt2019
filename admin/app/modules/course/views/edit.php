<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-edit"></i> <?php echo lang('edit_course');?> | <?php echo $course->name;?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="pull-right form-module-action">
                            <a href="<?php echo base_url();?>course" class="btn btn-sm btn-white"><i class="fa fa-reply"></i> <?php echo lang('exit');?></a>
                            <a href="<?php echo base_url();?>course/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('course_name'); ?> (<sub class="required">*</sub>)</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo set_value('name', $course->name); ?>" class="autoSlug auto-course-slug form-control">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tiêu đề mở rộng</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" value="<?php echo set_value('title', $course->title); ?>" class="form-control">
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('course_code'); ?> (<sub class="required">*</sub>)</label>
                            <div class="col-sm-10">
                                <input name="course_code" type="text" value="<?php echo set_value('course_code', $course->course_code); ?>" class="form-control">
                                <?php echo form_error('course_code', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Danh mục khóa học (<sub class="required">*</sub>)</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="category_id">
                                    <option value="">--Chọn--</option>
                                    <?php foreach ($categories as $category) :?>
                                    <option <?php if($course->category_id == $category->id):?> selected="selected" <?php endif;?> value="<?php echo $category->id;?>"><?php echo $category->name;?></option>
                                    <?php endforeach;?>  
                                </select>
                                <?php echo form_error('category_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>                         
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Môn học (<sub class="required">*</sub>)</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="subject_id">
                                    <option value="">--Chọn--</option>
                                    <?php foreach ($subjects as $subject) :?>
                                    <option <?php if($course->subject_id == $subject->subject_id):?> selected="selected" <?php endif;?> value="<?php echo $subject->subject_id;?>"><?php echo $subject->name;?></option>
                                    <?php endforeach;?>  
                                </select>
                                <?php echo form_error('subject_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>                         
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('course_parent'); ?></label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="parent" id="parent_id">
                                    <option value="0">--<?php echo lang('parent'); ?>--</option>
                                    <?php echo $courseOption;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giáo viên phụ trách (<sub class="required">*</sub>)</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="manager_id">
                                    <option value="">--Chọn--</option>
                                    <?php foreach ($teachers as $teacher) :?>
                                    <option <?php if($course->manager_id == $teacher->id): ?> selected="selected" <?php endif;?>  value="<?php echo $teacher->id;?>"><?php echo $teacher->username;?> <?php echo $teacher->full_name;?> (<?php echo $teacher->email;?>)</option>
                                    <?php endforeach;?>  
                                </select>
                                <?php echo form_error('manager_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>                                                  
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('avatar'); ?></label>
                            <div class="col-sm-10">
                                <div id="load-image" class="load-image-course">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="image" value="<?php echo set_value('image', $course->image); ?>" id="image" />
                                <button type="button" id="load-form-featured-image" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredImageModal"><?php echo lang('avatar_button'); ?></button>
                                <button type="button" id="remove-featured-image" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_button'); ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Video giới thiệu khóa học</label>
                            <div class="col-sm-10">
                                <input name="intro_video" type="text" value="<?php if($course->intro_video) echo set_value('intro_video', $course->intro_video); else echo set_value('intro_video');?>" class="form-control">
                            </div>
                        </div>                        
                        <div class="form-group"><label class="col-sm-2 control-label"><?php echo lang('description'); ?></label>
                            <div class="col-sm-10">
                                <textarea name="description" rows="8" class="editor form-control note-codable"><?php echo set_value('description', $course->description); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('teacher'); ?></label>
                            <div class="col-sm-10">
                                <input name="teacher" type="text" value="<?php if($course->teacher) echo set_value('teacher', $course->teacher); else echo set_value('teacher');?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group" id="start-date">
                            <label class="col-sm-2 control-label"><?php echo lang('start_date'); ?></label>
                            <div class="input-group date col-sm-10">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="start_date" class="form-control" value="<?php echo set_value('start_date', $course->start_date); ?>">
                            </div>
                        </div>
                        <div class="form-group" id="end-date">
                            <label class="col-sm-2 control-label"><?php echo lang('end_date'); ?></label>
                            <div class="input-group date col-sm-10">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="end_date" class="form-control" value="<?php echo set_value('end_date', $course->end_date); ?>">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('slug'); ?></label>
                            <div class="col-sm-10"><input name="slug" type="text" value="<?php echo set_value('slug', $course->slug); ?>" class="slug slug-course form-control"></div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('seo_keywords');?></label>
                            <div class="col-sm-10"><input name="meta_keywords" type="text" value="<?php echo set_value('meta_keywords', $course->meta_keywords); ?>" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('seo_description');?></label>
                            <div class="col-sm-10"><textarea name="meta_description" type="text" class="form-control"><?php echo set_value('meta_description', $course->meta_description); ?></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá gốc</label>
                            <div class="col-sm-3"><input type="number" name="origin_price" value="<?php echo set_value('origin_price', $course->origin_price); ?>" class="form-control"></div>
                            <p class="support-text">Đơn vị VNĐ</p>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá bán</label>
                            <div class="col-sm-3"><input type="number" name="price" value="<?php echo set_value('price', $course->price); ?>" class="form-control"></div>
                            <p class="support-text">Đơn vị VNĐ</p>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label class="col-sm-2 control-label">Giảm giá [áp dụng cho CTV]</label>
                            <div class="col-sm-3"><input type="number" name="discount" value="<?php echo set_value('discount', $course->discount); ?>" class="form-control"></div>
                            <p class="support-text">Đơn vị VNĐ</p>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('order');?></label>
                            <div class="col-sm-2"><input type="number" name="ordering" value="<?php echo set_value('ordering', $course->ordering); ?>" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giới hạn lượt xem bài giảng</label>
                            <div class="col-sm-2"><input type="number" name="lesson_view_limit" value="<?php echo set_value('lesson_view_limit', $course->lesson_view_limit); ?>" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giới hạn lượt thực hành</label>
                            <div class="col-sm-2"><input type="number" name="practice_view_limit" value="<?php echo set_value('practice_view_limit', $course->practice_view_limit); ?>" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giới hạn lượt thi online</label>
                            <div class="col-sm-2"><input type="number" name="exam_view_limit" value="<?php echo set_value('exam_view_limit', $course->exam_view_limit); ?>" class="form-control"></div>
                        </div>                          
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('status');?></label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php if( set_checkbox('status', '1') ) echo set_checkbox('status', '1'); else if($course->status == 1) echo 'checked=""'; ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>                                              
                    </div>  
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset"><?php echo lang('reset');?></button>
                            <button class="btn btn-primary" name="submit" type="submit"><?php echo lang('submit');?></button>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>  
<!-- Featured Image Modal -->
<div id="featuredImageModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-image-iframe" src="<?php echo base_url(); ?>assets/js/filemanager/dialog.php?field_id=image" width="100%" height="450px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<script type="text/javascript">
    var _parent_id = "<?php echo $course->parent; ?>";
    var _baseUrl = "<?php echo $_baseUrl; ?>";
</script>

<?php echo $tinymce; ?>