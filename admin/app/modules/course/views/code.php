<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                <i class ="fa fa-barcode"></i> Danh sách mã
                </h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" action="<?php echo base_url('course/listcodes/' . $course->id);?>" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">                    
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                       
                            </div>
                            <?php if(isset($back)):?>
                                <a href="<?php echo base_url();?>course" class="btn btn-white btn-sm">
                                <i class="fa fa-cubes"></i> <?php echo lang('course');?></a>
                            <?php endif;?>
                            <a href="<?php echo base_url();?>course/gencode/<?php echo $course->id;?>" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Tạo mã</a>
                        </div>                    
                    </div>
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                        <div class="col-md-3 text-right">
                            <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="code" value="<?php echo $code;?>">                                
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                        </div>
                        <div class="col-md-2">
                          
                        </div>                                               
                        <div class="col-md-3 padding-middle text-right pull-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div>                        
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="text-center" width="30">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th class="text-center" width="90">ID
                                <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">Mã
                                    <a href="javascript: doSort('name', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center" width="110">Đã sử dụng
                                    <a href="javascript: doSort('total_use', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>                                
                                <th class="text-center" width="150">Ngày tạo
                                    <a href="javascript: doSort('created_at', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center" width="150">Tình trạng
                                    <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>                                
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php
                                if (isset($listItems) && count($listItems) > 0): 
                                    foreach ($listItems as $item) :
                            ?>
                                <tr class="gradeA odd" role="item">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $item->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="text-center"><?php echo $item->id;?></td>
                                    <td><strong><?php echo $item->code;?></strong></td>
                                    <td><?php echo number_format($item->total_use);?></td>
                                    <td><?php echo date('d/m/Y H:i:s', strtotime($item->created_at));?></td>
                                    <td class="text-center">
                                        <?php if($item->status == 1):?>
                                            <span class="label label-success">Còn hiệu lực</span>
                                        <?php else : ?>
                                            <span class="label label-danger">Hết hiệu lực</span>
                                        <?php endif;?>
                                    </td>
                                </tr>
                            <?php 
                                    endforeach;
                                else:
                            ?>
                            <tr>
                                <td colspan="7" class="text-center">Không có bản ghi nào!</td>
                            </tr>
                            <?php
                                endif;
                            ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">                                
                                    <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($totalCount) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">                             
                                </div>  
                                <a href="<?php echo base_url();?>course/gencode/<?php echo $course->id;?>" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Tạo mã</a>
                            </div>                   
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
