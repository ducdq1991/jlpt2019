<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends Bigshare_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('user/user_model');
		$this->load->model('lesson/lesson_model');
		$this->load->model('codes/codes_model');
		$this->load->model('subjects/subjects_model');
		$this->load->model('coursecategory/coursecategory_model');
		$this->lang->load('course',$this->session->userdata('lang'));			
	}

	public function index($param1 = 0, $param2 = 0)
	{
		$parent = intval(str_replace('id-', '', $param1));
		$page = intval(str_replace('page-', '', $param2));	
		if (!$parent) {
			$page = intval(str_replace('page-', '', $param1));
		}

		if ($page < 1) {
			$page = 1;
		}
		$data = $where = $orderCondition = $likes = $sortFields = [];	

		$subjects = $this->subjects_model->getSubjects([], 0, 0, ['subject_id' => 'desc']);
		$nameOfSubjects = [];
		foreach ($subjects as $subject) {
			$nameOfSubjects[$subject->subject_id] = $subject->name;
		}		
		$data['nameOfSubjects'] = $nameOfSubjects;
		$data['subjects'] = $subjects;		

		// Set language	Condition
		$where['course_details.lang_id'] = $this->_langId;
		if(empty($parent)) {
			$where['courses.parent'] = 0;
		} else {
			$where['courses.parent'] = (int) $parent;
			$data['back'] = true;
		}
		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;        
		$offset = ($page - 1) * $limit;
		//Get Filters
		$orderOption = $this->input->get('order', '');
		$orderBy = $this->input->get('by', 'asc');
		$keyword = $this->input->get('keyword');
		$status = $this->input->get('status');
		// Set Status Condition
		if ($status != '') {
		  $where['courses.status'] = ($status ==='active') ? 1 : 0;
		}		

		if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
			$orderCondition[$orderOption] = $orderBy;
		}

		if ($keyword != '') {
			$likes['field'] = 'name';
			$likes['value'] = (string) trim($keyword);
		}
		$subjectId = $this->input->get('subject_id');
		if($subjectId != NULL) {
			$where['courses.subject_id'] = (int) $subjectId;
		}
		$data['subjectId'] = (int) $subjectId;

		// Set actived Fillter
		$data['order'] = $orderOption;
		$data['keyword'] = $keyword;
		$data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		$data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
		$data['status'] = $status;		
		$sortFields['name'] = lang('name_course');
		$sortFields['ordering'] = lang('orders');	
		$data['sortFields'] = $sortFields;
		$data['itemPerPages'] = $this->config->item('rowPerPages');
		$data['sortByItems'] = $this->config->item('sortBy');
		//Query
		$total = $this->course_model->totalCourse($where);    		
		$courses = $this->course_model->getCourses($where, $limit, $offset, $orderCondition, $likes);	
        $showFrom = $offset + count($courses);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;

		//Render Paging
		$queryString = $_SERVER['QUERY_STRING'];
		if ($parent) {
			$data['paging'] = paging(base_url() .'course/id-'.$parent, $page, $total, $limit, $queryString);
		} else {
			$data['paging'] = paging(base_url() .'course', $page, $total, $limit, $queryString); 
		}

		//Render Data
		$data['courses'] = $courses;  
		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'course');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
		$this->load->view('course/index', $data);
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$where = $data = $course = $courseDetail = [];	
        $data['base_url'] = $this->getBaseUrl();

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);

		// Get Parent Course
		$courseOption = $this->course_model->getCourseOption();
		$data['courseOption'] = $courseOption;			
		$subjects = $this->subjects_model->getSubjects([], 0, 0, ['subject_id' => 'desc']);
		$data['subjects'] = $subjects;

		$categories = $this->coursecategory_model->getCategories([], 0, 0, ['id' => 'desc']);
		$data['categories'] = $categories;

		//Get Teachers
		$condTecher = ['IN' => ['key' => 'users.group_id', 'value' => [21,24]]];
		$teachers = $this->user_model->getTechers($condTecher);
		$data['teachers'] = $teachers;
		//Check form input
		$this->load->library('form_validation');
		$config = array(
            array(
                'field' => 'name',
                'label' => lang('name_course'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'category_id',
                'label' => 'Danh mục khóa học',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Danh mục khóa học là bắt buộc.',
                ),
            ),
            array(
                'field' => 'subject_id',
                'label' => 'Môn học',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Môn học là bắt buộc.',
                ),
            ),            
            array(
                'field' => 'manager_id',
                'label' => 'Giáo viên phụ trách',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Giáo viên phụ trách là bắt buộc.',
                ),
            ),                        
            array(
                'field' => 'course_code',
                'label' => lang('course_code'),
                'rules' => 'trim|required|is_unique[courses.course_code]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Khóa học', base_url().'course');
	        $this->breadcrumbs->add('Thêm khóa học', base_url().'course/add');
	        $head['breadcrumb'] = $this->breadcrumbs->display();			
			$this->load->view('layouts/header', $head);
    		$this->load->view('course/add', $data);
			$this->load->view('layouts/footer'); 				
        } else {
        	$course = [];
        	// Get Data from Input Form
        	$postData = $this->input->post();
        	//$postData = $this->nonXssData($postData);
			// Course
			$course['parent'] = ($postData['parent'] == '') ? 0 : $postData['parent'];
			$course['status'] = isset($postData['status']) ? $postData['status'] : 0;
			$course['ordering'] = ($postData['ordering'] == '') ? 0 : $postData['ordering'];
			$course['created'] = date('Y-m-d H:i:s');
			$course['subject_id'] = isset($postData['subject_id']) ? $postData['subject_id'] : NULL;
			$course['category_id'] = isset($postData['category_id']) ? $postData['category_id'] : NULL;
			if(!empty($postData['start_date'])) {
				$date = date_create($postData['start_date']);
				$date = date_format($date, 'Y-m-d H:i:s');
				$course['start_date'] = $date;
			} else {
				$course['start_date'] = date('Y-m-d H:i:s');
			}
			if(!empty($postData['end_date'])) {
				$date = date_create($postData['end_date']);
				$date = date_format($date, 'Y-m-d H:i:s');
				$course['end_date'] = $date;
			}
			$course['course_code'] = $postData['course_code'];
			$course['intro_video'] = $postData['intro_video'];
			$course['lesson_view_limit'] = $postData['lesson_view_limit'];
			$course['exam_view_limit'] = $postData['exam_view_limit'];
			$course['practice_view_limit'] = $postData['practice_view_limit'];
			// Course Detail
			$courseDetail['name'] = $postData['name'];
			$courseDetail['title'] = isset($postData['title']) ? $postData['title'] : NULL;
			$courseDetail['description'] = $postData['description'];
			$courseDetail['slug'] = $postData['slug'];
			$courseDetail['meta_keywords'] = $postData['meta_keywords'];
			$courseDetail['meta_description'] = $postData['meta_description'];
			$courseDetail['lang_id'] = $this->_langId;
			$courseDetail['teacher'] = $postData['teacher'];
			$courseDetail['price'] = ($postData['price'] == '') ? 0 : $postData['price'];
			$courseDetail['origin_price'] = ($postData['origin_price'] == '') ? NULL : $postData['origin_price'];

			$courseDetail['discount'] = ($postData['discount'] == '') ? 0 : (float) $postData['discount'];

			$courseDetail['manager_id'] = isset($postData['manager_id']) ? $postData['manager_id'] : NULL;
			$courseId = $this->course_model->addCourse( $course );
			if( $courseId ) {
				$courseDetail['course_id'] = $courseId;
				if( $this->course_model->addCourseDetail( $courseDetail ) ) {
					$message = sprintf(lang('message_create_success'), lang('new_course'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_create_error'), lang('new_course_details'));
                    $this->session->set_flashdata('error', $message);
                }
			}
			redirect( base_url() . 'course');
        }      
	}
/**
 * [edit Course]
 * @param  integer $courseId
 * @return [type]
 */
	public function edit($courseId = 0 )
	{	
		if ( !$this->course_model->getCourseById( $courseId ) ) {
			$message = sprintf(lang('message_delete_warning'), lang('course'));
            $this->session->set_flashdata('error', $message);
			redirect( base_url() . 'course');
		}
		$data = $where = $courseUpdate = [];

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);
        		
		$data['_baseUrl'] = $this->getBaseUrl();
		$course = $this->course_model->getCourseById( (int) $courseId );
		$date = date_create($course->start_date);
		$date = date_format($date, 'd-m-Y');
		$course->start_date = $date;
		if($course->end_date != 0 ) {
			$date = date_create($course->end_date);
			$date = date_format($date, 'd-m-Y');
			$course->end_date = $date;
		}
		$data['course'] = $course;		
		// Get Parent Course
		$courseOption = $this->course_model->getCourseOption(0,$course->parent);		
		$data['courseOption'] = $courseOption;

		$subjects = $this->subjects_model->getSubjects([], 0, 0, ['subject_id' => 'desc']);
		$data['subjects'] = $subjects;

		$categories = $this->coursecategory_model->getCategories([], 0, 0, ['id' => 'desc']);
		$data['categories'] = $categories;

		//Get Teachers
		$condTecher = ['IN' => ['key' => 'users.group_id', 'value' => [21,24]]];
		$teachers = $this->user_model->getTechers($condTecher);
		$data['teachers'] = $teachers;

		// Load Validate Library
		$this->load->library('form_validation');
		
		$config = array(
            array(
                'field' => 'name',
                'label' => lang('name_course'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'subject_id',
                'label' => 'Môn học',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Môn học là bắt buộc.',
                ),
            ),
            array(
                'field' => 'category_id',
                'label' => 'Danh mục khóa học',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Danh mục khóa học là bắt buộc.',
                ),
            ),            
            array(
                'field' => 'manager_id',
                'label' => 'Giáo viên phụ trách',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Giáo viên phụ trách là bắt buộc.',
                ),
            ),                             
            array(
                'field' => 'course_code',
                'label' => lang('course_code'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
        $course_code = $this->input->post('course_code');
        if($course_code == $course->course_code) {
        	unset($config[1]);
        }
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Khóa học', base_url().'course');
	        $this->breadcrumbs->add('Sửa khóa học', base_url().'course/edit/' .$courseId);
	        $head['breadcrumb'] = $this->breadcrumbs->display();			
			$this->load->view('layouts/header', $head);
    		$this->load->view('course/edit', $data);
			$this->load->view('layouts/footer'); 				
        } else {
        	$course = [];
        	// Get Data from Input Form
        	$postData = $this->input->post();
        	//$postData = $this->nonXssData($postData);
			// Course
			$course['parent'] = isset($postData['parent']) ? $postData['parent'] : 0;
			$course['status'] = isset($postData['status']) ? $postData['status'] : 0;
			$course['subject_id'] = isset($postData['subject_id']) ? $postData['subject_id'] : NULL;
			$course['category_id'] = isset($postData['category_id']) ? $postData['category_id'] : NULL;
			$course['ordering'] = $postData['ordering'];			
			if(!empty($postData['start_date'])) {
				$date = date_create($postData['start_date']);
				$date = date_format($date, 'Y-m-d H:i:s');
				$course['start_date'] = $date;
			} else {
				$course['start_date'] = date('Y-m-d H:i:s');
			}
			if(!empty($postData['end_date'])) {
				$date = date_create($postData['end_date']);
				$date = date_format($date, 'Y-m-d H:i:s');
				$course['end_date'] = $date;
			} else {
				$course['end_date'] = '';
			}
			$course['course_code'] = $postData['course_code'];
			$course['image'] = $postData['image'];
			$course['intro_video'] = $postData['intro_video'];
			$course['lesson_view_limit'] = $postData['lesson_view_limit'];
			$course['exam_view_limit'] = $postData['exam_view_limit'];
			$course['practice_view_limit'] = $postData['practice_view_limit'];
						
			// Course Detail
			$courseDetail['name'] = $postData['name'];
			$courseDetail['title'] = isset($postData['title']) ? $postData['title'] : NULL;
			$courseDetail['description'] = $postData['description'];
			$courseDetail['slug'] = $postData['slug'];
			$courseDetail['meta_keywords'] = $postData['meta_keywords'];
			$courseDetail['meta_description'] = $postData['meta_description'];
			$courseDetail['teacher'] = $postData['teacher'];
			$courseDetail['lang_id'] = $this->_langId;
			$courseDetail['price'] = $postData['price'];
			$courseDetail['origin_price'] = $postData['origin_price'];
			$courseDetail['discount'] = $postData['discount'];
			$courseDetail['manager_id'] = isset($postData['manager_id']) ? $postData['manager_id']: NULL;
			if( $this->course_model->updateCourse( $courseId, $course ) ) {
				if( $this->course_model->updateCourseDetail( $courseId, $courseDetail ) ) {
					$message = sprintf(lang('message_update_success'), lang('course'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('course_details'));
                    $this->session->set_flashdata('error', $message);
                }
			}
			redirect( base_url() . 'course');
        }       
	}

	/**
     * @name Del
     * @desc delete a Module from modules table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
        	$message = sprintf(lang('message_delete_warning'), lang('the_course'));
            $this->session->set_flashdata('warning', $message);
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->course_model->deleteCourse( $where, true );
            if( $query ) {
            	$message = sprintf(lang('message_delete_success'), lang('courses'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('courses'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->course_model->deleteCourse( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('course'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('course'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
	/**
	 * [changeStatus of Course]
	 * @return [int]
	 */
	public function changeStatus()
	{
		$args = [];
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['status'] = $status;	
				if($this->course_model->changeStatus( $ids, $args))
				{
					$message = sprintf(lang('message_change_success'), lang('courses'));
	                $this->session->set_flashdata('success', $message);
	            } else {
	                $message = sprintf(lang('message_change_error'), lang('courses'));
	                $this->session->set_flashdata('error', $message);
	            }
                echo 1;
                die;
			}
		} else {	
			$args['status'] = ($status == 1) ? 0 : 1;		
			if( $this->course_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('course'));
	                $this->session->set_flashdata('success', $message);
	            } else {
	                $message = sprintf(lang('message_change_error'), lang('course'));
	                $this->session->set_flashdata('error', $message);
	            }
            echo 1;
            die;	
		}			
	}

	/**
	* Change Ordering
	**/
	public function ordering()
	{
		$data = [];
		$ordering = $this->input->post('ordering');
		$id = $this->input->post('id');
		if(isset($ordering) && !empty($ordering) && intval($ordering) && isset($id)){
			// Try catch Error
			try{
				$data['ordering'] = $ordering;
				if($this->course_model->updateCourse( $id, $data ))
				{
					echo 1; exit;
				} 
			} catch(Exception $ex){
				echo $ex->getMessage(); exit;
			}	
		}		
	}


	public function removeuser()
	{
		$courseId = $this->input->get('course_id', 0);
		$userId = $this->input->get('user_id', 0);
		if ($courseId > 0 && $userId > 0) {
			$where = [
				'user_id' => (int) $userId,
				'course_id' => (int) $courseId,
			];
			$this->course_model->deleteUserInCourse($where);
			$this->session->set_flashdata('success', 'Đã xóa học sinh ra khỏi khóa học');
			redirect(base_url('course/members/' . $courseId));
		}
	}
	
	/**
	 * Show all members register the course
	 **/
	public function members($courseId = 0, $page = 0)
	{		
		if ($page < 1) {
			$page = 1;
		}
		// Paging Configs
		$this->load->helper('paging');	
        	
        $numRecord = $this->input->get('numRecord');
        if ($numRecord == NULL) {
        	$limit = $limit = $this->config->item('per_page');;
        } else {
        	$limit = ($numRecord > 0) ? (int) $numRecord : 0;
        }
              
		$offset = ($page - 1) * $limit;

		//Get Filters
		$orderOption = $this->input->get('order', '');
		$orderBy = $this->input->get('by', 'asc');
		$keyword = $this->input->get('keyword');
		$type = $this->input->get('type');

		$data = $where = $likes = $head = $orderCondition = [];
		$where['user_course.course_id'] = $courseId;
        if (!empty($keyword)) {
            $likes['field'] = 'users.full_name';
            $likes['value'] = $keyword;
        }
        $data['keyword'] = $keyword;
        $username = $this->input->get('username');
        if (!empty($username)) {
            $likes['field'] = 'users.username';
            $likes['value'] = $username;
        }
        $data['username'] = $username; 

        if ($type != '') {
        		$where['user_course.type'] = (int) $type;
        }
        $data['type'] = $type;  

        // Set Status Condition
		if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
			$orderCondition[$orderOption] = $orderBy;
		}
        // Set actived Fillter
		$data['order'] = $orderOption;
		$data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		$data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;	
		$sortFields['name'] = lang('name_course');
		$sortFields['ordering'] = lang('orders');	
		$data['sortFields'] = $sortFields;
		$data['itemPerPages'] = $this->config->item('rowPerPages');
		$data['sortByItems'] = $this->config->item('sortBy');
        $total = $this->course_model->totalCourseUser($where, $likes);
        $orderCondition['user_course.created'] = 'desc';   
		$members = $this->course_model->getCourseUsers($where , $limit, $offset, $likes, $orderCondition);
        $data['listItems'] = $members;

        $showFrom = $offset + count($members);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        //Render Paging
		$queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'course/members/'.$courseId, $page, $total, $limit, $queryString);
		$course = $this->course_model->getCourseById($courseId);
		$data['course'] = $course;
		//Render Data
		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'course');
        $this->breadcrumbs->add('Thành viên khóa học - '.$course->name, base_url().'course/members');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
		$this->load->view('course/members', $data);
		$this->load->view('layouts/footer');
	}

	public function copy($courseId = 0)
	{	
		$item = $this->course_model->getCourseById($courseId);
		$courseArgs = ['id', 'intro_video', 'course_code', 'image', 'parent', 'created', 'ordering', 'status', 'start_date', 'end_date', 'subject_id'];
		$courseDetail = $course = [];
		foreach ($item as $key => $value) {
			if (in_array($key, $courseArgs)) {
				$course[$key] = $value;
			} else {
				$courseDetail[$key] = $value;
			}
		} 

		if (!$item) {
            $this->session->set_flashdata('error', 'Khóa học không tồn tại.');
			redirect( base_url() . 'course');
		}

        $rand = rand(1,100);
        $courseDetail['name'] = $courseDetail['name'] . '-copy' . $rand;        
        $course['status'] = 0;
        $course['course_code'] = $courseDetail['slug'] . '-copy' . $rand;
        $courseDetail['slug'] = $courseDetail['slug'] . '-copy' . $rand;    
        unset($course['id']);

        $where = ['name' => $courseDetail['name']];
        $checkexist = $this->course_model->getCourseDetail($where);
        //$this->copySubCourse($courseId, 10000);
        if (empty($checkexist)) {
            if($newCourseId = $this->course_model->addCourse($course)){              	
            	$courseDetail['course_id'] = $newCourseId;
                $this->course_model->addCourseDetail($courseDetail);          
                $this->session->set_flashdata('success', 'Đã copy chuyên đề thành công!');
                $this->copySubCourse($courseId, $newCourseId);
            } else {                
                $this->session->set_flashdata('error', 'Không copy được chuyên đề');
            } 
        } else {
            $this->session->set_flashdata('error', 'Chuyên đề này đã tồn tại');
        }    

        redirect(base_url() . 'course');
    }



    public function copySubCourse($id = 0, $cid = 0) {
		$courseDefaultArgs = ['id', 'intro_video', 'course_code', 'image', 'parent', 'created', 'ordering', 'status', 'start_date', 'end_date', 'subject_id'];
		
    	if ($id > 0 && is_numeric($id) && is_numeric($cid) && $cid > 0) {
    		$courses = $this->course_model->getcourses(['courses.parent' => $id]);
    		if (count($courses) > 0) {
    			$courseArgs = [];
	    		foreach ($courses as $item) {
	    			$courseDetail = $course = [];
					foreach ($item as $key => $value) {
						if (in_array($key, $courseDefaultArgs)) {
							$course[$key] = $value;
						} else {
							$courseDetail[$key] = $value;
						}
					}

			        $rand = rand(1,100);
			        $courseDetail['name'] = $courseDetail['name'] . '-copy' . $rand;        
			        $course['status'] = 0;
			        $course['course_code'] = $courseDetail['slug'] . '-copy' . $rand;
			        $courseDetail['slug'] = $courseDetail['slug'] . '-copy' . $rand;
			        unset($courseDetail['parent_name']);
			        unset($courseDetail['child']);
			        $course['parent'] = $cid;
			        $where = ['name' => $courseDetail['name']];	
			        $args = ['item' => $course, 'detail' => $courseDetail];
			        $courseArgs[] = $args;				        		        
	    		}
	    		unset($courseArgs[0]);
	    		foreach ($courseArgs as $item) {
			        $id = $item['item']['id'];  
			        unset($item['item']['id']);	    			
	    			$checkexist = $this->course_model->getCourseDetail(['name' => $item['detail']['name']]);
					if (empty($checkexist)) {
			            if($ncid = $this->course_model->addCourse($item['item'])){ 
			            	$item['detail']['course_id'] = $ncid;
			                $this->course_model->addCourseDetail($item['detail']);
			                $cid = $ncid;
			                $this->copyLesson($id, $ncid);
			                $this->copySubCourse($id, $cid);
			            }
			        }
	    		}
    		}
    	}    	
    }

    public function copyLesson($id = 0, $cid = 0) {
    	if ($id > 0 && is_numeric($id) && is_numeric($cid) && $cid > 0) {
    		$lessons = $this->lesson_model->getLessons(['lessons.course_id' => $id]);
    		foreach ($lessons as $item) {    			
    			$rand = rand(1,100);
		        $item->title = $item->title.'-copy' . $rand;        
		        $item->status = 0;
		        $item->slug = $item->slug . '-copy' . $rand;
    			$lesson = (array) $item;
    			$lesson['course_id'] = $cid;
    			unset($lesson['id'], $lesson['hash_id'], $lesson['course']);    			
		        $where = ['lessons.title' => $lesson['title']];
		        $checkexist = $this->lesson_model->getLesson($where);
		        if (empty($checkexist)) {
		            if($lessonId = $this->lesson_model->insertLesson($lesson)){  
		                $this->lesson_model->updateHashId($lessonId);
		            }
		        } 
    		}	
    	}    	
    }

	public function listcodes($courseId = 0, $page = 0)
	{	
		$course = $this->course_model->getCourseById($courseId);
		if (!$course) {
            $this->session->set_flashdata('error', 'Khóa học không tồn tại.');
			redirect( base_url() . 'course');
		}

		if ($page < 1) {
			$page = 1;
		}

		// Paging Configs
		$this->load->helper('paging');	
        	
        $numRecord = $this->input->get('numRecord');
        if ($numRecord == NULL) {
        	$limit = $limit = $this->config->item('per_page');;
        } else {
        	$limit = ($numRecord > 0) ? (int) $numRecord : 0;
        }
              
		$offset = ($page - 1) * $limit;		

		//Get Filters
		$orderOption = $this->input->get('order', '');
		$orderBy = $this->input->get('by', 'asc');
		$code = $this->input->get('code');
		$type = $this->input->get('type');

		$data = $where = $likes = $head = $orderCondition = [];
		$where['course_codes.course_id'] = $courseId;
        if (!empty($code)) {
            $likes['field'] = 'course_codes.code';
            $likes['value'] = $code;
        }
        $data['code'] = $code;

        // Set Status Condition
		if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
			$orderCondition[$orderOption] = $orderBy;
		}
        // Set actived Fillter
		$data['order'] = $orderOption;
		$data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		$data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;	
		$sortFields['name'] = lang('name_course');
		$sortFields['ordering'] = lang('orders');	
		$data['sortFields'] = $sortFields;
		$data['itemPerPages'] = $this->config->item('rowPerPages');
		$data['sortByItems'] = $this->config->item('sortBy');
        $total = $this->codes_model->totalCode($where, $likes);
        $orderCondition['course_codes.created_at'] = 'desc';        
		$codes = $this->codes_model->getCodes($where , $limit, $offset, $orderCondition, $likes);
        $data['listItems'] = $codes;

        $showFrom = $offset + count($codes);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        //Render Paging
		$queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'course/listcodes/'.$courseId, $page, $total, $limit, $queryString);		
		$data['course'] = $course;
		//Render Data
		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Khóa học', base_url().'course');
        $this->breadcrumbs->add($course->name, base_url().'course/edit/' . $course->id);
        $this->breadcrumbs->add('Danh sách mã khóa học:'. $course->name, base_url().'course/members');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
		$this->load->view('course/code', $data);
		$this->load->view('layouts/footer');
    }

	public function gencode($courseId = 0)
	{	
		$data = [];

		$this->load->helper('codes');

		//Get code by id
		$course = $this->course_model->getCourseById($courseId);
		if (!$course) {
            $this->session->set_flashdata('error', 'Khóa học không tồn tại.');
			redirect( base_url() . 'course');
		}

		$this->load->library('form_validation');
		$config = [
            [
                'field' => 'nums',
                'label' => 'Số lượng mã cần tạo',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Vui lòng nhập số lượng mã cần tạo',
                ],
            ],
        ];
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			//Render breadcrumbs
			$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Khóa học', base_url().'course');
	        $this->breadcrumbs->add($course->name, base_url().'course/edit/' . $course->id);
	        $this->breadcrumbs->add('Tạo mã khóa học:'. $course->name, base_url().'course/members');
	        $head['breadcrumb'] = $this->breadcrumbs->display();

	        //Call views
			$this->load->view('layouts/header', $head);
			$this->load->view('course/gencode', $data);
			$this->load->view('layouts/footer');				
        } else {
        	$postData = $this->input->post();        	
        	$nums = (int) $postData['nums'];

        	if ($nums > 0) {
        		$i = 0;
        		while ($i <= $nums) {
	        		$code = createRandomCode(8);        		
	        		if (is_null($this->codes_model->getCode(['course_codes.code' => $code]))) {
	        			$args = [];
	        			$args['code'] = $code;
	        			$args['course_id'] = $courseId;
	        			$args['total_use'] = 0;
	        			$args['created_at'] = date('Y-m-d H:i:s');
	        			if ($this->codes_model->addCode($args) ) {
	        				unset($args);	 	        			
	        				$i++;
	        			}
	        		}
        		}
        		$numCode = $i-1;
    			$this->session->set_flashdata('success', 'Đã tạo ' . $numCode . ' thành công!');
    			redirect( base_url() . 'course');
        	}
        }
    }   
}