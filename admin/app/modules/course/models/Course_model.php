<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends Bigshare_Model {

	public function __construct()
	{
		parent::__construct();
        $this->_table = 'courses';
	}

	/**
	 * [addCourse]
	 * @param [array] $args
	 */
	
	public function addCourse( $args )
	{	
		$this->_table = 'courses';
		$this->_data = $args;
		if($id = $this->addRecord()){
			return $id;
		}

		return false;
	}

	/**
	 * [addCourseDetail]
	 * @param [array] $args
	 */
	
	public function addCourseDetail($args)
	{
		$this->_table = 'course_details';
		$this->_data = $args;
		if( $this->addRecord()){
			return true;
		}

		return false;
	}
	/**
	 * [updateCourse]
	 * @param  [int] $courseId
	 * @param  [array] $args
	 * @return [boolean]
	 */
	
	public function updateCourse($courseId, $args)
	{
		$this->_table = 'courses';
		$where = array();
		$where['id'] = (int) $courseId;
		$this->_wheres = $where;
		$this->_data = $args;
		if( $this->updateRecord()){
			return true;
		}
		return false;
	}

	/**
	 * [updateCourseDetail]
	 * @param  [int] $courseId
	 * @param  [array] $args
	 * @return [boolean]
	 */	
	
	public function updateCourseDetail($courseId, $args)
	{
		$this->_table = 'course_details';
		$where = array();
		$where['course_details.course_id'] = (int) $courseId;
		$this->_wheres = $where;
		$this->_data = $args;
		if( $this->updateRecord()){
			return true;
		}
		return false;
	}	

	/**
	 * [totalCourse]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalCourse($where = array())
	{
		$joins = array();
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'courses.id = course_details.course_id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;
		$this->_joins = $joins;
		$this->_table = 'courses';
		$this->_wheres = $where;
		return $this->totalRecord();
	}

	/**
	 * [delCourse delete One Course]
	 * @param  [int] $courseId
	 * @return [boolean]
	 */
    public function deleteCourse($where = array(), $multiple = false)
    {
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }

	/**
	 * [changeStatus Of Course]
	 * @param  [int or array] $courseIds
	 * @param  [array] $courseUpdate
	 * @return [boolean]
	 */
    public function changeStatus( $ids, $args = array() )
    {
    	$this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

	/**
	 * [getCourseById]
	 * @param  [int] $id
	 * @return [object]
	 */
	public function getCourseById($courseId)
	{
		if (!$courseId){
			return null;
		}
		$where = $whereDetail = array();
		$where['courses.id'] = $courseId;
		$whereDetail['course_details.course_id'] = $courseId;
		$this->resetQuery();
		$this->_table = 'courses';		
		$this->_wheres = $where;
		$this->_joins = [];
		$course = (array) $this->getOneRecord();
		$courseDetail = (array) $this->getCourseDetail($whereDetail);	
		$data = (object) array_merge($course, $courseDetail);
        return $data;
	}
	/**
	 * [getCourseDetail description]
	 * @param  [type] $where [description]
	 * @return [type]        [description]
	 */
	public function getCourseDetail( $where )
	{
		$this->_table = 'course_details';
		$this->_selectItem = 'course_details.course_id, course_details.name, course_details.slug, course_details.description, course_details.meta_keywords, course_details.meta_description, course_details.teacher, course_details.price, course_details.discount, course_details.origin_price, course_details.manager_id, course_details.title';
		$this->_wheres = $where;
		$this->_joins = [];
		return $this->getOneRecord();
	}
	/**
	 * [getParentCourse description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getParentCourse( $id )
	{
		$this->resetQuery();
		$this->_table = 'course_details';
		$this->_selectItem = 'course_details.name as parent_name';
		$where = array('course_details.course_id' => $id);
		$this->_wheres = $where;
		$result = (array) $this->getOneRecord();		
		return $result;
	}
	/**
	 * [getcourses description]
	 * @param  array   $where   [description]
	 * @param  integer $limit   [description]
	 * @param  integer $offset  [description]
	 * @param  array   $orderBy [description]
	 * @param  array   $likes   [description]
	 * @return [type]           [description]
	 */
	public function getcourses($where = array(), $limit = 10, $offset = 0, $orderBy = array(), $likes = array())
	{		
		$this->resetQuery();
		$this->_selectItem = "courses.id, courses.image, courses.subject_id, courses.course_code, courses.parent, courses.created, courses.ordering, courses.status, courses.start_date, courses.end_date, course_details.name, course_details.slug, course_details.lang_id, course_details.teacher, course_details.description, course_details.meta_description, course_details.meta_keywords, course_details.price, course_details.discount, course_details.origin_price, course_details.manager_id, course_details.title";
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_from = 'courses';
		$joins = array();
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'courses.id = course_details.course_id';
		$joinCourseDetail['type'] = 'right';
		$joins[] = $joinCourseDetail;
		$this->_joins = $joins;		
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		$course = $this->getListRecords();
		$this->resetQuery();	
		$data = array();
		foreach($course as $item){
			$items = (array) $item;
			$where = array('course_details.course_id' => $item->id);		
			if($item->parent > 0){
				$parent = $this->getParentCourse($item->parent);
				if($parent){
					$items = array_merge((array) $items, $parent);
				}
			}
			$child = $this->totalCourse(['courses.parent' => $item->id]);
			$items['child'] = $child;
			$data[] = (object) $items;				
		}
		$this->resetQuery();
		return $data;
	}

/**
 * [getCourseOption description]
 * @param  integer $parentId [description]
 * @param  string  $str      [description]
 * @return [type]            [description]
 */
	public function getCourseOption($parentId = 0, $currentId = 0, $str = '') {
	  $courses = array();
	  $html = '';
	  $this->db->select('courses.id, courses.parent, course_details.name');
	  $this->db->from('courses');
	  $this->db->join('course_details', 'courses.id = course_details.course_id', 'left');
	  $this->db->where('courses.parent', $parentId);
	  $result = $this->db->get()->result();	  
	  $selected = '';
	  foreach ($result as $item) {	   		
	  		$name = $str.$item->name;

	  		if($currentId  === $item->id) {
	  			$html .= '<option selected = "selected" data-parent="'.$item->parent.'" value="' . $item->id . '">' . $name . '</option>';
	  		} else {
	  			$html .= '<option data-parent="'.$item->parent.'" value="' . $item->id . '">' . $name . '</option>';
	  		}
	  		
	  		$html .= $this->getCourseOption($item->id, $currentId, $str.'|---');
	  }	 
	  return $html;
	}

	public function getChildId($id)
	{
	  $this->db->select('courses.id');
	  $this->db->from('courses');	  
	  $this->db->where('courses.parent', $id);
	  $result = $this->db->get()->result();
	  $args = [];
	  foreach ($result as $item) {
	  	$args[] = (int) $item->id;
	  }
	  return $args;	 		
	}

	public function totalCourseUser($where = [], $likes = [])
	{		
		$this->_table = 'user_course';
		$joins = [];
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'course_details.course_id = user_course.course_id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;

		$joinUser['table'] = 'users';
		$joinUser['condition'] = 'user_course.user_id = users.id';
		$joinUser['type'] = 'left';
		$joins[] = $joinUser;
		$this->_joins = $joins;
		$this->_wheres = $where;
		return $this->totalRecord();		
	}

	public function getCourseUsers($where = [], $limit = 10, $offset = 0, $likes = [], $orderBy = [])
	{		
		$this->_selectItem = "(SELECT count(user_course.id) FROM user_course WHERE user_course.user_id=users.id) as courseCount, users.id as user_id, course_details.name as course_name, course_details.price, course_details.origin_price, users.username, users.amount, course_details.discount, users.total_point, users.full_name, users.phone, users.email, users.total_exam_question, users.total_exam_question_true";
		if (isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}

		$this->_from = 'user_course';
		$joins = [];
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'course_details.course_id = user_course.course_id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;

		$joinUser['table'] = 'users';
		$joinUser['condition'] = 'user_course.user_id = users.id';
		$joinUser['type'] = 'left';
		$joins[] = $joinUser;		
		$this->_joins = $joins;		
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		return $this->getListRecords();		
	}	

    public function deleteUserInCourse($where = [])
    {
    	$this->_table = 'user_course';
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }
    	
}