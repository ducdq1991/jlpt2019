<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> <?php echo lang('notification_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-left btn-function" style="display:none;">
                        <a class="changeView btn btn-danger btn-sm" url="<?php echo base_url().'notification/changeView'; ?>">
                        <i class="fa fa-circle-o"></i> <?php echo lang('readed');?></a>
                        <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'notification/changeFlag'; ?>"><i class="fa fa-times"></i> <?php echo lang('hidden');?></a> 
                    </div>    
                    <div class="pull-right form-notification-action">
                        <?php echo lang('num_records');?>:
                        <select name="numRecord" class="form-control input-sm sys-filler">                                
                            <option value="">--<?php echo lang('num_rows');?>--</option>
                            <?php foreach($itemPerPages as $item):?>
                            <option value="<?php echo $item;?>" <?php if($numRecord == $item):?> selected="selected" <?php endif;?>><?php echo $item;?></option>
                            <?php endforeach;?>
                        </select>                         
                        <input type="text" class="form-control input-sm sys-filler" placeholder="Tìm kiếm" name="keyword" value="<?php echo $keyword;?>">
                        <select name="type" class="form-control input-sm sys-filler">
                            <option value="">Loại</option>
                            <?php $notificationType = $this->config->item('notificationType'); ?>
                            <?php foreach($notificationType as $key => $item) :?>
                            <option value="<?php echo $key; ?>" <?php if($key == $type):?> selected = "selected" <?php endif;?>><?php echo $item; ?></option>
                            <?php endforeach; ?>
                        </select>  
                        <select name="view_flag" class="form-control input-sm sys-filler">
                            <option value="">Tất cả</option>
                            <option value="read" <?php if('read' == $view_flag):?> selected = "selected" <?php endif;?>>Đã đọc</option>
                            <option value="unread" <?php if('unread' == $view_flag ):?> selected = "selected" <?php endif;?>>Chưa đọc</option>
                        </select>                              
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                        <a href="<?php echo base_url();?>notification/send" class="btn btn-primary btn-sm">
                        <i class="fa fa-share" aria-hidden="true"></i> <?php echo lang('send');?></a>
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('content');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('type');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('date_time');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!is_null($notifies)) :?>
                            <?php foreach ($notifies as $row) :?>
                                <?php if($row->view_flag == 1) : ?>
                                <tr class="gradeA odd thick" role="row">
                                <?php else: ?>
                                <tr class="gradeA odd thin" role="row">
                                <?php endif; ?>
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->content;?></td>
                                    <?php $notiType = $this->config->item('notificationType'); ?>
                                    <td><?php echo $notiType[$row->type];?></td>
                                    <td>
                                        <?php echo date('d/m/Y H:i:s', strtotime($row->created)); ?>
                                    </td>
                                    <td class="center">
                                        <?php if($row->view_flag == 1) : ?>
                                        <a onclick="changeView('<?php echo base_url('notification/changeView'); ?>', <?php echo $row->id; ?>)" title="Đánh dấu đã đọc">
                                            <button class="btn btn-xs btn-default" type="button">
                                                 <i class="fa fa-circle-o"></i>
                                            </button>
                                        </a>
                                        <?php endif; ?>
                                        <a onclick="changeState('<?php echo base_url('notification/changeFlag'); ?>', <?php echo $row->id; ?>, 0)" title="Ẩn">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-times-circle-o"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <div class="paging">
                        <?php echo $paging;?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
