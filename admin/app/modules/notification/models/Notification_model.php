<?php defined('BASEPATH') or exit('No direct script access allowed');

class Notification_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'user_logs';
    }
    public function getNotifications( $where = [], $where_not_in = [], $limit = 0, $offset = 0, $likes = [] )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        if(!empty($where_not_in)) {
            $this->_where_not_in = $where_not_in;
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
	 * [totalNotifications]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalNotifications($where = [], $where_not_in = [])
	{
		$this->_wheres = $where;
        $this->_where_not_in = $where_not_in;
		return $this->totalRecord();
	}
    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = [] )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeView( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
}