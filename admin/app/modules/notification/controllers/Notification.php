	<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Notification
* @author ChienDau
* @copyright Bigshare .,JSC
* 
*/

class Notification extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('notification_model');
        $this->lang->load('notification', $this->session->userdata('lang'));
	}

	public function index($page = 0)
	{
		$data = $where = $where_not_in = $likes = array();
       	$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $where['user_logs.assigner_flag'] = 1;
        $where['user_logs.assigner'] = 0;
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
		$view_flag = $this->input->get('view_flag');
		$data['view_flag'] = $view_flag;
		if($view_flag != ''){
		  $where['user_logs.view_flag'] = ($view_flag == 'read') ? 0 : 1;
		}
        if(!empty($keyword)){
            $likes['field'] = 'user_logs.content';
            $likes['value'] = $keyword;
        }
        $where_not_in = [
        	'field' => 'type',
        	'value' => [LOG_IN, LOG_OUT, PURCHASE_COURSE, PAYMENT, RECHARGE, PURCHASE_LESSON],
        ];
        $type = $this->input->get('type');
        $data['type'] = $type;
		if($type != ''){
			$where['user_logs.type'] = (int) $type;
			$where_not_in = [];
		}
        $numRecords = $this->notification_model->totalNotifications( $where, $where_not_in );      
		$data['paging'] = paging(base_url() .'notification', $page, $numRecords, $limit); 
		$notifies = $this->notification_model->getNotifications( $where, $where_not_in, $limit, $offset, $likes );
        $data['notifies'] = $notifies;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');
        $this->load->view('layouts/header');
        $this->load->view('notification/index', $data);
        $this->load->view('layouts/footer');

	}
	/**
     * @name changeStatus
     * @desc change status a Module from modules table
     * */
    public function changeFlag()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['user_logs.assigner_flag'] = $status;	
				if($this->notification_model->changeStatus( $ids, $args))
				{
                    $message = sprintf(lang('message_change_success'), lang('notification'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('notification'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
			}
		} else {
			$args['user_logs.assigner_flag'] = 0;
			if( $this->notification_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('notification'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('notification'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;	
		}			
	}
	/**
     * @name changeView
     * @desc change status a Module from modules table
     * */
    public function changeView()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$view_flag = (int) $this->input->post('view_flag');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['view_flag'] = $view_flag;	
				$this->notification_model->changeView( $ids, $args);
                echo 1;
                die;
			}
		} else {	
			$args['view_flag'] = 0;		
			$this->notification_model->changeView( $ids, $args );
            echo 1;
            die;	
		}			
	}
	public function send($page = 0)
	{
		$user = $this->session->userdata('user');
		$data = $where = $where_not_in = $likes = array();
       	$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $where['user_logs.user_flag'] = 1;
        $where['user_logs.user_id'] = $user->id;
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
		$view_flag = $this->input->get('view_flag');
		$data['view_flag'] = $view_flag;
		if($view_flag != ''){
		  $where['user_logs.view_flag'] = ($view_flag == 'read') ? 0 : 1;
		}
        if(!empty($keyword)){
            $likes['field'] = 'user_logs.content';
            $likes['value'] = $keyword;
        }
        $where_not_in = [
        	'field' => 'type',
        	'value' => [LOG_IN, LOG_OUT],
        ];
        $type = $this->input->get('type');
        $data['type'] = $type;
		if($type != ''){
			$where['user_logs.type'] = (int) $type;
			$where_not_in = [];
		}
        $numRecords = $this->notification_model->totalNotifications( $where, $where_not_in );      
		$data['paging'] = paging(base_url() .'notification', $page, $numRecords, $limit); 
		$notifies = $this->notification_model->getNotifications( $where, $where_not_in, $limit, $offset, $likes );
        $data['notifies'] = $notifies;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');
        $this->load->view('layouts/header');
        $this->load->view('notification/send', $data);
        $this->load->view('layouts/footer');

	}
	public function changeSendFlag()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['user_logs.user_flag'] = $status;	
				if($this->notification_model->changeStatus( $ids, $args))
				{
                    $message = sprintf(lang('message_change_success'), lang('notification'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('notification'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
			}
		} else {
			$args['user_logs.user_flag'] = 0;
			if( $this->notification_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('notification'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('notification'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;	
		}			
	}
}