<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-money"></i> Lịch sử mua khóa học</h5>
            </div>
            <div class="ibox-content">
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="form-notification-action">
                        <?php echo lang('num_records');?>:
                        <select class="rowPerPage form-control input-sm sys-filler">
                        <?php foreach($itemPerPages as $key=>$value):?>
                            <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                            <?php endforeach;?>
                        </select>
                        <div class="input-group date datepicker sys-filler" id="datetimepicker-payment-1" style="width: 250px;">
                            <span class="input-group-addon">Từ ngày</span>
                            <input type="text" name="fromDate" value="<?php if ($fromDate != NULL) echo $fromDate;?>" class="form-control input-sm sys-filler">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="input-group date datepicker sys-filler" id="datetimepicker-payment-2" style="width: 250px;">
                            <span class="input-group-addon">Đến ngày</span>
                            <input type="text" name="toDate" value="<?php if ($toDate != NULL) echo $toDate;?>" class="form-control input-sm sys-filler">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </span>
                        </div>                                                
                        <select class="form-control input-sm sys-filler" name="receiver" style="width: 200px;">
                            <option value="">--Chọn người phụ trách--</option>
                            <?php foreach ($teachers as $teacher) :?>
                            <option <?php if($receiver == $teacher->id): ?> selected="selected" <?php endif;?>  value="<?php echo $teacher->id;?>"><?php echo $teacher->username;?> <?php echo $teacher->full_name;?> (<?php echo $teacher->email;?>)</option>
                            <?php endforeach;?>  
                        </select>
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                        <div class="form-group">
                            Tổng tiền: <strong><?php echo number_format($totalPrice);?></strong> VNĐ
                        </div>                                                                   
                        
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1">Tiêu đề</th>
                                <th tabindex="0" rowspan="1" colspan="1">Thời gian</th>
                                <th tabindex="0" rowspan="1" colspan="1">Khóa học</th>
                                <th tabindex="0" rowspan="1" colspan="1">ID khóa học</th>
                                <th tabindex="0" rowspan="1" colspan="1">Người mua</th>
                                <th tabindex="0" rowspan="1" colspan="1">Người nhận</th>
                                <th tabindex="0" rowspan="1" colspan="1">Số tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            if (isset($payments) && count($payments) > 0) : 
                                foreach ($payments as $item) :
                        ?>
                            <tr>
                                <td><?php echo $item->payment_id;?></td>
                                <td><?php echo $item->name;?></td>
                                <td><?php echo date('d/m/Y H:i:s', strtotime($item->created_time));?></td>
                                <td><?php echo $item->c_name;?></td>
                                <td><?php if (isset($item->data_id)) echo $item->data_id;?></td>
                                <td><?php echo $item->py_username;?> (<?php echo $item->py_full_name;?>)</td>
                                <td><?php echo $item->rc_username;?> (<?php echo $item->rc_full_name;?>)</td>
                                <td><?php echo number_format($item->price);?></td>
                            </tr>
                        <?php 
                                endforeach;
                            endif;
                        ?>     
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="paging col-md-8">
                            <?php echo $paging;?>
                        </div>
                        <div class="col-md-4">
                            Tổng số bản ghi: <?php echo number_format($totalCount);?>
                        </div>                        
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
