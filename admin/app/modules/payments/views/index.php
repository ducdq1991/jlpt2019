<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-money"></i> Lịch sử thanh toán</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-left btn-function" style="display:none;">
                        <a class="changeView btn btn-danger btn-sm" url="<?php echo base_url().'payments/changeview'; ?>">
                            <i class="fa fa-circle-o"></i> Đánh dấu đã đọc
                        </a>
                        <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'payments/changeflag'; ?>">
                            <i class="fa fa-times"></i> Ẩn
                        </a> 
                    </div>
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="pull-right form-notification-action">
                        <?php echo lang('num_records');?>:
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                          
                        <input type="text" class="form-control input-sm sys-filler" placeholder="Tìm theo mã thành viên" name="keyword_name" value="<?php echo $keyName;?>">
                        <select name="type" class="form-control input-sm sys-filler">
                            <option value="">Loại</option>
                            <?php $paymentType = $this->config->item('paymentType'); ?>
                            <?php foreach($paymentType as $key => $item) :?>
                            <option value="<?php echo $key; ?>" <?php if($key == $type):?> selected = "selected" <?php endif;?>><?php echo $item; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="input-group date" id="datetimepicker-payment">
                            <input type="text" name="date" value="<?php echo $dateSearch;?>" class="form-control input-sm sys-filler">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </span>
                        </div>
                        <select name="view_flag" class="form-control input-sm sys-filler">
                            <option value="">Tất cả</option>
                            <option value="read" <?php if('read' == $view_flag):?> <?php endif;?>>Đã đọc</option>
                            <option value="unread" <?php if('unread' == $view_flag ):?><?php endif;?>>Chưa đọc</option>
                        </select>                              
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                    </div>
                    <div class="row">
                    <div class="col-md-12 padding-middle text-right">
                            <?php 
                                if (count($totalCount) > 0) {                                        
                                    echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                }
                            ?>
                    </div>                         
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1">Nội dung</th>
                                <th tabindex="0" rowspan="1" colspan="1">Loại</th>
                                <th tabindex="0" rowspan="1" colspan="1">Thời gian</th>
                                <th tabindex="0" rowspan="1" colspan="1">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( $payments ) : ?>
                                <?php foreach( $payments as $row ) : ?>
                                    <?php if($row->view_flag == 1) : ?>
                                        <tr class="gradeA odd thick" role="row">
                                    <?php else: ?>
                                        <tr class="gradeA odd thin" role="row">
                                    <?php endif; ?>
                                            <td class="sorting_1">
                                                <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                            </td>                                    
                                            <td class="sorting_1"><?php echo $row->id;?></td>
                                            <td><?php echo $row->content;?></td>
                                            <?php $paymentType = $this->config->item('paymentType'); ?>
                                            <td><?php echo $paymentType[$row->type];?></td>
                                            <td><?php echo date('d/m/Y H:i:s', strtotime($row->updated));?></td>
                                            <td class="center">
                                                <?php if($row->view_flag == 1) : ?>
                                                    <a onclick="changeView('<?php echo base_url('payments/changeview'); ?>', <?php echo $row->id; ?>)" title="Đánh dấu đã đọc">
                                                        <button class="btn btn-xs btn-default" type="button">
                                                             <i class="fa fa-circle-o"></i>
                                                        </button>
                                                    </a>
                                                    <?php endif; ?>
                                                    <a onclick="changeState('<?php echo base_url('payments/changeflag'); ?>', <?php echo $row->id; ?>, 0)" title="Ẩn">
                                                        <button class="btn btn-xs btn-default" type="button">
                                                            <i class="fa fa-times-circle-o"></i>
                                                        </button>
                                                    </a>
                                            </td>
                                        </tr>
                                <?php endforeach; ?>        
                            <?php endif; ?>        
                        </tbody>
                    </table>
                    <div class="paging">
                        <?php echo $paging;?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
