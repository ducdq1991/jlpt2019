<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends Bigshare_Model {

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'user_logs';
    }

    public function totalCoursePayments($where = [], $likes = [])
    {
        $this->_wheres = $where;
        $this->_from = 'payments as PM';
        $this->_table = 'payments as PM';
        $this->_likes = $likes;

        $joins = [];
        $creator = [];
        $creator['table'] = 'users as NM';
        $creator['condition'] = 'NM.id = PM.payer';
        $creator['type'] = 'left';
        $joins[] = $creator;

        $receiver = [];
        $receiver['table'] = 'users as NN';
        $receiver['condition'] = 'NN.id = PM.receiver';
        $receiver['type'] = 'left';
        $joins[] = $receiver;

        $course = [];
        $course['table'] = 'course_details as C';
        $course['condition'] = 'C.course_id = PM.data_id';
        $course['type'] = 'left';
        $joins[] = $course;

        $this->_joins = $joins;
        
        return $this->totalRecord();
    }

    public function totalPrice($where = [], $likes = [])
    {
        $this->_selectItem = 'SUM(PM.price) as total_price';
        $this->_wheres = $where;
        $this->_from = 'payments as PM';
        $this->_table = 'payments as PM';
        $this->_likes = $likes;

        $joins = [];
        $creator = [];
        $creator['table'] = 'users as NM';
        $creator['condition'] = 'NM.id = PM.payer';
        $creator['type'] = 'left';
        $joins[] = $creator;

        $receiver = [];
        $receiver['table'] = 'users as NN';
        $receiver['condition'] = 'NN.id = PM.receiver';
        $receiver['type'] = 'left';
        $joins[] = $receiver;

        $course = [];
        $course['table'] = 'course_details as C';
        $course['condition'] = 'C.course_id = PM.data_id';
        $course['type'] = 'left';
        $joins[] = $course;

        $this->_joins = $joins;
        
        $item = $this->getOneRecord();
        if (is_object($item) && $item->total_price > 0) {
            return $item->total_price;
        }
        return 0;
    }


    public function getCoursePayments($where = [], $likes = [], $limit = 0, $offset = 0, $order = [])
    {
        $this->_selectItem = 'PM.payment_id, PM.price, PM.name, PM.content, PM.created_time, PM.payer, PM.receiver, NN.username as rc_username, NN.full_name as rc_full_name, NM.username as py_username, NM.full_name as py_full_name, C.name as c_name';
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'payments as PM';
        $this->_order = $order;
        $this->_wheres = $where;

        $joins = [];
        $creator = [];
        $creator['table'] = 'users as NM';
        $creator['condition'] = 'NM.id = PM.payer';
        $creator['type'] = 'left';
        $joins[] = $creator;

        $receiver = [];
        $receiver['table'] = 'users as NN';
        $receiver['condition'] = 'NN.id = PM.receiver';
        $receiver['type'] = 'left';
        $joins[] = $receiver;

        $course = [];
        $course['table'] = 'course_details as C';
        $course['condition'] = 'C.course_id = PM.data_id';
        $course['type'] = 'left';
        $joins[] = $course;

        $this->_joins = $joins;

        return $this->getListRecords();
    }

    /**
    * [get payment history]
    **/
    public function getPaymentHistory( $where = [], $whereIn = [], $limit = 0, $offset = 0 )
    {
        if(!empty($whereIn)) {
           $this->_batchConditionField = 'type';
           $this->_batchConditionValue = $whereIn;
        }
        $this->_order = ['user_logs.updated' => 'DESC'];
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
    * [total payment history]
    **/
    public function totalPayment( $where = [], $whereIn = [] )
    {
        try {
            return $this->db->where($where)->where_in('type', $whereIn)->count_all_results($this->_table);
        } catch( Exception $ex ){
            $this->writeLog($ex->getMessage());
        }
    }
    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = [] )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    /**
     * @name changeStatus
     * @desc change status a row or any rows from modules table
     * @param [array] $where
     * */
    public function changeView( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getBatch($where = [])
    {
        $this->_offset = 0;
        $this->_limit = 0;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }

    public function getPayment($where = []) 
    {
        $this->_table = 'payments';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }

    public function addPayment($args) 
    {
        $this->_table = 'payments';
        $this->_data  = $args;
        return $this->addRecord();        
    }

    public function getCourseManager($where = []) 
    {
        $this->_table = 'course_details';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }


    public function getPaymentList($where = [])
    {
        $this->_offset = 0;
        $this->_limit = 0;
        $this->_from = 'payments';
        $this->_wheres = $where;
        $this->_order = ['payment_id' => 'DESC'];
        return $this->getListRecords();
    }

}