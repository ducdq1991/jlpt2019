<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('payment_model');		
		$this->load->model('user/user_model');
	}

	public function batch()
	{
		$data = $this->payment_model->getBatch(['user_logs.type' => 15]);
		foreach ($data as $item) {
			$args = [];
			$args['name'] = 'Mua khóa học';
			$args['content'] = $item->content;
			$args['payer'] = (int) $item->user_id;
			$args['type'] = 'course';
			$args['data_id'] = (int) $item->data;
			$args['created_time'] = $item->created;
			$args['price'] = 0; 
			$course = $this->payment_model->getCourseManager(['course_details.course_id' => $args['data_id']]);
			$args['receiver'] = isset($course->manager_id) ? $course->manager_id : NULL;
			$where = ['payments.data_id' => $args['data_id'], 'payer' => $args['payer']];
			$payment = $this->payment_model->getPayment($where);
			if ($payment == NULL) {
				$this->payment_model->addPayment($args);
			}
		}
	}

	public function courses($page = 0)
	{
		$data = $where = $likes = [];
		$this->load->helper('paging');
		//Get Teachers
		$condTecher = ['IN' => ['key' => 'users.group_id', 'value' => [21,24]]];
		$teachers = $this->user_model->getTechers($condTecher);
		$data['teachers'] = $teachers;

				
        $limit = $this->config->item('per_page');
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;

		$where['PM.type'] =  'course';
		$order = ['PM.created_time' => 'desc'];	
		$fromDate = $this->input->get('fromDate');
		$toDate = $this->input->get('toDate');
		if ($toDate != NULL) {
			$where['PM.created_time <='] = date('Y-m-d', strtotime($toDate)) . ' 23:59:59';
		}
		$data['toDate'] = $toDate;
		if ($fromDate != NULL) {
			$where['PM.created_time >='] = date('Y-m-d', strtotime($fromDate)) . ' 00:00:00';			
		}
		$data['fromDate'] = $fromDate;

		$where['(NN.group_id = 21 OR NN.group_id = 24)'] = NULL;

		$receiver = $this->input->get('receiver');
		if ($receiver != NULL) {
			$where['PM.receiver'] = (int) $receiver;
		}
		$data['receiver'] = $receiver;

		$totalPrice = $this->payment_model->totalPrice($where);
		$data['totalPrice'] = $totalPrice;
        $total = $this->payment_model->totalCoursePayments($where);
        
        $payments = $this->payment_model->getCoursePayments($where, $likes, $limit, $offset, $order);
        $data['payments'] = $payments;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($payments);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;        

        $queryString = $_SERVER['QUERY_STRING'];
        $data['paging'] = paging(base_url() .'payments/courses', $page, $total, $limit, $queryString);
		//load view
        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Thanh toán', base_url().'payments');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 		
		$this->load->view('layouts/header', $head);
		$this->load->view('course', $data);
		$this->load->view('layouts/footer');
	}

	/**
	* [list payment history]
	**/
	public function index( $page = 0 )
	{		
		$data = $where = $whereIn = [];
		$this->load->helper('paging');
		$where['user_logs.assigner_flag'] = 1;
        $where['user_logs.assigner'] = 0;
		$whereIn = [PURCHASE_LESSON, RECHARGE, PAYMENT, PURCHASE_COURSE, BONUS_NORMAL, MINUS_BONUS_NORMAL];
        $limit = $this->config->item('per_page');
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
		//search name user
		$keyName = $this->input->get('keyword_name');
		$data['keyName'] = $keyName;
		if( !empty($keyName) ){
            $where['user_logs.user_id'] = (int)$keyName;
        }
		// search course or lesson
		$type = $this->input->get('type');
        $data['type'] = $type;        
        if($type != ''){
			$where['user_logs.type'] = (int) $type;
		}
		//search date payment
		$dateSearch = $this->input->get('date');
		if( !empty($dateSearch) ) {
			$where['user_logs.created >='] = date('Y-m-d H:i:s', strtotime($dateSearch));
			$where['user_logs.created <'] = date('Y-m-d H:i:s', (strtotime($dateSearch) + 24*60*60));
		}
		$data['dateSearch'] = $dateSearch;
		//search read and hidden
		$view_flag = $this->input->get('view_flag');
		$data['view_flag'] = $view_flag;
		if($view_flag != ''){
		  $where['user_logs.view_flag'] = ($view_flag == 'read') ? 0 : 1;
		}
        $total = $this->payment_model->totalPayment( $where, $whereIn );
        
        $payments = $this->payment_model->getPaymentHistory(  $where, $whereIn, $limit, $offset );
        $data['payments'] = $payments;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($payments);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;        

        $queryString = $_SERVER['QUERY_STRING'];
        $data['paging'] = paging(base_url() .'payments', $page, $total, $limit, $queryString);
		//load view
        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Thanh toán', base_url().'payments');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 		
		$this->load->view('layouts/header', $head);
		$this->load->view('payments/index', $data);
		$this->load->view('layouts/footer');
	}
	/**
     * @name changeStatus
     * @desc change status a Module from modules table
     * */
    public function changeFlag()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['user_logs.assigner_flag'] = $status;	
				if($this->payment_model->changeStatus( $ids, $args))
				{
                    $this->session->set_flashdata('success', 'Ẩn thành công');
                } else {
                    $this->session->set_flashdata('error', 'Ẩn thất bại');
                }
                echo 1;
                die;
			}
		} else {
			$args['user_logs.assigner_flag'] = 0;
			if( $this->notification_model->changeStatus( $ids, $args ) )
			{
                $this->session->set_flashdata('success', 'Ẩn thành công');
            } else {
                $this->session->set_flashdata('error', 'Ẩn thất bại');
            }
            echo 1;
            die;	
		}			
	}
	/**
     * @name changeView
     * @desc change status a Module from modules table
     * */
    public function changeView()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$view_flag = (int) $this->input->post('view_flag');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['view_flag'] = $view_flag;	
				$this->payment_model->changeView( $ids, $args);
                echo 1;
                die;
			}
		} else {	
			$args['view_flag'] = 0;		
			$this->payment_model->changeView( $ids, $args );
            echo 1;
            die;	
		}			
	}

	public function buildPayouts()
    {    	
    	$this->load->model('salemans/salemans_model');

        $payments = $this->payment_model->getPaymentList();
        foreach ($payments as $payment) {

        	if (!is_null($payment->coupon_code) && $payment->type == 'course') {

        		$where = [
					'coupon_code' => $payment->coupon_code
				];
				$saleman = $this->salemans_model->getSaleman($where);
				$args = ['amount' => (float) ($saleman->amount + $payment->discount)];

				if ($saleman) {
	        		$payout = [];
	                $payout['name'] = 'Cộng tiền CTV KH #' . $payment->data_id . '-UID' . $payment->payer;
	                $payout['note'] = 'KH' . $payment->data_id . '-UID' . $payment->payer;
	                $payout['amount'] = $payment->discount;
	                $payout['receiver_id'] = $saleman->id;
	                $payout['receiver_name'] = $saleman->name . '-' . $saleman->coupon_code;
	                $payout['data_id'] = $payment->data_id;
	                $payout['type'] = 'plus';
	                $payout['pay_group'] = 'course';
	                $payout['created_at'] = date('Y-m-d H:i:s');
	                if ($this->checkPayout($payout)) {
	                    $this->salemans_model->updateSaleman($where, $args);
	                    $this->setPayout($payout);
	                }
            	}
        	}
        	
        }
    }
}