<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salemans extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('salemans_model');
        $this->load->model('payout_model');     
	}
    
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'salemans.name';
            $likes['value'] = $keyword;
        }
        $orderCondition['id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if( $status != '' ){
          $where['salemans.status'] = $status;
        }
        $total = $this->salemans_model->totalSaleman($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'salemans', $page, $total, $limit, $queryString);
        $salemans = $this->salemans_model->getSalemans($where, $limit, $offset, $orderCondition, $likes);
        $data['salemans'] = $salemans;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($salemans);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Cộng tác viên', base_url().'salemans');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
    }

    public function add()
    {
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên cộng tác viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Tên cộng tác viên là bắt buộc.',
                    'is_unique' => 'Cộng tác viên này đã tồn tại.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Cộng tác viên', base_url().'salemans');
            $this->breadcrumbs->add('Thêm mới', base_url().'salemans/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('add', $data);
            $this->load->view('layouts/footer');
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);   
            $postData['password'] = md5('ctv123456@');         
            $id = $this->salemans_model->addSaleman($postData);
            if($id) {
                $code = $this->generateCode($id, $postData['name']);            
                $this->salemans_model->updateSaleman(['id' => $id],['coupon_code' => $code]);
                $message = 'Thêm cộng tác viên thành công!';
                $messageType = 'success';                
            } else {  
                $message = 'Lỗi! Không thêm được cộng tác viên';                
            }
            $this->session->set_flashdata($messageType, $message);

            redirect(base_url() . 'salemans');
        }
    }


    protected function generateCode($userId = 0, $name = '', $prefix = 'KH')
    {               
        $code = null;
        if ($userId > 0) {
            $code = '';
            if ($name != '') {
                $name = sanitizeTitle($name);
                $arrName = @explode('-', $name);                
                for ($i = 0; $i < count($arrName); $i++) {
                    $code .= strtoupper($arrName[$i][0]);
                }               
            } else {
                $code .= $prefix;
            }
            $code .= $userId;
        }

        return $code;
    }

    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if(empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'salemans');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->salemans_model->deleteSaleman($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa cộng tác viên thành công.');
            } else {
                $this->session->set_flashdata('error', 'Không xóa được cộng tác viên');
            }
            echo 1;
            die;
        } else {
            $query = $this->salemans_model->deleteSaleman( array( 'id' => (int) $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa cộng tác viên thành công.');
            } else {                
                $this->session->set_flashdata('error', 'Không xóa được cộng tác viên');
            }
            if( $id ) {
                redirect(base_url() . 'salemans');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'salemans.id' => (int) $id,
            );
        $saleman = $this->salemans_model->getSaleman($where);        
        if (empty($saleman)) {            
            $this->session->set_flashdata('error', 'Cộng tác viên không tồn tại');
            redirect( base_url() . 'salemans');
        }
        $postData = $this->input->post();

        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $data['saleman'] = $saleman;
        $this->load->library('form_validation');
        $validateName = $validateSlug = '';
        if (isset($postData['name'])) {
            if ($saleman->name != $postData['name']) {
                $validateName = '|is_unique[salemans.name]';
            }
        }

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên cộng tác viên',
                'rules' => 'trim|required' . $validateName,
                'errors' => array(
                    'required' => 'Tên cộng tác viên là bắt buộc.',
                    'is_unique' => 'Cộng tác viên này đã tồn tại',
                ),
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Cộng tác viên', base_url().'salemans');
            $this->breadcrumbs->add($saleman->name, base_url().'salemans/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('edit', $data);
            $this->load->view('layouts/footer');
        } else {                        
            if( $this->salemans_model->updateSaleman($where, $postData ) ) {
                $this->session->set_flashdata('success', 'Đã cập nhật cộng tác viên.');
            } else {                    
                $this->session->set_flashdata('error', 'Không cập nhật được cộng tác viên.');
            }

            redirect(base_url() . 'salemans/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->salemans_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }


    public function history($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'payouts.name';
            $likes['value'] = $keyword;
        }
        $orderCondition['id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $total = $this->payout_model->totalRecord($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'salemans/history', $page, $total, $limit, $queryString);
        $salemans = $this->payout_model->getPayouts($where, [], [], $orderCondition, $limit, $offset);
        $data['salemans'] = $salemans;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($salemans);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Cộng tác viên', base_url().'salemans');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('history', $data);
        $this->load->view('layouts/footer');
    }


    public function payment()
    {
        $data = array();

        $salemans = $this->salemans_model->getSalemans([], 0);
        $data['salemans'] = $salemans;

        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'receiver_id',
                'label' => 'Cộng tác viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Vui lòng chọn CTV.',
                ),
            ),

            array(
                'field' => 'amount',
                'label' => 'Số tiền cần chi',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Vui lòng nhập số tiền cần chi',
                ),
            ),

            array(
                'field' => 'note',
                'label' => 'Lý do chi',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Vui lòng nhập lý do chi.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Cộng tác viên', base_url().'salemans');
            $this->breadcrumbs->add('Thêm mới', base_url().'salemans/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('payment', $data);
            $this->load->view('layouts/footer');
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);
            $postData['type'] = 'minus';
            $postData['created_at'] = date('Y-m-d H:i:s');
            $postData['pay_group'] = 'payout';
            $postData['data_id'] = NULL;
            $postData['name'] = 'Trả tiền cho CTV #' . $postData['receiver_id'];
            $postData['created_at'] = date('Y-m-d H:i:s');
            $postData['amount'] = (float) $postData['amount'];
            $saleman = $this->salemans_model->getSaleman(['salemans.id' => $postData['receiver_id']]);
            if ($saleman) {
                $postData['receiver_name'] = $saleman->name . '-' . $saleman->coupon_code;
                if ($postData['amount'] <= $saleman->amount) {
                    if ($this->payout_model->addPayout($postData)) {
                        $amount = $saleman->amount - $postData['amount'];
                        $this->salemans_model->updateSaleman(['salemans.id' => $saleman->id], ['amount' => $amount]);
                        $message = 'Đã tạo phiếu chi cho CTV: ' . $saleman->name . '-' . $saleman->coupon_code;
                        $messageType = 'success';
                    } else {
                        $message = 'Lỗi! Không thể tạo được phiếu chi cho CTV: ' . $saleman->name . '-' . $saleman->coupon_code;
                    }
                } else {
                    $message = 'Lỗi! Số tiền cần chi phải nhỏ hoăn bằng hoặc số dư của CTV hiện tại là: ' . number_format($saleman->amount);
                    $this->session->set_flashdata('error', $message);
                    redirect(base_url() . 'salemans/payment');
                }
                
            } else {
                $message = 'Lỗi! CTV không tồn tại.';
            }

            $this->session->set_flashdata($messageType, $message);

            redirect(base_url() . 'salemans');
        }
    }

}