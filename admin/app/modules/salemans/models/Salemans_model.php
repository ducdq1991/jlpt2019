<?php defined('BASEPATH') or exit('No direct script access allowed');

class Salemans_model extends Bigshare_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->_table = 'salemans';
    }
    
    public function addSaleman($args = []) 
    {
        $this->_data  =$args;
        return $this->addRecord();
    }
    
    
    public function updateSaleman($where = [], $args = [])
    {
        $this->_table = 'salemans';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getSalemans($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {
        $this->_selectItem = '*';
        $this->_from = 'salemans';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;       
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }

    public function totalSaleman($where = [])
    {
        $this->_wheres = $where;
        return $this->totalRecord();
    }


    public function deleteSaleman($where = [], $multiple = false)
    {
        
        $this->_multiple = $multiple;
        $this->_wheres = $where;
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }

    public function changeStatus($ids, $args = [])
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int) $ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getSaleman($where = [])
    {
        $this->_selectItem = '*';
        $this->_wheres = $where;
        $this->_table = 'salemans';
        return $this->getOneRecord();
    }
}