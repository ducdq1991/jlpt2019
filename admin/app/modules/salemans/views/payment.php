<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-name">
                <h5><i class="fa fa-plus"></i> Tạo phiếu chi</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Tên CTV<span> *</span></label>
                            <div class="col-sm-10">
                                <select name="receiver_id" class="select2 form-control">
                                    <option value="">Chọn CTV</option>
                                    <?php foreach ($salemans as $saleman) :?>
                                        <option value="<?php echo $saleman->id;?>"><?php echo $saleman->name;?></option>
                                    <?php endforeach;?>
                                </select>
                                <?php echo form_error('receiver_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                                <input type="hidden" name="receiver_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Số tiền cần chi</label>
                            <div class="col-sm-10">
                                <input type="text" name="amount" class="form-control" id="slug" value="<?php echo set_value('amount'); ?>" placeholder="">
                                <?php echo form_error('amount', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="note">Lý do chi</label>
                            <div class="col-sm-10">
                                <textarea name="note" class="form-control"><?php echo set_value('note'); ?></textarea> 
                                <?php echo form_error('note', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name"></label>
                            <div class="col-sm-10">
                            <button type="button" id="article-submit" class="btn btn-sm btn-info">Tạo phiếu</button>
                            </div>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[name=receiver_id]').change(function() {
            console.log(1);
        });
    });
</script>