<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-name">
                <h5><i class="fa fa-plus"></i> Thêm mới CTV</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Tên CTV<span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-article-slug form-control" id="name" value="<?php echo set_value('name'); ?>" placeholder="<?php echo lang('name_placeholder'); ?>">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="form-control" id="slug" value="<?php echo set_value('email'); ?>" placeholder="">
                                <?php echo form_error('email', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="phone">SĐT</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone" class="form-control" id="slug" value="<?php echo set_value('phone'); ?>" placeholder="">
                                <?php echo form_error('phone', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name"></label>
                            <div class="col-sm-10">
                            <button type="button" id="article-submit" class="btn btn-sm btn-info">Lưu</button>
                            </div>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>