<div class="row">
    <div class="col-lg-12">        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-book"></i> Cộng tác viên</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                    <form method="GET" accept-charset="utf-8" id="formList">
                        <input type="hidden" id="sort" name="order" value="">
                        <input type="hidden" id="sortBy" name="by" value="">
                        <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">

                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'salemans/del'; ?>">
                                    <i class="fa fa-remove"></i> Xóa</a>                                
                                </div>
                                <a href="<?php echo base_url();?>salemans/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Thêm mới</a>
                                <a href="<?php echo base_url();?>salemans/payment" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Tạo phiếu chi</a>
                            </div>                    
                        </div>                                                
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="col-md-3 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                            </div>  
                            <div class="col-md-5 padding-middle text-right">
                                <?php if (count($salemans) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                                                                     
                        </div>
                        <table class="table table-striped table-bordered table-hover " id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>
                                    <th class="text-center">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Họ tên <a href="javascript: doSort('name', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">SĐT <a href="javascript: doSort('phone', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Code <a href="javascript: doSort('coupon_code', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Email <a href="javascript: doSort('email', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">Số dư <a href="javascript: doSort('amount', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($salemans as $key => $item) : ?>
                                <tr id="row-<?php echo $key; ?>" class="">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $item->id;?>" name="checkItems[]" />
                                    </td> 
                                    <td class="text-center"><?php echo $item->id; ?></td>
                                    <td><strong><?php echo $item->name; ?></strong></td>
                                    <td class="text-center"><?php echo $item->phone; ?></td>
                                    <td class="text-center"><?php echo $item->coupon_code; ?></td>
                                    <td class="text-center"><?php echo $item->email; ?></td>
                                    <td class="text-center"><?php echo number_format($item->amount); ?> <sup>đ</sup></td>
                                    <td width="10%" class="text-center">
                                        <a href="salemans/edit/<?php echo $item->id; ?>" title="">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('salemans/del'); ?>', <?php echo $item->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($salemans) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                                <?php if (count($salemans) > 0):?>
                                Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                                - <strong><?php echo number_format($showTo);?></strong> 
                                trong tổng <strong><?php echo number_format($totalCount);?></strong>
                                <?php endif;?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'salemans/del'; ?>">
                                    <i class="fa fa-remove"></i> Xóa</a>                               
                                </div>  
                                <a href="<?php echo base_url();?>salemans/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Thêm mới</a>
                            </div>                    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var message_confirm_delete = '<?php echo lang('message_confirm_delete'); ?>';
</script>