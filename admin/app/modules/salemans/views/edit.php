<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-edit"></i> Sửa CTV | <?php echo $saleman->name;?></h5>
            </div>
            
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-page" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-1" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-11">
                                <input type="text" name="name" class="form-control" id="title" value="<?php echo set_value('name', $saleman->name); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-1" for="email">Email</label>
                            <div class="col-sm-11">
                                <input type="text" name="email" class="form-control" id="email" value="<?php echo set_value('email', $saleman->email); ?>" placeholder="">
                                <?php echo form_error('email', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-1" for="phone">SĐT</label>
                            <div class="col-sm-11">
                                <input type="text" name="phone" class="form-control" id="phone" value="<?php echo set_value('phone', $saleman->phone); ?>" placeholder="">
                                <?php echo form_error('phone', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <button type="submit" id="page-submit" class="btn btn-sm btn-primary">Lưu</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
var _baseUrl = "<?php echo base_url(); ?>";
function confirmDelete( $url ) {
    if ( confirm(message_confirm_delete) ) {
        window.location.href = $url;
    } 
}
</script>