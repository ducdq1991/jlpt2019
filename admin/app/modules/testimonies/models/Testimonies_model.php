<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonies_model extends Bigshare_Model {

	public function __construct()
	{
		parent::__construct();
	}
	/**
	* [add testimonies]
	**/
	public function addTestimonie( $args )
	{
		$this->_table = 'testimonies';
		$this->_data = $args;
		if( $this->addRecord()){
			return true;
		}
		return false;
	}
	/**
	* [get one record testimonies]
	**/
	public function getTestimonie( $where = [] )
	{
		$this->_selectItem = 'id, full_name, description, content, image, status';
		$this->_table = 'testimonies';
        $this->_wheres = $where;
        return $this->getOneRecord();
	}
	/**
	* [get all record testimonies]
	**/
	public function getTestimonies( $where = [], $limit = 0, $offset = 0, $likes = [] )
	{
		$this->_selectItem = 'id, full_name, description, content, image, status';
        $this->_from = 'testimonies';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $query = $this->getListRecords();
        return $query;
	}
	/**
	* [total testimonies]
	**/
	public function totalTestimonies( $where = [] )
	{
		$this->_table = 'testimonies';
        $this->_wheres = $where;
        return $this->totalRecord();
	}
	/**
	* [upadte testimonies]
	**/
	public function updateTestimonie( $id, $args = [] )
	{
		$where = [];
		$where['testimonies.id'] = (int)$id;
		$this->_table = 'testimonies';
		$this->_wheres = $where;
		$this->_data = $args;
		return $this->updateRecord();
	}
	/**
	* [delete testimonies]
	**/
	public function delTestimonie( $where = [], $multiple = false )
    {
        $this->_wheres = $where;
    	$this->_multiple = $multiple;
        $this->_table = 'testimonies';
        return $this->deleteRecord();
    }
    /**
    * [change status tastimonies]
    **/
    public function changeStatus( $ids, $args = [] )
    {
    	$this->_data = $args;
    	$this->_table = 'testimonies';
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
}