<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('add_testimonie');?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('full_name');?></label>
                            <div class="col-sm-10">
                                <input type="text" name="full_name" value="<?php echo set_value('full_name');?>" class="autoSlug auto-course-slug form-control">
                                <?php echo form_error('full_name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label"><?php echo lang('description');?></label>
                            <div class="col-sm-10">
                                <textarea name="description" value="<?php echo set_value('description'); ?>" class="form-control"></textarea>
                                <?php echo form_error('description', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2" for="title"><?php echo lang('content');?></label>
                            <div class="col-sm-10">                                                                     
                                <textarea id="content" name="content" rows="10" cols="60" class="form-control"><?php echo set_value('content'); ?></textarea>
                                <?php echo form_error('content', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">    
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('avartar');?></label>
                            <div class="col-sm-10">
                                <div id="load-image" class="load-image-course">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="image" value="<?php echo set_value('image'); ?>" id="image" />
                                <button type="button" id="load-form-featured-image" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredImageModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-featured-image" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo lang('status');?></label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php echo set_checkbox('status', '1'); ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>                                              
                    </div>  
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset"><?php echo lang('reset'); ?></button>
                            <button class="btn btn-primary" type="submit"><?php echo lang('add_new'); ?></button>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>  
<!-- Featured Image Modal -->
<div id="featuredImageModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-image-iframe" src="<?php echo base_url(); ?>assets/js/filemanager/dialog.php?field_id=image" width="100%" height="450px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>