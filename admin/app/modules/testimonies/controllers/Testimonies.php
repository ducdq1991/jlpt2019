<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonies extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('testimonies_model');
		$this->lang->load('testimonies', $this->session->userdata('lang'));
	}
	/**
	* [ all testimonies ]
	**/
	public function index( $page = 0 )
	{
		$data = $where = $like = [];
		$this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        //Get Filters
		$keyword = $this->input->get('keyword');
		$status = $this->input->get('status');
		// Set Status Condition
		if($status != ''){
		  $where['testimonies.status'] = ($status ==='active') ? 1 : 0;
		}
		// Set Likes Condition
		if($keyword !=''){
			$like['field'] = 'testimonies.full_name';
			$like['value'] = (string) trim($keyword);
		}
		// Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('itemPerPages');
        //get testimonies
        $totalRecord = $this->testimonies_model->totalTestimonies( $where ); 
        $data['paging'] = paging(base_url() .'testimonies', $page, $totalRecord, $limit);
        $testimonies = $this->testimonies_model->getTestimonies( $where, $limit, $offset, $like );
        $data['testimonies'] = $testimonies;
		//load view
		$this->load->view('layouts/header');
        $this->load->view('testimonies/index', $data);
        $this->load->view('layouts/footer');
	}
	/**
	* [add testimonies]
	**/
	public function add()
	{
		$data = [];
		$this->load->library('form_validation');
		$config = array(
			array(
                'field' => 'full_name',
                'label' => 'Họ và tên',
                'rules' => 'trim|required|min_length[5]|max_length[35]',
                'errors' => array(
                    'required' => 'Họ và tên không được để trống',
                    'min_length' => 'Họ và tên ít nhất là 5 ký tự',
                    'max_length' => 'Họ và tên nhiều nhất là 35 ký tự',
                )
            ),
            array(
                'field' => 'description',
                'label' => 'Thông tin học viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Thông tin học viên không được để trống'
                )
            ),
            array(
                'field' => 'content',
                'label' => 'ý kiến của học viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'ý kiến của học viên học viên không được để trống'
                )
            )
		);
		$this->form_validation->set_rules($config);
		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('layouts/header');
	        $this->load->view('testimonies/add', $data);
	        $this->load->view('layouts/footer');
		} else {
			$testimonies = [];
			$postData = $this->input->post();
			$postData = $this->nonXssData($postData);
			$testimonies['full_name'] = $postData['full_name'];
			$testimonies['description'] = $postData['description'];
			$testimonies['content'] = $postData['content'];
			$testimonies['image'] = $postData['image'];
			$testimonies['status'] = isset($postData['status']) ? $postData['status'] : 0;
			if( $this->testimonies_model->addTestimonie( $testimonies ) ) {
				$this->session->set_flashdata('success', 'Bạn đã thêm ý kiến học viên thành công');
				redirect(base_url().'testimonies');
			} else {
				$this->session->set_flashdata('error', 'Thêm ý kiến học viên thất bại');
			}
		}
	}
	/**
	* [edit testimonies]
	**/
	public function edit( $id )
	{
		$data = $where = [];
		$data['_baseUrl'] = $this->getBaseUrl();
		$where['testimonies.id'] = $id;
		//get info testimonie
		$testimonie = $this->testimonies_model->getTestimonie( $where );
		$data['testimonie'] = $testimonie;
		$this->load->library('form_validation');
		$config = array(
			array(
                'field' => 'full_name',
                'label' => 'Họ và tên',
                'rules' => 'trim|required|min_length[5]|max_length[35]',
                'errors' => array(
                    'required' => 'Họ và tên không được để trống',
                    'min_length' => 'Họ và tên ít nhất là 5 ký tự',
                    'max_length' => 'Họ và tên nhiều nhất là 35 ký tự',
                )
            ),
            array(
                'field' => 'description',
                'label' => 'Thông tin học viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Thông tin học viên không được để trống'
                )
            ),
            array(
                'field' => 'content',
                'label' => 'ý kiến của học viên',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'ý kiến của học viên học viên không được để trống'
                )
            )
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			//load view
			$this->load->view('layouts/header');
	        $this->load->view('testimonies/edit', $data);
	        $this->load->view('layouts/footer');
		} else {
			$infoTestimonie = [];
			$infoTestimonie = $this->input->post();
			$infoTestimonie = $this->nonXssData($infoTestimonie);
			if( !$infoTestimonie['status'] ){
				$infoTestimonie['status'] = 0;
			}
			if( $this->testimonies_model->updateTestimonie( $id, $infoTestimonie ) ){
				$this->session->set_flashdata('success', 'Bạn đã cập nhật thành công');
				redirect(base_url().'testimonies');
			} else {
				$this->session->set_flashdata('error', 'Cập nhật thất bại');
			}
		}
	}
	/**
	* [delete testimonies]
	**/
	public function del( $id = 0 )
	{
		if( $id ) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }
        if( empty($ids) ) {
            $this->session->set_flashdata('warning', 'lựa chọn của bạn không có');
            redirect(base_url() . 'testimonies');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->testimonies_model->delTestimonie( $where, true );
            if( $query ) {
                $this->session->set_flashdata('success', 'Bạn đã xóa thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại');
            }
            echo 1;
            die;
        } else {
            $query = $this->testimonies_model->delTestimonie( array( 'id' => (int) $ids ) );
            if( $query ) {
                $this->session->set_flashdata('success', 'Bạn đã xóa thành công');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại');
            }
            echo 2;
            die;
        }
	}
	/**
	* [change status statimonies]
	**/
	public function changeStatus()
    {
        $args = [];
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->testimonies_model->changeStatus( $ids, $args))
                {
                    $this->session->set_flashdata('success', 'Trạng thái đã được thay đổi thành công');
                } else {
                    $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại');
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->testimonies_model->changeStatus( $ids, $args ) )
            {
                $this->session->set_flashdata('success', 'Trạng thái đã được thay đổi thành công');
            } else {
                $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại');
            }
            echo 1;
            die;    
        } 
    }
}