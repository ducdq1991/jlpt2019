<?php defined('BASEPATH') or exit('No direct script access allowed');

class Promotion_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'user_course';
    }

    public function getAllUser()
    {
        $this->_from = 'users';
        $this->_selectItem = 'users.id, users.username';
        $this->_wheres = ['user_groups.access_key=' => 'member'];
        $joinExamDetail['table'] = 'user_groups';
        $joinExamDetail['condition'] = 'users.group_id = user_groups.id';
        $joinExamDetail['type'] = 'left';
        $joins[] = $joinExamDetail;
        $this->_joins = $joins;
        $this->_limit = 0;
        $users = $this->getListRecords();
        $this->resetQuery();
        $listUser = [];
        if(!empty($users)) {
            foreach ($users as $value) {                
                $listUser[$value->id] = isset($fullName) ? $value->username.'('.$fullName.')' : $value->username;
            }
        }
        return $listUser;
    }

    public function getCourses()
    {
		$this->_selectItem = 'courses.id, course_details.name';
		$this->_from = 'courses';
		$joins = array();
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'courses.id = course_details.course_id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;
		$this->_joins = $joins;
		$this->_limit = 0;
		$this->_offset = 0;
		$this->_wheres = ['courses.parent' => 0];
		$result = $this->getListRecords();
		$this->resetQuery();
		if($result) {
			return $result;
		}
		return false;
    }

    public function getUserById( $id )
    {
        $this->_selectItem = 'A.id, A.username, A.password, A.email, A.phone, A.status, A.permission, A.last_login, A.group_id, B.name AS group, B.access_key';
        $this->_table = 'users as A';
        $joins = array();
        $joinGroup['table'] = 'user_groups as B';
        $joinGroup['condition'] = 'A.group_id = B.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
        $this->_joins = $joins;
        $where = array();
        $where['A.id'] = (int) $id;
        $this->_wheres = $where;
        $user = $this->getOneRecord();        
        $userMetas = $this->getUserMetas($id);
        $user->metas = $userMetas;
        $this->resetQuery();
        return $user;
    }

    public function insertUserCourse($args = [])
    {
    	$this->_data = $args;
    	$this->_table = 'user_course';
    	return $this->addRecord();
    }

    public function getPromotion($where = []) 
    {
    	$this->_wheres = $where;
    	$this->_from = 'user_course';
        $this->_limit = 0;
    	return $this->getListRecords();
    }

    public function updatePromotion($where=[], $args=[])
    {
    	$this->_wheres = $where;
    	$this->_data = $args;
    	$this->_table = 'user_course';
    	return $this->updateRecord();
    }

    public function deletePromotion($where=[], $multiple = false)
    {
    	$this->_wheres = $where;
    	$this->_table = 'user_course';
        $this->_multiple = $multiple;
    	return $this->deleteRecord();
    }

    public function totalPro($where=[])
    {
        $this->_selectItem = 'course_id';
        $this->_distinct = 'course_id';
    	$this->_wheres = $where;
       // $this->_groupBy = ['user_course.course_id'];
    	$this->_table = 'user_course';
    	return $this->totalRecord();
    }

    public function getUserCourses($where = [], $limit = 0, $offset = 0, $likes=[], $order=[])
    {
    	$this->_selectItem = 'user_course.course_id, course_details.name';
    	$this->_from = $this->_table;
    	$joins = array();
        $joinCourse['table'] = 'course_details';
        $joinCourse['condition'] = 'course_details.course_id = user_course.course_id';
        $joinCourse['type'] = 'left';
        $joins[] = $joinCourse;
        $this->_joins = $joins;
    	$this->_offset = $offset;
        $this->_limit = $limit;
        $this->_wheres = $where;
        $this->_groupBy = ['user_course.course_id'];
        $this->_order = $order;
        $query = $this->getListRecords();
        $this->resetQuery();
        return $query;
    }

    public function existUserCourse($where)
    {
        $this->_wheres = $where;
        $this->_table = 'user_course';
        if ($this->getOneRecord() != null) {
            return true;
        }
        return false;
    }

    public function getUserOfCourse($course_id) 
    {
        $this->_wheres = ['course_id' => (int) $course_id, 'type' => 2];
        $this->_table = 'user_course';
        $userCourse = $this->getListRecords();
        $this->resetQuery();
        if ($userCourse != null) {
            $userIds = [];
            foreach ($userCourse as $key => $value) {
                $userIds[] = $value->user_id;
            }
            return $userIds;
        }
        return [];
    }
}