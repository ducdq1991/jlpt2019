<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('edit_promotion');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user-promotion">
                    <br/>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="course_id"><?php echo lang('course');?> <span>*</span>:</label>
                                <div class="col-sm-8">
                                <input type="hidden" name="course_id" value="<?php echo $course->course_id; ?>">
                                    <?php echo $course->name; ?>                                 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="user_id"><?php echo lang('user'); ?></label>
                                <div class="col-sm-10">
                                    <div class="input-group no-padding" style="position: relative">
                                        <input id="search-users" class="form-control" placeholder="Tên học sinh">
                                        <ul id="list-users"></ul>                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <h3>Hướng dẫn:</h3>
                        <p>- Nhập tên sau đó bấm phím Cách để tìm học viên </p>
                        <p>- Chọn học viên cần khuyến mại </p>
                    </div>                    
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-striped dataTable" id="table-users" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th width="10%" tabindex="0" rowspan="1" colspan="1">ID</th>
                                        <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('name');?></th>
                                        <th width="20%" tabindex="0" rowspan="1" colspan="1"><?php echo 'Tài khoản';?></th>
                                        <th width="30%" tabindex="0" rowspan="1" colspan="1"><?php echo 'Email';?></th>
                                        <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($promotions as $item):?>
<tr class="gradeA odd" role="row" id="row-<?php echo $item->user_id;?>"><td class="sorting_1"><?php echo $item->user_id;?></td><td><?php echo $item->full_name;?></td><td><?php echo $item->username;?></td><td><?php echo $item->email;?></td><td class="center text-center"><a title=""><button class="btn btn-xs btn-danger" data-id="<?php echo $item->user_id;?>" id="remove-user" type="button"><i class="fa fa-trash"></i></button></a></td></tr>       
                                    <?php endforeach;?>               
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var courseId = <?php echo $course->course_id;?>;
</script>

  <script>
  $(function() {

    $("#search-users").change(function(event) {
        var $keyword = $(this).val();

        $.ajax({
            url : adminUrl + 'user/ajaxsearch?term=' + $keyword,
            dataType : 'json',
            success : function(res){

            if (res.length == 0) {
                var $html = 'Không tìm thấy học sinh nào!';
            } else {
                var $html = '';
                $.each(res, function(k,v){
                    $html += '<li><a class="user-item" data-fullname="'+v.full_name+'" data-username="'+v.username+'" data-email="'+v.email+'" data-id="'+v.id+'">'+v.full_name+' ( '+ v.username +')</a></li>';
                });
            }
            $('ul#list-users').empty().append($html);
            $('ul#list-users').show();
            return true;

            },
        });

    });

    $(document).on('click', 'ul#list-users li a.user-item', function(){
        $userId = $(this).attr('data-id');
        $fullname = $(this).attr('data-fullname');
        $username = $(this).attr('data-username');
        $email = $(this).attr('data-email'); 
        $html = ''; 
        $html += '<tr class="gradeA odd" role="row" id="row-' + $userId + '">'; 
            $html += '<td class="sorting_1">' + $userId + '</td>';
            $html += '<td>' + $fullname + '</td>';
            $html += '<td>' + $username + '</td>';
            $html += '<td>' + $email + '</td>';
            $html += '<td class="center text-center">';
                $html += '<a title="">';
                    $html += '<button class="btn btn-xs btn-danger" data-id="' + $userId + '" id="remove-user" type="button">';
                        $html += '<i class="fa fa-trash"></i>';
                    $html += '</button>';
                $html += '</a>';
            $html += '</td>';
        $html += '</tr>';

        $.ajax({
            url : adminUrl + 'promotion/addajax',
            method : 'post',
            data : {user_id:$userId, course_id: courseId},
            dataType : 'json',
            success : function(res){
                if (res == 1) {
                    $('#table-users tbody').append($html);
                    $('ul#list-users').hide();                    
                    alert('Đã thêm ' + $fullname);
                    return true;
                }               
                alert('Không thêm được học viên!');
                return false; 
            },
        });

    });
  });
  </script>

  <?php 

  ?>