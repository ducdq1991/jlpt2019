<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('add_new_promotion');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user-promotion" role="form" method="POST" accept-charset="utf-8">
                    <br/>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="course_id"><?php echo lang('course');?> <span>*</span>:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="course_id" name="course_id">
                                        <option value="">--<?php echo lang('choose_course');?>--</option>
                                        <?php if($courses) :?>
                                            <?php foreach ($courses as $value) : ?>
                                                <option value="<?php echo $value->id; ?>" <?php echo set_select('course_id', $value->id); ?>><?php echo $value->name; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    </select>
                                    <?php echo form_error('course_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>