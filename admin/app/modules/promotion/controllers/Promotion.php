<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Notification
* @author ChienDau
* @copyright Bigshare .,JSC
* 
*/

class Promotion extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('promotion_model');
        $this->load->model('course/course_model');
        $this->lang->load('promotion', $this->session->userdata('lang'));
	}

	public function index($page = 0)
	{
		$data = $where = $likes = $order = array();
		$data['baseUrl'] = $this->getBaseUrl();
       	$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        $user = $this->input->get('user');
        $attribute = $this->input->get('attribute');
        $date = $this->input->get('date');
        $exam_id = $this->input->get('exam_id');
        $data['date'] = $date;
        $data['keyword'] = $keyword;
        $data['user'] = $user;
        $data['attribute'] = $attribute;
        $data['exam_id'] = $exam_id;
        if(!empty($exam_id)) {
            $where['user_exam.exam_id'] = (int) $exam_id;
        }
        if(!empty($keyword)){
            $likes['field'] = 'exam.title';
            $likes['value'] = $keyword;
        }
        if($date != '') {
            $date = date('Y-m-d H:i:s', strtotime($date));
            $where['user_exam.created >='] = $date;
            $where['user_exam.created <'] = date('Y-m-d H:i:s', (strtotime($date) + 24*60*60) );
        }
		if($user != ''){
			$where['user_exam.user_id'] = (int) $user;
		}
        if($attribute != ''){
            $where['user_exam.attribute'] = (int) $attribute;
        }
		$order = [
			'user_exam.created' => 'DESC',
			'user_exam.exam_id'	=> 'DESC'
		];

		$where['user_course.type'] = 2;
		$order = ['user_course.course_id' => 'DESC'];
        $total = $this->promotion_model->totalPro($where); 
		$promotions = $this->promotion_model->getUserCourses( $where, $limit, $offset, $likes, $order );
        $data['promotions'] = $promotions;

        $showFrom = $offset + count($promotions);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];     
        $data['paging'] = paging(base_url() .'promotion', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Khuyến mại khóa học', base_url().'promotion');
        $head['breadcrumb'] = $this->breadcrumbs->display();   

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
	}

    public function addajax()
    {
        $args = $this->input->post();
        $args['type'] = 2;
        $args['created'] = date('Y-m-d H:i:s');
        if (!empty($args)) {
            if ($this->promotion_model->insertUserCourse($args)) {
                echo 1;
                exit;
            }
        }
        echo 2;
        exit;
    }

    public function add()
    {
        $data = array();
        $this->load->library('form_validation');
        $courses = $this->promotion_model->getCourses();
        $data['courses'] = $courses;
        $config = array(
            array(
                'field' => 'course_id',
                'label' => lang('course'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  lang('mes_required'),
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Khuyến mại', base_url().'promotion');
            $this->breadcrumbs->add(lang('add'), base_url().'promotion/add');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('promotion/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            if (!empty($info['user_id'])) {
                $users = explode(',', $info['user_id']);
            }
            $d = 0;
            foreach ($users as $item) {
                $args = [
                    'user_id' => (int) $item,
                    'course_id' => (int) $info['course_id'],
                    'type' => 2,
                    'created' => date('Y-m-d H:i:s')
                ];
                if ($this->promotion_model->insertUserCourse($args)) {
                    $d++;
                }
            }
            if( $d ) {
                $message = sprintf('Đã khuyến mại khóa học cho %d học viên', $d);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf('Không có học viên nào được khuyến mại');
                $this->session->set_flashdata('warning', $message);
            }
            redirect(base_url('promotion/edit/' .$info['course_id'] ));
        }
    }

	public function getUser()
	{
		$id = (int) $this->input->post('id');
		$user = $this->promotion_model->getUserById( $id );
		if( !empty($user) ) {
			$html = '';
			$html .= '<tr class="gradeA odd" role="row" id="row-'.$user->id.'">'; 
		        $html .= '<td class="sorting_1">' . $user->id . '</td>';
		        $html .= '<td>' . $user->metas['full_name'] . '</td>';
		        $html .= '<td>' . $user->username . '</td>';
		        $html .= '<td>' . $user->email . '</td>';
		        $html .= '<td class="center text-center">';
		            $html .= '<a title="">';
		                $html .= '<button class="btn btn-xs btn-danger" data-id="'.$user->id.'" id="remove-user" type="button">';
		                    $html .= '<i class="fa fa-trash"></i>';
		                $html .= '</button>';
		            $html .= '</a>';
		        $html .= '</td>';
		    $html .= '</tr>';
	    	echo $html;
	    	die;
	    } else {
	    	echo 0;
	    	die;
	    }
	}

	public function edit($course_id)
	{
		$data = array();
        $this->load->library('form_validation');
                
        $course = $this->course_model->getCourseDetail(['course_details.course_id' => $course_id]);             
        $data['course'] = $course;
        $where = [
        	'user_course.course_id' => (int) $course_id,
        	'type' => 2,
            'cost' => 0
        ];
        $orderCondition = [];
        $orderCondition['user_course.created'] = 'desc';
        $promotions = $this->course_model->getCourseUsers($where, 0, 0, [], $orderCondition);
        $data['promotions'] = $promotions;
        $config = array(
            array(
                'field' => 'course_id',
                'label' => lang('course'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  lang('mes_required'),
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Khuyến mại', base_url().'promotion');
            $this->breadcrumbs->add(lang('edit'), base_url().'promotion/add');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 

            $this->load->view('layouts/header', $head);
            $this->load->view('promotion/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            if (!empty($info['user_id'])) {
            	$users = explode(',', $info['user_id']);
	            $add = 0; $del=0;
                $userIds = $this->promotion_model->getUserOfCourse($course_id);
                $diff =array_diff($users,$userIds);
	            foreach ($users as $item) {
                    $where = [
                        'type' => 2,
                        'user_id' => $item,
                        'course_id' => (int) $course_id,
                    ];
                    if (in_array($item, $diff)) {
                        $args = [
                            'user_id' => (int) $item,
                            'course_id' => (int) $info['course_id'],
                            'type' => 2,
                            'created' => date('Y-m-d H:i:s')
                        ];
                        if ($this->promotion_model->insertUserCourse($args)) {
                            $add++;
                        }
                    }
	            }
                if ($add) {
                    $message = sprintf('Đã Thêm khuyến mại khóa học cho %d học viên', $add);
                    $this->session->set_flashdata('success', $message);
                }
	        } else {
                $where = [
                    'course_id' => (int) $course_id,
                    'type' => 2,
                ];
                $this->promotion_model->deletePromotion($where);
                $message = sprintf('Hủy bỏ khuyến mại của tất cả các thành viên');
                    $this->session->set_flashdata('success', $message);
            }
            redirect(base_url('promotion'));
        }
	}
    /**
     * @name Del
     * @desc delete a Promotion from modules table
     * */

    public function deletePromotion()
    {
        $userId = $this->input->post('user_id');
        $courseId = $this->input->post('course_id');

        if( empty($userId)  || empty($courseId)) {
            $message = sprintf(lang('message_delete_warning'), lang('promotion_course'));
            $this->session->set_flashdata('warning', $message);
            return false;
        }

        $where = [
            'type' => 2,
            'user_id' => $userId,
            'course_id' => (int) $courseId,
        ];
        if ($this->promotion_model->deletePromotion($where)) {
            echo 1;
            exit;
        }
        echo 2;
        exit;
    }

    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('promotion_course'));
            $this->session->set_flashdata('warning', $message);
            return false;
        }
        $query = $this->promotion_model->deletePromotion( array( 'course_id' => (int) $ids, 'type' => 2 ) );
        if( $query ) {
            $message = sprintf(lang('message_delete_success'), lang('promotion_course'));
            $this->session->set_flashdata('success', $message);
        } else {
            $message = sprintf(lang('message_delete_error'), lang('promotion_course'));
            $this->session->set_flashdata('error', $message);
        }
        echo 1;
        die;
    }
}