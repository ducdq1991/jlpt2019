<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Sửa câu hỏi | <?php echo $questionItem->name;?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>            
                <form class="form form-horizontal form-add-quiz" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="col-sm-12">
                            <label class="control-label" for="name"><?php echo lang('name');?><span>*</span>:</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?php echo set_value('name', $questionItem->name); ?>">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>                           
                            </div>                            
                            <div class="col-sm-12">
                            <label class="control-label"><?php echo lang('question_mcq');?></label>
                                <textarea name="question" id="question" rows="3" cols="20" class="editor form-control"><?php echo set_value('question', $questionItem->question); ?></textarea>
                            </div>
                        </div>
                        <?php 
                            $choicesAnswer = (object) unserialize($questionItem->choices_answer);
                        ?>
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label class="control-label">A</label>
                                <textarea name="question_aw_a" id="question_aw_a" rows="3" cols="20" class="editor form-control"><?php echo set_value('question_aw_a', $choicesAnswer->A); ?></textarea>
                            </div>
                            <div class="col-sm-6">
                            <label class="control-label">B</label>
                                <textarea name="question_aw_b" id="question_aw_b" rows="3" cols="20" class="editor form-control"><?php echo set_value('question_aw_b', $choicesAnswer->B); ?></textarea>
                            </div>                            
                        </div> 
                        <div class="form-group">
                            <div class="col-sm-6">
                            <label class="control-label">C</label>
                                <textarea name="question_aw_c" id="question_aw_c" rows="3" cols="20" class="editor form-control"><?php echo set_value('question_aw_c', $choicesAnswer->C); ?></textarea>
                            </div>
                            <div class="col-sm-6">
                            <label class="control-label">D</label>
                                <textarea name="question_aw_d" id="question_aw_d" rows="3" cols="20" class="editor form-control"><?php echo set_value('question_aw_d', $choicesAnswer->D); ?></textarea>
                            </div>
                        </div>                                                                                                                          
                    </div>              
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?php echo lang('true_answer');?></label>
                            <div>                            
                                <div class="radio i-checks"><label> <input type="radio" value="A" name="true_answer" <?php echo  set_radio('true_answer', 'A', $questionItem->true_answer == 'A'); ?> > <i></i> A </label></div>
                                <div class="radio i-checks"><label> <input type="radio" value="B" name="true_answer" <?php echo  set_radio('true_answer', 'B', $questionItem->true_answer == 'B'); ?>> <i></i> B </label></div>
                                <div class="radio i-checks"><label> <input type="radio" value="C" name="true_answer" <?php echo  set_radio('true_answer', 'C', $questionItem->true_answer == 'C'); ?>> <i></i> C </label></div>
                                <div class="radio i-checks"><label> <input type="radio" value="D" name="true_answer" <?php echo  set_radio('true_answer', 'D', $questionItem->true_answer == 'D'); ?>> <i></i> D </label></div>
                            </div> 
                            <?php echo form_error('true_answer', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>                                                                      
                        </div> 
                        <div class="form-group">
                            <label class="control-label"><?php echo lang('score');?></label>
                            <div>
                                <input class="form-control" type="number" name="score" id="score" value="<?php echo set_value('score', $questionItem->score);?>"></input>
                                <?php echo form_error('score', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="link_media_answer"><?php echo lang('answers'); ?></label>
                            <div>
                                <div id="load-mcq-answer-file">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="link_media_answer" value="<?php echo set_value('link_media_answer', $questionItem->link_media_answer); ?>" id="link_media_answer" />
                                <button type="button" id="load-form-mcq-answer-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#mcqAnswerFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-mcq-answer-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                                <?php echo form_error('link_media_answer', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>

                                <div class="hr-line-dashed"></div>
                                <input type="text" class="form-control" name="link_video_answer" placeholder="<?php echo lang('video_answer');?>" value="<?php echo set_value('link_video_answer', $questionItem->link_video_answer); ?>">
                                <div class="hr-line-dashed"></div>
                                <input type="text" class="form-control" name="link_practice" placeholder="<?php echo lang('link_practices');?>" value="<?php echo set_value('link_practice', $questionItem->link_practice); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <select name="question_category_id" id="question_category_id" class="form-control">
                                <option value="">Kho câu hỏi</option>                     
                                <?php 
                                    if (isset($categories) && count($categories) > 0) :
                                        foreach ($categories as $category) :
                                ?>
                                <option value="<?php echo $category->id;?>" <?php if ($category->id == $questionItem->question_category_id):?> selected="selected" <?php endif;?>><?php echo $category->name;?></option>  
                                <?php 
                                        endforeach;
                                    endif;
                                ?>
                            </select>
                            <?php echo form_error('question_category_id', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>                            
                        </div>
                        <div class="form-group">
                            <label class="control-label"><?php echo lang('active');?></label>
                            <input type="checkbox" value="1" id="status" name="status" <?php if($questionItem->status == 1) echo 'checked="checked"'; ?>>
                        </div>                                                 
                    </div>                         
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="hidden" name="id" value="<?php echo $questionItem->id;?>">
                            <a href="<?php echo base_url('mcq');?>" class="btn btn-white"><?php echo lang('exit');?></a>
                            <button type="submit" class="btn btn-info"><?php echo lang('edit');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Answer Modal -->
<div id="mcqAnswerFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="answer-up-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=link_media_answer" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<?php echo $tinymce;?>