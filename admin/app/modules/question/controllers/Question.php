<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @package Question <Multiple-choice questions>
 * @author Trung Doan <trung.doan@bigshare.vn>
 * @date 4/2/2016
 */
class Question extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();	
		$this->load->model('question_model');
		$this->lang->load('mcq', $this->session->userdata('lang'));	
	}

	public function categories($page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];			
		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		$page = intval(str_replace('page-', '', $page));		
		if ($page < 1) {
			$page = 1;
		}
		$offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		
		$status = $this->input->get('status');		
		if ($status != '') {
		  $where['question_categories.status'] = ($status ==='active') ? 1 : 0;
		}
		$data['status'] = $status;

		$keyword = $this->input->get('keyword');
		if ($keyword != '') {
			$likes['field'] = 'name';
			$likes['value'] = (string) trim($keyword);
		}
		$data['keyword'] = $keyword;

		//Query
		$total = $this->question_model->numRowCategory($where);      		
		$listItems = $this->question_model->getQuestionCategories($where, $limit, $offset, $orderCondition, $likes);
		$data['listItems'] = $listItems;
        $showFrom = $offset + count($listItems);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'question/categories', $page, $total, $limit, $queryString); 
		$data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Kho câu hỏi', base_url().'question');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
		$this->load->view('question/category', $data);
		$this->load->view('layouts/footer');
	}

	public function addCategory()
	{
		$data = $config = [];
		$this->load->library('form_validation');	
		// Set Rules		

        $config = [
            [
                'field' => 'name',
                'label' => 'Tên kho',
                'rules' => 'trim|required|is_unique[question_categories.name]',
                'errors' => [
                    'required' => 'Tên kho là bắt buộc.',
                    'is_unique' => 'Kho câu hỏi này đã tồn tại.',
                ],
            ],
            [
                'field' => 'code',
                'label' => 'Mã kho',
                'rules' => 'trim|required|is_unique[question_categories.code]',
                'errors' => [
                    'required' => 'Mã kho là bắt buộc.',
                    'is_unique' => 'Mã kho này đã tồn tại.',
                ],
            ],
        ];

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {        				
	        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Kho câu hỏi', base_url().'question/categories');
	        $this->breadcrumbs->add('Tạo kho câu hỏi', base_url().'question/addcategory');
	        $head['breadcrumb'] = $this->breadcrumbs->display();

			$this->load->view('layouts/header', $head);
			$this->load->view('question/add_category', $data);
			$this->load->view('layouts/footer');	
        } else {
        	$postData = $this->input->post();
        	$postData = $this->nonXssData($postData);			    	

			if($id = $this->question_model->addQuestionCategory($postData)){            	
            	$this->session->set_flashdata('success', 'Tạo kho câu hỏi thành công!');
            	redirect(base_url() . 'question/categories');
            } else {            	
				$this->session->set_flashdata('error', 'Không thể tạo kho câu hỏi');
				redirect(base_url() . 'question/addcategory');
			} 
			
        }	
	}

	public function editcategory($categoryId = 0)
	{	
		$data = $where = [];

		if (!$categoryId) {
			redirect( base_url() . 'question/categories', 'refresh');
		}

		$category = $this->question_model->getQuestionCategory((int)$categoryId);
		$data['category'] = $category;
		
		
		if (!$category) {
			$this->session->set_flashdata('error', 'Kho câu hỏi không tồn tại!');
			redirect( base_url() . 'question/categories', 'refresh');
		}

        if ($this->input->post('name') != $category->name) {
            $isUniqueName = '|is_unique[question_categories.name]';
        } else {
            $isUniqueName = '';
        }

        if ($this->input->post('code') != $category->code) {
            $isUniqueCode = '|is_unique[question_categories.code]';
        } else {
            $isUniqueCode = '';
        } 

        $config = [
            [
                'field' => 'name',
                'label' => 'Tên kho',
                'rules' => 'trim|required' . $isUniqueName,
                'errors' => [
                    'required' => 'Tên kho là bắt buộc.',
                    'is_unique' => 'Kho câu hỏi này đã tồn tại.',
                ],
            ],
            [
                'field' => 'code',
                'label' => 'Mã kho',
                'rules' => 'trim|required' . $isUniqueCode,
                'errors' => [
                    'required' => 'Mã kho là bắt buộc.',
                    'is_unique' => 'Mã kho này đã tồn tại.',
                ],
            ],
        ];

        $this->load->library('form_validation');
        $this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE) {
			$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Kho câu hỏi', base_url().'question/categories');
	        $this->breadcrumbs->add('Sửa kho câu hỏi | ' . $category->name, base_url().'question/editcategory/' . $category->id);
	        $headers['breadcrumb'] = $this->breadcrumbs->display();

	        $this->load->view('layouts/header', $headers);
	        $this->load->view('category/edit_category',$data);
			$this->load->view('layouts/footer');
        } else {
        	$postData = $this->input->post();
        	$postData = $this->nonXssData($postData);

			if ($this->question_model->updateQuestionCategory($categoryId, $postData)) {
				$this->session->set_flashdata('success','Cập nhật kho câu hỏi thành công!');
			} else {
				$this->session->set_flashdata('error','Cập nhật kho câu hỏi thất bại!');
			}
			redirect( base_url() . 'question/categories', 'refresh');
        }        
	}

	public function index($page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];			
		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		$page = intval(str_replace('page-', '', $page));		
		if ($page < 1) {
			$page = 1;
		}
		$offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       

        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		
		$status = $this->input->get('status');		
		if ($status != '') {
		  $where['questions.status'] = ($status ==='active') ? 1 : 0;
		}
		$data['status'] = $status;
		$type = $this->input->get('type');		
		if ($type != '') {
		  $where['questions.type'] = (int) $type;
		}
		$data['type'] = $type;

		$keyword = $this->input->get('keyword');
		if ($keyword != '') {
			$likes['field'] = 'questions.name';
			$likes['value'] = trim($keyword);
		}
		$data['keyword'] = $keyword;

		$categoryId = $this->input->get('category_id');		
		if ($categoryId != '' && $categoryId != 0) {
		  $where['questions.question_category_id'] = (int) $categoryId;
		}
		$data['categoryId'] = $categoryId;

		//Query
		$total = $this->question_model->numRows($where, $likes);      		
		$listItems = $this->question_model->getQuestions($where, $limit, $offset, $orderCondition, $likes);
		$data['listItems'] = $listItems;
        $showFrom = $offset + count($listItems);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'question', $page, $total, $limit, $queryString); 
		$data['itemPerPages'] = $this->config->item('rowPerPages');

		$data['categories'] = $this->question_model->getQuestionCategories([], 0);

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Ngân hàng câu hỏi', base_url().'question');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
		$this->load->view('question/index', $data);
		$this->load->view('layouts/footer');
	}	

	public function add()
	{
		$this->load->helper('upload');
		$data = $config = [];
		// Load TinyMce Editor
		$this->load->helper('tinymce');
		$data['tinymce'] = math_tinymce(100);
		// Load Validate Form Library
		$this->load->library('form_validation');	
		// Set Rules				
		$this->form_validation->set_rules('name', lang('name'), 'required', array('required' => lang('mes_required')));
		$this->form_validation->set_rules('true_answer', lang('true_answer'), 'required', array('required' => lang('mes_required')));
		$this->form_validation->set_rules('question_category_id', 'Kho câu hỏi', 'required', array('required' => 'Kho câu hỏi là bắt buộc'));		
		$this->form_validation->set_rules('score', lang('score'), 'required|is_natural', array('required' => lang('mes_required'), 'is_natural' =>lang('mes_is_natural')));

		//Danh sach kho cau hoi
		$data['categories'] = $this->question_model->getQuestionCategories([], 0);

        if ($this->form_validation->run() == FALSE) {        				
	        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Câu hỏi', base_url().'question');
	        $this->breadcrumbs->add('Tạo câu hỏi mới', base_url().'question/add');
	        $head['breadcrumb'] = $this->breadcrumbs->display();

			$this->load->view('layouts/header', $head);
			$this->load->view('question/add', $data);
			$this->load->view('layouts/footer');	
        } else {
        	$postData = $this->input->post();
        	$postData = $this->nonXssData($postData);
        	$choicesAnswer = array();
        	$choicesAnswer['A'] = $postData['question_aw_a'];
        	$choicesAnswer['B'] = $postData['question_aw_b'];
        	$choicesAnswer['C'] = $postData['question_aw_c'];
        	$choicesAnswer['D'] = $postData['question_aw_d'];
        	$args['name'] = trim($postData['name']);
        	if($this->question_model->checkExistQuestion($args['name']) == true){
        		$message = sprintf(lang('exited_item'), $args['name'], lang('name'));     		
        		$this->session->set_flashdata('error', $message);
        		redirect(base_url() . 'question/add');
        	}
        	$args['question'] = $postData['question'];
        	$args['choices_answer'] = serialize($choicesAnswer);
			$args['true_answer'] = $postData['true_answer'];
			$args['link_media_answer'] = $postData['link_media_answer'];
			$args['score'] = $postData['score'];
			$args['type'] = 1;
			$args['question_category_id'] = $postData['question_category_id'];
        	if(isset($postData['link_video_answer']) && !empty($postData['link_video_answer'])){
        		$args['link_video_answer'] = $postData['link_video_answer'];
        	}
        	if(isset($postData['link_practice']) && !empty($postData['link_practice'])){
        		$args['link_practice'] = $postData['link_practice'];
        	}

        	if(isset($postData['status']) && !empty($postData['status'])){
        		$args['status'] = $postData['status'];
        	}
			if($id = $this->question_model->addQuestion($args)){
            	$message = sprintf(lang('mes_add_success'), lang('question'));
            	$this->session->set_flashdata('success', $message);
            } else {
            	$message = sprintf(lang('mes_add_error'), lang('question'));
				$this->session->set_flashdata('error', $message);
			} 
			redirect(base_url() . 'question/edit/' . $id);
        }	
	}

	/**
	 * [edit Quiz Question]
	 */
	public function edit($questionId = '')
	{
		$question = $this->question_model->getOneQuestion($questionId);
		if (empty($question)) {
			redirect(base_url() . 'question');
		}
		$this->load->helper('upload');
		$data = $config = array();
		// Load TinyMce Editor
		$this->load->helper('tinymce');
		$data['tinymce'] = math_tinymce(100);
		// Load Validate Form Library
		$this->load->library('form_validation');	
		// Set Rules			

        if ($this->input->post('name') != $question->name) {
            $isUniqueName = '|is_unique[questions.name]';
        } else {
            $isUniqueName = '';
        }

        $config = [
            [
                'field' => 'name',
                'label' => 'Tên câu hỏi',
                'rules' => 'trim|required' . $isUniqueName,
                'errors' => [
                    'required' => 'Tên câu hỏi là bắt buộc.',
                    'is_unique' => 'Tên câu hỏi này đã tồn tại.',
                ],
            ],
            [
                'field' => 'true_answer',
                'label' => 'Đáp án đúng',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Đáp án đúng là bắt buộc.',
                ],
            ],
            [
                'field' => 'question_category_id',
                'label' => 'Kho câu hỏi',
                'rules' => 'trim|required',
                'errors' => [
                    'required' => 'Kho câu hỏi là bắt buộc.',
                ],
            ],            
        ];

        $this->form_validation->set_rules($config);

		//Danh sach kho cau hoi
		$data['categories'] = $this->question_model->getQuestionCategories([], 0);

		$data['questionItem'] = $question;
        if ($this->form_validation->run() == FALSE) {        	
	        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
	        $this->breadcrumbs->add('Ngân hàng câu hỏi', base_url().'question');
	        $this->breadcrumbs->add('Câu hỏi', base_url().'question/edit/' . $questionId);
	        $head['breadcrumb'] = $this->breadcrumbs->display();

			// Load View
			$this->load->view('layouts/header', $head);
			$this->load->view('question/edit', $data);
			$this->load->view('layouts/footer');	
        } else {
        	$postData = $this->input->post();
        	$postData = $this->nonXssData($postData);
        	$choicesAnswer = array();
        	$choicesAnswer['A'] = $postData['question_aw_a'];
        	$choicesAnswer['B'] = $postData['question_aw_b'];
        	$choicesAnswer['C'] = $postData['question_aw_c'];
        	$choicesAnswer['D'] = $postData['question_aw_d'];
        	$args['name'] = trim($postData['name']);
        	if($args['name'] != $question->name){        		
	        	if($this->question_model->checkExistQuestion($args['name']) == true){
	        		$message = sprintf(lang('exited_item'), $args['name'], lang('name'));     		
	        		$this->session->set_flashdata('error', $message);
	        		redirect(base_url() . 'question/edit/'. $questionId);
	        	}
        	}
        	$args['question'] = $postData['question'];
        	$args['choices_answer'] = serialize($choicesAnswer);
			$args['true_answer'] = $postData['true_answer']; 
			$args['question_category_id'] = $postData['question_category_id'];
			$args['link_media_answer'] = $postData['link_media_answer'];
			$args['score'] = $postData['score'];  			    	
        	if(isset($postData['link_video_answer']) && !empty($postData['link_video_answer'])){
        		$args['link_video_answer'] = $postData['link_video_answer'];
        	}
        	if(isset($postData['link_practice']) && !empty($postData['link_practice'])){
        		$args['link_practice'] = $postData['link_practice'];
        	}

        	if(isset($postData['status']) && !empty($postData['status'])){
        		$args['status'] = $postData['status'];
        	}

			if($this->question_model->updateQuestion($questionId, $args)){
            	$message = sprintf(lang('mes_update_success'), lang('question'));
            	$this->session->set_flashdata('success', $message);
            } else {
            	$message = sprintf(lang('mes_update_error'), lang('question'));
				$this->session->set_flashdata('error', $message);
			} 
			redirect(base_url() . 'question/edit/'.$questionId);
        }	
	}


    public function delcategory()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('question'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'question');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->question_model->deleteQuestionCategory( $where, true );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa thành công!');
                $this->session->set_flashdata('success', $message);
            } else {
                $this->session->set_flashdata('error', 'Không thể xóa bản ghi!');
            }
            echo 1;
            die;
        } else {
            $query = $this->question_model->deleteQuestionCategory( array( 'id' =>  $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa thành công!');
            } else {                
                $this->session->set_flashdata('error', 'Không thể xóa bản ghi!');
            }
            echo 1;
            die;
        }
    }

    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('question'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'question');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->question_model->deleteQuestion( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('question_question'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('question_question'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->question_model->deleteQuestion( array( 'id' =>  $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('question'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('question'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }

    public function changeStatusCategory()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['status'] = $status;	
				if($this->question_model->changeStatusCategory( $ids, $args))
				{	                
	                $this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
	            } else {	                
	                $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
	            }
                echo 1;
                die;
			}
		} else {	
			$args['status'] = ($status == 1) ? 0 : 1;		
			if( $this->question_model->changeStatusCategory( $ids, $args ) )
			{				
                $this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
            } else {                
                $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
            }
            echo 1;
            die;	
		}			
	}

    public function changeStatus()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['status'] = $status;	
				if($this->question_model->changeStatus( $ids, $args))
				{
	                $message = sprintf(lang('message_change_success'), lang('question_question'));
	                $this->session->set_flashdata('success', $message);
	            } else {
	                $message = sprintf(lang('message_change_error'), lang('question_question'));
	                $this->session->set_flashdata('error', $message);
	            }
                echo 1;
                die;
			}
		} else {	
			$args['status'] = ($status == 1) ? 0 : 1;		
			if( $this->question_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('question'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('question'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;	
		}			
	}


	public function batch()
	{
		$categories = $this->question_model->getQuestionCategories([], 0);
		$k = 0;
		while ($k < count($categories)) {			
			$i = 0;
			while($i < 2000) {
				$i++;
				$args = [];
				$args['name'] = 'CH ' .$i . '-' . $categories[$k]->code;
	        	$args['question'] = 'CH ' .$i . '-' . $categories[$k]->code;
	        	$args['choices_answer'] = '';
				$args['true_answer'] = 'A';
				$args['link_media_answer'] = '#';
				$args['score'] = 10;
				$args['type'] = 1;
				$args['question_category_id'] = $categories[$k]->id;				
				$this->question_model->addQuestion($args);
			}

			$k++;
		}
	}
}