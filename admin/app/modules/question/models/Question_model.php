<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @package Question_model < Multiple-choice questions Model > 
 * @author Trung Doan <trung.doan@bigshare.vn>
 * @date 3/4/2016
 */
class Question_model extends Bigshare_Model
{
    public function __construct()
    {            
            parent::__construct();
            $this->_table = 'questions';
    }

    public function addQuestionCategory($args = [])
    {   
        if (empty($args)) {
            return false;
        }
        $this->_table = 'question_categories';
        $args['created_at'] = date('Y-m-d H:i:s');
        $this->_data = $args;
        if($id = $this->addRecord()){            
            return $id;
        }
        return false;
    }

    public function updateQuestionCategory($id = null, $args = [])
    {            
        if (empty($args) || $id == null) {
            return false;
        }

        if (isset($args['id'])) {
            unset($args['id']);
        }

        if (is_null($this->getQuestionCategory($id))) {
            return false;
        }

        $where = [];

        $where['id'] = $id;
        $args['updated_at'] = date('Y-m-d H:i:s');
        $this->_wheres = $where;
        $this->_data = $args;
        $this->_table = 'question_categories';

        if ($this->updateRecord()) {
            return true;
        }

        return false;
    }

    public function getQuestionCategory($id)
    {
        $this->_table = 'question_categories';
        $where = [];
        $where['id'] = $id;
        $this->_wheres = $where;
        $item = $this->getOneRecord();
        return $item;        
    }

    public function numRowCategory($where = [])
    {
        $this->_table = 'question_categories';
        $this->_wheres = $where;
        return $this->totalRecord();
    }

    public function getQuestionCategories($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {       
        $this->resetQuery();
        $this->_selectItem = "*";
        $this->_table = 'question_categories';
        $this->_from = 'question_categories';
        if (isset($likes['field'])) {
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $likes['value'];
        }

        if (isset($likes['field'])) {
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str($likes['value']);
        }

        $this->_table = 'question_categories';
        $this->_order = $orderBy;
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }    

    public function deleteQuestionCategory($where = [], $multiple = false)
    {
        $this->_table = 'question_categories';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }


    public function addQuestion($args = [])
    {   
        if(empty($args)){
            return false;
        }
        $this->_table = 'questions';
        $args['created_at'] = date('Y-m-d H:i:s');
        $this->_data = $args;
        if($id = $this->addRecord()){            
            return $id;
        }
        return false;
    }

    public function updateQuestion($id = null, $args = [])
    {            
        if(empty($args) || $id == null) return false;        
        if(isset($args['id'])) unset($args['id']);
        if(is_int($id)){
            $id = md5($id);
        }            
        if(is_null($this->getOneQuestion($id))){
            return false;
        }           
        $where = [];
        $where['id'] = $id;
        $args['updated_at'] = date('Y-m-d H:i:s');
        $this->_wheres = $where;
        $this->_data = $args;
        $this->_table = 'questions';
        if($this->updateRecord()){
            return true;
        }
        return false;
    }


    public function getOneQuestion($id)
    {
        $this->_table = 'questions';
        if(is_int($id)){
            $id = md5($id);
        }
        $where = [];
        $where['id'] = $id;
        $this->_wheres = $where;
        $item = $this->getOneRecord();
        return $item;        
    }

    public function checkExistQuestion($title)
    {
        $this->_table = 'questions';        
        $where = [];
        $where['name'] = trim($title);
        $this->_wheres = $where;
        $item = $this->getOneRecord();
        if(!empty($item)) return true;
        return false;
    }

    public function numRows($where = [], $likes = [])
    {
        $this->_table = 'questions';
        $this->_wheres = $where;
        $this->_from = 'questions'; 

        $joins = [];
        $joinCategory['table'] = 'question_categories';
        $joinCategory['condition'] = 'question_categories.id = questions.question_category_id';
        $joinCategory['type'] = 'inner';
        $joins[] = $joinCategory;
        $this->_joins = $joins; 
        if (!empty($likes) &&  isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        
        return $this->totalRecord();
    }

    public function getQuestions($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {       
        $this->resetQuery();
        $this->_table = 'questions';
        $this->_selectItem = "questions.id, questions.name, questions.type, questions.true_answer, questions.question_category_id, questions.status, questions.created_at, questions.updated_at, questions.deleted_at, question_categories.name as cate_name";
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $likes['value'];
        }
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_from = 'questions'; 

        $joins = [];
        $joinCategory['table'] = 'question_categories';
        $joinCategory['condition'] = 'question_categories.id = questions.question_category_id';
        $joinCategory['type'] = 'inner';
        $joins[] = $joinCategory;
        $this->_joins = $joins; 
        $this->_order = $orderBy;
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }    

    public function deleteQuestion( $where = [], $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
 
    public function changeStatus( $ids, $args = [] )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function changeStatusCategory( $ids, $args = [] )
    {
        $this->_data = $args;
        $this->_table = 'question_categories';
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }    
}