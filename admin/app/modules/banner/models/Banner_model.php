<?php defined('BASEPATH') or exit('No direct script access allowed');

class Banner_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_table = 'banners';
    }
    
    /**
     * @name insertBanner
     * @desc insert new banner in banner table
     * @param [array] $args
     * */
    public function insertBanner( $args = array() )
    {
        $this->_data = $args;
		if($id = $this->addRecord() ){
			return $id;
		}
		return false;
    }
    
    /**
     * @name getBanners
     * @desc get all row from banner table
     * @param [array] $where
     * */
    public function getBanners( $where = array(), $limit = 0, $offset = 0, $likes = array() )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = $this->_table;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
	 * [totalBanners]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalBanners($where = array())
	{
		$this->_wheres = $where;
		return $this->totalRecord();
	}
    /**
     * @name changeStatus
     * @desc change status a row or any rows from banner table
     * @param [array] $where
     * */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    /**
     * @name deleteBanner
     * @desc delete a row from banner table
     * @param [array] $where
     * */
    public function deleteBanner( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
    
    
    public function getBanner( $where = array() )
    {
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    
    public function updateBanner( $where = array(), $args = array() )
    {
        $this->_table = 'banners';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
    public function getItems( $where, $limit = 0, $likes )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_limit = $limit;
        $this->_from = 'banner_items';
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
     * @name insertBannerItem
     * @desc insert new item in banner_items table
     * @param [array] $args
     * */
    public function insertBannerItem( $args = array() )
    {
        $this->_table = 'banner_items';
        $this->_data = $args;
        if($id = $this->addRecord() ){
            return $id;
        }
        return false;
    }
    public function getBannerItem( $where = array() )
    {
        $this->_table = 'banner_items';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
     public function updateBannerItem( $where = array(), $args = array() )
    {
        $this->_table = 'banner_items';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
    /**
     * @name changeStatusItem
     * @desc change status a row or any rows from banner_items table
     * @param [array] $where
     * */
    public function changeStatusItem( $ids, $args = array() )
    {
        $this->_table = 'banner_items';
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    /**
     * @name deleteBannerItem
     * @desc delete a row from banner_items table
     * @param [array] $where
     * */
    public function deleteBannerItem( $where = array(), $multiple = false )
    {
        $this->_table = 'banner_items';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }
}