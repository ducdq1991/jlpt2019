<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-picture-o" aria-hidden="true"></i> <?php echo lang('banner_management');?> - <?php echo $parent_name; ?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-left btn-function" style="display:none;">
                        <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'banner/del'; ?>">
                        <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                        <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'banner/changestatus'; ?>">
                        <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                        <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'banner/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a> 
                    </div>    
                    <div class="pull-right form-banner-action">
                        <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">
                        <select name="status" class="form-control input-sm sys-filler">
                            <?php foreach($this->config->item('statusConfigs') as $key=>$value):?>
                            <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>
                            <?php endforeach;?>
                        </select>                             
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                        <a href="<?php echo base_url();?>banner" class="btn btn-white btn-sm">
                        <i class="fa fa-reply"></i> <?php echo lang('exit');?></a>
                        <a href="<?php echo base_url();?>banner/add_item/<?php echo $parent_id;?>" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                    </div>
                    <table class="table table-bordered table-striped dataTable banner-table" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('title');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('desc');?></th>
                                <th width="220px" tabindex="0" rowspan="1" colspan="1"><?php echo lang('image');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('link');?></th>
                                <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('status');?></th>
                                <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($items as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->title;?></td>
                                    <td><?php echo $row->desc;?></td>
                                    <td>
                                        <img class="banner-thumb" src="<?php echo $baseUrl.'/'.$row->image;?>">
                                    </td>
                                    <td>
                                        <?php echo $row->link;?>
                                    </td>
                                    <td class="text-center">
                                        <?php if($row->status == 1):?>
                                            <i onclick="changeState('<?php echo base_url().'banner/changestatusitem'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="banner đã kích hoạt" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeState('<?php echo base_url().'banner/changestatusitem'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="banner đã bị vô hiệu" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="text-center">                        
                                        <a href="<?php echo base_url();?>banner/edit_item/<?php echo $row->id;?>" title="" class="btn btn-xs btn-default">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" onclick="delSingle('<?php echo base_url('banner/del_item'); ?>', <?php echo $row->id; ?>)" title="">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
