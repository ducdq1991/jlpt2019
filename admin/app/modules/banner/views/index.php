<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-buysellads"></i> <?php echo lang('banner_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-left btn-function" style="display:none;">
                        <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'banner/del'; ?>">
                        <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                        <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'banner/changestatus'; ?>">
                        <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                        <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'banner/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a> 
                    </div>    
                    <div class="pull-right form-banner-action">
                        <?php echo lang('num_records');?>:
                        <select name="numRecord" class="form-control input-sm sys-filler">                                
                            <option value="">--<?php echo lang('num_rows');?>--</option>
                            <?php foreach($itemPerPages as $item):?>
                            <option value="<?php echo $item;?>" <?php if($numRecord == $item):?> selected="selected" <?php endif;?>><?php echo $item;?></option>
                            <?php endforeach;?>
                        </select>                         
                        <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">
                        <select name="type" class="form-control input-sm sys-filler">
                            <option value=""><?php echo lang('choose_type');?></option>
                            <?php foreach($this->config->item('bannerType') as $key=>$value):?>
                            <option value="<?php echo $key;?>" <?php if($key == $type):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>
                            <?php endforeach;?>
                        </select>
                        <select name="status" class="form-control input-sm sys-filler">
                            <?php foreach($this->config->item('statusConfigs') as $key=>$value):?>
                            <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>
                            <?php endforeach;?>
                        </select>                             
                        <input type="submit" value="<?php echo lang('filter');?>" class="btn btn-warning btn-sm">
                        <a href="<?php echo base_url();?>banner/add" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                    </div>
                    <table class="table table-bordered table-striped dataTable banner-table" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('title');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('type');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('group');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('status');?></th>
                                <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($banners as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->title;?></td>
                                    <td>
                                        <?php
                                        $bannerType = $this->config->item('bannerType');
                                        echo $bannerType[$row->type];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if($row->group) {
                                            $bannerPosition = $this->config->item('bannerPosition');
                                            echo $bannerPosition[$row->group];
                                        }
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php if($row->status == 1):?>
                                            <i onclick="changeState('<?php echo base_url().'banner/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="banner đã kích hoạt" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeState('<?php echo base_url().'banner/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="banner đã bị vô hiệu" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="center">
                                        <a href="<?php echo base_url();?>banner/items/<?php echo $row->id;?>" title="" class="btn btn-xs btn-success">
                                                <i class="fa fa-list-ul"></i>
                                        </a>                                    
                                        <a href="<?php echo base_url();?>banner/edit/<?php echo $row->id;?>" title="" class="btn btn-xs btn-default">
                                                <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger" onclick="delSingle('<?php echo base_url('banner/del'); ?>', <?php echo $row->id; ?>)" title="">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                    <div class="paging">
                        <?php echo $paging;?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
