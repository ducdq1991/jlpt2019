<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('edit_banner');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title"><?php echo lang('title');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" class="form-control" id="title" value="<?php echo set_value('title', $banner->title); ?>" placeholder="">
                            <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="type"><?php echo lang('type');?>:</label>
                        <div class="col-sm-8">
                            <select name="type" id="type" class="form-control">
                                <option value="">--<?php echo lang('choose_type');?>--</option>
                                <?php
                                    $tyepOptions = $this->config->item('bannerType');
                                    foreach ($tyepOptions as $key => $value) :
                                ?>
                                <option value="<?php echo $key;?>" <?php echo set_select('type', $key, $key==$banner->type); ?>><?php echo lang($value);?></option>
                                <?php endforeach; ?>
                            </select>
                            <?php echo form_error('type', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="group"><?php echo lang('group');?>:</label>
                        <div class="col-sm-8">
                            <select name="group" id="group" class="form-control">
                                <option value="">--<?php echo lang('choose_group');?>--</option>
                                <?php
                                $bannerPosition = $this->config->item('bannerPosition');
                                foreach ($bannerPosition as $key => $value) :
                                ?>
                                <option value="<?php echo $key;?>" <?php echo set_select('group', $key, $key==$banner->group); ?>><?php echo $value;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="method"><?php echo lang('status');?>:</label>
                        <div class="col-sm-8">
                            <label><input type="checkbox" class="" value="1" name="status" id="status" <?php echo set_checkbox('status', '1', $banner->status == 1); ?>><?php echo lang('active');?></label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('update');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var _baseUrl =  '<?php echo $baseUrl;?>'; 
</script>