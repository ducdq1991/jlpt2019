<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('edit_banner');?></h5>
            </div>
            <div class="ibox-content">
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title"><?php echo lang('title');?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="title" class="form-control" id="title" value="<?php echo set_value('title', $banner->title); ?>" placeholder="">
                            <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="image"><?php echo lang('image'); ?> <span>*</span>:</label>
                        <div class="col-sm-8">
                            <div id="load-image" class="load-image-banner">
                                <!-- section display image uploaded -->
                            </div>
                            <input type="hidden" name="image" value="<?php echo set_value('image', $banner->image); ?>" id="image" />
                            <button type="button" id="load-form-featured-image" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredImageModal"><?php echo lang('choose_button'); ?></button>
                            <button type="button" id="remove-featured-image" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_button'); ?></button>
                            <?php echo form_error('image', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="link"><?php echo lang('link', $banner->link);?>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="link" class="form-control" id="link" value="<?php echo set_value('link'); ?>" placeholder="E.g: http://sitename/param">
                            <?php echo form_error('link', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="desc"><?php echo lang('desc', $banner->desc);?>:</label>
                        <div class="col-sm-8">
                            <input type="text" name="desc" class="form-control" id="desc" value="<?php echo set_value('desc'); ?>" placeholder="">
                            <?php echo form_error('desc', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="ordering">Thứ tự hiển thị:</label>
                        <div class="col-sm-8">
                            <input type="text" name="ordering" class="form-control" id="ordering" value="<?php echo set_value('ordering', $banner->ordering); ?>">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="method"><?php echo lang('status');?>:</label>
                        <div class="col-sm-8">
                            <label><input type="checkbox" class="" value="1" name="status" id="status" <?php echo set_checkbox('status', '1', $banner->status == 1); ?>><?php echo lang('active');?></label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" class="btn btn-info"><?php echo lang('update');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Featured Image Modal -->
<div id="featuredImageModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-image-iframe" src="<?php echo base_url(); ?>assets/js/filemanager/dialog.php?field_id=image" width="100%" height="450px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<script>
    var _baseUrl =  '<?php echo $baseUrl;?>'; 
</script>