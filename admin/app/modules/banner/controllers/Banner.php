<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Banner
* @author ChienDau
* @copyright Bigshare
* 
*/

class Banner extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('banner_model');
        $this->lang->load('banner', $this->session->userdata('lang'));
	}
    
    /**
     * @name Index
     * @desc show all of Banner has registed
     * */
    public function index( $page = 0 )
    {
        $data = $where = $likes = [];
       	$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));
		
		if($page < 1) $page = 1;
		$offset = ($page - 1) * $limit;
        
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
		$status = $this->input->get('status');
        $type = (int) $this->input->get('type');
        $data['status'] = $status;
        $data['type'] = $type;
		if($status != ''){
		  $where['banners.status'] = ($status ==='active') ? 1 : 0;
		}
        if($type != ''){
          $where['banners.type'] = $type;
        }
        if(!empty($keyword)){
            $likes['field'] = 'banner.title';
            $likes['value'] = $keyword;
        }  
        $numRecords = $this->banner_model->totalBanners( $where );      
		$data['paging'] = paging(base_url() .'banner', $page, $numRecords, $limit); 
		$banners = $this->banner_model->getBanners( $where, $limit, $offset, $likes );
        $data['banners'] = $banners;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
        $headers['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $headers);
        $this->load->view('banner/index', $data);
        $this->load->view('layouts/footer');
    }
    
    /**
     * @name Add
     * @desc register a new Banner
     * */
    public function add()
    {
        $data = array(
            'base_url' => $this->getBaseUrl(),
        );
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'type',
                'label' => lang('type'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
            $this->breadcrumbs->add(lang('add_new_banner'), base_url().'banner/add');
            $headers['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $headers);
            $this->load->view('banner/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            if( $this->banner_model->insertBanner( $info ) ) {
                $message = sprintf(lang('message_create_success'), lang('the_banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), lang('the_banner'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'banner');
        }
    }
    
    public function edit( $id )
    {
        $data = array();
        $banner = $this->banner_model->getBanner( array('id' => (int) $id) );
        if(empty($banner)) {
            redirect(base_url() . 'banner');
        }
        $data['baseUrl'] = $this->getBaseUrl();
        $data['banner'] = $banner;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'type',
                'label' => lang('type'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
            $this->breadcrumbs->add(lang('edit_banner') . ' | ' . $banner->title, base_url().'banner/edit/' . $banner->id);
            $headers['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $headers);
            $this->load->view('banner/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            if( $this->banner_model->updateBanner( array('id' => (int) $id), $info ) ) {
                $message = sprintf(lang('message_update_success'), lang('the_banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_banner'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'banner');
        }
    }
    /**
     * @name Del
     * @desc delete a Banner from banner table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('banner'));
            $this->session->set_flashdata('warning', $message);
            echo 1;
            die;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->banner_model->deleteBanner( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->banner_model->deleteBanner( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    
    /**
     * @name changeStatus
     * @desc change status a Banner from banner table
     * */
    public function changestatus()
	{
		$args = array();
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
			return false;
		}
		if( isset($type) && !empty($type) && $type =='multi' ){				
			if( !empty($ids) ){
				$args['status'] = $status;	
				if($this->banner_model->changeStatus( $ids, $args))
				{
                    $message = sprintf(lang('message_change_success'), lang('banner'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('banner'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
			}
		} else {	
			$args['status'] = ($status == 1) ? 0 : 1;		
			if( $this->banner_model->changeStatus( $ids, $args ) )
			{
				$message = sprintf(lang('message_change_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;	
		}			
	}
    /**
     * @name Item
     * @desc show all of banner that is child of them parent has registed
     * */
    public function items( $parent = 0 )
    {
        if(!$parent){
            redirect(base_url().'banner');
        }
        $banner = $this->banner_model->getBanner( array('id' => (int) $parent) );
        $data = $where = $likes = array();
        $data['parent_name'] = $banner->title;
        $data['baseUrl'] = $this->getBaseUrl();
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['banner_items.status'] = ($status ==='active') ? 1 : 0;
        }
        if(!empty($keyword)){
            $likes['field'] = 'banner_items.title';
            $likes['value'] = $keyword;
        }
        $where['parent'] = (int) $parent;
        $items = $this->banner_model->getItems( $where, $limit = 0, $likes );
        $data['items'] = $items;
        $data['parent_id'] = (int) $parent;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
        $this->breadcrumbs->add($banner->title, base_url().'banner/items/' . $parent);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('banner/items', $data);
        $this->load->view('layouts/footer');
    }
    /**
     * @name Add
     * @desc register a new Banner
     * */
    public function add_item( $parent = 0 )
    {
        if(!$parent) {
            redirect(base_url('banner'));
            return false;
        }
        $data = array(
            'base_url' => $this->getBaseUrl(),
        );
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'image',
                'label' => lang('image'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'link',
                'label' => lang('link'),
                'rules' => 'trim|valid_url',
                'errors' => array(
                    'valid_url' => lang('mes_valid_url'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
            $this->breadcrumbs->add(lang('banner_management'), base_url().'banner/add_item/' . $parent);
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('banner/add_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $info['parent'] = (int) $parent;
            if( $this->banner_model->insertBannerItem( $info ) ) {
                $message = sprintf(lang('message_create_success'), lang('the_banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), lang('the_banner'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'banner/items/'.$parent);
        }
    }
    
    public function edit_item( $id = 0 )
    {
        $data = array();
        $banner = $this->banner_model->getBannerItem( array('id' => (int) $id) );
        if(empty($banner)) {
            redirect(base_url() . 'banner');
        }
        $data['baseUrl'] = $this->getBaseUrl();
        $data['banner'] = $banner;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
            array(
                'field' => 'image',
                'label' => lang('image'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ), 
            array(
                'field' => 'link',
                'label' => lang('link'),
                'rules' => 'trim|valid_url',
                'errors' => array(
                    'valid_url' => lang('mes_valid_url'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {

            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('banner_management'), base_url().'banner');
            $this->breadcrumbs->add($banner->title, base_url().'banner/edit_item/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('banner/edit_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            if( $this->banner_model->updateBannerItem( array('id' => (int) $id), $info ) ) {
                $message = sprintf(lang('message_update_success'), lang('the_banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_banner'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'banner/items/'.$banner->parent);
        }
    }
    /**
     * @name Del
     * @desc delete a Banner from banner table
     * */
    public function del_item()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('banner'));
            $this->session->set_flashdata('warning', $message);
            echo 1;
            die;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->banner_model->deleteBannerItem( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->banner_model->deleteBannerItem( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        }
    }
    
    /**
     * @name changeStatus
     * @desc change status a Banner from banner table
     * */
    public function changeStatusItem()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->banner_model->changeStatusItem( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('banner'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('banner'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->banner_model->changeStatusItem( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('banner'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('banner'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }
}