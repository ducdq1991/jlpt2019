<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('media'); ?></h5>
            </div>
            <div class="ibox-content">
                <?php echo $mediaFrame; ?>
            </div>
        </div>
    </div>
</div>