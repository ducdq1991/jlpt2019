<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Media
* @author ChienDau
* @copyright Bigshare
* 
*/

class Media extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
	}
    public function index()
    {
        $data = array();
        $media = '<iframe src="' . base_url('assets/js/filemanager/dialog.php') . '" style="border: 1px solid #f0f0f0; min-height: 400px" width="100%"></iframe>';
        $data['mediaFrame'] = $media;
        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('media'), base_url().'media');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
                
        $this->load->view('layouts/header', $head);
        $this->load->view('media/index', $data);
        $this->load->view('layouts/footer');
    }
}