<?php defined('BASEPATH') or exit('No direct script access allowed');

class Exam_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'exam';
    }
    
    /**
     * [getOneMcq]
     * @param  [int or string] $id
     * @return [array or null]
     */
    public function getOneMcq( $id )
    {
        $this->_table = 'questions';
        $where['id'] = (int) $id;
        $this->_wheres = $where;
        return $this->getOneRecord();      
    }

    public function getListQuestion($where = array(), $limit = 10, $offset = 0, $likes = [])
    {       
        $this->resetQuery();
        $this->_selectItem = "questions.id, questions.name";
        $this->_from = 'questions';    
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        if (!empty($likes)) {
            $this->_likes = $likes;
        }
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }

    public function getGroupQuestion($ids = array())
    {   
        $result = [];
        if ($ids) {
            foreach ($ids as $id) {
                $item = $this->getOneMcq($id);
                $result[] = $item;
            }
        }
        return $result;
    }

    public function insertExam( $args = array())
    {
        $this->_table = 'exam';
        $this->_data = $args;
        return $id = $this->addRecord();
    }
    /*
    * getExams
    * get all multi choice exam from exam table
    */
    public function getExams( $where = array(), $limit = 10, $offset = 0, $orderBy = array(), $likes = array() )
    {
        $this->_selectItem = "exam.id, exam.title, exam.type, exam.extend_question_config, exam.publish_up, exam.publish_down, exam.status, course_details.name AS course, exam.ordering";
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_from = 'exam';
        $joins = array();
        $joinCourseDetail['table'] = 'course_details';
        $joinCourseDetail['condition'] = 'exam.course_id = course_details.course_id';
        $joinCourseDetail['type'] = 'left';
        $joins[] = $joinCourseDetail;
        $this->_joins = $joins;     
        $this->_order = $orderBy;
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
     * [totalExam]
     * @param  array  $where [description]
     * @return [int]
     */
    
    public function totalExam($where = array())
    {
        $this->_table = 'exam';
        $this->_wheres = $where;
        return $this->totalRecord();
    }
    /**
     * [delCourse delete a/any Exam]
     * @param  [int] $courseId
     * @return [boolean]
     */
    public function deleteExam( $where = array(), $multiple = false )
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }

    /**
     * [changeStatus Of a/any Exam]
     * @param  [int or array] $courseIds
     * @param  [array] $courseUpdate
     * @return [boolean]
     */
    public function changeStatus( $ids, $args = array() )
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getExam($where = [])
    {
        if(!$id){
            return false;
        }
        $this->_table = 'exam';
        $where = array();
        $this->_wheres = $where;
        return $this->getOneRecord();
    }

    public function getExamById( $id )
    {
        if(!$id){
            return false;
        }
        $this->_table = 'exam';
        $where = array();
        $where['id'] = (int) $id;
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
    public function updateExam( $id, $args )
    {
        $this->_table = 'exam';
        $where = array();
        $where['id'] = (int) $id;
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
}