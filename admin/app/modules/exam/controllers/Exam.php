<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package exam
* @author ChienDau
* @copyright Bigshare
* 
*/

class Exam extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('exam_model');
        $this->lang->load('exam', $this->session->userdata('lang'));
        $this->load->model('course/course_model');
        $this->load->model('question/question_model');
	}
	/**
	* index
	* Default page for exam package, list all record of exam table
	*/
	public function index($page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];
        $where['attribute'] = 1;
        $where['exam.type'] = 'online-exam';


        $courseId = $this->input->get('course_id', 0);
        $courseOption = $this->course_model->getCourseOption(0, $courseId);
        $data['courseOption'] = $courseOption;

		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		if ($page < 1) {
            $page = 1;
        }
		$offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		
		$type = $this->input->get('type');
		$data['type'] = $type;

        $status = $this->input->get('status');
		if($status != ''){
		  $where['exam.status'] = ($status ==='active') ? 1 : 0;
		}
        $data['status'] = $status;		
		
        $keyword = $this->input->get('keyword');
		if($keyword !=''){
			$likes['field'] = 'exam.title';
			$likes['value'] = (string) trim($keyword);
		}
        $data['keyword'] = $keyword;
				
		$total = $this->exam_model->totalExam($where);
		$exams = $this->exam_model->getExams( $where, $limit, $offset, $orderCondition, $likes );
        $data['exams'] = $exams;

        $showFrom = $offset + count($exams);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
		
        $queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'exam', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add(lang('exam_management'), base_url().'exam');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
        $this->load->view('exam/index', $data);
        $this->load->view('layouts/footer');
	}
	/**
	* Add
	* Insert a new exam into exam table
	*/
	public function add()
	{
		$data = array();        
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array( 'status' => 1, 'type' => 1);
        $questions = $this->exam_model->getListQuestion($where);
        $data['questions'] = $questions;

        //Get question categories
        $questionCategories = $this->question_model->getQuestionCategories([], 0, 0, ['id' => 'asc']);        
        $data['questionCategories'] = $questionCategories;

        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exam.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exam.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'exam_code',
                'label' => lang('exam_code'),
                'rules' => 'trim|required|is_unique[exam.exam_code]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'time',
                'label' => 'Thời gian làm bài',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Thời gian làm bài là bắt buộc',                    
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('exam_management'), base_url().'exam');
            $this->breadcrumbs->add('Tạo đề thi trắc nghiệm', base_url().'exam/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('exam/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $exam = array();
            $exam = $this->input->post();
            $exam = $this->nonXssData($exam);
            $exam['time'] = (int) $exam['time'];
            foreach ($exam as $key => $value) {
            	if( $key == 'publish_up' ) {
            		if( empty($value) ) {
                        $exam['publish_up'] = date('Y-m-d H:i:s');
                    } else {
                        $date=date_create($value);
                        $exam['publish_up'] = date_format($date,"Y-m-d H:i:s");
                    }
            	} elseif ( $key == 'publish_down') {
                    if( !empty($value) ) {
                        $date=date_create($value);
                        $exam['publish_down'] = date_format($date,"Y-m-d H:i:s");
                    }
                } elseif ( empty($value) )
            		unset($exam[$key]);
            }

            $exam['publish_down'] = date('Y-m-d H:i:s', strtotime($exam['publish_up']) + ($exam['time'] * 60));

            $exam['attribute'] = 1;
            $exam['extend_question_config'] = serialize($exam['extend_question_config']);
            $examId = $this->exam_model->insertExam( $exam );
            if( $examId ) {
                $message = sprintf(lang('message_create_success'), lang('new_exam'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'exam/edit/' . $examId);
            } else {
                $message = sprintf(lang('message_create_error'), lang('new_exam'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'exam');
        }
	}

    public function getmatchnamequestion()
    {
        $res = [
            'code' => 0,
        ];
        $where = array( 'status' => 1, 'type' => 1);
        $match = $this->input->post('match');
        $likes = [
            'field' => 'mcq_questions.title',
            'value' => $match,
        ];
        $questions = $this->exam_model->getListQuestion($where, $limit = 0, $offset = 0, $likes);
        if ($questions !== null) {
            $res['code'] = 1;
            $res['data'] = $questions;
        }
        echo json_encode($res);die;
    }

    public function validate_publish_down($publish_down)
    {
        $publish_up = $this->input->post('publish_up');
        if( strtotime($publish_down) - strtotime($publish_up) > 0 ) {
            return true;
        }
        $this->form_validation->set_message('validate_publish_down', 'Ngày kết thúc phải lớn hơn ngày bắt đầu');
        return false;
    }

	public function getgroupquestion()
	{
		$ids = $this->input->post('ids');
        $ids = explode(',', $ids);
		$mcqQuestion = $this->exam_model->getGroupQuestion( $ids );
		if( !empty($mcqQuestion) ) {
            $html = '';
            foreach ($mcqQuestion as $item) {
    			
    			$html .= '<tr class="gradeA odd" role="row" id="row-'.$item->id.'">'; 
    		        $html .= '<td class="sorting_1">' . $item->id . '</td>';
    		        $html .= '<td>' . $item->title . '</td>';
    		        $html .= '<td>' . $item->level . '</td>';
    		        $html .= '<td>' . $item->true_answer . '</td>';
    		        $html .= '<td class="center text-center">';
                        $html .= '<a target="_blank" href="/admin/mcq/edit/'.$item->hash_id.'" class="btn btn-xs btn-default">';
                                $html .= '<i class="fa fa-edit"></i>';
                        $html .= '</a>   ';                    
    		            $html .= '<a title="">';
    		                $html .= '<button class="btn btn-xs btn-danger" data-id="'.$item->id.'" id="remove-question" type="button">';
    		                    $html .= '<i class="fa fa-trash"></i>';
    		                $html .= '</button>';
    		            $html .= '</a>';
    		        $html .= '</td>';
    		    $html .= '</tr>';
            }
	    	echo $html;
	    	die;
	    } else {
	    	echo 0;
	    	die;
	    }
	}

    public function getquestion()
    {
        $id = $this->input->post('id');
        $mcqQuestion = $this->exam_model->getOneMcq( $id );
        if( !empty($mcqQuestion) ) {
            $html = '';
            $html .= '<tr class="gradeA odd" role="row" id="row-'.$mcqQuestion->id.'">'; 
                $html .= '<td class="sorting_1">' . $mcqQuestion->id . '</td>';
                $html .= '<td>' . $mcqQuestion->title . '</td>';
                $html .= '<td>' . $mcqQuestion->level . '</td>';
                $html .= '<td>' . $mcqQuestion->true_answer . '</td>';
                $html .= '<td class="center text-center">';
                    $html .= '<a title="">';
                        $html .= '<button class="btn btn-xs btn-danger" data-id="'.$mcqQuestion->id.'" id="remove-question" type="button">';
                            $html .= '<i class="fa fa-trash"></i>';
                        $html .= '</button>';
                    $html .= '</a>';
                $html .= '</td>';
            $html .= '</tr>';
            echo $html;
            die;
        } else {
            echo 0;
            die;
        }
    }
    
	/**
     * @name delete
     * Delete a exam or any exam table by id
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'exam');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->exam_model->deleteExam( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('exams'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('exams'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->exam_model->deleteExam( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 2;
            die;
        }
    }
    /*
    * changeFeatured
    * activate, deactivate a featured article
    */
    /**
     * [changeStatus of Course]
     * @return [int]
     */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->exam_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('exams'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('exams'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->exam_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }
    /**
	* Edit
	* update a exam info to exam table
	*/
	public function edit( $id )
	{
		$exam = $this->exam_model->getExamById( (int) $id );
		if( !$exam ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'exam');
            return false;
        }
        $date=date_create($exam->publish_up);
        $exam->publish_up = date_format($date,"d-m-Y H:i");
        if($exam->publish_down != 0) {
            $date=date_create($exam->publish_down);
            $exam->publish_down = date_format($date,"d-m-Y H:i");
        }
		$data = array();        
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array( 'status' => 1, 'type' => 1);
        $questions = $this->exam_model->getListQuestion($where);
        $data['questions'] = $questions;
        $data['exam'] = $exam;

        //Get question categories
        $questionCategories = $this->question_model->getQuestionCategories([], 0, 0, ['id' => 'asc']);
        $data['questionCategories'] = $questionCategories;

        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exam.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exam.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'exam_code',
                'label' => lang('exam_code'),
                'rules' => 'trim|required|is_unique[exam.exam_code]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'time',
                'label' => 'Thời gian làm bài',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Thời gian làm bài là bắt buộc',                    
                ),
            ),
        );
        $info = array();
        $info = $this->input->post(array('title', 'slug', 'exam_code'));
        if( $info['title'] == $exam->title ) {
        	unset($config[0]);
        }
        if( $info['slug'] == $exam->slug ) {
        	unset($config[1]);
        }
        if( $info['exam_code'] == $exam->exam_code ) {
        	unset($config[2]);
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add(lang('exam_management'), base_url().'exam');
            $this->breadcrumbs->add('Sửa đề thi trắc nghiệm', base_url().'exam/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('exam/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $info['time'] = (int) $info['time'];
            if (isset($info['is_random'])) {
                $info['is_random'] = (int) $info['is_random'];
            } else {
                $info['is_random'] = 0;
            }
            
            if( empty($info['publish_up']) ) {
                $info['publish_up'] = date('Y-m-d H:i:s');
            } else {
                $date=date_create($info['publish_up']);
                $info['publish_up'] = date_format($date,"Y-m-d H:i:s");
            }
            if( !empty($info['publish_down']) ) {
                $date=date_create($info['publish_down']);
                $info['publish_down'] = date_format($date,"Y-m-d H:i:s");
            }

            $info['publish_down'] = date('Y-m-d H:i:s', strtotime($info['publish_up']) + ($info['time'] * 60));

            $info['extend_question_config'] = serialize($info['extend_question_config']);
            $examId = $this->exam_model->updateExam( $id, $info );
            if( $examId ) {
                $message = sprintf(lang('message_update_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'exam/edit/' . $id);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'exam');
        }
	}

    public function copy($id = 0)
    {
        $exam = $this->exam_model->getExamById( (int) $id );
        if( !$exam ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'exam');
            return false;
        }
        $rand = rand(1,100);
        $exam->title = $exam->title.'-copy' . $rand;
        $exam->exam_code = $exam->exam_code . $rand;
        $exam->status = 0;
        $args = (array) $exam;        
        unset($args['id']);
        $where = ['title' => $args['title']];
        $checkexist = $this->exam_model->getExam($where);
        if (empty($checkexist)) {
            if($examId = $this->exam_model->insertExam($args)){    
                $updates = [
                    'hash_id' => md5($examId)
                ];          
                $this->exam_model->updateExam($examId, $args);
                $this->session->set_flashdata('success', 'Đã Copy đề thi thành công!');
            } else {                
                $this->session->set_flashdata('error', 'Không Copy được đề thi');
            } 
        } else {
            $this->session->set_flashdata('error', 'Đề này đã tồn tại');
        }    

        redirect(base_url() . 'exam');
    }    
}