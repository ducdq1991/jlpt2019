<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> Tạo đề thi trắc nghiệm mới</h5>
            </div>
            <div class="ibox-content">
                <form style="display: table; width: 100%" id="form-add-exam" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="text-right col-md-12">
                        <a href="<?php echo base_url('exam');?>" class="btn btn-sm btn-white"><i class="fa fa-reply"></i> <?php echo lang('exit'); ?></a>
                        <button type="button" id="submit_exam" class="btn btn-sm btn-info"><?php echo lang('submit'); ?></button>
                    </div>                
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Thể loại<span> *</span></label>
                            <div class="col-sm-10">
                                <select name="type" class="form-control">
                                    <option value="exercise">Luyện Tập</option>
                                    <option value="online-exam">Thi Online</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="autoSlug auto-exam-slug form-control" id="title" value="<?php echo set_value('title'); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="slug"><?php echo lang('alias'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="slug" class="slug slug-exam form-control" id="slug" value="<?php echo set_value('slug'); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="exam_code"><?php echo lang('exam_code'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="exam_code" class="form-control" id="exam_code" value="<?php echo set_value('exam_code'); ?>" placeholder="">
                                <?php echo form_error('exam_code', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="content_file"><?php echo lang('exam_upload'); ?></label>
                            <div class="col-sm-10">
                                <div id="load-content-file">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="content_file" value="<?php echo set_value('content_file'); ?>" id="content_file" />
                                <button type="button" id="load-form-content-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#contentFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-content-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                                <?php echo form_error('content_file', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="answer_file"><?php echo lang('answer_upload'); ?></label>
                            <div class="col-sm-10">
                                <div id="load-up-file-answer">
                                    
                                </div>
                                <input type="hidden" name="answer_file" value="<?php echo set_value('answer_file'); ?>" id="answer_file" />
                                <button type="button" id="load-form-answer-up-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#answerUpFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-answer-up-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" id="publish-up-exam">
                            <label class="col-sm-3 control-label"><?php echo lang('time_start'); ?></label>
                            <div class="input-group date col-sm-9">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="publish_up" class="form-control" value="<?php echo set_value('publish_up'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="time"><?php echo lang('time'); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="time" class="form-control" id="time" value="<?php echo set_value('time'); ?>" placeholder="">
                                <?php echo form_error('time', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>                        

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('course_thematic'); ?></label>
                            <div class="col-sm-9">
                                <select class="select2 form-control m-b" name="course_id" id="course_id">
                                    <option value="">--<?php echo lang('choose_course'); ?>--</option>
                                    <?php echo $courseOption;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('status'); ?></label>
                            <div class="col-sm-9">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php echo set_checkbox('status', '1'); ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row questionConfig">
                        <h5 class="text-center">Thiết lập kho câu hỏi tự động</h5>
                        <?php
                            if (isset($questionCategories) && count($questionCategories) > 0) :
                                foreach ($questionCategories as $cate):
                        ?>
                            <div style="width: 300px; float: left;margin:10px;">
                                <div class="input-group">
                                    
                                    <span class="input-group-addon"><?php echo $cate->name;?></span>
                                    <input type="text" name="extend_question_config[<?php echo $cate->id;?>]" class="form-control" value="0">
                                </div>
                            </div>
                        <?php 
                                endforeach;
                            endif;
                        ?>
                        </div>
                    </div>                    
                    <div class="col-md-12" style="display: none;">
                        <table class="table table-bordered table-striped dataTable" id="table-questions" role="grid">
                            <thead>
                                <tr role="row">
                                    <th width="10%" tabindex="0" rowspan="1" colspan="1">ID</th>
                                    <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('title');?></th>
                                    <th width="20%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('level');?></th>
                                    <th width="20%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('true_answer');?></th>
                                    <th width="10%" tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                                </tr>
                            </thead>
                            <tbody>                        
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Exam Modal -->
<div id="contentFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="content-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=content_file" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<!-- Answer Modal -->
<div id="answerUpFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="answer-up-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=answer_file" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<!-- Add questions Modal -->
<div id="addQuestionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm câu hỏi</h4>
            </div>
            <div class="modal-body">
                <input id="match-question" type="text" name="match" class="form-control" value="" placeholder="Tìm câu hỏi...">
                <div class="result-questions-box" style="display:none">
                    <ul></ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .questionConfig .input-group{
        padding: 0px!important;
    }
    .questionConfig .input-group-addon{
        min-width: 50px;
    }
    .row.questionConfig {
        border: 1px solid green;
        padding: 15px;
        border-radius: 5px;
    }    
</style>

<script type="text/javascript">
$(document).ready(function() {
  $(".select2").select2();
});
</script>