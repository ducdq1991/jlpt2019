<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Practicle extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('exam/exam_model');
        $this->lang->load('exam', $this->session->userdata('lang'));
        $this->load->model('course/course_model');
	}
	/**
	* index
	* Default page for praticle package, list all record of praticle table
	*/
	public function index( $page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];
        $where['attribute'] = 1; //1: thi trắc nghiệm, 2: thi tự luận

		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		if ($page < 1) {
            $page = 1;
        }
		$offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

		
		$status = $this->input->get('status');
		if($status != ''){
		  $where['exam.status'] = ($status ==='active') ? 1 : 0;
		}		
        $data['status'] = $status;

        $keyword = $this->input->get('keyword'); 
		if($keyword !=''){
			$likes['field'] = 'title';
			$likes['value'] = (string) trim($keyword);
		}
        $data['keyword'] = $keyword;

        $where['exam.type'] = 'exercise';

		$data['type'] = 'exercise';

        $courseId = $this->input->get('course_id', 0);
        $courseOption = $this->course_model->getCourseOption(0, $courseId);
        $data['courseOption'] = $courseOption;

        if ($courseId > 0) {
            $where['exam.course_id'] = $courseId;
        }

		$total = $this->exam_model->totalExam($where);
		$exams = $this->exam_model->getExams( $where, $limit, $offset, $orderCondition, $likes );
		$data['exams'] = $exams;
        $showFrom = $offset + count($exams);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        
        $queryString = $_SERVER['QUERY_STRING'];        
		$data['paging'] = paging(base_url() .'practicle', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');



        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Đề luyện tập', base_url().'practicle');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
        $this->load->view('practicle/index', $data);
        $this->load->view('layouts/footer');
	}
	/**
	* Add
	* Insert a new praticle into practicle table
	*/
	public function add()
	{
		$data = array();        
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array( 'status' => 1, 'type' => 1);
        $questions = $this->exam_model->getListQuestion($where);
        $data['questions'] = $questions;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exam.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exam.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'exam_code',
                'label' => lang('exam_code'),
                'rules' => 'trim|required|is_unique[exam.exam_code]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'time',
                'label' => lang('time'),
                'rules' => 'trim|is_natural',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Đề luyện tập', base_url().'practicle');
            $this->breadcrumbs->add('Tạo đề luyện tập', base_url().'practicle/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('practicle/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $exam = array();
            $exam = $this->input->post();
            $exam = $this->nonXssData($exam);
            foreach ($exam as $key => $value) {
            	if( $key == 'publish_up' ) {
            		if( empty($value) ) {
                        $exam['publish_up'] = date('Y-m-d H:i:s');
                    } else {
                        $date=date_create($value);
                        $exam['publish_up'] = date_format($date,"Y-m-d H:i:s");
                    }
            	} elseif ($key == 'publish_down') {
                    if (!empty($value)) {
                        $date=date_create($value);
                        $exam['publish_down'] = date_format($date,"Y-m-d H:i:s");
                    }
                } elseif (empty($value))
            		unset($exam[$key]);
            }
            $exam['attribute'] = 1;
            $exam['type'] = 'exercise';
            $examId = $this->exam_model->insertExam( $exam );
            if ($examId) {
                $message = sprintf(lang('message_create_success'), lang('new_exam'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'practicle/edit/' . $examId);
            } else {
                $message = sprintf(lang('message_create_error'), lang('new_exam'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'practicle');
        }
	}

	public function getQuestion()
	{
		$id = $this->input->post('id');
		$mcqQuestion = $this->exam_model->getOneMcq( $id );
		if( !empty($mcqQuestion) ) {
			$html = '';
			$html .= '<tr class="gradeA odd" role="row" id="row-'.$mcqQuestion->id.'">'; 
		        $html .= '<td class="sorting_1">' . $mcqQuestion->id . '</td>';
		        $html .= '<td>' . $mcqQuestion->title . '</td>';
		        $html .= '<td>' . $mcqQuestion->level . '</td>';
		        $html .= '<td>' . $mcqQuestion->true_answer . '</td>';
		        $html .= '<td class="center text-center">';
		            $html .= '<a title="">';
		                $html .= '<button class="btn btn-xs btn-danger" data-id="'.$mcqQuestion->id.'" id="remove-question" type="button">';
		                    $html .= '<i class="fa fa-trash"></i>';
		                $html .= '</button>';
		            $html .= '</a>';
		        $html .= '</td>';
		    $html .= '</tr>';
	    	echo $html;
	    	die;
	    } else {
	    	echo 0;
	    	die;
	    }
	}
	/**
	* Edit
	* update a exam info to praticle table
	*/
	public function edit( $id )
	{
		$exam = $this->exam_model->getExamById( (int) $id );
		if( !$exam ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'exam');
            return false;
        }
        $date=date_create($exam->publish_up);
        $exam->publish_up = date_format($date,"d-m-Y H:i");
        if($exam->publish_down != 0) {
            $date=date_create($exam->publish_down);
            $exam->publish_down = date_format($date,"d-m-Y H:i");
        }
		$data = array();
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $where = array( 'status' => 1, 'type' => 1);
        $questions = $this->exam_model->getListQuestion($where);
        $data['questions'] = $questions;
        $data['exam'] = $exam;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exam.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exam.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'exam_code',
                'label' => lang('exam_code'),
                'rules' => 'trim|required|is_unique[exam.exam_code]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'time',
                'label' => lang('time'),
                'rules' => 'trim|is_natural',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_natural' => lang('mes_is_unique'),
                ),
            ),
        );
        $info = array();
        $info = $this->input->post(array('title', 'slug', 'exam_code'));
        if( $info['title'] == $exam->title ) {
        	unset($config[0]);
        }
        if( $info['slug'] == $exam->slug ) {
        	unset($config[1]);
        }
        if( $info['exam_code'] == $exam->exam_code ) {
        	unset($config[2]);
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Đề Luyện tập', base_url().'practicle');
            $this->breadcrumbs->add('Sửa đề luyện tập', base_url().'practicle/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('practicle/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $info['status'] = isset($info['status']) ? (int) $info['status'] : 0;

            if( empty($info['publish_up']) ) {
                $info['publish_up'] = date('Y-m-d H:i:s');
            } else {
                $date=date_create($info['publish_up']);
                $info['publish_up'] = date_format($date,"Y-m-d H:i:s");
            }
            if( !empty($info['publish_down']) ) {
                $date=date_create($info['publish_down']);
                $info['publish_down'] = date_format($date,"Y-m-d H:i:s");
            }
            $examId = $this->exam_model->updateExam( $id, $info );
            if( $examId ) {
                $message = sprintf(lang('message_update_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'practicle/edit/' . $id);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'practicle');
        }
	}

    /**
    * Change Ordering
    **/
    public function ordering()
    {
        $data = [];
        $ordering = $this->input->post('ordering');
        $id = $this->input->post('id');
        if(isset($ordering) && !empty($ordering) && intval($ordering) && isset($id)){
            // Try catch Error
            try{
                $data['ordering'] = $ordering;
                if($this->exam_model->updateExam( $id, $data ))
                {
                    echo 1; exit;
                } 
            } catch(Exception $ex){
                echo $ex->getMessage(); exit;
            }   
        }       
    }
        
	/**
     * @name delete
     * Delete a practicle or any practicle table by id
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_exam'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'practicle');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->exam_model->deleteExam( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('exams'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('exams'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->exam_model->deleteExam( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 2;
            die;
        }
    }
    /**
     * [changeStatus of Course]
     * @return [int]
     */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->exam_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('exams'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('exams'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->exam_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('the_exam'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('the_exam'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }
}