<?php defined('BASEPATH') or exit('No direct script access allowed');

class Books_model extends Bigshare_Model
{
    public function __construct()
    {        
        parent::__construct();
        $this->_table = 'books';
    }
    
    public function addBook($args = []) 
    {
        $this->_data  =$args;
        return $this->addRecord();
    }
    
    
    public function updateBook($where = [], $args = [])
    {
        $this->_table = 'books';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getBooks($where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [])
    {
        $this->_selectItem = '*';
        $this->_from = 'books';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;       
        $this->_wheres = $where;
        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $items = $this->getListRecords();
        $this->resetQuery();
        return $items;
    }

    public function totalBook($where = [])
    {
        $this->_wheres = $where;
        return $this->totalRecord();
    }


    public function deleteBook($where = [], $multiple = false)
    {
        
        $this->_multiple = $multiple;
        $this->_wheres = $where;
        if($this->deleteRecord()){
            $this->resetQuery();
            return true;
        }
        return false;
    }

    public function changeStatus($ids, $args = [])
    {
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'book_id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('book_id' => (int) $ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getBook($where = [])
    {
        $this->_selectItem = '*';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }
}