<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> Thêm sách mới</h5>
            </div>
            <div class="ibox-content">
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-article-slug form-control" id="title" value="<?php echo set_value('name'); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('name', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('alias'); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="slug" class="slug slug-article form-control" id="slug" value="<?php echo set_value('slug'); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Ảnh</label>
                            <div class="col-sm-10">
                            <div id="load-image">
                                <!-- section display image uploaded -->
                            </div>
                            <input type="hidden" name="image" value="<?php echo set_value('image'); ?>" id="image" />
                            <button type="button" id="load-form-featured-image" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredImageModal">Chọn ảnh</button>
                            <button type="button" id="remove-featured-image" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_button'); ?></button>
                            </div>
                        </div>                         
                        <div class="form-group">
                            <label class="control-label col-sm-2">Giá</label>
                            <div class="col-sm-10">
                                <input type="text" name="price" class="form-control" value="<?php echo set_value('price'); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Giá gốc</label>
                            <div class="col-sm-10">
                                <input type="text" name="origin_price" class="form-control" value="<?php echo set_value('origin_price'); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Khổ giấy</label>
                            <div class="col-sm-10">
                                <input type="text" name="paper_size" class="form-control" value="<?php echo set_value('paper_size'); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Số Trang</label>
                            <div class="col-sm-10">
                                <input type="text" name="num_page" class="form-control" value="<?php echo set_value('num_page'); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Trọng lượng</label>
                            <div class="col-sm-10">
                                <input type="text" name="weight" class="form-control" value="<?php echo set_value('weight'); ?>">                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Link xem</label>
                            <div class="col-sm-10">
                                <input type="text" name="extra_link" class="form-control" value="<?php echo set_value('extra_link'); ?>">                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Số lượng sách</label>
                            <div class="col-sm-10">
                                <input type="text" name="qty" class="form-control" value="<?php echo set_value('qty'); ?>">                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Mô tả</label>
                            <div class="col-sm-10">                                                                     
                                <textarea id="description" name="description" rows="15" cols="80" class="editor form-control"><?php echo set_value('description'); ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Nội dung</label>
                            <div class="col-sm-10">                                                                     
                                <textarea id="content" name="content" rows="15" cols="80" class="editor form-control"><?php echo set_value('content'); ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2"></label>
                            <div class="col-sm-10">
                            <button type="button" id="article-submit" class="btn btn-sm btn-info">Lưu</button>
                            </div>
                        </div>
 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Featured Image Modal -->
<div id="featuredImageModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-image-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=image" width="100%" height="450px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>

<?php echo $tinymce; ?>