<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Books
* @author TrungDoan
* @copyright Bigshare
* 
*/

class Books extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('books_model');        
	}
    
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'books.name';
            $likes['value'] = $keyword;
        }
        $orderCondition['book_id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if( $status != '' ){
          $where['books.status'] = $status;
        }
        $total = $this->books_model->totalBook($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'books', $page, $total, $limit, $queryString);
        $books = $this->books_model->getBooks($where, $limit, $offset, $orderCondition, $likes);
        $data['books'] = $books;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($books);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Quản lý sách', base_url().'books');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
    }

    public function add()
    {
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);

        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên sách',
                'rules' => 'trim|required|is_unique[books.name]',
                'errors' => array(
                    'required' => 'Tên sách là bắt buộc.',
                    'is_unique' => 'Sách này đã tồn tại.',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => 'Đường dẫn tĩnh',
                'rules' => 'trim|required|is_unique[books.slug]',
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Sách', base_url().'books');
            $this->breadcrumbs->add('Thêm mới', base_url().'books/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('add', $data);
            $this->load->view('layouts/footer');
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);            
            $bookId = $this->books_model->addBook($postData);
            if($bookId) {
                $message = 'Thêm sách thành công!';
                $messageType = 'success';                
            } else {  
                $message = 'Lỗi! Không thêm được sách';                
            }
            $this->session->set_flashdata($messageType, $message);

            redirect(base_url() . 'books');
        }
    }


    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if(empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'books');
            return false;
        }
        $where = array(
            'field' => 'book_id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->books_model->deleteBook($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa sách thành công.');
            } else {
                $this->session->set_flashdata('error', 'Không xóa được sách');
            }
            echo 1;
            die;
        } else {
            $query = $this->books_model->deleteBook( array( 'book_id' => (int) $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa sách thành công.');
            } else {                
                $this->session->set_flashdata('error', 'Không xóa được sách');
            }
            if( $id ) {
                redirect(base_url() . 'books');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'books.book_id' => (int) $id,
            );
        $book = $this->books_model->getBook($where);        
        if (empty($book)) {            
            $this->session->set_flashdata('error', 'Sách không tồn tại');
            redirect( base_url() . 'books');
        }
        $postData = $this->input->post();

        $data = array();    

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);
            
        $data['_baseUrl'] = $this->getBaseUrl();
        $data['book'] = $book;
        $this->load->library('form_validation');
        $validateName = $validateSlug = '';
        if (isset($postData['name'])) {
            if ($book->name != $postData['name']) {
                $validateName = '|is_unique[books.name]';
            }
        }

        if (isset($postData['slug'])) {
            if ($book->slug != $postData['slug']) {
                $validateSlug = '|is_unique[books.slug]';
            }
        }

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên sách',
                'rules' => 'trim|required' . $validateName,
                'errors' => array(
                    'required' => 'Tên sách là bắt buộc.',
                    'is_unique' => 'Sách này đã tồn tại',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required' . $validateSlug,
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Sách', base_url().'books');
            $this->breadcrumbs->add($book->name, base_url().'books/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('edit', $data);
            $this->load->view('layouts/footer');
        } else {                        
            if( $this->books_model->updateBook($where, $postData ) ) {
                $this->session->set_flashdata('success', 'Đã cập nhật sách.');
            } else {                    
                $this->session->set_flashdata('error', 'Không cập nhật được sách.');
            }

            redirect(base_url() . 'books/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->books_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
}