<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Coursecategory
* @author TrungDoan
* @copyright Bigshare
* 
*/

class Coursecategory extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('coursecategory_model');        
	}
    
    public function index($page = 0)
    {
        $data = $where = $likes = $orderCondition = [];
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $keyword = $this->input->get('keyword');
        if (!empty($keyword)) {
            $likes['field'] = 'course_categories.name';
            $likes['value'] = $keyword;
        }
        $orderCondition['id'] = 'desc';

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

        $status = $this->input->get('status');
        $data['status'] = $status;
        if( $status != '' ){
          $where['course_categories.status'] = $status;
        }
        $total = $this->coursecategory_model->totalCategory($where);
        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'coursecategory', $page, $total, $limit, $queryString);
        $categories = $this->coursecategory_model->getCategories($where, $limit, $offset, $orderCondition, $likes);
        $data['categories'] = $categories;
        // Set actived Fillter
        $data['keyword'] = $keyword;
        $data['numRecord'] = $limit;
        $data['status'] = $status;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $showFrom = $offset + count($categories);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Danh mục khóa học', base_url().'coursecategory');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('index', $data);
        $this->load->view('layouts/footer');
    }

    public function add()
    {
        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();  
        $messageType = 'error';      
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => 'Danh mục khóa học',
                'rules' => 'trim|required|is_unique[course_categories.name]',
                'errors' => array(
                    'required' => 'Danh mục khóa học là bắt buộc.',
                    'is_unique' => 'Danh mục khóa học này đã tồn tại.',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => 'Đường dẫn tĩnh',
                'rules' => 'trim|required|is_unique[course_categories.slug]',
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Danh mục khóa học', base_url().'coursecategory');
            $this->breadcrumbs->add('Thêm mới', base_url().'coursecategory/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('add', $data);
            $this->load->view('layouts/footer');
        } else {
            $postData = $this->input->post();
            $postData = $this->nonXssData($postData);            
            $subjectId = $this->coursecategory_model->addCategory($postData);
            if($subjectId) {
                $message = 'Thêm danh mục khóa học thành công!';
                $messageType = 'success';                
            } else {  
                $message = 'Lỗi! Không thêm được danh mục';                
            }
            $this->session->set_flashdata($messageType, $message);

            redirect(base_url() . 'coursecategory');
        }
    }


    public function del($id = 0)
    {
        if ($id) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }

        if(empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('page'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'coursecategory');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->coursecategory_model->deleteCategory($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa danh mục thành công.');
            } else {
                $this->session->set_flashdata('error', 'Không xóa được danh mục');
            }
            echo 1;
            die;
        } else {
            $query = $this->coursecategory_model->deleteCategory( array( 'id' => (int) $ids ) );
            if( $query ) {                
                $this->session->set_flashdata('success', 'Xóa danh mục thành công.');
            } else {                
                $this->session->set_flashdata('error', 'Không xóa được danh mục');
            }
            if( $id ) {
                redirect(base_url() . 'coursecategory');
            }
            echo 2;
            die;
        }
    }
    /**
    * Edit
    * Update info of page into page table by id
    **/
    public function edit( $id )
    {
        $where = array(
            'course_categories.id' => (int) $id,
            );
        $subject = $this->coursecategory_model->getCategory($where);        
        if (empty($subject)) {            
            $this->session->set_flashdata('error', 'Danh mục khóa học không tồn tại');
            redirect( base_url() . 'coursecategory');
        }
        $postData = $this->input->post();

        $data = array();
        $data['_baseUrl'] = $this->getBaseUrl();
        $data['subject'] = $subject;
        $this->load->library('form_validation');
        $validateName = $validateSlug = '';
        if (isset($postData['name'])) {
            if ($subject->name != $postData['name']) {
                $validateName = '|is_unique[course_categories.name]';
            }
        }

        if (isset($postData['slug'])) {
            if ($subject->slug != $postData['slug']) {
                $validateSlug = '|is_unique[course_categories.slug]';
            }
        }

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Tên danh mục',
                'rules' => 'trim|required' . $validateName,
                'errors' => array(
                    'required' => 'Tên danh mục là bắt buộc.',
                    'is_unique' => 'Danh mục khóa học này đã tồn tại',
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required' . $validateSlug,
                'errors' => array(
                    'required' => 'Bạn cần nhập đường dẫn tĩnh.',
                    'is_unique' => 'Đường dẫn tĩnh đã tồn tại.',
                ),
            ),
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Danh mục khóa học', base_url().'coursecategory');
            $this->breadcrumbs->add($subject->name, base_url().'coursecategory/edit/' . $id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('edit', $data);
            $this->load->view('layouts/footer');
        } else {                        
            if( $this->coursecategory_model->updateCategory($where, $postData ) ) {
                $this->session->set_flashdata('success', 'Đã cập nhật danh mục.');
            } else {                    
                $this->session->set_flashdata('error', 'Không cập nhật được danh mục.');
            }

            redirect(base_url() . 'coursecategory/edit/' . $id);
        }
    }
    
    public function changeStatus()
    {
       $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = ($status == 1) ? 'publish' : 'draft';  
                if($this->coursecategory_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
}