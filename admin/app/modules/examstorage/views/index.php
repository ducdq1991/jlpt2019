<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-database"></i> Kho đề</h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'examstorage/del'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'examstorage/changestatus'; ?>">
                                <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'examstorage/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                              
                            </div>

                            <a href="<?php echo base_url();?>examstorage/categories" class="btn btn-success btn-sm">
                            <i class="fa fa-tags"></i> <?php echo lang('category');?></a>                           
                            <a href="<?php echo base_url();?>examstorage/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Thêm đề thi mới</a> 
                        </div>                    
                    </div>
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                        <div class="col-md-3 text-right">
                            <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                        </div>
                        <div class="col-md-2">
                            <select name="status" class="form-control input-sm sys-filler" onchange="this.form.submit();">
                                <?php foreach($this->config->item('statusConfigs') as $key=>$value):?>
                                <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>                                
                                <?php endforeach;?>
                            </select>                            
                        </div>                        
                        <div class="col-md-5 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div>                        
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="text-center">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th class="text-center">
                                    ID
                                    <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">
                                    <?php echo lang('title');?>
                                    <a href="javascript: doSort('title', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">
                                    <?php echo lang('category');?>
                                    <a href="javascript: doSort('category_id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">
                                    <?php echo lang('exam');?>
                                    <a href="javascript: doSort('question', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">
                                <?php echo lang('answer');?>
                                    <a href="javascript: doSort('answer', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center">
                                    <?php echo lang('status');?>
                                    <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($documents as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="text-center"><?php echo $row->id;?></td>
                                    <td><strong><?php echo $row->title;?></strong></td>
                                    <td>
                                        <?php
                                            $category = isset($row->category) ? $row->category : 'N/A';
                                            echo $category;
                                        ?>
                                    </td>
                                    <td class="text-center">
                                    <?php $exam = !empty($row->question) ? $baseUrl.'/'.$row->question : null;?>
                                    <?php if(is_null($exam)) :
                                        echo lang('not_exist');
                                    else : ?>
                                        <button type="button" class="btn btn-xs btn-white previewPdf" data-url="<?php echo $exam; ?>" data-toggle="modal" data-target="#previewPdfModal"><?php echo lang('preview_exam');?></button>
                                    <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                    <?php $answer = !empty($row->answer) ? $baseUrl.'/'.$row->answer : null;?>
                                    <?php if(is_null($answer)) :
                                        echo lang('not_exist');
                                    else : ?>
                                        <button type="button" class="btn btn-xs btn-white previewPdf" data-url="<?php echo $answer; ?>" data-toggle="modal" data-target="#previewPdfModal"><?php echo lang('preview_answer');?></button>
                                    <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if($row->status == 1):?>
                                            <i onclick="changeState('<?php echo base_url().'examstorage/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="Kho đề đã kích hoạt" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeState('<?php echo base_url().'examstorage/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="Kho đề đã bị vô hiệu" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="center text-center">
                                        <a href="<?php echo base_url();?>examstorage/edit/<?php echo $row->hash_id;?>" title="">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('examstorage/del'); ?>', <?php echo $row->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>                            
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                        <div class="paging col-md-8 text-center">
                            <?php
                                if (count($totalCount) > 0) {
                                    echo $paging;    
                                }                                 
                            ?>                                
                        </div>                                                
                        <div class="col-md-3 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div>                        
                    </div>                    
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'examstorage/del'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'examstorage/changestatus'; ?>">
                                <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'examstorage/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                              
                            </div>

                            <a href="<?php echo base_url();?>examstorage/categories" class="btn btn-success btn-sm">
                            <i class="fa fa-tags"></i> <?php echo lang('category');?></a>                           
                            <a href="<?php echo base_url();?>examstorage/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Thêm đề thi mới</a> 
                        </div>                    
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="previewPdfModal" class="modal fade" role="dialog">
    <div></div>
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content fileFramePreview">
            <div class="modal-content fileFramePreview">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title fileHeader"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
        </div>
    </div>
</div>