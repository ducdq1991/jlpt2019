<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('edit_document');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="pull-right form-module-action">
                            <a href="<?php echo base_url();?>examstorage" class="btn btn-sm btn-white"><i class="fa fa-reply"></i> <?php echo lang('exit');?></a>
                            <a href="<?php echo base_url();?>examstorage/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="title" class="autoSlug auto-document-slug form-control" id="title" value="<?php echo set_value('title', $document->title); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="slug"><?php echo lang('alias'); ?><span> *</label>
                            <div class="col-sm-9">
                                <input type="text" name="slug" class="slug slug-document form-control" id="slug" value="<?php echo set_value('slug', $document->slug); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('category'); ?></label>
                            <div class="col-sm-9">
                                <select class="form-control m-b" name="category_id" id="category_id">
                                    <option value="">--<?php echo lang('choose_category'); ?>--</option>
                                    <?php echo $categorydocOption;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tỉnh</label>
                            <div class="col-sm-9">
                                <select class="form-control m-b" name="prov_id" id="prov_id">
                                    <option value="">--Chọn--</option>
                                    <?php foreach($provinces as $item):?>
                                    <option <?php if($document->prov_id == $item->prov_id):?> selected="selected" <?php endif;?> value="<?php echo $item->prov_id;?>">--<?php echo $item->prov_name;?>--</option>   
                                    <?php endforeach;?>                                 
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Năm</label>
                            <div class="col-sm-9">
                                <select class="form-control m-b" name="year_publish" id="year_publish">
                                    <option value="">--Năm phát hành--</option>
                                    <?php for($i = 1990; $i <= date('Y'); $i++):?>
                                        <option <?php if($document->year_publish == $i):?> selected="selected" <?php endif;?> value="<?php echo $i;?>">Năm <?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                        </div>                          
                       
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('exam_upload'); ?></label>
                            <div class="col-sm-9">
                                <div id="load-file-exam">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="question" value="<?php echo set_value('question', $document->question); ?>" id="question" />
                                <button type="button" id="load-form-exam-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#examFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-exam-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('answer_upload'); ?></label>
                            <div class="col-sm-9">
                                <div id="load-file-answer">
                                    
                                </div>
                                <input type="hidden" name="answer" value="<?php echo set_value('answer', $document->answer); ?>" id="answer" />
                                <button type="button" id="load-form-answer-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#answerFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-answer-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group">Meta keywords[SEO]</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="meta_keywords" value="<?php echo set_value('meta_keywords', $document->meta_keywords); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group">Meta Description[SEO]</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="meta_description"><?php echo set_value('meta_description', $document->meta_description); ?></textarea>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('status'); ?></label>
                            <div class="col-sm-9">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php echo set_checkbox('status', '1', $document->status == 1); ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                        <button type="reset" class="btn btn-sm btn-white"><?php echo lang('reset'); ?></button>
                        <button type="submit" class="btn btn-sm btn-info"><?php echo lang('submit'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Exam Modal -->
<div id="examFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="exam-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=question" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<!-- Answer Modal -->
<div id="answerFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="answer-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=answer" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<script type="text/javascript">
    var _document_category_id = "<?php echo $document->category_id; ?>";
</script>