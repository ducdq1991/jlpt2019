<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-edit"></i> Sửa loại đề | <?php echo $category->name;?></h5>
                <div class="ibox-tools">
                    <a href="<?php base_url('categories/add');?>" class="collapse-link">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>              
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tên danh mục</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-category-slug form-control" value="<?php echo $category->name;?>">
                                <?php echo form_error('name', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Danh mục cha</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="parent">
                                    <option value="0">Danh mục cha</option>
                                    <?php echo $categoryOption;?>
                                </select>
                            </div>
                        </div>                          
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Hình ảnh đại diện</label>
                            <div class="col-sm-5">
                                <input type="file" name="image" value="<?php echo $category->image;?>" <?php if($category->image) echo $category->image;?>/>
                            </div>
                            <div class="col-sm-5">
                                <?php if (!empty($category->image)):?>
                                   <img src="../../../<?php echo $category->image;?>" height="35">
                                <?php endif;?>                                
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Mô tả</label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control note-codable"><?php echo $category->description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slug</label>
                            <div class="col-sm-10"><input name="slug" type="text" class="slug slug-category form-control" value="<?php echo $category->slug;?>"></div>
                        </div>                                              
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Từ khóa (SEO)</label>
                            <div class="col-sm-10"><input name="meta_keywords" type="text" class="form-control" value="<?php echo $category->meta_keywords;?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mô tả (SEO)</label>
                            <div class="col-sm-10"><textarea name="meta_description" type="text" class="form-control"><?php echo $category->meta_description;?></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thứ tự</label>
                            <div class="col-sm-2"><input type="number" name="ordering" class="form-control" value="<?php echo $category->ordering;?>"></div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php if($category->status == 1) echo 'checked="checked"'; ?> id="status_category">Kích hoạt</label> 
                            </div>
                        </div>                                              
                    </div>  
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Xóa</button>
                            <input type="hidden" name="category_id" value="<?php echo $category->id;?>">                            
                            <input type="submit" value="Cập nhật" class="btn btn-primary">
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>          