<?php defined('BASEPATH') or exit('No direct script access allowed');

class Examstorage_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'exams_storage';
    }
    public function insertDocument( $args = array() )
    {
    	$this->_data  = $args;
        return $this->addRecord();
    }

    public function updateHashId( $id )
	{
		$hash_id = md5( $id );
		$this->_table = 'exams_storage';
		$this->_data = array( 'hash_id' => $hash_id );
		$this->_wheres = array( 'id' => $id );
		return $this->updateRecord();
	}

    public function getExams( $where = array(), $limit = 10, $offset = 0, $orderBy = array(), $likes = array() )
    {
    	$this->_selectItem = "exams_storage.id, exams_storage.hash_id, exams_storage.title, exams_storage.question, exams_storage.answer, exams_storage.status, exam_categories.name AS category";
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_from = 'exams_storage';
		$joins = array();
		$joinCourseDetail['table'] = 'exam_categories';
		$joinCourseDetail['condition'] = 'exams_storage.category_id = exam_categories.id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;
		$this->_joins = $joins;		
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		return $this->getListRecords();
    }

	public function numRows($where = array())
	{
		$this->_wheres = $where;
		return $this->totalRecord();
	}


    public function deleteExam( $where = array(), $multiple = false )
    {
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }

    public function changeStatus( $ids, $args = array() )
    {
    	$this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function changeStatusCategory( $ids, $args = array() )
    {
    	$this->_table = 'exam_categories';
    	$this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

    public function getExamById( $hash_id )
    {
    	if(!$hash_id){
			return false;
		}
		$where = array();
		$where['hash_id'] = $hash_id;
		$this->_wheres = $where;
		return $this->getOneRecord();
    }
    public function updateExam( $hash_id, $args )
    {
    	$this->_table = 'exams_storage';
		$where = array();
		$where['hash_id'] = $hash_id;
		$this->_wheres = $where;
		$this->_data = $args;
		return $this->updateRecord();
    }


	public function addCategory( $args )
	{	
		$this->_table = 'exam_categories';
		$this->_data = $args;
		if($id = $this->addRecord()){
			return $id;
		}

		return false;
	}

	public function updateCategory($categoryId, $args)
	{
		$this->_table = 'exam_categories';
		$where = array();
		$where['id'] = (int) $categoryId;
		$this->_wheres = $where;
		$this->_data = $args;
		if( $this->updateRecord()){
			return true;
		}
		return false;
	}

	public function numCategories($where = array())
	{
		$this->_table = 'exam_categories';
		$this->_wheres = $where;
		return $this->totalRecord();
	}	

    public function deleteCategory($where = array(), $multiple = false)
    {
    	$this->_table = 'exam_categories';
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }

	public function getCategoryById($categoryId)
	{
		if(!$categoryId){
			return null;
		}
		$where = $whereDetail = array();
		$where['id'] = $categoryId;
		$whereDetail['exam_categories.id'] = $categoryId;
		$this->_table = 'exam_categories';
		$this->_wheres = $where;
		$category = $this->getOneRecord();
        return $category;
	}   

	public function getParentCategory( $id )
	{
		$this->resetQuery();
		$this->_table = 'exam_categories';
		$this->_selectItem = 'exam_categories.name as parent_name';
		$where = array('exam_categories.id' => $id);
		$this->_wheres = $where;
		$result = (array) $this->getOneRecord();		
		return $result;
	}

	public function getCategoryOption($parentId = 0, $currentId = 0, $str = '') {	  
	  $html = '';
	  $this->db->select('exam_categories.id, exam_categories.parent, exam_categories.name');
	  $this->db->from('exam_categories');	  
	  $this->db->where('exam_categories.parent', $parentId);
	  $result = $this->db->get()->result();	  
	  $selected = '';
	  foreach ($result as $item) {	   		
	  		$name = $str.$item->name;

	  		if($currentId > 0) {
	  			$selected = 'selected = "selected"';
	  		}
	  		$html .= '<option ' .$selected. ' data-parent="'.$item->parent.'" value="' . $item->id . '">' . $name . '</option>';
	  		$html .= $this->getCategoryOption($item->id, $currentId, $str.'|---');
	  }	 
	  return $html;
	}


	public function getCategories($where = array(), $limit = 10, $offset = 0, $orderBy = array(), $likes = array())
	{		
		$this->resetQuery();
		$this->_selectItem = "exam_categories.id, exam_categories.image, exam_categories.parent, exam_categories.created, exam_categories.ordering, exam_categories.status, exam_categories.name, exam_categories.slug";
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $likes['value'];
		}
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_from = 'exam_categories';	
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		$category = $this->getListRecords();
		$this->resetQuery();	
		$data = array();
		foreach($category as $item){
			$items = (array) $item;	
			if($item->parent > 0){
				$parent = $this->getParentCategory($item->parent);	
				if($parent){
					$items = array_merge((array) $items, $parent);
				}							
			}	
			$data[] = (object) $items;					
		}
		$this->resetQuery();
		return $data;
	}

	public function getProvinces()
	{	$result = array();	
		$query = $this->db->get('provinces');
		if($query)
		{
			$result = $query->result();
			return $result;
		}
		return $result;
	}

}