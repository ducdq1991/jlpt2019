<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Document
* @author ChienDau
* @copyright Bigshare
* 
*/

class Examstorage extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('examstorage_model');
        $this->lang->load('document', $this->session->userdata('lang'));
	}
	/*
    * [index]
    * default page for document package, list all document in table
    */
	public function index( $page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];	
		$data['baseUrl'] = $this->getBaseUrl();

		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		if ($page < 1) {
            $page = 1;
        }
		$offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
		
		$status = $this->input->get('status');		
		if($status != ''){
		  $where['exams_storage.status'] = ($status ==='active') ? 1 : 0;
		}
        $data['status'] = $status;		
		
        $keyword = $this->input->get('keyword');
		if($keyword !=''){
			$likes['field'] = 'title';
			$likes['value'] = (string) trim($keyword);
		}
		$data['keyword'] = $keyword;

		$total = $this->examstorage_model->numRows($where);
		$items = $this->examstorage_model->getExams( $where, $limit, $offset, $orderCondition, $likes );
		$data['documents'] = $items;
        $showFrom = $offset + count($items);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        
        $queryString = $_SERVER['QUERY_STRING'];        
		$data['paging'] = paging(base_url() .'examstorage', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Quản lý kho đề', base_url().'examstorage');
        $head['breadcrumb'] = $this->breadcrumbs->display();        

		$this->load->view('layouts/header', $head);
        $this->load->view('examstorage/index', $data);
        $this->load->view('layouts/footer');
	}
    /**
    * [Add]
    * insert a new document into documents table 
    */
	public function add()
	{
		$data = array();
        error_reporting(-1);
        ini_set('display_errors', 1);
        $categorydocOption = $this->examstorage_model->getCategoryOption();
        $data['categorydocOption'] = $categorydocOption;
        $provinces = $this->examstorage_model->getProvinces();
        $data['provinces'] = $provinces;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exams_storage.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exams_storage.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Quản lý kho đề', base_url().'examstorage');
            $this->breadcrumbs->add('Thêm đề thi', base_url().'examstorage/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('examstorage/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $document = [];
            $document = $this->input->post();
            $document = $this->nonXssData($document);
            $documentId = $this->examstorage_model->insertDocument( $document );
            if( $documentId ) {
                $hash_id = md5($documentId);
            	$this->examstorage_model->updateHashId( $documentId );
                $message = sprintf(lang('message_create_success'), lang('new_document'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'examstorage/edit/' . $hash_id);
            } else {
                $message = sprintf(lang('message_create_error'), lang('new_document'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'examstorage');
        }
	}
    /**
    * [Edit]
    * update a document in documents table 
    */
    public function edit( $hash_id )
    {		
        $data = array();	
        $document = $this->examstorage_model->getExamById( $hash_id );
        $provinces = $this->examstorage_model->getProvinces();
        $data['provinces'] = $provinces;        
        if( !$document ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_document'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'examstorage');
            return false;
        }
        $categorydocOption = $this->examstorage_model->getCategoryOption();
        $data['categorydocOption'] = $categorydocOption;
        $data['document'] = $document;
        $this->load->library('form_validation');
        $config = array();
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[exams_storage.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[exams_storage.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
        );
        $title = $this->input->post('title');
        $slug = $this->input->post('slug');
        if( $title == $document->title ) {
            $config[0] = array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            );
        }
        if( $slug == $document->slug ) {
            $config[1] = array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            );
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Quản lý kho đề', base_url().'examstorage');
            $this->breadcrumbs->add('Sửa đề thi', base_url().'examstorage/edit/' . $hash_id);
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('examstorage/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $documentInfo = array();
            $documentInfo = $this->input->post();
            $documentInfo = $this->nonXssData($documentInfo);
            if(!isset($documentInfo['status']) )
            {
            	$documentInfo['status'] = (int) 0;
            }
            $documentId = $this->examstorage_model->updateExam( $hash_id, $documentInfo );
            if( $documentId ) {
                $message = sprintf(lang('message_update_success'), lang('the_document'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_document'));
                $this->session->set_flashdata('error', $message);
            }
            redirect( base_url('examstorage/edit'). '/'. $hash_id);
        }
    }
    /**
     * @name delete
     * Delete a article or any article by id
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_document'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'examstorage');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->examstorage_model->deleteExam( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('documents'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('documents'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->examstorage_model->deleteExam( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_document'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_document'));
                $this->session->set_flashdata('error', $message);
            }
            echo 2;
            die;
        }
    }
    /**
     * [changeStatus of Course]
     * @return [int]
     */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->examstorage_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('documents'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('documents'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->examstorage_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('the_document'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('the_document'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }

    public function categories($page = 1)
    {
        $data = $where = $orderCondition = $likes = $sortFields = [];  

        // Paging Configs
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));    
        $page = intval(str_replace('page-', '', $page));        
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
        
        $status = $this->input->get('status');
        if($status != ''){
          $where['exam_categories.status'] = ($status ==='active') ? 1 : 0;
        }
         $data['status'] = $status;

        $keyword = $this->input->get('keyword');
        if($keyword !=''){
            $likes['field'] = 'name';
            $likes['value'] = (string) trim($keyword);
        }
        $data['keyword'] = $keyword;

        //Query
        $total = $this->examstorage_model->numCategories($where);           
        $items = $this->examstorage_model->getCategories($where, $limit, $offset, $orderCondition, $likes);
        $data['categories'] = $items;         

        $showFrom = $offset + count($items);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;
        
        $queryString = $_SERVER['QUERY_STRING'];
        $data['paging'] = paging(base_url() .'examstorage/categories', $page, $total, $limit, $queryString); 
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'examstorage');
        $this->breadcrumbs->add('Loại đề thi', base_url().'examstorage/categories');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/categories', $data);
        $this->load->view('layouts/footer');
    }


    public function addCategory()
    {
        $where = $data = $category = $categoryDetail = array();     
        // Get Parent Category
        $categoryOption = $this->examstorage_model->getCategoryOption();        
        $data['categoryOption'] = $categoryOption;  
        //Check form input
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang('name_category'), 'required');

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            if ($this->form_validation->run() == FALSE) {
                redirect( base_url() . 'examstorage_model/addcategory', 'refresh');                   
            } else {
                $category = array();

                // Config Upload
                $photoPath = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
                $config = array(
                    'upload_path'   => $photoPath.'/upload',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size'      => 3000,
                    'max_width'     => 1000,
                    'max_height'    => 1000,
                    );  
                $photo = null;
                // Check Upload
                if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('image')) {
                        $imgArray = $this->upload->data();
                        $photo = isset($imgArray) ? 'upload/' . $imgArray['file_name'] : '';
                        $category['image'] = $photo;                    
                    } else {                        
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect( base_url() . 'examstorage/addcategory', 'refresh');
                    } 
                }
                // Get Data from Input Form
                $postData = $this->input->post();
                $postData = $this->nonXssData($postData);
                if( $this->input->post('slug') ) {
                    $slug = $this->input->post('slug');
                } else {
                    $this->load->helper('text');
                    $convertTitle = convert_accented_characters($postData['name']);
                    $slug = strtolower(url_title($convertTitle, 'dash', true));
                }
                // Category
                $category['parent'] = $postData['parent'];
                $category['status'] = isset($postData['status']) ? $postData['status'] : 0;
                $category['ordering'] = $postData['ordering'];
                $category['created'] = date('Y-m-d H:i:s');     
                // Category Detail
                $category['name'] = $postData['name'];
                $category['description'] = $postData['description'];
                $category['slug'] = $slug;
                $category['meta_keywords'] = $postData['meta_keywords'];
                $category['meta_description'] = $postData['meta_description'];                
                $categoryId = $this->examstorage_model->addCategory( $category );
                if( $categoryId ) {
                        $this->session->set_flashdata('success','Thêm danh mục thành công!');
                } else {
                        $this->session->set_flashdata('error','Thêm danh mục thất bại!');
                }
                redirect( base_url() . 'examstorage/categories');
            }
        }
        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'examstorage');
        $this->breadcrumbs->add('Tạo loại đề', base_url().'examstorage/addcategory');
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/add_category', $data);
        $this->load->view('layouts/footer');        
    }

    public function editCategory($categoryId = 0 )
    {   
        $data = $where = $categoryUpdate = array();
        if ( !$categoryId ) {
            redirect('examstorage/categories');
        }
        $category = $this->examstorage_model->getCategoryById( (int) $categoryId );
        $data['category'] = $category;      
        // Get Parent Category
        $categoryOption = $this->examstorage_model->getCategoryOption(0, $category->parent);     
        $data['categoryOption'] = $categoryOption;  
        // Load Validate Library
        $this->load->library('form_validation');
        if (!$category) {
            $this->session->set_flashdata('error', 'Danh mục không tồn tại!');
            redirect('examstorage/categories');
        } 
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            //check data input
            $this->form_validation->set_rules('title_category', 'Tên danh mục', 'required');
            if ($this->form_validation->run() == FALSE) {

                $postData = $this->input->post();
                $postData = $this->nonXssData($postData);
                if( $this->input->post('slug') ) {
                    $slug = $this->input->post('slug');
                } else {
                    $this->load->helper('text');
                    $convertTitle = convert_accented_characters($postData['name']);
                    $slug = strtolower(url_title($convertTitle, 'dash', true));
                }               
                // Config Upload
                $photoPath = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
                $config = array(
                    'upload_path'   => $photoPath.'/upload',
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'max_size'      => 3000,
                    'max_width'     => 1000,
                    'max_height'    => 1000,
                    );  

                $photo = null;
                // Check Upload
                if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {

                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('image')) {
                        $imgArray = $this->upload->data();
                        $photo = isset($imgArray) ? 'upload/' . $imgArray['file_name'] : '';
                        $categoryUpdate['image'] = $photo;
                        @unlink($photoPath . '/' .$category->image);
                    } else {
                        
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect( current_url(), 'refresh');
                    }
                }
                /*--------------set Data Updated--------------*/
                $categoryUpdate['parent'] = $postData['parent'];
                $categoryUpdate['status'] = isset($postData['status']) ?  $postData['status'] : $category->status;
                $categoryUpdate['ordering'] = $postData['ordering'];
                $categoryUpdate['created'] = date('Y-m-d H:i:s');               
                // Update Detail
                $categoryUpdate['name'] = $postData['name'];
                $categoryUpdate['description'] = $postData['description'];
                $categoryUpdate['slug'] = $slug;
                $categoryUpdate['meta_keywords'] = $postData['meta_keywords'];
                $categoryUpdate['meta_description'] = $postData['meta_description'];
                // Check Update
                if($this->examstorage_model->updateCategory( $categoryId, $categoryUpdate )){
                    $this->session->set_flashdata('success','Cập nhật danh mục thành công!');
                } else {
                    $this->session->set_flashdata('error','Thay đổi danh mục thất bại!');
                }
                redirect( base_url() . 'examstorage/categories');
            }
        }

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Kho đề', base_url().'examstorage');
        $this->breadcrumbs->add('Sửa loại đề', base_url().'examstorage/editcategory/' . $categoryId);
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('examstorage/edit_category',$data);
        $this->load->view('layouts/footer');        
    }    

    public function delCategory()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $this->session->set_flashdata('warning', 'Không có lựa chọn nào để xóa');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->examstorage_model->deleteCategory( $where, true );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa lựa chọn thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa lựa chọn thất bại!');
            }
            echo 1;
            die;
        } else {
            $query = $this->examstorage_model->deleteCategory( array( 'id' => (int) $ids ) );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại!');
            }
            echo 1;
            die;
        }
    }

    public function changeStatusCategory()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->examstorage_model->changeStatusCategory( $ids, $args))
                {
                    $this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
                } else {
                    $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->examstorage_model->changeStatusCategory( $ids, $args ) )
            {
                $this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
            } else {
                $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
            }
            echo 1;
            die;    
        }           
    }

    public function orderingCategory()
    {
        $data = array();
        $ordering = $this->input->post('ordering');
        $id = $this->input->post('id');
        if(isset($ordering) && !empty($ordering) && intval($ordering) && isset($id)){
            // Try catch Error
            try{
                $data['ordering'] = $ordering;
                if($this->examstorage_model->updateCategory( $id, $data ))
                {
                    echo 1; exit;
                } 
            } catch(Exception $ex){
                echo $ex->getMessage(); exit;
            }   
        }       
    }        

}