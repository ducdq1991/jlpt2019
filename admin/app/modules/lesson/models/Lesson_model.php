<?php defined('BASEPATH') or exit('No direct script access allowed');

class Lesson_model extends Bigshare_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->_table = 'lessons';
    }

    public function insertLesson( $args = array() )
    {
    	$this->_table = 'lessons';
    	$this->_data  = $args;
        return $this->addRecord();
    }

    public function getLessons( $where = [], $limit = 10, $offset = 0, $orderBy = [], $likes = [], $arrayCourseId = [])
    {
    	$this->_selectItem = "lessons.id, lessons.hash_id, lessons.title, lessons.file, lessons.video, lessons.ordering, lessons.created, lessons.price, lessons.status, lessons.access, lessons.video_relates, course_details.name AS course, lessons.slug";
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_from = 'lessons';
		$joins = [];
		$joinCourseDetail['table'] = 'course_details';
		$joinCourseDetail['condition'] = 'lessons.course_id = course_details.course_id';
		$joinCourseDetail['type'] = 'left';
		$joins[] = $joinCourseDetail;
		$this->_joins = $joins;		
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		$this->_batchConditionField = 'lessons.course_id';
		$this->_batchConditionValue = $arrayCourseId;		
		return $this->getListRecords();
    }
    /**
	 * [totalLesson]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalLesson($where = [], $likes = [], $arrayCourseId = [])
	{
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_wheres = $where;
		$this->_batchConditionField = 'lessons.course_id';
		$this->_batchConditionValue = $arrayCourseId;		
		return $this->totalRecord();
	}

	public function updateHashId( $id )
	{
		$hash_id = md5( $id );
		$this->_table = 'lessons';
		$this->_data = array( 'hash_id' => $hash_id );
		$this->_wheres = array( 'id' => $id );
		return $this->updateRecord();
	}
	/**
	 * [delCourse delete a/any Lesson]
	 * @param  [int] $courseId
	 * @return [boolean]
	 */
    public function deleteLesson( $where = array(), $multiple = false )
    {
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }

	/**
	 * [changeStatus Of a/any Lesson]
	 * @param  [int or array] $courseIds
	 * @param  [array] $courseUpdate
	 * @return [boolean]
	 */
    public function changeStatus( $ids, $args = array() )
    {
    	$this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }
    public function getLessonById( $hash_id )
    {
    	if(!$hash_id){
			return false;
		}
		$where = array();
		$where['hash_id'] = $hash_id;
		$this->_wheres = $where;
		return $this->getOneRecord();
    }

    public function getLesson($where = [])
    {
    	if (empty($where)) {
			return false;
		}
		$this->_table = 'lessons';
		$this->_wheres = $where;
		return $this->getOneRecord();
    }

    public function updateLesson( $hash_id = '', $args = [] )
    {
    	$this->_table = 'lessons';
		$where = array();
		$where['hash_id'] =  $hash_id;
		$this->_wheres = $where;
		$this->_data = $args;
		return $this->updateRecord();
    }
}