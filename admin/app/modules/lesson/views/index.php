<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-file-video-o"></i> <?php echo lang('lessons');?></h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8" id="formList">
                    <input type="hidden" id="sort" name="order" value="">
                    <input type="hidden" id="sortBy" name="by" value="">
                    <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">
                    <div class="form-group">
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'lesson/del'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'lesson/changestatus'; ?>">
                                <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'lesson/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                              
                            </div>

                            <a href="<?php echo base_url();?>course" class="btn btn-success btn-sm">
                            <i class="fa fa-cubes"></i> Khóa học</a>   
                            <a href="<?php echo base_url();?>lesson/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Thêm mới bài giảng</a>
                        </div>                    
                    </div>
                    <div class="form-group">
                        <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>
                        <div class="col-md-2 text-right">
                            <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                        </div>
                        <div class="col-md-2">
                            <select name="status" class="form-control input-sm sys-filler" onchange="this.form.submit();">
                                <?php foreach($this->config->item('statusConfigs') as $key=>$value):?>
                                <option value="<?php echo $key;?>" <?php if($key == $status):?> selected = "selected" <?php endif;?>><?php echo lang($value);?></option>                                
                                <?php endforeach;?>
                            </select>                            
                        </div>
                        <div class="col-md-6">
                                <select class="select2 form-control input-sm sys-filler" name="course_id" id="course_id" onchange="this.form.submit();">
                                    <option value="">--<?php echo lang('choose_course'); ?>--</option>
                                    <?php echo $courseOption;?>
                                </select>                                                     
                        </div>                                                
                        <!--<div class="col-md-4 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div>-->                        
                    </div>    
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th class="text-center">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th class="text-center">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                <th class="text-center"><?php echo lang('title');?> 
                                <a href="javascript: doSort('title', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                <th class="text-center"><?php echo lang('course_thematic');?>
                                     <a href="javascript: doSort('lessons.course_id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center"><?php echo lang('video');?></th>
                                <th class="text-center"><?php echo lang('access');?>
                                     <a href="javascript: doSort('access', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th style="width:90px;" class="text-center"><?php echo lang('ordering');?>
                                     <a href="javascript: doSort('ordering', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center"><?php echo lang('status');?>
                                     <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>
                                </th>
                                <th class="text-center"><?php echo lang('created');?>
                                    <a href="javascript: doSort('created', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a>                                    
                                </th>
                                <th class="text-center"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php 
                                if (count($lessons) > 0):    
                                    foreach ($lessons as $row):
                            ?>
                                <tr class="gradeA odd" role="row">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="text-center"><?php echo $row->id;?></td>
                                    <td><strong><?php echo $row->title;?></strong></td>
                                    <td>
                                        <?php
                                            $course = isset($row->course) ? $row->course : 'N/A';
                                            echo $course;
                                        ?>
                                    </td>
                                    <td>
                                    <?php
                                        $exp = explode('/', $row->video);
                                        $video = $youtube = '';
                                        if(isset($exp[2]) && $exp[2] == 'www.youtube.com') {
                                            $youtubeExp = explode('v=', $row->video);
                                            $videoId = isset($youtubeExp[1]) ? $youtubeExp[1] : '';
                                            $youtube = 'http://www.youtube.com/embed/'.$videoId.'?vq=hd720&fs=0&rel=0&showinfo=0&disablekb=1&enablejsapi=1';
                                        } else {
                                            $video = $baseUrl.'/'.$row->video;
                                        }
                                    ?>
                                    <?php if( isset($row->video) ) : ?>
                                    <button type="button" class="btn btn-xs btn-white previewVideo" data-video="<?php if(!empty($video)) echo $video;?>" data-youtube="<?php if(!empty($youtube)) echo $youtube;?>" data-toggle="modal" data-target="#previewVideoModal"><?php echo lang('preview_video');?></button>
                                    <?php else : ?>
                                        echo lang('not_file');
                                    <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?php if($row->access == 1):?>
                                            <i title="bải giảng xem miễn phí" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i title="bài giảng thu phí" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="text-center">
                                        <input data-id='<?php echo $row->id;?>' url="<?php echo base_url().'lesson/ordering'; ?>" class="form-control ordering" type="number" value="<?php echo $row->ordering;?>" style="max-width: 70px;">
                                    </td>
                                    <td class="text-center">
                                        <?php if($row->status == 1):?>
                                            <i onclick="changeState('<?php echo base_url().'lesson/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="bài giảng đã kích hoạt" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeState('<?php echo base_url().'lesson/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="bài giảng đã bị vô hiệu" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="text-center"><?php $created_date = new DateTime( $row->created); 
                                        echo $created_date->format('d-m-Y H:i:s'); ?></td>
                                    <td class="center text-center">
                                        <a href="<?php echo base_url();?>lesson/edit/<?php echo $row->hash_id;?>" title="">
                                            <button class="btn btn-xs btn-default" type="button">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        <a onclick="delSingle('<?php echo base_url('lesson/del'); ?>', <?php echo $row->id; ?>)" title="">
                                            <button class="btn btn-xs btn-danger" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php 
                                    endforeach;
                                else:
                            ?>  
                            <tr>
                                <td colspan="10" class="text-center">Không có bản ghi nào!</td>
                            </tr>
                            <?php 
                                endif;
                            ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">                                
                                    <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($totalCount) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'lesson/del'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                    <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'lesson/changestatus'; ?>">
                                    <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                    <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'lesson/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                              
                                </div>

                                <a href="<?php echo base_url();?>course" class="btn btn-success btn-sm">
                                <i class="fa fa-cubes"></i> Khóa học</a>   
                                <a href="<?php echo base_url();?>lesson/add" class="btn btn-primary btn-sm">
                                <i class="fa fa-plus"></i> Thêm mới bài giảng</a>
                            </div>                    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="previewVideoModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content videoFramePreview">
            
        </div>
    </div>
</div>
<script type="text/javascript">
    var _base_url = '<?php echo $baseUrl;?>';
</script>
<script type="text/javascript">
$(document).ready(function() {
  $(".select2").select2();
});
</script>