<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-edit"></i> <?php echo lang('edit_lesson');?> | <?php echo $lesson->title;?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form style="display: table; width: 100%" id="form-add-article" class="form form-horizontal" role="form" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="col-md-12">
                    <div class="pull-right form-module-action">
                        <a href="<?php echo base_url();?>lesson" class="btn btn-sm btn-white"><i class="fa fa-reply"></i> <?php echo lang('exit');?></a>
                        <a href="<?php echo base_url();?>lesson/add" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>

                    </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title"><?php echo lang('title'); ?><span> *</span></label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="autoSlug auto-lesson-slug form-control" id="title" value="<?php echo set_value('title', $lesson->title); ?>" placeholder="<?php echo lang('title_placeholder'); ?>">
                                <?php echo form_error('title', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="slug"><?php echo lang('alias'); ?><span> *</label>
                            <div class="col-sm-10">
                                <input type="text" name="slug" class="slug slug-lesson form-control" id="slug" value="<?php echo set_value('slug', $lesson->slug); ?>" placeholder="">
                                <?php echo form_error('slug', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="video"><?php echo lang('video'); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="video" class="form-control" id="video" value="<?php echo set_value('video', $lesson->video); ?>" placeholder="">
                                <br>
                                <button type="button" id="btn-upload-video" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredVideoModal"><?php echo lang('upload_video'); ?></button>
                                <p class="support-text">chọn một video từ máy tính của bạn, hoặc một video đã được tải lên trước đây!</p>
                                <button type="button" id="btn-remove-video" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_video'); ?></button>
                                <?php echo form_error('video', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="video_title">Tiêu đề video mặc định</label>
                            <div class="col-sm-10">
                                <input type="text" name="video_title" class="form-control" id="video_title" value="<?php echo set_value('video_title', $lesson->video_title); ?>" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="video_relates">Video khác</label>
                            <div class="col-sm-10">
                                
                                <div class="video-groups">
                                    <?php
                                    $videoRelates = unserialize($lesson->video_relates); 
                                    if (!empty($videoRelates)):
                                        $i = 0; 
                                        foreach ($videoRelates as $vr) :
                                    ?>
                                    <div class="video-<?php echo $i;?> relates_video" style="margin-bottom: 10px;display: block;overflow: hidden;">
                                        <input type="text" name="video_relates[<?php echo $i;?>][name]" class="c50 video_relates-name-<?php echo $i;?> form-control" value="<?php echo $vr['name'];?>" placeholder="Tên Video">
                                        <input type="text" name="video_relates[<?php echo $i;?>][link]" class="c50 video_relates-link-<?php echo $i;?> form-control" value="<?php echo $vr['link'];?>" placeholder="Link Video">
                                        <a href="" title="" class="pull-right remove-relate-video" data-id="<?php echo $i;?>"><i class="fa fa-remove"></i></a>
                                    </div>
                                    <?php
                                        $i++;
                                        endforeach;
                                    endif;
                                    ?>
                                </div>
                                <button type="button" class="btn btn-success btn-sm addRelateVideo">Thêm video</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="link"><?php echo lang( 'related_link' ); ?></label>
                            <div class="col-sm-10">
                                <input type="text" name="link" class="form-control" id="link" value="<?php echo set_value('link', $lesson->link ); ?>" placeholder="">
                                <?php echo form_error('link', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('upload_file'); ?></label>
                            <div class="col-sm-9">
                                <div id="load-file">
                                    <!-- section display image uploaded -->
                                </div>
                                <input type="hidden" name="file" value="<?php echo set_value( 'file', $lesson->file ); ?>" id="file" />
                                <button type="button" id="load-form-featured-file" class="btn btn-sm btn-white" data-toggle="modal" data-target="#featuredFileModal"><?php echo lang('choose_file'); ?></button>
                                <button type="button" id="remove-featured-file" class="btn btn-sm btn-link" style="display: none"><?php echo lang('remove_file'); ?></button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('course_thematic'); ?></label>
                            <div class="col-sm-9">
                                <select class="form-control m-b" name="course_id" id="course_id" curent-course-id="<?php echo $lesson->course_id; ?>">
                                    <option value="">--<?php echo lang('choose_course'); ?>--</option>
                                    <?php echo $courseOption;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('ordering'); ?></label>
                            <div class="col-sm-9">
                                <input type="number" name="ordering" value="<?php echo set_value('ordering', $lesson->ordering);?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('access'); ?></label>
                            <div class="col-sm-9">
                                <label class="checkbox-inline"><input type="checkbox" name="access" value="1" <?php echo set_checkbox('access', '1', $lesson->access == 1 ); ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>  
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('price'); ?></label>
                            <div class="col-sm-9">
                                <input type="text" name="price" value="<?php echo set_value('price', $lesson->price);?>" class="form-control">
                                <?php echo form_error('price', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>
                            </div>
                        </div> --> 
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="group"><?php echo lang('status'); ?></label>
                            <div class="col-sm-9">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1" <?php echo set_checkbox('status', '1', $lesson->status == 1 ); ?>><?php echo lang('active');?></label> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                        <button type="reset" class="btn btn-sm btn-white"><?php echo lang('reset'); ?></button>
                        <button type="submit" class="btn btn-sm btn-info"><?php echo lang('submit'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Featured Image Modal -->
<div id="featuredFileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-file-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=file" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<!-- Featured video Modal -->
<div id="featuredVideoModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">           
            <iframe id="featured-video-iframe" src="<?php echo base_url();?>assets/js/filemanager/dialog.php?field_id=video" width="100%" height="500px" tabindex="-1"></iframe>                               
        </div>
    </div>
</div>
<script>
    var __course_id = "<?php echo $lesson->course_id;?>";
</script>
<style type="text/css" media="screen">
    .c50{
        width: 49%;
        float: left;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', 'a.remove-relate-video', function(e) {
            e.preventDefault();
            var videoId = $(this).attr('data-id');
            console.log(videoId);
            $('.video-' + videoId).remove();
        });

        $('.addRelateVideo').click(function() {
        
            videoIndex = parseInt($(".video-groups").children().length);
            console.log(videoIndex);

           var _vHTML = '<div class="video-'+videoIndex+' relates_video" style="margin-bottom: 10px;display: block;overflow: hidden;">';
           _vHTML += '<input type="text" name="video_relates['+videoIndex+'][name]" class="c50 video_relates-name-'+videoIndex+' form-control" value="" placeholder="Tên Video">';
            _vHTML += '<input type="text" name="video_relates['+videoIndex+'][link]" class="c50 video_relates-link-'+videoIndex+' form-control" value="" placeholder="Link Video">';
           
            _vHTML += '<a style="float:right" href="" title="" class="pull-right remove-relate-video" data-id="'+videoIndex+'"><i class="fa fa-remove"></i></a>';
             _vHTML += '</div>';
            console.log(_vHTML);
            $('.video-groups').append(_vHTML);
        });
    });
</script>
