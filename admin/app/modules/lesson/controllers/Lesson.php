<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Lesson
* @author ChienDau
* @copyright Bigshare
* 
*/

class Lesson extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('lesson_model');
        $this->lang->load('lesson', $this->session->userdata('lang'));
        $this->load->model('course/course_model');
	}
    /*
    * [index]
    * default page for lesson package, list all lesson in table
    */
	public function index( $page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = $arrayCourseId = [];	
        $data['baseUrl'] = $this->getBaseUrl();
		// Paging Configs
		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');	
		$numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;
		$page = intval(str_replace('page-', '', $page));	
		if ($page < 1) {
            $page = 1;
        }
		$offset = ($page - 1) * $limit;
		//Get Filters				

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';
        
        $courseId = $this->input->get('course_id', 0);        
        $courseOption = $this->course_model->getCourseOption(0, $courseId);
        $data['courseOption'] = $courseOption;
          
        if ($courseId > 0) {
            //$where['lessons.course_id'] = $courseId;  
            $arrayCourseId = $this->course_model->getChildId($courseId);
            $arrayCourseId[] = (int) $courseId;                      
        }
        $data['course_id'] = $courseId;


		// Set Status Condition
        $status = $this->input->get('status');
		if($status != ''){
		  $where['lessons.status'] = ($status ==='active') ? 1 : 0;
		}		
		// Set Order Condition
		if( isset($orderOption) && ( !empty($orderOption) || $orderOption !='') ) {
			$orderCondition[$orderOption] = $orderBy;
		}
		// Set Likes Condition      
        $keyword = $this->input->get('keyword');
		if($keyword !=''){
			$likes['field'] = 'lessons.title';
			$likes['value'] = (string) trim($keyword);
		}
        $data['keyword'] = $keyword;

		// Set actived Fillter
		
		
		$data['numRecord'] = $limit;
		$data['status'] = $status;		
		$sortFields['lessons.title'] = lang('name_lesson');
		$sortFields['lessons.course_id'] = lang('course_thematic');	
		$data['itemPerPages'] = $this->config->item('rowPerPages');

		$total = $this->lesson_model->totalLesson($where, $likes, $arrayCourseId);
		$lessons = $this->lesson_model->getLessons( $where, $limit, $offset, $orderCondition, $likes, $arrayCourseId);
        $data['lessons'] = $lessons;

        $showFrom = $offset + count($lessons);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

		
        $queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'lesson', $page, $total, $limit, $queryString); 

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Bài giảng', base_url().'lesson');
        $head['breadcrumb'] = $this->breadcrumbs->display();

		$this->load->view('layouts/header', $head);
        $this->load->view('lesson/index', $data);
        $this->load->view('layouts/footer');
	}
    /**
    * [Add]
    * insert a new lesson into lessons table 
    */
	public function add()
	{
		$data = array();
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[lessons.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[post_details.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'price',
                'label' => lang('price'),
                'rules' => 'trim|numeric',
                'errors' => array(
                    'numeric' => '%s phải là số'
                ),
            ),
        );
        
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Bài giảng', base_url().'lesson');
            $this->breadcrumbs->add('Thêm bài giảng', base_url().'lesson/add');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('lesson/add', $data);
            $this->load->view('layouts/footer');
        } else {
            $lesson = array();
            $lesson = $this->input->post();
            $lesson = $this->nonXssData($lesson);
            $lesson['ordering'] = ($lesson['ordering'] == '') ? 0 : $lesson['ordering'];
            $date = date('Y-m-d H:i:s');
            $lesson['created'] = $date;
            $relateVideos = [];
            if (isset($lesson['video_relates']) && count($lesson['video_relates']) > 0) {
                foreach ($lesson['video_relates'] as $relate) {
                    if ($relate['name'] != '') {
                        $relateVideos[] = $relate;
                    }
                }
            }
            $lesson['video_relates'] = serialize($relateVideos);
            
            $lessonId = $this->lesson_model->insertLesson( $lesson );
            if( $lessonId ) {
                $hash_id = md5($lessonId);
            	$this->lesson_model->updateHashId( $lessonId );
                $message = sprintf(lang('message_create_success'), lang('new_lesson'));
                $this->session->set_flashdata('success', $message);
                redirect(base_url() . 'lesson/edit/' . $hash_id);
            } else {
                $message = sprintf(lang('message_create_error'), lang('new_lesson'));
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'lesson');
        }
	}
    /**
    * [Edit]
    * update a lesson in lessons table 
    */
    public function edit( $hash_id )
    {
        $lesson = $this->lesson_model->getLessonById( $hash_id );
        if( !$lesson ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_lesson'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'lesson');
            return false;
        }
        $data = array();
        $courseOption = $this->course_model->getCourseOption();
        $data['courseOption'] = $courseOption;
        $data['lesson'] = $lesson;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'title',
                'label' => lang('title'),
                'rules' => 'trim|required|min_length[3]|is_unique[lessons.title]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'min_length' => lang('mes_minlength'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[post_details.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),
            
            array(
                'field' => 'price',
                'label' => lang('price'),
                'rules' => 'trim|numeric',
                'errors' => array(
                    'numeric' => '%s phải là số'
                ),
            ),
        );
        $info = $this->input->post(array('title', 'slug'));
        if( $info['title'] == $lesson->title ) {
            unset( $config[0] );
        }
        if( $info['slug'] == $lesson->slug ) {
            unset( $config[1] );
        }
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
            $this->breadcrumbs->add('Bài giảng', base_url().'lesson');
            $this->breadcrumbs->add('Sửa bài giảng', base_url().'lesson/edit/' . $hash_id);
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('lesson/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $lesson = array();
            $lesson = $this->input->post();
            $relateVideos = [];
            if (isset($lesson['video_relates']) && count($lesson['video_relates']) > 0) {
                foreach ($lesson['video_relates'] as $relate) {
                    if ($relate['name'] != '') {
                        $relateVideos[] = $relate;
                    }
                }
            }
            $lesson['video_relates'] = serialize($relateVideos);
            
            $lesson = $this->nonXssData($lesson);
            if(!isset($lesson['status'])) {
                $lesson['status'] = 0;
            }
            if(!isset($lesson['access'])) {
                $lesson['access'] = 0;
            }
            $lessonId = $this->lesson_model->updateLesson( $hash_id, $lesson );
            if( $lessonId ) {
                $message = sprintf(lang('message_update_success'), lang('the_lesson'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), lang('the_lesson'));
                $this->session->set_flashdata('error', $message);
            }
            redirect( base_url('lesson/edit'). '/'. $hash_id);
        }
    }
    /**
     * @name delete
     * Delete a article or any article by id
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $message = sprintf(lang('message_delete_warning'), lang('the_lesson'));
            $this->session->set_flashdata('warning', $message);
            redirect(base_url() . 'lesson');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->lesson_model->deleteLesson( $where, true );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('lessons'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('lessons'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;
        } else {
            $query = $this->lesson_model->deleteLesson( array( 'id' => (int) $ids ) );
            if( $query ) {
                $message = sprintf(lang('message_delete_success'), lang('the_lesson'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_delete_error'), lang('the_lesson'));
                $this->session->set_flashdata('error', $message);
            }
            echo 2;
            die;
        }
    }
    /*
    * changeFeatured
    * activate, deactivate a featured article
    */
    /**
     * [changeStatus of Course]
     * @return [int]
     */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->lesson_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('lessons'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('lessons'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->lesson_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('the_lesson'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('the_lesson'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }
    /**
    * Change Ordering
    **/
    public function ordering()
    {
        $data = array();
        $ordering = $this->input->post('ordering');
        $id = (int) $this->input->post('id');
        $hash_id = md5($id);
        if(isset($ordering) && !empty($ordering) && intval($ordering) && isset($id)){
            // Try catch Error
            try{
                $data['ordering'] = $ordering;
                if( $this->lesson_model->updateLesson( $hash_id, $data ) )
                {
                    echo 1; exit;
                } 
            } catch(Exception $ex){
                echo $ex->getMessage(); exit;
            }   
        }       
    }

    public function copy($id = '')
    {
        $lesson = $this->lesson_model->getLessonById($id);
        if( !$lesson ) {            
            $this->session->set_flashdata('warning', 'Không có bài giảng phù hợp.');
            redirect(base_url() . 'lesson');
            return false;
        }
        $rand = rand(1,100);
        $lesson->title = $lesson->title.'-copy' . $rand;        
        $lesson->status = 0;
        $lesson->slug = $lesson->slug . '-copy' . $rand;
        $args = (array) $lesson;        
        unset($args['id'], $args['hash_id']);
        $where = ['title' => $args['title']];
        $checkexist = $this->lesson_model->getLesson($where);
        if (empty($checkexist)) {

            if($lessonId = $this->lesson_model->insertLesson($args)){  

                $this->lesson_model->updateHashId($lessonId);          
                $this->session->set_flashdata('success', 'Đã Copy bài giảng thành công!');
            } else {                
                $this->session->set_flashdata('error', 'Không Copy được bài giảng');
            } 
        } else {
            $this->session->set_flashdata('error', 'Bài giảng này đã tồn tại');
        }    

        redirect(base_url() . 'lesson');
    }

}