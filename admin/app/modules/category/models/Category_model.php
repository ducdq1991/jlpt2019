<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends Bigshare_Model {

	public function __construct()
	{
		parent::__construct();
        $this->_table = 'categories';
	}

	/**
	 * [addCategory]
	 * @param [array] $args
	 */
	
	public function addCategory( $args )
	{	
		$this->_table = 'categories';
		$this->_data = $args;
		if($id = $this->addRecord()){
			return $id;
		}

		return false;
	}

	/**
	 * [addCategoryDetail]
	 * @param [array] $args
	 */
	
	public function addCategoryDetail($args)
	{
		$this->_table = 'category_details';
		$this->_data = $args;
		if( $this->addRecord()){
			return true;
		}

		return false;
	}
	/**
	 * [updateCategory]
	 * @param  [int] $categoryId
	 * @param  [array] $args
	 * @return [boolean]
	 */
	
	public function updateCategory($categoryId, $args)
	{
		$this->_table = 'categories';
		$where = array();
		$where['id'] = (int) $categoryId;
		$this->_wheres = $where;
		$this->_data = $args;
		if( $this->updateRecord()){
			return true;
		}
		return false;
	}

	/**
	 * [updateCategoryDetail]
	 * @param  [int] $categoryId
	 * @param  [array] $args
	 * @return [boolean]
	 */	
	
	public function updateCategoryDetail($categoryId, $args)
	{
		$this->_table = 'category_details';
		$where = array();
		$where['category_details.category_id'] = (int) $categoryId;
		$this->_wheres = $where;
		$this->_data = $args;
		if( $this->updateRecord()){
			return true;
		}
		return false;
	}	

	/**
	 * [totalCategory]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalCategory($where = array())
	{
		$this->_table = 'categories';
		$joins = array();
		$joinCategoryDetail['table'] = 'category_details';
		$joinCategoryDetail['condition'] = 'categories.id = category_details.category_id';
		$joinCategoryDetail['type'] = 'left';
		$joins[] = $joinCategoryDetail;
		$this->_joins = $joins;
		$this->_wheres = $where;
		return $this->totalRecord();
	}

	/**
	 * [delCategory delete One Category]
	 * @param  [int] $categoryId
	 * @return [boolean]
	 */
    public function deleteCategory($where = array(), $multiple = false)
    {
    	$this->_wheres = $where;
    	$this->_multiple = $multiple;
        return $this->deleteRecord();
    }

	/**
	 * [changeStatus Of Category]
	 * @param  [int or array] $categoryIds
	 * @param  [array] $categoryUpdate
	 * @return [boolean]
	 */
    public function changeStatus( $ids, $args = array() )
    {
    	$this->_data = $args;
        if( is_array($ids) ) {
        	$this->_batchConditionField = 'id';
        	$this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array('id' => (int)$ids);
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }

	/**
	 * [getCategoryById]
	 * @param  [int] $id
	 * @return [object]
	 */
	public function getCategoryById($categoryId)
	{
		if(!$categoryId){
			return null;
		}
		$where = $whereDetail = array();
		$where['id'] = $categoryId;
		$whereDetail['category_details.category_id'] = $categoryId;
		$this->_table = 'categories';
		$this->_wheres = $where;
		$category = (array) $this->getOneRecord();
		$categoryDetail = (array) $this->getCategoryDetail($whereDetail);	
		$data = (object) array_merge($category, $categoryDetail);
        return $data;
	}
	/**
	 * [getCategoryDetail description]
	 * @param  [type] $where [description]
	 * @return [type]        [description]
	 */
	public function getCategoryDetail( $where )
	{
		$this->_table = 'category_details';
		$this->_selectItem = 'category_details.name, category_details.slug, category_details.description, category_details.meta_keywords, category_details.meta_description';
		$this->_wheres = $where;
		return $this->getOneRecord();
	}
	/**
	 * [getParentCategory description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getParentCategory( $id )
	{
		$this->resetQuery();
		$this->_table = 'category_details';
		$this->_selectItem = 'category_details.name as parent_name';
		$where = array('category_details.category_id' => $id);
		$this->_wheres = $where;
		$result = (array) $this->getOneRecord();		
		return $result;
	}
	/**
	 * [getCategories description]
	 * @param  array   $where   [description]
	 * @param  integer $limit   [description]
	 * @param  integer $offset  [description]
	 * @param  array   $orderBy [description]
	 * @param  array   $likes   [description]
	 * @return [type]           [description]
	 */
	public function getCategories($where = array(), $limit = 10, $offset = 0, $orderBy = array(), $likes = array())
	{		
		$this->resetQuery();
		$this->_selectItem = "categories.id, categories.image, categories.parent, categories.created, categories.ordering, categories.status, category_details.name, category_details.slug, category_details.lang_id";
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $likes['value'];
		}
		if(isset($likes['field'])){
			$this->_likes['field'] = $likes['field'];
			$this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
		}
		$this->_from = 'categories';
		$joins = array();
		$joinCategoryDetail['table'] = 'category_details';
		$joinCategoryDetail['condition'] = 'categories.id = category_details.category_id';
		$joinCategoryDetail['type'] = 'left';
		$joins[] = $joinCategoryDetail;
		$this->_joins = $joins;		
		$this->_order = $orderBy;
		$this->_limit = $limit;
		$this->_offset = $offset;
		$this->_wheres = $where;
		$category = $this->getListRecords();
		$this->resetQuery();	
		$data = array();
		foreach($category as $item){
			$items = (array) $item;
			$where = array('category_details.category_id' => $item->id);		
			if($item->parent > 0){
				$parent = $this->getParentCategory($item->parent);	
				if($parent){
					$items = array_merge((array) $items, $parent);
				}							
			}
			$categoryDetail = (array) $this->getCategoryDetail( $where );	
			$data[] = (object) array_merge($items, $categoryDetail);					
		}
		$this->resetQuery();
		return $data;
	}

/**
 * [getCategoryOption description]
 * @param  integer $parentId [description]
 * @param  string  $str      [description]
 * @return [type]            [description]
 */
	public function getCategoryOption($parentId = 0, $currentId = 0, $str = '') {
	  $categories = array();
	  $html = '';
	  $this->db->select('categories.id, categories.parent, category_details.name');
	  $this->db->from('categories');
	  $this->db->join('category_details', 'categories.id = category_details.category_id', 'left');
	  $this->db->where('categories.parent', $parentId);
	  $result = $this->db->get()->result();	  
	  $selected = '';
	  if (count($result) > 0) {
		  foreach ($result as $item) {	   		
		  		$name = $str.$item->name;

		  		if($currentId > 0) {
		  			$selected = 'selected = "selected"';
		  		}
		  		$html .= '<option ' .$selected. ' data-parent="'.$item->parent.'" value="' . $item->id . '">' . $name . '</option>';
		  		$html .= $this->getCategoryOption($item->id, $currentId, $str.'|---');
		  }	
	  } 
	  
	  return $html;
	}	
}