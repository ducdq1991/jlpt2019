<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Bigshare_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_model');
		$this->lang->load('category', $this->session->userdata('lang'));
	}
	/**
	 * [index Load Category]
	 * @return [array]
	 */
	public function index($page = 0)
	{
		$data = $where = $orderCondition = $likes = $sortFields = [];	

		$where['category_details.lang_id'] = $this->_langId;

		$this->load->helper('paging');	
        $limit = $this->config->item('per_page');
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit;        
		$page = intval(str_replace('page-', '', $page));
		if ($page < 1) {
			$page = 1;
		}
		$offset = ($page - 1) * $limit;

		$orderOption = $this->input->get('order', '');
		$orderBy = $this->input->get('by', 'asc');
		if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
			$orderCondition[$orderOption] = $orderBy;
		}		
		$data['order'] = $orderOption;
		$data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc';

		$status = $this->input->get('status');
		if ($status != '') {
		  $where['categories.status'] = ($status ==='active') ? 1 : 0;
		}		

		$keyword = $this->input->get('keyword');
		if ($keyword != '') {
			$likes['field'] = 'name';
			$likes['value'] = (string) trim($keyword);
		}
		// Set actived Fillter

		$data['keyword'] = $keyword;				
		$data['status'] = $status;		
		$sortFields['name'] = lang('name_category');
		$sortFields['ordering'] = lang('orders');	
		$data['sortFields'] = $sortFields;
		$data['itemPerPages'] = $this->config->item('rowPerPages');
		$data['sortByItems'] = $this->config->item('sortBy');

		//Query
		$total = $this->category_model->totalCategory($where);      		
		$categories = $this->category_model->getCategories($where, $limit, $offset, $orderCondition, $likes);		
		$data['categories'] = $categories;

        $showFrom = $offset + count($categories);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $total;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord;

        $queryString = $_SERVER['QUERY_STRING'];
		$data['paging'] = paging(base_url() .'category', $page, $total, $limit, $queryString);
		 
		//Render Data
		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Danh mục', base_url().'category');
        $head['breadcrumb'] = $this->breadcrumbs->display();
		
		$this->load->view('layouts/header', $head);
		$this->load->view('category/index', $data);
		$this->load->view('layouts/footer');
	}
/**
 * [add Category]
 */
	public function add()
	{		
		$where = $data = $category = $categoryDetail = [];		

		$categoryOption = $this->category_model->getCategoryOption();		
		$data['categoryOption'] = $categoryOption;	
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', lang('name_category'), 'required');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if ($this->form_validation->run() == FALSE) {
				redirect( base_url() . 'category/add', 'refresh');					
	        } else {
	        	$category = [];

	        	// Config Upload
				$photoPath = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
	        	$config = [
	        		'upload_path'	=> $photoPath.'/upload',
	        		'allowed_types' => 'gif|jpg|png|jpeg',
					'max_size'     	=> 3000,
					'max_width' 	=> 1000,
					'max_height' 	=> 1000,
	        	];	
	        	$photo = null;
	        	// Check Upload
				if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
		        	$this->load->library('upload', $config);
		        	if ($this->upload->do_upload('image')) {
		        		$imgArray = $this->upload->data();
		        		$photo = isset($imgArray) ? 'upload/' . $imgArray['file_name'] : '';
		        		$category['image'] = $photo;		        	
		        	} else {						
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect( base_url() . 'category/add', 'refresh');
		        	} 
	        	}
	        	// Get Data from Input Form
	        	$postData = $this->input->post();
	        	$postData = $this->nonXssData($postData);
				if ($this->input->post('slug')) {
					$slug = $this->input->post('slug');
				} else {
					$this->load->helper('text');
					$convertTitle = convert_accented_characters($postData['name']);
					$slug = strtolower(url_title($convertTitle, 'dash', true));
				}
				// Category
				$category['parent'] = $postData['parent'];
				$category['status'] = isset($postData['status']) ? $postData['status'] : 0;
				$category['ordering'] = $postData['ordering'];
				$category['created'] = date('Y-m-d H:i:s');		
				// Category Detail
				$categoryDetail['name'] = $postData['name'];
				$categoryDetail['description'] = $postData['description'];
				$categoryDetail['slug'] = $slug;
				$categoryDetail['meta_keywords'] = $postData['meta_keywords'];
				$categoryDetail['meta_description'] = $postData['meta_description'];
				$categoryDetail['lang_id'] = $this->_langId;
				$categoryId = $this->category_model->addCategory( $category );
				if ($categoryId) {
					$categoryDetail['category_id'] = $categoryId;
					if ($this->category_model->addCategoryDetail($categoryDetail)) {
						$this->session->set_flashdata('success','Thêm danh mục thành công!');
					} else {
						$this->session->set_flashdata('error','Thêm danh mục thất bại!');
					}
				}

				redirect( base_url() . 'category', 'refresh');
	        }
	    }

		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Danh mục', base_url().'category');
        $this->breadcrumbs->add('Thêm mới', base_url().'category/add');
        $headers['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $headers);
        $this->load->view('category/add', $data);
		$this->load->view('layouts/footer');        
	}
/**
 * [edit Category]
 * @param  integer $categoryId
 * @return [type]
 */
	public function edit($categoryId = 0)
	{	
		$data = $where = $categoryUpdate = [];

		if (!$categoryId) {
			redirect('category','refresh');
		}

		$category = $this->category_model->getCategoryById((int)$categoryId );
		$data['category'] = $category;
		
		$categoryOption = $this->category_model->getCategoryOption(0,$category->parent);		
		$data['categoryOption'] = $categoryOption;	
		
		$this->load->library('form_validation');
		if (!$category) {
			$this->session->set_flashdata('error', 'Danh mục không tồn tại!');
			redirect('category','refresh');
		}

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->form_validation->set_rules('title_category', 'Tên danh mục', 'required');
			if ($this->form_validation->run() == FALSE) {
	        	$postData = $this->input->post();
	        	$postData = $this->nonXssData($postData);
				if ($this->input->post('slug')) {
					$slug = $this->input->post('slug');
				} else {
					$this->load->helper('text');
					$convertTitle = convert_accented_characters($postData['name']);
					$slug = strtolower(url_title($convertTitle, 'dash', true));
				}				
				
				$photoPath = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
	        	$config = [
	        		'upload_path'	=> $photoPath.'/upload',
	        		'allowed_types' => 'gif|jpg|png|jpeg',
					'max_size'     	=> 3000,
					'max_width' 	=> 1000,
					'max_height' 	=> 1000,
	        	];	

	        	$photo = null;
				if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {

		        	$this->load->library('upload', $config);
		        	if ($this->upload->do_upload('image')) {
		        		$imgArray = $this->upload->data();
		        		$photo = isset($imgArray) ? 'upload/' . $imgArray['file_name'] : '';
		        		$categoryUpdate['image'] = $photo;
		        		@unlink($photoPath . '/' .$category->image);
		        	} else {
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect( current_url(), 'refresh');
		        	}
				}
				
				$categoryUpdate['parent'] = $postData['parent'];
				$categoryUpdate['status'] = isset($postData['status']) ?  $postData['status'] : $category->status;
				$categoryUpdate['ordering'] = $postData['ordering'];
				$categoryUpdate['created'] = date('Y-m-d H:i:s');				
				// Update Detail
				$categoryUpdateDetail['name'] = $postData['name'];
				$categoryUpdateDetail['description'] = $postData['description'];
				$categoryUpdateDetail['slug'] = $slug;
				$categoryUpdateDetail['meta_keywords'] = $postData['meta_keywords'];
				$categoryUpdateDetail['meta_description'] = $postData['meta_description'];
				$categoryUpdateDetail['lang_id'] = $this->_langId;
				// Check Update
				if ($this->category_model->updateCategory($categoryId, $categoryUpdate)) {
					if ($this->category_model->updateCategoryDetail($categoryId, 
						$categoryUpdateDetail)) {
						$this->session->set_flashdata('success','Cập nhật danh mục thành công!');
					} else {
						$this->session->set_flashdata('error','Thay đổi danh mục thất bại!');
					}
				}

				redirect( base_url() . 'category', 'refresh');
	        }
		}

		$this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), base_url());
        $this->breadcrumbs->add('Danh mục', base_url().'category');
        $this->breadcrumbs->add('Sửa danh mục | ' . $category->name, base_url().'category/edit/' . $category->id);
        $headers['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $headers);
        $this->load->view('category/edit',$data);
		$this->load->view('layouts/footer');        
	}

	/**
     * @name Del
     * @desc delete a Module from modules table
     * */
    public function del()
    {
        $ids = $this->input->post('ids');
        if (empty($ids)) {
            $this->session->set_flashdata('warning', 'Không có lựa chọn nào để xóa');
            return false;
        }

        $where = [
            'field' => 'id',
            'value' => $ids
        ];

        if (is_array($ids)) {
            $query = $this->category_model->deleteCategory($where, true);
            if ($query) {
                $this->session->set_flashdata('success', 'Xóa lựa chọn thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa lựa chọn thất bại!');
            }

            echo 1;
            die;
        } else {
            $query = $this->category_model->deleteCategory(['id' => (int) $ids]);
            if ($query) {
                $this->session->set_flashdata('success', 'Xóa thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại!');
            }

            echo 1;
            die;
        }
    }
	/**
	 * [changeStatus of Category]
	 * @return [int]
	 */
	public function changeStatus()
	{
		$args = [];
		$ids = $this->input->post('ids');
		$type = $this->input->post('type');
		$status = (int) $this->input->post('status');

        if (!isset($ids) || empty($ids)){
			return false;
		}

		if (isset($type) && !empty($type) && $type =='multi') {				
			if (!empty($ids)){
				$args['status'] = $status;	
				if ($this->category_model->changeStatus($ids, $args)) {
                    $this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
				} else {
				    $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
                }

                echo 1;
                die;
			}
		} else {	
			$args['status'] = ($status == 1) ? 0 : 1;		
			if ($this->category_model->changeStatus($ids, $args)) {
				$this->session->set_flashdata('success', 'Thay đổi trạng thái thành công!');
			} else {
                $this->session->set_flashdata('error', 'Thay đổi trạng thái thất bại!');
            }

            echo 1;
            die;	
		}			
	}

	/**
	* Change Ordering
	**/
	public function ordering()
	{
		$data = [];
		$ordering = $this->input->post('ordering');
		$id = $this->input->post('id');

		if (isset($ordering) && !empty($ordering) && intval($ordering) && isset($id)) {
			// Try catch Error
			try {
				$data['ordering'] = $ordering;
				if ($this->category_model->updateCategory($id, $data)) {
					echo 1; exit;
				} 
			} catch(Exception $ex) {
				echo $ex->getMessage(); exit;
			}	
		}		
	}
}