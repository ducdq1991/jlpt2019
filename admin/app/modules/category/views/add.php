<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Thêm mới Danh mục</h5>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tên danh mục</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-category-slug form-control">
                                <?php echo form_error('name', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Danh mục cha</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="parent">
                                    <option value="0">--<?php echo lang('parent');?>--</option>
                                    <?php echo $categoryOption;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Mô tả</label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control note-codable"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slug</label>
                            <div class="col-sm-10"><input name="slug" type="text" value="" class="slug slug-category form-control"></div>
                        </div>                                              
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Từ khóa (SEO)</label>
                            <div class="col-sm-10"><input name="meta_keywords" type="text" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mô tả (SEO)</label>
                            <div class="col-sm-10"><textarea name="meta_description" type="text" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thứ tự</label>
                            <div class="col-sm-2"><input type="number" name="ordering" class="form-control"></div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-10">
                                <label class="checkbox-inline"><input type="checkbox" name="status" value="1">Kích hoạt</label> 
                            </div>
                        </div>                                              
                    </div>  
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Xóa</button>
                            <button class="btn btn-primary" name="submit" type="submit">Thêm mới</button>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>          