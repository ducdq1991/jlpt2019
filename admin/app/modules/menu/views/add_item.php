<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Tạo Menu</h5>
                <div class="ibox-tools">
                    <a href="<?php base_url('menu/add');?>" class="collapse-link">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tên menu</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control">
                                <?php echo form_error('name', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Menu cha</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="parent_id">
                                    <option value="0">--<?php echo lang('parent');?>--</option>
                                    <?php echo $menuOption;?>
                                </select>
                            </div>
                        </div>                          
                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>
                            <div class="col-sm-10">
                                <input type="text" name="link" class="form-control">
                                <?php echo form_error('link', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Icon</label>
                            <div class="col-sm-10">
                                <input type="text" name="icon" class="form-control">
                            </div>
                        </div>                        
                        <div class="form-group"><label class="col-sm-2 control-label">Ordering</label>
                            <div class="col-sm-10">
                                <input type="number" name="ordering" class="form-control">                                
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Trạng thái</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="status">
                                    <option value="0">Active</option>
                                    <option value="1">Inactive</option>
                                </select>                            
                            </div>
                        </div>                                                                    
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Xóa</button>
                            <button class="btn btn-primary" name="submit" type="submit">Thêm mới</button>
                            <a href="<?php echo base_url();?>menu/items/<?php echo $menuCategory->hash_id;?>" class="btn btn-success">Menu</a>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>          