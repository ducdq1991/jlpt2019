<?php defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();        
    }
    
    /**
     * @name addMenu
     * @desc insert new menu
     * @param [array] $args
     * */
    public function addMenu( $args = array() )
    {
        $this->_table = 'menus';
        $this->_data = $args;
		if($id = $this->addRecord() ){
			return $id;
		}
		return false;
    }

    public function addMenuItem( $args = array() )
    {
        $this->_table = 'menu_items';
        $this->_data = $args;
        if($id = $this->addRecord() ){
            return $id;
        }
        return false;
    }    
    
    /**
     * @name getMenus
     * @desc get all row from menus table
     * @param [array] $where
     * */
    public function getMenus( $where = array(), $limit = 0, $offset = 0, $likes = array() )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'menus';
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
     * @name getMenuItems
     * @desc get all row from menu_items table
     * @param [array] $where
     * */
    public function getMenuItems( $where = array(), $limit = 0, $offset = 0, $likes = array())
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'menu_items';
        $this->_wheres = $where;
        $menuItems = $this->getListRecords();

        $this->resetQuery();    
        $data = array();
        foreach($menuItems as $item){
            $items = (array) $item;
            $where = array('menu_items.id' => $item->id);        
            if($item->parent_id > 0){
                $parent = $this->getParentMenu($item->parent_id);  
                if($parent){
                    $items = array_merge((array) $items, $parent);
                }                           
            }            
            $data[] = (object) $items;                  
        }
        return $data;
    }   

    public function getParentMenu( $id )
    {
        $this->resetQuery();
        $this->_table = 'menu_items';
        $this->_selectItem = 'menu_items.name as parent_name';
        $where = array('menu_items.id' => $id);
        $this->_wheres = $where;
        $result = (array) $this->getOneRecord();        
        return $result;
    }     
    /**
	 * [numMenus]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function numMenu($where = array())
	{
        $this->_table = 'menus';
		$this->_wheres = $where;
		return $this->totalRecord();
	}
    public function numMenuItem($where = array())
    {
        $this->_table = 'menu_items';
        $this->_wheres = $where;
        return $this->totalRecord();
    }    
    /**
     * @name deleteMenu
     * @desc delete a row from menus table
     * @param [array] $where
     * */
    public function deleteMenu( $where = array(), $multiple = false )
    {
        $this->_table = 'menus';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }

    public function deleteMenuItem( $where = array(), $multiple = false )
    {
        $this->_table = 'menu_items';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }    
    
    
    public function getMenu( $where = array() )
    {
        $this->_table = 'menus';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }

    public function getMenuItem( $where = array() )
    {
        $this->_table = 'menu_items';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }    
    
    public function updateMenu( $where = array(), $args = array() )
    {
        $this->_table = 'menus';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
    public function updateMenuItem( $where = array(), $args = array() )
    {
        $this->_table = 'menu_items';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getMenuItemOption($menuId = '', $parentId = 0, $currentId = 0, $str = '') {
      $menuItems = $where = array();
      $html = '';
      $where['menu_items.menu_id'] = $menuId;
      $where['menu_items.parent_id'] = $parentId;
      $this->db->select('menu_items.id, menu_items.name, menu_items.parent_id');
      $this->db->from('menu_items');      
      $this->db->where($where);
      $result = $this->db->get()->result();   
      $this->resetQuery();
      $selected = '';
      foreach ($result as $item) {          
            $name = $str.$item->name;

            if($currentId === (int) $item->id) {              
                $selected = 'selected = "selected"';   
                $html .= '<option ' . $selected . ' value="' . $item->id . '">' . $name . '</option>';
            } else {
                $html .= '<option value="' . $item->id . '">' . $name . '</option>';
            }
            
            $html .= $this->getMenuItemOption($menuId, $item->id, $currentId, $str.'|---');
      }  
      return $html;
    }        
}