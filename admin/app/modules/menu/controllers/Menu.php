<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Menu
* @author TrungDoan <trung.doan@bigshare.vn>
* @copyright Bigshare
* 
*/

class Menu extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('menu_model');
	}
    public function index($page = 0)
    {
        $data = $where = $likes = array();
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));
        
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        
        $keyword = $this->input->get('keyword');
        $data['keyword'] = $keyword;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['menus.status'] = ($status ==='active') ? 1 : 0;
        }
        $numRecords = $this->menu_model->numMenu( $where );      
        $data['paging'] = paging(base_url() .'menu', $page, $numRecords, $limit); 
        $menus = $this->menu_model->getMenus( $where, $limit, $offset, $likes );
        $data['menus'] = $menus;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');

        $this->load->view('layouts/header');
        $this->load->view('menu/index', $data);
        $this->load->view('layouts/footer');
    }

    public function items($menuId = '', $page = 0)
    {
        $data = $where = $likes = array();
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));
        
        if($page < 1) $page = 1;
        $offset = ($page - 1) * $limit;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['menu_items.status'] = ($status ==='active') ? 1 : 0;
        }
        $menu = $this->menu_model->getMenu( array('hash_id' => $menuId) );
        if(!$menu){
            redirect(base_url().'menu');
        }
        $data['menu'] = $menu;
        $where['menu_id'] = $menu->id;
        $numRecords = $this->menu_model->numMenuItem( $where );      
        $data['paging'] = paging(base_url() .'module', $page, $numRecords, $limit); 
        $menus = $this->menu_model->getMenuItems( $where, $limit, $offset, $likes );
        $data['menus'] = $menus;
        $data['numRecord'] = $limit;
        $data['itemPerPages'] = $this->config->item('itemPerPages');

        $this->load->view('layouts/header');
        $this->load->view('menu/items', $data);
        $this->load->view('layouts/footer');
    }    

    public function add()
    {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('menu/add');
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args['name'] = $info['name'];

            if( $id = $this->menu_model->addMenu( $args ) ) {
                $update = array();
                $update['hash_id'] = md5($id);
                $this->menu_model->updateMenu(array('id' => $id), $update);
                $message = sprintf(lang('message_create_success'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'menu');
        }        
    }

    public function edit( $id )
    {
        if(!$id) {
            return false;
        }
        $data = array();
        $menu = $this->menu_model->getMenu( array('hash_id' => $id) );
        $data['menu'] = $menu;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('menu/edit', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args['name'] = $info['name'];
            if( $this->menu_model->updateMenu( array('hash_id' => $id), $args ) ) {
                $message = sprintf(lang('message_update_success'), $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'menu');
        }
    }

    public function delete($id)
    {
        if(!$id) {
            redirect(base_url() . 'menu');
        }
        $data = array();
        $menu = $this->menu_model->getMenu( array('hash_id' => $id) );   
        if($menu){
            if($this->menu_model->deleteMenu(array('id'=>$menu->id))){
                $this->menu_model->deleteMenuItem(array('menu_id' => $menu->id));
                redirect(base_url() . 'menu');
            }
        }   

        redirect(base_url() . 'menu');    
    }



    public function addItem($hashMenuId = ''){
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $menuCategory = $this->menu_model->getMenu(array('hash_id' => $hashMenuId));
        $menuOption = $this->menu_model->getMenuItemOption($menuCategory->id, 0);
        $data = array();
        $data['menuOption'] = $menuOption;
        $data['menuCategory'] = $menuCategory;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('menu/add_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args['name'] = $info['name'];
            $args['menu_id'] = $menuCategory->id;
            $args['parent_id'] = $info['parent_id'];
            $args['link'] = $info['link'];
            $args['ordering'] = $info['ordering'];
            $args['icon'] = $info['icon'];

            if( $id = $this->menu_model->addMenuItem( $args ) ) {
                $update = array();
                $update['hash_id'] = md5($id);
                $this->menu_model->updateMenuItem(array('id' => $id), $update);
                $message = sprintf(lang('message_create_success'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'menu/additem/'.$hashMenuId);
        }  
    }

    public function editItem($id = '', $menuId = ''){
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $menuCategory = $this->menu_model->getMenu(array('hash_id' => $menuId)); 
        $menuItem = $this->menu_model->getMenuItem(array('hash_id' => $id));   
        $menuOption = $this->menu_model->getMenuItemOption($menuCategory->id, 0, (int)$menuItem->parent_id);
        $data = array();
        $data['menuOption'] = $menuOption;
        $data['menuCategory'] = $menuCategory;
        $data['menu'] = $menuItem;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('layouts/header');
            $this->load->view('menu/edit_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args['name'] = $info['name'];
            $args['menu_id'] = $menuCategory->id;
            $args['parent_id'] = $info['parent_id'];
            $args['link'] = $info['link'];
            $args['ordering'] = $info['ordering'];
            $args['icon'] = $info['icon'];

            if($this->menu_model->updateMenuItem(array('hash_id'=> $id), $args ) ) {
                $message = sprintf(lang('message_updated_success'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' Menu ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'menu/items/'.$menuId);
        }  
    }  

    public function deleteItem($id, $menuId)
    {
        if(!$id) {
            redirect(base_url() . 'menu');
        }
        $data = array();
        $menu = $this->menu_model->getMenuItem( array('hash_id' => $id) );   
        if($menu){
            if($this->menu_model->deleteMenuItem(array('hash_id' => $id))){
                redirect(base_url() . 'menu/items/'.$menuId);
            }
        }   
        redirect(base_url() . 'menu/items/'.$menuId);    
    }      
}