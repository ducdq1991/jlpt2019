<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Index
* @author Trung Doan <trung.doan@bigshare.vn>
* @copyright Bigshare
* 
*/

class Index extends Bigshare_Controller {

	function __construct()
	{
		parent::__construct();
	}

    function switchLang($language = "") {    	
        $language = ($language != "") ? $language : "vietnamese";        
        if($language == 'vi'){
        	$langDir = 'vietnamese';
        } else {
        	$langDir = 'english';
        }
        $this->session->set_userdata('lang', $langDir);
        redirect(base_url());
    }	
}