<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Bigshare_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
	}
/**
 * [index Load Form Login]
 * @return [html]
 */
	function index(){
		$this->load->library('form_validation');
		$this->load->helper('form');
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->input->post()){
				try{
					$this->form_validation->set_rules('login','login','callback_checkLogin');
					if ($this->form_validation->run()){
						redirect(base_url() . 'dashboard','refresh');
					}	
				} catch(Exception $ex){
					echo $ex->getMessage();
					exit;
				}		
			}	
		}
		$this->load->view('login/login');
	}
/**
 * [checkLogin]
 * @return [type]
 */
	function checkLogin()
	{
		$this->form_validation->set_error_delimiters('<span class="label label-warning">', '</span>'); 
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$standardPassword = md5($password);	
		$where = array('username' => $username);
		try{
			if($this->auth_model->getUserByUsername($where)){
				$user = $this->auth_model->getUserByUsername($where);
				if($user->status != 1) {
					$this->form_validation->set_message(__FUNCTION__,'Tài khoản không được kích hoạt!');
					return false;
				}
				if($user->password === $standardPassword){ 
					$this->session->set_userdata('user', $user);
					$this->auth_model->updateLastLogin($username);	
					return true;
				} else {
					$this->form_validation->set_message(__FUNCTION__,'Mật khẩu không đúng!');
					return false;
				}
			}
		} catch( Exception $ex ){
			echo $ex->getMessage();
		}		
		$this->form_validation->set_message(__FUNCTION__,'Không tồn tại tài khoản!');
		
		return false;
	}

	public function logout()
	{
		$this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect(base_url(),'refresh');
	}
	
}