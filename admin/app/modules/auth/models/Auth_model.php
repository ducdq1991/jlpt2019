<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth_model extends Bigshare_model {

	protected $_table;

	public function __construct()
	{
		parent::__construct();
		$this->_table = 'users';
	}

	/**
	 * [getUserByUsername Get user by username]
	 * @param  array  $where
	 * @return [type]        [data or false]
	 */
	public function getUserByUsername($where = array())
    {
    	$this->_selectItem = 'users.id, users.username, users.password, users.email, users.phone, users.status, users.permission, users.last_login, users.group_id, user_groups.access_key';
        $this->_table = 'users';
        $joins = array();
        $joinGroup['table'] = 'user_groups';
        $joinGroup['condition'] = 'users.group_id = user_groups.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
        $this->_joins = $joins;
        $this->_wheres = $where;
        $user = $this->getOneRecord();
        $this->resetQuery();
        return $user;
	}

	public function updateLastLogin($username)
	{
		$where = array();
		$where['username'] = $username;
		$data['last_login'] = date('Y-m-d H:i:s');
		try{
			if($this->db->where($where)->update('users', $data))
				return true;
		} catch( Exception $ex ){
			return false;
		}
		return false;
	}

	/**
	 * [generateSalt Createing security satl code]
	 * @param  integer $s [description]
	 * @return [type]     [description]
	 */
	public function generateSalt($s=8)
	{
		$rand='';
		for($i=0;$i<8;++$i)
			$rand.=pack('S',mt_rand(0,0xffff));
		$rand.=microtime();
		$rand=md5($rand);
		$salt=strtr(substr(base64_encode($rand),0,(int)$s),array('+'=>'.'));
		return $salt;
	}
}