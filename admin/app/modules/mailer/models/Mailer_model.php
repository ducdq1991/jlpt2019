<?php defined('BASEPATH') or exit('No direct script access allowed');

class Mailer_model extends Bigshare_Model
{
     public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
    public function getUsers( $where = array(), $limit = 0, $offset = 0, $likes = array())
	{
        $this->_selectItem = 'users.id, users.username, users.email';
        $this->_from = 'users';
		$joins = array();
        $joinGroup['table'] = 'user_groups';
        $joinGroup['condition'] = 'users.group_id = user_groups.id';
        $joinGroup['type'] = 'left';
        $joins[] = $joinGroup;
		$this->_joins = $joins;
        $this->_limit = $limit;
		$this->_offset = $offset;
        $where['user_groups.access_key ='] = 'member';
        $this->_wheres = $where;
        $queryUsers = $this->getListRecords();
        $this->resetQuery();
        $users = array();
        foreach( $queryUsers as $user ) {
            $userMetas = $this->getUserMetas($user->id);
            $user->full_name = $userMetas;
            $users[] = $user;
        }
        return $users;
	}
    
    public function getUserMetas( $userId )
    {
        $this->_selectItem = 'user_metas.meta_value';
        $where = array();
        $where['user_metas.user_id'] = (int) $userId;
        $where['user_metas.meta_key'] = 'full_name';
        $this->_wheres = $where;
        $this->_table = 'user_metas';
        $metas = $this->getOneRecord();
        if($metas) {
            return $metas->meta_value;
        }
        return null;
    }
    public function getGroups( $where = array(), $limit = 0, $offset = 0 )
    {
        $this->_selectItem = 'id, name';
        $where['access_key'] = 'member';
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'user_groups';
        $this->_wheres = $where;
        return $this->getListRecords();
    }
}