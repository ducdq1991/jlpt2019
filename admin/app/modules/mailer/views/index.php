<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo lang('mail_management');?></h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <form class="form form-horizontal form-add-user" role="form" method="POST" accept-charset="utf-8">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="subject"><?php echo lang('subject');?></label>
                        <div class="col-sm-8">
                            <input type="text" name="subject" class="form-control" id="subject" value="<?php echo set_value('subject'); ?>" placeholder="">
                            <?php echo form_error('subject', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="time"><?php echo lang('to'); ?></label>
                        <div class="col-sm-8">
                                <select name="emails[]" id="multi-select-user" data-placeholder="Chọn thành viên" class="chosen-select" multiple>
                                <?php foreach ($users as $value) : ?>
                                    <option value="<?php echo $value->email; ?>">
                                        <?php 
                                            echo $value->username; 
                                            if(!empty($value->full_name)) echo ' ('.$value->full_name.')'; 
                                            echo ' - '.$value->email;
                                        ?>
                                    </option>
                                <?php endforeach; ?>
                                </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="time"></label>
                        <div class="col-sm-8">
                            <select name="group" class="form-control" id="group_email">
                                <option value="">Nhóm học viên</option>
                                <?php foreach ($groups as $value) : ?>
                                    <option value="<?php echo $value->id; ?>">
                                        <?php echo $value->name;?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="time"><?php echo lang('message'); ?></label>
                        <div class="col-sm-8">
                            <textarea id="message" name="message" class="form-control editor"></textarea>
                            <?php echo form_error('message', '<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>', '</div>'); ?>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="reset" class="btn btn-white"><?php echo lang('reset');?></button>
                            <button type="submit" id="send-email-group" class="btn btn-info"><?php echo lang('send');?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $tinymce; ?>