<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Mailer
* @author ChienDau
* @copyright Bigshare.,JSC
* 
*/

class Mailer extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('mailer_model');
        $this->lang->load('mailer', $this->session->userdata('lang'));
        $this->load->model('user/user_model');
	}
	public function index()
	{
		$data = [];
		$this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);
		$this->load->library('form_validation');		
		$users = $this->mailer_model->getUsers();
		$data['users'] = $users;
		$groups = $this->mailer_model->getGroups();
		$data['groups'] = $groups;
		$config = array(
            array(
                'field' => 'message',
                'label' => lang('message'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),
        );
		$this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Newletters', base_url().'mailer');
            $head['breadcrumb'] = $this->breadcrumbs->display(); 
                    
            $this->load->view('layouts/header', $head);
            $this->load->view('mailer/index', $data);
            $this->load->view('layouts/footer');
        } else {
        	$info = $this->input->post();
        	$where = ['group_id' => (int) $info['group']];
			$users = $this->mailer_model->getUsers($where);
			$emails = [];
			foreach ($users as $value) {
				$emails[] = $value->email;
			}
            if(isset($info['emails'])) {
    			$emails = array_merge($emails, $info['emails']);
    			$emails = array_unique($emails);
            }
			$r = 0;
			foreach ($emails as $value) {
				$this->sendEmail($value, $info['subject'], $info['message']);
				$r++;
			}
			$message = sprintf( $r.' thành viên đã được gửi mail');
            $this->session->set_flashdata('warning', $message);
            redirect(base_url('mailer'));
        }
	}
	/**
     * @name sendEmail
     * send a mail to user
     * */
    public function sendEmail( $email = '', $subject = '', $message = '' )
    {
        date_default_timezone_set('GMT');
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'ngocquangman2@gmail.com',
            'smtp_pass' => 'dufrckfwgreudulm',
            'smtp_port' => 465,
            'charset' => 'utf8',
            'mailtype'  => 'html',
        ));
        $this->email->from($this->getEmail(), $this->getName());
        $this->email->to($email);     
        $this->email->subject($subject);
        $this->email->set_newline("\r\n");
        $this->email->message($message);
        $this->email->send();
    }
}
