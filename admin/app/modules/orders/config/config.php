<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['statusOrder'] = [
	'pending' => 'pending',
	'progressing' => 'progressing',
	'denied' => 'denied',
	'complete' => 'complete',
];