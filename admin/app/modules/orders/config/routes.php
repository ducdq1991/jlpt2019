<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['orders/page-([\d]+)'] = "orders/index/$1";