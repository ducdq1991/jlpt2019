<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Bigshare_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('orders_model');
        $this->load->model('salemans/salemans_model'); 
        $this->lang->load('orders', $this->session->userdata('lang'));       
    }

    public function index($page = 0)
    {
        $data = $where = $like = $orderCondition = [];
        $data['langId'] = $this->_langId;

        $langId = $this->_langId;

        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');   
        $numRecord = $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? (int) $numRecord : $limit; 

        $page = intval(str_replace('page-', '', $page));
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;

        //Get Filters
        $fullname = $this->input->get('fullname');
        if($fullname !=''){
            $like['field'] = 'orders.fullname';
            $like['value'] = (string) trim($fullname);
        }
        $data['fullname'] = $fullname;

        $email = $this->input->get('email');
        if($email !=''){
            $like['field'] = 'orders.email';
            $like['value'] = (string) trim($email);
        }
        $data['email'] = $email;

        $phone = $this->input->get('phone');
        if($phone !=''){
            $like['field'] = 'orders.phone';
            $like['value'] = (string) trim($phone);
        }
        $data['phone'] = $phone;                

        $status = $this->input->get('status');
        if ($status != '') {
            $where['orders.status'] = $status;
        }

        $data['status'] = $status;

        $orderOption = $this->input->get('order', '');
        $orderBy = $this->input->get('by', 'asc');
        if (isset($orderOption) && ( !empty($orderOption) || $orderOption !='')) {
            $orderCondition[$orderOption] = $orderBy;
        }       
        $data['order'] = $orderOption;
        $data['sortBy'] = ($orderBy == 'asc') ? 'desc' : 'asc'; 
        $fromDate = $this->input->get('from_date');
        $data['fromDate'] = $fromDate;

        $toDate = $this->input->get('to_date');
        $data['toDate'] = $toDate;

        if ($fromDate != '' && $toDate != '') {
            $where['orders.created_at >='] = date('Y-m-d', strtotime(str_replace('/', '-', $fromDate)));
            $where['orders.created_at <='] = date('Y-m-d', strtotime(str_replace('/', '-', $toDate)));
        }

        $totalCount = $this->orders_model->totalOrder($where); 
        $listItems = $this->orders_model->getOrders($where, $limit, $offset, $orderCondition, $like);
        $data['listItems'] = $listItems;

        $isExport = $this->input->get('is_export');
        if ($isExport == 1) {
            $exportDatas = $this->orders_model->getOrders($where, 0, $offset, $orderCondition, $like);
            $this->exportCSV($exportDatas, []);
        }
        
        $queryString = $_SERVER['QUERY_STRING'];   
        if ($queryString == '') {
            $data['queryStringExport'] = '?is_export=1';
        } else {
            $data['queryStringExport'] = '?' . $queryString . '&is_export=1';
        }


        $queryString = $_SERVER['QUERY_STRING'];   
        $data['paging'] = paging(base_url() .'orders', $page, $totalCount, $limit, $queryString);
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $statusOrder = $this->config->item('statusOrder');
        $data['statusOrder'] = $statusOrder;

        $showFrom = $offset + count($listItems);        
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $totalCount;
        $data['numRecord'] = ($numRecord == '') ? $limit : $numRecord; 

        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), 
            base_url());
        $this->breadcrumbs->add('Đơn hàng', base_url().'orders');        
        $head['breadcrumb'] = $this->breadcrumbs->display();

        $this->load->view('layouts/header', $head);
        $this->load->view('orders/index', $data);
        $this->load->view('layouts/footer');
    }

    protected function exportCSV($orders = [], $courseNames = [])
    {
        $this->load->helper('download');
        $this->load->library('PHPReport');
        $template = 'DS_DH.xlsx';
        $templateDir = FCPATH . "assets" . DIRECTORY_SEPARATOR;
        $config = [
            'template' => $template,
            'templateDir' => $templateDir      
        ];
        $data = []; 
        $i = 1;
        if (!empty($orders)) {
            foreach ($orders as  $item) { 
                $p = 0;                      
                foreach ($item->order_details as $product) {
                    $p++;

                    $orderItem = [];
                    $orderItem['stt'] = ($p == 1) ? $i : '';
                    $orderItem['formatDate'] = ($p == 1) ? date('d/m/Y', strtotime($item->created_at)) : '';
                    $orderItem['order_customer_name'] = ($p == 1) ? $item->fullname : '';
                    $orderItem['order_customer_address'] = ($p == 1) ? $item->address : '';
                    $orderItem['order_customer_phone'] = ($p == 1) ? $item->phone : '';
                    $orderItem['district_name'] = ($p == 1) ? $item->district_name : '';
                    $orderItem['province_name'] = ($p == 1) ? $item->province_name : '';
                    $orderItem['discount'] = ($p == 1) ? $item->discount : '';
                    $orderItem['inv_total'] = ($p == 1) ? (float) $item->total - (float) $item->discount : '';
                    $orderItem['product_name'] = $product->product_name;
                    $orderItem['quantity'] = $product->qty;
                    $orderItem['price'] = $product->price;
                    $orderItem['subtotal'] = $product->qty * $product->price;
                    $data[] = $orderItem;
                }
                $i++;
            }
        }

        //load template
        $R = new PHPReport($config);

        $R->load([
            [
              'id' => 'item',
              'repeat' => TRUE,
              'data' => $data  
            ]
        ]);

        // define output directoy
        $outputFileDir =  FCPATH . "assets" . DIRECTORY_SEPARATOR;
        $filename = 'DS_DH' . time();
        $file = $outputFileDir  . $filename . ".xlsx";
        $result = $R->render('excel', $file);
        force_download($file, NULL);        
    }

    public function update($id = 0)
    {
        $message = sprintf(lang('message_update_error'), 'Đơn hàng');
        $messageClass = 'error';

        if ($id == 0 || $id == '') {
            redirect(base_url() . 'orders');
        }

        $data = $where = [];
        $langId = $this->_langId;


        $where['orders.id'] = $id;

        $item = $this->orders_model->getOrder($where);
        $data['order'] = $item;

        $this->load->library('form_validation');

        $config = [
            [
                'field' => 'label',
                'label' => lang('label'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('label') . lang('is_required'),
                ]
            ],
            [
                'field' => 'name',
                'label' => lang('name'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('name') . lang('is_required'),
                ]
            ],
            [
                'field' => 'meta_group_id',
                'label' => lang('metagroups'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('metagroups') . lang('is_required'),
                ]
            ],
            [
                'field' => 'type',
                'label' => lang('dataType'),
                'rules' => 'trim|required',
                'errors' => [
                    'required' => lang('dataType') . lang('is_required'),
                ]
            ]                                                 
        ];

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), 
                base_url());
            $this->breadcrumbs->add('Đơn hàng', base_url().'orders');
            $this->breadcrumbs->add(lang('update'), base_url().'orders/update');
            $head['breadcrumb'] = $this->breadcrumbs->display();              
            $this->load->view('layouts/header', $head);
            $this->load->view('orders/update', $data);
            $this->load->view('layouts/footer');
        } else {
            $args = [];
            $args = $this->input->post();
            $args = $this->nonXssData($args);           
            if ($this->orders_model->updateMeta($id, $args)) {
                $message = sprintf(lang('message_update_success'), 'Đơn hàng');
                $messageClass = 'error';                
            }

            redirect(base_url().'orders');
        }
    }

    public function view($orderId = 0)
    {
        $data = $head = [];
        $where = [
            'id' => $orderId
        ];
        $order = $this->orders_model->getOrder($where);
        $data['order'] = $order;
        $district = $this->orders_model->getDistrict(['districts.id' => $order->district_id]);
        $data['district'] = $district;
        $province = $this->orders_model->getProvince(['provinces.id' => $order->province_id]);
        $data['province'] = $province;

        $statusOrder = $this->config->item('statusOrder');
        $data['statusOrder'] = $statusOrder;

        $orderDetails = $this->orders_model->getOrderDetail(['order_id' => $orderId]);
        $data['orderDetails'] = $orderDetails;
        
        $this->breadcrumbs->add('<i class="fa fa-home"></i> ' . lang('dashboard'), 
            base_url());
        $this->breadcrumbs->add('Đơn hàng', base_url().'orders');
        $this->breadcrumbs->add(lang('view'), base_url().'orders/view');
        $head['breadcrumb'] = $this->breadcrumbs->display();   

        $this->load->view('layouts/header', $head);
        $this->load->view('orders/view', $data);
        $this->load->view('layouts/footer');
    }

    public function delete($id = 0)
    {
        if( $id ) { 
            $ids = (int) $id; 
        } else {
            $ids = $this->input->post('ids');
        }
        if( empty($ids) ) {
            $this->session->set_flashdata('warning', lang('no_chooise'));
            redirect(base_url() . 'orders');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->orders_model->deleteOrder($where, true);
            if( $query ) {
                $this->session->set_flashdata('success', sprintf(lang('message_delete_success'), ''));
            } else {
                $this->session->set_flashdata('error', sprintf(lang('message_delete_error'), ''));
            }
            echo 1;
            die;
        } else {
            $query = $this->orders_model->deleteOrder(['id' => (int) $ids]);
            if( $query ) {
                $this->session->set_flashdata('success', sprintf(lang('message_delete_success'), ''));
            } else {
                $this->session->set_flashdata('error', sprintf(lang('message_delete_error'), ''));
            }
            echo 2;
            die;
        }
    }

    public function changeStatus()
    {
        $data = $this->input->post();
        if (!empty($data)) {
            $orderId = $data['id'];
            unset($data['id']);
            $where = [
                'id' => $orderId
            ];
            $order = $this->orders_model->getOrder($where);
            if ($order) {
                if ($this->orders_model->updateOrder($orderId, $data)) {
                    if ($data['status'] == 3) {
                        $saleman = $this->salemans_model->getSaleman(['salemans.coupon_code' => $order->discount_code]);
                        if ($saleman) {
                            
                            $payout = [];
                            $payout['name'] = 'Cộng tiền CTV từ đơn mua sách #' . $order->id;
                            $payout['note'] = $order->id . ', ' . $order->discount_code . ', ' . $saleman->id;
                            $payout['amount'] = $order->discount;
                            $payout['receiver_id'] = $saleman->id;
                            $payout['receiver_name'] = $saleman->name . '-' . $saleman->coupon_code;
                            $payout['data_id'] = $order->id;
                            $payout['type'] = 'plus';
                            $payout['pay_group'] = 'book_order';
                            $payout['created_at'] = date('Y-m-d H:i:s');
                            if ($this->checkPayout($payout)) {
                                $amount = $saleman->amount + $order->discount;
                                $this->salemans_model->updateSaleman(['salemans.id' => $saleman->id], ['amount' => $amount]);
                                $this->setPayout($payout);
                            }
                            
                        }
                    }
                    echo 1;
                    exit;
                }
            }
        }
        echo 2;
        exit;
    }
}