<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-plus"></i> <?php echo lang('create');?></h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                <?php echo validation_errors('<div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>', '</div>'); ?>                
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('label');?></label>
                            <div class="col-sm-10">
                                <input type="text" name="label" value="<?php echo set_value('label');?>" class="autoSlug auto-course-slug form-control">
                            </div>
                        </div>                     
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('name');?></label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?php echo set_value('name');?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('metagroups');?></label>
                            <div class="col-sm-10">
                                <select class="form-control" id="meta_group_id" name="meta_group_id">
                                    <option value=""><?php echo lang('choose');?></option>                                    
                                <?php 
                                    foreach ($metaGroups as $item):
                                ?>
                                     <option value="<?php echo $item->meta_group_id;?>"><?php echo $item->name;?></option>
                                <?php 
                                    endforeach;
                                ?>
                                 </select>
                            </div>
                        </div>                        
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('description');?></label>
                            <div class="col-sm-10">
                                <textarea name="note" class="form-control"><?php echo set_value('note');?></textarea>
                            </div>
                        </div>                                                                 
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('dataType');?></label>
                            <div class="col-sm-10">
                                <select class="form-control" id="type" name="type" onchange="meta_Type(this.value)">
                                    <option value=""><?php echo lang('choose');?></option>
                                     <option value="varchar">Kiểu chuỗi</option>
                                     <option value="text">Kiểu văn bản</option>
                                     <option value="int">Kiểu số nguyên</option>
                                     <option value="float">Kiểu số thực</option>
                                     <option value="select">Kiểu danh sách</option>
                                     <option value="datetime">Kiểu ngày tháng</option>
                                 </select>
                            </div>
                        </div>
                        <div id="fieldSelect" class="form-group list-item-select" style="display: none">
                            <label class="col-sm-2 control-label">List Item</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" cols="2" name="list_items"></textarea>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label"><?php echo lang('dataSize');?></label>
                            <div class="col-sm-10">
                                <input type="number" name="length" value="<?php echo set_value('length', 128);?>" class="form-control">
                            </div>
                        </div>                                                        
                    </div>  
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-1">
                            <a class="btn btn-info" href="<?php echo base_url();?>metas"><?php echo lang('back_to');?></a>                        
                            <button class="btn btn-warning" type="reset">
                                <?php echo lang('reset');?>
                            </button>
                            <button class="btn btn-primary" type="submit">
                                <?php echo lang('create');?>
                            </button>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>  
<script type="text/javascript">
    function meta_Type(value)
    {
        if (value == 'select') {
            $('#fieldSelect').css('display', 'block');
        } else {
            $('#fieldSelect').css('display', 'none');
        }
    }
</script>