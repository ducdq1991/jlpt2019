<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5><i class="fa fa-tag"></i> <?php echo lang('view_of');?>:  <?php echo $order->fullname;?></h5>
				<div class="ibox-tools">
				</div>
			</div>
			<div class="ibox-content" style="overflow: hidden">
				<div>
					<label for="">Khách hàng:</label> <?php echo $order->fullname;?>
				</div>
				<div class="col-md-2">
					<label for=""><?php echo lang('created_date_order');?>:</label> <?php echo date('d/m/Y H:i A', strtotime($order->created_at));?>
				</div>
				<div class="col-md-2">
					<label for=""><?php echo lang('phone');?>:</label> <?php echo $order->phone;?>
				</div>
				<div class="col-md-2">
					<label for=""><?php echo lang('email');?>:</label> <?php echo $order->email;?>
				</div>
				<div class="col-md-2">
					<label for=""><?php echo lang('address');?>:</label> <?php echo $order->address;?>, <?php echo $district->name;?>, <?php echo $province->name;?>
				</div>
				<div class="col-md-2">
					<label for=""><?php echo lang('total');?>:</label> <?php echo number_format($order->total);?>
				</div>
                <div class="col-md-2">
                    <label for="">Giảm giá:</label> <?php echo number_format($order->discount);?>
                </div>
                <div class="col-md-2" style="color:red; font-weight: 700">
                    <label for="">Tổng thanh toán:</label> <?php echo number_format($order->total - $order->discount);?>
                </div>
				<div class="col-md-2">
					<label for=""><?php echo lang('status');?>:</label> <?php echo lang($statusOrder[$order->status]);?>
				</div>
				<div class="col-md-12">
					<h4>
						<?php echo lang('order_detail');?>
					</h4>

            <table class="table table-striped table-bordered table-hover" id="editable" >
                    <thead>
                        <tr role="row">
                            <th class="text-center"><?php echo lang('product_name');?></th>
                          <th class="text-center"><?php echo lang('price');?> </th>                                      
                            <th class="text-center"><?php echo lang('quantity');?></th>                                                                      
                            <th class="text-center"><?php echo lang('sub_total');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        if (count($orderDetails) > 0):
                            foreach($orderDetails as $key=>$item) : 
                    ?>
                        <tr id="row-<?php echo $key; ?>" class="">                                   
                            <td>
                                <strong><?php echo $item->product_name; ?></strong>
                            </td>
                            
                            <td class="text-center">
                                <?php echo $item->price; ?>
                            </td>                                      
                            <td class="text-center">
                                <?php echo $item->qty; ?>
                            </td>                                  
                            <td class="text-center">
                                <?php echo number_format($item->price * $item->qty); ?>
                            </td>                                                   
                        </tr>
                    <?php 
                            endforeach;
                        else:
                    ?>
                   <tr>
                        <td colspan="10" class="text-center">
                        <?php echo lang('no_record');?></td>
                    </tr>                        
                    <?php 
                        endif;
                    ?>
                    </tbody>
                </table>

				</div>																
			</div>
		</div>
	</div>
</div>