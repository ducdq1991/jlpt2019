<div class="row">
    <div class="col-lg-12">        
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-book"></i> Đơn hàng</h5>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                    <form method="GET" accept-charset="utf-8" id="formList">
                        <input type="hidden" id="sort" name="order" value="">
                        <input type="hidden" id="sortBy" name="by" value="">
                        <input type="hidden" id="numRecord" name="numRecord" value="<?php echo $numRecord;?>">

                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'orders/delete'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>                                
                                </div>
                            </div>                    
                        </div>                                                
                        <div class="form-group">

                            <div class="col-md-1">
                            <select class="rowPerPage form-control input-sm sys-filler">
                            <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="fullname" value="<?php echo $fullname;?>">                         
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="Email" name="email" value="<?php echo $email;?>">                         
                            </div>
                            <div class="col-md-1 text-right">
                                <input type="text" class="form-control datepicker input-sm sys-filler" placeholder="Ngày bắt đầu" name="from_date" value="<?php echo @$fromDate;?>">                         
                            </div>
                            <div class="col-md-1 text-right">
                                <input type="text" class="form-control datepicker input-sm sys-filler" placeholder="Ngày kết thúc" name="to_date" value="<?php echo @$toDate;?>">                         
                            </div>
                            <div class="col-md-2 text-right">
                                <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('phone');?>" name="phone" value="<?php echo $phone;?>">                         
                            </div>                                                        
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> <?php echo lang('search');?></button>
                                <a href="<?php echo base_url('orders') .  $queryStringExport;?>" title="" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Excel</a>
                            </div>

                            <div class="col-md-2">
                                <select class="form-control" id="status" name="status" onchange="this.form.submit();">
                                    <option value=""><?php echo lang('status');?></option>                                    
                                <?php 
                                    foreach ($statusOrder as $key=>$value):
                                ?>
                                     <option <?php if ($status === $key) echo 'selected="selected"'; ?>  value="<?php echo $key;?>"><?php echo $value;?></option>
                                <?php 
                                    endforeach;
                                ?>
                                 </select>                         
                            </div>           
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="editable" >
                            <thead>
                                <tr role="row">
                                    <th class="text-center">
                                        <input id="checkAll" type="checkbox"/>
                                    </th>
                                    <th class="text-center">ID <a href="javascript: doSort('id', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                  <th class="text-center"><?php echo lang('created_date');?> <a href="javascript: doSort('created_date', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>                                      
                                    <th class="text-center">Họ tên <a href="javascript: doSort('fullname', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>                                                                        
                                    <th class="text-left" width="250">Địa chỉ <a href="javascript: doSort('address', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center">SĐT <a href="javascript: doSort('phone', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>

                                    <th class="text-left">Sách</th>

                                    <th class="text-center" width="100"><?php echo lang('amount');?> <a href="javascript: doSort('total', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>
                                    <th class="text-center" width="100">Giảm giá</th>
                                    <th class="text-center" width="100">Tổng Thanh Toán</th>
                                    <th class="text-center" width="150"><?php echo lang('status');?> <a href="javascript: doSort('status', '<?php echo $sortBy;?>')"><i class="fa fa-sort"></i></a></th>                                                                                                             
                                    <th class="text-center"><?php echo lang('actions');?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if (count($listItems) > 0):
                                    foreach($listItems as $k=>$item) : 
                            ?>
                                <tr id="row-<?php echo $k; ?>" class="">
                                    <td class="text-center">
                                        <input class="checkItem" type="checkbox" value="<?php echo $item->id;?>" name="checkItems[]" />
                                    </td>
                                    <td class="text-center"><?php echo $item->id; ?></td>                                    
                                    <td>
                                        <?php echo date('d/mY H:i:A', strtotime($item->created_at));?>
                                    </td>                                     
                                    <td>
                                        <strong><?php echo $item->fullname; ?></strong>
                                    </td>                                     
                                    <td>
                                        <?php echo $item->address . ', ' . $item->district_name . ', ' . $item->province_name; ?>
                                    </td>                                  
                                    <td class="text-center">
                                        <?php echo $item->phone; ?>
                                    </td>
                                    <td class="text-left">
                                        <?php echo $item->order_details[0]->product_name; ?>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php echo number_format($item->total); ?></strong> <sup>đ</sup>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php echo number_format($item->discount); ?></strong> <sup>đ</sup>
                                        <br>
                                        <span><?php echo $item->discount_code;?></span>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php echo number_format($item->total - $item->discount); ?></strong> <sup>đ</sup>
                                    </td>                                                    
                                    <td class="text-center">
                                        <?php 
                                            
                                            if ($item->status == 'progressing') {
                                                $statusLabel = 'info';
                                            } elseif ($item->status == 'denied') {
                                                $statusLabel = 'danger';
                                            } elseif ($item->status == 'complete') {
                                                $statusLabel = 'success';
                                            } else {
                                                $statusLabel = 'default';
                                            }
                                        ?>   
                                        <select class="form-control changeStatusOrder" data-id="<?php echo $item->id;?>">
                                            <?php 
                                                foreach ($statusOrder as $key=>$value):
                                            ?>
                                                 <option <?php if ($item->status == $key) echo 'selected="selected"'; ?>  value="<?php echo $key;?>"><?php echo $value;?></option>
                                            <?php 
                                                endforeach;
                                            ?>                                        
                                        </select>                                     
                                    </td>                                                                                                                                 
                                    <td width="10%" class="text-center">
                                        <a class="btn btn-xs btn-info" href="<?php echo base_url();?>orders/view/<?php echo $item->id; ?>" title="View">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <!--<a class="btn btn-xs btn-default" href="<?php //echo base_url();?>orders/update/<?php //echo $item->id; ?>" title="Update"><i class="fa fa-pencil-square-o"></i></a>-->
                                        <a class="btn btn-xs btn-danger" onclick="delSingle('<?php echo base_url('orders/delete'); ?>', <?php echo $item->id; ?>)" title="Delete"><i class="fa fa-trash"></i>
                                        </a>                                        
                                    </td>
                                </tr>
                            <?php 
                                    endforeach;
                                else:
                            ?>
                           <tr>
                                <td colspan="10" class="text-center">
                                <?php echo lang('no_record');?></td>
                            </tr>                        
                            <?php 
                                endif;
                            ?>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <div class="col-md-1">
                                <select class="rowPerPage form-control input-sm sys-filler">
                                <?php foreach($itemPerPages as $key=>$value):?>
                                    <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                    <?php endforeach;?>
                                </select>                             
                            </div>
                            <div class="paging col-md-8 text-center">
                                <?php
                                    if (count($listItems) > 0) {
                                        echo $paging;    
                                    }                                 
                                ?>                                
                            </div>
                            <div class="col-md-3 padding-middle text-right">
                                <?php 
                                    if (count($listItems) > 0) {                                        
                                        echo sprintf(lang('showing'), number_format($showFrom), number_format($showTo), number_format($totalCount));
                                    }
                                ?>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 form-module-action text-right">
                                <div class="pull-left btn-function" style="display:none;">
                                    <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'orders/delete'; ?>">
                                    <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>                               
                                </div>  
                            </div>                    
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var message_confirm_delete = '<?php echo lang('message_confirm_delete'); ?>';
$(document).ready(function () {
    $('.changeStatusOrder').change(function(){
        status = $(this).val();
        orderId = $(this).attr('data-id');
        data = {status:status,id:orderId};
        $.ajax({
            url : "<?php echo base_url();?>orders/changestatus",
            type: "POST",
            data : data,
            success: function(res, textStatus, jqXHR)
            {
                if (res == 1){
                    alert('Success');
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
         
            }
        });     
    });
});
</script>