<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['module_name'] = 'Quản lý đơn hàng';
$lang['phone'] = 'Số điện thoại';
$lang['email'] = 'Email';
$lang['amount'] = 'Tổng tiền';
$lang['address'] = 'Địa chỉ';
$lang['view'] = 'Xem đơn hàng';
$lang['view_of'] = 'Đơn hàng của';
$lang['total'] = 'Tổng tiền';
$lang['created_date_order'] = 'Ngày đặt hàng';
$lang['order_detail'] = 'Chi tiết đơn hàng';
$lang['product_name'] = 'Tên sản phẩm';
$lang['price'] = 'Đơn giá';
$lang['quantity'] = 'Số lượng';
$lang['sub_total'] = 'Thành tiền';
