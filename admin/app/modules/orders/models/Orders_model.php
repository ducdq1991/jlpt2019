<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends Bigshare_Model {

	public function __construct()
	{
		parent::__construct();
	}



    public function updateOrder($id, $args = [])
    {
        $where = [];
        $where['orders.id'] = (int) $id;
        $this->_table = 'orders';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function deleteOrder($where = [], $multiple = false)
    {
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        $this->_table = 'orders';
        if ($this->deleteRecord()) {
            $this->_wheres = $where;
            $this->_multiple = $multiple;
            $this->_table = 'orderinfos';
            return $this->deleteRecord();
        }
    }

	public function getOrder($where = [])
	{
		$this->_selectItem = '*';
		$this->_table = 'orders';
        $this->_wheres = $where;
        return $this->getOneRecord();
	}

	public function getOrders($where = [], $limit = 0, $offset = 0, $orderBy = [], $likes = [])
	{
		$this->_selectItem = 'orders.*';
        $this->_from = 'orders';
        $this->_limit = $limit;
        $this->_offset = $offset;
        $this->_order = $orderBy;
        $this->_wheres = $where;

        if(!empty($likes)){
            $this->_likes['field'] = $likes['field'];
            
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }        
        $result = $this->getListRecords();
        $items = [];
        foreach ($result as $item) {
            $district = $this->getDistrict(['districts.id' => $item->district_id]);
            $province = $this->getProvince(['provinces.id' => $item->province_id]);
            $item->district_name = $district->name;
            $item->province_name = $province->name;
            $item->order_details = $this->getOrderDetail(['order_items.order_id' => $item->id]);
            $items[] = $item;
        }

        return $items;
	}

    public function totalOrder( $where = [] )
    {
        $this->_table = 'orders';
        $this->_wheres = $where;
        return $this->totalRecord();
    }  

    public function getOrderDetail($where = [])
    {       
        
        $this->resetQuery();
        $this->_selectItem = "order_items.*";
        $this->_from = 'order_items';
        $joins = [];   
        $this->_limit = 0;
        $this->_wheres = $where;
        $data = $this->getListRecords();
        
        return $data;
    }

    public function getProvince($where = [])
    {
        $this->resetQuery();
        $this->_selectItem = 'provinces.*';
        $this->_table = 'provinces';
        $this->_wheres = $where;
        $data = $this->getOneRecord(); 
        
        return $data;
    }

    public function getDistrict($where = [])
    {
        $this->resetQuery();
        $this->_selectItem = 'districts.*';
        $this->_table = 'districts';
        $this->_wheres = $where;
        $data = $this->getOneRecord(); 
        
        return $data;
    }

}