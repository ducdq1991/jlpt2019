<?php defined('BASEPATH') or exit('No direct script access allowed');

class Faqs_model extends Bigshare_Model
{
    public function __construct()
    {
        parent::__construct();        
    }
    
    /**
     * @name addFaqCategory
     * @desc insert new faq category
     * @param [array] $args
     * */
    public function addCategory( $args = array() )
    {
        $this->_table = 'faq_categories';
        $this->_data = $args;
		if($id = $this->addRecord() ){
			return $id;
		}
		return false;
    }

    public function addFaq( $args = array() )
    {
        $this->_table = 'faqs';
        $this->_data = $args;
        if($id = $this->addRecord() ){
            return $id;
        }
        return false;
    }    
    
    /**
     * @name getFaqCategories
     * @desc get all row from faq_categories table
     * @param [array] $where
     * */
    public function getCategories( $where = array(), $limit = 0, $offset = 0, $likes = array() )
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'faq_categories';
        $this->_wheres = $where;
        return $this->getListRecords();
    }
    /**
     * @name getMenuItems
     * @desc get all row from menu_items table
     * @param [array] $where
     * */
    public function getFaqs( $where = [], $limit = 0, $offset = 0, $likes = [])
    {
        if(isset($likes['field'])){
            $this->_likes['field'] = $likes['field'];
            $this->_likes['value'] = $this->db->escape_like_str( $likes['value'] );
        }
        $this->_offset = $offset;
        $this->_limit = $limit;
        $this->_from = 'faqs';
        $this->_wheres = $where;
        $items = $this->getListRecords();

        $this->resetQuery();    
        foreach($items as $key => $item){
            //$items = (array) $item;
            $where = array('faqs.id' => $item->id);        
            if($item->faq_category_id > 0){
                $parent = $this->getCategoryName($item->faq_category_id); 
                if($parent){
                    $items[$key]->cate_name = $parent['cate_name'];
                }                        
            } else {
                $items[$key]->cate_name = '';
            }  
            if ($item->user_id) {
                $items[$key]->username = $this->getUser($item->user_id);
            } else {
                $items[$key]->username = '';
            }                
        }
        return $items;
    }   

    public function getUser($user_id)
    {
        $query = $this->db->select('username')->where(['id' => $user_id])->get('users')->result();
        if ($query) {
            return $query[0]->username;
        }
        return null;
    }

    public function getCategoryName( $id )
    {
        $this->resetQuery();
        $this->_table = 'faq_categories';
        $this->_selectItem = 'faq_categories.name as cate_name';
        $where = array('faq_categories.id' => $id);
        $this->_wheres = $where;
        $result = (array) $this->getOneRecord();        
        return $result;
    }     
    /**
	 * [numMenus]
	 * @param  array  $where [description]
	 * @return [int]
	 */
	
	public function totalCategory($where = array())
	{
        $this->_table = 'faq_categories';
		$this->_wheres = $where;
		return $this->totalRecord();
	}
    public function totalFaq($where = array())
    {
        $this->_table = 'faqs';
        $this->_wheres = $where;
        return $this->totalRecord();
    }    
    /**
     * @name deleteMenu
     * @desc delete a row from menus table
     * @param [array] $where
     * */
    public function deleteCategory( $where = array(), $multiple = false )
    {
        $this->_table = 'faq_categories';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }

    public function deleteFaq( $where = array(), $multiple = false )
    {
        $this->_table = 'faqs';
        $this->_wheres = $where;
        $this->_multiple = $multiple;
        return $this->deleteRecord();
    }    
    
    
    public function getCategory( $where = array() )
    {
        $this->_table = 'faq_categories';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }

    public function getFaq( $where = array() )
    {
        $this->_table = 'faqs';
        $this->_wheres = $where;
        return $this->getOneRecord();
    }    
    
    public function updateCategory( $where = array(), $args = array() )
    {
        $this->_table = 'faq_categories';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }
    public function updateFaq( $where = array(), $args = array() )
    {
        $this->_table = 'faqs';
        $this->_wheres = $where;
        $this->_data = $args;
        return $this->updateRecord();
    }

    public function getCategoryOption($parentId = 0, $currentId = 0, $str = '') {
      $where = [];
      $html = '';
      $where['faq_categories.parent_id'] = $parentId;
      $this->db->select('faq_categories.id, faq_categories.name, faq_categories.parent_id');
      $this->db->from('faq_categories');      
      $this->db->where($where);
      $result = $this->db->get()->result();   
      $this->resetQuery();
      $selected = '';
      foreach ($result as $item) {          
            $name = $str.$item->name;

            if($currentId == $item->id) { 

                $selected = 'selected = "selected"';   
                $html .= '<option ' . $selected . ' value="' . $item->id . '">' . $name . '</option>';
            } else {
                $html .= '<option value="' . $item->id . '">' . $name . '</option>';
            }
            
            $html .= $this->getCategoryOption($item->id, $currentId, $str.'|---');
      }  
      return $html;
    }

    public function changeStatus( $ids, $args = array() )
    {
        $this->_table = 'faqs';
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    } 

    public function changeStatusCategory( $ids, $args = array() )
    {
        $this->_table = 'faq_categories';
        $this->_data = $args;
        if( is_array($ids) ) {
            $this->_batchConditionField = 'id';
            $this->_batchConditionValue = $ids;
            return $this->updateBatchRecord();
        } else {
            $where = array( 'id' => (int) $ids );
            $this->_wheres = $where;
            return $this->updateRecord();
        }
    }            
}