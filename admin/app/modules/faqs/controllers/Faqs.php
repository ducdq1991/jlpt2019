<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* @package Faqs
* @author TrungDoan <trung.doan@bigshare.vn>
* @copyright Bigshare
* 
*/

class Faqs extends Bigshare_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('faqs_model');
	}

    public function index($page = 0)
    {
        $data = $where = $likes = [];
        //Pagination Config
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));        
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        
        // Search condition
        $keyword = $this->input->get('keyword');
        $categoryId = $this->input->get('faq_category_id');
        if($keyword != '') {
            $likes['field'] = 'faqs.title';
            $likes['value'] = $keyword;
        }
        $data['keyword'] = $keyword;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['faqs.status'] = ($status ==='active') ? 1 : 0;
        }

        if($categoryId != ''){
          $where['faqs.faq_category_id'] = $categoryId;
        }

        $categoryOption = $this->faqs_model->getCategoryOption(0, $categoryId);
        $data['categoryOption'] = $categoryOption;
        $numRecords = $this->faqs_model->totalFaq( $where );      
        $data['paging'] = paging(base_url() .'faqs/', $page, $numRecords, $limit); 

        $items = $this->faqs_model->getFaqs( $where, $limit, $offset, $likes );
        
        $showFrom = $offset + count($items);        
        $data['faqs'] = $items;
        $data['numRecord'] = $limit;
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $numRecords;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
                
        $this->load->view('layouts/header', $head);
        $this->load->view('faqs/index', $data);
        $this->load->view('layouts/footer');
    }

    public function categories($page = 0)
    {
        $data = $where = $likes = [];
        //Pagination Config
        $this->load->helper('paging');  
        $limit = $this->config->item('per_page');
        $numRecord = (int) $this->input->get('numRecord');
        $limit = ($numRecord > 0) ? $numRecord : $limit;
        $page = intval(str_replace('page-', '', $page));        
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        
        // Search condition
        $keyword = $this->input->get('keyword');
        if($keyword != '') {
            $likes['field'] = 'faq_categories.name';
            $likes['value'] = $keyword;
        }
        $data['keyword'] = $keyword;
        $status = $this->input->get('status');
        $data['status'] = $status;
        if($status != ''){
          $where['faq_categories.status'] = ($status ==='active') ? 1 : 0;
        }

        $numRecords = $this->faqs_model->totalCategory( $where );      
        $data['paging'] = paging(base_url() .'faqs/categories', $page, $numRecords, $limit); 
        $items = $this->faqs_model->getCategories( $where, $limit, $offset, $likes );
        
        $showFrom = $offset + count($items);
        $perPage = ceil($numRecords/$limit);
        $data['faqs'] = $items;
        $data['numRecord'] = $limit;
        $data['showFrom'] = $offset + 1;        
        $data['showTo'] = $showFrom;
        $data['totalCount'] = $numRecords;
        $data['itemPerPages'] = $this->config->item('rowPerPages');

        $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
        $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
        $this->breadcrumbs->add('Danh mục', base_url().'faqs/categories');
        $head['breadcrumb'] = $this->breadcrumbs->display(); 
                
        $this->load->view('layouts/header', $head);
        $this->load->view('faqs/category', $data);
        $this->load->view('layouts/footer');
    }   

    public function addCategory()
    {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $this->form_validation->set_rules($config);
        $data = [];
        $categoryOption = $this->faqs_model->getCategoryOption(0);
        $data['categoryOption'] = $categoryOption;
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
            $this->breadcrumbs->add('Danh mục', base_url().'faqs/categories');
            $this->breadcrumbs->add('Tạo danh mục', base_url().'faqs/addcategory');
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('faqs/addcategory', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args = $info;
            if( $id = $this->faqs_model->addCategory( $args ) ) {
                $update = array();
                $update['hash_id'] = md5($id);
                $this->faqs_model->updateCategory(array('id' => $id), $update);
                $message = sprintf(lang('message_create_success'), ' danh mục ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' danh mục ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'faqs/categories');
        }        
    }

    public function editCategory($id)
    {
        if (!$id) {
            return false;
        }
        $data = [];
        $item = $this->faqs_model->getCategory( array('hash_id' => $id) );
        $data['item'] = $item;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'name',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )

        );
        $categoryOption = $this->faqs_model->getCategoryOption(0, $item->parent_id);
        $data['categoryOption'] = $categoryOption;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
            $this->breadcrumbs->add('Danh mục', base_url().'faqs/categories');
            $this->breadcrumbs->add('Sửa danh mục', base_url().'faqs/editcategory');
            $head['breadcrumb'] = $this->breadcrumbs->display();

            $this->load->view('layouts/header', $head);
            $this->load->view('faqs/editcategory', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $info = $this->nonXssData($info);
            $args = $info;
            if( $this->faqs_model->updateCategory( array('hash_id' => $id), $args ) ) {
                $message = sprintf(lang('message_update_success'), $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_update_error'), $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'faqs/categories');
        }
    }

    public function deleteFaqAjax()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $this->session->set_flashdata('warning', 'Không có lựa chọn nào để xóa');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->faqs_model->deleteFaq( $where, true );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa lựa chọn thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa lựa chọn thất bại!');
            }
            echo 1;
            die;
        } else {
            $query = $this->faqs_model->deleteFaq( array( 'id' => (int) $ids ) );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại!');
            }
            echo 1;
            die;
        }
    }

    public function deleteCategoryAjax()
    {
        $ids = $this->input->post('ids');
        if( empty($ids) ) {
            $this->session->set_flashdata('warning', 'Không có lựa chọn nào để xóa');
            return false;
        }
        $where = array(
            'field' => 'id',
            'value' => $ids
        );
        if( is_array($ids) ){
            $query = $this->faqs_model->deleteCategory( $where, true );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa lựa chọn thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa lựa chọn thất bại!');
            }
            echo 1;
            die;
        } else {
            $query = $this->faqs_model->deleteCategory( array( 'id' => (int) $ids ) );
            if( $query ) {
                $this->session->set_flashdata('success', 'Xóa thành công!');
            } else {
                $this->session->set_flashdata('error', 'Xóa thất bại!');
            }
            echo 1;
            die;
        }
    }


    public function deleteCategory($id)
    {
        if(!$id) {
            redirect(base_url() . 'faqs/categories');
        }
        $data = array();
        $item = $this->faqs_model->getCategory( array('hash_id' => $id) );   
        if($item){
            if($this->faqs_model->deleteCategory(array('id'=>$item->id))){
                $this->faqs_model->deleteFaq(array('faq_category_id' => $item->id));
                redirect(base_url() . 'faqs/categories');
            }
        }   

        redirect(base_url() . 'faqs/categories');    
    }



    public function addFaq(){

        $data = [];
        $this->load->library('form_validation');        
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Tiêu đề là bắt buộc',
                ),
            array(
                'field' => 'faq_category_id',
                'label' => lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            ),            
            array(
                'field' => 'slug',
                'label' => lang('alias'),
                'rules' => 'trim|required|is_unique[faqs.slug]',
                'errors' => array(
                    'required' => lang('mes_required'),
                    'is_unique' => lang('mes_is_unique'),
                ),
            ),                
            )
        );        

        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);

        $categoryOption = $this->faqs_model->getCategoryOption(0);
        
        $data['categoryOption'] = $categoryOption;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
            $this->breadcrumbs->add('Tạo hỏi đáp', base_url().'faqs/addfaqs');
            $head['breadcrumb'] = $this->breadcrumbs->display();            
            $this->load->view('layouts/header', $head);
            $this->load->view('faqs/add_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $answer = $info['answer'];
            $info = $this->nonXssData($info);
            $info['status'] = 1;
            $args = $info;
            $args['answer'] = $answer;
            if( $id = $this->faqs_model->addFaq( $args ) ) {
                $update = array();
                $update['hash_id'] = md5($id);
                $this->faqs_model->updateFaq(array('id' => $id), $update);
                $message = sprintf(lang('message_create_success'), ' hỏi đáp ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' hỏi đáp ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'faqs');
        }  
    }

    public function editFaq($id = ''){
        $data = [];
        $this->load->library('form_validation');
        $this->load->helper('tinymce');
        $data['tinymce'] = tinymce(400);        
        $config = array(
            array(
                'field' => 'title',
                'label' =>  lang('title'),
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => lang('mes_required'),
                ),
            )
        );
        $item = $this->faqs_model->getFaq(array('hash_id' => $id));   
        $categoryOption = $this->faqs_model->getCategoryOption(0, $item->faq_category_id);
        
        $data['categoryOption'] = $categoryOption;
        $data['item'] = $item;
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == FALSE) {
            $this->breadcrumbs->add('<i class="fa fa-home"></i> '.lang('dashboard'), base_url());
            $this->breadcrumbs->add('Hỏi đáp', base_url().'faqs');
            $this->breadcrumbs->add('Sửa hỏi đáp', base_url().'faqs/editfaq');
            $head['breadcrumb'] = $this->breadcrumbs->display();             
            $this->load->view('layouts/header', $head);
            $this->load->view('faqs/edit_item', $data);
            $this->load->view('layouts/footer');
        } else {
            $info = $this->input->post();
            $answer = $info['answer'];
            //$info = $this->nonXssData($info);
            $args = $info;
            if($this->faqs_model->updateFaq(array('hash_id'=> $id), $args ) ) {
                $message = sprintf(lang('message_updated_success'), ' hỏi đáp ' . $info['name']);
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_create_error'), ' hỏi đáp ' . $info['name']);
                $this->session->set_flashdata('error', $message);
            }
            redirect(base_url() . 'faqs');
        }  
    }  

    public function deleteFaq($id)
    {
        if(!$id) {
            redirect(base_url() . 'faqs');
        }
        $data = array();
        $menu = $this->faqs_model->getFaq( array('hash_id' => $id) );   
        if($menu){
            if($this->faqs_model->deleteFaq(array('hash_id' => $id))){
                redirect(base_url() . 'faqs');
            }
        }   
        redirect(base_url() . 'faqs');   
    }

    /**
     * [changeStatus of Course]
     * @return [int]
     */
    public function changeStatus()
    {
        $args = array();
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if( !isset($ids) || empty($ids) ){
            return false;
        }
        if( isset($type) && !empty($type) && $type =='multi' ){             
            if( !empty($ids) ){
                $args['status'] = $status;  
                if($this->faqs_model->changeStatus( $ids, $args))
                {
                    $message = sprintf(lang('message_change_success'), lang('documents'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_change_error'), lang('documents'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } else {    
            $args['status'] = ($status == 1) ? 0 : 1;       
            if( $this->faqs_model->changeStatus( $ids, $args ) )
            {
                $message = sprintf(lang('message_change_success'), lang('the_document'));
                $this->session->set_flashdata('success', $message);
            } else {
                $message = sprintf(lang('message_change_error'), lang('the_document'));
                $this->session->set_flashdata('error', $message);
            }
            echo 1;
            die;    
        }           
    }

    public function changeStatusCategory()
    {
       $args = [];
        $ids = $this->input->post('ids');
        $type = $this->input->post('type');
        $status = (int) $this->input->post('status');
        if (!isset($ids) || empty($ids)) {
            return false;
        }
        if (isset($type) && !empty($type) && $type =='multi' ) {             
            if( !empty($ids) ){
                $args['status'] = $status; 
                if($this->faqs_model->changeStatusCategory( $ids, $args))
                {
                    $message = sprintf(lang('message_update_success'), lang('status'));
                    $this->session->set_flashdata('success', $message);
                } else {
                    $message = sprintf(lang('message_update_error'), lang('status'));
                    $this->session->set_flashdata('error', $message);
                }
                echo 1;
                die;
            }
        } 
    }
                  
}