<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> Hỏi đáp</h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-group">
                <form method="GET" accept-charset="utf-8">
                    <div class="form-group">                    
                        <div class="col-md-12 form-module-action text-right">
                            <div class="pull-left btn-function" style="display:none;">
                                <a class="delMultiple btn btn-danger btn-sm" url="<?php echo base_url().'faqs/deletefaqajax'; ?>">
                                <i class="fa fa-remove"></i> <?php echo lang('delete');?></a>
                                <a class="activeStatus btn btn-info btn-sm" url="<?php echo base_url().'faqs/changestatus'; ?>">
                                <i class="fa fa-check"></i> <?php echo lang('active');?></a>
                                <a class="deactivateStatus btn btn-default btn-sm" url="<?php echo base_url().'faqs/changestatus'; ?>"><i class="fa fa-check"></i> <?php echo lang('deactive');?></a>                              
                            </div>

                            <a href="<?php echo base_url();?>faqs/categories" class="btn btn-success btn-sm">
                            <i class="fa fa-tags"></i> Danh mục</a>   
                            <a href="<?php echo base_url();?>faqs/addfaq" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-1">
                            <select name="numRecord" class="form-control input-sm sys-filler" onchange="this.form.submit()">                                
                                <?php foreach($itemPerPages as $key=>$value):?>
                                <option value="<?php echo $value;?>" <?php if($numRecord == $value):?> selected="selected" <?php endif;?>><?php echo $key;?></option>
                                <?php endforeach;?>
                            </select>                             
                        </div>                                            
                        <div class="col-md-3 text-right">
                            <input type="text" class="form-control input-sm sys-filler" placeholder="<?php echo lang('search_by_name');?>" name="keyword" value="<?php echo $keyword;?>">                                
                        </div>
                        <div class="col-md-3 text-right">
                            <select name="faq_category_id" class="form-control input-sm sys-filler">
                                <option value="">Chọn danh mục</option>
                                <?php echo $categoryOption;?>
                            </select>                               
                        </div>                         
                        <div class="col-md-1 text-right">                            
                            <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-search-plus"></i> Tìm kiếm</button>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-2 padding-middle text-right">
                            <?php if ($totalCount > 0):?>
                            Hiển thị <strong><?php echo number_format($showFrom);?></strong> 
                            - <strong><?php echo number_format($showTo);?></strong> 
                            trong tổng <strong><?php echo number_format($totalCount);?></strong>
                            <?php endif;?>
                        </div>                                              
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1" class="col-checkbox">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center col-Id">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center"><?php echo lang('title');?></th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center">Học viên</th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center">Danh mục</th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center col-status"><?php echo lang('status');?></th>
                                <th tabindex="0" rowspan="1" colspan="1" class="text-center col-actions"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php
                                if ($totalCount > 0): 
                                    foreach ($faqs as $row) :
                            ?>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1 text-center"><?php echo $row->id;?></td>
                                    <td class="sorting_1"><strong><?php echo $row->title;?></strong></td>
                                    <td><?php echo $row->username;?></td>
                                    <td><?php if(isset($row->cate_name)) echo $row->cate_name;?></td>
                                    <td class="text-center">
                                        <?php if($row->status == 1):?>
                                            <i onclick="changeState('<?php echo base_url().'faqs/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="Câu hỏi đã kích hoạt" class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i onclick="changeState('<?php echo base_url().'faqs/changestatus'; ?>', <?php echo $row->id;?>, <?php echo $row->status;?>)" title="Câu hỏi đã bị vô hiệu" class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="center text-center">                                
                                        <a href="<?php echo base_url();?>faqs/editfaq/<?php echo $row->hash_id;?>" title="" class="btn btn-xs btn-default">
                                                <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a href="<?php echo base_url();?>faqs/deletefaq/<?php echo $row->hash_id;?>" class="btn btn-xs btn-danger">
                                                <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php 
                                    endforeach;
                                else:
                            ?>
                                <tr>
                                    <td colspan="6" class="text-center">No Record!</td>
                                </tr>
                            <?php endif;?>                            
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>

                    <div class="form-group">                        
                        <div class="col-md-12">
                            <div class="paging text-center">
                                <?php echo $paging;?>
                            </div>                            
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
