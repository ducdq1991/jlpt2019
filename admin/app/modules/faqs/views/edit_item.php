<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Sửa hỏi đáp</h5>
                <div class="ibox-tools">
                    <a href="<?php base_url('menu/add');?>" class="collapse-link">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tiêu đề</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="autoSlug auto-category-slug form-control" value="<?php echo $item->title;?>">
                                <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slug</label>
                            <div class="col-sm-10"><input name="slug" type="text" value="<?php echo $item->slug;?>" class="slug slug-category form-control"></div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Danh mục</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="faq_category_id" data-current="<?php echo $item->faq_category_id;?>">
                                    <option value="0">Danh mục</option>
                                    <?php echo $categoryOption;?>
                                </select>
                            </div>
                        </div>                          
                        <div class="form-group"><label class="col-sm-2 control-label">Câu hỏi</label>
                            <div class="col-sm-10">
                                <textarea class="form-control editor" rows="5" name="question" id="question"><?php echo set_value('question', $item->question); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Câu trả lời</label>
                            <div class="col-sm-10">
                                <textarea class="form-control editor" rows="5" name="answer" id="answer"><?php echo set_value('answer', $item->answer); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="link_video">Link Video (Youtube)</label>
                            <div class="col-sm-10">
                                <input type="text" name="link_video" class="form-control" id="link_video" value="<?php echo set_value('link_video', $item->link_video);?>">
                            </div>
                        </div>          
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Reset</button>
                            <button class="btn btn-primary" type="submit">Cập nhật</button>
                            <a href="<?php echo base_url();?>faqs" class="btn btn-success">Hỏi đáp</a>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>          
<?php echo $tinymce; ?>