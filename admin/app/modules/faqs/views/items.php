<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class ="fa fa-tags"></i> Menu [<?php echo $menu->name;?>]</h5>
                <div class="ibox-tools">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <?php if( $this->session->flashdata('success') ) : ?>
                    <div class="alert alert-sm alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('success'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-sm alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>
                <?php if( $this->session->flashdata('warning') ) : ?>
                    <div class="alert alert-sm alert-warning alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><?php echo $this->session->flashdata('warning'); ?></div>
                <?php endif; ?>
                <div class="dataTables_wrapper form-inline">
                <form method="GET" accept-charset="utf-8">
                    <div class="pull-right form-module-action">
   
                        <a href="<?php echo base_url();?>menu/additem/<?php echo $menu->hash_id;?>" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <?php echo lang('add_new');?></a>
                    </div>
                    <table class="table table-bordered table-striped dataTable" role="grid">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" rowspan="1" colspan="1">
                                    <input id="checkAll" type="checkbox"/>
                                </th>
                                <th tabindex="0" rowspan="1" colspan="1">ID</th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('title');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('parent');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('status');?></th>
                                <th tabindex="0" rowspan="1" colspan="1"><?php echo lang('actions');?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php foreach ($menus as $row) :?>
                                <tr class="gradeA odd" role="row">
                                    <td class="sorting_1">
                                        <input class="checkItem" type="checkbox" value="<?php echo $row->id;?>" name="checkItems[]" />
                                    </td>                                    
                                    <td class="sorting_1"><?php echo $row->id;?></td>
                                    <td><?php echo $row->name;?></td>
                                    <td>
                                        <?php
                                        if(isset($row->parent_name)){
                                            echo $row->parent_name;
                                        } else {
                                            echo 'Root';
                                        }                                             
                                        ?>
                                    </td>
                                    <td class="center">
                                        <?php if($row->status == 1):?>
                                            <i class="fa fa-check-circle active-status"></i>
                                        <?php else : ?>
                                            <i class="fa fa-check-circle deault-status"></i>
                                        <?php endif;?>
                                    </td>
                                    <td class="center text-center">
                                        <a href="<?php echo base_url();?>menu/edititem/<?php echo $row->hash_id;?>/<?php echo $menu->hash_id;?>" title="" class="btn btn-xs btn-default">
                                                <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a href="<?php echo base_url();?>menu/deleteitem/<?php echo $row->hash_id;?>/<?php echo $menu->hash_id;?>" class="btn btn-xs btn-danger">
                                                <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;?>                            
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    <div class="paging">
                        <?php //echo $paging;?>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
