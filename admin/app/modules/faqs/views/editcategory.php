<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cập nhật danh mục [<?php echo $item->name;?>]</h5>
                <div class="ibox-tools">
                    <a href="<?php base_url('faqs/addcategory');?>" class="collapse-link">
                        <i class="fa fa-plus"></i>
                    </a>
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="overflow: hidden">
                <?php if( $this->session->flashdata('error') ) : ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php endif; ?>            
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Tên danh mục</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="autoSlug auto-category-slug form-control" value="<?php echo $item->name;?>">
                                <?php echo form_error('name', '<div class="alert alert-danger">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slug</label>
                            <div class="col-sm-10"><input name="slug" type="text" class="slug slug-category form-control" value="<?php echo $item->slug;?>"></div>
                        </div>                        
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Danh mục cha</label>
                            <div class="col-sm-10">
                                <select name="parent_id" class="form-control" data-id="<?php echo $item->parent_id;?>">
                                    <option value="">Chọn danh mục</option>
                                    <?php echo $categoryOption;?>
                                </select>
                            </div>
                        </div>                                                                                            
                    </div> 
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">Xóa</button>
                            <button class="btn btn-primary" type="submit">Cập nhật</button>
                        </div>
                    </div>                      
                </form>
            </div><!--inbox content-->
        </div>
    </div>  
</div>          