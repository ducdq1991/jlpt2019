<?php
    $url = $_SERVER['REQUEST_URI'];     
    $session = $this->session->userdata;
    $accessKey = $session['user']->access_key;    
    $controller = (string) $this->router->fetch_class();
    $method = $this->router->fetch_method();
    $permissions = unserialize($session['user']->permission);
    $args = [];
    foreach ($permissions as $key => $item){
        $args[] = '.m_' . $key . '';
    }
    $allowedClass = implode(', ', $args);      
 ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VNA | Dashboard</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">    
    <!-- custom css -->
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/select2.min.css" rel="stylesheet">
    <script type="text/javascript">
        var adminUrl = '<?php echo base_url();?>';
        var message_confirm_delete = '<?php echo lang('message_confirm_delete'); ?>';
    </script> 
    <style type="text/css" media="screen">
        .hideMenu{
            display: none;
        }
        <?php echo $allowedClass;?> {
            display: block;
        }
    </style>
    <script src="<?php echo base_url();?>assets/js/jquery-2.1.1.js"></script>           
</head>
<body class="top-navigation">
    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="<?php echo base_url();?>" class="navbar-brand" style="padding-top: 17px;">
                    VNA
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li class="<?php if ($controller =='index' || $controller =='dashboard'):?> active <?php endif;?>">
                        <a aria-expanded="false" role="button" href="<?php echo base_url();?>dashboard">
                        <i class="fa fa-dashboard"></i></a>
                    </li>
                    <li class="dropdown <?php if ($controller =='course'
                    || $controller =='lessons'
                    || $controller =='practicle'
                    || $controller =='mark'):?> active <?php endif;?>">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-graduation-cap"></i> Đào tạo</a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="hideMenu m_course"><a href="<?php echo base_url('coursecategory');?>"><i class="fa fa-cubes"></i> Danh mục khóa học</a></li>                            
                            <li class="hideMenu m_course"><a href="<?php echo base_url('course');?>"><i class="fa fa-cubes"></i> <?php echo lang('courses');?></a></li>
                            <li class="hideMenu m_lesson"><a href="<?php echo base_url();?>lesson"><i class="fa fa-file-video-o"></i>  <?php echo lang('lessons');?></a></li>
                            <li class="hideMenu m_exam"><a href="<?php echo base_url();?>exam"><i class="fa fa-folder-open-o"></i> <?php echo lang('the_tests');?></a></li>
                            <li class="hideMenu m_promotion"><a href="<?php echo base_url();?>promotion"><i class="fa fa-tags"></i> Khuyến mại Khóa học</a></li>                            
                            <li class=""><a href="<?php echo base_url();?>payments/courses"><i class="fa fa-tags"></i> Lịch sử khóa học được mua</a></li>
                            <li class=""><a href="<?php echo base_url();?>mark"><i class="fa fa-tags"></i> Bài thi</a></li>
                            </ul>
                    </li>
                    <li class="hideMenu m_page <?php if ($controller =='question'):?> active <?php endif;?>">
                        <a href="<?php echo base_url();?>question">
                        <i class="fa fa-book"></i> Ngân hàng câu hỏi </a>
                    </li>                    
                    <li class="hideMenu m_codes <?php if ($controller =='codes'):?> active <?php endif;?>">
                        <a href="<?php echo base_url();?>codes">
                        <i class="fa fa-book"></i> Quản lý mã </a>
                    </li> 
                    <li class="m_examstorage <?php if ($controller =='examstorage'):?> active <?php endif;?>">
                        <a href="<?php echo base_url();?>examstorage">
                        <i class="fa fa-book"></i> Tài liệu </a>
                    </li>
                    <li class="m_books <?php if ($controller =='books'):?> active <?php endif;?>">
                        <a href="<?php echo base_url();?>books">
                        <i class="fa fa-book"></i> Sách </a>
                    </li>
                    <li class="m_orders <?php if ($controller =='orders'):?> active <?php endif;?>">
                        <a href="<?php echo base_url();?>orders">
                        <i class="fa fa-book"></i> Đơn hàng </a>
                    </li>
                    <li class="dropdown <?php if ($controller =='salemans'):?> active <?php endif;?>">
                        <a aria-expanded="false" role="button" href="<?php echo base_url('salemans'); ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cloud"></i> CTV</a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="hideMenu m_course"><a href="<?php echo base_url('salemans/history'); ?>"><i class="fa fa-cubes"></i> Lịch sử cộng tiền</a></li>                                                       
                            <li class=""><a href="<?php echo base_url('salemans'); ?>"><i class="fa fa-tags"></i> Danh sách CTV</a></li>
                            </ul>
                    </li>                                                                           
                    <li class="dropdown <?php if ($controller =='payments' || $controller =='banner' 
                    || $controller =='notification'
                    || $controller =='testimonies'
                    || $controller =='faqs'
                    || $controller =='article'
                    || $controller =='category'
                    || $controller =='menu'
                    || $controller =='mailer'
                    || $controller =='media'):?> active <?php endif;?>">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cloud"></i> <?php echo lang('extensions');?></a>
                        <ul role="menu" class="dropdown-menu">
                            <li class="m_article"><a href="<?php echo base_url();?>page"><i class="fa fa-tags"></i> Trang nội dung</a></li>
                            <li class="m_article"><a href="<?php echo base_url();?>article"><i class="fa fa-tags"></i> Tin tức</a></li>
                            <li class="hideMenu m_payments"><a href="<?php echo base_url();?>payments">
                            <i class="fa fa-money"></i> <?php echo lang('payment_history');?></a></li>
                            <li class="hideMenu m_banner"><a href="<?php echo base_url();?>banner"><i class="fa fa-buysellads"></i> <?php echo lang('ads');?></a></li>
                            <li class="hideMenu m_notification"><a href="<?php echo base_url();?>notification"><i class="fa fa-bell"></i> Hoạt động</a></li>
							<li class="hideMenu m_testimonies"><a href="<?php echo base_url();?>testimonies"><i class="fa fa-quote-left"></i> <?php echo lang('testimonies');?></a></li>                    
                            <li class="hideMenu m_menu"><a href="<?php echo base_url();?>menu"><i class="fa fa-navicon"></i> Menu</a></li>
                            <li class="hideMenu m_media"><a href="<?php echo base_url();?>media"><i class="fa fa-file-movie-o"></i> <?php echo lang('media');?></a></li>                            
                        </ul>
                    </li>                 
                    <li class="hideMenu m_setting dropdown <?php if ($controller =='setting' 
                    || $controller =='user' 
                    || $controller =='group'):?> active <?php endif;?>">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Hệ thống</a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>setting/general"><i class="fa fa-cog"></i> <?php echo lang('setting_general');?></a></li>
                            <li><a href="<?php echo base_url('user'); ?>"><i class="fa fa-male"></i> <?php echo lang('users');?></a></li>                            
                            <li><a href="<?php echo base_url('salemans'); ?>"><i class="fa fa-male"></i> Cộng tác viên</a></li>
                            <li><a href="<?php echo base_url('group'); ?>"><i class="fa fa-users"></i> <?php echo lang('user_groups');?></a></li>              
                            <li><a href="<?php echo base_url(); ?>setting/videointro"><i class="fa fa-tags"></i> Video Intro</a></li>
                            <li><a href="<?php echo base_url(); ?>monitoring"><i class="fa fa-tags"></i> Monitoring</a></li>
                        </ul>
                    </li>                  
                </ul>               
                <ul class="nav navbar-top-links navbar-right">
                    <li> 
                        <a target="_blank" href="http://<?php echo $_SERVER['HTTP_HOST'];?>"><i class="fa fa-external-link"></i></a>
                    </li>                
                    <li> 
                        <a href="<?php echo base_url();?>user/edit/<?php echo $session['user']->id; ?>"><?php echo lang('hello');?> <?php echo $session['user']->username;?></a>
                    </li>                    
                    <li>
                        <a href="<?php echo base_url();?>auth/logout">
                            <i class="fa fa-sign-out"></i> <?php echo lang('logout');?>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        </div>
        <div class="row wrapper">
            <div class="col-lg-10">
                <?php if( !empty($breadcrumb) ){ echo $breadcrumb; } ?>
            </div>
            <div class="col-lg-2">

            </div>
        </div> 
        <div class="wrapper wrapper-content animated fadeInRight">       
