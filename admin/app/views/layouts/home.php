<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script> 
<div class="wrapper wrapper-content dashboard">
   <div class="row">
       <div class="col-lg-7">
            <div id="container" style="width:100%; height: 350px; margin: 0 auto; border:1px solid #e7eaec;"></div>
        <div style="clear: both; margin-top: 15px; width: 100%">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lịch sử thanh toán</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="feed-activity-list">
                        <?php if( !empty($paymentHistory) ) : ?>
                            <?php foreach( $paymentHistory as $item ) : ?>
                                <div class="feed-element">
                                    <div>
                                        <?php
                                            $date_in = new DateTime($item->created);
                                            $date = new DateTime(date('Y-m-d H:i:s'));
                                            $interval = date_diff($date_in, $date);
                                            $year = $interval->format('%y');
                                            $month = $interval->format('%m');
                                            $days = $interval->days;
                                            $hours = $interval->format('%h');
                                            $minute = $interval->format('%i');
                                            $second = $interval->format('%s');
                                        ?>
                                        <small class="pull-right text-navy">
                                            <?php 
                                                if((int)$year > 0): 
                                                    echo $year.' năm trước';
                                                elseif((int)$month > 0 && (int)$year == 0): 
                                                    echo $month.' tháng trước';
                                                elseif((int)$days > 0 && (int)$month == 0 && (int)$year == 0 ):
                                                    echo $days.' ngày trước';
                                                elseif((int)$hours > 0 && (int)$days == 0 && (int)$month == 0 && (int)$year == 0):
                                                    echo $hours.' giờ trước';
                                                elseif((int)$minute > 0 && (int)$hours == 0 && (int)$days == 0 && (int)$month == 0 && (int)$year == 0):
                                                    echo $minute.' phút trước';
                                                else:
                                                    echo $second.' giây trước';
                                                endif;
                                            ?>
                                        </small>
                                        <!--<strong>Monica Smith</strong>-->
                                        <div><?php echo $item->content; ?></div>
                                        <small class="text-muted"><?php echo date('H:i a - d.m.Y', strtotime( $item->created ));?></small>
                                    </div>
                                </div>
                            <?php endforeach; ?>    
                        <?php else: ?>    
                            <div class="feed-element">
                                <strong>Chưa có thực hiện thanh toán nào</strong>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>                   
        </div>
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Hoạt động gần đây</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content inspinia-timeline">
                    <?php if( !empty($activities) ) : ?>
                        <?php foreach( $activities as $item ) : ?>
                            <div class="timeline-item">
                                <div class="row">
                                    <div class="col-xs-3 date">                                        
                                        <?php echo date('H:i a', strtotime( $item->updated )); ?>
                                        <?php
                                            $date_in = new DateTime($item->updated);
                                            $date = new DateTime(date('Y-m-d H:i:s'));
                                            $interval = date_diff($date_in, $date);
                                            $year = $interval->y;
                                            $month = $interval->m;
                                            $days = $interval->days;
                                            $hours = $interval->h;
                                            $minute = $interval->i;
                                            $second = $interval->s;
                                        ?>
                                        <br/>
                                        <small class="text-navy">
                                            <?php 
                                                if((int)$year > 0): 
                                                    echo $year.' năm trước';
                                                elseif((int)$month > 0 && (int)$year == 0): 
                                                    echo $month.' tháng trước';
                                                elseif((int)$days > 0 && (int)$month == 0 && (int)$year == 0 ):
                                                    echo $days.' ngày trước';
                                                elseif((int)$hours > 0 && (int)$days == 0 && (int)$month == 0 && (int)$year == 0):
                                                    echo $hours.' giờ trước';
                                                elseif((int)$minute > 0 && (int)$hours == 0 && (int)$days == 0 && (int)$month == 0 && (int)$year == 0):
                                                    echo $minute.' phút trước';
                                                else:
                                                    echo $second.' giây trước';
                                                endif;
                                            ?>
                                        </small>
                                    </div>
                                    <div class="col-xs-9 content no-top-border"><p><?php echo $item->content; ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>               
   </div>           

   <div class="row" style="margin-top: 15px;">



    </div>

</div>



<script>
$(function () {
    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Tổng quan dữ liệu đào tạo'
        },
        subtitle: {
            text: 'Di chuột vào các cột để xem thông tin chi tiết.'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Tổng số'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },

        series: [{
            name: 'QSTUDY',
            colorByPoint: true,
            data: [{
                name: 'Khóa học',
                y: <?php echo $numRecordCourse;?>,
            }, {
                name: 'Bài giảng',
                y: <?php echo $numRecordLesson;?>,
            }, {
                name: 'Câu hỏi',
                y: <?php echo $numQuestion;?>,
            }]
        }],
    });
});
</script> 