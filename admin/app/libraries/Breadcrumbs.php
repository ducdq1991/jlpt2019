<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Breadcrumbs {
  
  private $breadcrumbs = array();
   
  public function __construct()
  { 
    // start and close div bread
    $this->tag_open = '<ol class="breadcrumb">';
    $this->tag_close = '</ol>';
    $this->crumb_open = '<li>';
    $this->crumb_close = '</li>';
    $this->crumb_last_open = '<li class="active">';
    $this->crumb_divider = '';
    //log_message('debug', "Breadcrumbs Class Initialized");
  }
  /** 
  * [add breadcrumb] [creat breadcrumb]
  **/
  public function add($page, $href)
  {
    if (!$page OR !$href) return;
    // push breadcrumb
    $this->breadcrumbs[$href] = array('page' => $page, 'href' => $href);
  }
  /**
  ** [display breadcrumb]
  **/
  public function display()
  {
    if ($this->breadcrumbs) {
      // set output variable
      $output = $this->tag_open;
      // construct output
      foreach ($this->breadcrumbs as $key => $crumb) {
        $keys = array_keys($this->breadcrumbs);
        if (end($keys) == $key) {
          $output .= $this->crumb_last_open . '<strong>' . $crumb['page'] . '</strong>' . $this->crumb_close;
        } else {
          $output .= $this->crumb_open.'<a href="' . $crumb['href'] . '">' . $crumb['page'] . '</a> '.$this->crumb_close.$this->crumb_divider;
        }
      }
      // return output
      return $output . $this->tag_close . PHP_EOL;
    }
    // no crumbs
    return '';
  }
}