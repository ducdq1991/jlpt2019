<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');        
        $siteLang = $ci->session->userdata('lang');
        if ($siteLang) {
            $ci->lang->load('common',$ci->session->userdata('lang'));
        } else {
            $ci->lang->load('common', 'vietnamese');
        }
    }
}