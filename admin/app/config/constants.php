<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| Banner Type
|--------------------------------------------------------------------------
*/
define('TYPE_SLIDER', 1);
define('TYPE_BANNER', 2);
define('TYPE_ADS', 3);
define('TYPE_OTHER', 0);
/*
|--------------------------------------------------------------------------
| Banner Position
|--------------------------------------------------------------------------
*/
define('TOP_UP', 1);
define('TOP_DOWN', 2);
define('RIGHT_TOP', 3);
define('RIGHT_BOTTOM', 4);
define('LEFT_TOP', 5);
define('LEFT_BOTTOM', 6);
define('CENTER_TOP', 7);
define('CENTER_CENTER', 8);
define('CENTER_BOTTOM', 9);
define('BOTTOM', 10);
define('FOOTER_LEFT', 11);
define('FOOTER_RIGHT', 12);
define('FOOTER_CENTER', 13);
define('FOOTER_BOTTOM', 14);
define('POSITION_3', 15);
define('POSITION_4', 16);
define('POSITION_5', 17);
define('POSITION_6', 18);
define('POSITION_7', 19);
define('POSITION_8', 20);
define('POSITION_9', 21);
define('POSITION_10', 22);
/*
|--------------------------------------------------------------------------
| type of user log
|--------------------------------------------------------------------------
| follow type to query data, general message...
|
*/
define('LOG_IN', 1); // data is null
define('LOG_OUT', 2); // data is null
define('UPDATE_INFO', 3); // data is null
define('VIEW_LESSON', 4); // data is lesson id
define('PURCHASE_LESSON', 5); // data is lesson id
define('DO_EXAM', 6); // data is exam id
define('DOWNLOAD_EXAM', 7); // data is exam id
define('SEND_ANSWER', 8); // data is exam id
define('UPLOAD_ANSWER', 9); // data is exam id
define('DO_EXERCISE', 10); // data is exam id
define('RECHARGE', 11); //data is null or 0
define('PAYMENT', 12);
define('RESULT_EXAM', 13);
define('REGISTER', 14);
define('PURCHASE_COURSE', 15);
define('BONUS_EXAM', 16);
define('BONUS_NORMAL', 17);
define('BONUS_POINT', 18);
define('MINUS_BONUS_NORMAL', 19);