<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
// pagination

$route['coursecategory'] = "coursecategory/index"; 
$route['coursecategory/page-([\d]+)'] = "coursecategory/index/page-$1";

$route['category'] = "category/index"; 
$route['category/page-([\d]+)'] = "category/index/page-$1";
$route['module/page-([\d]+)'] = "module/index/page-$1";
$route['user/page-([\d]+)'] = "user/index/page-$1";
$route['article/page-([\d]+)'] = "article/index/page-$1";
$route['course/page-([\d]+)'] = "course/index/page-$1";
$route['course/id-([\d]+)'] = "course/index/id-$1";
$route['course/members/([\d]+)/page-([\d]+)'] = "course/members/$1/$2";
$route['course/listcodes/([\d]+)/page-([\d]+)'] = "course/listcodes/$1/$2";
$route['course/id-([\d]+)/page-([\d]+)'] = "course/index/id-$1/page-$2";
$route['lesson/page-([\d]+)'] = "lesson/index/page-$1";
$route['exam/page-([\d]+)'] = "exam/index/page-$1";
$route['codes/page-([\d]+)'] = "codes/index/page-$1";
$route['page/page-([\d]+)'] = "page/index/page-$1";
$route['salemans/page-([\d]+)'] = "salemans/index/page-$1";
$route['document/page-([\d]+)'] = "document/index/page-$1";
$route['categorydoc/page-([\d]+)'] = "categorydoc/index/page-$1";
$route['mcq/page-([\d]+)'] = "mcq/index/page-$1";
$route['question/page-([\d]+)'] = "question/index/page-$1";
$route['promotion/page-([\d]+)'] = "promotion/index/page-$1";
$route['examstorage/page-([\d]+)'] = "examstorage/index/page-$1";
$route['exam-category/page-([\d]+)'] = "examstorage/index/page-$1";
$route['practicle/page-([\d]+)'] = "practicle/index/page-$1";
$route['notification/page-([\d]+)'] = "notification/index/page-$1";
$route['testimonies/page-([\d]+)'] = "testimonies/index/page-$1";
$route['mark/page-([\d]+)'] = "mark/index/page-$1";
$route['payments/page-([\d]+)'] = "payments/index/page-$1";
$route['user/page-([\d]+)'] = "user/index/page-$1";
$route['banner/page-([\d]+)'] = "banner/index/page-$1";