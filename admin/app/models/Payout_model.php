<?php defined('BASEPATH') or exit('No direct script access allowed');

class Payout_Model extends CI_Model 
{

	public function __construct() 
	{
		parent::__construct();		
	}

	public function addPayout($args = [])
	{
		try{

			if ($this->db->insert('payouts', $args)){			
				return $this->db->insert_id();
			}

		} catch(Exception $ex) {
			echo $ex->getMessage();
		}

		return false;
	}

	public function getPayout($where = [])
    {
        try {
			$query = $this->db->where($where)->get('payouts');
			if ($query) {
				if (is_object($query->row())) {
					return $query->row();
				}
			}

			return null;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	exit;
		}

		return null;
    }

    public function totalRecord($where = [], $whereIn = [], $likes = [])
	{
		try{

			if (!empty($whereIn)) {
				$this->db->where_not_in($whereIn['field'], $whereIn['value']);
			}	

			if (!empty($likes)){          	
	        	$this->db->like( $likes['field'], $likes['value']); 
	        } 

	        if (!empty($where)) {
	        	$this->db->where($where);
	        }    

	        $total = $this->db->count_all_results('payouts');
			return $total;			

		} catch( Exception $ex ){

		}

		return 0;		
	}

    public function getPayouts($where = [], $whereIn = [], $likes = [], $order = [], $limit = 10, $offset = 0)
    {
        try {
			$query = $this->db->where($where);

			if ($limit > 0) {
				$query->limit($limit, $offset);
			}

			if (!empty($likes)){          	
	        	$query->like( $likes['field'], $likes['value']); 
	        }  

	        if (is_array($order) && !empty($order)){
	        	foreach($order as $key => $value){
	        		$query->order_by($key, $value);
	        	}
	        } else {
	           $query->order_by('id', 'desc');
	        }

			$result = $query->get('payouts');
			if ($result) {
				if ($result = $result->result()) {
					return $result;
				}
			}

			return null;
		} catch (Exception $ex) {
        	echo $ex->getMessage();
        	exit;
		}

		return null;
    }
}