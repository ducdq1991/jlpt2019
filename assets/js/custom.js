function getNumAswed()
{
    num = 0;
    $('.exam-detail-content .box-item' ).each(function() {
        var key = $(this).attr('data-id');
        var value = $(this).find('input[type="radio"]:checked').val();
        value = (typeof value != 'undefined') ? value : null;
        if (value == null) {
            
        } else {
            num++;
        }                        
    });
    return num;
}

function chooseASW(questionId, asw, num) {
    id = questionId + '-' + asw;    
    $('a.' + questionId).removeClass('active');
    $('a#' + id).addClass('active');    
    name = 'answer-' + num;
    $("input[name="+name+"][value="+asw+"]").prop('checked', true);   
    $('span.aswed').empty().html(getNumAswed()); 
}

$(document).ready(function(){


    // facebook plugins
    $('.facebook-container .open-dialog').click(function(event) {
        $(this).hide();
        $('.facebook-container .dialog-content').fadeIn();
    });

    $('.facebook-container .close-dialog').click(function(event) {
        $('.facebook-container .dialog-content').hide();
        $('.facebook-container .open-dialog').fadeIn();
    });


    $('.item_code_checker').on('click', function(){
        $lessonSlug = $(this).attr('lesson-slug');
        $('.btn-check-code').attr('lesson-slug', $lessonSlug);
    });

    $('.btn-check-code').click(function(){
        $lessonSlug = $(this).attr('lesson-slug');
        $courseId = $('#rootCourseId').val();
        $code = $('#course_code_checker').val();
        $.ajax({
            type: "POST",
            url: siteUrl +"courses/checkcode",
            data: {lessonSlug:$lessonSlug, courseId:$courseId, code:$code},
            success: function(data){
                if (data == 2) {
                    alert('Mã này không tồn tại hoặc không được áp dụng cho khóa học này!');
                    return false;
                }

                if (data == 3) {
                    alert('Vui lòng đăng nhập trước khi bắt đầu học!');
                    return false;
                }  

                if (data == 4) {
                    alert('Dữ liệu không hợp lệ!');
                    return false;
                }

                if (data == 1) {
                    window.location.href = '/khoa-hoc/' + $lessonSlug + '.html';
                }
            },
            error: function() { 
                console.log("Error."); 
                return false;
            }
       });                
    });

    $( "div.radio input.answer-item" ).on( "click", function() {
        questionId = $(this).attr('id');
        dataKey = $(this).attr('data-key');
        $('a.' + dataKey).removeClass('active');
        $('a#' + questionId).addClass('active');  
        $('span.aswed').html(getNumAswed());
    });


    $('.vticker').easyTicker({
        direction: 'up',
        easing: 'easeInOutBack',
        speed: 'slow',
        interval: 2000,
        height: 'auto',
        visible: 1,
        mousePause: 0,
        controls: {
            up: '.up',
            down: '.down',
            toggle: '.toggle',
            stopText: 'Stop !!!'
        }
    }).data('easyTicker');
    
    $("#banner-top").owlCarousel({
                items: 1,
                slideSpeed: 1000,
                autoPlay:true,
                navigation: true,
                navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                pagination: true
            });
    if($('#item-commnent').length > 0){
        $("#item-commnent").owlCarousel({
                items: 1,
                slideSpeed: 1000,
                autoPlay:true,
                navigation: false,
                pagination: true
        });
    }
    $('#menu .dis-hidden-search i').click(function(){
        $('#search').slideToggle(500);
    });
    //Display Login Form
    $('.btn-head-login').click(function() {
        $('#login').css('display','block');
        $('.overlay').css('display','block');
    });
    // Hide Login Form
    $('.close-login').click(function() {
        $('#login').css('display','none');
        $('.overlay').css('display','none');
    });
    //event enter
    $('#login').keypress(function(e) {
        if(e.which == 13) {
            $('.btn-login').click();
        }
    });
    $('.overlay').click(function() {
        $('#login').css('display','none');
        $('.overlay').css('display','none');
    });
    /*-------Ajax Login---------------*/
    $('.btn-login').click(function() {
        var form = $('form#formLogin');
        $.ajax({
            type: "POST",
            url: siteUrl +"user/login",
            data: form.serialize(),
            dataType: "html",
            success: function(data){
                var res = jQuery.parseJSON(data);
                if(res.status == 100){
                    $('.errorLogin').empty().append(res.message);
                    return false;
                } else {
                    window.location.href = res.url;
                    return false;
                }
            },
            error: function() { 
                console.log("Error."); 
                return false;
            }
       });
    });
    //set course_id for practice
    if(typeof _course_id_practice != 'undefined') {
        $('#practice-module #course_id option').each(function(){
            if($(this).val() == _course_id_practice){
                $(this).prop('selected', true);
            }
        });
    }
    //checklogin when do exam
    $('.logged').click(function(){
        var url = $(this).attr('href');
        if(typeof url == 'undefined') {
            url = '';
        }
        $.ajax({
            method: 'POST',
            url: siteUrl +"user/logged",
            data: {redirector:url},
            dataType: "json",
            success: function(res){
                //console.log(res); return false;
                if(res.code == 403) {
                    $('#display-login').click();
                    return false;
                } else if(res.code == 200) {
                    if(url == '') {
                        return;
                    }
                    //window.open(url);
                    window.location.href = url;
                    return false;
                }
            },  
            error: function() { 
                console.log("Error."); 
                return false;
            }
        });
        return false;
    });
    //button reset
    $('#reset-page').click(function(){
        window.location.reload();
    });
    //del question saved
    $('#user-module .question-saved-del').click(function() {
        var question_id = $(this).attr('data-id');
        var delQuestion = confirm("Bạn muốn xóa câu hỏi đã lưu?");
        if (delQuestion == true) {
            $.ajax({
                url: siteUrl+ 'user/delQuestionSave',
                method: 'POST',
                data: {'question_id':question_id},
                dataType: 'json',
                success: function(response){
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $('input[name=coupon_code]').change(function(){
        let courseId = $('input[name=course_id]').val();   
        let coupon_code = $(this).val();
        let price = parseFloat($('input[name=course_price]').val());
        let total = price;
        $.ajax({
            url: siteUrl+ 'courses/coupon',
            method: 'POST',
            data: {coupon_code:coupon_code, course_id: courseId},
            dataType: 'json',
            success: function(res){                
                if (res.status == 1) {
                    $('.discountFee').show();
                    $('#totalDiscount').empty().html(numberWithCommas(parseFloat(res.discount))  + '<sup>đ</sup>');
                    total -= parseFloat(res.discount);
                    console.log(total);
                    $('#courseTotal').empty().html(numberWithCommas(total)  + '<sup>đ</sup>');
                } else {
                    $('#courseTotal').empty().html(numberWithCommas(total)  + '<sup>đ</sup>');
                }
            }
        });
    })

    $('#btn-purchase').click(function(){
        var id = $(this).attr('data-id');
        var price = $(this).attr('data-price');
        var _html = '';
        _html += 'Học phí: <strong>'+ numberWithCommas(price) + ' đ</strong>';
        _html += '<input type="hidden" name="course_price" value="' + price + '">';
        $('#courseTotal').empty().html(numberWithCommas(price) + '<sup>đ</sup>');
        $('#purchaseModal .price-text').html(_html);
        $('#purchaseModal .purchase').attr('data-id', id);
    });
    $('#purchaseModal .purchase').click(function() {
        let id = $(this).attr('data-id');
        let coupon_code = $('input[name=coupon_code]').val();
        $.ajax({
            url: siteUrl+ 'courses/purchaseCourse',
            method: 'POST',
            data: {id:id, coupon_code:coupon_code},
            success: function(response){
                if (response == 10) {
                    window.location.href = '/thanh-toan';
                } else {
                    location.reload();    
                }
            }
        });
    })
    //survey form
    $('#btn-send-survey').click(function(e){
        e.preventDefault();
        var data = new FormData($('#survey-upload-form')[0]);
        $.ajax({
            type:"POST",
            url:siteUrl +"index/examination",
            data:data,
            dataType: 'json',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success:function(response)
            {
                if(response.code == 203) {
                    generateToast('warning', 'Hệ thống', response.message);
                    return;
                }
                if(response.code == 200) {
                    generateToast('success', 'Hệ thống', response.message);
                }
                if(response.code == 202) {
                    generateToast('warning', 'Hệ thống', response.message);
                }
                if(response.code == 201) {
                    generateToast('error', 'Hệ thống', response.message);
                }
                $('#examinationModal').modal('toggle');
                $('#survey-upload-form')[0].reset();
            }
       });
        return false;
    });
    //rank form submit
    $('#btn-rank-submit').click(function(){
        $('form#rank-search-form').submit();
    });
    $('#btn-rank-month-submit').click(function(){
        $('form#rank-month-search-form').submit();
    });
    $('#detail-lesson .video-lesson').find('iframe').find('.ytp-watermark').css('display', 'none');
    //auto load more
    $('.btn-load-more').on('click', function() {
        //if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var offset = parseInt($('#vertical-timeline').attr('data-offset'));
            if(offset != 0) {
                $.ajax({
                    url: siteUrl +'/user/activityByUser',
                    method: 'POST',
                    data: {offset: offset},
                    dataType: 'json',
                    success: function(res){
                        $('#vertical-timeline').attr('data-offset', res.offset);
                        $('#vertical-timeline #mCSB_1_container').append(res.html);
                        if(res.offset == 0) {
                            $('.load-more a').hide();
                        }
                    },
                    beforeSend: function() {
                        $('.load-more p').show();
                        $('.load-more a').hide();
                    },
                    complete: function(res) {
                        var response = $.parseJSON(res.responseText);
                        if(response.offset != 0) {
                            $('.load-more a').fadeIn('slow');
                        }
                        $('.load-more p').fadeOut('slow');
                    },
                });
            }
        //}
    });
    $('#vertical-timeline').mCustomScrollbar();

    //resolve form user question request
    $('#send-question').click(function(){
        //var content = $('form#user-request-form textarea[name=content]').val();
        //var title = $('form#user-request-form input[name=title]').val();
        var form = $('form#user-request-form');
        $.ajax({
            url: siteUrl +'faqs/sendquestion',
            method: 'POST',
            data: form.serialize(),
            dataType: 'json',
            success: function(res){
                if (res.code == 201) {
                    generateToast('warning', 'Hệ thống:', res.msg);
                }
                if(res.code == 202) {
                    generateToast('error', 'Hệ thống:', res.msg);
                }
                if(res.code == 200) {
                    generateToast('success', 'Hệ thống:', res.msg);
                    $('form#user-request-form')[0].reset();
                }
                return;
            }
        });
    });
    $('#add-answer-upload').on('click', function(){
        $(this).before('<input type="file" name="answer_file_upload[]"><br/>');
    });
});

function generateToast(shortCutFunction, title, msg) {
    var shortCutFunction = shortCutFunction;
    var msg = msg;
    var title = title;
    var $showDuration = 400;
    var $hideDuration = 1000;
    var $timeOut = 7000;
    var $extendedTimeOut = 1000;
    var $showEasing = 'swing';
    var $hideEasing = 'linear';
    var $showMethod = 'fadeIn';
    var $hideMethod = 'fadeOut';
    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: true,
        positionClass: 'toast-top-left',
        onclick: null
    };
    $("#toastrOptions").text("Command: toastr["
            + shortCutFunction
            + "](\""
            + msg
            + (title ? "\", \"" + title : '')
            + "\")\n\ntoastr.options = "
            + JSON.stringify(toastr.options, null, 2)
    );
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
}
function popup_left_min()
{
    $('.banner.popup.left-popup .btn-control.min').hide();
    $('.banner.popup.left-popup .btn-control.max').show();
    $('.banner.popup.left-popup .popup-content').hide();
}
function popup_left_max()
{
    $('.banner.popup.left-popup .btn-control.min').show();
    $('.banner.popup.left-popup .btn-control.max').hide();
    $('.banner.popup.left-popup .popup-content').show();
}
function popup_left_hide()
{
    $('.banner.popup.left-popup').hide();
}
function popup_right_min()
{
    $('.banner.popup.right-popup .btn-control.min').hide();
    $('.banner.popup.right-popup .btn-control.max').show();
    $('.banner.popup.right-popup .popup-content').hide();
}
function popup_right_max()
{
    $('.banner.popup.right-popup .btn-control.min').show();
    $('.banner.popup.right-popup .btn-control.max').hide();
    $('.banner.popup.right-popup .popup-content').show();
}
function popup_right_hide()
{
    $('.banner.popup.right-popup').hide();
}