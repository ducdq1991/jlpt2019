$(document).ready(function(){
    //Dong ho bam gio
	var check_view_result = false;
	var exam_time = parseInt( $('.exam-detail-heading .count-down').attr('data-time') );
	if (exam_time != 0) {
		var h = 0;
		var m = 0;
		var s = 0;
		var time_display = $('.exam-detail-heading .count-down span');
		if(exam_time>=60) {
			h = parseInt(exam_time/60);
			m = exam_time%60;
		} else {	
			m = exam_time;
		}
        $('.exam-detail-heading .count-down').show();
		var count_down_time = setInterval(function(){
            if(m == 0 && h>0) {
                h--;
                m = 60;
            }
			if( (s == 0 && m>0) || ( s==0 && m==0 && h>0 )) {
				m--;
				s = 60;
			}
			var hour = h<10 ? '0' + h : h;
			var munite = m<10 ? '0' + m : m;
			var second = s<10 ? '0' + s : s;
			time_display.text(hour+':'+munite+':'+second);
			if ( parseInt(h, 10) == 0 && parseInt(m, 10) == 0 && parseInt(s, 10) == 0 ) {
   				clearInterval(count_down_time);
                $('#view-result').click();
   				generateToast('info','Thông báo', 'Hết giờ làm bài!');
			}
			s--;
		}, 1000);
	}

    $('.exam-detail-content .box-meta a.protected').click(function(e){
        if(!check_view_result && isExam == false) {
            e.preventDefault();
            generateToast('warning','Thông báo', 'Bạn phải làm bài trước!');
            return false;
        }
    });

	//Xem dap an PDF
    jQuery('.previewPdf').click(function(){
        var embedUrl = '';
        var id = $(this).attr('data-url');
        console.log(id);
        var _ob = $(this).attr('data-id');  
        $.ajax({
            url: '/practice/getLoiGiai',
            method: 'GET',
            data: {id:id},
            success: function(html) {                
                console.log(_ob);  
                console.log(html);
                $('#fileCollapse-'+_ob).html(html);                
            }
        });
                    
    });


    //Xem dap an Video
    jQuery('.previewVideo').click(function(e){
        if(!check_view_result) {
            e.preventDefault();
            return false;
        }
        var _ob = $(this).attr('data-id');
        $('#videoCollapse-'+_ob).on('hidden.bs.collapse', function () {
            $('#videoCollapse-'+_ob).html('');
            return;
        })
        jQuery('#videoCollapse-'+_ob).on('hidden.bs.modal', function(e) {   
            console.log('close');return; 
            var $if = $(e.delegateTarget).find('iframe');
            var src = $if.attr("src");
            $if.attr("src", '');
            return;
        });
        var embedUrl = $(this).attr('data-url');
        
        iframe = '<iframe width="100%" height="400" src="'+embedUrl+'" frameborder="0"></iframe>';
        jQuery('#videoCollapse-'+_ob).empty().append(iframe);        
        
    });


    $('.view-result').click(function(){
        if(count_down_time)
    	   clearInterval(count_down_time);
        
    	check_view_result = true;
        var option = [];
        var noChooseAsw = [];
    	$('#practice-form-content .box-item').each(function() {
    		var key = $(this).attr('data-id');
    		var value = $(this).find('input[type="radio"]:checked').val();
            value = (typeof value != 'undefined') ? value : null;
            var op = {
                'id' : key,
                'true_answer' : value
            };
            if (value == null) {
               noChooseAsw.push(op); 
            }
            option.push(op); 
    	});

        if (noChooseAsw.length > 0) {
            //alert('Bạn còn ' + noChooseAsw.length + ' chưa làm!');
            //return false;
        }

        $("input[type=radio]").attr('disabled', true);        
        if(typeof _exam_id != 'undefined'){
            var data = {
                'option' : option,
                'exam_id' : _exam_id,
            }
        }

    	$.ajax({
    		method: 'POST',
    		data: data,
    		dataType: 'json',
    		url: _url_result,
    		success: function(res){
                $('.view-result').hide();
    			$('.exam-detail-content .box-result span.mes-result').html(res.message);
    			$('.exam-detail-content .box-result').css('display', 'table');
    			$('.box-item').each(function(){
    				var ques_id = $(this).attr('data-id');
                    var $sheetResultId = $('a#' + ques_id + '-' + res.answer[ques_id]);
                    var $activeAsw = $('a#' + ques_id + '-' + res.answer[ques_id]);                    
    				if($.inArray(ques_id, res.result) != -1) {
    					if( $(this).hasClass('answer-wrong') ) {
    						$(this).removeClass('answer-wrong');
    					}
    					$(this).addClass('answer-right');
                        $sheetResultId.addClass('asw-right');
    					$('.answer-right span.right').html('<i class="fa fa-check" aria-hidden="true"></i> Đúng');
    				} else {
                        $('.view-result').hide();
    					if( $(this).hasClass('answer-right') ) {
    						$(this).removeClass('answer-right');
    					}
    					$(this).addClass('answer-wrong');
                        $sheetResultId.addClass('asw-wrong');
    					$('.answer-wrong span.wrong').html('<i class="fa fa-times" aria-hidden="true"></i> Sai');
    				}
                    $(this).find('.previewVideo').attr('href', '#videoCollapse-'+ques_id);
                    $(this).find('.previewPdf').attr('href', '#fileCollapse-'+ques_id);
                    var $radio = $(this).find('.answer-item[value='+res.answer[ques_id]+']').parent().parent();
                    $radio.addClass('answer-true');
    			});
    			check_view_result = true;
    		}
    	});
        $('.view-result').hide();

    });
});


function generateToast(shortCutFunction, title, msg) {
    var shortCutFunction = shortCutFunction;
    var msg = msg;
    var title = title;
    var $showDuration = 400;
    var $hideDuration = 1000;
    var $timeOut = 7000;
    var $extendedTimeOut = 1000;
    var $showEasing = 'swing';
    var $hideEasing = 'linear';
    var $showMethod = 'fadeIn';
    var $hideMethod = 'fadeOut';
    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: true,
        positionClass: 'toast-top-left',
        onclick: null
    };
    $("#toastrOptions").text("Command: toastr["
            + shortCutFunction
            + "](\""
            + msg
            + (title ? "\", \"" + title : '')
            + "\")\n\ntoastr.options = "
            + JSON.stringify(toastr.options, null, 2)
    );
    var $toast = toastr[shortCutFunction](msg, title);
}