function getNumAswed()
{
    num = 0;
    $('.exam-detail-content .box-item' ).each(function() {
        var key = $(this).attr('data-id');
        var value = $(this).find('input[type="radio"]:checked').val();
        value = (typeof value != 'undefined') ? value : null;
        if (value == null) {
            
        } else {
            num++;
        }                        
    });
    return num;
}

function chooseASW(questionId, asw, num) {
    id = questionId + '-' + asw;    
    $('a.' + questionId).removeClass('active');
    $('a#' + id).addClass('active');    
    name = 'answer-' + num;
    $("input[name="+name+"][value="+asw+"]").prop('checked', true);   
    $('span.aswed').empty().html(getNumAswed()); 

    $('.answer-item-' + questionId).attr('disabled', 'disabled');
    $('.quick-answer-' + questionId + ' label a').attr('onclick', '');
    getResult(questionId, asw);
}

function getResult(questionId, answer)
{
    $.ajax({
        url: '/exam/checkresult',
        method: 'POST',
        data: {exam_id: _data_exam_id, question_id:questionId, option: answer},
        dataType:'json',
        success: function(res) {
            console.log(res);    
            if (res.status == true) {
                showGift('success','', '<i class="fa fa-gift" style="font-size:25px;"></i> ' + res.message);
            }       
        }
    }); 
}

$(document).ready(function(){

    $( "div.radio input.answer-item" ).on( "click", function() {
        questionId = $(this).attr('id');
        dataKey = $(this).attr('data-key');
        $('a.' + dataKey).removeClass('active');
        $('a#' + questionId).addClass('active');  
        $('span.aswed').html(getNumAswed());
        console.log('.answer-item-' + dataKey);

        $('.answer-item-' + dataKey).attr('disabled', 'disabled');
        $('.quick-answer-' + dataKey + ' label a').attr('onclick', '');
        getResult(questionId, $(this).val());
    });

    var $timeExcution;
    var hours;
    var minutes;
    var seconds;
    /** BEGIN EXAM TIME CHECKER ONLINE */
    function showRemaining() {
        var now = new Date();    
        var distance = endExamTime - now;
        if (distance < 0) {

            clearInterval($examTimer);
            document.getElementById('countdown').innerHTML = 'Hết giờ làm bài!';
            generateToast('info','Thông báo', 'Hết giờ làm bài!');
            $timeExcution = '';
            sendResult();
            return;
        }

        var days = Math.floor(distance / _day);
        hours = Math.floor((distance % _day) / _hour);
        minutes = Math.floor((distance % _hour) / _minute);
        seconds = Math.floor((distance % _minute) / _second);
        var $strHours = (hours < 10) ? '0' + hours : hours;
        var $strMinutes = (minutes < 10) ? '0' + minutes : minutes;
        var $strSeconds = (seconds < 10) ? '0' + seconds : seconds;
        $stringTimer = $strHours + ':' + $strMinutes + ':' + $strSeconds;
        $timeExcution = $stringTimer;
        document.getElementById('countdown').innerHTML = $stringTimer; 
        $('.count-down span').html($stringTimer);       
    }

    $examTimer = setInterval(showRemaining, 1000);

    var check_view_result = false;

    /** END EXAM TIME CHECKER ONLINE */
    exam_time = parseInt($('.exam-detail-heading .count-down').attr('data-time') );
    $totalMunite = parseInt($('.exam-detail-heading .count-down').attr('data-time'));
	
	/*if (exam_time > 0) {
		var h = 0;
		var m = 0;
		var s = 0;
		var time_display = $('.exam-detail-heading .count-down span');
		if(exam_time>=60) {
			h = parseInt(exam_time/60);
			m = exam_time%60;
		} else {	
			m = exam_time;
		}
        //$('.exam-detail-heading .count-down').show();
		count_down_time = setInterval(function(){
            if(m == 0 && h>0) {
                h--;
                m = 60;
            }
			if( (s == 0 && m>0) || ( s==0 && m==0 && h>0 )) {
				m--;
				s = 60;
			}
			var hour = h<10 ? '0' + h : h;
			var munite = m<10 ? '0' + m : m;
			var second = s<10 ? '0' + s : s;
			//time_display.text(hour+':'+munite+':'+second);
			if ( parseInt(h, 10) == 0 && parseInt(m, 10) == 0 && parseInt(s, 10) == 0 ) {
   				clearInterval(count_down_time);
   				//sendResult();
                generateToast('info','Thông báo', 'Hết giờ làm bài!');
			}
			s--;
		}, 1000);
	}*/


    $('.button-save').on('click', function(event){
        event.preventDefault();
        var $this = $(this).parent();
        var $grand_father = $(this).parent().parent();
        var question_id = $(this).attr('data-id');
        $.ajax({
            method: 'POST',
            data: {'id': question_id},
            dataType: 'json',
            url: siteUrl + 'practice/saveExam',
            success: function(res){
                if(res.code == 200){
                    $this.find('i.save-success').show();
                    $grand_father.find('.save-message').text(res.message);
                    setTimeout(function() { 
                        $this.find('i.save-success').hide();
                        $grand_father.find('.save-message').text('');
                    }, 3000);
                    return false;
                } else {
                    $this.find('i.save-failed').show();
                    $grand_father.find('.save-message').text(res.message);
                    setTimeout(function() { 
                        $this.find('i.save-failed').hide();
                        $grand_father.find('.save-message').text('');
                    }, 3000);
                    return false;
                }
            }
        });
    });

    $('#submit-modal').click(function(){
        sendResult();
        $('#confirmModal').modal('toggle');
    });

    $('.view-result').click(function(){
        sendResult();
    });
    
    function sendResult(){
        if(typeof count_down_time != 'undefined')
            clearInterval(count_down_time);
            clearInterval($examTimer);
        var option = [];
        var noChooseAsw = [];
        var timer = parseFloat($totalMunite - ( hours*60 + minutes + parseFloat(seconds/60) ));
        $('.exam-detail-content .box-item' ).each(function() {
            var key = $(this).attr('data-id');
            var value = $(this).find('input[type="radio"]:checked').val();
            value = (typeof value != 'undefined') ? value : null;
            var op = {
                'id' : key,
                'true_answer' : value
            };
            if (value == null) {
               noChooseAsw.push(op); 
            }

            option.push(op);                        
        });

        if (noChooseAsw.length > 0) {
            //alert('Bạn còn ' + noChooseAsw.length + ' chưa làm!');
            //return false;
        }

        $('#aswed').html(option.length);
        $("input[type=radio]").attr('disabled', true);
        $(".send-result").attr('disabled', true);
        $('#upload_answer').attr('disabled', true);

        if(typeof _data_exam_id != 'undefined'){
            var data = {
                'option' : option,
                'exam_id' : _data_exam_id,
                'time' : timer,
                'time_execution': $timeExcution,
            }
        }
        //console.log(option);return;
        $.ajax({
            method: 'POST',
            data: data,
            dataType: 'json',
            url: _url_result,
            success: function(res){
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
                $('.exam-detail-content .box-result span.mes-result').html(res.message);
                $('.exam-detail-content .box-result').css('display', 'table');
                if(typeof res.result != 'undefined') {
                    $('.box-item').each(function(){
                        var ques_id = $(this).attr('data-id');
                        var $sheetResultId = $('a#' + ques_id + '-' + res.answer[ques_id]);
                        var $activeAsw = $('a#' + ques_id + '-' + res.answer[ques_id]);
                        if(typeof res.answer != 'undefined') {
                            var $radio = $(this).find('.answer-item[value='+res.answer[ques_id]+']').parent().parent();                                                        
                            $radio.addClass('answer-true');
                            $sheetResultId.addClass('asw-true');
                        }
                        if($.inArray(ques_id, res.result) != -1) {                            
                            if( $(this).hasClass('answer-wrong') ) {
                                $(this).removeClass('answer-wrong');
                            }
                            $(this).addClass('answer-right');
                            $sheetResultId.addClass('asw-right');
                            $('.answer-right span.right').html('<i class="fa fa-check" aria-hidden="true"></i> Đúng');
                        } else {
                            if( $(this).hasClass('answer-right') ) {
                                $(this).removeClass('answer-right');
                            }
                            $sheetResultId.addClass('asw-wrong');
                            $(this).addClass('answer-wrong');
                            $('.answer-wrong span.wrong').html('<i class="fa fa-times" aria-hidden="true"></i> Sai');
                        }
                    });
                    check_view_result = true;
                }
            },
            beforeSend: function() {
                $('.overlay-black').show();
            },
            complete: function() {
                $('.overlay-black').fadeOut('slow');
            },
        });
    }
   //load preview PDF document
    jQuery('.previewPdf').click(function(){
        var embedUrl = $(this).attr('data-url');
        var extension = embedUrl.split('.').pop();
        var _ob = $(this).attr('data-id');
        if (extension == 'pdf') {
            var iframe = '<iframe src="http://docs.google.com/gview?url='+embedUrl+'&embedded=true" style="width:100%; height:300px;" frameborder="0"></iframe>';            
        } else {
            var iframe = '<img src="' + embedUrl + '">';
        }
        
        jQuery('#fileCollapse-'+_ob).empty().append(iframe);
        //http://infolab.stanford.edu/pub/papers/google.pdf file test
    });
    //load preview video
    jQuery('.previewVideo').click(function(e){
        if(!check_view_result) {
            e.preventDefault();
            return false;
        }
        var _ob = $(this).attr('data-id');
        $('#videoCollapse-'+_ob).on('hidden.bs.collapse', function () {
            $('#videoCollapse-'+_ob).html('');
            return;
        })
        jQuery('#videoCollapse-'+_ob).on('hidden.bs.modal', function(e) {   
            console.log('close');return; 
            var $if = $(e.delegateTarget).find('iframe');
            var src = $if.attr("src");
            $if.attr("src", '');
            return;
        });
        var embedUrl = $(this).attr('data-url');
        
        iframe = '<iframe width="100%" height="500" src="'+embedUrl+'" frameborder="0"></iframe>';
        jQuery('#videoCollapse-'+_ob).empty().append(iframe);
        //jQuery('#videoCollapse-'+_ob).find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    $('.exam-detail-content .box-meta a.protected').click(function(e){
        if(!check_view_result) {
            e.preventDefault();
            if (is_done == 0) {
                generateToast('warning','Thông báo', 'Bạn phải làm bài trước!');
                return false;
            }
        }
    });
    //load preview PDF document
    jQuery('#view_exam_answer').click(function(){
        var embedUrl = $(this).attr('data-url');
        var iframe = '<iframe src="http://docs.google.com/gview?url='+embedUrl+'&embedded=true" style="width:100%; height:1000px;" frameborder="0"></iframe>';
        jQuery('#previewPDF span').empty().append(iframe);
        //http://infolab.stanford.edu/pub/papers/google.pdf file test
    });
    /*-------Ajax Login---------------*/
    $('#send-file-upload').click(function(e) {
        e.preventDefault();
        $('#upload-file-modal').modal('toggle');
        if(typeof count_down_time != 'undefined')
            clearInterval(count_down_time);
            clearInterval($examTimer);
        var timer = parseFloat($totalMunite - ( hours*60 + minutes + parseFloat(seconds/60) ));
        $('#upload-answer-file-form input[name=time]').val(timer);
        var data = new FormData($('#upload-answer-file-form')[0]);
        $.ajax({
            type:"POST",
            url:siteUrl +"exam/uploadFile",
            data:data,
            dataType: 'json',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            success:function(response)
            {
                if(response.code == 200) {
                    generateToast('success', 'Hệ thống', response.message);
                    $('#upload_answer').attr('disabled', true);
                    $('#upload_answer').hide();
                }
                if(response.code == 202) {
                    generateToast('warning', 'Hệ thống', response.message);
                    $('#upload_answer').attr('disabled', true);
                    $('#upload_answer').hide();
                }
                if(response.code == 201)
                    generateToast('error', 'Hệ thống', response.message);
            },
            beforeSend: function() {
                $('.overlay-black').show();
            },
            complete: function() {
                $('.overlay-black').fadeOut('slow');
            },
       });
        return false;
    });
    $('#view_exam_answer').click(function(){
        $(this).toggleClass('hidden-text');
        if($(this).hasClass('hidden-text')) {
            $(this).text('Ẩn đáp án');
        } else {
            $(this).text('Xem đáp án');
        }
    });
    $('#add-answer-upload').click(function(){
        $(this).before('<input type="file" name="answer_file_upload[]"><br/>');
    });
});
function generateToast(shortCutFunction, title, msg) {
    var shortCutFunction = shortCutFunction;
    var msg = msg;
    var title = title;
    var $showDuration = 400;
    var $hideDuration = 1000;
    var $timeOut = 7000;
    var $extendedTimeOut = 1000;
    var $showEasing = 'swing';
    var $hideEasing = 'linear';
    var $showMethod = 'fadeIn';
    var $hideMethod = 'fadeOut';
    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: true,
        positionClass: 'toast-top-left',
        onclick: null
    };
    $("#toastrOptions").text("Command: toastr["
            + shortCutFunction
            + "](\""
            + msg
            + (title ? "\", \"" + title : '')
            + "\")\n\ntoastr.options = "
            + JSON.stringify(toastr.options, null, 2)
    );
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
}

function showGift(shortCutFunction, title, msg) {
    var shortCutFunction = shortCutFunction;
    var msg = msg;
    var title = title;
    var $showDuration = 40000;
    var $hideDuration = 100000;
    var $timeOut = 700000;
    var $extendedTimeOut = 1000000;
    var $showEasing = 'swing';
    var $hideEasing = 'linear';
    var $showMethod = 'fadeIn';
    var $hideMethod = 'fadeOut';
    toastr.options = {
        closeButton: true,
        debug: false,
        progressBar: true,
        positionClass: 'toast-top-left',
        onclick: null,
        icon: 'info',
        positionClass: 'toast-bottom-right'
    };
    $("#toastrOptions").text("Command: toastr["
            + shortCutFunction
            + "](\""
            + msg
            + (title ? "\", \"" + title : '')
            + "\")\n\ntoastr.options = "
            + JSON.stringify(toastr.options, null, 2)
    );
    var $toast = toastr[shortCutFunction](msg, title);
}

